
/**
* Example snippets about jQuery validation plugin. Autumn 2014.
*
*
*
*/




/* Fire Valaidate */
$(document).ready(function(){
   $.validator.addMethod("nourl", 
      function(value, element) {
         return !/http\:\/\/|www\.|link\=|url\=/.test(value);
            }, 
         "No URL's"
      );
   $("#form").validate({
      rules: {
         comments: {
            nourl: true
             }
         },
         messages: {
            comments: "No URL's"
         }
     });
});
----------------
// parent   //parent ili taj  //samo na labelu sa tom klasom se odnosi ovaj css
#register-form .fieldgroup label.error {
    color: #FB3A3A;
    display: inline-block;
    margin: 4px 0 5px 125px;
    padding: 0;
    text-align: left;
    width: 220px;
}

<!-- HTML form for validation demo -->
<form action="" method="post" id="register-form" novalidate="novalidate">
 
    <h2>User Registration</h2>
 
    <div id="form-content">
        <fieldset>
 
            <div class="fieldgroup">
                <label for="firstname">First Name</label>
                <input type="text" name="firstname"/>
            </div>
 
            <div class="fieldgroup">
                <label for="lastname">Last Name</label>
                <input type="text" name="lastname"/>
            </div>
 
            <div class="fieldgroup">
                <label for="email">Email</label>
                <input type="text" name="email"/>
            </div>
 
            <div class="fieldgroup">
                <label for="password">Password</label>
                <input type="password" name="password"/>
            </div>
 
            <div class="fieldgroup">
                <p class="right">By clicking register you agree to our <a target="_blank" href="/policy">policy</a>.</p>
                <input type="submit" value="Register" class="submit"/>
            </div>
 
        </fieldset>
    </div>
 
        <div class="fieldgroup">
            <p>Already registered? <a href="/login">Sign in</a>.</p>
        </div>
</form>

---------------------
$('#formid').serialize();// kao query string "Fname=&Lname=&quote=Enter+your+favorite+quote!&education=Jr.High"
$('#formid').serializeArray();// kao hashmapa name value [{"name":"Fname","value":""},{"name":"education","value":"Jr.High"}]
//serijalizuje formu kao objekat bez submit
//jquery plugin poziv na tacku na var json = $(#form_id).serializeObject()
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(function() {
 
    $('#idd').click(function() {
		$('#result').text(JSON.stringify($('form').serializeObject()));
		return false;
    });
});
-----------------
//sredjen za 0 i ''
$('#my-form').serializeObject();
(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);
------------------------
poziv anonimne fje
(function(arg1,arg2,arg3) {
	//definicija	
	}
)(arg1,arg2,arg3);// jQuery jeste alias $ , prosledjen

-------------------
fja( {} ) // znaci prima objekat kao arg
-------------------
form.submit(); // trigeruje submit kao da je neko pritiso submit dugme
---------
//ceo jquery je na foru objekta, $.ajax success:, error: itd

	$('#modal').find('form[name="' + name + '"]').validate({//objekat
		rules: ValidationRules[name],		//postavis propertije objekta
		messages: ValidationMessages[name],//name messages samo za taj modal
		onkeyup: false,						//dogadjaji na koje se poziva
		onfocusout: function (element) {	//definisan objekat options, property je bool ili bool fja koja prima element arg, a event handler prima event i validator
			$(element).val($.trim($(element).val())); //element je input tag
			$(element).valid(); 			//trigerujes validaciju, ugradjena
		}
	});
	
----------------------
							//ime fje
jQuery.validator.addMethod("ccDescriptionAllowed", function (value, element) { //value string koji je korisnik upisao koij se prover
	var result = (value == (/[a-z0-9 '\-.&\/]*/i).exec(value));					// element input tag
	return this.optional(element) || result;	// or - oba treba false da bi bilo false. 1 true vraca true
}, "message: poruka recimo Invalid email.");		// fo je ako si ga postavio kao optional input polje da se ne buni

--------

$(document).ready(function () {
	
	//validator je bool metod koji odlucuje da li ili ne da se ispise poruka, ova sa kraja fje 'Please enter a non zero integer value!'
	//radi na binding dok korisnik pise da ga ispravi, ne na submit
	//custom validator prakticno
    jQuery.validator.addMethod('integer', function (value, element, param) {
        //alert("pozvan");
        return (value != 0) && (value == parseInt(value,http://jsfiddle.net/2VJZT/#run 10));http://jsfiddle.net/2VJZT/#run
    }, 'Please enter a non zero integer value!');

    $('#myform').validate({
        rules: {
            field1: {
                required: true, //meces true ako koristis to pravilo, false ako ne, a ne mmatchuje se true false sa addMethod() bool povrat vredn
                integer: true //evo ovde pozvan, zove se integer
            },
            field2: {
                required: true,
                integer: true
            }
        },
        submitHandler: function (form) { // poziva se na submit dogadjaj
            alert('valid form');
            return false;
        }
    });

});


-----
// samo pravila i poruke

    var JQUERY4U = {};

    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //form validation rules
            $("#register-form").validate({
                rules: {
                    firstname: "required",
                    lastname: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    agree: "required"
                },
                messages: {
                    firstname: "Please enter your firstname",
                    lastname: "Please enter your lastname",
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    email: "Please enter a valid email address",
                    agree: "Please accept our policy"
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });
----------
//ne mozes unutar objekta da pauziras debuger
----------	
//Each message can be a String or a Callback. The callback is called in the scope
//of the validator, with the rule’s parameters as the first argument and the element
//as the second, and must return a String to display as the message.
$(".selector").validate({
	rules: {
		name: {
			required: true,
			minlength: 2
		}
	},
	messages: {
		name: {
			required: "We need your email address to contact you",
			minlength: jQuery.format("At least {0} characters required!")// u {0} se prosledjuje minlength tj 2, jer je callback za to pravilo
		}
	}
});
// apply() call() javascript custom context pass as param
//jquery $.proxy()
// implict params context	
// arguments.callee.count = ++arguments.callee.count || 1 //1 kao default ako ovo nije definisan	

------------

//19 9
validate(optionsObjekat) postavlja pravila tj rules, messages, dogadjaje, a valid() proverava validnost forme ili polja na osnovu postavljenog
submitHandler: function(form) // submit hendler poziva se na submit dogadjaj
----
debug: true prevent submit forme i ispisuje dodatno u konzolu

<form id="myform">
	<label for="field">Required, URL: </label>
	<input class="left" id="field" name="namePolja">
</form>

$( "#myform" ).validate({ //prima options objekat
	rules: {
		namePolja: {		//gadja se na ime polja tu
			required: true,
			url: true
				}
			}
});

jQuery.validator.addMethod() //staticka funkcija

//-----------------------------------
//-----------------------------------
//all in one example

<form name="emailConfirm">
	<input type="hidden" name="bn" value="{{bn}}" />
	<div class="form-group side-lbl">
		<label class="lbl side-lbl">Email</label>
		<input type="text" name="email" value="" class="form-control side-lbl medium" />
	</div>
</form>
			

var ValidationRules = {
	'emailConfirm': {//name forme
		'email': {  //name input text polja
			required: true,
			emailCustom: true,//ukljucen helper
			maxlength: 45
		}
	},
	'changeCC': {
	...
	
	var ValidationMessages = {
	'emailConfirm': {//name forme
		'email': {  //name text polja
		}
	},
	'changeCC': {

//helper, vraca bool	
jQuery.validator.addMethod("emailCustom", function (value, element) {
	var val = $.trim(value);
	var allowedChars = (val == (/^[a-z0-9.\-_@]*$/i).exec(val)),
		atPosOk = /^[^@]+@[^@]+$/i.test(val),
		dotsAfterAt = ((val.split('@').length > 1) && (val.split('@')[1].indexOf('.') > -1)),
		topDomainOk = /^[a-z]{2,}$/i.test(val.substr(val.lastIndexOf('.') + 1));
	$(element).val(val);
	return this.optional(element) || (allowedChars && atPosOk && dotsAfterAt && topDomainOk);
}, "Invalid email.");

//poziv

function openModal(name) {
	var data = { 'bn': $('#bn').val() };
	$('#modal').html(Handlebars.templates[modalTemplates[name]](data));
	$('#modal').modal();
	$('#modal').find('form[name="' + name + '"]').validate({//forma.validate()
		rules: ValidationRules[name],//postavi pravila za formu
		messages: ValidationMessages[name],//postavi poruke za formu
		onkeyup: false,//na dogadjaje
		onfocusout: function (element) {//prima element kao param, u definiciji arg validate(argObj)
			$(element).val($.trim($(element).val()));
			$(element).valid();
		}
	});//poziv validate(), vidi se name u argumentu, objektu
}

-----------
//nece onclick
//samo na domready
$(document).ready(function () {
	$('.header').on('click','.hdr-title',function () {
		jumpToPage('home');
	});
});
$(".neki-elem").on("click", ".dete-selektor", fja())//kad dinamicki menjas dom da radi onclick jer propagira na gore
-------	
//validacija nije radila, ostale pormenljive find(element_prom) umesto selektora, po fjama i pravilima
//nije proverio match != null u regex
emailCustom:true ili false, ukljuceno ili iskljuceno, nema poredjenja
message se pise emailCustom:"neka poruka";
regex escape pattern: "[A-Za-z0-9,\.'-\\s]+"//samo za s duplo\\

//
$('textarea').on('keyup', function () {
    $(this).capitalize();//this je js node pa ga wrappujes u jquery
}).capitalize();//poziv onload
-----------------------
$.fn.capitalize = function () {
	//this je base element na koji se poziva, dakle <textarea>
	//jQuery.each( object ili  array, callback )
    $.each(this, function () {
        var split = this.value.split(' ');
        for (var i = 0, len = split.length; i < len; i++) {
            split[i] = split[i].charAt(0).toUpperCase() + split[i].slice(1);
        }
        this.value = split.join(' ');
    });
    return this;
};

$('textarea').capitalize();//prosledjuje this
--------------
za focus event element mora da ima tabindex atribut
-------------
depends se odnosi na rule, ako je direktno onda je rule
rule:
	depends:
--------------



























