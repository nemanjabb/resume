
/**
* Notes from jQuery docs. Autumn 2014.
*
*
*
*References:
*	http://learn.jquery.com/
*
*/



---------------------
//jquery
--------------
//The reason this fails is that the code executes myCallBack( param1, param2 ) immediately and then passes myCallBack()'s return value as the second parameter to $.get().
$.get( "myhtmlpage.html", myCallBack( param1, param2 ) );//u get() se prosledjuje return value od myCallBack
//ma kastuje tip samo u fju//get() prima fju kao 2. arg
$.get( "myhtmlpage.html", function() {
	myCallBack( param1, param2 );//prosledjuje fju a ne njenu povr vr
});
--------------
// jQuery object methods
$("selektor").fja();//$.fn namespace
					//vracaju this za ulancavanje, tj jquery obj selektovanog cvora
//utility fje
$.fja() //$ namespace//ne primaju deafault arg i vracaju sve i svasta a ne this nuzno
--------------
var $j = jQuery.noConflict();//drugi alias umesto $//$() spada na prototip tj getElementById()
--------------
//$ konflikti
//prenesen
jQuery( document ).ready(function( $ ) {
// You can use the locally-scoped $ in here as an alias to jQuery.
$( "div" ).hide();
});
//IIFE
(function( $ ) {
// Your jQuery code here, using the $
})( jQuery );
-----------------
$( "ul li" ).filter( ".current" );//iz vracenog podskup
$( "ul" ).find( ".current" );//decu
-----------------
$( "form :checked" );// :type ili value// first, last
-----------------
$( "#content" )
	.find( "h3" )
	.eq( 2 )	//eq index
		.html( "new text for the third h3!" )
		.end() // Restores the selection to all h3s in #content
	.eq( 0 )
		.html( "new text for the first h3!" );//prosledjuje ceo niz sve vreme
------------------
$( "<p>This is a new paragraph</p>" );//create element from string
------------------
css klasa roditelja se propagira na svu decu
event se propagira do body, zato $('parent').on('click', 'dete selektor', function() {})
u jquery rade svi css selektori
------------------
var domObjekat = $( "h1" ).get( 0 );//get() kastuje iz jquery obj u native dom objekat
------------------
var logo1 = $( "#logo" );
var logo2 = $( "#logo" );//svaki put novi jquery objekat//sa get() imaju isti dom ===
------------------
static = variable or method associated to a "type" but not to an instance
------------------
$('div') se ne updateuje ako dinamicki dodas nove divove decu
------------------
$( "div.grandparent" ).children( "div" );//neposrednu decu, dubina 1
$( "div.grandparent" ).find( "div" );//rekurzivno svu decu
--------------------
next(); prev(); siblings() //siblings
--------------------
//ubacis property u jquery obj 
$( "#myDiv" ).data( "keyName", { foo: "bar" } );
//ocitas property 
$( "#myDiv" ).data( "keyName" );
----------------------
//ulancane fje se izvrsavaju odpozadi
-----------------------
//iteratori
$('selektor').map() vraca novo polje//za selektovane cvorove
$('selektor').each() vraca referencu
-----------------------
$.each() //Iterates over arrays and objects. OBJECTS//za polja i objekte
-----------------------
$.inArray( elem, myArray ) !== -1//index ili -1
-----------------------
//spajanje objekata
var newObject = $.extend( {}, firstObject, secondObject );//{} optional, onda u prvi
------------------------
var myProxyFunction = $.proxy( myFunction, myObjectThis );//bind()
------------------------
//array //samo za arr i obj ne za jquery kolekcije - selektore
$.each( arr, function( index, value ){//index, value su ti prom iz scopea funkcije, ne prosledjuju se fji, nisu globalni
	sum += value;					  //samo ti kaze da postoje te lokalne u fji. each() je to prosledio fji iz arr	
});
//objekat
$.each( obj, function( key, value ) {
	sum += value;
});
-------------------------
$( "li" ).each( function( index, element ){
	console.log( $( this ).text() );//this je DOM, kastuje ga
});
------------------
this - objekat u kome je definisana fja i na kome se poziva
$.ajax({
	success: function( data ) {
		console.log(this);//ajax optionsArg objekat
	}
});
-----------------
//$( "li" ).map()
//kad hoces da vratis novo polje a ne da menjas postojece onda ide $( "li" ).map()
$( "li" ).map() actually returns a jQuery-wrapped collection, even if we return strings out of the callback
.join() spoji polje stringova u 1 string
----------------
$.map() works on plain JavaScript arrays while .map() works on jQuery element collections.//kad koja
$.map() switches the order of callback arguments
$.map() - utility fja, .map() - object fja
---------------------
delegat je DEFINICIJA
ako ocekuje delegat tj anonimnu fju ocekuje DEFINICIJU fje, a ne promenljivu tj povratnu vr poziva fje
---------------------
---------------------
//eventi
------------
//kad se postavlja event na node, node mora d apostoji u tom trenutku, ne posle
---------------
event arg, propertiji 
pageX, pageY, type, which, data (opcioni arg), target, namespace, timeStamp, preventDefault(), stopPropagation()
target hvata tacno dete element (DOM cvor) koji je kliknut, a event na kontejner
--------------------
$( "input[id]" ).on( "focus mouseover keydown", {data: "data"}, function(event){});
.on(), .one(), .off(), .toggle() prima 2 ili vise fja
--------------------
skratchpad mora da restartujes posle loada strane ili ne radi
--------------------
bind() on() isto, click() je mapiran na on()
--------------------
//postavlja svim buttonima na bodyju
$( "body" ).on( "click", "button", function( event ) {
	alert( "Hello." );
});
//Event delegation works because of the notion of event bubbling
//svaki event obavestava parent cvorove do window
$( "samo parent ima hendler fju" ).on( "click", "deca", function( event ) {
//1 hendler na parenta pa deca delegiraju njemu, umesto na svu decu

We can also prevent the event from bubbling up the DOM tree using .stopPropagation() so that parent elements 
arent notified of its occurrence (in the case that event delegation is being used)//dete da ne obavestava roditelja na kome je postavljen on()
$('body delegate target').on('click',"p current target", function (event) {//p current target, body delegate target u event objekat
  console.log(event);
});
//posto svaki klik event nosi property iz kog cvora je nastao, postavis 1 on na roditelja i decu targete, umesto n na decu

//event.stopPropagation
//ako klik na nekom listu bez stopPropagation() svaki roditelj koji ima klik event ce da se izvrsi
//sa stopPropagation samo klik na listu se izvrsava
http://www.w3schools.com/jquery/tryit.asp?filename=tryjquery_event_stoppropagation
---------
capture phase na dole, bubble phase na gore, target kliknuti
// Dispatch the event.//poslati event//trigger
elem.dispatchEvent(event);
------------------
ako treba oba .preventDefault() and .stopPropagation() simultaneously, you can instead return false
------------------
// Values that evaluate to false:
false
"" // An empty string.
NaN // JavaScript's "not-a-number" variable.
null
undefined // Be careful -- undefined can be redefined!
0 // The number zero.
----------
// Everything else evaluates to true, some examples:
"0"
"any string"
[] // An empty array.
{} // An empty object.
1 // Any non-zero number.
-----------------------
// Set foo to 1 if bar is true; otherwise, set foo to 0:
var foo = bar ? 1 : 0;
-----------------------
//plugins
(function ( $ ) { //IIFE da ne zagadjuje global
$.fn.greenify = function() {	//$.fn jquery funkcije prototip
	this.css( "color", "green" );
	return this;		//za chain
}}( jQuery ));
---------
1. u fn prototip $.fn.greenify = function() {, nad svim instancama
2. return this chaining
3. pass jQuery da $ unutra ne zavisi od glob(function ( $ ) {}( jQuery ));
4. sto manje fja u fn
5. $().each() vraca this
6. options objekat za args
//advanced
7. javni propertyji i fje
	$.fn.hilight.defaults = {//prop
		foreground: "red"
	};
	$.fn.hilight.format = function( txt ) {//fja
		return "<strong>" + txt + "</strong>";
	};	
8. privatne fje unutra lokalne clojure
9. Callback Capabilities onImageShow : function() {}//prazna
	
	
	
------------------
$("#id").get(0); vraca DOM node od jqueryja
-------------
//find() na gore ka parentima
.parents("table")
ili .closest("table") (ako nemas ugnezdene tabele)
-------------------
$(".ui-dialog-content:visible"); za vidljivi jquery ui dijalog bug
-------------
jquery $ je property windowa
parent.$('body').trigger( 'eventName' );
---------------
lista lisp, python je lancana lista
-----------------
{nesto:nesto} dictionary, a {nesto, nesto} set, nema : parove





















