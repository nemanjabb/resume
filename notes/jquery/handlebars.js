

/**
* Notes from Handlebars.js docs. Autumn 2014.
*
*
*
*References:
*	http://handlebarsjs.com/
*
*/



contekst - ulazni podaci tj json ili polje. to ti je i this u helper fji
this je kontekst tj json polje koje ubacujes
helper fju zoves u templateu da izvadis iz jsona sta hoces
helper fja kad hoces neki properti iz jsona da izvadis. detaljnije vadjenje iz jsona
kastujes json u jquery $(json)
---------
za debug 

JSON.stringify(obj);
myObject.toSource();//ispise i fje
--------

{{ <tag> }} prevodi < &lt; tag &gt;
{{{ <tag> }}} dozvoljava izvrsenje html taga

#unless je ifnot
#if prom, prom ora da bude bool u json

autor.name ides sa {{author/name}}
---------

{{{agree_button}}} agree_button() helper fja, sa this, koja stampa tagove, this je json tj kontext ili njegov deo ako je u #each

options.fn(items[i]) stampa na {{firstName}} {{lastName}}

--------
options.toSource()

({hash:{},
inverse:function noop() { "use strict"; return ""; }, 
fn:(function (context, options) 
{ "use strict";
 options = options || {};// ako nije postojao onda prazno polje init
 return fn(context, options.data || data); 
}), 
data:{}})

items je json

--------
 [ je polje u jsonu koje mozes da vrtis sa each

comments: [{ - {{#each comments}}
--------

// The Handlebars.compile function returns a function to theTemplate variable
var theTemplate = Handlebars.compile (theTemplateScript);//vraca funkciju
$(document.body).append (theTemplate (json));

Also, if the data object passed to the each helper is not an array, 
the entire object is the current context and we use the this keyword.

this ne moze da bude polje, samo objekat

{{naitiveTo.0.country}} // kroz polje
---------
helper

We register the custom helper before all the rest of the Handlebars JS code

Custom Function Helpers - proizvoljan broj ul param

Custom Block Helpers - automatically adds an options object as the last parameter in the callback 
options object has an fn method, a hash object, and an inverse method. 
options.fn (dataObject[i]) kastuje iz objekta u string
this je ceo json, ili prenesi deo
--------

../ se odnosi jedan roditelj properti (cvor) iznad u jsonu
--------

Handlebars.registerHelper('ifSeatStatus', function (code, options) {
	if ((code == 'HK') || (code == 'PK') || (code == 'BK')) {
		return options.fn(this);
	}
	return options.inverse(this);
});

prakticno definises funkciju ifSeatStatus(code, options)

-------

	'zip': {
		required: {
			depends: function (element) {
				var value = $(element).parents('form').find(el_country).val();
				//$(element).data('country', value);
				return (value == 'US' || value == 'CA');
			}
		},
		maxlength: 10,
		zipAllowed: true
	}

i funkciju mozes u json da stavis?

It's a valid OBJECT, but not valid JSON

--------

You can just wrap it like this:

var jQueryItem = $(item);

where item is a DOM element. In fact, you'll find yourself doing this a lot in callback functions, since usually this refers to a DOM element and you'll usually want to operate on that using jQuery API calls

cast u jquery

-----------

Conditional operator

The conditional operator is the only JavaScript operator that takes three operands. The operator can have one of two values based on a condition. The syntax is:

condition ? val1 : val2

If condition is true, the operator has the value of val1. Otherwise it has the value of val2. You can use the conditional operator anywhere you would use a standard operator.

For example,

var status = (age >= 18) ? "adult" : "minor";

----------

var adjustCase = function () {
	var val = $.trim($(this).val()).replace(/\b(\w)/ig, function (a, b) { return b.toUpperCase(); });
	$(this).val(val);
};

preg_replace(regular, cime)
a je CAPTURE za (\w)

gi global, case-insensitive
Perform a global, case-insensitive replacement:
var str = "Mr Blue has a blue house and a blue car";
var res = str.replace(/blue/gi, "red"); 

The g modifier is used to perform a global match (find all matches rather than stopping after the first match

-----------

The following example will set newString to "abc - 12345 - #$*%":

function replacer(match, p1, p2, p3, offset, string){
  // p1 is nondigits, p2 digits, and p3 non-alphanumerics
  return [p1, p2, p3].join(' - ');
}
newString = "abc12345#$*%".replace(/([^\d]*)(\d*)([^\w]*)/, replacer);

The \w metacharacter is used to find a word character.

A word character is a character from a-z, A-Z, 0-9, including the _ (underscore) character.

----------

var ime = nesto, ime2 = nesto, ... da ne pise var var var

----------
// 18 9

#with menja kontext, kao if, nije na parent.user nego na user, i each menja kontekst
---------

Handlebars.registerHelper('priceSum', function (pass) {
	var sum = 0;
	for (var i = 0; i < pass.length; i++)
		sum += pass[i].total;
	return sum.toFixed(2);
});

{{priceSum passengers}} tj {{imeFje argumentPodCvorJsonaIdenticnoIme}} passengers i pass//KAO ARG PROSLEDJ CEO JSON ILI NJEG PODCVOR

-----------

you would use options.fn to return when the expression in the callback evaluates to a truthy value. But you would use options.inverse when the expression evaluates to falsy (to render content in the else part of the block).

----------

options.fn()

helper fja #helperFja je ujedno i uslov i ima else blok (moze biti izostavljen) u templejtu.
tarabu ima jer je i uslov #helper, inace nema
u true bloku u templejtu stampa options.fn(), ukljucuje taj blok u stvari
u else bloku stampa options.inverse()

helper NIJE uvek bool fja, nekad vraca string

--------
 #with se krece kroz json, tj story.intro// samo skope menja

  {{#with story}}
    <div>{{{intro}}}</div>
  {{/with}}
---------
# TARABA JE BLOK, znaci vise linija naredbi ugnjezdenih
-------
funkciju dodelis u promenljivu pa pozoves promenljiva(), stalno to
-------
http://jsfiddle.net/g27whzec/ sablon za hendlbars cackanje
http://jsfiddle.net/u5gcqr0q/
-------
{{comments.0.author.firstName}} polje i objekti
----------
JSON IMA SVUDA "", JS OBJEKAT SAMO NA STRINGOVIMA
---------
HELPER NEMA OPTIONS PARAM, BLOK HELPER IMA OPTIONS DODATNI PARAM
options ima samo metode fn i inverse kao poziv template()
---------
//Block expresions:
//tacno to, helper sluzi samo da ne prosledis ceo json nego samo podcvor koji ti treba
//Block expressions allow you to define helpers that will invoke a section of your template with a different context than the current.
//jedino sto nisi prosledio ceo json nego cvor koji ti treba people arg, {{#list people}}
{
  people: [
    {firstName: "Yehuda", lastName: "Katz"},
    {firstName: "Carl", lastName: "Lerche"},
    {firstName: "Alan", lastName: "Johnson"}
  ]
}

{{#list people}}{{firstName}} {{lastName}}{{/list}} //prosledjujes people cvor tj polje kao prvi arg
						    // imas 2 polja firstName lastName koja ce options.fn() u helperu da popuni kao template()

//The helper receives the people as its first parameter, and an options hash as its second parameter.//hash misli js objekat tj hashtabela
//isto ugradjen default list helper implementacija
Handlebars.registerHelper('list', function(items, options) { //items je people cvor prosledj kao arg
  var out = "<ul>";

  for(var i=0, l=items.length; i<l; i++) {
    out = out + "<li>" + options.fn(items[i]) + "</li>";
  }

  return out + "</ul>";
});

The options hash contains a property named fn, which you can invoke with a context just as you would invoke a normal Handlebars template

//contains a property named fn, which you can invoke - dakle funkcija
//which you can invoke with a context just as you would invoke a normal Handlebars template - znaci kao poziv template()
-----
default helper koji se ponasa kao template(), zameni gde treba samo

Handlebars.registerHelper('noop', function(options) {
  return options.fn(this);
});

-----------
default #each helper kako je implementiran

Handlebars.registerHelper('each', function(context, options) {
  var ret = "";

  for(var i=0, j=context.length; i<j; i++) {
    ret = ret + options.fn(context[i]);
  }

  return ret;
});
------------

{{#if isActive}}
  <img src="star.gif" alt="Active">
{{else}}
  <img src="cry.gif" alt="Inactive">
{{/if}}

//default #if helper implementiran

Handlebars.registerHelper('if', function(conditional, options) {
  if(conditional) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

-------
<p>{{./name}} or {{this/name}} or {{this.name}}</p>
//da bi razlikovao name helper od name prpoerty u json
//Any of the above would cause the name field on the current context to be used rather than a helper of the same name. 

-----
kompajlira u js a ne html jer kompajlira samo jednom (i to moze na serveru) i izvrsenje tog js je manje zahtevno od kompajliranja
----------
u obicnom helperu this je ceo context json ili manje za #each itd
----------
addMethod validator vraca bool a ne register helper, zamenio
-----------

options.hash je da prosledis parametre kao objekat

{{#myNewHelper score=30 firstName="Jhonny" lastName="Marco"}}
Show your HTML content here.
{{/myNewHelper}}

console.log(JSON.stringify (options.hash)); 
//The output is: {score:30, firstName:"Jhonny", lastName:"Marco"}

--------------
u helper moze koliko god args, samo ih poredjas
--------------
{{fullName author}} author se gadja sa   {author: {firstName: "Alan", lastName: "Johnson"},...}
u helperu je to prvi arg Handlebars.registerHelper('fullName', function(person) {
----------------
this je kontekst tj podrazumevani cvor koji je prosledjen
----------------















