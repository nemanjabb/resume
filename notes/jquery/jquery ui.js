
/**
* jQueryUI working notes. Autumn 2014.
*
*
*
*/


//19 9
 postavljas i css za taj node na koji pozivas fju
 
$(function() {
	$( "#selectable" ).selectable();
});

 #selectable .ui-selected { background: #F39814; color: white; }
 
----------
$.each() menja scope na item
----------
$.ajax({
	url: "http://gd.geobytes.com/AutoCompleteCity",
	dataType: "jsonp",
	data: {
		q: request.term
	},
	success: function( data ) {
		response( data );
	}
});
//eto kako parametri fje u objektu, unapred odredjeni spoljnom fjom, tako je za sve objekte
//closure lokalna definicija funkcije
//u ugnjezdenoj funkciji se vidi sve iz spoljnih - to je closure, global scope je drugo
//parametri fje se gadjaju samo po redosledu, ne po imenu options itd
// scope se kreira sa new

// less css samo drugcije organizovan kod, za laksu logiku, svi se prevode u css

//lokalne fje $.ajax() i success delegat su lokalne fje - clousure, vide spoljne promenljive
$( "#city" ).autocomplete({
	source: function( request, response ) {
		$.ajax({
			url: "http://gd.geobytes.com/AutoCompleteCity",
			dataType: "jsonp",
			data: {
				q: request.term
			},
			success: function( data ) {
							response( data );
						}
					});
	},...
}
//invoke = poziv

function foo(x) {
  var tmp = 3;
  return function (y) {
    alert(x + y + (++tmp)); // will also alert 16
  }
}
var bar = foo(2); // bar is now a closure.//vratio unutrasnju funkciju a ne vrednost// pa dobro to je izgleda big dil
bar(10);

//vraca fju a ne vrednost i vide se promenljive iz spoljnih fja, to je sve

----------------------
  
//         ____________Immediately executed (self invoked)___________________
//         |                                                                |
//         |      Scope Retained for use        __Returned as the______     |
//         |     only by returned function     |  value of func       |     |
//         |             |            |        |                      |     |
//         v             v            v        v                      v     v
var func = (function() { var a = 'val'; return function() { alert(a); }; })();  

//All variables outside the returned function are available to the returned function, 
//but are not directly available to the returned function object.... eto, ok

func();  // alerts 'val';  
func.a;  // undefined  


