# shell function positional arguments
# https://stackoverflow.com/questions/6212219/passing-parameters-to-a-bash-function

# def
function function_name {
   command...
} 

# or

function_name () {
   command...
} 

# poziv
function_name "$arg1" "$arg2"

function_name () {
   echo "Parameter #1 is $1"
}

function_name 2
# Parameter #1 is 2

# $0 is the name of the script (file) itself
-------------------
# yarn -- , $1=--
"with:env": "fn() { echo \"1=$1 2=$2 3=$3\";}; fn --",
"ae": "yarn with:env 'yarn test:e2e' '.env.test.local'"
1=-- 2=yarn test:e2e 3=.env.test.local
----
### Reusable yarn script - function
# original
"test:e2e:env:original": "dotenv -e .env.test.local -- sh -c 'yarn test:e2e'",
# print args
"with:env:debug": "fn() { echo \"1=$1 2=$2 3=$3\";}; fn --",
# fn
"with:env": "fn() { npx dotenv -e \"$3\" -- bash -c \"$2\";}; fn --",
# call
"test:e2e:env": "yarn with:env 'yarn test:e2e' '.env.test.local'"
#
# shorter form without yarn and ''
"with:env": "fn() { npx dotenv -e \"$3\" -- bash -c \"yarn $2\";}; fn --",
"test:e2e:env": "yarn with:env test:e2e .env.test.local"
# zaknjucak promenljive/komande su stringovi, samo concat
----------------------
# set e+x
# https://stackoverflow.com/questions/29141436/set-e-and-set-x-in-shell-script
set -x # Print shell command before execute it - debug
set -e # exit on error
------------------------
# .profile vs .bash_profile vs .bashrc
# https://serverfault.com/questions/261802/what-are-the-functional-differences-between-profile-bash-profile-and-bashrc
# .profile - all shells, ENV vars here
.profile is for things that are not specifically related to Bash, 
like environment variables PATH and friends, and should be available anytime.
---
# .bashrc - bash
.bashrc is for the configuring the interactive Bash usage, like Bash aliases, 
setting your favorite editor, setting the Bash prompt, etc.
---
# .bash_profile - load .profile in bash
.bash_profile is for making sure that both the things in .profile and .bashrc are 
loaded for login shells.
---------------------
# shell var vs environment var
shell var je global var u tom shellu
env var je exported shell var, sa export MY_VAR
----
# UID and GID env vars for Docker volumes permissions
export UID=$(id -u)
export GID=$(id -g)
```

```bash
# shell variable
echo $UID
1000
---
# environment variable
printenv | grep UID  # no output
env | grep UID # same
--------------------
# print default shell
echo $SHELL
-----------------------
# export env var if not already exported

#!/bin/bash

if ! env | grep -q ^UID=
then
  echo env variable is NOT exported
else
  echo env variable is already exported
fi
-----------------
https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker/blob/master/scripts/lint.sh
# exit imidiatelly, prekini na prvi gresku, da ne izvrsava komande ispod, non 0 return
set -e
---
# trace mode, stampa komandu koja se izvrsava, debug
set -x 
----------------------


