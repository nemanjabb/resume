// generate and copy ssh keys to new machine, proxmox lxc npr 
// generate rsa key
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
// copy key from my path, kopira gde treba
ssh-copy-id -i ~/.ssh/homelab/lxc1_id_rsa username@192.168.1.120
-----
// add to config file in /home/username/.ssh/config 
# ssh lxc1 proxmox, mora lxc11
Host lxc11 192.168.1.120
    HostName 192.168.1.120
    IdentityFile ~/.ssh/homelab/lxc1_id_rsa
    User username
-----
// sad moze 
ssh lxc1
----------------
// isprazni fajl
truncate -s 0 authorized_keys
--------------
ssh lxc1 // connection refused 
ide na bezveze dns za lxc1, u ceskoj, mora drugo ime 
--------------
// copy file and preserve permissions (metadata)
sudo cp -rp ~/homelab/traefik-proxy ~/traefik-proxy
------------
// VS CODE SSH // ne zaboravi nikad, ni za remote
zapravo uvek kreiraj ssh host i onda moze edit (normalan rad) za vs code 
1. create and add ssh key 
2. add host and key in /home/username/.ssh/config 
3. to je to, dns moze da bude pogresan, zauzet lxc1, proveri sa ping lxc1 
-----------------------
// fstab, data partition mount on boot
// nadji ime particije po velicini
lsblk 
// nadji uuid of device
sudo blkid 
// backup
sudo cp /etc/fstab /etc/fstab.bak 
// dodaj za data particiju
sudo nano /etc/fstab
UUID=4624a187-3b3a-461f-a1c7-499de423b801  /media/username/data  ext4  defaults  0  2
// proveri uspeh
// mount all from fstab
sudo mount -a
// check if mounted
mount | grep /media/username/data
// check sizes
df -h
----------------------
// create folder shortcut
ln -s "/media/username/data/torrenti 2024/The Software Designer Mindset" "$HOME/Desktop/The Software Designer Mindset"
// ~/ ne moze za home u "..." kad ima space u stazi
---------------------------
// mount webdav, does NOT work
sudo apt update
sudo apt install davfs2
---
mkdir ~/Desktop/webdav-pro128gb
---
// ne radi, dovlaci ceo folder // ne moze ovo
sudo mount -t davfs -o username=username1,uid=1000,gid=1000 http://192.168.1.1/dav/card/sda1/ ~/Desktop/webdav-pro128gb/
---
sudo umount ~/Desktop/webdav-pro128gb/
--------------------
// expose local dev webserver over ssh tunnel - RADI, cool
ssh -R 1080:localhost:3000 ubuntu@152.70.160.21
curl http://152.70.160.21:1080
-------
// jos krace, izvuci ce ip iz ssh config
ssh -R 1080:localhost:3000 amd2
curl http://amd2.nemanjamitic.com:1080
------
// poenta:
1. remote ssh mora da binduje na 0.0.0.0, a ne na 127.0.0.1 
sudo nano /etc/ssh/sshd_config -> GatewayPorts yes
2. flush iptables after open port in oracle // glavna fora
sudo iptables -F
// list iptables
sudo iptables -L
------
// list listening ports
ss -tuln | grep 1080
// ovo treba da vidis 0.0.0.0:1080 na oba
tcp   LISTEN 0      128          0.0.0.0:1080      0.0.0.0:*    
----------
// mora ssh option u ssh config da bi slusao na 0.0.0.0, a ne na 127.0.0.1
remote port forwarding is set up to listen only on the loopback interface_ (127.0.0.1 and ::1) of the remote server
which means its not accessible from other machines
----------
sudo nano /etc/ssh/sshd_config
// ne samo uncomment, nego change to yes
GatewayPorts yes
// check if applied, ovo je samo regex iz fajla
sudo grep -i gatewayports /etc/ssh/sshd_config
----
// restart ssh service
sudo systemctl restart ssh
i logout iz ssh sessije
-----
// ovo je kljucno bilo da se otvori 1080 port posle open in oracle network subnet
// flush iptables rules
sudo iptables -F
-------------------------
// ostalo oko ip ping
// hops do adrese
sudo apt install traceroute
traceroute -n 152.70.160.21
-------
// ping fails because of ICMP on Oracle nesto...?
ICMP - Internet Control Message Protocol
---------
// ping on port
telnet 152.70.160.21 1080
nc -vz 152.70.160.21 1080
// linux only
ping -p 1080 152.70.160.21
--------------------
// RDP over an SSH connection using port forwarding 
// zapravo ovo je samo enkriptovanje rdp-a ssh-om, ono prvo ide i za rdp, samo drugi port // to
// 1. uspostavi ssh konekciju
ssh username@remote_host
// 2. run this on LOCAL machine in parallel, 2. ssh konekcija // zapazi
ssh -L 13389:localhost:3389 username@remote_host
local port forwarding, binduje 13389 na lokalnoj masini na 3389 na remote masini (gde je rdp server)
127.0.0.1:13389 -> remote_host:3389
3389 - default_ rdp port
13389 - moj port
// 3. connect rdp client na localhost
localhost:13389
zapazi localhost je adresa, jer je forwardovano na remote 
na serveru je default_ rdp port, lokalno je custom
---------------
// other cool SSH tricks, ask chatGpt for more tricks in general
// SSH ProxyJump
ssh -J intermediate_host username@final_host
// copy ssh key
ssh-keygen  
ssh-copy-id username@remote_host  
// Remote Port Monitoring
ssh -L 8080:localhost:80 username@remote_host
// SSHFS (SSH File System): Mount remote directories securely over SSH
sshfs username@remote_host:/remote/directory /local/mount/point
--------------------
// open another window gnome terminal from vs code terminal in same dir
gnome-terminal &
-----------------------
// ssh tunnel dev server with traefik https
https://github.com/linuxserver/docker-mods/tree/openssh-server-ssh-tunnel // mod script
https://github.com/linuxserver/docker-openssh-server/issues/22
// MUST have *: before, important
ssh -R *:1082:localhost:3000 amd1c
https://preview.amd1.nemanjamitic.com
1082 is tunnel only port, doesnt need to be opened on host 
----
ssh 1081 -> 2222 // 22 ne moze container da expose na host, alredy in use
http 1082 for traefik
localhost:3000 -> tunnel:1082 -> traefik
----------------------
// current time with timezone
TZ='Europe/Belgrade' date
уто, 30. јул 2024.  16:25:42 CEST
// when OS is started in current timezone
TZ='Europe/Belgrade' who -b
system boot  2024-07-30 07:46
--------------------------
// ubuntu trash icon
// put on desktop
gsettings set org.gnome.shell.extensions.ding show-trash true
// remove from dock 
gsettings set org.gnome.shell.extensions.dash-to-dock show-trash false
------------
// install sound recorder
sudo apt install gnome-sound-recorder
------------------
// ovaj zapravo generise ssh kljuc na lokaciju, stavi svoj email ovde
ssh-keygen -t ed25519 -C "your_email@example.com" -f ~/.ssh/REMOTE_KEY_ED25519
// copy key to server
// key is associated ONLY with that single user // important
// vise puta kopira svakom useru u njegov home folder
nano ~/.ssh/authorized_keys
ssh-copy-id -i ~/.ssh/REMOTE_KEY_ED25519.pub user@remote_server
ssh-copy-id -i ~/.ssh/homelab/orange_pi_ed25519.pub orangepi@192.168.1.127
// login with new key
ssh -i ~/.ssh/REMOTE_KEY_ED25519 user@remote_server
-------------------
// reorder grub
// https://www.youtube.com/watch?v=sngl6Izz_CE
// GRUB boot menu customizer | Grub boot menu not showing | Change Boot order Timeout Default OS - Brainers Technology
GLAVNA FORA je da grub customizer mora u ZADNJI instalirani ubuntu // zapazi, zato nije htelo
zadnji OS kontrolise grub
-----
sudo add-apt-repository ppa:danielrichter2007/grub-customizer
sudo apt update
sudo apt install grub-customizer
sudo grub-customizer
----------------
// linux terminal shortcuts, slika twitter, korisni za svaki dan
ctrl + a // beginning of a line // taj
ctrl + e // end of a line // taj
ctrl + -> // one word right
ctrl + <- // one word left
ctrl + u // cut // taj
ctrl + f // one char right, isto ko ->, beskorisno
ctrl + b // isto char, left
ctrl + L // clear terminal, odlican // taj
-----------------------
// get all IPs on lan, nmap
sudo apt install nmap
sudo nmap -sn 192.168.1.0/24
----------
// zip local folder
sudo for files with all permisions 
sudo zip -r traefik-proxy-pi.zip traefik-proxy
// chown from root
sudo chown username:username traefik-proxy-pi.zip
// download remote file
rsync -avz -e ssh pi:~/traefik-proxy-pi.zip ~/Desktop/traefik-proxy-pi.zip 
--------------------
// default sound recorder koji meni treba, eto
// https://wiki.gnome.org/Apps/SoundRecorder
sudo apt install gnome-sound-recorder
------------------
// benchmark ubuntu
sudo apt install sysbench
// CPU test
sysbench cpu --threads=4 run
// Memory test
sysbench memory run
// Disk I/O test
sysbench fileio --file-total-size=2G --file-test-mode=rndrw prepare
sysbench fileio --file-total-size=2G --file-test-mode=rndrw run
-------------------
// create username user and group
sudo adduser username
// add to sudo group
sudo usermod -aG sudo username
---------------
// battery wear
upower -i $(upower -e | grep 'BAT')