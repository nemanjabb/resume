// print time in wsl2, ubuntu
date "+%H:%M:%S   %d/%m/%y"
----------
// copy-overwrite git repo without node-modules and .git

// nextjs // -n test (dry run) option
rsync -av --progress acme-nextjs/ temp-acme-nextjs --exclude='node_modules' --exclude='.git'

//2
rsync -av --progress temp-acme-nextjs/ acme-nextjs

// storybook
rsync -av --progress acme-storybook/ temp-acme-storybook --exclude='node_modules' --exclude='.git'

//2
rsync -av --progress temp-acme-storybook/ acme-storybook

--------------
// delete folder in wsl2, radi defined
rm -rf node_modules
--------------
//windows
//find PID of the process holding a port
netstat -ano | findstr :44342

//kill process by PID
taskkill /PID 12896 /F

//linux
//find PID of the process holding a port
lsof -i :3002

//kill process that holds a port
sudo kill -9 $(sudo lsof -t -i:3002) //it will restart vs code in wsl2, then must run nvm use v14.12.0

// list listening ports
sudo netstat -tulpn

// list firewall rules 
sudo ufw status
----------------------
// run multiple commands
cd myfolder; ls //always run second
cd myfolder && ls // only if first succeeds
cd myfolder || ls // only if first fails
... & // run in background 
-----------------------------------
-----------------------------------
// Techworld With Nana - DevOps Bootcamp
// Operating Systems & Linux Basics

// introduction into os
linux je unix-like ali nema source unixa
macos je unix source
posix Portable Operating System Interface - standardizovan api za os, zato su os-ovi i hardware kompatibilni

// virtualization
hypervisor
type 2 instalira se na host os, obican vbox npr, za desktop
type 1 instalira se direktno na hardware, npr vmware esxi, ms hyper v, za servere
virtuelizovan hw, moze svakoj masini da dodeli cpu koliko hoce

abstraction, hw and os tightly coupled
os je portable file, image
snapshot - backup os + apps

// file system

/root je home dir za root user, ostali useri su u /home/username

/bin - izvrsni programi cat, cp... komande u konzoli, komanda u konzoli je kompajl fajl programa
/sbin - system bins need sudo, admin programs
/lib - libraries for bins, razdvajano i linkovano bin - exe,  lib - dll, lib
/usr - istorijski /bin i /lib zbog malo mesta, konsolne komande izvrsava odavde ls, cp...
/usr/local - program files, sta instaliras tu ide, za sve usere
/opt - 3rd party programs koji nisu split u bin i lib
/boot - za boot, ne diraj
/etc - main configuration location, conf.d ...
/dev - devices, drivers
/var - logs, /var/cache za kes aplikacija
/tmp - temp files
/media - removable media, diskovi, usb
/mnt - temporary mount points

sve ovo se ne pipa rucno, apt radi sta treba
---
hidden files - svi pocinju sa dot, pa dotfiles, i .folderi, checkbox to show them, ko win

// command line

linux-user@computer-name:~$ // ~ home folder
$ - regular user
# - root user


// basic commands

pwd - print working directory
everything is a file - folders, commands, devices
rename - mv existing-file new-name
history // lista svih komandi
ls -a // all, hidden files
uname -a // system and kernel info
sudo // root user for one command
su - username //switch user
shift ctr c, v // copy paste u konzoli

// package manager

ima apt i apt-get // apt preporucen
sudo apt update // update repositories
/etc/apt/sources.list // urls repozitorijuma

alternative ways for installing:
1. ubuntu sw center // snap ispod haube
2. snap // self-contained, apt je split, snap ne sharuje deps nego ponavlja
3. add repository, ppa - personal private archive

based:
1. debian - ubuntu, debian, mint - apt package manager
2. red hat - rhel, centos, fedora - yum package manager


pm je glavna razlika u distroima, ostalo je sitno gui...


// vim

vi i vim // vim malo siri

command mode, insert mode
:wq // sacuvaj i zatvori
:q! // discard i close

command mode
dd // delete line
d10 // delete 10 lines
u // undo
shift A // cursor at end of line
0 // start of line
$ // end of line
12G // goto line 12
/nginx // search, n next, N previous
%s/old/new // replace string


// accounts and groups
// ACL - access control lists, permissions prakticno

users:
1. superuser account - root, 1 samo
2. user account
3. service account // za aplikacije, mysql, apache...

permissions:
1. user
2. group

/etc/passwd // all users on system
username:password:uid:gid:gecos:homedir:shell
user id, group id, gecos - name description, default shell

sudo adduser username // create user
sudo passwd username // change pass
su - username // switch user
su - // login as root

sudo groupadd groupname // create group
/etc/groups // lista svih grupa

adduser, addgroup - interactive sa defaults
useradd, groupadd - low level, moras ids rucno da das kao args // in shell scripts
deluser, delgroup - isto

sudo usermod -g groupname username // change users primary group
sudo usermod -G groupname username // change users secondary group
sudo usermod -aG newgroupname username // kreiraj newgroupname ako ne postoji

man ls // manual pages, dokumentacija za komandu

groups username // listaj sve grupe u koje user pripada

sudo gpasswd -d username groupname // remove user from a group

zbrdozdolisano, nidje veze


// file ownership and permissions


ls -l // da vidis permissions
drxrwxr-x 2 user group 4096 May 7 11:00 folderName
type, permissions, owner, owner group ...

user, group // user that created file, primary group of that user

sudo chown username:groupname file.txt // change ownership
sudo chown username file.txt // samo user
sudo chgrp groupname file.txt // samo group

d rwx rwx r-x : type, owner, group, other
file type: -, d, c, l - regular, directory, char, link

r, w, x, - : read, write, exec, none
rwx // po 3 idu, ima sve znaci

sudo chmod -x file.txt // remove x for all, - minus

chown - change user and group
chmod - change permissions

// symbolic
sudo chmod g-w file.txt // makni write na group, g+x dodaj x na group
u - user, g - group, o - other, a - svi ili izostavis
+ add, - remove

// set permission sa =
sudo chmod g=rwx file.txt // g= nakucas sta ti treba

// numeric
0 ---
1 --x
2 -w-
3 -wx
4 r--
5 r-x
6 rw-
7 rwx

// tezina, sabiras
4 read
2 write
1 exec
0 none

sudo chmod 777 file.txt // prvi 7 owner, drugi 7 group, treci 7 other

ls -la // list za hidden, chown i chmod isto kao za normalne

// folder mora da ima x permission da bi mogao da udjes u njega sa cd, zapazi
// a ne samo read
chmod +x /path/to/dir/

// pipes and redirects

// pipe | // u komandu

svaka komanda ima input i output
input -> command1 -> output -> input -> command2 -> otuput

cat /var/log/syslog | less // less je paginacija, space sl strana, b back, q exit
grep "string sa space" // regex, radi na liniju g flag... 

// redirect > // u fajl

history | grep sudo > sudo-commands.txt // forwarduj text u fajl
> overrides content
>> append content

// streams
stdin (0)
stdout (1)
stderr (2)

// chain without pipe ;
clear; sleep 1; echo "123" // jedna za drugom po redu


// shell scripting 1

// interpreters
sh (bourne shell) - /bin/sh // uvek ima
bash (bourne again shell) - /bin/bash // improoved sh, default for unix, macos, linux

// shebang, koji shell da izvrsi .sh fajl
#!/bin/sh
#!/bin/bash
#!/bin/zsh

// exec script
./file.sh // universal
bash file.sh // za bash
ne mora da se zavrsava na .sh

// shell scripting 2

// variables
file_name=file.txt
echo "bla $file_name bla"
config_files=$(ls config) // command out to string to var

var1=$var1:bla-bla // kad se dodeljuje nema $, kad se cita ima $

// if else

if [ -d "config" ] // ako je folder
then
	...
elif
	...
else
	...
fi

// operators
file operators: -d, -f, -r, -w, -x, -s - not empty
number comparisons: -eq, -lt, -gt, -ge, -ne
string: == bash, = posix
if [ "$var1" == "str1" ]
if [[ ... ]] // bash, prosireno

// args
arg1=$1
arg2=$2 // 1 to 9
$* // all args
$# // number of args


./script.sh arg1 arg2

// user input
read -p "poruka: " my_var1
echo "bla $my_var1"

// for loop
for var1 in arr
	do
		...
	done
	
	
// while loop
while [ ]
	do
		...
		break
	done

$var1 + $var2 // concat strings
$(($var1 + $var2)) // add numbers


// shell scripting 3

// functions

function my_func {
	...
}

my_func // poziv

---

function my_func2() {
	var1=$1 // arg isto kao za fajl, skriptu
	...
}

my_func2 arg // prosledjivanje arg

---
result=$(sum 2 10) // assign

sum 2 10
result=$? // $? sadrzi rez zadnje komande, fje iznad

pocetnicki skroz, osnovna skola


// environment variables

key value, uppercase by convention, system scope

// listaj sve vars
printenv // sve
printenv USER // jednu, grep...
echo $USER // $ kao shell var

// create env var
export ENV_VAR=somevalue // zivi samo u toj instanci konzole

// remove env var
unset ENV_VAR

/home/username/.bashrc // conf bash
export ENV_VAR=somevalue // tamo, user scope
source ~/.bashrc // apply env vars

// system wide vars, all users
/etc/environment // tu su, :q! izadji bez save vim


// networking

switch - lan
router - wan (internet)
gateway - ip adresa routera

subnet - opseg ip adresa
192.168.0.0/16 - 2 ili 3 prva broja fiksna

NAT - facebook vidi ip routera, a ne lan computera, zamenjena ip

firewall - sve je disabled default, pa whitelist
ip, port, inbound dolazni saobracaj
port forwarding - otvaranje porta u firewallu

dns - domain na ip mapping
root -> tld domains

dns client -> isp resolver -> root server
resolver -> tld server
resolver -> nameserver server

computer and isp have dns cache

// commands
ifconfig - lan info
netstat - active connections, listening ports
ps aux - procesi, koje portove drze
nslookup google.com - ip domena, ip nameservera, i obrnuto
ping - 


// ssh

// auth
user/pass
ssh keys - private key, tajni na klijentu, public key na serveru, isto user pass prakticno

firewall - server, port 22, whitelisted IPs

// digital ocean
ssh root@159.89.14.94

// gen keys
ssh-keygen -t rsa // t type, pass opcionalan
id_rsa // private
id_rsa.pub // public
/home/username/.ssh/id_rsa.pub // lokacija default na clientu

// na serveru auth keys lista
/home/authorized_keys // tu kopiras public key
ssh root@159.89.14.94 // sad samo ovo, ne treba pass

ssh -i .ssh/id_rsa root@159.89.14.94 // koji private biras ako imas vise

// copy fajl sa klijenta na server
scp test.sh root@159.89.14.94:/folder // scp secure copy kao cp
scp -i .ssh/id_rsa test.sh root@159.89.14.94:/folder // sa spec priv kljucem

---------------------------
// F:\pluralsight 2020\linux\Getting-Started-with-the-Linux-Command-Line\2-Working-with-Linux-Command-Line-Basics
// 4-Linux-Command-Syntax-Patterns-and-Shortcuts.mp4

man wget // help za komandu wget

-o // - short word option
--option // -- full word option
----------------------
// /**/ staza - glob
// https://stackoverflow.com/questions/28176590/what-do-double-asterisk-wildcards-mean/62985520#62985520
** - bilo koja dubina foldera
* - wildcard u tom folderu
// tailwind docs
Use ** to match zero or more directories
----------------------
----------------------
// manage multiple ssh keys

// default github
// ~/.ssh/config
# Github myusername
Host github.com # alias za HostName
    HostName github.com
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/myusername_github__id_rsa

-----
// multiple github users
// https://stackoverflow.com/questions/67593657/setting-up-multiple-ssh-key-access-to-github
Host username1_github.com
    HostName github.com # odavde vuce domen
    User git
    IdentityFile ~/.ssh/username1_github__id_rsa # private key
    IdentitiesOnly yes

// set git remote:
git remote add origin git@username1_github.com:username1/some-repo.git
// git@hostname:username/repo
// git@github.com:nemanjam/nextjs-prisma-boilerplate.git

-------
// multiple servers
// https://stackoverflow.com/questions/2419566/best-way-to-use-multiple-ssh-private-keys-on-one-client

Host myshortname1 realname.example.com
    HostName realname.example.com
    IdentityFile ~/.ssh/realname_rsa # private key for realname
    User remoteusername # linux user

Host myshortname2 realname2.example.org
    HostName realname2.example.org
    IdentityFile ~/.ssh/realname2_rsa  # different private key for realname2
    User remoteusername

// connect:
ssh myshortname1
ssh myshortname2
--------------------
// velicina diska
lsblk
----
// umesto ifconfig, lan ip
ip -c a
-----
// list open ports
ss -l
-------
// put ip in env variable
// moj wifi interface wlp3s0
$ export LANIP=$(ip -4 addr show wlp3s0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
$ echo $LANIP
--------------
// preview website on mobile phone lan
// shell, works, samo VAR1= ...$VAR2... i to je to
export INTERFACE=wlp3s0 && \
export WAN_IP=$(ip -4 addr show $INTERFACE | grep -oP '(?<=inet\s)\d+(\.\d+){3}') && \
export NEXTAUTH_URL=https://$WAN_IP:3001 && \
echo $NEXTAUTH_URL
---------------
// list linux users
cat /etc/passwd
--------------
research ip 0.0.0.0 vs :: vs 127 ?
-----------------------
// set up ubuntu 20.04 digitalocean
// https://rafrasenberg.com/posts/ubuntu-20.04-basic-server-set-up-beginner-tutorial/
sifra prilikom kreiranja ssh kljuca se trazi posle kad ssh-ujes u masinu, svaki put
// 1
// kreiranje ssh kljuca
ssh-keygen -t rsa
--------
// 2
// UFW firewall - na nivou ubuntua - firewall otvara i zatvara portove, i to je to
pre enabliranja firewalla dodaj ssh da bi mogao da udjes ikad posle
ufw allow OpenSSH
---
ufw enable // enable fw
ufw status // check status
---
ufw allow 443 // enable https
ufw reload // uvek kad dodas pravilo moras reload da apply
--------
// 3
// promeni ssh od 22 na drugi
ufw allow 3467
nano /etc/ssh/sshd_config // ssh config
Port 3467 // ctrl x, pa y u nano
systemctl restart sshd.service // restart ssh service za apply
---
ssh root@167.23.10.37 -p 3467 // login na custom port
-------------
disable password auth u ssh config ako cloud provider vec nije
-------------
i to je to, nista specijalno od tutorijala
------------------------
// ssh copy
// local file to user@computer:/path/to/FOLDER
scp ./core/.env ubuntu@amd1:/home/ubuntu/traefik-proxy/core
---
// copy folder foo to folder bar
scp -r foo your_username@remotehost.edu:/some/remote/directory/bar
---
moze i sa remote na local - download prakticno
-----------------
// obrisi sadrzaj foldera, a sam folder ostavi
rm -r /path/to/dir/* //*/
mora -r inace brise samo fajlove, nebuloza
--------------
// trazi lokaciju fajla
find / -name pg_hba.conf
----------------
// check cgnat
traceroute 77.243.28.65
-----------------------------
// get distro name, works for containers too
lsb_release -a
// npr
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 20.04.4 LTS
Release:        20.04
Codename:       focal
-------------------------
// paste in nano eitor
ctrl + shift + v // normalan paste iz clipboarda hosta, to mi treba
ctrl + u // sto si kopirao iz samog nano, ne treba mi
// save changes, pita te ime fajla, samo enter
ctrl + o // slovo, ne nula, ovo je ctrl + s, sacuvaj i edituj dalje
// exit (and save), pita te pa y or n da li da sacuva promene
ctrl + x // ovo je sacuvaj i zatvori fajl
-------
// reload .bashrc da vidis promenjenu env var
source ~/.bashrc
printenv MY_PUBLIC_SERVER_IP_V4
--------------------------------
// ssh keys permissions
// https://unix.stackexchange.com/questions/257590/ssh-key-permissions-chmod-settings
//
// public
chmod 644 amd2-ssh-key-2022-12-19.key.pub
// private keys, config, known_hosts
chmod 600 amd2-ssh-key-2022-12-19.key
// folder
chmod 700 ~/.ssh
---------
how to copy .ssh folder with preserved permissions?
-------------------------
// sortirani fajlovi po velicini
// du -sh ./node_modules/* | sort -nr | grep '\dM.*'
------------------
// .bashrc - env vars common for all servers
// for postgres volumes
# UID and GID env vars for Docker volumes permissions
export MY_UID=$(id -u)
export MY_GID=$(id -g)

// export server ip into env var
# export server public IP v4
export MY_PUBLIC_SERVER_IP_V4=$(dig @resolver4.opendns.com myip.opendns.com +short)

# server hostname, domain
export SERVER_HOSTNAME=arm1.localhost3002.live
-----------------
// ssh koji kljuc na koji url, za git npr
// definise se u ~/.ssh/config
# all Dokku docker on amd1 key
Host dokku.arm1.localhost3002.live
    HostName dokku.arm1.localhost3002.live
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/oracle/dokku_docker_amd1__id_rsa
    Port 3022
----------------------------
// koja je distribucija
lsb_release -a
----------------------
// check ports in use
sudo lsof -i -P -n | grep LISTEN
sudo netstat -tulpn | grep LISTEN
---------------------------
// shell alias
// .bashrc - non-login shell, ovaj
// .bash_profile - login shell
alias dokku="docker exec -it dokku bash dokku"
// reload shell to apply
source ~/.bashrc
-------------------
// korisno, stampa foldere kao stablo
sudo apt install tree
tree -da .
------------
chmod - menja permisije na fajlu
chown - menja vlasnika
-------------
// nix shell
// https://www.youtube.com/watch?v=0ulldVwZiKA
// https://www.youtube.com/watch?v=eW8KU6h_ZNo
package manager kao apt
functional - pure, no sideeffects. only input and output
declerative - paketi u fajl, nix language
nix-shell - za lokalne pakete, kao node_modules, ili virtualenv
radi preko cache i symlinks
