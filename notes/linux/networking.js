// add here wireguard, tailscale, iptables, etc
---------------------
// docker rusi wifi hotspot
// https://askubuntu.com/questions/699995/hotspot-created-and-connected-from-linux-but-no-internet-access-on-phone
// https://github.com/moby/moby/issues/43719
// add iptables rules
// Allow established connections to be accepted
sudo iptables -I DOCKER-USER -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
// ovde wwan0 ili eth0, zavisi koji interfjes daje internet
// Allow traffic from the hotspot (wlp61s0) to the GSM interface (wwan0)
sudo iptables -I DOCKER-USER -i wlp61s0 -o wwan0 -j ACCEPT
// check if applied
sudo iptables -L DOCKER-USER -v
// restart docker
sudo systemctl restart docker
// make it persistent
sudo iptables-save > /etc/iptables/rules.v4
// mora okolo
sudo mkdir -p /etc/iptables
sudo iptables-save > /tmp/rules.v4
sudo mv /tmp/rules.v4 /etc/iptables/rules.v4
// verify
sudo cat /etc/iptables/rules.v4
// mora install za persistent
sudo apt install iptables-persistent
// save current iptables
sudo netfilter-persistent save
----------------
// fix 5ghz hotspot ubuntu
// https://unix.stackexchange.com/questions/612498/hotspot-network-creation-took-too-long-and-failed-activation
// https://gist.github.com/ctubbsii/a3f15f7806830945a0b6c68ec0e23afa
// disable country randomization
sudo nano /etc/NetworkManager/conf.d/90-disable-randomization.conf
[device-mac-randomization]
wifi.scan-rand-mac-address=no
// install iw for config wifi
sudo apt install iw
// get country
iw reg get
// set Serbia country
sudo iw reg set RS
// make it persistent
sudo nano /etc/default/crda
REGDOMAIN=RS
// set 5ghz
sudo nmcli connection modify "Hotspot" wifi.band a
// restart network manager
sudo systemctl restart NetworkManager
---------------
// iperf
sudo apt install iperf3
// server
iperf3 -s
// client
iperf3 -c 192.168.1.132
---------
// kill current iperf3 -s by pid
sudo kill $(pgrep iperf3)
-------------------
// wifi 5, 6, 7 real world speeds 
Wi-Fi 5	- theoretical: 437 MB/s	    real-world: 25-75 MB/s
Wi-Fi 6	- theoretical: 1,200 MB/s   real-world: 60-125 MB/s
Wi-Fi 7	- theoretical: 5,750 MB/s	real-world: 250-625 MB/s

