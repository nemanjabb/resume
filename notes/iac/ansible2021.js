
// 193 Module Intro + Checklist
nista, sadrzaj


// 194 Introduction to Ansible 

agentless, samo ssh
module - small specific task
yaml - ne treba ruby

tasks -> modules -> description and arguments
hosts - koje masine
remote_user - user
vars: var1 ... {{ var1 }}

play -> task, host, user
playbook - 1 or more plays

inventory list - hosts file with ips or hostnames

playbook - dockerfile van dockera
tower - web ui

// 195 Install Ansible 

control node - locally, ne moze windows
written in python, moze sa pip

// 196 Setup Managed Server to Configure with Ansible 

serveri - linux ssh i python, na ubuntu ima vec, windows powershell
/usr/bin/python3

// 197 Ansible Inventory and Ansible ad-hoc commands 

IPs u inventory
/etc/ansible/hosts

ansible needs user pass or private key for ssh
ansible_ssh_private_key_file=~/.ssh/id_rsa
ansible_user=root

ili [droplet:vars]

// test
ansible all -i hosts -m ping // all servers ili groupname ili ip, m module, ping komanda, i - inventory

grouping hosts
[some-group]


// 198 Configure AWS EC2 server with Ansible 

ansible.pem // private key
chmod 400 ansible.pem // read samo za usera
ansible needs python3
ansible_python_interpreter=/usr/bin/python3


// 199 Managing Host Key Checking and SSH keys

// long living servers
eliminisi yes prompt za ssh
client masina treba da ima server u listi
~/.ssh/known_hosts
ssh-keyscan -H 165.22.201.197 >> ~/.ssh/known_hosts

// na serveru
~/.ssh/authorized_keys // public key
ssh-copy-id root@188.166.30.219 // da ubacu pub key ako je kreiran droplet bez, sa user i pass

// ephemeral servers, disable check
// ansible config
~/.ansible.cfg // globalan ili po projektu
host_key_checking = False


// 200 Introduction to Playbooks 

--- // yaml blocks separator
- // yaml list - array, to to, elementi liste
ansible je kao dockerfile na os umesto kontejnera

apt: // module
	name: nginx // package
	state: latest
service: // module
	name: nginx
	state: started

// idempotentno, moze vise puta ce da updatuje
// isto ko terraform, deklerativno, conf i state
ansible-playbook -i hosts my-playbook.yaml // ansible - run, ansible-playbook - docker-compose
ps aux | grep nginx // proveri da li je nginx proces startovan

// fixed version
launchpad.net/ubuntu // verzije apt paketa
name: nginx=1.18.0-0ubuntu1 
state: present

// uninstall
state: absent
state: stopped


// 201 Ansible Modules

ansible docs, modules index, params

// 202 Collections in Ansible

v2.9 modules
v2.10 razdvojeno, ansible + collections

collection - bundle - playbook, modules, plugin, docs
plugin - ansible code, python
ansible.collection.module

ansible-galaxy collection list // list all local collections
galaxy - repository za collections


// 203 Project: Deploy Nodejs application - Part 1 

apt: update_cache=yes // short 1 line version
apt:
	update_cache: yes // multiline version

/root // home folder root usera, a ne /home/root

svi moduli koje je koristila su vec u ansible.builtin
copy, src, dest, unarchive...

// https://gitlab.com/nanuchi/ansible-learn/-/blob/master/deploy-node.yaml

rm -rf folder // brisanje foldera i sadrzaja



// 204 Project: Deploy Nodejs application - Part 2

// install js dependencies
control node, managed node
src local, path remote, staza do foldera ne fajla package.json
comunity.general.npm - isto built in module

ansible.builtin.command - izvrsava bilo koju shell komandu
command:
	chdir:
	cmd: node server
async: 1000 // in background, detached mode
poll: 0

shell: ps aux | grep node // isto ko command sem sto je moguce pipe, redirect, boolean shell operatori, env vars, vise komandi prakticno, radi u shell zapravo
register: app_status // assigns izlaz prethodnog modula to variable koji mozes da odstampas u ansible output
debug: msg={{app_status.stdout_lines}} // stampanje


ansible je kao shell samo deklerativan, idempotentan
checks state, current, desired, change


// 205 Project: Deploy Nodejs application - Part 3

umesto root, user sa permission samo da pokrene app
become: True // false default
become_user: // ako izostavis default je root, default je iz hosts ansible_user=root
i dalje instalira pakete i kreira usera kao root, plays



// 206 Ansible Variables - make your Playbook customizable 

print vars za debug
parametrize
src: { } // { iza : je yaml dictionary
src: "{{ var1 }}" // zato mora " da bi se var razlikovala

// define vars in playbook
vars:
	- var1: blabla
	- var2: blabla

// from cmdline
ansible-playbook -i hosts my-playbook.yaml --extra-vars (ili -e) "var1=bla var2=bla"

var scope je play
vars snake_case

// vars file, najbolje
.yaml ili samo ime // a jeste yaml
var1: bla
var2: bla

var_files:
	- project-vars // u svaki play
	
	
// 207 Project Deploy Nexus - Part 1 
bilo koji rucni install procedura moze da se prevede u ansible playbook
deklerativni shell koji handluje state, idempotentan

control node - linux or macos, managed node	- moze i win
downloaded filename u var pa pass dalje	
	
find: // module nadji fajl kao regex, nexus-123
	paths: /folder
	pattern: "nexus-*"
	file_type: directory
register: find_result
...
	shell: mv {{find_result.files[0].path}} /opt/nexus
	
// conditionals	
shell i command modules nisu idempotentni, uvek ih izvrsava	

stat: // if file exists
	path:

shel: ...
when: not stat_result.stat.exists // skip shell if not exists


// 208 Project Deploy Nexus - Part 2
// prazan klip manje vise
adduser username // kreira usera i default group username, nelogicno malo
install module?

file:
	owner:
	group:
	recurse: yes
	
// write to file
blockinfile: // modul za upis bloka sa komentarima
	block: | // begining of multiline string
		run_as_user="nexus"
	
lineinfile:
	line:
	regexp: '^#run_as_use=""' // regex replace

na svaki run gledaj sta je changed, sta skipped

name: play-name

pause, wait_for moduli da sacekaju da se otvori port

// 209 Ansible Configuration - Default Inventory File
// kratak

ansible.cfg
inventory = hosts // staza sa fajlom da ga ne prosledjujes -h hosts u cmd


// 210 Project: Run Docker applications - Part 1

prevodi ovu shell skriptu u ansible
//https://gitlab.com/nanuchi/java-maven-app/-/blob/feature/sshagent-terraform/terraform/entry-script.sh

u ovo
// https://gitlab.com/nanuchi/ansible-learn/-/blob/master/deploy-docker-ec2-user.yaml

on amazon ec2 nema apt nego yum
aws python2 default, digital ocean python3

instaliras pakete kao root user, become: yes, become_user: root, default

// global
ansible.cfg
interpreter_python = /usr/bin/python3

// on task level, override
vars:
	ansible_python_interpreter: /usr/bin/python // python 2.7, ostalo radi sa 3

yum radi sa python 2

get_url: // download, mesto curl
mode: +x // chmod
// jinja2 template
{{lookup('pipe', 'uname -m')}} // {uname -m} shell, interpolac izlaz komande kao string

systemd: // systemctl, start service
sudo izvrsava kao root user, i to je to

sudo usermod -aG docker ec2-user // add ec2-user to docker group
da bi docker cmd radila bez sudo
mora logout login da bi se dodavanje usera u grupu reflektovalo

sudo gpasswd -d ec2-user docker // undo, remove user from group
meta: reset_connection // logout login module

name: desired outcome convention // bzvz


// 211 Project: Run Docker applications - Part 2 

command: // izvrsavas direktno samo kad nemas odgovarajuci modul, last resort, nemaju state managment, runs svaki put


community.docker.docker_image
namespace.collection.module
ansible.builtin // default

ansible tera python na serveru koji izvrsava module
pip: docker // install docker (python module) on server, python "js paket"
aws, digital ocean, nemaju sve linux distribucije iste pakete preinstalirane

docker_login: // za private repo za fetch image
// prompt u konzoli za var
vars_prompt:
	- name: docker_password
	 prompt: Enter pass for docker

vars_files: // za fajl sa vars

docker_compose:
	project_src: folder
	state: present // up, default, ne mora

gather_facts: False // disable it, needs python3, runs on begining of each play

playbook treba da bude sto je vise moguce reusable na svim distribucijama, a ne samo na toj kojoj radis

kreiraj novog usera, ne koristi default iz distroa
kad kreiras novog usera i dodas ga u grupe ne treba logout login kao kad dodajes postojeceg u grupu


// 212 Project: Terraform & Ansible 
// run ansible from terraform prakticno

handover from terraform to ansible, callback, pipe
ip in hosts file

terraform provisioners:
1. local-exec
2. remote-exec
3. file

// local machine
provisioner "local-exec" {
	working_dir = "..."
	command = "ansible-playbook --inventory ${self.public_ip}, --private-key ${var.ssh_key_private} --user ec2-user ...yaml" // overrides hosts file
	// ip, ssh priv key, user
}

yaml playbook, cfg, vars, hosts - 4 fajla

wait untill server is created, u ansible

name: wait for ssh connection
gather_facts: False
	wait_for: // promise
		port: 22
		search_regex: OpenSSH // on
		delay: 10
		timeout: 100 // sec
		hosts: ...
	vars:
		ansible_connection: local
		ansible_python_interpreter /usr/bin/python // 2


terraform pokrece ansible skriptu
ansible ima promise callback da ceka da ssh postane dostupan

// izvrsi ansible u posebnom terraform resourceu
null_resource // terraform ne kreiras resource nego izvrsavas komande
aws_instance.myapp-server.public_ip - self.public_ip u drugom resource
	
triggers = {
	trigger = aws_instance.myapp-server.public_ip // event, on ip change
}
	

// 213 Dynamic Inventory for EC2 Servers

stalno kreiraju i sklanjaju serveri
auto-scaling
inventory plugin umesto host file with ips, umesto hardcode ips

inventory plugin - yaml, i script - python, za login u aws sa ansible
plugin ima state management
aws_ec2 plugin
	
// ansible.cfg
enable_plugins = aws_ec2
	
// plugin configuration
inventory_aws_ec2.yaml // mora da se zavrsava tako aws...
poveze se na aws nalog da vidi instance
	
aws hostnames public, private - from vpc // dns name
enable_dns_hostnames = true
aws_ec2 stavi za hosts, i to je to
private ssh key i username u ansible.cfg
ansible-playbook -i inventory_aws_ec2.yaml playbook.yaml
inventory = inventory_aws_ec2.yaml // u cfg umesto -i ...

// filter dev prod on server name
// inventory_aws_ec2.yaml
filters:
	tag:Name: dev*

// print all dns names
ansible-inventory -i file.yaml --graph // ili --list za instance
all.aws_ec2.dns1...

// group servers
// inventory_aws_ec2.yaml
keyed_groups:
	- key: tags
	

// 214 Project: Deploying Application in K8s

k8s module

python3 -c "import openshift" // compile da vidis da li je modul instaliran

namespace i deployment nginx

kubeconfig: // kubern deplyment yaml file

terraform written in go // wiki

// 215 Project: Run Ansible from Jenkins Pipeline - Part 1

droplet za jenkins
ansible ne local nego poseban droplet box za njega, control node i on conf 2 aws ec2

instalira ansible rucno na do preko ssh
boto3, botocore - python moduli za aws

exit // exit ssh


// 216 Project: Run Ansible from Jenkins Pipeline - Part 2
// run ansible from jenkins, to je to prakticno, ko sto kaze naslov

jenkins je web app panel

ssh-keygen -p -f .ssh/id_rsa -m pem -P "" -N "" // convert new priv key format to old format

kopira from jenkins do to ansible do
ansible fajlove 
pem key za ec2
scp file root@167.99.136.157:/root/ssh-key.pem //kao root user na serveru

git commit // otvara vim, upisi poruku gore pa :wq
git push -u origin branch-name

jenkinsfile u groovy?


// 217 Project: Run Ansible from Jenkins Pipeline - Part 3

nista, detalji

// 218 Ansible Roles - Make your Ansible content more reusable and modular

reusable logic, functions, paketi prakticno // to to

role package - tasks, static files, vars, custom modules
ansible-galaxy // hub repo

roles/create_user/tasks/main.yaml // paste tasks
roles/create_user/files/... // static files
roles/create_user/vars/main.yaml // vars, prioritet za override...
roles/create_user/defaults/main.yaml // fallbacks, low precedence

// roles poziv u playbooku
roles:
	- create_user

-------------
-------------

// What is Ansible | Ansible Playbook explained | Ansible Tutorial for Beginners
// Techworld With Nana
// https://www.youtube.com/watch?v=1id6ERvfozo

no agent samo ssh na serveru
na klijenti se instalira ansible

yaml
modules - executes task
playbooks - 1 or more plays - rutina, skripta
how, which order, when, where, what modules
hosts, user
hosts file, ip adrese ili hostname foo.com - inventory list
variables - vars: i  {{ var1 }}
---
dockerfile - docker container
playbook - docker container, vagrant container, cloud instance, vm, bare metal
---
tower - ui admin panel
--- 
konkurenti - puppet, chef
oni koriste ruby umesto yaml
agent - install i update na target machines
----
rezime
automate repetitive, complex, tedious work
avoid human errors
-------------
// playbook = n plays in single file // 1 or more
- name: Something with database // play desc = tasks + hosts + user
  hosts: databases
  remote_user: root

  tasks: // configuration
    - name: Rename table foo to bar // task description
      postgresql_table: // module
        table: foo // arg
        rename: bar // arg

    - name: Set owner to someuser // taskovi se izvrsavaju po redosledu
      postgresql_table:
        name: foo
        owner: someuser

- name: Something with nginx // play 2
  hosts: websever


-----------------
-----------------

// Ansible Full Course | Ansible Tutorial For Beginners | Learn Ansible Step By Step | 
// Simplilearn
// https://www.youtube.com/watch?v=EcnqJbxBcM0

ansible je push system
playbook - yaml fajl

primer playbook instalira apache i html stranicu

-----

serveri koji se ponavljaju moraju biti identicni
chef, puppet - konkurenti, ruby umesto yml

push configuration - nemas nikakav runtime na klijentima, samo ssh, sve je na serveru, agentless
master slave je pull conf, chef, puppet

// na local machine
playbook - yaml script
inventory - node -> machine, aliasi za ip adrese

orchestration - install sw
configuration managment - os state
deployment application

agentless
efficient, nema overhead na klijetima, (client, slave, node)
flexible, ako se promeni infrastruktura
simple, samo yaml
idempotent, playbook mozed da izvrsis puno puta bez sideefects
automated reporting, clients vrati info sta je uspelo, a sta ne

// tutorial
yum install ansible -y

// hosts file
vi /etc/ansible/hosts

// yml
--- //pocetak

- name
...
	- name // - name zapravo
	
// pokreni playbook
ansible-playbook sample.yml --syntax-check // provera
ansible-playbook sample.yml

------------------
// Ansible Tutorial | What is Ansible | Ansible | DevOps Tools | 
// Edureka
// https://www.youtube.com/watch?v=XJpN8qpxWbA

// features
agentless, python, ssh, push, easy setup
	
// push pull razlika
push - server prati razliku i ako je nadje gurne je, bolje jer brze
pull - agent (client) periodicno zove server da updatuje konfiguraciju, bolje za veliki br nodes - scale	
	
rat sa skriptama prakticno

// architecture
host inventory, ips, group name, hosts
playbook -> play -> task -> module (750)
plugins, rade na local machine, master
connection plugin

modules - apt, copy, file, service... na nodes sto bi na ubuntu inace

ad hoc commands, kao docker run, za 1 komandu
ansible all -s-n shell -a 'uptime' // shell je module
---
// playbook
notify, handler - trigger and callback

---
// tut

ansible -m ping 'my-node-group' // text connection from inventory

vim
:wq // save and exit