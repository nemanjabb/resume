
//  Kubernetes Explained in 6 Minutes | k8s Architecture - ByteByteGo
// https://www.youtube.com/watch?v=TlHvYWVUZyc
cluster, nodes 
control plane - control node 
worker nodes 
pod - 1 ili vise containera, storage, networking
-------
// control plane:
1. api server - rest api 
2. etcd - key val db for state of cluster 
3. scheduler - alocira pods on worker nodes, resources usage
4. controller manager - manage state of pods, pali gasi, replicas
// worker nodes:
1. kubelet - client for control plane 
2. container runtime - runs containers
3. kube-proxy - networking, routing, load balancing
--------
// features:
1. self healing - high availability
2. auto rollback
3. horizontal scaling
4. portable - on premise, in cloud, hybrid
// mane:
1. complex 
2. expensive, resources 
// kompromis, magaed kubernetes service
aws EKS, google GKE, azure AKS
set up, control plane, scaling cluster, maintenance

