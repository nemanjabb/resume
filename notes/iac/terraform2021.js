
// Terraform Crash Course: Cloud/AWS Automation 
// Sanjeev Thiyagarajan
// https://www.youtube.com/watch?v=b1P2AH9bjpI

// platform provider
aws, azure...

// resource
create, modify, destroy, trivial
declerative, sync config file i state, not imperative

// kreira resurse samo, ne konfigurise ih
web server primer

--------------------
--------------------

// 129 Introduction to Terraform

// terraform vs ansible
terraform - provisioning infrastructure, new
ansible - configuring existing infrastructure, mature

// architecture
tf config
state
core - calculates diff and creates the plan

providers
cloud - iaas
kubernetes - paas
fastly - iaas

providers 100 -> resources 1000

refresh (get state), plan, apply, destroy

----------------
----------------

// Terraform explained in 15 mins | Terraform Tutorial for Beginners
//  TechWorld with Nana
// https://www.youtube.com/watch?v=l5k1ai_GBDE
// da ne bi kliktao po komplikovanom aws panelu nego skripta

automate infrastructure, platform, services on platform

// nevezano, moja ideja
prebaci i masine u softver, sta se lako a sta tesko menja
prebaci logiku program iz hw u softver
-----

declarative - what, end result
imperative - how, define each step
---
1. provisioning infrastructure - terraform
2. deploying applications on that infrastructure - programeri

e.g. create vpc, start servers, aws users and permissions, install docker
---
// zajedno oba su
ansible, terraform - automate infrastructure, iac
provisioning, configuring, managing

// to to
terraform - infrastructure provisioning tool
ansible - infrastructure configuration tool
---
// faze
1. create infrastructure
2. manage

replicate, dev, staging, prod env
---
2 ulaza -  tf.config file i current state
core diff state i conf file i kreira execution plan

providers (aws, kubernetes...) -> resources (ec2...) // i cvorovi u hcl, config file
---
declarative dolazi do izrazaja u updating fazi kad ne moras da brines o trenutnom stanju
sta da skloni sta da doda, i end state je uvek pregledan, nema diff
---
commands
refresh - sample current state
plan
apply - execute plan
destroy - in right order, no mem leaks
----------------------------
// What is infrastructure as code? - Terraform Tutorial
// The Digital Life
// https://www.youtube.com/watch?v=fEIIxZUf4co&t=42s
// https://github.com/xcad2k/boilerplates/tree/main/terraform/examples
nista prakticno
terraform init // downloads providers specified in tf
terraform moze samo docker containere da podigne, ne samo cloud
------------------
resource "aws_instance" "web" { // res type, res name
    ami = "some-ami-id"         // attribute
    instance_type = "t2.micro"
  }
