// default values for env vars in docker-compose.yml
// https://stackoverflow.com/questions/70772517/docker-compose-env-default-with-fallback
// to je zapravo shel sintaksa za expand env vars
// posix parameter expansion 
// https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
// dobra fora
----
// crtica :-
services:
  myapp:
    environment:
      SOME_VALUE: ${SOME_VALUE:-default-value}
-----
// moze i default env_file
services:
  myapp:
    env_file: ${ENV_FILE:-.env.local}
-----------------------------
-----------------------------
// toml
TOML (Toms Obvious, Minimal Language) 
YAML (YAML Aint Markup Language) 
----
// toml je trivijalan isto kao i yaml, svega nekoliko pravila
toml nije osetljiv na indentaciju
= umesto :
-----
// poredjenje toml i yaml, chatGpt
---
// objekti
// toml
[database]
  server = "localhost" 
  port = 5432
  connection_max = 100
// yaml
database:
  server: localhost
  port: 5432
  connection_max: 100
----
// ugnjezdavanje objekata
// toml
[server]
  name = "MyServer"
  location = "Data Center 1"

  [server.network] // eto, zapazi naslov sa ., inicijalno server mora da bude objekat, logicno, ok
  ip = "192.168.0.1"
  subnet = "255.255.255.0"
// yaml
server:
  name: MyServer
  location: Data Center 1
  network:
    ip: 192.168.0.1
    subnet: 255.255.255.0
------
// tabela..., zanimljivo
// toml
[[servers]]
name = "Server 1"
ip = "192.168.1.100"

[[servers]]
name = "Server 2"
ip = "192.168.1.101"
// yaml
servers:
  - name: Server 1
    ip: 192.168.1.100 // ovde nema -, nije lista, zapazi, fora
  - name: Server 2
    ip: 192.168.1.101
----
// polje, tj litsa
// toml
hobbies = ["Reading", "Hiking", "Cooking"]
// yaml
hobbies:
  - Reading
  - Hiking
  - Cooking
---------------------
// yaml | multiline scalar blocks
// prazne linije ignored, kao single space
example: |
  This is a multiline
  block of text.
  
  It preserves newlines
  and can include indentation.
----------
// linije su shell, ali celina je yaml
script: |
  cd /home/ubuntu/traefik-proxy/apps/astrowind

  docker-compose down
---------------------



