---------------------
// forward and reverse proxy
// https://roadmap.sh/guides/proxy-servers
forward je obican proxy (n clients), reverse je load balancer (n servers), 
reverse proxy je router, transformise jedan link na drugi i rutira kome treba
---
// load balancer - medju repliciranim instancama ISTE aplikacije
reverse proxy - razlicite apps
load balancer - same app
------------------------
------------------------
// traefik 2
// traefik je router, reverse proxy je router
// https://www.digitalocean.com/community/tutorials/how-to-use-traefik-v2-as-a-reverse-proxy-for-docker-containers-on-ubuntu-20-04
// https://www.alexhyett.com/traefik-vs-nginx-docker-raspberry-pi
// sustina, prednost traefika:
// za nginx mora da se edituje config file i da se rebuilduje Dockerfile kad se doda novi sajt
// za traefik ne mora, slusa docker socket
toml fajl - standardizovani ini fajlovi
----
// traefik.toml
entryPoints - 2 za 80 i 443 portove
// lets encrypt
acme - protocol za lets encrypt certifikate
email - kojim si napravio lets encrypt nalog
acme.json - fajl koji se servira na 443 za challenge
------
// static, dynamic
traefik.toml - static configurations
traefik_dynamic.toml - dynamic config
---
[providers.file] - gde je dynamic config - u traefik_dynamic.toml fajlu
---
dynamic configuration - middlewares i routers
http.middlewares.simpleAuth.basicAuth - protocol.middleware.name.koji - middleware za basic http auth za dashboard
middleware je vezan za protokol
----
http.routers.api - protocol.router.xxx
redirectuje 80 na 443
----
service - je zadnji handler gde zavrsava http request
------
// docker network - external
docker network create web
sluzi da komuniciraju kontejneri u vise od 1 docker-compose.yml
------------
// acme.json - fajl za lets encrypt
only the owner of the file has read and write permission.
chmod 600 acme.json
-----
// cemu sluzi dashboard
1. to see the health of your containers
2. to visualize the routers, services, and middlewares that Traefik has registered
// url
https://monitor.your_domain/dashboard/ (the trailing / is required)
-----------
// tvoji app containeri
// docker-compose.yml
// wordpress container - service
networks:
  web: // external da komunicira traefik sa apps containerima
    external: true 
  internal: // internal za wordpress i mysql kontejnere, za services u d-c.yml, containeri za tvoju app
    external: false
---
// labels: - povezuje traefic config sa app containers
// definicija za rutu
labels:
- traefik.http.routers.blog.rule=Host(`blog.your_domain`) // ruta regex
- traefik.http.routers.blog.tls=true // https
- traefik.http.routers.blog.tls.certresolver=lets-encrypt // cert provider
- traefik.port=80 // port kroz koji spoljni svet dolazi u kontejner, ne 3001 od app
----
// d-c.yml depends_on - redosled u kom se podizu kontejneri
blog:
	image: wordpress:4.9.8-apache
...
depends_on: // je kao load event, prvo mora mysql container da bude podignut
  - mysql
----------
// mysql container - service
labels:
	- traefik.enable=false // da traefik zatvori mysql container za spoljni svet
----------
// adminer container - service
------------
traefik v2.5.6 je zadnja
----
volumes:
- /etc/localtime:/etc/localtime:ro
- /var/run/docker.sock:/var/run/docker.sock:ro // :ro je read only
------------------------
// https://rafrasenberg.hashnode.dev/docker-container-management-with-traefik-v2-and-portainer
traefik.yml - static config umesto traefik.toml
dynamic.yml - dynamic config umesto traefik_dynamic.toml 
dynamic.yml moze da se edituje bez restartovanja traefika
---
u static svi default enabled false, pa svaki posebno label: enabled: true
---
// docker-compose.yml, traefik service
labels:
	// API handler - backend api za dashboard
	- "traefik.http.routers.traefik-secure.service=api@internal"
---------------
// docker-compose.yml aplikacije
// makni port i dodaj labele na container koji exposujes, i to je to
makni port sa glavnog app service koji exposujes, jer radi preko traefik-docker socket
//
labels:
  - "traefik.enable=true" // enable
  - "traefik.docker.network=proxy" // docker network
  - "traefik.http.routers.app-secure.entrypoints=websecure" // https spoljni svet
  - "traefik.http.routers.app-secure.rule=Host(`express.yourdomain.com`)" // ruta
----
traefik.http.router.app1-secure // .app1-secure must be unique ako deploy isti ap na vise subdomena
--------------------
entrypoint is incoming connection and loadbalancer is where Traefik redirects the requests
---
- traefik.http.routers.uptime-https.service=uptime-svc // ovde ga dodeli u service
- traefik.http.services.uptime-svc.loadbalancer.server.port=3001 // pa ga posle koristi services.uptime-svc
-----
// portovi
// direktno u docker-compose.yml ports ga rutira na http://whoami.docker.local:8081
whoami:
	image: containous/whoami
	ports:
		- "8081:80"
---
// traefik labela ga rutira na http://whoami.docker.local
labels:
	- traefik.http.routers.whoami.rule=Host(`whoami.docker.local`)
	- traefik.http.services.whoami.loadbalancer.server.port=80
-----------------------
// regex za ascii, slova i brojevi, subdomain
// https://stackoverflow.com/questions/3203190/regex-any-ascii-character
[[:ascii:]]
-------------------
// traefik za dokku i regex wildcard subdomains za http i https
// https://medium.com/@nithinmeppurathu/traefik-2-0-tls-passthrough-with-docker-ab-f560072a1bb
// HostRegexp, HostSNIRegexp
networks:
	- proxy
labels:
	- 'traefik.enable=true'
	- 'traefik.docker.network=proxy'
	# traefik will handle letsencrypt
	- 'traefik.http.routers.dokku-http.rule=HostRegexp(`{subdomain:[[:ascii:]]+}.dokku.${SERVER_HOSTNAME}`)'
	- 'traefik.http.routers.dokku-http.entrypoints=web'
	# dokku default exposes 80 and 443
	- 'traefik.http.services.dokku-http.loadbalancer.server.port=80'
	# passthrough https
	- 'traefik.tcp.routers.dokku-https.tls=true'
	- 'traefik.tcp.routers.dokku-https.tls.passthrough=true'
	- 'traefik.tcp.routers.dokku-https.rule=HostSNIRegexp(`{subdomain:[[:ascii:]]+}.dokku.${SERVER_HOSTNAME}`)'
	- 'traefik.tcp.routers.dokku-https.entrypoints=websecure'
	- 'traefik.tcp.services.dokku-https.loadbalancer.server.port=443'

networks:
	proxy:
		external: true
----------------------
// zbog ovoga je auth fail
// must put value in quotes "..."
TRAEFIC_AUTH=
"" ne pomaze zapravo
echo $TRAEFIC_AUTH
-------
// pogledaj uvek env var u portainer koja je zavrsila // to, to
yes part of var is lost
from this:
TRAEFIC_AUTH="admin:$apr1$E3mWMbAo$3MFvyqS6Ks0JJ4LVU.HCX"
i get only this in container:
TRAEFIC_AUTH=admin:$3MFvyqS6Ks0JJ4LVU.HCX
----------------
// mora escape dollar \$
i u docker-compose.yml
environment:
TRAEFIC_AUTH=${TRAEFIC_AUTH}
-----
// jeste to je sintaksa ={VAR}
//important: no quotes, or quotes will be included literally
environment:
- DOKKU_HOSTNAME=dokku.${DOMAIN}
// pogledaj uvek env var u portainer koja je zavrsila // to, to
------------
// poenta:
// jeste escape dollar ga resio, i docker-compose.yml TRAEFIC_AUTH=${TRAEFIC_AUTH}
TRAEFIC_AUTH="admin:\$apr1\$E3mWMbAo\$3MFvyqS6Ks0JJ4LVU.HCX"
-----------------
// traefik 3.0 ima breaking changes za yml fajlove, koristi 2.9.8
image: 'traefik:v2.9.8'
------------------
// Host vs HostSNI - (Server Name Indication) 
Host - za http rute
HostSNI za https rute, based on the value of the SNI header in the TLS handshake
--------------------
traefic.yml - static configuration - cant use env vars
dynamic.yml - dynamic configuration
----
both can be passed in docker-compose.yml command:, for env vars
----------------
// moze vise kontejnera na isti port, ali ne moze na isti subdomen // zapazi
- 'traefik.http.routers.nginx-with-volume.rule=Host(`${SITE_HOSTNAME}`)' // samo ovaj mora da ude jedinstven
- 'traefik.http.services.nginx-with-volume.loadbalancer.server.port=8080' // mogu svi, samo port u kontejneru
-----------------
// da bi razumeo cofiguraciju moras da znas strukturu, arhitekturu traefika, osnovne delove
// chatGpt
// 1. EntryPoints
Defines the network entry points where Traefik listens for incoming requests.
// 2. Routers
Matches incoming requests to services based on rules (e.g., domain names, paths).
// 3. Services
Defines how Traefik forwards requests to backend applications, including load balancing.
// 4. Middlewares
Modifies requests before they are forwarded to services or responses before they are sent back to the client (e.g., authentication, redirection).
// 5. Providers
Supplies Traefik with configuration data from various sources (e.g., Docker, Kubernetes, files).
// 6. TLS
Configures HTTPS, including certificates and secure connections.
// 7. CertificatesResolvers
Manages SSL/TLS certificates automatically using ACME providers like Lets Encrypt.
// 8. Static Configuration
Defines core behavior settings (e.g., entry points, providers) set in files or command-line arguments.
// 9. Dynamic Configuration
Settings that can be updated without restarting Traefik (e.g., routers, services, middlewares, TLS configurations).
------------
glavni ruter za Docker i Kubernetes, to mu je namena 
---------------
// kad menjas acme server sa staging na prod u core/traefik.yml
// always start with staging certificate
// caServer: "https://acme-staging-v02.api.letsencrypt.org/directory"
caServer: 'https://acme-v02.api.letsencrypt.org/directory'
moras da ispraznis acme.json da traefik popuni novi 
// 1. backup
cp acme.json acme.json.bak 
// 2. stop traefik
docker compose down 
// 3. isprazni acme.json ceo
// 4. podigni traefik
docker compose up -d


