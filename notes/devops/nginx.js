// https://www.nginx.com/blog/creating-nginx-rewrite-rules/
// $1, $2 su capturing grupe (), npr (/download/.*) i (\w+)
server {
    # ...
    rewrite ^(/download/.*)/media/(\w+)\.?.*$ $1/mp3/$2.mp3 last;
    rewrite ^(/download/.*)/audio/(\w+)\.?.*$ $1/mp3/$2.ra  last;
    return  403;
    # ...
}
-------
// ignore path
location /assets/ {
    # Do nothing
}
--------------------
// Treafik match Host and Path
// https://doc.traefik.io/traefik/master/routing/routers/#path-pathprefix-and-pathregexp
----------------
// NGINX Explained in 100 Seconds - Fireship
// https://www.youtube.com/watch?v=JKxlsvZXG7c
ima event driven arhitekturu sa nginx event loop // zapazi
event loop is about queue 
etc config folder za nginx.conf // etc kao linux
-----
// nginx.conf
directive - key value pair 
{ ... } context za directive, ili global 
-----
// contexts
http {
    server {
        listen 80

        location / {
            root /app/http; // serving folder, root je serve file
        }

        location / {
            root ~/.(gif|jpg|png);
        }

        location / {
            proxy_pass http://localhost:5000; // proxy_pass je redirect na drugi server
        }
    }
}
---------
proxy_pass, redirect je reverse proxy, routing, load balancing
