
---------------------
---------------------
// Apache Kafka in 6 minutes
// https://www.youtube.com/watch?v=Ch5VhJzaoaI

distribuirani redovi, queues, producer consumer

---------------------------------
// Yaml Tutorial | Learn YAML in 18 mins - TechWorld with Nana
// https://www.youtube.com/watch?v=1uFVr15xDGg

// used in
docker, ansible, kubernetes, prometheus

serialization language, xml, json
.yml ili . yaml

# comment

list:
	- item1
	- item2

list: [1, 2, 3] // isto lista

boolean: true, yes, no, on, off

// isto
name: some-string
name: "some-string"
name: 'some-string'

multilineString: |
	first line
	second line...

singlelineString: >
	this is
	long string

$ENV_VAR

placeholder {{ .Value.prop }}

--- mulyiple documents

kubernetes moze i yaml i json

------------------
------------------
// DevOps Roadmap 2021 - How to become a DevOps Engineer?
// https://www.youtube.com/watch?v=9pZ2xmsSDdo

1. development
2. os - linux - cli, shell, file system, ssh
3. network and security - firewall, proxy, load balancers, ports, ip, dns

opeations -> sys admin - os, network admin, security

4. containers, virtualization, containers

5. development -> continuous integration -> continuous deployment -> operations

continuous integration
tests -> package app -> build docker image -> image repository -> deploy to server

6. puno kontejnera, docker-compose.yml nije dovoljan - kubernetes, orkestracija kontejnera
7. monitoring - prometheus
8. infrastructure as code, prod, test, staging, dev environments
9. scripting language - bash, python
10. git - app code, iac code

------------------
------------------
// sadrzaj bootcamp
linux
git
build tools
digital ocean
nexus
docker
 jenkins
 aws
 kubernetes
 eks
terraform
python
 ansible
---------------------
// forward and reverse proxy
// https://roadmap.sh/guides/proxy-servers
forward je obican proxy (n clients), reverse je load balancer (n servers), 
reverse proxy je router, transformise jedan link na drugi i rutira kome treba
---
// load balancer - medju repliciranim instancama ISTE aplikacije
reverse proxy - razlicite apps
load balancer - same app




