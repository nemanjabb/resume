// 83 aws Module Intro

infrastructure as service

// sadrzaj
iam - users and permissions
vpc - virtual private network
ec2 - vps
aws cli

// 84 Introduction to AWS

scopes:
1. global // iam
2. region // s3, vpc, dynamodb
3. availaibilty zone // ec2, rds

// 85 Create an AWS account

trivijal, nista

// 86 IAM - Manage Users, Roles and Permissions 

default root user

1. users - human or system (app) users
2. groups - multiple users
3. roles - grant access, 2 services relation, service as user

permissions (policy) se dodeljuje roli, pa rola servisima

admin user // manje od root
samo administrator access chekirala

// 87 Regions & Availability Zones

geografija, nista
gde su useri taj datacentar

// 88 VPC - Manage Private Network on AWS

vpc - private network, prostire se preko vise availability zona
subnets - delovi vpc, u 1 availability zoni

svaki service je u vpc

private subnet - zatvorena firewallom za internet, scope vpc, baza npr
public subnet, web app npr

internal ip
ip/x je range

// access control, firewall
nacl - network access control lists - subnet level
security group - instance level

// 89 CIDR Blocks explained

cidr block - range of ips

cidr = 172.31.0.0/16
172.31.0.0 - 172.31.255.255

/16 broj fiksnih bitova
/16 32-16 broj promenljivih bitova
/32 = 1 adresa
/1 = 2^31 adresa
0.0.0.0/0 = any ip

// 90 Introduction to EC2 Virtual Cloud Server
// serve react app in docker from ec2, koristan

vpc default
1 subnet per a zone

8gb disk default

// ssh
create keys pair on aws
download them
store public key on instance

// kopiraj kljuc
mv private-key.pem ~/.ssh/

chmod 400 ~/.ssh/private-key.pem // samo read by user, skloni ssh warning
ssh -i  ~/.ssh/private-key.pem ec2-user@15.236.202.138

sudo yum update // pre bilo kog installa

sudo service docker start
ps aux | grep docker // da li radi docker process

sudo usermod -aG docker $USER // add user to docker group, da radi docker bez sudo
logout.login
groups // listaj grupe current usera

docker login // docker hub default
docker pull img-name:tag
docker images // list images
docker run -d -p 3000:3080 img-name:tag

otvori port
security groups
inbound rules - requests, into server
outbond - leaving server, odlazni saobracaj, dozvoljen sav, npr yum install nas server da salje zahteve celom internetu

dodala 3000 u inbound za react // otvorila server za 3k

// 91 Deploy to EC2 server from Jenkins Pipeline - CI/CD Part 1

build app -> build docker image -> push image to docker hub -> deploy image to ec2

ssh agent jenkins plugin

docker login keys vec pokretano na ec2

docker login, ssh login, docker run img, otvori ssh 22 za jenkins ip i 3080

stage('deploy') {
	steps {
		script {
			def dockerCmd = 'docker run -p 3080:3080 -d user/image:tag'
			sshagent([ec2-server-key']) {
				sh "ssh -o StrictHostKeyChecking=no ec2-user@35.180.251.121 ${dockerCmd}"
			}
		|
	}
}

// slozenost
1. docker run
2. docker-compose 
3. kubernetes

// 92 Deploy to EC2 server from Jenkins Pipeline - CI/CD Part 2
docker-compose primer

install docker-compose on ec2
sudo curl -L "https://..."
sudo chmod +x /usr/local/bin/docker-compose // za current user
// to je to
docker-compose --version

// pise docker-compose.yaml
services: // kontejneri

// copy docker-compose.yaml to ec2 server
sh "scp docker-compose.yaml ec2-user@135.180.251.121:/home/ec2-user"

// exec dc
def dockerComposeCmd = "docker-compose -f docker-compose.yaml up --detach"

... u shell scriptu
def shellCmd = "bash ./server-cmds.sh" // mora bash

// nevezano
jenkonsfile je onaj stack na azure pipeline, steps

// variable za image version u docker-compose, budzenje env var
// dc
image: ${IMAGE} // env var, na ec2

// jenkinsfile
def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}" // prosledj arg shell skripti, pros

// server-cmds.sh
export IMAGE=$1 // hvatanje arg u shell skripti

// 93 Deploy to EC2 server from Jenkins Pipeline - CI/CD Part 3
copy paste iz jenkins dela, trivijal

increment version stage // add step
commit version stage // commit edited pom.xml

ako step fail svi steps iza su otkazani


// 94 Introduction to AWS CLI

awscli --version // 2+

// set user
aws configure // user, region, out format json
~/.aws // tu cuva

aws command subcommand options-and-params

subnet je po availability zone

// create ec2 instance, nabavlja argumente potrebne
aws ec2 run-instances

filter - input args
query - filter output

// create user, group, add permissions
aws iam ...

policy = group of permissions

copy/paste za vim, nano...

// create user login and access key, switch users in cli

env vars zive po console prozoru

// 95 AWS & Terraform Preview

cli - no overview sta je kreirano
bolje terraform nego cli

// 96 Container Services on AWS Preview
nista, uvod u aws eks

--------------------------

// 117 eks Module Intro

sadzaj

// 118 Container Services on AWS 

container orchestration tools:
kubernetes, docker swarm, aws ecs, apache mesos, nomad

// ecs
ecs - na ec2, docker agent, ecs agent
fargate - serverles containers, aws hendluje instance, scales

// eks - kubernetes
master nodes: aws hendluje master nodes
workers: 1. ec2, 2. nodegroup, 3. fargate

node group ima autoscaling za razliku od nekoliko ec2

// ecr
elastic container registry
repository for docker images, privatni docker hub nista specijalno

-----------------
// 2024.
Fargate - serverless containers, ne diras OS EC2 i scaling - serverless containers
ECS - Elastic Container Service, ti manage EC2 i scalling - containers
EKS - Elastic Kubernetes Service


// 119 Create EKS cluster with AWS Management Console

sat vremena kliktanja po aws panelu

steps to create eks cluster:
1. eks iam role
2. vpc for worker nodes
3. eks cluster - master nodes
4. conn kubectl with eks
5. iam role for node group
6. node group
7. auto scaling
8. deploy app to eks

// 120 Configure Autoscaling in EKS cluster

dosta tumbanja i demonstracije auto scalera i load balancera

kreira custom policy od 3 ugradjenih
edit autoscaler deployment yaml and apply
sve ovo da bi ustedeo novac

// deploy app and load balancer
koristi native aws load balancer
instance po availability zonama

// 121 Create Fargate Profile for EKS Cluster 

bzvz

na fargate ne moze statefull apps

// 122 Create EKS cluster with eksctl command line tool

kreira eks komandom i ostatak videa objasnjava sta se sve kreiralo

eksctl je kubectl za aws klastere, za automatizaciju

chocolatey je brew za windows

eksctl komande moze i u yaml da se scriptuje

// 123 Deploy to EKS Cluster from Jenkins Pipeline

install u jenkins container:
kubectl
aws-iam-authenticator
kubeconfig file
aws credentials

docker exec -u 0 -it id bash // bash into cont as root

// kopiranje fajla u docker container
docker cp id:/var/jenkins_home/.kube/

// 124 BONUS: Deploy to LKE Cluster from Jenkins Pipeline 

linode
jenkins plugin, slicno

// 125 Jenkins Credentials Note on Best Practices

poseban jenkins user za kubectl

// 126  Complete CI/CD Pipeline with EKS and DockerHub

// https://gitlab.com/nanuchi/java-maven-app/-/blob/jenkins-jobs/Jenkinsfile-kubernetes/Jenkinsfile

deploy step sa kubernetes u jenkins pipeline
prosledi env var u yaml
sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'

kubernetes secret za docker hub

build pipeline

// 127 Complete CI/CD Pipeline with EKS and ECR
//isti gitlab repo file

aws ecr mesto docker hub za image

create ecr repository - aws gui
credentials in jenkins - jenkins form gui
secret for ecr - kubectl create type name url-option user-pass-options
jenkinsfile
---------------------
// 2023.
// AWS fault tolerant cloud architectures, cloud engineer, chatGpt
Multi-Availability Zone (Multi-AZ) Deployment
Auto Scaling
Elastic Load Balancing (ELB)
Amazon RDS Multi-AZ
S3 Data Replication
Distributed Architecture
Chaos Engineering
Monitoring and Alarming
Backup and Restore Strategies
Immutable Infrastructure
Stateless Services
Global Content Delivery, CDN
Cross-Region Failover
AWS Well-Architected Framework
Security Best Practices





