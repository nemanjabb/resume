<?php

/**
* Laravel notes from Laracasts videos (around 200 videos). Autumn 2015.
*
*
*
*References:
*	https://laracasts.com/skills/laravel
*
*
*/

//Service Providers

But, what do we mean by "bootstrapped"? In general, we mean registering things, including registering service container bindings, event listeners, filters, and even routes.

//make provider
php artisan make:provider RiakServiceProvider
class RiakServiceProvider extends ServiceProvider {
	public function register()

booth() method is called after all other service providers have been registered,
	public function boot()

//Registering Providers
All service providers are registered in the config/app.php configuration file. This file contains a providers array
'providers' => [
	...
    'App\Providers\AppServiceProvider',
],

//Deferred Providers
odlozi registraciju dok stvarno ne zatreba, iskljuci iz boot 
protected $defer = true;
public function provides()
{
	return ['Riak\Contracts\Connection'];
}
-------------
//Facade

$value = Cache::get('key');
$value = $app->make('cache')->get('key');
//vraca ime servisa

class Payment extends Facade {
    protected static function getFacadeAccessor() { return 'payment'; }

//Facades provide a "static" interface to classes that are available in the application's service container. 
//servisi sintaksno kao staticke fje

---------------
command bus command obrazac u service lejeru
-------------
comand patern  - transakcije za komande

svaka komanda u posebnu klasu, ta klasa pamti stanje pre i posle, objekti tih klasa mogu da se mecu u red - istorija
ne moze svaka operacija da ima undo redo, svaka promena enkapsulirana, atomicna, istorija od diskretnih koraka


Four terms always associated with the command pattern are command, receiver, invoker and client. A command object knows about receiver 
and invokes a method of the receiver. Values for parameters of the receiver method are stored in the command. The receiver then does the work. 
An invoker object knows how to execute a command, and optionally does bookkeeping about the command execution. The invoker does not know anything 
about a concrete command, it knows only about command interface. Both an invoker object and several command objects are held by a client object. 


Razdvaja objekat koji poziva operaciju od objekta koji zna kako da je izvrši


Primenljivost
	Parametrizovanje objekata prema akcijama koje izvršavaju
	Specificiranje, smeštanje u red i izvršavanje zahteva
	Registrovanje istorijata zahteva
	Obavljanje undo/redo u više nivoa (višestruki)

-----------------

//dependancy inversion
//high level modul ne sme da zavisi od low level modula, i low lev modul ne sme da zavisi od high level modula
dependancy inversion IOC high and low level modules su nezavisni u oba smera, decoupling
nezavisni od implementacija, argumenti koji se prosledjuju u construct treba da budu interfejsi
kodiranje prema interfejsu, da li objekat treba da zna (knowledge) kako drugi objekt funkcionise
zavisi od ugovora, ne od implementacije i to je to

//interface segregation - cepkanje interfejsa
//klijent ne sme da bude primoran da implementira interfejse koje ne koristi
interface segregation, lancane zavisnosti, A zav od B, B od C => A od C
objekat ne sme da zavisi (bude primoran da implementira (ima fje) koje ne koristi) od interfejsa koje ne koristi
podeli na vise manjih interfejsa koliko je potrebno, pa implements vise kojih treba samo
interfejs ugovor, klasa implementacija
treba da zna samo za ono sto mora, sve ostalo ne treba da bude svestan

//sass readibility
napravis desktop layout first, pa ga overajdujes za mobile - responsive @media

//kolekcija - objektna enkapsulacija polja
radi sa objektom u indentity map, ne radi sql query svaki put
radis upite u tinker napipas kako treba da izgleda upit
prosledis polje u konstruktor kolekcije i radis sa kolekcijom

//observables - publish subscriber
izmestis kod iz kontrolera, u kontroleru emitujes event, hvatas gde treba
Event::fire("eventName", $user)
Event::listen("eventName", function($user){})
Event::subscribe("klasaSaHendlerima")

//namespace
stablo imena klasa i promenljivih umesto skupa
default krece od namespace-a klase u kojoj se nalazi ako je bez \
use klasa as alias

//composer
"require" : { "ime":""verzija""}, "require-dev":
composer.lock sadrzi tacne verzije koje su instalirane, u json mogu wildcardovi, ide u git
radi sa composer install, sa composer update ga ignorise

require_once("vendor/autoload.php")
"autoload": { "files": ["fajl1",...], "classmap":[ klasa ili samo namespace]}
composer dump cvor ili sve
autoload klasa, fajlova, namespaces

"psr-0":{"namesp":"staza"}

"scripts":{staticka fja ili bash}
za dev update, za produkciju i kad drugi salje install

//cache
file, mem, rpc
Route::nesto() su hendleri pretplaceni na dogadjaje
"klasa@fja" hendler fje kao stringovi
filter ima before i aftter hendlere koji uticu na request ili response
Cache:: get, put, hash
kesiraju se rute

//single responsibility
klasa radi 1 stvar, menja se za 1 stvar
kontroler samo poziva interfejse ili servise, nema impl u kontroleru
createUser servis
izvrsavanje u pozadini Queue:push()


//incremental validation
kad prosledis celu formu objekat u db, zato u eloquent model oznacis polja koja user sme da menja
izvadi pravila za validaciju van kontrolera da bi bila reupotrebljiva

//service providers
$app["imeServ"]
App:make("imeServ")
bind() vezuje "imeServ" za fju, kontejner na jednom mestu ubacuje sve povezane zavisnosti
use ($var1) prenosi u clojure
illuminate je laravel root namespace

autoload provider klasu, config register(), impl klasu, nasledi serviceProvider

//beanstalkd
redovi se izvrsavaju asinhrono

//view-presenter
sve sto se upotrebljava vise od 1 puta napravi klasu
Trait mini klasa
Exception ima poruku i nested exception, composite obrazac

//extracting packages 137
composer.json u root paketa

radis sa ugovorima,prosledjujes implementacije

//efficient sql
oprezno sa select *, ne vadi vise podataka nego sto ti treba
prevodi sql u eloquent, innerjoin, DB::raw
lesson vadi sve u kolekciju pa upituje nju
lesson() nastavlja direktan sql upit


//psr4
composer dump, linker
mapiranje namespace na stazu, smesta u /composer/autoload_psr4.php
composer dump-autoload -o

//gulp//prost kao pasulj
moduli u promenlj pa pipe(), default, run i watch taskovi, gulpfile.js
package.json navedeni pluginovi

//laravel and gulp
testovi na save
testovi su semanticka ogranicenja nad aplikacijom kao kompajler za sintaksna
require() je include za node

//deploy
git, trigger composer, env(password) da ne ide u git
treba detaljnije


//url shortener demo
kontroler treba samo da prima request i salje response
//2
fasada - service extends Facade getFacadeAccessor( return "Little"), provider bind, register u app.php, psr4 namespace na stazu u composer dump autoload cvor
class LittleService make i druge fje impl
validacija se okida emitovanjem eventa

servis - reupotrebljivi singleton koji ubacujes sa di

//auth essentials
ruta + templejt je stranica

//blade 101
@yield(content) ubacuje content templejt tu, (parent, master) nema @extends("app")
u child @extends("app") 
@section("content") sadrzaj @stop

@foreelse = @if @foreach

//basic model controller view-presente
baza vrati null u controller, ne if(is_null()) svaki put nego findOrFail() koji baci exception pa hendlujes na jednom mestu za exception
link href="{{action('ArticlesController@show', params)}}"
url(), route()
compact('articles') prosledjuje [articles, $articles] u templejt

//flash messaging
user()->articles()->create($request->all());
Session::flash()
redirect('articles')->with([poruke])//prosledjuje u templejt
import paket - 1. composer require 2. app.php service provider 3. alias - fasada 'string'
flash() paket



//one form to rule them all
izvuci zajednicko za formu u partial template
za edit prosledjujes usera sa Form::model($user, ...)

//exceptions 60 dobar 61 nastavak
nikad fail silently ili  return, baci exception
tip exceptiona opisuje sta je puklo
laravel prozor prikazuje gde je throw
get_class_methods($objekat);
ClassLoader::addDirectories u global.php radi autoload kao psr4-0
prazna definic klase za exception
mozes sve exceptione umesto try catch da hendlujes u App:error() u global.php
exception buubles do root koji je Exception
moze globalni hendler za exceptione koji ne znaju za lokalne prom, jeste event model donekle
//zapamti sql dump koja fja bese

//di
1. kroz konstruktor
2. refleksija i property
3. sa init fjom klojure

servis globalni singlton koji mozes svuda da instanciras i mockupujes za testiranje i da
napravis paket od njega da upotrebis u drugoj aplikaciji kroz composer
//gledaj gore u kom je fajlu na klipu

objekat.fun1("imefjekojustvrnozoves")//command pattern
refleksija - reversing tipova
watch na promenljivu zaustavi debuger kad god se ona promeni da vidis gde je zadnji put dodeljena, kako?

//27 event driven code
Event::fire("ime.eventa");
Event::listen("ime.eventa","namespace\klasa@fja")
Event::subscribe("namespace\klasa"); poziva klasa.subscribe()
u events.php posebnom fajlu ili u konstruktoru controllera, ili subscribe u impl klasi
//ne vidis u kontroleru ko sve hvata event losa strana

//33 search essentials
ide get jer post radi postback na istu stranicu
EntityKlasa::where('title', 'LIKE', "%$query%")->top(10)->orderBy("kolona")->get();
u ruti ili u App::make("DBRepoKlasa"); repo klasa koja se poziva u ruti

//35 eloquent events
Entity:create([polje]); kreira row
Entity:saving(function(){}); hendler koji se okida na save, return false ne insertuje//hendler u ruti ili boot() overload u modelu

validacija
$v = Validator::make($objekat->getAttributes(), static::$rules); polje koje se vidi tu
$v->fails() vraca bool

//48 dependancy injection containers
 App::bind("klasa", function(){
	 return new klasa(new klasa1);//poziva konstruktore na stringove na make
 });
 
App::make("klasa");
App::bind(), make(), singleton(), instance()

trivijal primer gura stringove i fje u polje i vraca, ne znam kako radi
$registry[$name] = callableResolverFja;

//55 bower
umesto da skidas js fajl i kopiras u folder radis bower instal lib.js//nicemu ne sluzi
bower install jquery -S// -S je save komandu u bower.json fajl// -D development zavisnosti conf fajl
bower search lib.js
js biblioteke u .gitignore


//83 coding with intent - cist kontroler umesto duge procedure, ostalo izolovano u druge obj
namespace je folder putanja po nekoj konvenciji prakticno
base klasa za validator, u izvedenim samo pravila
u kontroler samo update i hvatanje iuzetka
update overidovan u baseModel
abstract validator validate(), validateForCreation(), validateForUpdate(), ima sve implement ali ne moze da se instanc, pravila u izvedenoj
nista nije uradio samo raspodelio kod u osnovne i izvedene, model i validator


//38 mock that things
ako A zavisi od B , B ima svoj test, kad se testira A ne testira se B vec se mokuje
testira se da li je pozvana fje a rezult se mokuje
injecting je prosledj obj kao arg, a ne = new Obj()

1. arrange - napravi mock
2. act - prosledi ga u fju
3. assert - poredi sa ocekiv rezult


//zasto rest ide post put patch delete
1. url definise resurs - entitet, uri - unique resurs identifier
2. http zahtev definise operaciju
3. argumenti podaci
//inace bi operaciju morao da dodas kao argument

//43 form model binding
za update treba obj da ucita u formu, binding Form::model($obj, ['method'=>'PATCH','route'=>['orders.update',$order->id]])

//41 say no to implicit routing
koristi eksplicitno kontroler i metodu za rutu kontroler@metod
Route::resource() generise nekoliko 7 kom get, put, patch ruta, "php artisan routes" da ih ispise

//45mass assignment explained
forma ubacuje ceo objekat, radi na nivou objekta
moze da prosledi propertije koji nisu u formi ako zna kako se zovu u entityju, hiden input
User::create(Input::all())//ranjiv kod

da ne bi morao za velike forme za svako polje odvojeno da dodeljujes
u User modelu
$guarded = [] //zabranjena
$fillable = [] //dozvoljena//zahtev prodje ali polje ne ode//bolje//whitelisting
i sad mozes bezbedno mass dodelu

//69 tinkering with boris
artisan tinker interaktiv php konzola
boris nova verzija
ima zavisnosti, enable u php ini, vagrant
extenzije readline i posix



//94 http with guzzle
guzzle http klijent objektni
OuathPlugin([kljucevi])
array_fetch($arr, $kljuc) mesto foreach

//95 refactoring prethodnog odlican
u servis, TwitterServiceProvider extends ServiceProvider registr u app.php providers[]
register() $this->app->bind('twitter', clojure(){return new TwitterAPI($client)})
u TwitterApi __constr(Client $client) se injektuje sa di
kljucevi u config da ne idu u git
App::make('twitter')->search()//poziv servisa
fasada Twitter extends Facade getFacadeAccessor() {return 'twitter';}
u app.php alias za klasu za full namespace
Twitter::search()

//94 95 96 odlicni

//169 decorator
veliki broj klasa, mora na puno mesta da se menja
build object runtime
prosledis klasu kroz konstrukt koliko ti treba, pozoves fju iz izvedene klase
implementiraju interfejs, nema nasledjivanja
cim je nasledjivanje to je compiltime, nije runtime

sve klase implementiraju isti interfejs i primaju ga kroz konstruktor (to je to), pa se prosledjuju new A(new B(new C))
umesto nasledjivanja kad ti ne treba cela funkcionalnost
Window decoratedWindow = new HorizontalScrollBarDecorator(new VerticalScrollbarDecorator(new SimpleWindow()));

//178 sanitizers phpspec
tdd bezveze dug

//172 routing best practices
Route::group(['prefix' => 'admin'], function(){}) dodaje /admin/svim rutama

php artisan routes
Route::resource() 7 ruta

link_to('neki/link') da ne menjas sve linkove kad promenis rutu i obratno
named rute 
Route::get('session/create', ['as', => 'login', 'uses' => 'SessionController@create']);
link_to('login', 'Log In');
route('login') vraca apsol putanju

//146 entities vs value objects

value immutable
single responsibility, poseban tip (to je value object) sa validacijom za tipove u konstruktoru entiteta

//166 interface necessity
 za db migrate treba dev mode u bootstrap.php enviroment var getenv()

 _lesson u teplejtu se mapira na $lesson prosledjeni
 
 interfejs i nasledjivanje templejta...
 
 
//171 pass hashing
cost 10
na 5.3 koristi pomocnu biblioteku trivijal klip

//173 user roles

pivot vezna tabela

Route::get()->before('filter');
Route::filter('role', function($route, $request, $role)){}


//168 crons and notifications
command
MailCatcher hvata mejlove za debug
mejlovi se salju asinhrono iz redova zbog tajmauta
kron minut, sat(24), dan, mesec, dan u nedelji(nedelja=0)
* bilo koji


//161 laracasts live
refleksija - reverse inzinjering objekta iz koda

bind - vezivanje klase (implemtntacije) za interfejs - refleksija DI

App::make('namespace\klasa') rekurzivno instancira sve zavisnosti
redirekcije idu u kontroler

when - znaci ide event

php hemoglobin eventi na youtube klip
envoy je automatizacija na serveru php (git, deploy itd) kao gulp

//gledaj bez prekida da udjes u sinergiju, bez pauza
//jedva cekas da pogledas sta ima i rastumacis, nema mnogo
//svake godine treba 10 ispita, sta za god dana naucio
//izvuces beleske korake da konkr mozes da radis
//sta neko kaze dosta je pojasnjeno u zav ko kaze i u kojim okolnostima

//mozes doctrine repo i eloquent repo//pitanje...

//010 larabook commands
use namespace da ne bi morao svuda fully qualified da pises tipove // TO JE TO, PROS, ZATO NAMESPACE
DTO object - struktura

extract vadi string polje u promenlj "var1" u $var1
compact("var1") pravi arr od stringova ["var1" => $var1]

command poziva hendler
RegisterUserCommandHandler.php klasa poziva se handle() metod, koji poziva User::register() staic iz User.php koji ga samo vraca, save(), emituje evente dispatchEventsFor($user), i return $user
iz kotrolera ide komanda $user = $this->execute(new RegisterUserCommand($args))

//011 larabook domain events
php trait ???
$user = new static() ???
event ide u past tense UserRgistered, $this->raise(new UserRegistered) dizanje eventa
raise() event, dodaje u red  $this->pendingEvents[] = $event;
releaseEvents() kopira u lokalnu, prazni $this->pendingEvents = []; i vraca lokalnu return $events;

UserRegistered event je DTO objekat, struktura obicna
command je enkapsulacija ...
event string je namespace event klase sa tackama "Registration.Events.UserRegistered"
Event::listen("Registration.Events.UserRegistered", function() {hendler telo});//hvatas event gde treba

//001 virtual machine
vagrant je cli za vbox slike

instaliras vagrant, 
vagrant -v //za verziju, da li je instaliran
vagrant box add laravel/homstead //dodas homstead sliku
git clone https://github.com.laravel/homstead.git Homstead //kloniras repo u folder gde ces da radis//fajlovi koji citaju yaml
ssh-keygen -t rsa -C "mejl@mejl.com" //kreiras ssh kljuc//gde?//na pub i id_rsa fajlovi na nivou lokalne masine

//---------yaml---------
editujes Homeostad.yaml staze,ssh kljuc, sinkovani folderi, env promenljive//homeostad je slika, yaml konfig za masinu ram, ip, cpu itd//u homstead folderu
authorize: staza sa lokalne masine gde je kljuc.pub
keys: lok staza do id_rsa
folders: map: lokal, to:virtualmachine staza //larabook

sites: map: url u lokal browser, to: file staza na linux vmachine www/public
lokalni etc/hosts 127.0.0.1 larabook.app//url na 127

variables: key:APP_ENV key value hashtablea sa promenlj

lokal cmd laravel new larabook//u sinkovan folder//to//nije homstead//laravel je u sinkovan larabook folder, a homstead masina je u svom homstead folderu
cd Homeostad//gde je repo checkoutovan (clone)
vagrant up //dize masinu
//------------

//-----udjes u vmasinu
vagrant ssh //iz larabook foldera//ne radi
cd ~/
ls

ssh vagrant@127.0.0.1 -p 2222 //user@localhost -p port // iz larabook foldera
sudo vi ~/.aliases //ne postoji taj fajl//lokalna masina macboock
alias vm="ssh vagrant@127.0.0.1 -p 2222"
sad samo vm

cd ~/larabook //to je sinkovan na vmachine // /home/vagrant/larabook
http://larabook.app:8000

mysql -uhomstead -p
pass je secret, ulazis u mysql>
mysql> show databases;
homstead baza vec ubacena u laravel instal
//nadji ceo homstead.yaml primer


//002 dependecies
composer moze i iz vm i local

u composer.json
"require-dev":"way/generators": "~2.0",... ili require
composer update
config/app.php ubaci provider
//svi paketi iz lekcija ima na laracasts packagelist.org



//003 database configuration
config/local/app.php "debug" => true
config/local/database.php homestead baza//ulazi u git onda

config/database.php getenv('DB_NAME')
.env.local.php u laravel root return [] env vars i ubaci ga u .gitignore .env.*.php

mysql admin spolja  host:127.0.0.1, username, pass, database, port 33060 //baza se uvek kaci na IP odaklegod, isto je

//004 master page
@yield('content') u master // layouts/default.blade.php
@extends('layouts.default') @section('content') sadrzaj @stop //u izveden template // pages/home.blade.php

//005 designing...
bootstrap uzeo navbar i jumbotron nista


//006 gulp sass
u linux npm instal gulp gulp-sass gulp-autoprefixer --save-dev //u package.json ubaci zavisnosti za npm install kasnije
gulpfile.js u root
autoprefixer su prefixi css3 za razlicite browsere npr transition all 1s;


//007 registration with bdd
testovi unit, functional, acceptance

vendor/bin/codecept build //hedles browser kao selenijum ili phantom//klikni link, popuni polje...
//napise test na pocetku, klikni link, treba da dobijes link, na strani pise assert...

vendor/bin/codecept run functional


//008 users

functional.suite.yaml enabled Asserts, build

//009 registr validation
composer require laracasts/validation //import valid modul 1.1
u /config/app.php Laracasts\Validation\ValidationServiceProvider

php artisan key:generate
 larabook/Forms/RegistrationForm.php rules
pa autoload: psr-4:Larabook\\ : app/Larabook , composer dumpautoload

FormValidation exception u try catch ili u App::error(function(){}) u global.php

//010 commands
require laracasts/commander , users actions as classes, command bus

CommandBus arg u constr kontrolera//trait koji ti kreiras poziva App::make("Laracasts\Commander\ComandBus")

RegisterUserCommand je DTO (data transfer object, struktura) username, email, pass
hvata ga handle() u RegisterUserCommandHandler implements ComandHandler //sto je islo u kontroler ide u hendler
u kontroler new RegisterUserCommand pa this->commandBus->execute($command)

ako klasa pocinje sa \Klasa znaci ref globalno, fully qualified name
ako Klasa onda koristu use Namespace
setPasswordAttribute() $password ce uvek da se postavlja kroz taj seter

u vm ~/.bashrc fajl
alias t="vendor/bin/codecept run"

//012 easy flash message
require easy flash message modul, provider, fasada
Flash::message("") , error, overlay (modal)
@include("flash::message") u templejt
paket koristi bootstrapove klase
u templejt uvek na load poziva $().modal()

//013 filters
u constr kontrolera $this->beforeFilter("guest") //default impl redir na home, filters.php
koje promenjive (objekti eloquent) se vide u templejtu?


//014 signing in
napisao test i pise kod da ga predje, sign in scenario
codecept test idi na str, pritisni link... je funkcionalni test
codecept custom assert ide napises u FunctionalHelper
dummy user u testDummy/factory.php vuce iz tests/fixtures.yml 
forma required pokrece html5 validaciju
compact(args) najobicnija helper fja napravi arr od promenljivih, ako fja prima 1 arr umesto vise args

//015 returning tests to green
2 buga u codecept, tad

//016 publishing statuses
fixtures $string, $text, $date, $string@example.com, embeded tipovi... generise rand

//017 integration tests Repositories
integration folder
TestDummy::create usera, sacuva kroz repo, pa hvata na stranici br postova i string u body tagu

---------------
command nezavisan objekat koji enkapsulira akciju, hendler odvojen od eventa, enkapsulira prelaze u automatu

za istoriju treba execute i restore metodu (up i down)
neke komande nepovratne

sto nema vise fja, ponasanja nema potrebe da bude klasa, dovoljno je fja da bude
sto ne treba vise od 1 da se izvrsi dovoljno je da bude anonimna fja

//018 gravatar and presenter
helpers.php ili presenter $user->presenter()->getGravatar()
presenter klasa nasledjuje entity klasu, User npr i onda filteruje neki property, da to ne bi bilo u entity klasi
i dodas protected $presenter u entity klasi

//019 design statuses
css za komentare cacka
kad izbaci sa float sredi sa overflow:hidden;
cim koristis vise od 1 ide partial template ili klasa

//trait klasa
prosiruje jednostruko nasledjivanje
php 5.4
u klasi use NekiTrait;
koristi metode, a stanje? staticka klasa...?

//20 dummy users and statuses, dobar
Faker klasa koja generise lazne podatke za entitije iz baze
DatabaseSeeder extends Seeder, StatusesTableSeeder extends Seeder
faker word moze da generise duplikate za pk pa +id za unique
popuni 2 referencirane tabele sa FK

//021 browsing users with pagination
codecept test za stranicu html, functional test
User::simplePaginate() queryje bazu samo za prev, next
napravi novu bazu za test konfiguraciju

//022 profiles
Route::get(["as" => "ime_rute","uses" => "ControllerClass@fja"]) ime_rute posle koristis npr u link_to("ime_rute")
User::with("statuses") vraca statuses u user objektu
TestDummy generise test objekte na osnovu yaml-a, vadi i referencirane usere iz baze...
-----------------
//uopste
web aplikacija je pogled na bazu koja JE model
pravis vece module koje reupotrebljavas gde treba brzo, forma npr, templejt

//paznja traje 10 min i to je to//pauza za novi peak
objekti imaju isti sablon ali razlicito stanje
staticka klasa nema instance sa razl stanjem

sve sto radis moras da znas zasto radis, zasto je to korisno i ubrzava i olaksava, testovi
framework je smesan, neke trivijalne helper fje
-------------
//023 refactoring
$string$integer dodaje autoinc za unique
uzima id od trenutno ulogovanog umesto hidden
bzvz mucka refactor

//024 following users
funkcionalni test
//manyToMany pivot vezna tabela
-------------------------
//Laravel 5 Fundamentals
//01 - Meet Composer
ceo composer je u composer.phar
composer require phpspec/phpspec
vendor/paket
composer create-project laravel/laravel moj_folder dev-develop (grana)//kreiras projekat a ne require modul u postojeci
laravel new moj_folder_proj
php -S localhost:8888 -t public //pokrenes//public folder je root
-S  php built in seerver
-------------------------
//02 - Virtual Machines and Homestead
composer global require "laravel/homestead=~2.0"
homestead komanda radi u cmd, ako ne ~/.composer/vendor/bin u path
npp run as admin, pa edit with npp, on otv kao admin za hosts
-------------------------
//06 - Environments and Configuration, pros
.env fajl ne ide u git, ubacen u .gitignore, iz njega se citaju env("DB_PASSWORD", "default_vr") env variable u config/database.php, "default" = "mysql" npr
u .gitignore /vendor i .env
u /config svi .php za konfig
homestead up
homestead ssh
default user pass za baze je homestead/secret
-------------------------
//00001 - laravel-guard-intro
za css min ko gulp
-------------------------
//01-Installation//trivijal bas
instalira composer
composer create-project laravel/laravel mojprojekat
php artisan serve //php server
--------------------------
 | grep u linux je regex match, search ili ctrk F
zagrade () redosled izvrsavanja izraza
--------------------------
//https://laravel.com/docs/5.2/lifecycle
The application instance is created, the service providers are registered, and the request is handed to the bootstrapped application.
deffered provider - lazy load graf
//https://laravel.com/docs/5.2/container
Since the service is injected, we are able to easily swap it out with another implementation. We are also able to easily "mock", or create a dummy implementation of the mailer when testing our application.
$this->app je kontejner, vezuje se za tip i moze da vraca (binduje za) novi objkeat, singleton ili specif instancu
biranje jedne implem za interfej. when needs give izabere drugu implem za svaki tip, moze i primit tip int npr uz
tag - resolve vise klasa
//Resolving - poziv konstrukt prakticno
$fooBar = $this->app->make('FooBar'); $fooBar = $this->app['FooBar'];
kontejner ispaljuje event kad resolvuje mozes hendler da napises, sam obj je dostupan kao arg za dalju izmenu
//https://laravel.com/docs/5.2/facades
facade is a class that provides access to an object from the container
getFacadeAccessor()	 job is to return the name of a service container binding
extend the base Illuminate\Support\Facades\Facade class.
A facade class only needs to implement a single method: getFacadeAccessor 
The Facade base class makes use of the __callStatic() magic-method to defer calls from your facade to the resolved object.
Facade Class Reference tabela
------------------
//sto sam pitao
//https://laravel.com/docs/5.2/eloquent-serialization
$user->toArray() ili $user->toJson() ili (string) $user; - nad jedim ili kolekcijom, vuce sve referencirane
controller radi tostring response
protected $hidden = ['password']; u modelu koji neces da idu u json, $user->makeVisible() za jednom, moze i appending olja kojih nema u bazi
------------------
namespaceovi svuda - sluze da ti struktura foldera ne diktira linkovanje klasa












