<?php

/**
* Notes about testing in Laravel. Summer 2016.
*
*
*
*References:
*	 https://semaphoreci.com/community/tutorials/getting-started-with-phpunit-in-laravel
*    https://semaphoreci.com/community/tutorials/getting-started-with-bdd-in-laravel
*    http://laravelcoding.com/blog/laravel-5-beauty-testing
*    https://www.sitepoint.com/mock-test-dependencies-mockery/
*    https://laravel.com/docs/5.3/mocking
*    http://laravel-recipes.com/recipes/27/environment-specific-configurations
*    https://scotch.io/tutorials/how-to-use-laravel-config-files
*    https://scotch.io/tutorials/how-to-use-environment-variables
*
*
*
*/




zasto dobro: 
jednom napises test dok pravis, i zavrsio sa tim, a ne stalno cackas i proveravas sa drugim
znaci jednom ili hiljdu puta
cim pravis nesto vece
kad promenis nesto znas da li i dalje prolazi test
testing - boza

svaka etepa u razvoju se testira, implementac, integracija, funkcional, jok na deploy cackaju, na 1. run




//phpunit
//https://semaphoreci.com/community/tutorials/getting-started-with-phpunit-in-laravel

laravel ima helpere za html strane, navigacija, klik...
inace koristis osnovne phpunit asert metode za strukture i promenljive, i to je to
------------------

//bdd
//https://semaphoreci.com/community/tutorials/getting-started-with-bdd-in-laravel
BDD comes in two flavors: SpecBDD and StoryBDD.
SpecDD - phpspec, kao phpunit, fje
StoryBDD - behat, feature testing

behat prevodi ljudske scenario recenice iz yaml u pozive test fja
vise igranje, prosto
behat je samo ljudska sintaksa za neprogramere, nepotrebno
phpspec je druga sintaksa za test fje, i asert metode idu u this testirajuce klase

---------------
//Functional / Acceptance Testing
//http://laravelcoding.com/blog/laravel-5-beauty-testing

crna kutija, aplikaciju, ne kod

Codeception - Problem the most popular framework for acceptance testing.
Selenium - Browser automation.
Mink - Brower automation.
-----------------
jeste sve to prosto, framework, razmotava se

-----------------
google:
mocks for phpunit i lokacija
asserting void functions

//https://www.sitepoint.com/mock-test-dependencies-mockery/
mesto di instance ili konstruktora instanciras mock objekat Mock:: pa njega prosledis, trivijal
sa shouldReceive() postavljas stanje mock objekta, pa njega prosledis

//https://laravel.com/docs/5.3/mocking
//shouldReceive(fja1) postavlja povratnu vrednost koju fja1 nadalje vraca, tacno to
//fja1 je definisana nad objektom (ili di ili fsadom) nad kojim je i shouldReceive pozvana
////mock ne testira (assert) samo overvrajtuje vrednosti koje fja vraca nad mock objektima

//postavlja vrednost za taj kljuc, ali nista stvarno nije upisano u cache, to je i poenta
Cache::shouldReceive('get')
            ->once()
            ->with('key1')
            ->andReturn('value1');

nadalje Cache::get('key1') vraca 'value1'
postavlja vrednost za $value = Cache::get('key1'); u gornjem kdu, u kontroleru
---------------------
//http://www.tutorials.kode-blog.com/laravel-5-faker-tutorial

//faker je samo za random vrednosti, name, email itd
$faker = Faker\Factory::create();
$faker->name . ', Email Address: ' . $faker->unique()->email . ', Contact No' . $faker->phoneNumber 

//a Factory je za test objekte
---------------------
app konfiguracije, debug, test, production... pogledaj docs
stanje u klasi unit testa
test konfiguracije cache u array ili stvarni, isto db, i drugi resursi perzistenti

------------------
//enviroment variables 
//https://scotch.io/tutorials/understanding-laravel-environment-variables

//cemu sluze
1. to extract sensitive credentials from their source code 
2. to use different configuration variables based on their working environment

---------
//https://laravel.com/docs/5.3/configuration

//citanje, pisanje, eto ti timezona
$value = config('app.timezone');
config(['app.timezone' => 'America/Chicago']);

//.env file 
//u .env fajlu su samo naslovi koji ne idu u git, a citaju se u aplikaciji, kodu, za konfiguracije
//isto i passwordi
 variables listed in this file will be loaded into the $_ENV PHP super-global 
 'debug' => env('APP_DEBUG', $default_value)// za citanje env() fja
 
 // eto app env environment
 The current application environment is determined via the APP_ENV variable from your .env file
 
 //citanje u kodu
$environment = App::environment();
if (App::environment('local')) {   // The environment is local }
if (App::environment('local', 'staging')) 

//trenutno
APP_ENV=local
APP_DEBUG=true

trebalo bi kad postavis APP_ENV da sam promeni i od baze, stablo, ali izgleda odvojeno ide sve

//caching config
//za produkciju, spaja sve fajlove iz config foldera u 1 fajl
php artisan config:cache //ksiraj config folder
//za testing i local
php artisan config:clear //izbrisi config kesh 

//maintanance mod templejt
resources/views/errors/503.blade.php
 
-----------------------
//https://laravel.com/docs/5.3/testing

kad pokrenes test APP_ENV se postavlja na 'testing', rezervisana rec
<env name="APP_ENV" value="testing"/> //u phpunit.xml

testing environment variables may be configured in the phpunit.xml file
obrisi config cache config:clear pre pokretanja testova, da se ucitava aktuelni config
php artisan make:test UserTest

--------------------------
//http://laravel-recipes.com/recipes/27/environment-specific-configurations
vec ima fasada koja cita ceo config folder
Config::get('app.timezone');
------------------------
neteans debug script i test samo desni klik debug file, inace tesko, da se ne zaboravi


--------------------------
//How to Use Laravel Config Files //odlican
//https://scotch.io/tutorials/how-to-use-laravel-config-files

Use Laravel configuration files to keep settings out of your main application code.
solid, razdvajanje settings od koda

//MOZES DA UPISUJES U CONFIG, a dal ce da sacuva?
boza
//get
Config::get('fajl.kljuc.podkljuc') ili config('fajl.kljuc.podkljuc')
//set
Config::set('fajl.kljuc.podkljuc', $val) ili config('fajl.kljuc.podkljuc', $val)

--------------------------
//How to Use Environment Variables
//https://scotch.io/tutorials/how-to-use-environment-variables

JESU SISTEMSKE PROMENLJIVE IZ WIN, TO JESTE TO, 

//linux
//set
export DB_PASSWORD="25v3(:/9CsWHA"
//citanje
echo $DB_PASSWORD
jeste scope OS, zivi koliko instanca procesa konzole
we lose the variable as soon as we close the terminal 

//za stalno 
u .bashrc fajl u home folderu tj ~, upises ovo odozgo
include podfajl bash_exports
if [ -f $HOME/.bash_exports ]; then
    . $HOME/.bash_exports
fi

//windows
system su za sve usere, a user samo za tog

//citanje u cmd
echo %NEW_VARIABLE_NAME%
    
ne rece kako set za stalno u win?

//php
//set
$_ENV['VARIABLE_NAME'] = 'super sentitive key';
putenv('KEY', 'VALUE');//isto
//citanje
echo $_ENV['PATH'];
echo getenv('PATH');


//DotEnv
ovo u laravel nije izmisljeno nego paket DotEnv
//ovako se linkuje svaki composer paket
require __DIR__ . '/vendor/autoload.php';
//u konstruktor ide folder ili fajl
$dotenv = new Dotenv\Dotenv(__DIR__);

//citanje
env() u laravel je odavde

//upis
$dotenv = new Dotenv\Dotenv(__DIR__, 'new-config-file-name');
$dotenv->overload();



        
        
        
        