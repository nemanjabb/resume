<?php


/**
* Laravel notes from official v5.x docs. Laravel and Angular integration and starter repositories. Autumn 2015.
*
*
*
*   References:
*	https://laravel.com/docs/5.2
*	https://scotch.io/tutorials/build-a-time-tracker-with-laravel-5-and-angularjs-part-1
*	http://www.sitepoint.com/flexible-and-easily-maintainable-laravel-angular-material-apps/
*	https://github.com/jadjoubran/laravel5-angular-material-starter
*	https://github.com/Zemke/starter-laravel-angular
*	https://en.wikipedia.org/wiki/GRASP_%28object-oriented_design%29
*	https://en.wikipedia.org/wiki/Command_pattern
*	Architecture and software design .pdf course material, Dragan Stojanovic
*
*
*
*/

---------------------------
---------------------------
//rad

.env fajl u root, db adresa, sifra... ne vidi se fajl u netbeans iz nekog razloga
za bower mora git u path
kako da odvojis njegov git od tvoga, njegov je sad composer i bower, tvoje git //samo stranice treba da dodajem
za dev dovoljno sqlite u app.php, ili lokalni ili vagrant mysql

------------------
//sve objasnjeno, bez ovoa nista
http://www.sitepoint.com/flexible-and-easily-maintainable-laravel-angular-material-apps/
//jeste za velike projekte, ok
glavni zahtev je da framework bude lak, puno stvari samo
koristi pola ekrana u windowsu za uporedno
logicno je da je lako jer je napravljeno da olaksa
------------------

composer.json, packages.json, bower.json dodajes u tvoj git, a foldere koje dovlace u gitignore
folder by feature - kad je nesto pokvareno problem je u jednom folderu

u gulpfile.js vidis gde ide spojeni css i js, public/js/vendor.js, vendor je spojeno i min sve
u angularu kontroler expozuje fje i promenljive koje mogu da se zovu iz html-a, boza
ispod angular folder idu folderi app, services, config, directives itd... logicno
angular/app se kopira u public, deploy ili bild prakticno, /public je /dist laravela
elixir je javascript tj node
Elixir.extend('bower', function(jsOutputFile,...) factory construktor izvedene prakticno...
bower install angular#1 --save //Don’t forget the --save flag because we want this to be saved in bower.json
laravel html templejti su u resources/views/index.blade.php

samo stranice treba da dodajem// odeljak Bridging Laravel & Angular

//Setting up Restangular podnaslov
angular kontroleri za svaku stranicu su u folderu app/feature angular.module('app.controllers').controller('DialogsCtrl',...
u main.js linkovao sve module u app obj
u routes.php pogledas sta ima od stranica, sta je sajt u stvari
u wellcomeController sample() fja je rest api endpoit primer, nema rutu taj kontroler, ne poziva se
zasto "use strict" u ang?
-----------
return response()->api($data); u controller je macro za json() u ResponseMacroServiceProvider boot()
samo je vratio json od polja, nema rest api siru logiku

resource contrller je makro za 7 kontrolera (create, store, show, edit, update, destroy)
ima dingo/api u vendor za rest api vec, ovo da procitam https://github.com/dingo/api/wiki

Im in dilemma about versioning and dependencies, if I also change some files in lib i find it unpractical to put whole lib in git, but what is alternative?
so for example this https://github.com/jadjoubran/laravel5-angular-material-starter is meant to be used as fork, as you will change a lot in app or angular folder making your application, so this whole project cant be included as dependency later?
//ovo
tacno tako, koristis kao fork, jer ovaj repozit sadrzi samo config, db, zapoceti app fajlove, ne sadrzi laravel, ni js biblioteke, bower itd...
to nije dependancy vec zapocet projekat, ne dodaje se kao dependancy, ima i git vec//eto poenta jasno
na githubu su projekti, a ne zavisnosti, zavisnosti su na packages org

gledaj issues na github, ima mnogo blizih informacija i pogleda

//to
procitam dingo api, youtube yeoman, koji php i ang testovi, rastmaci jasno koji testovi i frameworci

transformer - prebaci objekat u polje za serijalizaciju
relationship eager loading - povuce zavisne tipove
advanced usage - custom serializer...
custom tranf layer - implem svoju transform fju, od obj do polja

authentication

//pocetno podesav - konfiguracija, ekvivalentno, bootstrap fajl i config su ekvivalentni
//ko papagaj ponavlja
config file
'jwt' => 'Dingo\Api\Auth\Provider\JWT'
bootstrap file
app('Dingo\Api\Auth\Auth')->extend('jwt', function ($app) {
   return new Dingo\Api\Auth\Provider\JWT($app['Tymon\JWTAuth\JWTAuth']);
});

use trait; (u klasi, ne namespace) - visestruko nasledjivanje, imas ga u this
clojure je samo fja tu odmah napisana i pozvana, inace bi bila norm definisana, arg je fja i to je to

-------------------
//pogledaj
procitam dingo api, youtube yeoman, koji php i ang testovi, rastmaci jasno koji testovi i frameworci
napravi netbeans u vbox za seljenje
api ili krace interfejs
provider - stategy, pogledaj, uvek koristi za config-boot
sta podrazumeva pod middleware
github iscitas sve, issues, dokumentacija, video previse pocetnicki, jedan repozit radi i uci
moras da iscitas pre rada ili ces budziti neoptimalno, za sta vec postoje paketi
docker?
-----------------
postman, curl, koji http klijent za testiranje api?
api mozes da versionises u linku ili u accept hederu
docblocks u API controllers, resource, sva dokumentacija za api je u kontrolerima
@Request, @Response...

@Post("/")
@Versions({"v1"})
@Request({"username": "foo", "password": "bar"})
@Response(200, body={"id": 10, "username": "foo"})

artisan cli komande $ php artisan api:routes
malo ih ima routes, cache i docs samo

---------------------
material gui ikonice itd na adroid 5, angular material port na angular
poredi foldere i raspored ova dva gita
angular style guide mgechev za raspored foldera i testove...
sve je jasno i lako, samo 1 po jedan da se prodju - addi osmani js patterni
----------------------
//11. frameworks and middleware
mom - messaging je 1 na 1, asinhrono, moze perzistentno, transkacije - vise poruka ili ni jedna
publish subscribe je 1 na n, n na n, ... //vazna poenta
publisher ne zna ko se sve pretplatio, subscriber ne zna ko je poslao event... arg..
ejb kontejner obezbedjuje: transakcije, security, directory (imena), threading, connection pooling
message broker - message transformation, rules engine, intelligent routing, adapters
------------------
13. arhitektura enterprise aplikacija
1. prezentacija
2. domenska logika:
					transaction script
					domain model (service layer opciono, objekti logike u data maper patternu prema view)
								active record
								data mapper
					table module
3. izvori podataka

transaction script - jedna procedura (logika sa upisom u bazu) za jednu rutu, sve u (dugacak) kontroler prakticno, izolovano
lose dupliranje

//domain model (entity ili active record objekat)
uml: zavisnost - koristi taj tip negde, agregacija - klasa ima atribut tog tipa
logika primera - registrovanje prihoda za ugovore i proizvode
Obrazac Strategy je efikasan kada treba da se refaktorise proceduralni kod koji sadrzi mnogo uslovnih naredbi i grananja.
zahtev prosledjuje od objekta do objekta sve dok ne stigne do objekta koji je kvalifikovan da obradi zahtev ( i  svi oop sistemi)
sekvencionalni dijagrami laki i jasni (izomorfan sa aktivity dijagr)
nadji kod primera
//table module
kao active record ali jedna instanca
---------------
//https://en.wikipedia.org/wiki/GRASP_%28object-oriented_design%29
DI smanjuje coupling
inversion of control zbog testabilnosti i mokinga, i nema coplinga
--------------
//service layer - jesu di ang larav servisi, layer (interfejs - api) za vise viewa, logika
logika aplikacije
1. operacioni skriptovi (nad active recordom ili orm umesto sql), 2. domenska fasada (poziva fje iz domain modela)
servisi su fje krupne granulac - malobrojni, organizov u servisne klase imena KlasaService(), identifikuju se iz slucajeva koriscenja
ove fje se pozivaju iz vise view-a, ako 1 view service layer nepotreban
interfejs je ogranicenje da nesto ima te fje
naveden primer RecognitionService i ApplicationService samo razdvojen interfejs i implementac, trivijalno
EJB poseduje ugradjenu podrsku za transakcije upravljane od strane kontejnera (container-managed transactions) - imaju rollback
fasada - skupljeni (grupisani) pozivi, postojeca implementacija na drugom mestu

transakcija - objekat ima rollback metodu i to je to - pros, dobro uoceno vazna poenta

------------------
pogledaj u kom slucaju je mgechev primenio publish-subscribe u ang? to je samo commandbus sa komandama da nema direktnog poziva metoda zbog couplinga
publish subscribe je messaging 1:n, n:n - vazna poenta
-------------------
tortoise git
ppogledaj yt kako workflow na githubu, issues itd...
-------------------
//https://en.wikipedia.org/wiki/Command_pattern objasnjeno dobit, svrha i slucajevi
komanda - command pattern + publish subscribe
GUI buttons and menu items, Macro recording, Mobile Code, Multi-level undo, Networking, Parallel Processing, Progress bars, Thread pools, Transactional behavior, Wizards 
enkapsulacija akcije u objekat i diskretizacija stanja aplikacije, transakcije, istorija, stanja, definises up i down komandu
//to je to, odlicno
command bus - asinhroni command pattern, asinhroni red izmedju invokera i command klase (command klasa je asinhrona) //http://www.codeproject.com/KB/architecture/DistributedCommandPattern/Figure4_CommandBus.jpg
event je red + proizvodjac potrosac //odlicno
functional interface java 8 - tip koji koristis ako hoces da prosldis lambdu u neku fju kao arg
objektna arhitektura jeste objekti prosledjuju poruke jedan drugome do onog koji je zaduzen da je obradi, obradjivac (single responsibility) plus mreza koja delegira i rutira
command klasa je samo def interfejsa, gde god se nasledjuje nesto to je samo interfejs, (mozda za za vise implem)
pise sve samo treba da se pogleda na pravo mesto , progugla, wiki u ovom slucaju pise svrha
-------------------
// https://scotch.io/tutorials/build-a-time-tracker-with-laravel-5-and-angularjs-part-1

ui-bootstrap kad koristis bootstrap sa angularom, jer ima interfejs na direktivama
googlaj gotove direktive za sve, tabele npr, jer imaju ang interfejs
controller as, this mesto scope (scope=view model), vm=this, pa u html vm.nesto

//service fja (klasa je module pattern doslovno
.factory('time', time);
function time($resource) {... return{public fje i attr}}
laravel je samo db kontroler, validacija?

i servis fje mogu da se pozivaju iz templatea
sto preciznije pitanje precizniji odgovor, i obrnuto losiji

//2. deo laravel strana
mysql user pass ide u .env
Route::get('/', function() {   return view('index'); //ruta za staticki html fajl
nadalje tupi crud obican

ne koristi resource nego samo kontrolere koje koristis
------------------
laravel middlewre su filteri, trivijal, prosledis ih u rutu
razlika izm provider i service, provider externe lib umotane, servisi su tvoji
models idu u /app direktno

idem okolo duze nego sto radim
tacno sve po reci ima u yt, precizno pitanje precizan odgovor
------------------
//2016 rad
//https://github.com/jadjoubran/laravel5-angular-material-starter/issues/114
bootstrap umesto material
bootstrap ui koristi angular umesto jquery, plus css
sav visak na kraju lako pobrises

//njegovo
remove angular-material dependencies from bower.json, bower update//makne js
edit main.js and remove 'ngMaterial' module
delete dialog.js and toast.js services//2 servisa
remove all features from /angular/app//sve foldere, neka ih gledas feature per folder

prebacivanje iz bower_components u vendor.js je u tasks/bower.task.js 2 gulp taska
koristi session manager za firefox tabove
--------------------
// https://github.com/Zemke/starter-laravel-angular
sta koristi za api?
sve po redu osim php -S localhost:8080 -t public/
ide php artisan serve //otvara na  http://localhost:8000 umesto 8080
da znas gde da pogledas da vidis sta se sve od lib i provajdera koristi u aplikaciji?
lib vidis u composer.json
provideri se vide u app.php
u rutama entry pointi
kako kontroleri vracaju respnse kad vr modele?
cemu sluze custom request/response?
procitaj controller u docs za rest
--------------------------
magicna metoda - finder u orm bese
--------------------------
odredi slozenost (koliko je bistar vec taj sto to koristi ili je napravio) i obim kolicinu posla
uvek iskopiraj postojece, nikad ne izmisljaj ako ne moras da izmisljas
-----------------------------
asoc polje u sql tabelu refer i pretrazivanje, ogranicenje 1 torka na tabelu? //mysql limit table to one row
3 zavisne pormenljive init pa promeni sa if
da li vec ima in_list sa datumom

//singleton na nivou orm-a
//dodas where id=1 u svim select, insert update
create table users (id int not null primary key auto_increment, name varchar(20), is_admin bool default 0);
set is_admin to 1 
select id,name from users where is_admin;

eventi u singlethreaded runtime (jeziku), treba threads za proizv potrosac
kako asinhrono na singlethread i eventi - javascript vrti se u message loop, idle

--------------
iznad svakog if napisi telo hendluje koji slucaj
php nema lokalne fje, najblize anonimne i $myfunc = create_function(..., ...);//c# nema, js nije oop jezik, neobicno za oop
gde koristis promenlj ili fje da budu kratke i lako objasnjive, helper fje 
kohezija - ne povezuj (trpaj) odvojene funkcionaln, povecavas kompleksnost i zavisnost
throw iz try da uhv odmah, moze
//fora - radi direktno sa bazom bez promenljivih da se kesiraju i komplikuju
//netbeans php region
// <editor-fold defaultstate="collapsed" desc="user-description">
  ...any code...
// </editor-fold>
laravel ti daje framew i za rad sa bazom, direkt iz objekata, nevezano za http
singleton eloquent model pitaj
----------------
bukvalno sve postoji samo treba da se seti, masta da se izgugla
git portable
----------------
pravi minimalne referenc tabele sa kljucevima, pa ref opisna tabela sa istim pk sa opcionim poljima
u dokumentacijama gledaj kod, a ne tekst, on je zbog koda
//eto sta je custom request u app/Http/Requests/
Form requests are custom request classes that contain validation logic. To create a form request class, use the make:request
--------------------
lumen za api, laravel za sajtove
--------------------
//sheduler https://laravel.com/docs/master/scheduling , jedan od servisa //trivijal utility
pogledaj koje sve servise imas na raspolaganju
uradis 1 cron na nivou linuxa pa laravel rasporedi dalje * * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
your scheduled tasks in the schedule method of the App\Console\Kernel class
$schedule->call(function () { DB::table('recent_users')->delete(); })->daily();//fju, artisan comandu, os cmd naredbu, program, fajl
$schedule->command('emails:send --force')->daily();
$schedule->exec('node /home/forge/script.js')->daily();
->when(function () {return true} sledece izvrsavanje zavisi od uspeha fje return, svi when &&
minimizuj broj poziva api, a da radi
->withoutOverlapping(); sekvenc 1 po 1
->appendOutputTo($filePath); log
->before(function () {... ->after(function () // filteri pajpovi
->pingBefore($url) guzzle
----------------------------
pogledaj Continuous Integration integracija bitbucket i digitalocean, deployment
override i handler razlike? inversion of control - obrnuti poziv, callback, implementiras metodu koju ce framework da zove, override sinhron, event ima queuing
sinhron = sekvencijalan, blokirajuci
ili delegat clan, a event je delegat + queuing, (queuing = asinhronost)
-----------------------
//cache https://laravel.com/docs/master/cache
The cache configuration is located at config/cache.php
file driver default, db sa tabelom id, value, expiration, in memory memcache i redis //to
memcache treba Memcached PECL package 
redis treba predis/predis package (~1.0)
TCP/IP ili na jednom os-u
-------
Cache:: fasada
$value = Cache::store('file')->get('foo');
Cache::store('redis')->put('bar', 'baz', 10);	
get() null na neuspeh
$value = Cache::get('key', 'default ili fja');//default na neuspeh, isto Cache::remember() za save
if (Cache::has('key')) {
Cache::put('key', 'value', $expiresAt); Cache::forever('')
Cache::forget('key');
Cache::flush(); //brise ceo kes
Cache tags allow you to tag related items in the cache and then flush all cached values
--------------
key value skladiste db sto si zamisljao ti je bukvalno cache
----------------
class CacheServiceProvider extends ServiceProvider //registrovanje novog drivera, mongo
{
    public function boot()
    {
        Cache::extend('mongo', function($app) { //"mongo" se poklapa sa driver option in the config/cache.php 
            return Cache::repository(new MongoStore);
        });
    }
Cache::extend could be done also in the boot method of the default App\Providers\AppServiceProvider
Illuminate\Contracts\Cache\Store interfejs sa metodama koje custom cache driver mora da impl, sve metode koje postoje nad cache prakticno
--------------
cache operac emituju evente na koje mozes da se kacis
--------
provider laravel servisi i u NET provideri, strategy pattern
redis i memcache su samo in memory drajveri
------------------------------
laravel cheatshet, sa cim god da radis izgooglaj cheatsheet
http://dfg.gd/blog/decoupling-your-code-in-laravel-using-repositiories-and-services
strategy pattern za enkapsulir slozenih if-ova
----------------------------------
----------------------------------
//zemke rad pokretan
composer install dev, kad uradis na produkciji nece da dovlaci sto je sa dev
enviroment dev ?

gulp laravel elixir nije bio instaliran

npm cache clean da pobrise, pa npm install kao admin, gulp && gulp watch
composer install nije generisao autoload.php u vendor

-----------
mora vhost u xampp jer gadja cist domen bez /nesto/nesto u css i js

//C:\xampp\apache\conf\extra\httpd-vhosts.conf // pa restart xampp apache

NameVirtualHost *:80

<VirtualHost *:80>
  DocumentRoot "D:/xampp/htdocs"
  ServerName localhost
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "C:/xampp/htdocs/1/zemke/public"
    ServerName dev.app
    ServerAlias www.dev.app
    <Directory "C:/xampp/htdocs/1/zemke/public">
        DirectoryIndex index.php
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>

//u etc/hosts, moze samo domen bez staze, staze hendluje apache
//C:\Windows\System32\drivers\etc\hosts
127.0.0.1  localhost dev.app
::1        localhost dev.app

zemke, stavio sav bootstrap, angular, css u git, nije koristio bower, bzvz
-------------
na github imas laku navigaciju kroz diffove izmedju bilo koje dva komita (i nesusedna), i cele verzije kroz istoriju
------------
ubacim migracije, seeder, validator u model, go_back() u model za sad, logika gde ide? servisi?, sutra procitam monolog logger, modeli za log templejte
mozda queuing za vise listi, sto pre da dobijem funkc koja radi za srpsku listu, sheduler mora cron na linux, vagrant, windows mozda ima rogooglaj
--------------
php artisan migrate:make add_votes_to_user_table --table=users //gadjas tabelu
php artisan migrate:make foo --path=app/migrations //ili fajl
----------------
za reset migracije mora da se makne fajl iz composer autoload
//composer dumpautoload and if you have some class/file not found error, check if vendor/composer/autoload_classmap.php
//http://stackoverflow.com/questions/30517098/how-to-delete-a-model-using-php-artisan
--------------------------
--------------------------
//logging and errors https://laravel.com/docs/5.1/errors
.env file APP_DEBUG var upisuje u config/app.php
------
Exception Handler u App\Exceptions\Handler
report salje, render pravi http responce i vraca browseru
The report method is used to log exceptions or send them to an external service like BugSnag. 
The render method is responsible for converting a given exception into an HTTP response that should be sent back to the browser.
-------
$dontReport ignorise tipove exceptiona
resources/views/errors/404.blade.php view koji se podize na taj broj
---------
Log::info($error); fasada, 7 kom: emergency, alert, critical, error, warning, notice, info and debug.
//detalji https://github.com/Seldaek/monolog
JsonFormatter
RotatingFileHandler: Logs records to a file and creates one logfile per day. It will also delete files older than $maxFiles. 
Processors - filter
//custom handler, model pa njega u custom handler, log tabela
//https://github.com/Seldaek/monolog/blob/master/doc/04-extending.md
mysql handler moze, 1 klasa
//https://github.com/waza-ari/monolog-mysql
------------------------------
------------------------------
//validacija https://laravel.com/docs/5.1/validation
validacija, pa exception, pa
In the case of a traditional HTTP request, a redirect response will be generated, while a JSON response will be sent for AJAX requests
validacija objekta 'author.name' => 'required',
Laravel will automatically redirect the user back to their previous location
$errors variable is always defined
------
$this->validate($request, [
    'title' => 'required|unique:posts|max:255',
    'author.name' => 'required',
    'author.description' => 'required',
]);
$errors popunjen

ili fasada

 $validator = Validator::make($request->all(), [
		'title' => 'required|unique:posts|max:255',
		'body' => 'required',
	]);

	if ($validator->fails()) {
		return redirect('post/create')
					->withErrors($validator)
					->withInput();
	}
-------
//form requests klasa cela prica
Form requests are custom request classes that contain validation logic.
All you need to do is type-hint the request on your controller method. * @param  StoreBlogPostRequest  $request
public function authorize() * @return bool
-------
$messages = $validator->errors(); kolekcija gresaka sa svojim geterima
resources/lang/xx/validation.php za jezike razlicite
---------
list of all available validation rules 
digits_between:min,max user_id i list_id npr
'email' => 'unique:tabela,email_address' // bindovano na tabelu// default database connection
'email' => 'unique:connection.users,email_address' //konekcija.tabela
sometimes - opciona slozenija logika, helper jquery
------------
registering custom validation rules Validator::extend('foo', 'FooValidator@validate');
Validator::replacer('foo' //za :replace placeholdere tvoje
Validator::extendImplicit() dodaje required
------------------------------
------------------------------
//paginacija https://laravel.com/docs/5.2/pagination
Paginating Query Builder Results  $users = DB::table('users')->paginate(15);
Paginating Eloquent Results $users = App\User::paginate(15);
------
Paginator, simplePaginate - prev next, ne zna koliko ima
LengthAwarePaginator , paginate - znaju za zadnji
------
Manually Creating A Paginator - prima array, ne radi nad db direktno
-------
paginator instances are iterators 
paginate or simplePaginate methods on a query builder or Eloquent query, you will receive a paginator instance LengthAwarePaginator or Paginator
-----
Displaying Results In A View
{!! $users->links() !!}  Bootstrap CSS 
{!! $users->appends(['sort' => 'votes'])->links() !!} query str
{!! $users->fragment('foo')->links() !!} hash str
-------
toJson().paginate()
The JSON from the paginator will include meta information such as total, current_page, last_page, and more. The actual result objects will be available via the data key
--------------------------
u git komit opisujes sta radi taj diff, posle lako vidis
testing i autorizacija dobri clanci
---------------------------------
---------------------------------
//testing https://laravel.com/docs/5.2/testing

tests/ExampleTest.php 
phpunit u cmd u folderu
php artisan make:test UserTest //test klasu i autoload
daje class UserTest extends TestCase{...
public function testBasicExample(){	$this->visit('/')->see('Laravel 5')	 ->dontSee('Rails');} //regex nad response objektom
unit testing - testiranje klasa
->click('About Us')
->type('Taylor', 'name')->check('terms')//forma submit
//json
->seeJson(['created' => true,]);// seeJson() kljuc i val postoji bilo gde u jsonu
->seeJsonEquals([ 	//i struktura, ne bilo gde
->seeJsonStructure([ //samo struktura
//sesija mockup
$this->withSession(['foo' => 'bar'])
$this->actingAs($user) //$user je model
//disable middleware
use WithoutMiddleware; // disable all middleware for the test class:
$this->withoutMiddleware(); //samo za jednu test fju
$response = $this->call('GET', '/');//vadi ceo response u test, kao kontroler
---------
Laravel additional assertion methods //asserti
---------
//Databases
$this->seeInDatabase('users', ['email' => 'sally@example.com']);//users tabela
reset db posle svakog testa da ne ostanu inserti iz proslog testa
use DatabaseMigrations;//trait rolbackuje
use DatabaseTransactions; //isto //trait radi identicno kao extend i zove default base() constr//samo druga sintaksa
-------
Model Factories // kreira dummy podat u db
database/factories/ModelFactory.php
//Faker servis//definicije
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
defineAs() druga pravila za isti model
raw() isto samo nasledi
//kreiranje modela
$user = factory(App\User::class)->make();//kreira samo, bez insert u db, ->save() je insert
$user = factory(App\User::class)->create(['name' => 'Abigail',]);//override svoje polje plus insert db
$users = factory(App\User::class, 3)->create()->each(function($u) {$u->posts()->save(factory(App\Post::class)->make());});//ubaci ugnjezd referencir model
-------
//events
$this->expectsEvents(App\Events\UserRegistered::class);//disablira hendlere za event, na db insert npr
$this->withoutEvents();//iskljuci sve evente potpuno
//jobs mocking
expectsJobs() jobs are dispatched, but not executed
//mock facade - DI objekata, servisa
Cache::shouldReceive('get')->once()->with('key')->andReturn('value');//shouldReceive("get") menja stvarnu Cache::get('key');


--------------------------
--------------------------


//Exception Handler u App\Exceptions\Handler 1 hendler za sve exceptione
The report method is used to log exceptions or send them to an external service like BugSnag, Sentry. (web sajt sa graficima i stat gresaka)
sam larav vec ima sto sam zamislio

eto ga linq https://laravel.com/docs/5.1/collections#method-each
$collection = collect($response);
$collection = $collection->each(function ($item, $key) {}
            
fasada je samo sintaksa za servis koji je iza
scope i stanje servisa?          
service je singleton

imenovana lokalna fja u php: 	you can assign closures into a locally named variable
$localFunc = function($foo) { return $foo+1; }; $localFunc(2);

search u github repozitorijum skroz koristan

koriscenje alata, mnogo manje lutanja, znas sta trazis i kako da nadjes i kako radi
----------------
//sve sto treba za fasadu
<?php namespace Acme\Facades;
class UserLog extends Facade {
    protected static function getFacadeAccessor() { return 'userlog'; }
}
---
app/config/app.php:  
'aliases' => array(
    ...
    'UserLog'			  => 'Acme\Facades\UserLog',
),
------------------------
//kad napravis nov folder sa klasama//bootstrap je folder, namespace
composer dumpautoload
"autoload": {
        "psr-4": {
                "Bootstrap\\": "bootstrap/"
        }
------------------
//logging monolog
report() u App\Exceptions\Handler ga prosledjuje do osnovne klase koja ga default loguje
vec to radi, samo ako hoces da overajdujes da radi drugo, ili filtriras exceptione   if ($e instanceof CustomException) {
$dontReport[] ignorises except koji se precesto javljaju
http exceptions  abort(404); messaging

//slozenije https://laracasts.com/discuss/channels/general-discussion/advance-logging-with-laravel-and-monolog
ili overide configureHandlers() u class ConfigureLogging extends BaseConfigureLogging{} pa nju bootas u App/Console/Kernal.php i app/Http/Kernal.php $bootstrappers 

//Error Detail 
debug config/app.php default APP_DEBUG iz .env

//log mode
config/app.php - 'log' => 'daily' - single, daily, syslog and errorlog 
 
 
//prostije u bootstrap/app.php docs https://laravel.com/docs/5.1/errors#configuration
$app->configureMonologUsing(function($monolog) {    $monolog->pushHandler(...); }

Log::info('Showing user profile for user: '.$id);
Log::info('User failed to login.', ['id' => $user->id]);
Log::emergency($error);Log::alert($error);... emergency, alert, critical, error, warning, notice, info and debug.

monolog html formater, vec ima u njemu default, mejl hendler...

uvek sve izmisljeno, cim izmisljas radis pogresno
http://laravel-tricks.com/tricks/monolog-for-custom-logging
http://www.sitepoint.com/logging-with-monolog-from-devtools-to-slack/

sitepoint odlican sajt

use namespace su samo za staze, da ne pises cele
monolog brings debug to new level

krastavac: http://cdn.makeuseof.com/wp-content/uploads/2013/05/swivel-arm.jpg ?
sisve	krastavac: Combine that, throw out any ideas about what would work and how much your wall could handle, and attack a http://ep.yimg.com/ay/eca/adjustable-multiple-monitor-stand-ehmtr-6x-6.jpg on that arm.

ako previse saobracaja samo rt-ove iz liste

jos sitniji log, reconfig, uslov rt, lista da se napravi, i moze da se pusta
top 10 sa user timelinea//ne, 2 iterac
facebook od usertimeline
quote tvit dana, pin twit srede, cetvrtka...//mora usertimeline za to

Eloquent doesnt support composite primary keys.
truncate brise rowove, resetuje autoincrem, ostavlja shemu tj tabelu
delete samo brise
drop unistava tabelu, shemu
If you wish to truncate the entire table, which will remove all rows and reset the auto-incrementing ID to zero, you may use the truncate method:	
	
	
izgleda bben-ovi tvitovi moraju da budu u listi// ne mora, samo vrednosti vece manje su bile pogresne	
	
I updated laravel but the filesystem completely screwed up	
pisi snipete koji olaksavaju debagiranje, jasno vidis i razumes
	
push to multiple repo proguglaj	
tabela statistika br rt-ovanih, br predjenih, na svako pokretanje, obrtanje petlje, dnevno	
blacklist useri u listi georgiev itd	
config ne sme da bude staticki, tj sti u svim instancama	
collections imaju api twitter
broj fav mozes da skaliras na broj pratilaca

\A::func1() means class A from global scope
A::func1() means class A from current scope
---------------------------------------
---------------------------------------
messaging, event loop, observer, watch - samo taj 1 objekat vrti loop kroz listu imena evenata i poziva listu hendler fja za taj kljuc - event, tako se ne gubi poruka
kako event ulazi  tu fju sa loopom
---------------------

static ili polje da li je isti u svim instancama ili razlicit
---------------------
---------------------
//Service providers https://laravel.com/docs/master/providers
extend the Illuminate\Support\ServiceProvider class. 
This abstract class requires that you define at least one method on your provider: register()
------
You should never attempt to register any event listeners, routes, or any other piece of functionality within the register method. 
Otherwise, you may accidently use a service that is provided by a service provider which has not loaded yet//mesto gde ne rade fasade
u boot() imas DI objekte kao args, mozes da koristis sve servise jer se poziva na kraju
kad boot() a kad register() ?
Register any application services.
Bootstrap any application services. 
php artisan make:provider RiakServiceProvider

protected $defer = true;//lazy load, instancira ne na boot nego tek kad se  upotrebi negde
 provides("prviServis","drugi"), binding names koje servise ta provider klasa registruje
 The provides method returns the service container bindings that the provider registers
---------------------------
---------------------------
//servisi sustina frejmworka i velikih aplikacija
service container https://laravel.com/docs/master/container
Since the service is injected, we are able to easily swap it out with another implementation. 
We are also able to easily "mock", or create a dummy implementation of the mailer when testing our application.
-----
binding() definises sta kontejner da vrati kad se zatrazi klasa
$this->app->bind, $this->app->singleton(), $this->app->instance()//u register()
Binding Interfaces To Implementations
$this->app->bind('App\Contracts\InterfaceEventPusher', 'App\Services\KlasaRedisEventPusher');//defin koju klasu za interfejs
//kad trazi to daj mu to
$this->app->when('App\Handlers\Commands\CreateOrderHandler')
          ->needs('App\Contracts\EventPusher')
          ->give('App\Services\PubNubEventPusher');
tag - grupe koje kako da resolvuje
-----------------
instanciranje
$fooBar = $this->app->make('FooBar');
$fooBar = $this->app['FooBar'];
navodjenjem tipa u konstruktoru
public function __construct(UserRepository $users){}
--------
instanciranje emituje evente ako ti trebaju
$this->app->resolving(FooBar::class, function (FooBar $fooBar, $app) {
    // Called when container resolves objects of type "FooBar"...
});
----------------------------
----------------------------
//fasade https://laravel.com/docs/master/facades
class Cache extends Facade{
    protected static function getFacadeAccessor() { return 'cache1'; }// cache1 je name of a service container binding
}
app/config/app.php:  
'aliases' => array(
    ...
    'cache1'			  => 'Acme\Facades\Cache1',
),
fasada je getService($serviceBindingName)
----------------------------
----------------------------
//servisi ti daju 2 stvari
kontrolisu koliko instanci je u opticaju u aplikaciji
resavaju linkovanje fajlova i namespace klasa
prenose se kao args, mogu da se mokuju
kad koristis klasu na puno mesta

$this->app->bind('Acme\Api\Services\Interfaces\ApiServiceInterface', function ($app) {//iz $app vadis ostale servise ako ti trebaju
        return new ApiService($app['Illuminate\Http\Request']);
});
-----------------------
nista - napravis klasu koju oces
napravis provider klasu i iz register metode vratis instancu tvoje klase, i to je to
http://www.n0impossible.com/article/how-to-create-facade-on-laravel-51
-----------------------
return iz fajla

// file1.php - returns an array
return array(
    'key1' => 'value1',
    'key2' => 'value2',
);
?>
use File;
// Fetching the array of the file above
$rez = File::getRequire(__DIR__.'/file1.php');
--------------------------
testing docs, prost clanak
--------------------------
gde sta ide folderi i klase https://mattstauffer.co/blog/laravel-5.0-directory-structure-and-namespace
---------------------------------



























