# hatch - poetry alternative 
https://github.com/pypa/hatch
pypa - Python Packaging Authority, napravili pip i viryualenv
-----------
# full stack, next.js i fastApi, cookiecutter, updated tiangolo starter
https://github.com/mongodb-labs/full-stack-fastapi-mongodb
-------------------------
# Why You Should Use Pydantic in 2024 | Tutorial - ArjanCodes
# https://www.youtube.com/watch?v=502XOB0u8OY
# https://github.com/ArjanCodes/examples/tree/main/2024/pydantic_refresh
kao zod - typing + validacija
serializacija
----
# koristi klase i nasledjivanje
# zod schema
class User(BaseModel):
dto klase, kao js object literal
-------
# validacija
# za jedan prop
@field_validator("name")
@classmethod # static fja, prima cls arg
def validate_name(cls, v: str) -> str:
----
# zod refine, ima ceo kontekst
@model_validator(mode="before")
@classmethod
def validate_user(cls, v: dict[str, Any]) -> dict[str, Any]:
---------
# pozivanje validacije explicitno
try:
    user = User.model_validate(data)
    print(user)
except ValidationError as e:
    print("User is invalid:")
    print(e)
----------------
# serializacija
# jedan prop
@field_serializer("role", when_used="json")
@classmethod
def serialize_role(cls, v) -> str:
    return v.name
# object
@model_serializer(mode="wrap", when_used="json")
def serialize_user(self, serializer, info) -> dict[str, Any]:
------------------
# fastApi
app = FastAPI()
# model koji ce se korisititi u endpoints, input or output arg
# zod schema
class User(BaseModel):
    ...
# output
@app.get("/users", response_model=list[User])
async def get_users() -> list[User]:
    return list(User.__users__)
# input, poziva validaciju
@app.post("/users", response_model=User)
async def create_user(user: User):
    User.__users__.append(user)
    return user
---------------------------
#  This Is How You Do PROPER Exception Handling With FastAPI - ArjanCodes
# https://www.youtube.com/watch?v=7MHDDOrDx-w
# https://github.com/ArjanCodes/examples/tree/main/2024/tuesday_tips/fastapi_custom_exceptions
prouci kad radis fastApi
-----------------------
-----------------------
# analiziranje primera
# https://github.com/fastapi/full-stack-fastapi-template

# pokretanje app bez Dockera
# https://github.com/fastapi/full-stack-fastapi-template/discussions/1111
------
# debilizam, start.sh i start-reload.sh in two levels parent image, u ovoj slici
prva slika
# https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker/blob/master/docker-images/python3.10.dockerfile
druga slika
# https://github.com/tiangolo/uvicorn-gunicorn-docker/blob/master/docker-images/start.sh
ovde nije pomenuto
# https://github.com/fastapi/full-stack-fastapi-template/tree/master/backend#readme
--------------
# resolve absolute import path
from app.core.config import settings
u sys.path, tj PYTHONPATH se dodaje staza skripte koja je prva pokrenuta - main.py u fastApi
i onda se odatle racunaju apsolutni imports
-----------
# zapravo isti je folder, u kom je main.py
from app import crud
/project_root/
    main.py
    crud.py
---------------------
# kad ima ovo, onda taj fajl se ne importuje samo u python
# nego i poziva direktno u terminal ili u bash skriptama
# ima pozvanu fju u global scope main() ili drugu
if __name__ == "__main__":
    main()
-------
# python builtin package
import logging
-------
# Annotated - feature from the "typing" module
kaci metadata koje framework moze da cita, opis tipa
CurrentUser = Annotated[User, Depends(get_current_user)]
-----
# beskorisni dijagrami
# https://gitlab.com/euri10/fastapi_cheatsheet
-----
Depends se koristi u ruote handlers, za args
# Scopes - dependency lifecycles:
# isto kao i u .NET i notes/other/dependency injection.js beleskama
request-scoped, session-scoped, or application-scoped
----
# https://fastapi.tiangolo.com/tutorial/dependencies/#share-annotated-dependencies
dependable - dependency fja
ima isti potpis kao route handler (osim dekoratora) - znaci jeste middleware # bzvz
i vraca vrednost za taj argument
route handler - path operation function_, kako on zove
-----
# prvo se izdvoji u posebnu var
CurrentUser = Annotated[User, Depends(get_current_user)]
# i onda koristi u rutama
@router.get("/me", response_model=UserPublic)
def read_user_me(current_user: CurrentUser) -> Any:
------------
# database schema je u sqlModel
# backend/app/models.py
class User(UserBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid.uuid4, primary_key=True) # pk
    hashed_password: str
    items: list["Item"] = Relationship(back_populates="owner", cascade_delete=True) # fk
------
SqlModel klasa nasledjuje (kompatibilna, ima methods) od both SqlAlchemy i Pydantic
-----------------
# FastAPI & SQLModel - Database Interaction in FastAPI apps with SQLModel - BugBytes
# https://www.youtube.com/watch?v=pRYzMF04fLw
# https://github.com/bugbytes-io/fastui-sqlmodel-demo
jeste, zapravo SqlModel nasledjuje Pydantic i moze da seda umesto njega
sqlmodel.SQLModel === pydantic.BaseModel, ove dve base klase su analogne
-----
# Base model, minimalna polja, zajednicka
class UserBase(SQLModel):
    email: EmailStr = Field(unique=True, index=True, max_length=255)
    is_active: bool = True
    is_superuser: bool = False
    full_name: str | None = Field(default=None, max_length=255)
# primarni i strani kljuc (relacija) u nasledjenoj tabeli
# table=True kreira tabelu u bazi za model
class User(UserBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid.uuid4, primary_key=True)
    hashed_password: str
    items: list["Item"] = Relationship(back_populates="owner", cascade_delete=True)
----
Field() ide za ogranicenja, pk, fk, default...
-----
init_db() je migrate (i seed)
poziva se u onStart lifecycle event, unutar (async) context managera jer resurs treba da se ocisiti
---
# async generator, izvrsi i nastavi, podseti se, prouci
@asynccontextmanager 
async def lifespan(app: FastAPI): 
    init_db() 
    yield 
-------
session je persistence manager iz J2EE, data mapper pattern
i prosledjuje se u rute sa Depends(get_session)
def route_handler(...args, session: Session = Depends(get_session))
------
item = Item.model_validate(item_in, update={"owner_id": current_user.id}) # python objekat
session.add(item)
session.commit() # sacuvaj
session.refresh(item) # refresh to get db generated id # zapazi
------------------
------------------
# Building a REST API with FastAPI, Async SQLModel, and PostgreSQL - Ssali Jonathan, uganda
# https://www.youtube.com/watch?v=I8WiIXMDydw
# https://github.com/jod35/lib-api
# generator prost i jasan, upamti
def my_gnenrator_fn():
    # runs on first fn call
    yield
    # krece odavde, vidi vars iz prvog bloka
    # runs on second fn call
    yield
    # krece odavde, vidi vars iz prvog bloka
    # runs on third call
    yield
# tako ovaj onStart, onEnd event, na prvi poziv izvrsava onStart, na drugi poziv onEnd
@asynccontextmanager
async def lifespan(app: FastAPI):
    print("server is starting")
    yield
    print("server is shutting down")
---------
# ovo ce biti pozvano samo jednom?
async with async_session() as session:
    yield session
----------
# chatGpt izlaz
def my_generator_fn():
    print('this is first call')
    yield
-----
# izlaz
gen = my_generator_fn()  # Create the generator object, ne poziva se direktno nego instancira, pa next(generatorObj)
-----
print(next(gen))  # Output: this is first call, None
print(next(gen))  # Output: StopIteration exception
print(next(gen))  # Output: StopIteration exception
print(next(gen))  # Output: StopIteration exception
print(next(gen))  # Output: StopIteration exception
---------------
# pydantic cita env vars from .env file sa ovim, i validira, kao zod
from pydantic_settings import BaseSettings, SettingsConfigDict
class Settings(BaseSettings):
    POSTGRES_URL: str # schema i tip
    model_config = SettingsConfigDict(env_file=".env", extra="ignore") # ucitava .env fajl, zapazi
--------
pydantic je zod schema i to je to, trivial
--------
# kreiranje tabela bez alembic migracija
async def init_db():
    async with async_engine.begin() as conn:
        from .models import Book
        await conn.run_sync(SQLModel.metadata.create_all) # iz metadata izvlaci semu
------
dependency injection - share state ili vars medju rutama # jeste, mesto middleware
---------
# keyword args je options object iz javascript, zapazi, uvek se ponavlja
# keyword args su implicitno dict, positional su tuple
async_session = sessionmaker(
    bind=async_engine, 
    class_=AsyncSession, 
    expire_on_commit=False
)
-----------
u fastApi sva validacija je preko tipova u rutama
router isto ko express
-------
# tags isto za grupisanje ruta..., kako?
api_router.include_router(users.router, prefix="/users", tags=["users"])
-----
u python klasa se poziva bez new, samo MyClass()
-----
select fja prima klasu kao arg, zapazi
statement = select(Book).order_by(Book.created_at)
------
# pydantic response type od SqlModel klase, samo nasledi
# runtime validated like zod
class BookResponseModel(Book):
    pass
-----
# u dekoratoru se postavlja response type i status, moze i fn return type
@book_router.get("/", response_model=List[BookResponseModel])
@book_router.post("/", status_code=HTTPStatus.CREATED)
-----
# spread keyword args, unpack
# .model_dump() convertuje pydantic schema model u dict, BookCreateModel
new_book = Book(**book_create_data.model_dump())
-----
# ovaj pydantic field u schemi definise example u swagger
model_config = {
    "json_schema_extra": {
        "example": {
            "title": "Python Cookbook",
            ...
        }
    }
}
---------------
---------------
# alembic migrations - sync database with models, history
# FastAPI & Alembic - Database Migrations in FastAPI apps - BugBytes
# https://www.youtube.com/watch?v=zTSmvUVbk8M
ne kreiras tabele u onStart nego sa alembic cmd, i imas istoriju baze, inace nemas
# povezivanje sqlmodel i alembic:
----
1. alembic/script.py.mako - template za migration files # ok
import sqlmodel
----
2. alembic/env.py
from sqlmodel import SQLModel  
from models import Band, Album 
# odavde ce da povuce tipove kolona, pk, etc
target_metadata = SQLModel.metadata
# database url u env.py
config = context.config 
DB_PATH str((Path().perent / 'db.sqlite').resolve()) 
config.set_main_option('sqlalchemy.url', f"sqlite:///{DB_PATH}") 
# ovo dinamicki postavlja sqlalchemy.url u alembic.ini
----
3. alembic.ini
# database url moze i hardkodirano ovde
sqlalchemy.url = url...
---------
# kreiranje migracija, py fajlova
# mora povezana baza sa prethodnom schemom
alembic revision --autogenerate -m "migration name" # sa space za name
# apply migration to database
alembic upgrade head # upgrade je fja iz migration fajla, head zadnji migration fajl
# obe komande i za prvu i za svaku sledecu migraciju
----------------
__init__.py je index.ts za javascript, folder postaje fajl # pros
----------------
# https://github.com/cdragos/fastapi-hatch-template
# __all__ kontrolise sta se exportuje iz fajla, fora
# src/scripts/__init__.py
__all__ = [seed_data]
