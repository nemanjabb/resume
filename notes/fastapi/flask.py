# Fullstack Flask – Build a Complete SaaS App with Flask
# 1-9
-----
# venv - 2 komande samo
da instalira lokalno dependecies u __pycache__ folder
python3 -m venv name # create
source env/folder/bin/activate # aktiviraj
to je to, sad su tu requirements i python verzija
------
# minimal env vars
FLASK_NAME, FLASK_ENV
-------
# ginga templejti
base layout
include partials
macro - reusable fja koja vraca html
filter za transform kao angular
if, foreach
url_for() za linkove
-------
# 8
blueprint - router, kolekcija ruta, cistom ime @decoratora rute
kao app i router u express
-----
__init__.py je kao index.ts za modul
sve vars su exported iz modula by default
--------
# 9 - trivial
form POST
render graf sa api, bzvz
sve templejti, nista python 1-9
----------
# 10 Using an Application Factory - funkcija
# js import
import { var1 } from './folder/file.js'
# python module import
from folder.file.var1 import var1
---
FLASK_APP=module:funkcija FLASK_ENV=development flask run
---
config.py sa base, dev, prod, test config classes
---
# error middleware, kao express (req, res, err)
@app.errorHandler(500)
def handle_error(exception)
----------
# 11 testing
pytest, flask_test libs
pip install -r requirements.txt # moze, ide u venv
# basic test
def test_addition():
    assert 1+1 == 2 # assert je kao operator fja, nema ()
----
conftest.py # jest.config.js
---
fixture definise client koji je app
response = client.get('/ruta')
assert response.status_code == 200
response.data je binaran
{'key1': 'val1'} # dict, 
"" i '' # je isto string
---------------
# 12 Getting to 100% Code Coverage
# monkey patch api call
monkeypatch.setattr(requests, "get", mock_get) # jest.spyOn()
b'some string' # converts string to binary object
mock_get(*args, **kwargs) # all positional and named args
----
test nema string opis, samo ime fje
----
# with je block, ovo je kao catch blok
with pytest.raises(KeyError):
----
pytest-flask
pytest-cov # pass folder za cov
# pragma:no cover # za ignore line
posle refactor testovi prolaze, poenta
---------------
# 13 Our Project: Yumroad
gumroad clone, i stripe
---------------
# 14 Starting from Scratch - config i flask factory
python3 -m venv env # env je folder
source env/bin/activate # activate
ls # daje env
pip install -r requirements.txt
-------
gunicorn je production http server
---
pass sluzi za praznu klasu, kraj bloka umesto {}
--------------
# 15 Databases & ORMs
safe import u posebanom fajlu...
flask extension prima app u contructor
---
db modeli su klase, a ne tipovi ili objekti, sql alchemy orm
extends db.Model
@validate decorator odmah u semi, baca exception
--------------
# 16 Using SQLAlchemy
session - persistence manager, moze i direct na model class Product.get()
ide commit() za transakciju
pokazao crud operacije na modelu
unit testovi za service
----------------
# 17 Building pages using our models 
# trivial product/id and products templates
add products blueprint - router
abort(404) # error stranica umesto exceptiona
add base layout for products list
render custom 404 template u error handler u blueprint
add products list template
----------------------
# 18 Testing Our Routes
tests for /products/id and products
client.get(template) # client je arg testa
assert str in response
proverava coverage odmah dok radi sa testovima
-----------------------
# 19 Building (HTML) Forms
build html form and api endpoint, trivial
-------------------
# 20 Rendering & Processing a Form
WTF form - fields, validator and form classes
form server rendered template, nema smisla sa react
skoro ceo kurs izdangubio sa templateima, vrlo malo python
------------------
# 21 Editing Data & CSRF Protection
pass obj to prepopulate form, trivial
---
csrf token kao hidden field u formi
nije objasnio kako se generise isti na serveru i clientu???
u testovima izostavio csrf token
--------------------
# 22 User Registration
user model
werkzeug.security for password hash
wtforms[email] # install part of package in requirements.txt
register form and validation
custom valudate function
blueprint - controller, register template
samo kreira usera u bazi, neulogovan
----------------------
# 23 Flask-Login & Sessions
compare password in validation fn
current_user je default defined u templateima
flash je bootstrap alert div
mixin python..., ima i visestruko nasledjivanje?
zesce dosadno i dugacko
---------------------------
podsetnik: ucio flask za python tw bota
# 24 Logging Out & Testing
protected routes - @login_required decorator
----
logout route
----
tests login
----
dosadno zesce
------------------------
# 25 DB Relationships & Migrations
objasnjava schemu, i to lose
------------------------
# 26 Implementing Relationships
definise relacije u modelima
templejti su potpuno beskorisni jer koristis react
---------------------------
---------------------------
# Build and Deploy a Complete REST API with Python Flask Tutorial - Cryce Truly
# https://www.youtube.com/watch?v=WFzRy8KVcrM
# https://github.com/CryceTruly/bookmarker-api
-----
# u venv da vidis koji je python
which python
-----
kad otvoris novi terminal mora da opet aktiviras venv
source venv/bin/activate
-----
__name__ - ime fajla
-----
FLASK_APP=app # modul u kome je pozvan Flask()
-----------
# application factory
1. fja gde instancira Flask()
2. ucitava env vars, 
3. glavne rute
4. load blueprints rute
----
.flaskenv - public vars, ide u git
.env - private vars, gitignored
os.environ.get(VAR_NAME) # ovako get vars iz .env fajla
--------------
# blueprints - moduli, podrute, express router
auth = Blueprint("auth", __name__, url_prefix="/api/v1/auth") # vraca decorator za podrute
@auth.post('/register')
def register():
----
u blueprints su rute decoratori i handleri
--------------
# sqlalchemy - orm
db = SQLAlchemy()
class User(db.Model): # nasledjuje, zapazi
    id = db.Column(db.Integer, primary_key=True) # schema nadalje
---
foreighn key i backref
----
napisao fju za unique slug string
---------------
# http status codes
konstante za brojeve
---------------
# register user
create user, validacija, hash, insert user model
----
kako return dve vars, jel tuple?
if User.query.filter_by(username=username).first() is not None:
    return jsonify({'error': "username is taken"}), HTTP_409_CONFLICT
----------------
# jeste tuple, za svaki tuple (x, y, z) zagrade su potpuno opcionalne
# tuple - immutable array prakticno
# a lista je muttable array prakticno
-----
# This function returns a tuple
def fun():
    str1 = "geeksforgeeks"
    x   = 20
    return str1, x;  # Return tuple, we could also

# poziv
str1, x = fun() # unpack returned tuple
----
# https://www.geeksforgeeks.org/g-fact-41-multiple-return-values-in-python/
# flask taj tuple posle spoji u repsonse object
# https://stackoverflow.com/questions/58638325/what-is-the-difference-between-returning-jsonify-and-returning-make-response-f/66140327#66140327
los tutorijal, trivial crud, nema veze sa flask
---------------
# user login
jwt, check hash
-----
pozvao 2 puta isto za access i refresh token, kako?
if is_pass_correct:
    refresh = create_refresh_token(identity=user.id)
    access = create_access_token(identity=user.id)
----------------
# protected routes
auth -> barer token, probaj postman, razne opcije za reprodukciju
----
@auth.get("/me")
@jwt_required() # ovim decoratorom se stiti ruta
def me():
----
# debugiranje, otvara immidiate window u terminal, repl
def me():
    import pdb
    pdb.set_trace()
----------------
# refreshing token
ovaj lib za jwt tokene
# https://flask-jwt-extended.readthedocs.io/en/stable/
posalje refresh token da dobije ovi access token, nije objasnio detalje
-------------------
# create and get
# src/bookmarks.py
@bookmarks.route('/', methods=['POST', 'GET']) # 2 verba odjednom
@jwt_required()
def handle_bookmarks(): # ime ove fje kako hoces
----
post - cita payload, validira, kreira objekat u bazi i vrati json
get - vrati polje (listu), trivial
-------------------
# pagination
page, limit, nista specificno za flask, trivial
--------------------
# get one - get item by id, trivial
# ovako hvata id parametar
@bookmarks.get("/<int:id>")
@jwt_required()
def get_bookmark(id): # evo gde izadje id param kao arg
--------------------
# delete item
--------------------
# user link click tracking
on route increment and redirect, trivial
--------------------
# error hanndling
api return always json and not html, and for errors
----
# handler ruta
@app.errorhandler(HTTP_404_NOT_FOUND)
def handle_404(ex): # ovde exception
    return jsonify({'error': 'Not found'}), HTTP_404_NOT_FOUND
---
500 moze da se testira samo za env=prod
--------------------
# get link stats - trivial
obicna get ruta koja vraca visits prop za sve linkove, trivial
----
# json se vraca kao dictionary, zapazi
return jsonify({'data': data}), HTTP_200_OK
--------------------
# swagger docs
flasgger paket
-----
# src/config/swagger.py
template, swagger_config json vars # swagger config, nevezano za flasgger
# src/__init__.py
Swagger(app, config=swagger_config, template=template)
----
u ovim yml ti rucno opises svaki endpoint, tags (za grupisanje), aprams, responses (success, error)
# src/docs/short_url.yaml
# src/docs/auth/login.yaml
u docs folder svi endpointi ponivljeni sa yml opisima
----
taj yml opis ucitas na pravu rutu
@auth.post('/login')
@swag_from('./docs/auth/login.yaml')
def login():
---------------------
# heroku deployment
pip install gunicorn - prod http server, kao nginx
gunicorn src.runner:application # ovako se startuje
-----
# export dependencies
pip freeze > requirements.txt
----
env vars
---------------------
# Build Modern APIs using Flask - Pythonist
# https://www.youtube.com/playlist?list=PLMOobVGrchXN5tKYdyx-d2OwwgxJuqDVH
poenta: na pocetku 2 run methods, request, response, context, posle crud sa in memory, sqlite, mysql
previse lak tutorial, pocetnicki
---------------
# 1. A brilliant introduction to Flask
bare framework bez orm, forms, etc... kao express
---
flask app je web server gateway interface
view function - controller action method
mapa ruta i view funkcija
--------------------
# 2. Build Modern APIs with Flask
# how to run flask app:
1. flusk run - starts dev server
export FLASK_APP=app.py # entry point fajl
2. programmatically - starije verzije, for running tests
if __name__ == '__main__':
    app.run()
------
re-loader - nodemon
debugging - FLASK_DEBUG=1
----
flask run --help
------------------------
# 3. Request and Response cycle in Flask
application and request context
request dispatch and object
response object
----
context - global object za deljenje data izmedju requests
-----
# application context
current_app
g
# request context
request object
session - kao php
------
from app import app
app_ctx = app.app_context()
app_ctx.push()
current_app.name # sad moze
-------
# request dispatch
app.url_map # lista rute
-------
# request object
# methods
get_data()
get_json() # parse body into dict
---
# vars
endpoint, method, host, URL
----
# hooks
before_request
before_first_request
after_request
teardown_request
---
koristi se sa g.user npr global context to share loggedin user
---------
# response object
# methods
set_cookie()
delete_cookie()
set_data()
get_data()
-----
# vars
status_code
headers
content_type
content_length
----
redirect object
---------------------------
# 4. Build a Rest API using Flask - in memory
url, domain, port, path, query
----
# interesantno grupisao metode za books i book/id sa if unutra
/books - GET, POST
/book/<int:id> - GET, PUT, DELETE
------------------------
# 5. SQLite database in flask API Python
# evo kod
# https://gist.github.com/arycloud/56df8f4d77e8324c4407361ffbe20cc7
import sqlite3
conn = sqlite3.connect('file.sqlite')
cursor = conn.cursor() # kursor je objekat na konekciji nad kojim se izvrsavaju queryji, zapazi
cursor.execute(sql_query)
----
konektuje u try catch
-----------------
# 6. Integrate mysql database in Flask 
import pymysql
conn = pymysql.connect(server, port, user, pass)
cursor - conn.cursor()
cursor.execute(sql_query)
conn.close()
-------------------
# 7. Build and Deploy a Rest API using Flask on Heroku
pip install gunicorn # u venv
gunicorn app:app
-------------------
# 8. Containerize a Flask application using Docker - trivial
Dockerfile sa python i requirements installed
docker run




