-------------------------------------
-------------------------------------
# Dockerizing Flask with Postgres, Gunicorn, and Nginx
# https://testdriven.io/blog/dockerizing-flask-with-postgres-gunicorn-and-nginx/
# https://github.com/testdrivenio/flask-on-docker
# ovo je vise o dockery nego o flasku
# dobre fore
# this rebulds images and restarts containers in one command
docker-compose up -d --build
# exec commands with docker-compose in service
docker-compose exec web python manage.py create_db
# remove both volumes and containers, dobar
docker-compose down -v
# listanje postgres baze
docker-compose exec db psql --username=hello_flask --dbname=hello_flask_dev
-------
# gunicorn
gunicorn - WSGI - Web Server Gateway Interface is a simple calling convention for web servers 
to forward requests to web applications or frameworks written in the Python
kao reverse proxy (nginx) + webserver (createServer) u node.js
interne komponente - Arbiter, Worker, Server, Worker Class, Loader, Logger
ima python interpreter u sebi da izvrsava app code
----
most zmedju nginx i python
------
Python wheels ??
-----
If you are root in the container, youll be root on the host
-------
# expose vs port
Now, port 5000 is only exposed internally, to other Docker services. The port will no longer be published to the host machine.
---
# static folder
STATIC_FOLDER config to services/web/project/config.py
# handle media files
-------------
# services/web/entrypoint.sh
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    # nc - netcat -z - head request
    # blokira container dok se baza ne podigne, wait_for utility
    # retry connect 0.1 sec
    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# reseed
python manage.py create_db


# forward all args and call first
# forward command u entrypoint, da skripta nastavi da radi kao entry point
exec "$@"
-------
"$@" varijabla sadrži sve argumente kojim je pozvana skripta
exec izvrsava prvi arg
----
# skripta.sh
#!/bin/bash
exec "$@"
----
Pokrećemo skriptu sa:
./skripta.sh echo neki text
neki text # izlaz
-------------------
__init__.py (flask factory) i config.py (env vars) su cela flask aplikacija
manage.py - migrate and seed scripts for sql-alchemy, nema veze sa flask, docker entrypoints
--------------------------------------
--------------------------------------
# https://testdriven.io/blog/flask-docker-traefik/
# https://github.com/testdrivenio/flask-docker-traefik
ovaj projekat je sve isto samo akcenat na traefik
samo flask backend, sve isto kao flask-on-docker, samo labele
# https://github.com/testdrivenio/flask-on-docker
---------------------------------
# https://gitlab.com/testdriven/flask-react-auth
# malo veca flask aplikacija nego inace, prouci
# ima react-scripts 18 i nginx
# backend je identican na ovom:
# https://gitlab.com/testdriven/flask-tdd-docker
--------------------------------------------
--------------------------------------------
# Flask for Node Developers - Michael Herman
# https://mherman.org/blog/flask-for-node-developers/
# _live folder je Postgres and Flask-SQLAlchemy, trivial
# https://github.com/mjhea0/flask-songs-api
# go crud, trivijalan tutorial
----------
venv koristi
----
# smisao - pozovi fje ako je pozvan iz terminala # to, to
# za import - nista
...
if __name__ == '__main__':
    drop_table()
    create_db()
----------
# POST iz curl
curl --data "artist='Beastie Boys'&title='Sabotage'&rating=4" http://localhost:5000/api/songs
# PUT
curl --data "artist='The Flaming Lips'&title='Buggin'&rating=2" http://localhost:5000/api/songs
------------
# ima odvojene controllere i services za db
@app.route('/api/song/<song_id>', methods=['GET', 'PUT', 'DELETE'])
def resource(song_id):
    if request.method == 'GET':
        song = get_single_song(song_id) # service db
        return json.dumps(song)
------------





