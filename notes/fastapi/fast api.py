# 19h fastApi playlist
#  Python FastAPI Course Intro: Part #1 Python API Course - Sanjeev Thiyagarajan
https://www.youtube.com/watch?v=Yw4LmMQXXFs&list=PL8VzFQ8k4U1L5QpSapVEzoSfob-4CR8zM
alembic je samo za migracije
-----------
# cookiecutter - da zameni vars u template projects, python, C
# fastApi, vue, postgres
# i on ovde koristi poetry, znaci to je standard
https://github.com/tiangolo/full-stack-fastapi-postgresql
https://github.com/homanp/superagent
------------
# ovo su samo base docker slike
https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker
https://github.com/tiangolo/meinheld-gunicorn-flask-docker
-------------------
-------------------
#  How to Use FastAPI: A Detailed Python Tutorial - ArjanCodes
https://www.youtube.com/watch?v=SORiTsvnU28
https://github.com/ArjanCodes/2023-fastapi
pip install fastapi[all] # zapazi [] sa pip
----------
# enum tako sto klasa nasledi enum
from enum import Enum # importuje se Enum
class Category(Enum):
    TOOLS = "tools"
    CONSUMABLES = "consumables"
-------
python ima dosta tipova kao typescript
automatski konvertuje objekte u json kad vratis, i obrnuto za params, i validira ih preko tipova, dekoratori
-------
# validacija query params
str | None je tip,  = Query() je default value
@app.put("/update/{item_id}")
def update(
    item_id: int = Path(ge=0),
    name: str | None = Query(defaut=None, min_length=1, max_length=8),
-----
# prva dva su tip, iza = ide default value
name: str | None = None,
--------------
# pydantic - validation and dto
shape za objects - tipovi, kao typescript
from pydantic import BaseModel
class Item(BaseModel):
    name: str
------
generate docs iz koda, metadata, docstrings
fastApi ima ugradjen swagger i redoc
---------
gunicorn - sync, kao php
uvicorn - concurent, kao pm2
---------------------
# fastApi ima dependency injection
https://fastapi.tiangolo.com/tutorial/dependencies/
@router.get("/users/me")
async def read_user_me(token=Depends(JWTBearer())):
---
DI je at runtime, import je compile time
------------------------------
# https://dev.to/tobias-piotr/patterns-and-practices-for-using-sqlalchemy-20-with-fastapi-49n8
# https://github.com/tobias-piotr/alchemist
nije bas jasan
koristi python sa tipovima
[] je generics, kao <> u TypeScript, Generic[Model] # zapazi
-------
1. definise tipove za modele - pydantic
2. definise orm modele i relacije - sqlalchemy
3. konekcija, pool, sesija
4. migracija, apply orm model to db - alembic (moze i bez for one time)
5. endpoints - fastApi
6. repository, reuse common code for endpoints
7. e2e testing with test db - pytest, httpx
pytest - create and run tests
httpx - http client to call api
--------------------------------
# Scalable FastAPI Applications on AWS
# vise devops tutorijal nego python # ZAPAZI
# https://testdriven.io/courses/scalable-fastapi-aws/
# https://github.com/rbraddev/talk-booking
# https://github.com/chasefrankenfeld/talk-booking
# 3.
Black - formatter
Flake8 - linter
# 4. init app and test
---
# poetry, vidi tut neki
poetry init # prompt - no, no
poetry add --dev pytest # dev
poetry add fastapi uvicorn # prod
poetry run pytest tests/integration # run tests
poetry run uvicorn web_app.main:app --reload # run app
---
# pytest
# fixture je kao beforeEach() verovatno
@pytest.fixture
def client(): # isto ime kao dole
    return TestClient(app)
def test_health_check(client): # ovaj arg je od fixture gore
---
# 5. gitlab ci-cd
gitlab ci-cd
build and push app docker image 
ta slika se pull i koristi za lint i test steps
-----
format, lint - black, flake8
# import external yaml file in gitlab ci
include:
  - local: /services/talk_booking/ci-cd.yml
------
# events to trigger ci
  only:
    changes:
      - ci_cd/python/Dockerfile # samo promena na ovom fajlu, zapazi
    refs:
      - master
---
  only:
    refs:
      - merge_requests # master, merge requests against master, zapazi
      - master
    changes:
      - services/talk_booking/**/*
--------
# 6. iac - terraform
run terraform in gitlab ci
------
# backend - remote state (za control loop) na gitlab, da mogu svi useri da ga imaju, zapazi
terraform {
  backend "http" {
  }
}
----
.vpc-job: # . hidden, reusable job with extends
---
# 3 jobs:
vpc-validate: # validate terraform files syntax
vpc-plan: # dry run, preview
vpc-apply: # apply
---
# include partials in gitlab
stages:
  - docker
  - test
  - validate
  - plan
  - apply # ovaj se rucno pokrece

include:
  - local: /infrastructure/global/vpc/ci-cd.yml
  - local: /services/talk_booking/ci-cd.yml
---
# zasto terraform u ci?
da napravi/update i infrastrukturu za deploy code, ne samo code
-----------------------
fastApi je samo za api, flask moze i template # eto
----
fn(arg: Union[str, None] = None) # ovako izgleda tip za opcionalni arg
---
uvicorn main:app --reload
main - main.py tvoj fajl
app - app = FastAPI() var koja je exportovana po defaultu
---------------
# https://github.com/zhanymkanov/fastapi-best-practices
# u pythonu : je dekleracija tipa
class UserBase(BaseModel):
    first_name: str = Field(min_length=1, max_length=128)
---
# dodeljuje se tip kao type alias u ts, type A = B...
my_field = Field(min_length=1, max_length=128)
first_name: my_field
----
pydantic je validacija plus tipovi, runtime checking, baca exceptione, kao zod
----
pydantic validira samo request data
za validaciju koja poredi sa bazom (existing email npr.) ide dependencies (iz fastapi)
# poziva se u arg fje
async def get_post_by_id(post: Mapping = Depends(valid_post_id)):
---
FastAPI caches dependencys result within a requests scope by default # samo 1 poziv po requestu
---
isti :profile_id param var da moze dependency da se reuse po rutama
-------------
# 7. Don't make your routes async, if you have only blocking I/O operations 
# ovo objasnjeno i u fastapi async docs
runs sync routes in the threadpool 
async route is called regularly via await
---
kod primer je jasan, tekst sam preskocio
time.sleep(10) # blocking IO
await asyncio.sleep(10) # non-blocking IO 
---
GIL - Python Global Interpreter Lock # 1 thread u 1 trenutku
------------
# 8. Custom base model from day 0 # teze malo
------
# docs
app_configs["openapi_url"] = None  # ovo sakriva docs
------
@router.get("/users/me", response_model=UserResponse) # response_model is for docs
-------
pydantic validira env vars, nested class za config
-----------
# 12. filename za migracije
file_template = %%(year)d-%%(month).2d-%%(day).2d_%%(slug)s
# 13. db naming
lower_case_snake
singular form (e.g. post, post_like, user_playlist)
# 14. async tests http client, integration tests
async_asgi_testclient - http client
# 15. background tasks
worker: BackgroundTasks # tip
worker.add_task(notifications_service.send_email, user_id) # bez await 
# 16. tip daje intelisense
user: TokenData = Depends(get_jwt_user_data),
TokenData je tip
Depends() je default value
------
dosta toga se zasniva na dobrom razumevanju event loop, async, IO, cpu, thread pool, 
-----
# 17. save uploaded files in chunks, stagod
# 18. dynamic pydantic fields, unions
class Article(BaseModel): # dict ispada
   text: str | None # optional field
----
pydantic je zod schema # zapazi
tacno, pises seme preko klasa
jesu method overrides
class Config: # lokalna klasa je var za override u pydantic BaseModel nasledjena
gde god je BaseModel to je pydantic
-----
class Config: 
    extra = Extra.forbid # forbid extra fields
# ili ovo, nebitno
  class Config:
    smart_union = True # radi samo za primitive types
------
# 19. SQL-first, Pydantic-second
operacije u sql, a ne u python # sto sam znao sam vec
---
# dobar primer, sve na jednom mestu
1. src.posts.service
2. src.posts.schemas
3. src.posts.router
# evo kako su svi pozvani u router
# pydantic schemas za ovo Post
@router.get("/creators/{creator_id}/posts", response_model=list[Post])
async def get_creator_posts(creator: Mapping = Depends(valid_creator_id)):
   # service za ovo
   posts = await service.get_posts(creator["id"])
----
# pydantic moze da validira json
numbers: Json[list[int]]
# 20. whitelist utl hosts, kao u next.js
# 21. Raise a ValueError - set custom errors, kao zod
# 22. Pydantic -> dict -> Pydantic -> JSON
jeste pydantic radi preko method overrides, protokoli su interfejsi u pythonu
2 puta se poziva, nije bas jasno?
# 23. run sync calls in a thread pool
from fastapi.concurrency import run_in_threadpool
# makes the HTTP calls in an external worker thread
await run_in_threadpool(client.make_request, data=my_data) # run_in_threadpool(sunc_func)
thread pool ima gotove niti koje cekaju, izbegava skupo kreiranje i unistavanje niti
# 24. Use linters (black, ruff)
black - formatter
ruff - linter - autoflake + isort # sortiranje imports verovatno
------------------
# fastapi pluralsight bezveze
# odakle je fastapi sta preuzeo:
auto docs - django rest framework (django extension)
routing - flask
method names - requests
swagger, openapi
code based schema, auto data validation - marshmallow, webargs
ujedno validacija, serializacija, docs - apistar
dependency injection - angular, nest.js
molten - extra validation, pydantic
---
# navedeno u docs ovde
https://fastapi.tiangolo.com/alternatives/
--------------------------
--------------------------
# fastApi docs
# Python Types Intro
editor autocmplete and errors
Generic types, type parameters (str je unutra)
list[str]
----
python 3.6+ 
from typing import List # mora imports, ne treba install
List[str]
---
python 3.9+ # ne treba import
list[str], dict[str, float], tuple[int, int, str], set[bytes]
----
# union
item: int | str
# optional
u pythonu arg nije optionalan, nego "moze biti None", nullable
ne moze biti izostavljen u pozivu, required je
---- 
def say_hi(name: str | None = None): # glavni nacin 3.10, taj i zdravo
Optional[str] = None # 3.6, Optional === nullable zapravo, pogresno je
Union[str, None] = None # preferred
# objasnjenje
Optional[Something] = Union[Something, None]
--------
# pydantic
Pydantic is library to perform data validation
declare the "shape" of the data as classes with attributes
each attribute has a type.
you create an instance of that class with some values
it will validate the values, convert them to the appropriate type, and give you an object with all the data
poneta: vrsi validaciju i cast kad pozivas constructor, user = User(**external_data)
---
# nasledjivanje je vazno
class User(BaseModel): # pydantic
    id: int
    name: str = "John Doe"
    signup_ts: datetime | None = None
    friends: list[int] = []
----------
# Metadata Annotations - typescript docblocks, za editor
mora import, prvi arg je tip, drugi opis
from typing import Annotated
def say_hello(name: Annotated[str, "this is just metadata"]) -> str:
---------
# python koristi tipove za:
Editor support
Type checks
# fastapi ih koristi za:
Define requirements: from request path parameters, query parameters, headers, bodies, dependencies, etc
Convert data: from the request to the required type
Validate data: coming from each request:
Generating automatic errors returned to the client when the data is invalid
--------------------
# Concurrency and async / await
I/O bound operations
asynchronous - non blocking, CPU moze da radi drugo i samo pokupi rezultat 
# concurrent vs parallel
concurrent - cpu neblokiran dok se IO ne zavrsi
# kad
dobar gde ima dosta IO cekanja, http, db, files
# gde
web apps
----
# cpu bound
parallel - cpu blokiran sa IO
# kad
samo posao, nema cekanja, ako posao moze da se podeli, i izvrsava paralelno vise jezgra
# gde
audio, video, ML, deep learning
-------
# podsetnik primeri
time.sleep(10) # IO blokira cpu
await asyncio.sleep(10) # cpu radi drugo dok ne stigne rezultat
---------------
# rezime:
1. concurrency - async, IO neblokiranje
2. multithreading - GIL, python interpreter
3. multiprocessing - OS, komunikacija
--------------
fastapi - api oko ML funkcionalnosti
---------
# sledece naredbe ce imati burger (pokupi rezultat)
# u medjuvremenu python radi drugo # to
burgers = await get_burgers(2)
----
fastapi koristi AnyIO 
AnyIO = asyncio + Trio
-----------
# Coroutines
thing returned by an async def function # znaci promise
------------
# Path operation functions - api route handlers
rute i dependencies sa def - u thread pool # kazahstanac sto je pisao
fastapi utice na path functions i dependeency functions, i def izvrsava u novi thread u thread pool # TO, zapazi
ostale fje u kodu ne dira
---
# async def vs def za rute
inace async def vs def za rute ide trivial tldr sa pocetka
async def - za async fje unutra, def za obicne blokirajuce # eto
# tacno kazakhstanac: 
ne ruta async def ako nema await u telu jer nece ici u novi thread
ako je def ide u thread pool u novi thread i ne blokira loop
inace sync code u telu blokira event loop
-------------------
# Tutorial - User Guide
# first steps
swagger url
http://127.0.0.1:8000/docs
schema, kao wsdl za servis
http://127.0.0.1:8000/openapi.json
---
redoc url
http://127.0.0.1:8000/redoc
--------
main - file, app - app = FastAPI()
uvicorn main:app --reload
-------
app = FastAPI()
@app.get("/") # ovde instanca se koristi
async def root():
    return {"message": "Hello World"} # auto serialized to json
-------------
# path parameters
@app.get("/items/{item_id}") # isti format kao f-strings
# samo od tipa: parsuje se u tip int, validira i throw error, i docs
async def read_item(item_id: int): # ovde je kao argument item_id
    return {"item_id": item_id} # arg variabla
---
redosled ruta vazan, kao u Next.js
------
# Predefined values
param set vrednosti, unija stringova prakticno
class ModelName(str, Enum): # 2-struko nasledjivanje, str mora za docs
    alexnet = "alexnet"
    resnet = "resnet"
    lenet = "lenet"

@app.get("/models/{model_name}")
async def get_model(model_name: ModelName): # ovde ta klasa
----
# compare enum value
if model_name is ModelName.alexnet:
if model_name.value == "lenet":
----
# ako je sam param file path, nebitno
@app.get("/files/{file_path:path}")
-------------
# Query Parameters
# fn args koji nisu navedeni u ruti
@app.get("/items/")
async def read_item(skip: int = 0, limit: int = 10): # default values ako se izostave
http://127.0.0.1:8000/items/?skip=0&limit=10
parsed into type, validated, docs
---
1, true, on, yes are converted to bool
------
# moze nekoliko params
@app.get("/users/{user_id}/items/{item_id}")
async def read_user_item(
    user_id: int, item_id: str, q: str | None = None, short: bool = False
):
----
needy: str # required
skip: int = 0 # optional, default value
limit: int | None = None # optional
--------------
# Request Body
# isto arg kao path i query params
class Item(BaseModel):
    name: str
    description: str | None = None
    price: float
    tax: float | None = None

@app.post("/items/")
async def create_item(item: Item):
----
isto parse, validate, docs
isto optional, default, required
-----------
# path, query, body od jednom
same name var in path - path param
singular type (int, float, str, bool) - query param
type of Pydantic - body
--------------------
# Query Parameters and String Validations, detalji, nije toliko vazno
------------------
[] su genericki tipovi u pythonu, args
generator - moze u for loop da se poziva
context manager - ima open i close i telo izmedju
----------------------------
----------------------------
# makefile - npm scripts za C
build automation tool
--------------------------
# tumacenje koda
# https://github.com/zhanymkanov/fastapi_production_template
-----
# Gunicorn vs Uvicorn
WSGI (Web Server Gateway Interface) HTTP server
Gunicorn - synchron, multiprocessing, django, flask
Uvicorn - async, fastapi, starlette, tornado
---------
auth_user = Table() # schema, constructor poziv, instanca
insert_query = (insert(auth_user)...) # tuple, prismaClient.User.create({...})
fetch_one(insert_query) # commit()
------
# sqlalchemy je data maper, sessija je context manager, chatGpt
# Prvo, napravite konekciju ka bazi podataka
engine = create_engine('sqlite:///my_database.db')
# Zatim, napravite Session klasu koristeći sessionmaker
Session = sessionmaker(bind=engine)
# Otvaranje nove sesije
session = Session()
# Kreiranje novog objekta i dodavanje ga u sesiju
new_user = User(name='John', email='john@example.com')
session.add(new_user)
# Izvršavanje promena i čuvanje u bazi podataka
session.commit()
# Zatvaranje sesije
session.close()
-------
# user model, tabela # postoji i pydantic model za type i validaciju, typescript
Base = declarative_base()
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
-------------------
# tumacenje koda
# https://github.com/jonra1993/fastapi-alembic-sqlmodel-async
minio - aws s3 za fajlove i slike
caddy + minio - static folder za uploads
----------
ruff - lint
black - format
-----
connection pooling - ready and available db connections, da se ne kreiraju nego reuse, performance
# backend/app/app/db/session.py
POOL_SIZE = max(DB_POOL_SIZE // WEB_CONCURRENCY, 5)
--------
# async vs sync context manager
async context manager ima u telu await ili yield i to je to, trivial # isto kao async fn u js
async with context() as var1
-------
# da bi conn bila lazy, kao, ne zove se u loop
async def get_db() -> AsyncGenerator[AsyncSession, None]: # generator
    async with SessionLocal() as session: # async context manager
        yield session
--------
@pytest.fixture(scope="session")
def event_loop() -> Generator[asyncio.AbstractEventLoop, None, None]:
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close() # ovo se izvrsi nakon sto loop zavrsi, ciscenje kao ctx manager
--------
# generator podsetnik
sa generator moze 2+ fje da se impl u jednoj fji
context manager ima onExit() callback koji moze da handluje exception (i cisti) nakon tela ctx managera
generator ima stanje od prethodne iteracije (iznad yield) ispod yield
----
__main__.py # executable paket, podsetnik, ima u pluralsight
--------------
# makefile su samo shell komande
run-dev-build:
	docker compose -f docker-compose-dev.yml up --build
cita .env fajl za vars 
-----
# seed script sa petljama
backend/app/app/db/init_db.py
# cita ugnjezdene foldere sa . kao dict # zapazi, __init__.py prica
from app.db.init_db import init_db
-----
# run python pakete sa poetry (yarn)
formatter:
	cd backend/app && poetry run black app
------
docker run - first CMD, docker exec posle kad kontejner vec radi
----
poetry toml, black, ruff, mypy, dependencies # package.json sve isto
---------------------
# two similar with arq
# sva 3 su sqlalchemy, a ne sqlmodel # zapazi
# another from linkedin
https://github.com/igormagalhaesr/FastAPI-boilerplate
https://github.com/igorbenav/FastAPI-boilerplate
# jos jedan od njega, promenio username u igorbenav
https://github.com/igorbenav/fastcrud
# dobar zapravo, kubernetes
https://github.com/Kludex/fastapi-microservices
Tiltfile - maps Dockerfile to kubernetes
----
# sve preuzeto iz tiangolo
https://github.dev/tiangolo/full-stack-fastapi-postgresql
----
kopira samo app folder u Dockerfile, ne postavlja PYTHONPATH 
COPY ./src/app /code/app
----
models - tip za bazu
schemas - tipovi pydantic
poziva samo sa gen tipovima, nema args
CRUDUser = CRUDBase[User, UserInDB, UserUpdateDB]
---
# typing builtin std module, tipovi za fje, args i return
# provides types
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
# mypy dev dependency, nema imports, samo pokrece kao typescript da proveri tipove iz typing
# static type checker
mypy mypy_example.py
----
# {{cookiecutter.project_slug}}/backend/app/app/crud/base.py
def __init__(self, model: Type[ModelType]):
model: Type[ModelType] # klasa, tip
model: ModelType # instanca klase, ne treba to
Type[] = typeof<Tip> typescript
----
# sqlalchemy
def get(self, db: Session, id: Any) -> Optional[ModelType]:
    return db.query(self.model).filter(self.model.id == id).first()
Session - persistance manager
db.query(self.model) - db.User.findById() # prisma
sqlalchemy prima u fju(nesto) umesto db.nesto
-----
# zakljucak:
base.py samo prosledjuje tipove kao args, nista razlicita runtime logika
# {{cookiecutter.project_slug}}/backend/app/app/crud/base.py
class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
--------------
# pydantic sema - zod sema, tip, pravila, poruka (Anotated)
src/app/schemas/user.py
  username: Annotated[
      str, 
      Field(min_length=2, max_length=20, pattern=r"^[a-z0-9]+$", examples=["userson"])
  ]
[] je generics u python, <T>
---
# sqlalchemy sema - prisma.schema, tipovi, kljucevi i relacije
src/app/models/user.py
# sqlalchemy schema, nizeg nivoa izgleda
name: Mapped[str] = mapped_column(String(30))
name: Tip = default_value
# sqlmodel, viseg nivoa
email: EmailStr = Field(nullable=True, index=True, sa_column_kwargs={"unique": True})
sa_ prefix - sqlalchemy
state: str | None # ovo npr, samo tip, viseg nivoa
---------------------
https://github.dev/asacristani/fastapi-rocket-boilerplate
# sto je u igormagalhaesr/FastAPI-boilerplate
src/app/crud/crud_base.py
# ovde je u:
app/core/models/base.py
----
# ogranicenje tipa, genericki tip T koji zadovoljava base tip
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
type CreateSchemaType<T extends BaseModel> = T; # typescript
TypeVar - fja koja prima tip i vraca tip, tako nesto...?
-------
u pythonu se klase (i lokalne za nested) koriste umesto object literals # zapamti
----
# folders:
# u app folder:
api - rute, endpoints, controlleri
core - config, jwt, ?
crud - sqlachemy, sqlmodel crud services
models - db schema, prisma.schema
schemas - pydantic validacija, zod
tests - testovi
migrations - alembic
# additional, ponegde
db - session i connection verovatno, ?
dependencies
utils
scripts - shell i python
----------------------
swagger je sad openApi
swagger docs za backend isto sto i storybook za react, interactive
swagger drzi sesiju, kada se loginujes mozes ostale zasticene rute da zoves # konfigurise se, prouci
----
# pass access_token u header:
curl -H "Authorization: Bearer YOUR_ACCESS_TOKEN" YOUR_URL
# ceo primer, ne zaboravi \ na kraju svake linije:
curl -X 'GET' \
  'http://fastapi.localhost/api/v1/user/list?page=1&size=50' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer access-token-here'
---------------
# kako naci base url i docs url
fastApi definise samo relativne staze, gunicorn definise host i port # zapazi
.env file
# app/app/main.py
app = FastAPI(
    title=settings.PROJECT_NAME, openapi_url=f"{settings.API_V1_STR}/openapi.json"
)
---
# zapravo je ovde base url i port
# gunicorn_conf.py
host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("PORT", "80")
----
# ovde tipovi i validacija za env vars
app/app/core/config.py


