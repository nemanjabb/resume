// .mjs, .cjs fajlovi, prouci 
-----------
await rec blokira promise dok se ne izvrsi, samo to 
// ako su bez await isto ce se izvrsiti ali random redosled // paralelno?
fn1()
fn2()
fn3()
-----
// po redosledu
await fn1() 
await fn2()
await fn3()
----
// ici ce parlelno
Promise.all() 
------------------
// local storage vs session storage
localStorage for ever, sessionStorage dok je otvoren tab, jedina razlika 
svi tabovi pod istim domenom mogu da vide sadrzaj
-----------------
// # - private fields, private modifier in class
export class CancelablePromise<T> implements Promise<T> {
    #isResolved: boolean; // private isResolved, u klasi samo
    constructor() {
        this.#isResolved = false; // # je deo imena
    }
}
-----------------------
// format date, native js
// ovo ga lepo formatira // zapazi, bez lib
new Date().toTimeString()
"07:22:56 GMT+0100 (Central European Standard Time)"
new Date().toDateString()
"Mon Mar 18 2024"
-------------
// Promise resolve, reject smisao
resolve je return_, reject je throw_ // to, zapazi, VAZNO 
export const parseMultipartForm = (req: Request): Promise<Result> => 
     new Promise((resolve, reject) => {})
------------------------
// shadow dom
kao iframe, scopeovan ceo dokument u custom tags, components 
moze da se podesi privatnost do potpuno zatvorenog
-------------
// pristup se svodi na ovo: // trivial
// jedno od ova dva:
const shadowRoot = shadowHost.shadowRoot
const shadowRoot = shadowHost.getShadowRoot()
// primer
var shadowHost = document.querySelector('main-dom-selector');
var elementInsideShadow = shadowHost.shadowRoot.querySelector('selector-in-shadow-dom');
--------
// moras kroz sve nested da ides jedan po jedan ako ih ima vise nested
https://stackoverflow.com/questions/68274994/how-do-i-click-a-button-inside-shadow-root-element
----------
// https://medium.com/torq-ai/shadow-dom-f4171d1e743a
// 3 different modes in Shadow DOM:  
1. Open mode - elementi i styles dostupni u outer dom 
2. Closed mode - styles and elements completely isolated, ali vidi se da je shadow dom
3. Shadow mode - closed mode + hides the existence of the Shadow DOM
------------
// events in shadow dom
// https://medium.com/torq-ai/shadow-dom-events-59a90068e612
----------------------
// measure elapsed time javascript
https://stackoverflow.com/questions/313893/how-to-measure-time-taken-by-a-function-to-execute
const startTime = performance.now();
const endTime = performance.now();
const elapsedTime = endTime - startTime;
----------------------
// custom events javascript
// https://blog.logrocket.com/custom-events-in-javascript-a-complete-guide/
// https://github.com/jameesjohn/custom-event-demo
event names 'myevent' are case insensitive // zapazi
----
// creating:
// created na 2 nacina:
1. Using the Event constructor
// ne salje data, nema arg // vazno
const myEvent = new Event('myevent', { // ime eventa
    bubbles: true,
    cancelable: true,
    composed: false
  })
// options:
// bubbles 
whether the event should be propagated upward to the parent element, parent can listen
za custom event default je false
za dom events je true
// cancelable
da li event.preventDefault() moze da ga canceluje or ignored
za dom events default je true
// composed
da li moze da bubble up from shadow dom to real dom, true da moze
----------
2. Using the CustomEvent constructor
// detail prop za slanje data, ostalo isto
const myEvent = new CustomEvent("myevent", {
    detail: {}, // eto
    bubbles: true,
    cancelable: true,
    composed: false,
  });
----------
// dispatching:
events can be dispatched to any object that extends EventTarget
all HTML elements, the document, the window
const myEvent = new CustomEvent("myevent", { // ovde ime eventa, zapazi
    detail: {},
    bubbles: true,
    cancelable: true,
    composed: false,
  });
const myElement = document.querySelector("#someElement");
myElement.dispatchEvent(myEvent);
---------
// listening
const otherElement = document.querySelector("#someOtherElement");
otherElement.addEventListener("myevent", (event) => {
    console.log("I'm listening on a custom event");
});
--------------
// primer
// ovako za reusable
const profileCard = document.querySelector(".profile-card");
const CARD_UPDATE_EVENT_NAME = "cardupdate"; // da se ne pogresi string

function dispatchCardEvent(data) {
  profileCard.dispatchEvent(
    new CustomEvent(CARD_UPDATE_EVENT_NAME, {
      detail: data, // eto detail, pass data
    })
  );
}
----
// dispatch primer
dispatchCardEvent({ image: event.target.result });
-------------------------
// lodash cesto koriscene
pick, omit
intersection - presek za arrays, not objects
cloneDeep, native verzija structuredClone
----------------------
// web component, kreira custom html element <mobile-button />, astro cactus // zanimljivo
customElements.define('mobile-button', MobileNavBtnClass);
---------------------------------
---------------------------------
// reading props from within object literal itself with this
// poneta: 
1. object literal nema this, mora da ga pretvoris u funkciju
2. ne moze iife u prop, te fje mogu da se pozivaju tek NAKON sto je objekt kreiran (vracen iz fje)
3. as_ const moze samo za object literals, ne za iife
// primer
export const ROUTES = (() => {
  // moze odmah return bez var
  return {
    // moze iznad da bude
    getTagRoute: function () { // mora function, a ne () => {}
      console.log('this', this.TAGS); // ROUTES je sad funkcija i dobio je njen this, jer poziva se na ROUTES.getTagRoute();
    },
    TAGS: '/blog/tags/',
  };
})(); // iife
// poziv moze tek nakon je object kreiran
ROUTES.getTagRoute();
------------
// drugi nacin da prosledis (postavis) this sa bind na funkciju iz object literal
ROUTES.getTagRoute = ROUTES.getTagRoute.bind(ROUTES);
originalni object literal se ne dira
// primer
export const ROUTES = {
  getTagRoute() { // moze name, value fje ovako skraceno
    console.log('this', this.TAGS);
    },
  TAGS: '/blog/tags/',
} as const;
ROUTES.getTagRoute = ROUTES.getTagRoute.bind(ROUTES);
-------------
methods su default public u javascript class_
------------------------
// new URL() object in js, props 
href: 'http://localhost:3000/blog/2023-01-19-example-article-3',
origin: 'http://localhost:3000',
protocol: 'http:',
username: '',
password: '',
host: 'localhost:3000',
hostname: 'localhost',
port: '3000',
pathname: '/blog/2023-01-19-example-article-3',
search: '',
searchParams: URLSearchParams {},
hash: ''
------
baseUrl je origin, domain je host
------------------
// najmanja biblioteka za vremena u js, mozda bolji od date-fns
https://bundlephobia.com/package/dayjs@1.11.12
------------------
// for in vs for of 
Feature	for...in	for...of
in  - over keys, indexes in array // item je key, mora obj[key] 
in - Objects
----
of - over values
of -Arrays, Strings, Maps, Sets
------------------
// concat absolute urls with URL()
da bi URL() pravilno nadovezivao segment na base, base mora da se zavrsava za '/' // fora
inace samo na hostname, izostavlja /first-segment  
new URL('my-id', 'http://localhost:3000/first-segment/').toString();