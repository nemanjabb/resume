// Content Scripts vs Background Scripts, chatGpt
// Content Scripts:
// Purpose: 
Content scripts are injected into web pages and have access to the DOM (Document Object Model) of the web page. 
They can manipulate the content of the page, respond to user actions, and interact with the webpage DOM elements.
// Execution Context: 
Content scripts run in the context of the web page and have limited access to browser APIs. 
They cannot directly access privileged browser APIs and are subject to the same-origin policy.
// Communication: 
Content scripts can communicate with the background script and vice versa using message passing. 
This allows them to coordinate actions and share information.

// Background Scripts:
// Purpose: 
Background scripts, also known as background pages or scripts, run in the background of the browser 
and have higher privileges than content scripts. They can access privileged browser APIs, 
such as storage, tabs, and alarms.
// Execution Context: 
Background scripts do not have access to the DOM of the web pages directly. 
They operate independently of the web pages and can continue running even when all browser tabs are closed.
// Communication: 
Background scripts can communicate with content scripts, other background scripts, and popup scripts 
using message passing. They serve as a centralized location for managing extension-wide functionality.

// ostali tipovi
Popup Scripts
Options Page Scripts
Content Security Policy (CSP) Scripts
DevTools Panel Scripts
Event Page Scripts (for Chrome Extensions)
------------------
// this is url in popup
window.location.href = moz-extension://217ba40e-ad16-4562-97db-ffe4a7af1be7/popup.html
-------------------------
-------------------------
// load the extension in Firefox:
1. navigate to about:debugging#/runtime/this-firefox 
2. load the manifest file from extension/firefox/manifest.json
-----------
// show extension debug console in Firefox
// https://stackoverflow.com/questions/12893981/logging-to-console-from-firefox-extension
1. in about:config, add a new option extensions.sdk.console.logLevel and give it the value "all"
2. Tools -> Web developer -> Browser console, ili Ctrl + Shift + j
-------------------
// load extension in Chrome:
1. navigate to chrome://extensions/
2. enable Developer mode switch 
3. select the entire Chrome extension folder extension/chrome


