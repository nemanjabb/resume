
/**
* General JavaScript notes from all articles on http://javascriptissexy.com/ blog. Autumn 2014.
*
*References:
*	http://javascriptissexy.com/
*	http://blog.mgechev.com/2013/04/24/why-to-use-publishsubscribe-in-javascript/
*	https://blog.risingstack.com/asynchronous-javascript/
*	https://addyosmani.com/resources/essentialjsdesignpatterns/book/ - Learning JavaScript Design Patterns
*
*
*
*
*/

//DELEGATI JS
------------
In JavaScript, functions are first-class objects, which means functions can be used in a first-class manner like objects, 
since they are in fact objects themselves: They can be “stored in variables, passed as arguments to functions, 
created within functions, and returned from functions” 
----------
A callback function is essentially a pattern (an established solution to a common problem), and therefore, 
the use of a callback function is also known as a callback pattern.

-----------

friends.forEach(function (eachName, index){//prosledjije ko poziva, tj forEach()
//u foreach this je window tj globalni

console.log(index + 1 + ". " + eachName); // 1. Mike, 2. Stacy, 3. Andy, 4. Rick
});

------------
CALLBACK JE ARG KOJI JE DEFINICIJA A NE POZIV
ovo(){} a ne ovo();
 When we pass a callback function as an argument to another function, we are only passing the function definition. We are not executing the function in the parameter. We aren’t passing the function with the trailing pair of executing parenthesis () like we do when we are executing a function.

----------------

It is “called back” (hence the name) at some specified point inside the containing function’s body. 

Even without a name, it can still be accessed later via the arguments object by the containing function. 

------------
This means the callback is essentially a closure.

As we know, closures have access to the containing function’s scope, so the callback function can access the containing functions variables, and even the variables from the global scope.
-------------

    // Make sure the callback is a function
    if (typeof callbackFja === "function") {
    // Call it, since we have confirmed it is callable
        callbackFja(options);
    }
-------------

angular $servisi su prakticno anotacije DI
---------------

Or else the this object will either point to the global window object (in the browser), if callback was passed to a global function.

Or it will point to the object of the containing method.

ako je GLOBALNA fja this je window, ako je lokalna onda spoljni objekat
-------------
call() apply() da postavis this
ako koristis this u definiciji za pristup propertijima gde se this odnosilo na objekat u kome je definisana fja, pa posle prosledis taj objekat u globalnu fju gde je this window

For now, know that every function in JavaScript has two methods: Call and Apply. And these methods are used to set the this object inside the function and to pass arguments to the functions. 
-------------
//prosledj fju kao param
In short, the getUserInput function is versatile: it can execute all sorts of callback functions with myriad of functionalities.

-------------
kad se koriste:

For asynchronous execution (such as reading files, and making HTTP requests)
In Event Listeners/Handlers
In setTimeout and setInterval methods
For Generalization: code conciseness
------------
//dovde javascript callbacks 
------------
prototype pattern kreiranje novih objekata kroz kloniranje starih, clone()
bez kreiranja novih podklasa kao abstract factory
bez poziva new, performanse
--------------
factory patern - kreiras factory = new uopsteniTip();
posle ides konkretniTip prom1 = factory.createKonkretniTip();
-------------
clojure podsetnik, 1. promenljive iznad lokalne ugnjezdene vidi sve, 
2. unutrasnju funkciju pozivas var unutr = spoljnaKojaVracaUnutr(); pa unutr();, promenljivim ne mozes da pristupis osim preko fje, enkapsulacija
-------------
-------------
//javacsript object

 it has five simple data types: Number, String, Boolean, Undefined, and Null
------------
if the property name is a number, it has to be accessed with the bracket notation.
var ageGroup = {30: "Children", 100:"Very Old"};
ageGroup["30"]

-------------
1. reference 2. primitivni tip  - po vrednosti, kopiranje
One of the main differences between reference data type and primitive data types 
is reference data type’s value is stored as a reference, it is not stored directly on the variable, 
as a value, as the primitive data types are

referenca - objekat, menja sve zavisne objekte svaki put, ne kopira se prilikom dodeljivanja

var person = {name: "Kobe"};
var anotherPerson = person;
person.name = "Bryant";

console.log(anotherPerson.name); // Bryant
console.log(person.name); // Bryant
------
primitivni se kopira// The primitive data type String is stored as a value
var person = "Kobe";  
var anotherPerson = person; // anotherPerson = the value of person
person = "Bryant"; // value of person changed

console.log(anotherPerson); // Kobe//ostao isti
console.log(person); // Bryant
------------------
svaki property ima 3 dodatna default=true atributa
Configurable Attribute: Specifies whether the property can be deleted or changed.
Enumerable: Specifies whether the property can be returned in a for/in loop.
Writable: Specifies whether the property can be changed.
------------------
2 nacina kreiranja objekta
var myBooks = {}; ili  var myBooks = {nesto:"nesto"};//literal
var mango =  new Object (); //konstruktor
----------
Constructor Pattern 

function Fruit (args) {object:"blabla"}
var pineappleFruit = new Fruit (args);

prototype Pattern for Creating Objects
function Fruit () {}
Fruit.prototype.color = "Yellow";

var mangoFruit = new Fruit ();
mangoFruit.zoviFju(); //
-------------
pristup propertiju

book.title
book["title"]
-----------------
da li postoji schoolName property u school objektu// 
"schoolName" in school //true//i nasledjeni i njegovi
njegovi school.hasOwnProperty ("schoolName")
--------
enumerating 
for (var eachItem in school) 
--------
delete property moze samo za objekat gde je definisan, nasledj i globalne ne moze//ali vraca true uvek
delete christmasList.mike; // deletes the mike property
------------
serijalizacija toString

var christmasList = {mike:"Book", jason:"sweater", chelsea:"iPad" }
JSON.stringify (christmasList);// Prints this string, navodnici: // "{"mike":"Book","jason":"sweater","chels":"iPad"}"
//za formatirano
JSON.stringify (christmasList, null, 4);

//obrnuto string to object
var christmasListStr = '{"mike":"Book","jason":"sweater","chels":"iPad"}';//string je ''
// Let’s convert it to an object
var christmasListObj = JSON.parse (christmasListStr); 

------------------
------------------
//javascript prototype

svaka funkcija ima property prototype koji sluzi za nasledjivanje, tu dodas fje i propertije da koristis u svim instancama

enumerable je nesto sto moze da se mota u for in loop
----------
__proto__ sluzi da se pristupi prototype u firefox i chrome
----------
prototype property, izvedena klasa, dodas fje i propertije na prototype propertiju i imas ih u svim instancama

//konstruktor
function PrintStuff (myDocuments) {
this.documents = myDocuments;
}
//dodas fju objektu na prototype propertiju//dodas u osnovnu klasu, pros
PrintStuff.prototype.print = function () {
console.log(this.documents);
}
//kreiraj objekat
var newObj = new PrintStuff ("I am a new Object and I can print.");

//poziv dodate fje//iz osnovne klase
newObj.print (); 
----------
prototype atribute
//An object’s prototype attribute points to the object’s “parent”—the object it inherited its properties from.
--------------
//atributi objekta
object attributes are prototype, class, and extensible attributes.//atribut built in property slobodan
--------------
//constructor property
var myObj = new Object ();
console.log(myObj.constructor); // Object()

var userAccount = new Account (); 
console.log(userAccount.constructor); // Account()
--------
//atributi, prototype attribute ili prototype object
za objekte kreirane sa new Object() i sa {}
prototype atribut je Object.prototype 

za objekte kreirane sa konstrukttor atribut je taKlasa.prototype
function Account () {}
var userAccount = new Account () // userAccount initialized with the Account () constructor and as such 
//its prototype attribute (or prototype object) is Account.prototype.
---------
//u javascriptu nema nasledjivanja
prototype nasledjivanje, prototype property

function Plant () {
	this.country = "Mexico";
	this.isOrganic = true;
}
// Add the showNameAndColor method to the Plant prototype property
Plant.prototype.showNameAndColor =  function () { //kad definises fje koristis .prototype, a kad pozivas onda direktno obj.fja()
console.log("I am a " + this.name + " and my color is " + this.color);
}

function Fruit (fruitName, fruitColor) {
	this.name = fruitName;
	this.color = fruitColor;
}

Fruit.prototype = new Plant ();//Fruit extends Plant
var aBanana = new Fruit ("Banana", "Yellow");
aBanana.showNameAndColor();//od plant

//Fruit’s prototype, which is Plant.prototype.//prototip je roditeljska klasa zdravo
------------
prototype chain (lancana lista)(atribut), trazi prototype roditelja (nnjegove fje i propertije) na gore sve dok ne nadje ili undefined
svi objekti nasledjuju Object.prototype
metode i propertiji Object su constructor, hasOwnProperty (), isPrototypeOf (), propertyIsEnumerable (), toLocaleString (), toString (), and valueOf ()
ECMAScript 5 also adds 4 accessor methods to Object.prototype.
-------------
//OOP javascript
Inheritance (objects can inherit features from other objects)
Polymorphism (objects can share the same interface—how they are accessed and used—while their underlying implementation of the interface may differ) 
Encapsulation (each object is responsible for specific tasks)

u javascriptu moguci samo Inheritance and Encapsulation
-----------
//enkapsulacija

However, whenever you want to create objects with similar functionalities (to use the same methods and properties), 
you encapsulate the main functionalities in a Function and you use that Function’s constructor to create the objects. 
This is the essence of encapsulation. 
----------
Combination Constructor/Prototype Pattern

function User (theName, theEmail) {
    this.name = theName;
	...
}
User.prototype = {
    constructor: User,//konstruktor property pise ti u prototype clanku gore
	//The one disadvantage of overwriting the prototype is that the constructor property
	//no longer points to the prototype, so we have to set it manually.
    saveScore:function (theScoreToAdd)  {
        this.quizScores.push(theScoreToAdd)
    },
	...
secondUser = new User("Peter", "Peter@examnple.com");
secondUser.saveScore(11);

//overwriting the prototype property with an object literal

In JavaScript, you add methods and properties on the prototype property when you want instances 
of an object to inherit those methods and properties. This is the reason we add the methods on 
the User.prototype property, so that they can be used by all instances of the User object.
//na svim objektima tipa User ne samo na tom jednom
-----------------
//Inheritance

inheritance is important: we can reuse code 
Overriding functions is another principle of OOP.

//Parasitic Combination Inheritance Pattern
Object.create(obj) method is that you pass into it an object that you want to inherit from, 
and it returns a new object that inherits from the object you passed into it. 
//vraca izvedeni obj od prosledjeni obj

 function inheritPrototype(childObject, parentObject) {
	var copyOfParent = Object.create(parentObject.prototype);
	copyOfParent.constructor = childObject;//copyOfParent je pom
	childObject.prototype = copyOfParent;//nasledjuje
}
//Whenever you overwrite an object’s prototype (object.prototype = someVal), you also overwrite the object’s constructor property
----------------------

//u fji-klasi promenlj bez this je privatna
function Question(theQuestion, theChoices, theCorrectAnswer) {//base klasa
    this.question = theQuestion;//samo propertiji
	...
}
 // Define the prototype methods that will be inherited
Question.prototype.getCorrectAnswer = function () {
    return  this.correctAnswer;
};

function MultipleChoiceQuestion(theQuestion, theChoices, theCorrectAnswer){//izvedena klasa
    Question.call(this, theQuestion, theChoices, theCorrectAnswer);//call() za postavljanje this
};
inheritPrototype(MultipleChoiceQuestion, Question);//nasledjivanje
//sad dodas nove metode izvedenoj klasi
//user.prototype koristis kad hoces da se odnosi na sve instance

// Override the displayQuestion method it inherited
MultipleChoiceQuestion.prototype.displayQuestion = function () {
    console.log(this.question);
};

----------------
//variable scope i hoisting

var myObj = {
  highValue: 20,
  constantVal: 5,
  calculateIt: function () {
    console.log(this); //ovde je this myObj
    setTimeout(function () {
      console.log(this); //ovde je this window globalni, zasto?//clojure, setTimeout() poziv i anonim definicija
    }, 2000);
  }
}
------------
//eto sta je hoisting, pomeranje svih promeljivih na pocetak

All variable declarations are hoisted (lifted and declared) to the top of the function, 
if defined in a function, or the top of the global context, if outside a function.

samo var x; a ne var x = 5;//on je hostuje gore var x; ali je posle dodeli = 5, pa je undefined
It is important to know that only variable declarations are hoisted to the top, 
not variable initialization or assignments 
----------
//hoisting fja
var myName;
function myName () {
console.log ("Rich");
}
// The function declaration overrides the variable name
console.log(typeof myName); // function
----------
// This is the variable assignment (initialization) that overrides the function declaration.
var myName = "Richard"; //jer dodeljena

function myName () {
console.log ("Rich");
}
console.log(typeof myName); // string 
----------
//dodela ne hoistuje se
var myName = function () {
console.log ("Rich");
} 
--------------------
--------------------
//this keyword clanak

when strict mode is being used, this holds the value of undefined in global functions 
and in anonymous functions that are not bound to any objects.
//this je objekat koji je pozvao funkciju

// A very common piece of jQuery code
$("button").click (function (event) {
	// $(this) will have the value of the button ($("button")) object
	// because the button object invokes the click () method
	console.log ($ (this).prop ("name"));
});
---------
$(this) je this wrapped it in the jQuery $() function.
----------
this nema vrednost u definiciji, dobije je tek kad se pozove fja na objekat (bilo koji)
----------
// all global variables and functions are defined on the window object
------------
//window.firstName
var firstName = "Peter",
lastName = "Ally";
//globalna fja
function showFullName () {
	console.log (this.firstName + " " + this.lastName);
}

var person = {
	firstName   :"Penelope",
	lastName    :"Barrymore",
	showFullName:function () {
		// fja u person objektu
		console.log (this.firstName + " " + this.lastName);
	}
}
//this je window
showFullName (); // Peter Ally
window.showFullName (); // Peter Ally
//this je person
person.showFullName (); // Penelope Barrymore

var anotherPerson = {
firstName   :"Rohit",
lastName    :"Khan"
};
//postavis this sa apply()
person.showFullName.apply(anotherPerson); // Rohit Khan
----------------
1 this when used in a method passed as a callback

var user = {clickHandler:function(event){alert("nesto");}};
$("button").click (user.clickHandler);//this je dugme a ne user, poziva se u dugmetovoj metodi, promenjen kontekst//this iz clojure-a veci prioritet
$("button").click (user.clickHandler.bind(user)); // mora preko bind() ili apply(), call() da mu prosledis user da mu bude this
--------------- 
//when strict mode is being used, this holds the value of undefined in global functions 
//and in anonymous functions that are not bound to any objects.

this is really just a shortcut reference for the invoking object

$ ("button").click (function (event) {//clojure, this je iz spoljne
	//definicija a ne poziv
    console.log ($ (this).prop ("name"));
});
------------
2 this u clojure
//prosta fora da preneses this u clojure, samo var prom = this; pa koristis prom.property

var user = {
  tournament: 'The Masters',
  data: [
    {name: 'T. Woods', age: 37},
    {name: 'P. Mickelson', age: 43}
  ],
  clickHandler: function (event) {
    var theUserObj = this;//sacuvaj this u prom
    this.data.forEach(function (person) {//javascript forEach nije jquery//forEach tu prosledjuje arg person, a ne prima return arg
										//poziva fju sa 1 arg jer anonim je definicija a NIJE POZIV
	obj.fjaKojaProsledjujeArg(function defFjeKojaPrimaArg(arg){definicija});//ispravno
	//a ne
	obj.fjaKojaPRIMAarg(function pozivFjeKojaVraca(arg){});//pogresno
      // Instead of using this.tournament, we now use theUserObj.tournament
      console.log(person.name + ' is playing at ' + theUserObj.tournament);
    })
  }
}
-----------
3 this when method is assigned to a variable
//ako dodeljujes showData delegat u promeljivu, moras user kroz bind(user) inace koristi globalni
var showUserData = user.showData.bind(user);
----------
4 this when borrowing methods
//Borrowing methods
appController.avg.apply(gameController, gameController.scores);//appController vlasnik fje, gameController samo podatke, preko this
---------------
---------------
//Create Chainable (Cascading) Methods 
//u fjama uvek vraca ceo pozivajuci objekat tj return this, a rezultat fjr cuva u promenljivoj currentUser u objektu
//osim u zadnjoj gde ne vraca nista

var userController = {

  currentUser:"",

  findUser:function (userEmail) {
	...
   return this;
  },
  formatName:function () {
	...
   return this;
  },
  createLayout:function () {
	...
   return this;
  },
  displayUser:function () {
   $(".members-wrapper").append(negde);
  }
 };
userController.findUser("test2@test2.com").formatName().createLayout().displayUser();
-----------------
-----------------
//12 Simple (Yet Powerful) JavaScript Tips - idiomi
---------
//FALSE
JavaScript Falsy Values: null, false, 0 undefined, NaN, and '' 
//TRUE
JavaScript Truthy Values: Anything other than the falsy values. 
------------
//OR ||
//To set default values, instead of this:
function documentTitle(theTitle)
if (!theTitle) {
  theTitle  = "Untitled Document";
  }
}
//use this
function documentTitle(theTitle)
  theTitle  = theTitle || "Untitled Document";
}
---------
//AND &&
function isAdult(age) {
   return age && age > 17 ;
}
---------
//sleva na desno
 userName && logIn (userName) || signUp ();
----------
var userID = userName && userName.loggedIn && userName.id
//If userName is truthy, call userName.loggedIn and check if it is truthy; if it is truthy, then get the id from userName.
//zadnji ce vratiti vrednost a ne bool
-----------
//IIFE
//ne zagadjuje global namespace//odmah poziv i to je to
//delegat nije poziv, ovo je poziv
(function (args) {
 // Do fun stuff
 }
)(args)//neimenovana funkcija def i pozvana//samo tako moze da se pozove jer nema ime
 
//imenovana immediately invoked func
(showName = function (name) {console.log(name || "No Name")}) (); // No Name//i pozvana

showName ("Rich"); // Rich//moze da se pozove, ima ime
---------------
//upotreba
1. da ne zagadjuje globalni scope, ista imena u bibliotekama itd
-----------------------
-----------------------
//jquery plugins

(function( $ ){
   $.fn.myfunction = function() {//fn is a shortcut for prototype in jQuery
      alert('hello world');
      return this;//vracas this za chainable function
   }; 
})( jQuery );//vraca this koji je js node//prima $ kao arg


$('#my_div').myfunction(); 
--------------
var x = $('#my_div');// selektor vraca node umotan u jquery, mozes da zoves jquery metode na njemu, isto i za this
x.blur() animate() itd//ili prazan jquery objekat za promasaj//samo metode, bez node
-------------
//find() je filter
$('#aParticularForm').find('input') //svi inputi u formi//samo iza selektora, ne moze $.find()
-------------
var y = $ ("#register1").click (function (event) {//y je od selektora samo
	return this;//unutra je DOM cvor, button
});
----------------
-----------------
//Apply, Call, and Bind Methods 

-----------------
//bind() da postavis this
//The bind () method is used primarily to call a function with the this value set explicitly. 
//bind () allows you to easily set which specific object will be bound to this when a function or method is invoked.
---------
//prosledis user objekat ako je globalna fja da ne uzima this window, nego iz userAccount
----------
//pozajmis userov metod

cars.showData = user.showData.bind(cars);
cars.showData (); // Honda Accord 27
---------------
//Function Currying, also known as partial function application, is the use of a 
//function (that accept one or more arguments) that returns a new function with some of the arguments already set. 

//od fje sa vise param bind ti vrati novu fju sa nekim args hardkodiranim (sa manje args)//overloading prakticno

var greetAYoungster = greet.bind (null, "", 16);
greetAYoungster ("Alex"); // "Hey, Alex."
---------------
//call() apply() isto ko bind() samo mozes i args pored this, call(thisObj, arg1Fje, arg2je...)
//apply() args kao polje
//call() args kao arg1, argg2...
--------------
//objekat pozajmljuje metode polja

Array.prototype.indexOf.call(anArrayLikeObj, "Martin");//call() moze
anArrayLikeObj.indexOf ("Martin");//bez call() ne moze
//indexOf() metoda polja , reverse(), push(), pop() itd
------------
//sa call() mozes da vidis sve arg prenesene fji

function doSomething () {
	var args = Array.prototype.slice.call(arguments);
	console.log (args);
}
doSomething ("Water", "Salt", "Glue"); // ["Water", "Salt", "Glue"]
-----------------
//variadic functions - functions that accept any number of arguments instead of a fixed number of arguments
-----------------
//ako zove fju sa new onda je ona this, ako je globalna onda je this window
-----------------
var ninjaB = new Ninja(); 
var ninjaB = new ninja.constructor(); //isto je
-----------------
prototype je osnovna klasa
-----------------
args fje mozes da pristupis bez prenosenja sa arguments
----------------
fn = this u okviru fje//fn je funkcija
----------------
funkcija.length = broj argumenata
----------------
//singletoni
object1 = {};
var user = new User();
---------------------
bar.prototype = foo;
var bar = Object.create( foo );//identicno
---------------------
//dobar privatni, public, static
function MyClass () { // constructor function
  var privateVariable = "foo";  // Private variable 
  this.publicVariable = "bar";  // Public variable 

  this.privilegedMethod = function () {  // Public Method
    alert(privateVariable);
  };
}

// Instance method will be available to all instance but only load once in memory 
MyClass.prototype.publicMethod = function () {    
  alert(this.publicVariable);
};

// Static variable shared by all instance 
MyClass.staticProperty = "baz";

//...
var myInstance = new MyClass();
---------------------------
//enkapsulacija - da mozes da imas property sa istim imenom i u osnovnoj i u izvedenoj klasi, multidim
-----------
//kad meces klik na vise elemenata uvek ide delegirani on() koji postavlja 1 klik na parent
$(document).on('focus','input, textarea, select', function () {
--------------
//IFrameovi

//From the parents perspective:
var iFrameValue = $('#iframe').get(0).contentWindow.myLocalFunction();//js promenljive
var iFrameValue = $('#iframe').get(0).contentWindow.myLocalVariable;
$("#iFrame").contents().find("#someDiv").removeClass("hidden"); //jquery select
//tj
$(document).ready(function(){
    $('#frameID').load(function(){
        $('#frameID').contents().find('body').html('Hey, i`ve changed content of <body>! Yay!!!');
    });
});

//Iz childa parnet.property ili fja
$('#DIVuParentu', window.parent.document).hide(); 

X-Frame-Options: SAMEORIGIN response
-----------------
//ovo je handler//izvrsava se na dogadjaj, a ne sekvencijalno
$(document).on('focus','input, textarea, select', function () {
	globalLastFocusedTabindex = $(this).attr('tabindex'); //save last focused input in iframe
});
--------------
web workers - multitreding u js, sa socketima
--------------
asinhroni javascript - event handleri i callbackovi
-----------------
event propagation - bubbling i capturing faza, nagore i nadole od roota-window
------------------
funkcija je objekat, tj klasa ima fje i atribute
-------------------
//ovo ce biti pozvano sa link(scope, element)
link: function($scope, $element) { $element.text('Planet: ' + planetName); }
---------------
apply() treba da se zove invoke()
--------------------
//iz asinhrone fje ne mozes da vratis

//ne moze
function GetUserById(id, func) {
	return $.getJSON('/api/Users/GetUserById/' + id + '/').done(function (data) {
		return data;
	});
}
//1. nacin, pass callback func
function GetUserById(id, func) {
	$.getJSON('/api/Users/GetUserById/' + id + '/').done(function (data) {
		func();
	});
}
//2. nacin return xhr objekat pa .done() na njemu gde treba
function GetUserById(id, func) {
	return $.getJSON('/api/Users/GetUserById/' + id + '/');
}
---------------
GetAllUserLog(id, function (logs) {//gde se poziva tu se postavi argument
	modal.html(Handlebars.templates.log(logs));
	modal.modal();
});
function GetAllUserLog(args, func) {//prenesena funkcija
	$.ajax({
		success: function (data) {
			func(data);//postavljen argument
		}
	});
}
--------------------
fje ne smes da definises unutar domready() nisu definisane, var modal = $(selektor) ide unutra
--------------
helper u handlebars je nacin da preneses funkciju u tmplejt, u kome se ona poziva imenom i argumentima, objekat prenosis i fje a ne samo propertije
-----------------
javascript hendleri su event loop i obrnuti pozivi (asinhroni)
-----------------
asinhroni model nesekvencijalno neblokirajuce izvrsavanje, vec registrovani hendleri, event.data
-------------
IIFE je poziv
-------------
$.each() za array
$(".nesto").each() za domai
jsArr.forEach() ne radi IE
for in za js objekat
--------------
hendleri, kontroleri, event loop arhitekturni mode;, jednostavno prosirenje registrovanjem novog dogadjaja
---------------
factory - virtual konstruktor, klasa u kojoj je selekcija koje klase se instanciraju//trivijal
---------------
var privatni, this public, override fje koju zelis, trivijalno

//definicija, nije poziv, izvrsava se kad se zove constructor
function izvedenaKlasa() {
	override fje koje hoces
	this.fja = function() { nova implememnt}//overajdujes fju u konstruktoru//od nule implem
}
//klasa
izvedenaKlasa.prototype = new osnovnaKlasa();
izvedenaKlasa.prototype.constructor = konstruktorIzvedene;

//u java moras da pozoves super() u constr izvedene
-----------------
continuation chain - asinhrnoni pozivi u js
-----------------------
//The Evolution of Asynchronous JavaScript
//https://blog.risingstack.com/asynchronous-javascript/

1. Callbacks //obrnuti poziv, prenesena fja kao argument sa svojim args
Something.save(function(err) {  
  if (err)  {
    //error handling
    return;
  }
  console.log('success');
});
//lose strane
it is easy to build callback hells or spaghetti code with them if not used properly
error handling is easy to miss
cant return values with the return statement, nor can use the throw keyword
---------
2. Promises//enkapsulacija ocekivanog dogadjaja, uspeha ili neuspeha

Both the then and the catch registers callbacks that will be invoked with either 
the result of the asynchronous operation or with the reason why it could not be fulfilled

Something.save()  
  .then(function() {
    console.log('success');
  })
  .catch(function() {
    //error handling
  })
 -------- 
3. Generators / yield
function* foo () {  
  var index = 0;
  while (index < 2) {
    yield index++;
  }
}
var bar =  foo();

console.log(bar.next());    // { value: 0, done: false }  
console.log(bar.next());    // { value: 1, done: false }  
console.log(bar.next());    // { value: undefined, done: true }  
----------
4. Async / await//ES7//async await par

async function save(Something) {  
  try {
    await Something.save()//randevu blokira nit dok ne dobije rezult
  } catch (ex) {
    //error handling
  }
  console.log('success');
} 
//all, join
async function save(Something) {  
  await Promise.all[Something.save(), Otherthing.save()]
} 
------------------------------
//mgechev kako pravi klasu, tj modul

/*
javni static, definisani na klasi
jedna kopija na nivou klase, prototip kopija na svakoj instanci
pravi const nije moguc

prototype fje su public i clanice
bez toga su private i clanice ako je referencirana u public clanici

 window.BlobReader = BlobReader; publikuje u scope, sa return bi moglo samo u 

promenljivu
*/

//mgechev kako pravi klasu, tj modul
;(function (global) {

	//konstruktor
	function BlobReader(blob, dataEndianness, endianness) {	}
	
	//static, na nivou klase, jedna kopija
	BlobReader.ARRAY_BUFFER = 'ArrayBuffer';
	
	//privatna fja, moze biti ref u nekoj public fji
	function invokeNext() {	}

	//javna fja clanica
	BlobReader.prototype.read = function (count, type, cb) {};

	//publikovanje u window.
	global.BlobReader = BlobReader;
	
	//browser i node modul
}(typeof window !== 'undefined' ? window : module.exports));

--------------------------------
--------------------------------
//Why I should use publish/subscribe in JavaScript http://blog.mgechev.com/2013/04/24/why-to-use-publishsubscribe-in-javascript/
// =========== Model =========== 

function Book(name, isbn) {
    this.name = name;
    this.isbn = isbn;
}
   
function BookCollection(books) {
    this.books = books;
}
    
BookCollection.prototype.addBook = function (book) {
    this.books.push(book);
    $.publish('book-added', book);
    return book;
}
    
BookCollection.prototype.removeBook = function (book) {
   var removed;
   if (typeof book === 'number') {
       removed = this.books.splice(book, 1);
   }
   for (var i = 0; i < this.books.length; i += 1) {
      if (this.books[i] === book) {
          removed = this.books.splice(i, 1);
      }
   }
   $.publish('book-removed', removed);//emitovanje jquery $.publish $.subscribe
   return removed;
}
    
// =========== View =========== 
var BookListView = (function () {
 
   function removeBook(book) {
      $('#' + book.isbn).remove();
   }
	//lokalne privatne fje
   function addBook(book) {
      $('#bookList').append('<div id="' + book.isbn + '">
  ' + book.name + '
</div>');
   }
  //module pattern, return javno, pozvano odmah iife init()
   return {
      init: function () {
         $.subscribe('book-removed', function (book) {
             removeBook(book);//view hendleri
         });
         $.subscribe('book-aded', function (book) {
             addBook(book);
         });
      }
   }
}());//ovde pozvano init()

//--------------------------------

//module pattern
var Twitter.Timeline = (function () {
	//priv clan
   var tweets = [];
   
   //priv fja
   function publishTweet(tweet) {
      tweets.push(tweet);
      //publishing the tweet
   };
   //javna fja
   return {
      init: function () {
         $.subscribe('tweet-posted', function (data) { //hendler
             publishTweet(data);//zove priv fju
         });
      }
   };
}());

var Twitter.TweetPoster = (function () {
   return {
       init: function () {
           $('#postTweet').bind('click', function () {
              var tweet = $('#tweetInput').val();
               $.publish('tweet-posted', tweet);//emit
           });
       }
   };
}());
---------------------
bubbling i tunneling se javljaju samo gde su objekti koji emituju i hvataju stabla, 
ima smisla i za emitovanje, moze neko na putu do roota da ga uhvati i canceluje
--------------
var that = this;//bekapujes this refer, petkovic
--------------
ne moras za fju koju prenosis da prenosis i argumente, mozes da ih ubacis u this sa $.proxy() ili apply()
---------------
var se hoistuje this ne

var func = function(){} 
this.func = function(){} 
function func()
---------------------------

//npm paket
(function(global, factory) {
	// CommonJS
	if (typeof exports === 'object' && exports) {
		module.exports = factory()
	}
	// AMD
	else if (typeof define === 'function' && define.amd) {
		define(factory)
	}
	// global
	else {
		global.cyrillicToLatin = factory()
	}
}(this, function () {//ovo je poziv gornje fje (global = this, factory = ova fja

	'use strict';

	return function(string) {
		var cyrillic = 'А_Б_В_Г_Д_Ђ_Е_Ё_Ж_З_И_Й_Ј_К_Л_Љ_М_Н_Њ_О_П_Р_С_Т_Ћ_У_Ф_Х_Ц_Ч_Џ_Ш_Щ_Ъ_Ы_Ь_Э_Ю_Я_а_б_в_г_д_ђ_е_ё_ж_з_и_й_ј_к_л_љ_м_н_њ_о_п_р_с_т_ћ_у_ф_х_ц_ч_џ_ш_щ_ъ_ы_ь_э_ю_я'.split('_')
		var latin = 'A_B_V_G_D_Đ_E_Ë_Ž_Z_I_J_J_K_L_Lj_M_N_Nj_O_P_R_S_T_Ć_U_F_H_C_Č_Dž_Š_Ŝ_ʺ_Y_ʹ_È_Û_Â_a_b_v_g_d_đ_e_ë_ž_z_i_j_j_k_l_lj_m_n_nj_o_p_r_s_t_ć_u_f_h_c_č_dž_š_ŝ_ʺ_y_ʹ_è_û_â'.split('_')

		return string.split('').map(function(char) {
			var index = cyrillic.indexOf(char)
			if (!~index)
			return char
			return latin[index]
		}).join('')
	}

}))
----------------
return function je javno, mozes da invokujes sa (), vratis fju - ce da je dodelis u fju
----------------
Promises are placeholders for values that'll be provided in the future. They allow composing asynchronous operations like synchronous ones
----------------
(function(){}()) krastavac, the reason you wrap a function expression is to avoid it become a function declaration//dekleracija - samo definicija potpisa
you wrap it in () to make it interpreted as an expression rather than a statement
you can immediately call a function expression but not a statement
statement - 1 linija sa ;
expression - if, function definicija

Expression: Something which evaluates to a value. Example: 1+2/x //podtip statementa
Statement: A line of code which does something. Example: def fje//js konzola undefined kad vrati
izraz i naredba

---------
(function(){}()) je isto sto i (function(){})()

//------------------------------
//------------------------------
//Addy Osmani - Learning JavaScript Design Patterns

//The Constructor Pattern 
1. var newObject = {};
2. var newObject = Object.create( Object.prototype );
3. var newObject = new Object();



//kilavo, svaki prop sa defineProp()//trivijal
var person = Object.create( Object.prototype );//Object//root tip
 
// Populate the object with properties
defineProp( person, "car", "Delorean" );
defineProp( person, "dateOfBirth", "1981" );
defineProp( person, "hasBeard", false );

//nasledjivanje
var driver = Object.create( person );//sad person, nasledjuje
// Set some properties for the driver
defineProp(driver, "topSpeed", "100mph");
--------------------
Car.prototype.toString = function () {
  return this.model + " has done " + this.miles + " miles";
};
Above, a single instance of toString() will now be shared between all of the Car objects.
-------------------
//module pattern
modul - object literal {}, var PRIVATNE, return {nesto: definFje...} JAVNE, to je to, (this javne, ne koristi ovde, object literal)//notorno
mozes i argument da prosledis jQuery ili _
//The Revealing Module Pattern //posebno ime za trivijalnost
return privatne lokalno definisane, trivijal, return {nekaJavna: lokalFja,...}
//Singleton - trivijal, test na undefined
var instance;//undefined
return {
    getInstance: function () {
      if ( !instance ) { instance = init();} //test na undefined
      return instance;
    }
--------------
//svaki patern navodi advantages, disadvantages, problem koji resava, usecase
//googlaj sa promisljenim recima, sve vec postoji negde, angularjs paterns, ne rteba ti dekompajler
//kad neko zna, zna i da tu nema preterane kompleksnosti, zna gde je kraj
-----------
kad fja vraca fju, znaci invokujes promenljivu koju si dobio
var fjaRez = fja1(); fjaRez();//kad mozes da preneses fju mozes i da je vratis, logika
-----------
funkcionalno programiranje je kao matematicko opisivanje problema (rekurzivno)
imperativno programiranje je kao davanje uputstva idiotu
------------
cista fja - izolovana, samo args, nema globalne, ne menja bazu... moze da se izvrsi vise puta, idepotentna
---------------
//function definition
function superPandaFunction() { …}

//function expression
var superPandaFunction = function() {…};
-------
fja1.call(noviThis)
fja1.apply(noviThis, argsObj) //js fje
--------------------
//Publish/Subscribe Implementation, razlika od observera ne poziva direktno metode subjekta, ne mora da ga linkuje, decoupled

publish() je poziv nad delegatima svih registrovanih objekata
// Storage for topics that can be broadcast or listened to
var topics = {};
-------------------------------
// && vraca zadnji truthy ili prvi falsy izraz
// cim naidje na falsy izlazi
const giftingUrl = inkindPage && inkindPage.inkindSituations.length > 0 && inkindPage.inkindSituations[0].giftingUrl;

// || vraca zadnji falsy ili prvi truthy izraz
// cim naidje na truthy izlazi

// izraz je vec pao, nema sta dalje da racuna
-----------------------------
// https://twitter.com/mgechev/status/1338497732273713152
const x = x || 'default'; // null, undefined, 0, '', false
const url ||= 'default_url'; // =|| novi izraz, logical or assignment
const x = x ?? 'default'; // samo za null i undefined, i to je to, nema sta oklevanje vise
const url ??= 'default_url'; // logical nullish asignment
---------------------
// https://www.youtube.com/watch?v=4KHiSt0oLJ0
// cors - cross origin resource sharing
// server kontrolise koji frontend moze da ga cita
Access-Control-Allow-Origin response header *, link or null, mora da match Origin from request header ili *, ako ga nema greska
OPTIONS zahtev kao izvidnica
za <img src=... /> nije potreban, ali za fetch
Origin request header
samo browser implementira, drugi http klijenti (server side) mogu da citaju sve
omogucava kontrolu deljenja inace bi svako mogao da cita
---------------------
// setTimeout 0
// https://stackoverflow.com/questions/3580068/is-settimeout-with-no-delay-the-same-as-executing-the-function-instantly
// fju u setTimeout gurne u queue iza postojceg konteksta

The reason is that setTimeout removes the function from the execution queue and it will only be invoked after 
JavaScript has finished with the current execution queue.

console.log(1);
setTimeout(function() {console.log(2)});
console.log(3);
console.log(4);
console.log(5);
//console logs 1,3,4,5,2

// Izvrsi se asinhrono, pomeri na kraj event loop-a

