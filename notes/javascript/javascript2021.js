
// notes
object drilling, can't read property x of undefined or null, za to postoje api modeli typescript tipovi
--------------
console.clear();
-------------
// kad trebas da uhvatis kasnjenje unutar neke vec postojece fje koja kasni, asinhrona
return new Promise(resolve => {
...
.animate({ opacity: 1 }, () => {
	  resolve();
	});
-----------------
default export moze da se importuje pod drugim imenom, imenovani ne moze, mora * as drugoIme
-----------------
optional chaining moze i na indexu polja, zapazi
thumbnails={featureVideos?.[0]?.[videoPlatformSettings]?.feature_video_thumbnails}  
------------------
gledaj da li je lakse da se specificira pozitivan ili negativan uslov, return ili ne
------------------ 

u svakoj funkciji treba da imas handled happy path, loading path (za async) i error path (if na vrhu)
------------------
http only cookie, nedostupan javascriptu
-----------------
// jira
labels desno sluze za search issues
-----------------
// delay with promise
await new Promise(resolve => setTimeout(resolve, 2000));
-----------------
// loading await
const returnFromFinally = (someFunction) => {
  try {
  setLoading(true);
    return someFunction()
  } catch (error) {
    return `Caught an error: ${error}``
  } finally {
	setLoading(false);
    // overrides all other returns
    return 'All done!'
  }
}
-------------------
// http error handling na frontendu?
kao na backend laravel, default error handler pa filter by type
-----------------
// calling async function without await intentionally
// https://stackoverflow.com/questions/55019537/is-possible-to-call-async-function-without-await-keyword-and-what-happens-if-we
if you don't need a result or a moment when it finished in that function, kao novi thread

1. Keep as is. usrs() will be run in the background completely and componentDidMount will continue running until its end.
2. Use await, so componentDidMount will wait for the return of usrs().
3. Use usrs().then(), so componentDidMount can continue, but the code specified in .then() is invoked after usrs() returns, if you need to do something that has to happen after usrs() is done.
-------------------
// razlika arrow function
this from outer scope
arguments keyword argumenti nisu dostupni
-------------------
// vs code
// files to include, drugo polje, ne glavno
npr trazis sve optimisticUpdate u OrganizersSection folderu koji nije u root
*/Orga*
// case sensitive jbg, i * na kraju
**? u path
------------------
parseInt('20px') // skida 'px'
------------------
examine events Firefox, pros, dobar
dom inspector kliknes na event labelu iza cvora odmah u domu
klik na desnu strelicu vodi te u debugger u kod, a na labelu samo preview koda
ev.stopPropagation() i event ne prolazi na parent
------------------
// scrollovanje dom
window.scrollTo() // apsolutno
window.scrollBy() // relativno
element.scrollIntoView(alignTo) // ubaci ga da bude vidljiv, gornja/donja ivica

divNode.offsetTop property returns the top position (in pixels) relative to the top of the offsetParent element
divNode.offsetParent element is the nearest ancestor that has a position other than static
// ako imaju zajednicki offsetParent
const offsetPosition = element.offsetTop - container.offsetTop - offset;
------------------
// izostavljanje await
ako ne koristis resultat async fje ili redosled nije vazan await ti ne treba
------------------
// prazna polja u javascriptu
// https://stackoverflow.com/questions/3746725/how-to-create-an-array-containing-1-n

Array.from(Array(10).keys())
//=> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
ter version using spread operator.

[...Array(10).keys()]
//=> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

Array.from({length: 10}, (_, i) => i + 1)
//=> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
------------------
// Decorators
// enhance single object
In object-oriented programming, the decorator pattern is a design pattern that allows behavior to be added to an individual object, either statically or dynamically, without affecting the behavior of other objects from the same class.
------------------
// ne mora dupli object i spread {{...x1, ...y1}}, moze var x1 = {a: 1, ...y1}
var x = {a: 1, b: 2};
var y = {c: 3};
var z = {d: 4, ...x};
z
Object { d: 4, a: 1, b: 2 } // dodao a, b

// overriduje iste
var w = {a: 5, ...x};
w
Object { a: 1, b: 2 } // pregazio a: 5 sa a: 1

// spread undefined se handluje ok, nema greske, samo ignorise, ...undefined
var q = undefined;
var e = {f: 6, ...q};
e
Object { f: 6 }
---------------------
// this u static funkciji pokazuje na klasu, bzvz
Yes, it’s always defined everywhere. In a static function it refers to the current class
Which is helpful for example for factory static functions, so you can “new this()”…
--------------------
null vs undefined ???, other langs?
--------------------
// Conditionally add a member to an object?
// https://stackoverflow.com/questions/11704267/in-javascript-how-to-conditionally-add-a-member-to-an-object
cesto zatreba

const a = {
  ...(someCondition && {b: 5})
};
-----------------------
// async fja svakako ceka
// sa await ceka sebe i ceka sve ispod nje, blokira
// bez await samo ona ceka, a ne blokira naredbe ispod, idu odmah
// await keyword blokira, ceka zaista
function sleep (fn, par) {
  return new Promise((resolve) => {
    // wait 3s before calling fn(par)
    setTimeout(() => resolve(fn(par)), 3000)
  })
}

console.log(1);
await sleep(console.log, 2) // sa await
console.log(3);
1
pauza
2 
3 

console.log(1);
sleep(console.log, 2) // bez await
console.log(3);
1
3
pauza
2
---------------------------------
// if B is a class, you’d need to bind the function
export const withBem = b_.with.bind(b_);
----------------------
// momment.js formats
D - 7
DD - 07
dddd - Wednesday
ddd -Wed
d - wrong

MMM - Dec
MMMM - December
----
// now
const CurrentDate = moment().format() // 2021-12-22T17:26:00+01:00
const secondsFrom1970 = moment().unix() // 1640190398
const CurrentDate = moment().toISOString(); // 2021-12-22T16:26:19.151Z
-----------------
// oba su foreach, zapazi
for...of - za polja, map, set, itd... - iterables
for...in - za objekte
----------------
// iife da dodas u objekat razlicite opcije za uslove
// vise od 2 sa ternary ? :
const env = {
  RESTURL_SPEAKERS: (() => {
    if (isDev) return 'http://localhost:4000/speakers'
    if (isProd) {
      return 'https://www.siliconvalley-codecamp.com/rest/speakers/ps'
    }
    if (isStaging) return 'http://localhost:11639'
    return 'RESTURL_SPEAKERS:not (isDev,isProd && !isStaging,isProd && isStaging)'
  })(),
  RESTURL_SESSIONS: (() => {
    if (isDev) return 'http://localhost:4000/sessions'
    if (isProd) return 'https://www.siliconvalley-codecamp.com/rest/sessions'
    if (isStaging) return 'http://localhost:11639'
    return 'RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)'
  })(),
}
-------------------------
// project object
// filter object za key - vraca rezultate na google
// https://stackoverflow.com/questions/38750705/filter-object-properties-by-key-in-es6
//reduce 1
const raw = {
  item1: { key: 'sdfd', value:'sdfd' },
  item2: { key: 'sdfd', value:'sdfd' },
  item3: { key: 'sdfd', value:'sdfd' }
};
const allowed = ['item1', 'item3'];
const filtered = Object.keys(raw)
  .filter(key => allowed.includes(key))
  .reduce((obj, key) => {
    obj[key] = raw[key];
    return obj;
  }, {});
----
// reduce 2
const allowed = ['item1', 'item3'];
const filtered = Object.keys(raw)
  .filter(key => allowed.includes(key))
  .reduce((obj, key) => {
    return {
      ...obj,
      [key]: raw[key]
    };
  }, {});
---------------
// delete i filter
const allowed = ['item1', 'item3'];
Object.keys(raw)
  .filter(key => !allowed.includes(key))
  .forEach(key => delete raw[key]);
------------------
// check if number js
parseInt('12px') vs Number()
// if valid number
!isNan('123') // handles 0 which is falsy
!!myNumber // ne handluje 0, pogresno, bug za rute
---------------------
// instanceof - koji je konstruktor bio
var p = new Person("Jon");
p instanceof Person
------------------------
// exporti is intex.ts zapazi
// samo 1 default moguc iz index.ts
// ovo je default iz index.ts, a ne Loading.tsx
// skraceno za {default<-index.ts as default<-Loading.tsx}
export { default } from 'components/Loading/Loading'; 
export { default as AnimatedLoading } from 'components/Loading/AnimatedLoading';

export * from 'components/Loading/ThirdLoading'; // nema default, prouci * jos malo, ili pitaj
---
// export tipa mora da ima rec type
export type { Props as SpinnerProps } from 'components/Loading/Spinner';
-----------------
coercing - cast to type, == and === e.g.
----------------
// globalThis - univerzalni alias za global object da ne brines o js env
// https://stackoverflow.com/questions/57157864/what-is-globalthis-in-javascript-what-will-be-the-ideal-use-case-for-this
// https://blog.logrocket.com/what-is-globalthis-why-use-it/
// global object i different javascript environments
window - browser
self - Web Workers
frames - browser
global - Node.js
----------------
// object dynamic key
isto kao myObject[myVar] u literal je {[myVar]: value} // zapazi [] oba
----------------
fetch ne baca rejected promise na 400 nego response.ok=false pa mora throw, axios baca
--------------
// u switch case moze {}
// tj mora ako deklarises promenljivu u case:, bez {} vazi i van case, nezgodno
// https://stackoverflow.com/questions/50752987/eslint-no-case-declaration-unexpected-lexical-declaration-in-case-block
// no-case-declarations eslint error
----------------
// regex sa promenljivom i string literal
// https://stackoverflow.com/questions/43390873/template-literal-inside-of-the-regex
// escape must use 2x \\
cy.url().should('match', RegExp(`/${fakeUser.username}/post/\\d+`, 'i'));
--------------------
// prefix, postfix increment operator ++, --
// https://www.valentinog.com/blog/python-for-js/
const a = 34;
const b = ++a; // a=35, b=35
const b = a++; // a=35, b=34
---------------------------
// 5 Async + Await Error Handling Strategies - wes bos
// https://www.youtube.com/watch?v=wsoQ-fgaoyQ
rejection (promise) error i thrown (exception) error nisu isto
.catch() ne hvata thrown exception - mozda smo exception unutar promisea
-----------------------------
// how programming languages handle arguments and return values:
// C, C++, C#, JavaScript su slicni, pass by value, return by value
In C, C++, and Java, arguments are passed by value by default, but you can use the & operator to pass arguments by reference. 
Return values are also passed by value.
----
In C#, arguments are passed to methods and functions by value by default, but you can use the ref keyword to pass arguments by reference. 
Return values in C# are also passed by value.
----
In JavaScript, arguments are passed by value, and you cannot pass arguments by reference. 
Return values are also passed by value.
----
// python pass and return by reference
In Python, arguments are passed by reference, but the references are passed as values. 
This means that you cannot use the & operator to pass arguments by reference, but you can modify the original arguments directly inside the function_. 
Return values are also passed by reference.
----
In PHP, arguments are passed by value by default, but you can use the & operator to pass arguments by reference. 
Return values are also passed by reference.
--------------------------
// ceo lodash je napravljen immutable
// tj sve fje koje vracaju novi, ne mutiraju prosledjeni, paradigma
ni jedna fja u lodash ne mutira original
--------------------------
datetime iso string moze da se sortira, bez new Date()
--------------------------
// immutable sort
const newArray = array.toSorted()
--------------------------
// array.reduce()
// petlja sa memorijom
// Array Reduce in 100 seconds - Fireship
// https://www.youtube.com/watch?v=tVCYa_bnITg
// tipican primer sumiranje niza
const array1 = [1, 2, 3, 4];
// 0 + 1 + 2 + 3 + 4
const initialValue = 0;
const sumWithInitial = array1.reduce(
  (accumulator, currentValue) => accumulator + currentValue,
  initialValue
);
// eto, to je to
accumulator - vrednost VRACENA iz prethodne iteracije
initialValue - acc za 0-tu iteraciju
-----
// ovo, zapazi, GLAVNA FORA
kad tumacis reduce, prvo pogledaj initial value i return value za sledeci akumulator
-----
// https://www.youtube.com/watch?v=g1C40tDP0Bk
ako se izostavi initialValue first elem je default initial value, jedna manje iteracija
-----------
// chatGpt
// filter preko reduce
prodje kroz arr, push u [] ako prolazi uslov, vrati acc
map je ovo isto bez uslova, samo push
-----
function filter(arr, callback) {
  return arr.reduce((filteredArr, currentValue) => {
    if (callback(currentValue)) {
      filteredArr.push(currentValue);
    }
    return filteredArr;
  }, []);
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const evenNumbers = filter(numbers, num => num % 2 === 0);
------
//map preko reduce, primenjuje callback na svaki el
function map(arr, callback) {
  return arr.reduce((mappedArr, currentValue) => {
    mappedArr.push(callback(currentValue));
    return mappedArr;
  }, []);
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const doubledNumbers = map(numbers, num => num * 2);
------
// find with reduce
function find(arr, callback) {
  return arr.reduce((result, currentValue) => {
    if (result) return result; // nasao ga u predhodnoj iteraciji
    return callback(currentValue) ? currentValue : undefined; // vrati currentValue ako proso predikat
  }, undefined); // init nije nasao
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const foundNumber = find(numbers, num => num === 7);
---------
// some, isto ko find, samo vraca boolean
function some(arr, callback) {
  return arr.reduce((result, currentValue) => {
    if (result) return true;
    return callback(currentValue);
  }, false);
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const hasEven = some(numbers, num => num % 2 === 0);
-----------
// sort ide sa splice() i insertion sort, nije optimalno
-----------
// forEach - ne treba mu acc i inital value, samo prodje kroz arr
// forEach vraca undefined
function forEach(arr, callback) {
  arr.reduce((_, currentValue) => callback(currentValue), undefined);
}
------------------
// Object.entries() - polje tuple [key, value] parova od objekta, iterate over object
// slicno kao for...in loop
const person = { name: 'John', age: 25, job: 'developer' };
const entries = Object.entries(person);
console.log(entries);
/*
[
  ['name', 'John'],
  ['age', 25],
  ['job', 'developer']
]
*/
------
// lako vadi key i value iz objekta za polja, [name, score] destruct
const scores = {
  John: 90,
  Jane: 85,
  Michael: 80,
  Sarah: 95
};

const totalScore = Object.entries(scores).reduce((acc, [name, score]) => {
  return acc + score;
}, 0);
------------------------
// iterate over object
In general, Object.entries() is a good choice when you want to iterate over an objects properties 
and have access to both the key and value in each iteration. 
------
// slicne fje
Object.keys() - polje samo kljuceva 
Object.values() - polje samo values
Object.getOwnPropertyNames() - kao Object.keys() including non-enumerable properties.
for...in loop: - iterate over object keys, include properties from the objects prototype chain
for...of loop: - iterate over object values, include values from the objects prototype chain.
-------------------------
// javascript IMA value (primitive) types i refeerence types, chatGpt
1. value type 
variable holds the value itself
// tipovi:
Numbers (e.g. 1, 3.14)
Strings (e.g. "hello", 'world')
Boolean (e.g. true, false)
Symbol
BigInt
-----------
2. reference type 
variable holds a reference to the memory location where the value is stored - pointer
// tipovi:
Objects (e.g. {x: 1, y: 2})
Arrays (e.g. [1, 2, 3])
Functions (e.g. function(){})
-----------------------
// deep clone
// https://www.builder.io/blog/structured-clone
structuredClone(obj) // taj, podrzan svuda
// ova dva losija
_.cloneDeep(obj)
JSON.parse(JSON.stringify(obj))
-----------------------
// stavi u notes u js i python conditionals najcesci kako se proveravaju // OVO, napravi listu
// bezbedni uslovi - robusna aplikacija
-----
if conditional chacks are robust enough and consistent
if (!(links?.assets?.block?.length > 0)) return { renderNode: null }
-----
const options = html?.links ? renderOptions(html.links) : undefined
documentToReactComponents(html.json, options)
------------------------
// filter falsy values from array
data = data.filter(Boolean)
-----------------------------
fora ima online tools js object to json, ne mora rucno, mnogo brze
------------
ideja za tutorial ili repo, precizno pisanje uslova, negacija npr
------------------------------
// instanceof, runtime typecheck
// ne ispituje props nego poredi constructorFn refs
The instanceof_ operator in JavaScript is used to check whether an object is an instance 
of a particular class_ or constructor function_. It determines if an object has the prototype of 
a specified constructor in its prototype chain.
----
instanceof_ operator checks the inheritance relationship between classes at runtime
-----
// u typescript
samo dodatno compile time type guards
----------
za props i runtime type check api response i errors mora zod, in ili hasOwnProperty
-------------
// await sleep()
await new Promise(r => setTimeout(r, 2000));
-------------
// flatten multidimensional array
https://stackoverflow.com/questions/10865025/merge-flatten-an-array-of-arrays
const merge3 = arrays.flat(1);
----------------------
constructor function_ - fja koja se poziva sa new // jasno i prosto
i uglavnom ima this unutra
function Person(name) {
  this.name = name;
}
----------------------
// trim leading and trailing commas and spaces, trim left, right prakticno
.replace(/(^\s*,)|(,\s*$)/g, ''); 
// , MON 09:00:00-17:00:00, TUE 09:00:00-17:00:00
----------
Object.values(object) je object.toArray() prakticno // eto
-----------------
// reduce ima jednu promenljivu koju prenosi u svaku iteraciju, accumulator
// globalna prom pre petlje
// sumiranje brojeva je uvek tipican primer
const businessHoursString = result.data.periods
.reduce(
  (accumulator, period) =>
    `${accumulator}, ${period.dayOfWeek} ${period.startLocalTime}-${period.endLocalTime}`,
  ''
)
--------
// return value
reduce return value je AKUMULATOR // zapazi, vazno
a ne zadnji return od callbacka // iako je isto
return od callbacka je akumulator u sledecoj iteraciji
--------
// chatGpt
// i jeste globalna var i petlja, bas prosto
// accumulator je izvan scope callbacka, zato jeste memorija izmedju iteracija
function reduce(array, callback, initialValue) { // potpis fje
  let accumulator = initialValue === undefined ? undefined : initialValue;

  for (let i = 0; i < array.length; i++) {
    if (accumulator === undefined) {
      accumulator = array[i];
    } else {
      accumulator = callback(accumulator, array[i], i, array); // potpis callbacka
    }
  }

  return accumulator;
}
-------------------
// u object literalu u fjama this jeste obj literal u kome su definisane
const objLiteral = {
  firstName: 'john',
  lastName: 'doe',
  fullName: function() {
    return this.firstName + this.lastName;
  }
}
-----------------
// u try catch, finally se izvrsava cak i ako vratis u try // zapazi
try {
  file.open()
  return;
} catch() {

} finally {
  // izvrsi bez obzira na return gore
  file.close()
}
-------------------------
// bind vs call vs apply
bind VRACA fju sa this postavljenim
call i apply POZIVAJU fju sa this postavljenim i args
call - args as args
apply - args as array
------
// bind
functton welcome() { console.log(`Welcome, ${thts.name}`) }
const sloba = { name: 'Sloba' };
const welcomeSloba = welcome.bind(sloba); // postavi this
welcomeSloba(); // Welcome, Sloba // pa poziv
--------
// call
function weicome(firstName, lastName) { console.log(`Welcome, ${this.name}! Nice meeting you ${firstName} ${lastName}`); 
const sloba = {name: 'Sloba'}; 
welcome.call(sloba, 'Mary', 'Watson'); // odmah poziva, sa postavljenim this i args
--------
// apply
function weicome(firstName, lastName) { console.log(`Welcome, ${this.name}! Nice meeting you ${firstName} ${lastName}`); 
const sloba = {name: 'Sloba'}; 
welcome.apply(sloba, ['Mary', 'Watson']); // sve isto samo args as array
-------------------------
// memory leaks uzroci:
// https://www.youtube.com/watch?v=IkoGmbNJolo
1. global vars, window.x = "nesto"
2. vars in setTimeout and setInterval callbacks
3. removed dom node refs in object, but not deleted from object
-------
performance and memory tabs u chrome
--------------------
// NodeListOf<HTMLElement> vs HTMLElement[], vazno
// NodeList podrzava live update dom (add class...) ali ne podrzava filter i map
const commentElements: NodeListOf<HTMLElement> = document.querySelectorAll<HTMLElement>(commentSelector);
// array podrzava filter, ali ne podrzava live update, mora append na container
filteredArray.forEach(element => { container.appendChild(element); });
----------------
// IntersectionObserver callback je async, zato i ima callback
async better performance jer ne blokira
const observer = new IntersectionObserver((entries) => {...})
-----------
setInterval je watch zapravo // zapazi
----------------
gledaj na kom elementu je scroll, nije uvek na html, moze modal 
----------------
ovako mozes da vadis vrednosti iz async callbacks
const value = await Promise(resolve => resolve()) 
Promise(resolve) u stvari postavlja return value za async funkciju // zapazi
---------------
const fja moze ispod ako se iznad koristi u drugoj fji, a ne global scope // zapazi
global scope se kreira pre svih vars u fjama
iako const nema hoist
// primer
const markAsRead = () => {
  const latestCommentUpdater = createLatestCommentUpdater( ...  );
};
const createLatestCommentUpdater = () => { ... }
------------
moze vars let u closure da izbegnes dodatnu petlju za max elem niza, chatGpt generisao
getResult() => ({closureVars}) da vratis rezultat posle
instancira se pre petlje, i vadi result posle petlje, update fju poziva u petlji //zapazi
------------
// datetime libs
date-fns, Day.js, Luxon, moment.js deprecated
Jeste, s tim što tree-shaking nije osobina biblioteke nego bundlera, 
a da bi biblioteka mogla iskoristiri tu osobinu bundlera ona mora biti modularno napisana. 
To ti omogucava da ako koristis 3 funkcije biblioteke od 30 kôd ostalih bude odsecen. 
Tree-Shaking je zapravo to, odsecanje kôda koji se ne koristi.
--------------
da li svaka funkcija treba da validira svoje ulazne argumente ili treba spolja da se validiraju pre prosledjivanja?
error states sa if pa return null, undefined ili throw pa try catch? neces da srusis aplikaciju
sta zapravo moze da pukne i gde to ide, koje posledice, kako handleovati
---------------
// povratni tip funkcije
funkcija moze da vrati samo value tip ako baca exceptione, a ne da vraca undefined
exceptioni ne ulaze u potpis
// tacno, velika fora, | null izbegavas bacanjem exceptiona i to je to // ZAPAMTI
export const getThreadIdFromDom = (): string | null => { ... }
// poenta: 
kad god je nesto tip | null trebao si da bacis exception, a ne da vracas null | undefined
------------
// reject() je throw za new Promise((resolve, reject) => { ... })
reject('Thread not found');
---
izbegavaj sa if(nesto) da pravis 2 funkcije u funkciji, nego 2 funkcije odmah
----
treba da razlikujes sta je greska (exception), a sta lose ali validno stanje, no data npr.
// ako ima loop
ako hvatas try/catch exception u parent fji izlazi iz loop, ako ne mora try catch u iteraciji
------------------
// scroll element (comment) to top of the modal or window
const headerHeight = getHeaderHeight();

if (modalScrollContainer) {
  // modal calculation
  const commentOffsetTop = commentElement.getBoundingClientRect().top;
  const modalOffsetTop = modalScrollContainer.getBoundingClientRect().top;

  const targetScrollTop =
    modalScrollContainer.scrollTop + commentOffsetTop - modalOffsetTop - headerHeight;

  modalScrollContainer.scrollTo({
    top: targetScrollTop,
    behavior: 'smooth',
  });
} else {
  // window calculation
  window.scrollTo({
    top: commentRect.top + window.scrollY - headerHeight,
    behavior: 'smooth',
  });
}
---------------------
// mece sve exports iz fajla u MyObject objekat i to je to, kao namespace
// slicno je i u python, file je module
import * as MyObject from 'lib'
MyObject = {
  defult: ...
  NamedImport: ...
  ...
}
slicno je i kad su komponente grupisane u objekte // trivial
Modal.Header itd.
