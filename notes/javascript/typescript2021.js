--------------
| unija tipova
& presek // kad je objekat to je unija bzvz
--------------
//ovo izmedju<> fja<Tip> je povratni tip fje
return useApi<InkindPageAPIModel[]>(inkindRouteIds ? path : null, config, {
--------------
export const recipientAddressFormSchema: (ref?: React.RefObject<HTMLElement>) => FormSchemaEntry[] = ref => [

    (ref?: React.RefObject<HTMLElement>) => FormSchemaEntry[] je tip od recipientAddressFormSchema fje
    ref je pravi argument, pros
------------------
type DeepPartial<T> = {
    [P in keyof T]?: DeepPartial<T[P]>;
  };
------------------
tipovi modela na klijentu treba da se poklapaju sa ogranicenjima na bekendu, argumenti api
sta je required, a sta ne
------------------
// enums
// https://www.typescriptlang.org/docs/handbook/enums.html
enum LogLevel {
    ERROR,
    WARN,
    INFO,
    DEBUG,
  }
  
  /**
   * This is equivalent to:
   * type LogLevelStrings = 'ERROR' | 'WARN' | 'INFO' | 'DEBUG';
   */
  type LogLevelStrings = keyof typeof LogLevel;
  ------------------
  // object kao enum
  const ODirection = {
    Up: 0,
    Down: 1,
    Left: 2,
    Right: 3,
  } as const;
  
  type Direction = typeof ODirection[keyof typeof ODirection];
  function run(dir: Direction) {}
------------------
// api modeli na klijentu opcioni
// za GET
opcioni ne baca gresku ako ga server ne posalje
nego gde ga koristis, ne mora da postoji
export interface IClaim {
  id: string;
  pageTitle?: string;
--------------------
// ts-node
ts-node is a TypeScript execution engine and REPL for Node.js
kao node file.js ts-node file.ts // u memoriji prevede pa izvrsi js verovatno...
------------------
// override property type in object
// https://stackoverflow.com/questions/41285211/overriding-interface-property-type-defined-in-typescript-d-ts-file
& je za type, a extends za interface
---
interface A {
  x: string
}
export type B = Omit<A, 'x'> & { x: number };
---
// for interface:
interface A {
  x: string
}
interface B extends Omit<A, 'x'> {
  x: number
}
----------------------
// object + additional props
// njegovo, cuva tip ulaznog objekta, bolje
type ObjectWithDates = { createdAt: Date; updatedAt: Date };
type ObjectWithStrings = { createdAt: string; updatedAt: string };

const datesToStrings = <T extends ObjectWithDates> (_object: T): Omit<T, keyof ObjectWithDates> & ObjectWithStrings => {
  return {
    ..._object, // has additional props
    createdAt: _object.createdAt.toISOString(),
    updatedAt: _object.updatedAt.toISOString(),
  };
};
---
// moje
type ObjectWithDates = { [key: string]: any } & { createdAt: Date; updatedAt: Date };
----------------------
// tip od kljuceva ili vrednosti objekta
// https://stackoverflow.com/questions/53662208/types-from-both-keys-and-values-of-object-in-typescript
const QueryKeys = {
  POSTS_HOME: 'posts-home',
  POSTS_PROFILE: 'posts-profile', // ili readonly mesto const
  POSTS_DRAFTS: 'posts-drafts',
} as const; // mora as const inace samo string a ne unija, zapazi

type Keys = keyof typeof QueryKeys; // "POSTS_HOME" | "POSTS_PROFILE" | "POSTS_DRAFTS"
type Values = typeof QueryKeys[Keys]; // 'posts-home' | 'posts-profile' | 'posts-drafts'
---
type QueryKeysType = typeof QueryKeys[keyof typeof QueryKeys]; // jedna linija
---
typeof konvertuje vrednost (objekat) u tip
keyof pravi uniju konstanti od kljuceva objekta

tipovi i vrednosti ne mogu da se mesaju i dodeljuju // ne bas, mogu da se mesaju ali ne da se dodeljuju
---------------------
// TypeScript type narrowing
// https://www.carlrippon.com/6-ways-to-narrow-types-in-typescript/
// type predicate
function isValidRating(
  rating: any
): rating is Rating { // vrac boolean
----
// assertion signature - baca izuzetak plus boolean
function checkValidRating(
  rating: any
): asserts rating is Rating 
throw_ new Error("Not a rating");
----------
// union types and conditions - ODLICNO
// https://twitter.com/housecor/status/1489975825529200644
// https://www.carlrippon.com/6-ways-to-narrow-types-in-typescript
Use a truthy condition to remove null/undefined
Use "typeof" to narrow a union of primitives
Use "instanceof" to narrow class_ types
Use "in" to narrow object types 
Use a function_ type_ guard with "is" to check the value and establish its type.
---------------------
unknown je any kad ne planiras da pristupas propertijima sa obj.nesto
any.biloSta
unknown.nista
-------------------------
// generic arrow function
// https://stackoverflow.com/questions/32308370/what-is-the-syntax-for-typescript-arrow-functions-with-generics
const foo = <T,>(x: T) => x;
// ili
const foo = <T extends unknown>(x: T) => x;
// zapazi
// poenta: povratne tipove mozes da izracunas na osnovu tipova argumenata ili arg fja koje su args
--------
// i tip moze da ima argument, ne samo fja
export type NextApiRequestWithResult<T> = NextApiRequest & { result: T };
---------------------------
// overloading functions
// Arrow functions dont support overloading
// https://stackoverflow.com/questions/50752853/typescript-overload-method-with-arrow-function
function createUsers(n: number): ClientUser[];
function createUsers(n: number, isServer: true): User[];
function createUsers(n: number, isServer = false): User[] | ClientUser[] { ... }
------------------
// get function args and return value types
args - Parameters<T> // [0], [1]...
return_ type - ReturnType<T>
-----
// access property type inside interface
MyInterface['myProp']
// isto sto i // zapravo nije isto...
Pick<Formatter, 'with'>
-----------------
// strictNullCheck tsconfig.json - point
1. definisi eksplicitno NekiTip | null 
2. svuda provera sa if ili && pre pristupa, i to je to
3. ili obj.nesto || 'fallback'
isto i undefined
------
?? je isto kao || samo za null i undefined
za 0, false, '', Nan nije, i to je to, pros
----------
zapazi kada vracas null iz funkcije, a kada bacas exception
kad vratis null return_ type mora da bude Nesto | null, za exception ne
------
compile time null checks vs runtime type_ checks // zapazi
-----------
// convert type to non-nullable/undefined - odlicno
const onDrop = useCallback<NonNullable<DropzoneOptions['onDrop']>>(){...}
-------
// Required za prop?: nesto;
// suprotno od Partial<> // i to je to
type MyOptionalType = {
  confirmPassword?: string;
};

type MyRequiredType = Required<MyOptionalType>;
----------
// za tip kljuceva keyof, za tip value-a [], za tip promenljive typeof
type MyType = typeof MyObj[keyof typeof MYObj];
--------------------
// tsconfig.json location
tsconfig.json mora da bude u istom (root) folderu u kome i package,json
ako ti treba u child folderu moras u tsconfig.child.json da extendujes root tsconfig.json
-----
// cypress/tsconfig.json - child tsconfig.json
{
  "extends": "../app/tsconfig.json",
  "compilerOptions": {
    "target": "es5",
    "lib": ["es5", "dom"],
    "types": ["cypress", "node", "@testing-library/cypress"],
  },
  "include": ["../node_modules/cypress", "**/*.ts"]
}
---------------------
// decorators in ts/js ARE just higher order functions, like in python
// take function as arg, enhance and return it
---------------------
// typescript
interface_ ne mozes da extendujes sa type, logicno
tada koristis type sa &
---------------------
// type narrowing, type guard function predicate, :b is Necklace - povratni tip
process of reducing the type of a value based on specific conditions
poenta: u if blok mozes da zoves object.fja()
------
// narrowing je ispitivanje tipa u uslovu i pozivanje fja u bloku
// na uniju bez if ne bi mogao da zove obj.fja() // POENTA
// za union tipove uvek ide ovo // TO
let value: string | number;
value = "Hello, world!";

// tip u uslovu
if (typeof value === "string") {
  console.log(value.toUpperCase());  // "HELLO, WORLD!"
  // The type of value is now narrowed to 'string' within this block
}
-----
// typeof za primitivne, instanceof za objekte koji imaju constructor
if (typeof value === "string") { value.toUpperCase(); }
if (animal instanceof Dog) { animal.bark();}
----------
// type guard predicate, is fja, eto
interface Necklace{
  kind: string;
  brand: string;
}
interface Bracelet{
  brand: string;
  year: number;
}
type Accessory = Necklace | Bracelet;

// evo predikat fja na osnovu object.someProp
// b is Necklace je povratan tip, poenta
// i unutra proverava tip, druga poenta
// b je i argument i u is return type, treca poenta
const isNecklace = (b: Accessory): b is Necklace => {
  return (b as Necklace).kind !== undefined
}
// poziv, obicna boolean fja
console.log(isNecklace(bracelet)) // logs false
---------
// jos jedan primer
function isString(value: string | number): value is string {
  return typeof value === "string";
}
------
// VELIKA POENTA, za sve staticke jezike
tipovi - compile time
values - runtime
type_ checking, samo validira kod at compile time // to
----
types do not exist at runtime
tipovi samo proveravaju ispravnost koda
kompajler ih sklanja do runtime
------
tipovi se odredjuju na osnovu:
1. type annotations // x: number, dekleracije
2. type inference // implicitno zakljucivanje
3. type guards // conditions
--------------
// ts-node - tool da izvrsis sa node, nije runtime, poenta
it uses the TypeScript compiler to compile the TypeScript code on the fly
and then executes the generated JavaScript code
---------
run TypeScript code by using the Node.js runtime
---------
// deno - isto ko ts-node, javascript runtime, koji moze in fly da izvrsi typescript
deno jeste runtime
runtimes: node, deno, browser
ne postoji typescript runtime
--------
// translators, prevodioci
1. interpreter - jedna po jedna linija - V8, REPL
2. compiler - ceo program odjednom u masinski kod
----
//  runtime environments
runtime is a software environment that provides the necessary resources and services for executing a program
includes components:
memory manager, a thread scheduler, an I/O system, and more
primeri: node.js, JVM, Python
----
// interpreteri
python, javascript V8, ruby, 
----
python interpreter: REPL
python runtime: interpreter, std library, OS
----------------------
DSL - sql, html
-----------
// Programmatic interfaces
also known as APIs (Application Programming Interfaces), are a way for different software systems 
to communicate with each other
// primeri
REST APIs, SOAP APIs, GraphQL APIs, gRPC APIs, Webhooks, RPC, CORBA, JDBC, ADO.NET
----------------------------------
typescript augmenting types, declare, pogledaj na youtube
----------------
type guards and type narrowing, inference su veliki deo rada sa typescript
handling user | null | undefined
------------------------------
// run .ts files as scripts
// da mozes da imas eksterne fajlove sa import i run sa ts-node
// vrh fora, da radi ovo, chatGpt mi rekao
"compilerOptions": {
  "baseUrl": ".",
  "paths": {
    "@/*": ["./*"]   /*// zatvorio sam comment ovde
  },
npx ts-node --require tsconfig-paths/register scripts/twilio-run.ts
// ovaj paket
https://github.com/dividab/tsconfig-paths
----------
// za ts-node u tsconfig.json da izbegnes --compiler-options '{\"module\":\"CommonJS\"}' u svaki yarn scripts
// nego samo jednom u tsconfig.json
// radi, probao
https://github.com/TypeStrong/ts-node/issues/922
// tsconfig.json 
"ts-node": {
  // these options are overrides used only by ts-node
  // same as our --compilerOptions flag and our TS_NODE_COMPILER_OPTIONS environment variable
  "compilerOptions": {
    "module": "commonjs"
  }
},
"compilerOptions": {
  "module": "esnext"
}
------------------
declare u typescript je dekleracija fje, tj samo potpis, kao .h fajlovi u c++
-------------
//  How to use TypeScript Enums and why not to, maybe - Andrew Burgess
https://www.youtube.com/watch?v=pWPClHdcvVg
enum je i tip i value istovremeno
bolje object as const pa tip, isto kao ja // poenta
za uniju stringova
typeof MyObj[keyof typeof MyObj] // unutra je keyof, val[key]
----------
// problem sa enum
ne mozes d adodelis string literal koji je unutraz, mora uvek samo MyEnum.Key
const x: MyEnum = 'STR_VAL' // type 'STR_VAL' not assignable to type MyEnum
--------------
//  any vs unknown vs never: TypeScript demystified - Andrew Burgess
https://www.youtube.com/watch?v=kWmUNChlzVw&list=LL&index=3
any - disable typescript, whitelist
unknown - nesto jeste, mora type guards sa if pre assignment
never - blcklist, da pokaze compiletime gresku, kao throw za runtime, switch default primer:

type Color = 'red' | 'green' | 'blue'
function getColorName(c: Color): string {
    switch(c) {
        case 'red':
            return 'is red';
        case 'green':
            return 'is green';
        default:
            const _unreachable: never = c;
            throw new Error('wrong color'); // Argument of type 'string' is not assignable to parameter of type 'never'
    }
}
-------------
// Interfaces vs Type Aliases: what's the difference? - Andrew Burgess
https://www.youtube.com/watch?v=QYO-sieqLD4
i type i interface definisu shape objekata
// interfaces
2 interface sa istim imenom rado kao extend by default, bzvz // interesantno
moze gole fja u interface, ne mora object
extends moze na type, ali ne na uniju bilo kakvih objekata // statically known members
--------
// types
moze alias na postojeci tip, bzvz
type MyType = string;
& - intersection, radi kao extends interface
---
moze & na interface
type MyType = { something: 'abc'} & MyInterface
& mose i na union of types
moze class MyClass implements MyType // fle definisane u type
--------------------------
// satisfies operator, ts 4.9
// Satisfies Operator - new TypeScript 4.9 feature - Andrew Burgess
https://www.youtube.com/watch?v=6uJeT7y6CCo
nije implements
const config = {
  host: 'my.db.com',
  port: 1234,
  tryReconnect: () => true,
  poolSize: 10,
} implements Config;
isto kao postavljanje tipa const config: Config; ali implements je uze, sta je stvarno u objektu
ako je type Config imao unije, port: number | string npr
----
// poenta, umesto ovoga:
const config: Config = {...}
----
moze as const implements Config;
------------------------
// The TypeScript feature I never use - Andrew Burgess
// https://www.youtube.com/watch?v=Whh-TzvzRFs
type casting - runtime
type asserting - as - compile time, typeascript
--------
// declare - dekleracija bez definicije, .h fajlovi
declare function greetMembertmember: Member): string;
declare const members: Array<Member>
----------
// type assertion function, vraca boolean, zapazi
// ovo je boolean za tip, arg is Member
// ulazni arg je broader
function isMember(arg: unknown): arg is Member {
   return !!arg &&
     typeOf arg = 'object' &&
     'id' in arg &&
     typeof arg.id = 'number';
}
// poziv
if(isMember(member)) greetMember(member);
-----------
// moze povratni tip 'asserts arg is Member' onda throws | void 


