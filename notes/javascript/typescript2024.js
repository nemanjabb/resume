// ovo drugo osigurava da nece uci u build koji je javascript
// astro docs, odlicna, svasta ima advanced
import { SomeType } from './script';
import type { SomeType } from './script';
-----------
// prouci env.d.ts files i declare, pitaj chatGpt
// env.d.ts
/// <reference types="astro/client" />
ovo /// je isto kao declare tip
--------------
// restart ts server vs code
ctrl + shift + p -> type typescript -> restart ts server
// na isti nacin i restart eslint server // vazno kad je eslint greska, turborepo npr
... -> restart eslint server
--------------------
// JSDoc notation
// ovi tipovi u komentarima su JSdoc tipovi za js
export default /** @type {import('astro').AstroUserConfig} */ {...}
----------------------
file.d.ts znaci da su samo tipovi unutra, nema koda i funkcija
------------------------
// var1:tip vs as vs satisfies, typescript v4.9, dec 2022.
// https://youtu.be/49gHWuepxxE
type Color =  string | { r: number; g: number; b: number; };
----
const red: Color = "red"; // type Color
const green = "green" as Color; // type Color, cast
const blue = "blue" satisfies Color; // type 'blue', izvuko iz vrednosti
------
as - programer zna bolje, kompajler ne proverava dalje, cast
satisfies - uzima u obzir dat vrednost, izvlaci dodatni tip (properties npr) iz nje, plus da match zahtevani tip
daje uzi, precizniji tip, izvlaci dodatni info iz vrednosti
a proverava se i zahtevani tip (za razliku od as)
---
satisfies poenta: proveri i da je tip zadovoljen i izvuce dodatni info iz vrednosti (inference)
---
moze i {...} as const satisfies Color - onda b je tip 0 umesto number - uze
------------------
// generic type function definition
https://stackoverflow.com/questions/68524136/how-to-define-a-generic-function-type-in-typescript-two-similar-ways
type GetTransformItemFnType = <T/>() => (item: T) => T | null;
const getExtractItemFn = <T/>(collection: string) => (entries: any): T | null =>{
    const resource = entries?.data?.[collection]?.items?.[0];
    return resource ? resource : null;
};
// <T>() => T | null; // <T>() ispred fn je fora 
----------------
// funkcija za tip
type WithLocale<T extends object /> = {
  [K in keyof T]: { 'en-US': T[K] }
}
const product: WithLocale<Product /> = ...
-------------------
// reduce typescript 
// https://stackoverflow.com/questions/14087489/typescript-and-array-reduce-function
poenta: mora tip inicijalnoj vrednosti da se postavi da bi akumulator infer from it 
a.reduce(fn, <string[]>[])
-------------------
hover u tsconfig.json daje objasnjenje sta radi taj option // au, zapazi, vazno
inace pastuj u chatGpt da objasni
----------------------
// infer arguments type from function 
Parameters<typeof myFunction>
// primer, [0] first arg
const fn2 = (arg1: Parameters<typeof someOtherFunction>[0]) => {...}
-------------------------------
-------------------------------
as const mu postavi i vrednost I TIP // zapazi
zato moze za union tip od array of objects
type UnionType = ObjectsArrayType[number]['myKey']; // [number] za array, zapazi
--------
[...] as const je TUPLE, tuple je immutable array // au, zapazi
za convert as const array to normal dovoljno [...constArray] // eto, trivial
-------
// jedno ocigledno resenje
[...TAGS] as string[]).includes(tag)
----------------
// ovako se postavlja tip za [objects...] as const array, a ne interface unapred // zapazi
export type NavigationItem = (typeof NAVIGATION_ITEMS)[number];
---------------------
// fix props types
// fora je mora as const jer 'primary' | 'default' | undefined doesn't match string | undefined // prosto, zapazi
const buttonProps = isActive ? ({ colors: 'primary', variant: 'solid' } as const) : {};
<Button href={href} {...buttonProps} />
-----------------
// Extracts the union type of values from a constant object // eto ga, uvek ovo
export type ValueUnion<T extends Record<PropertyKey, unknown>> = T[keyof T];
-------------
// satisfies vs type declaration
1. myVar: MyType (Type Annotation)
mora exact match ili overlap, MyType is used for myVar
2. myVar satisfies MyType (Type Constraint)
labavije, opet mora isto ili overlap, ali NOVI tip je inferred
type Person = {
  name: string;
  age: number;
};
const person = {
  name: "Alice",
  age: 30,
  job: "Engineer" // extra field
} satisfies Person; 
// `person` is inferred to have type:
// {
//   name: string;
//   age: number;
//   job: string;
// }
----------------
// exclude je za union a | b
type NonNullableExclude<T> = Exclude<T, null | undefined>;
---------------


