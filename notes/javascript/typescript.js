interface Promise<T> {
  catch(onrejected?: (reason: any) => T | PromiseLike<T>): Promise<T>;
  catch(onrejected?: (reason: any) => void): Promise<T>;
}

interfaces, function declarations, and method syntax use `:`
if you are defining the type of a value, it is =>

----
why all javascript in vs code intelisense has typescript signature definitions?

because the typescript definitions provide full coverage over all the APIs available to you
so JS can make use of them,
its one of the main reasons TS is so damn useful, that the .d.ts definitions that describe a JS environment can be both leveraged in a TS and JS context.
----------------
// Understanding TypeScript
// maximilian schwarzmuler
// https://www.udemy.com/course/understanding-typescript/

generic T je samo tip kao argument, fora, pros
interfejsi kao tipovi su objekti bukvalno
na pozivu fje da osigura sta vraca jer moze da vrati vise tipva const [nesto] = <neki_tip>useState(null), kad ne moze da urade infere od inicijalne vrednosti
tuple je polje sa fiksnim brojem elemenata
interfejsi za def tipova objekata, Type za ostalo, fje itd
& je unija interfejsa, tj tipova
useState<> je genericka funkcija
& je intersection tipova, nad objektima je kombinacija, kao extend 2 interfejsa, nad unijama je presek tj zajednicki
nesto! programer zna da nesto nije null
kastovanje, ili <tip> ispred promenljive ili as tip
typeof radi samo nad javascript tipovima, instanceof radi nad typescript klasama jer se prevode na konstruktor fje, ne radi nad interfejsima
sa if proverava tipove
interface ErrorContainer { [prop: string]: string] } index property, moze da ima bezbroj string propertijea gde je kljuc string
?? nulish coalescing isto kao const x = y || default samo za null i undefined, za ostale falsy vrednosti ne radi, '' itd
<T extends object> extends je constraint ovde da T mora da bude objekt
Partial<tip> svi opcioni, Readonly<tip> readonly
-------------------------
-------------------------
// Using TypeScript with React 
// Dmytro Danylov
// https://www.udemy.com/course/react-with-typescript/

//6 setting up node ts project
npx concurrently -k -n COMPILER,NODEMON -c yellow, blue "tsc -w" "nodemon -w dist -q dist/index.js"
k - kill, kad jedan umre
-n -c ime, boja
-w watch
-q quiet, bez logova

//7 modules
modul je fajl koji ima import ili export statement
promenljive u modulu su privatne
moze da ima vise named exports i 1 default export
default export moze da se renameuje kako god u import
moze sve da se importuje u jedn namespace
import * as namespaceName from './someFile';
default ref sa namespaceName.default, named sa namespacename.nesto
alias (rename) import {nesto as drugoIme} from '.fajl';

//8 types
object, array, tuple (polje sa fixnim brojem elemenata proizvoljnog tipa)
enum, skup, implicitno 0,1,2... ili string, moze obrnuto od indeksa
type assertion - kastovanje - kad programmer bolje zna koji je specificniji tip
da bi mogao da pristupis propertijima tog tipa
e.eventTarget as HTMLInputElement ili <HTMLInputElement> ispred osim u jsx

//9  interfaces
ducktypeing
reaadonly prop, optional?
index signature [key: string]: string
interfejs moze da ima dekleracije funkcija
moze extend, onda child mora da ima te prop, moze extend vise interfejsa

//10 functions
type Myfunc = (a: number, b: number) => number;//dekleracija tipa, pa posle dodela arrow func
optional args, default args, default je i optional
spread ...args
overloads, vise dekleracija jedna iznad druge, pa implementacija, mora da hendluje sve dekleracije

//11 classes
public readonly promenlj; samo u konstruktoru moze da se postavi
getters i setters, get i set, process props while set or read
set name(value: string){}, prop _name
static klasa.nesto
default modifier public

//13 describe constructors using interfaces
interface nesto {
 new (volume: number): Klasa; //dekleracija konstruktora koji mora da ima klasa koja implem interfejs
}

//14 generics (tip kao argument)

// functions
function genericfunction<T>(x: T): T {return x;}
const genericArrowFunction = <T>(x: T): T -> x;

// interfaces
interface GenericInterface<T> {
	(a: T): T;
	someProp: T;
}

interface GenericInterface<T> {
	<U>(a: U): U;
	someProp: T;
}

// classes
class GenericClass<P> {
	constructor(public props: P){}
	getProps(): P {
		return this.props;
	}
}
staticki prop i fje ne mogu da koriste generics
ogranicenje <Item extends Expirable>
explicitno koji tip vraca poziv genericke fje
const promenlj = pozivFje<Tip>(); //ts radi infere za default
specif tip za this fje u interfejsu
interface ShoppingCart<ItemId, Item> {
	addItem(this: ShoppingCart<ItemId, Item>, item Item): void;
}

//15 union types
type guards sa if i typeof i instanceof

//16 intersection types
typeA & typeB kombinacija 2 ili vise tipova, ima props od oba
& nije presek nego kombinacija

//17 type alias
type je alias, nije nov tip nego ref
type Alias1 = string | number;
type Alias4 = {a: number; b: number}; ref to a shape of object
interface je novi tip

//18 using external packages and types
ili vec ima tipove, ili instaliras @types/react -D ili pravis react.d.ts fajl sa dekleracijama rucno
declare module 'react' {
	export function someFn(): number;
}

//19 decleration merging
vise definicija za interfejs se merguje u jednu, kombinacija svih, moze i overload
isto i za namespaces, sa istim imenom
kombinuje funkciju i namespace sa istim imenom
extend props of some package you cant modify directly
namespace sa props preko postojeceg tipa, samo exported members
augment module react
dekleracija u declare module 'react'...
definicija u React.Component.prototype.fja = () => {}
ne mozes da dodas nov export, samo postojeci

//24 class components
interfejsi TaskListProps, TaskListState

//25 redux typescript

//26 higher order component

//27 render props

//28 handling events
nista, tipovi argumenata u event.target 

//30 useState
useState vraca tuple
nista specijalno

//31 useEffect
sideeffect - fetch i modify data, subscribing to dom events, modifying dom manually
useEffect -> componentDidmount, componentDidUpdate, componentWillUnmount
cleanup fja se poziva na unmount tj kad promenis stranicu, inace listener radi i na drugoj stranici
kad se menja prom u [dependancy] poziva se i cleanup fja iz prethodnog ciklusa

-----------------------------
-----------------------------
// React & TypeScript for everyone 
// Levelup tutorials

// #8 Typing Children Props
type Props = {
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
};
// Use React.FC to type components that use children props
export const Button: React.FC<Props> = ({ onClick, children }) => {

// #9 Typing useState

useState<string | null>(null)

// #10 useRef and Typing Dom Elements
// null! Read Only
connst ref = useRef<HTMLInputElement>(null!);//strict null check

// #11 Strict Null Checking & Optional Chaining

// #12 useReducer Part 1

type State = {
   rValue: boolean;
;

type Action = {
  type: "one" | "two";
};

function reducer(state: State, action: Action) {}
const [state, dispatch] = useReducer(reducer, initialValues);

// #13 Better useReducer

// #14 useEffect & Custom Hook
const useClickOutside = (
  ref: React.MutableRefObject<HTMLElement>,
  handler: (event: MouseEvent | TouchEvent) => void
) => {
  useEffect(() => {
    const listener = (event: MouseEvent | TouchEvent) => {}
}

// #15 Generics
ctrl+click da vidis koji je tip ili base tip

... context

// #18 Class Components
type Props = {
  title: string;
};

type State = {
  status: string;
};

export default class BigC extends Component<Props, State> {

// #19 Types vs Interfaces

Types
more constrained
better intersecting
can be used in unions

Interfaces
extendable
can be augmented

// #20 Libraries & Types
declare module "styled-components"

---------------------------
// vraca zadnji truthy ili prvi falsy izraz
// cim naidje na falsy izlazi
const giftingUrl = inkindPage && inkindPage.inkindSituations.length > 0 && inkindPage.inkindSituations[0].giftingUrl;

// || vraca zadnji falsy ili prvi truthy izraz
// cim naidje na truthy izlazi
-------------------------
// unknown vs any
you cant call functions nor access properties on unknown type
https://www.typescriptlang.org/docs/handbook/2/functions.html#unknown
-----------------------
// Type narrowing
// https://www.carlrippon.com/6-ways-to-narrow-types-in-typescript/
TypeScript automatically narrows the type of a variable in conditional branches. 
Doing a truthly condition check will remove null and undefined from a type. 
A typeof type guard is a great way to narrow a union of primitive types. 
The instanceof type guard is useful for narrowing class types. 
The in type guard is an excellent way of narrowing object types. 
Function type guards are helpful in more complex scenarios where the variable’s value needs to be checked to establish its type.

