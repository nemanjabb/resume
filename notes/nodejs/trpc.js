// https://trpc.io/docs/quickstart
// to je to, isto kao Apollo, samo rest plus tipovi, rest in graphql way
// dobro je sto je resio sve, router, validaciju, react-query, tipove
// integracija out of the box
prelaz izmedju rest i graphql, sa tipovima
-----
// server
ima query i mutations, kao graphql resolvers
procedura - endpoint, query, mutation ili subscription
input() - parsuje i validira args i tipuje ih
query | mutation: ...
ima router
na kraju serve router, kao Apollo server
server bilo kojeg (Node.js) backend frameworka, ili Node.js http server
--------------
// client
client importuje tip sa servera AppRouter, na taj nacin zna koji query i mutations su dostupni
links - modifikuju request sa klijenta pre nego je poslat, za batch
// query na clientu, user ima tip
const user = await trpc.userById.query('1');
// mutacija
const createdUser = await trpc.userCreate.mutate({ name: 'sachinraja' });
--------
// next.js
https://trpc.io/docs/nextjs/introduction





