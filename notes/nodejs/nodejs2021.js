import next, { getServerSideProps } from "../react/next"

--------------
yarn workspaces @/staze korisno
ne moras da brines o relativnim stazama
mozes da kopiras komponente
apsolutne staze prakticno
---------------
"list-node-modules": "find . -type d -name node_modules -prune",
"delete-node-modules": "find . -type d -name node_modules -prune -exec rm -rf {} \\;"
----------------
// nvm
// https://gist.github.com/chranderson/b0a02781c232f170db634b40c97ff455

// check node version
node -v

// list installed versions of node (via nvm)
nvm ls

// Lists installed Node versions
nvm list node

// Lists installed Node versions with additional release info                 
nvm list  (or)  nvm ls          

// install specific version of node
nvm install v6.9.2

// install latest LTS node
nvm install --lts

// set default version of node
nvm alias default v6.9.2

// switch version of node
nvm use v6.9.1

// uninstall a specific Node version
nvm uninstall v6.9.2

// list available node releases
nvm ls-remote

// list available node releases on windows
nvm list available

------
// delete node_modules windows
npx rimraf node_modules
npx rimraf bower_components

// delete node_modules linux
rm -rf node_modules


// install yarn
npm install --global yarn
--------------------
"peerDependencies": { // yarn workspace, dependencies that child workspace gets from the root workspace
    it means the package won't run without typescript but it also doesn't need a specific version
------------------
// paketi u yarn workspaces
"workspaces": key in package.json and yarn install
------------------
// delis link, dobar
npx localtunnel --port 3002
------------------
// call one yarn script from another yarn script
// https://stackoverflow.com/questions/52151353/yarn-run-multiple-scripts-in-parallel
{
    "parallel": "yarn script1 & yarn script2",
    "serial": "yarn script1 && yarn script2",
    "script1": "... some script here",
    "script2": "... some there script here"
}
---------------------
// eslint, prettier
// svi config fajlovi imaju tacku ispred, zapazi, zato pravila i scripte ne rade
.eslintrc.json, .eslintignore, .prettierrc, .prettierignore

// package.json
"type-check": "tsc --pretty --noEmit",
"lint": "eslint . --ext ts --ext tsx --ext js",
"format": "prettier --write .",
----------------------
// yarn why
// yarn why @next-auth/prisma-adapter // da vidis koja verz je zapravo instalirana
"@next-auth/prisma-adapter": "0.5.2-next.19",
// makni ^ jer ce instalirati 0.5.4 koja je veci broj ali nije next, zapazi
"^0.5.2-next.19"
------------------
yarn remove package // da maknes i iz yarn.lock i iz node_modules
samo iz packages.json i yarn install ih ostavlja TransformStream, mora ih brises
---------------------------
// REST routes

// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
// https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api
// https://restfulapi.net/resource-naming/

// Getting Started with Next.js | REST API Routes
// https://www.youtube.com/watch?v=urPyuOIjoPI

// https://medium.com/@mwaysolutions/10-best-practices-for-better-restful-api-cbe81b06f291
za resurse ide glavni path params /users/:id/posts/:id/comments/:id
query params ?arg=1 ide za filtering, sorting, field selection and paging for collections

GET /cars?color=red Returns a list of red cars // filter
GET /cars?sort=-manufactorer,+model // sort
GET /cars?fields=manufacturer,model,id,color // selection
GET /cars?offset=10&limit=5 // pagination

HATEOAS - Hypermedia as the Engine of Application State // ???... googlaj u youtube
--------------------
// Error Handling in Express
// https://www.youtube.com/watch?v=DyqVqaf1KnA
// https://github.com/productioncoder/express-error-handling/blob/master/error/api-error-handler.js
ako ima (err, req, res, next) 4 argumenta onda je express error handling middleware // to to
next() poziva sledeci middleware i tako do zadnjeg
next(error) ako ima arg, arg je error // to to
mora return_ iza next(err) da bi error middleware bio zadnji
kida lanac greskom i error middleware zadnji salje res.status(400).json(message)
error handler na kraju ima catchAll res.json('something went wrong')
---
app.use(routes) ...
app.use(errorMiddleware) // ide kao zadnji app.use() u server.js
----------------------
cim middleware nije zadnji mora da zove next() ili res.send('123')
---------
pm2 i systemd (default linux) su process manageri, restart itd
------------------------
// A Guide to Error Handling in Express.js
// https://scoutapm.com/blog/express-error-handling
bez try catch rusi stranicu, stack trace u konzoli
sa try catch stack trace je na stranici ili network tab 400, 500, kozola radi
---
next() zove sledeci middleware u app.use(...) ili route.use() , nema neki treci imaginarni // to je to

app.use(...)
app.use(...)
app.use(...)
...
--------------------------------
// jonas schmedtmman node js
// https://github.com/jonasschmedtmann/complete-node-bootcamp/tree/master/4-natours/after-section-14
step out u debugger izvrsi sve u tekucoj fji i izlazi u spoljnu fju, dobar
-----
// 111
catch all route
-----
// 113
kad se zove next(arg) sa arg, trigeruju se SAMO error middleware sa 4 args (err, req, res, next)
normalni se skipuju
napravio error handler sa 4 args i to je to, trivijal
-----
// 114
pravi AppError extends Error klasu samo constructor, za throwing, trivijal
-----
// 115
middleware su fje koje vracaju fje, kao middleware u redux thunk
const catchAsync = fn => (req, res, next) => { ... }
------
// 116
bacio return_ next(new AppError('message', 404)), nista
---------
// 117
sendDev i sendProd() errors iz errorControllera
operational i programming errors, (programing ozbiljne, neocekivane)
--------
// 118
convert programing err to operational za mongoose, if name === CastError, bzvz skroz
----
// 119
opet za drugu mongo err, regex gluposti
----
// 120
treba da hendlujes i ispises response za sve greske koje rute mogu da naprave
if za validation errors
sve samo za prod
-----
// 121
node exceptions, do sad bili express
unhandled promise rejection - neki await nema try catch ili promise ima samo .then() bez catch() // eto
samo fali db.connect().catch() i onda je handled

// handluje async greske
// server.js, handluje unhandled promise rejection u celoj app na 1 mestu
process.on('unhandledRejection', err => {
    console.log('UNHANDLED REJECTION! Shutting down...');
    console.log(err.name, err.message);
    server.close(() => { // prvo ugasi server, pa onda process
      process.exit(1); // gasi aplikaciju
    });
  });
treba pm2 da restartuje app
ne izlazi iz konzole
------
// 122
sync errors - uncought exceptions
async equivalent je unhandled promise rejection, neuhvacen exception u async fji

// mora na vrhu koda da bi hvatao ispod sebe
process.on('uncaughtException', err => {
    // isto gasi aplikaciju, jos vaznije, node process in unclean state
    process.exit(1); // nema veze sa server
})
mozda i ne treba ova dva event handlera...
--------------
// package-lock.json
npm ci // instalira iz lock fajla
yarn install --frozen-lockfile // isto to za yarn
-----
// generate lock file without install node_modules
npm i --package-lock-only
yarn install --mode update-lockfile
-----------
// edge functions, edge computing
geografski rasporedjeni backendi kao cdn
middleware u next.js
-------------------
(req, res, next) - next je middleware, tacno sledeci middleware
---------------------
// update package to the latest version with yarn
// https://stackoverflow.com/questions/62650640/yarn-how-do-i-update-each-dependency-in-package-json-to-the-latest-version
// all packages
// samo stampa plan, yarn.lock obavezan
// instalira pakete, probao, 
// strelice gore dole, a - sve, space - select 1, i - toggle, enter  instaliraj sve selektovano
// strelice, space, enter
yarn upgrade-interactive --latest
yarn // (install) updateuje node_modules i package.json
// all sa bibliotekom
npx yarn-upgrade-all
---
// single package to the latest major version
yarn upgrade <package-name> --latest
---
// single package to specific version
yarn upgrade package@version
yarn upgrade @types/express-session@^1.15.16 // npr
---
// stampa koji su outdated, po bojama, crvena major...
yarn outdated

info Color legend : 
 "<red>"    : Major Update backward-incompatible updates 
 "<yellow>" : Minor Update backward-compatible features 
 "<green>"  : Patch Update backward-compatible bug fixes
---------------------
// node_modules after build
na frontendu ne, jer webpack transformise u html + js
na backendu da, jer ide require, import...
next.js treba node_modules ako ima getServerSideProps, kao...?
-------------------
// circular dependency
Cannot access '__WEBPACK_DEFAULT_EXPORT__' before initialization
lose importovana 3rd party biblioteka, iz dist umesto root
-----------------
// run shell commands in js
// https://github.com/shelljs/shelljs
// ili require('child_process') node native
// https://www.freecodecamp.org/news/node-js-child-processes-everything-you-need-to-know-e69498fe970a/
-------------------
// list folder size opadajuce u node_modules da vidis sta najvise zauzima
// du -sh ./node_modules/* | sort -nr | grep '\dM.*'
-------
require('...') gadja ime foldera u node_modules
-----------
Promise.all([]) // izvrsava in parallel instead of jedan za drugim, koliko traje najduzi, multithread bukvalno
await p1()
await p2() ... // t1 + t2 ...blokiraju se odozgo na dole
-------------------
// await u petlji, dobar, isto bilo za seed za baze, komplikovano prilicno
// https://stackoverflow.com/questions/37576685/using-async-await-with-a-foreach-loop
sequential - for petlja, ne moze forEach
paralelno - map i Promise.all([])
--------------------
// import default vs * as Nesto
// sve kao React, default + all named
import * as React from 'react'
// samo default
import React from 'react'
---------------
// clean cache
// https://stackoverflow.com/questions/39991508/how-to-clear-cache-in-yarn
yarn cache clean // cleans that directory
yarn cache list // shows the list of cached dependencies
yarn cache dir // prints out the path of your cached directory 
// tj ~/.yarn-cache/
// /home/username/.cache/yarn/v6 - zapravo ovo
----
// npm
npm cache clean
--------------------------
// controllers, services - separation of concerns, tests
controller i middlware vrse sve provere permisija i http
services rade samo sa bazom, bez ikakvih provera, sve je javno, i daje funkcije controlleru
----
// notes from project
1. middleware - checks permissions as soon as possible, as soon it has required informations
2. controller - transforms http request into service args, and handles response and statuses, throws http errors
3. service - handles database and provides functions for controller, agnostic of permissions and http and session user, throws only database exceptions, no double checks

- no service depends on logged in user
- services dont return null, either data or exception, and trim password (db operation)
- services check 404 on input and 400 on output, and all db checks (double email, username 409, 403)
- permissions/access rights in middleware, requireAdmin, requireLogin, 401
- too custom permissions in controller, isAdmin || isOwner 401, controllers should be thin
- services should be standalone
- service layer reuses errors from controller, to small app for custom service errors and error translations to controller layer
- 404 checked in service because of `excludeFromUser(user)` so it doesnt throw on null, should be fixed in Prisma
- getServerSideProps reuses services and has its own api error handler wrapper
- clear controller/service separation - clear unit tests
----------------------
// db seed
// clear db - keep schema, delete data
za deleteMany moraju pozivi po redosledu zavisno od relacija medju modelima
inace raw sql truncate
-----------------
// install in CI, from lock file, in Dockerfile
// npm
ENV CI=1
npm ci
// yarn
yarn install --frozen-lockfile
-------------------
// da izvrsis lokalni npm paket u konzoli ili docker-compose.yml command
// https://stackoverflow.com/questions/9679932/how-to-use-executables-from-a-package-installed-locally-in-node-modules
npx cypress
command: npx wait-on http://npb-app-test:3001 && npx cypress run --project ./tests-e2e
---
// ako samo cypress, cannot find module...
---
// drugi nacin node path bin
-------------------------
// NODE_ENV vs APP_ENV
// https://rafaelalmeidatk.com/blog/why-you-should-not-use-a-custom-value-with-node-env
// https://seanconnolly.dev/dont-be-fooled-by-node-env
// https://koistya.medium.com/demystifying-node-env-var-b25ed43c9af
**point:** there is already existing convention for `NODE_ENV=dev | prod | test` 
that all npm libraries respect, it cant have custom value (will affect libs)
----
custom value is same as undefined and same as development
----
use `APP_ENV` for custom values that describe environment, e.g. local-prod, local-test, ci-test...
-----
CI-CD build job needs to set APP_VERSION var and tag
-----------------
// best APP_ENV methods
// https://getridbug.com/reactjs/how-to-use-different-env-files-with-nextjs/
// https://blitzjs.com/docs/environment-variables
------------------
you cant combine yarn and npm commands, it might work ali samo slucajno, ne radi se tako
failed in github actions
----------------------------
// dodaje lokalni node_modules u path, da bi lokalni paketi radili u shell bez npm run, kao globalni
// https://gitlab.com/testdriven/flask-react-auth/-/blob/master/services/client/Dockerfile
// samo bez "npm run" i to je to, proveri
// Dockerfile
// set working directory
WORKDIR /usr/src/app
// add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH
--------------------------------
// moze https i http odjednom, ne mora if
createServer(httpServer, httpsServer).listen(port, ...)
---------------------------
// Creating a Node.js Command-line Tool, Linux Terminal CLI and NPM Package ,
// https://dev.to/basskibo/creating-a-nodejs-command-line-tool-linux-terminal-cli-and-npm-package-50na
1. node.js command line tool
// poziv
node my-program.js arg1 arg2
// args pri pozivu
const filePath = process.argv[2];
const string = process.argv[3];
// input u toku rada
const readline = require("readline");
// commander paket za slozene opcije
--------------
2. linux cli tool
#!/usr/bin/env node
chmod +x script.js
// stavi u folder koji je u path
mv script.js /usr/local/bin/your_command
-------------
3. npm package
// create package.json
npm init
// locall tarball
npm pack
// login
npm login
// publish
npm publish
---------------------------
// create npm module
// 5 Simple Steps to Creating Your Very Own npm Module
// https://www.bojanjagetic.com/post/create-npm-module
1. init project
2. write code
3. tests
4. publish
5. update versions
---------------------------
// multer.memoryStorage() za citanje file properties bez cuvanja na disk
app.post('/upload', multer({storage: multer.memoryStorage()})
   .single("file"), async (req, res, next) => {...})
----------------
// chatGPT na dole
// memoryStorage() example
const upload = multer({ storage: multer.memoryStorage() });
app.post('/upload', upload.single('avatar'), (req, res) => {
  const file = req.file;
  // ove props mogu da iscitam
  console.log(file.fieldname);
  console.log(file.originalname);
  console.log(file.encoding);
  console.log(file.mimetype);
  console.log(file.buffer); // Buffer class in Node.js, which represents a chunk of binary data
  console.log(file.size);
  res.send('Avatar uploaded!');
});
-----
// stream from buffer
const { Readable } = require('stream');
const bufferToStream = buffer => {
  const stream = new Readable();
  stream.push(buffer);
  stream.push(null);
  return stream;
};
------
// ovo je dovoljno za Contentful
body: file.buffer,
------------------
poenta: uvek je ili req.file ili req.files // to, zapazi
------------
// single()
// 'avatar' se matchuje sa <input type="file" name="avatar" /> u formi na clientu
app.post('/upload', upload.single('avatar'), (req, res) => {
  const file = req.file; // a ovde je req.file
  res.send('File uploaded!');
});
--------------
// multer().array() vs multer().fields()
multer().array() is used to handle an array of files in a single field, 5 slika u jedno input polje npr
const upload = multer().array('photos', 5);
const files = req.files;
------
multer().fields() is used to handle multiple file fields in a single request
const upload = multer().fields([
  {
    name: 'avatar',
    maxCount: 1,
  },
])
const avatar = req.files['avatar'][0];
-----------------------------
// zato multer ne parsuje req.body i req.file
RESENJE: content type ne sme biti application/json vec multipart/form-data // VAZNO
fetch() ovo postavlja default sa new FormData().append(...)
-----------
// koristi ovu fju uvek
const convertObjectToFormData = (jsObject) => {
  const formData = new FormData()
  Object.keys(jsObject).forEach((key) => formData.append(key, jsObject[key]))
  return formData
}
---------------
export const fetchCreateJobApplication = async (
  jobApplicationData: JobApplicationFormData
) =>
  fetch('/api/contentful/job-applications', {
    method: 'POST',
    // headers: {
    //   'Content-Type': 'application/json', // OVO, pogresno
    // },
    body: convertObjectToFormData(jobApplicationData),
  }).then((response) => response.json())
------------------
// client
poenta sa multer.single('fieldName') je da sa clienta iz forme posaljes File a ne FileList // to
<input name="fieldName" value={value} onChange={handleChange} />, react-hook-form
// server
export const resumeFileUpload = multer({ storage, fileFilter, limits }).single(
  'resumeFile'
)
-----------------------
// set node i npm version, package.json
"engines": {
  "node": "16.19.1",
  "npm": "8.19.3"
},
------------------------
// npx degit
// npx - node.js
// degit - paket
npx degit - npm utility paket da downloadujes samo jedan folder iz github repoa
-----------------
sa environment variables uvek je fora da li su build time ili runtime // zapamti to
-----------------
dotenv default cita .env file samo pored package.json i node_modules, process.cwd()
----------------
// za substitution mora install dotenv-expand package
VAR_1=prefix-${VAR_2}
require('dotenv').config();
const dotenvExpand = require('dotenv-expand');
const myEnv = dotenvExpand(process.env).parsed;
-----
docker-compose.yml expanduje env var isto
---------------------
// Dockerfile
ENV PATH /usr/src/node_modules/.bin:$PATH
is used to add the node_modules/.bin directory to the systems (OS) PATH environment variable
mozes da pokreces pakete is LOKALNOG node_modules u terminal
---------------------------------
// nvm install latest lts, mora celo ime
nvm install lts/hydrogen
nvm use lts/hydrogen
// mora set default inace node -v undefined
nvm alias default v18.16.0
------------------
// limit versions package.json
"engines": {
  "node": "18.x",
  "yarn": ">= 1.22.19"
}
-------------------------
// webhook
web url event handler, route + payload
http event system
----------------------------
// pino logger za next.js
pino pipeline ne mogu da se grupisu
// ovo radi
1. nacin pipeline stream, komplikovan, samo za transform
https://github.com/pinojs/pino/blob/master/docs/transports.md#creating-a-transport-pipeline
kreiras pipeline stream, pa ga stavis u oba destinations i onda destinations loadujes u pino.multistream([])
{ level: 'trace', target: transformStreamTargetAbsolutePath, options: {} },
1. destination pipeline sa pino-pretty i transform
2. destination pipeline sa slack i transform
const logger = pino(pinoOptions, pino.multistream([destination, destination2]));
----------------
// 2. nacin - custom serializer, nece nesto
function customSerializer(log) {
  return {
    ...log,
    additionalField: 'value'
  };
}
// Create the Pino logger with the custom serializer
const logger = pino({
  serializers: {
    ...pino.stdSerializers,
    custom: customSerializer
  }
});
-------
// probaj load kao string od npm paketa, kao za 'pino-pretty'
const { createTransport } = require('pino-slack-transport');
------------
neka druga dependencies breaks 'pino-pretty', na bare next.js app radi
---------------
// 3. nacin - child logger, najjednostavnije, ne moze redosled da se podesava
const parentLogger = pino();
// Create a child logger with additional fields
const childLogger = parentLogger.child({ additionalField: 'value' });
------------------
// get project root dir for absolute paths for .mjs files
// resolve parent dir (in Vercel, serverless env)
process.cwd() fails
const path = require('path');
const parentDir = path.resolve(path.dirname(__dirname));
console.log('Parent directory:', parentDir);
-----------------------
Date na serveru jeste Date object, na client stize kao isoString string, ide new Date()
------------------
// execute shell commands (yarn scripts, npx commands) from ts-node or node.js
https://stackoverflow.com/questions/20643470/execute-a-command-line-binary-with-node-js
// with callback
const { exec } = require('child_process');
exec('cat *.js bad_file | wc -l', (err, stdout, stderr)
--------
// with Promises:
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const { stdout, stderr } = await exec('ls');
-------
// spawn
if you wish to receive the data gradually in chunks (output as a stream), use child_process.spawn:
const { spawn } = require('child_process');
--------
// sync, ovaj najbolji zapravo za skripte, da blokira komande iza &&, i nema top level await
const { execSync } = require('child_process');
// stderr is sent to stderr of parent process
// you can set options.stdio if you want it to go elsewhere
let stdout = execSync('ls');
---------------------
// run scripts with ts-node, additional
// npx command
its a tool that now comes with npm that allows you to run binaries that are local to the project from the command line
binaries from node_modules prakticno
https://www.digitalocean.com/community/tutorials/typescript-running-typescript-ts-node
// -T or --transpileOnly, za tudji kod
to transpile down to JavaScript without checking for any TypeScript errors
------------------------
// env var u node.js moze odmah da se dodeli, odmah se reflektuje
process.env.MY_VARIABLE = 'initial value';
console.log(process.env.MY_VARIABLE); // Output: initial value
-----
process.env.MY_VARIABLE = 'updated value';
console.log(process.env.MY_VARIABLE); // Output: updated value
-------------------------
// print formated env vars
https://stackoverflow.com/questions/22658488/object-getownpropertynames-vs-object-keys
// ovaj hvata i enumerable: false props - nema pojma, isto je za env vars
Object.getOwnPropertyNames(a) 
// malo drugacije, samo key, pa envVars[key]
Object.getOwnPropertyNames(envVars).forEach(key => {
  console.log(`${key}: ${envVars[key]}`);
});
---------
// obican console.log(process.env) ispise sve, ali objekat
-----------
const envVars = process.env;
// tuple, ovaj lepo formatira, bolje nego u moj projekat
Object.entries(envVars).forEach(([key, value]) => {
  console.log(`${key}: ${value}`);
});
-------
// cela fja
const envVars = process.env;
let varsString = '';

const includeVars = ['NEXTAUTH_URL', 'DATABASE_URL', 'VERCEL_ENV', 'VERCEL_URL', 'VERCEL_GIT_COMMIT_REF'];

// ovaj je bolji
Object.entries(envVars)
  .reverse()
  .forEach(([key, value]) => {
    if (includeVars.includes(key) || true) {
      varsString = `${varsString}\n${key}: ${value}`;
    }
  });

console.log('varsString', varsString);
-------------------------
zod validation for env varsString, undefined // preuzmi iz onog projekta
-------------------------
// abort node.js script
function myFunction() {
  // Perform some logic

  if (/* error condition */) {
    console.error('Error occurred.');
    process.exit(1);
  }

  // Continue with the function logic

  return /* value */;
}
------------------------
// run only in terminal, skip on import
// node.js equivalent from python
if __name__ == "__main__":
  cli()
------
// node.js
if (require.main === module) {
  cli();
}
--------------------------
// evo ti validacija za process.env // odlicno
https://github.com/af/envalid
