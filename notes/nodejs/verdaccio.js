// Verdaccio Cheatsheet for Local Registry
// Install Verdaccio globally
npm install -g verdaccio
// Start Verdaccio server
verdaccio
// Access Verdaccio UI and API
// Open your browser at http://localhost:4873
// Configure npm to use Verdaccio as the registry
npm set registry http://localhost:4873
// Log in to Verdaccio (use any username and password; Verdaccio will create a local user)
npm adduser --registry http://localhost:4873
// Publish a package to Verdaccio
npm publish --registry http://localhost:4873
// Install a package from Verdaccio
npm install <package-name> --registry http://localhost:4873
// Switching back to the default npm registry
npm set registry https://registry.npmjs.org
// Optional: Reset npm registry to default
npm config delete registry
// Useful Verdaccio commands
// Restart Verdaccio
verdaccio --listen 0.0.0.0:4873
// View Verdaccio logs (if installed globally)
// tail -f ~/.config/verdaccio/logs/*.log
// Unpublish a package from Verdaccio (useful for testing)
npm unpublish <package-name> --registry http://localhost:4873 --force
// Add a scoped registry (for a specific package scope)
npm config set @<scope />:registry http://localhost:4873
// Example: Publish a scoped package to Verdaccio
npm publish --registry http://localhost:4873 --access public
--------
// Dockerized Verdaccio (Alternative)
// Run Verdaccio container
docker run -it --rm --name verdaccio -p 4873:4873 verdaccio/verdaccio
// Configure npm to use Dockerized Verdaccio
npm set registry http://localhost:4873
-----------------
// verdaccio docker
// Creating a Local Private NPM registry using Verdaccio - Manuel Gutierrez
// https://www.youtube.com/watch?v=dYE9vc8KtE4
poente:
1. set user in docker-compose.yml // required for publish
config.yaml in volume ./config:/verdaccio/conf
// publish, mora url
npm publish --registry http://localhost:4873
2. in host project root .npmrc, ne mora home // required for installing
npm get registry // check which is used
// .npmrc 
registry=http://localhost:4873