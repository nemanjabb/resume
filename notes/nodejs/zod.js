// zod, parse string to number, validate 'asc' | 'desc' with union type, not string
// parse '1' to 1
export const businessGetCallsSchema = z.object({
    limit: z.preprocess(
        // ovo puca kad ne prosledis limit, pogresno, OVO z.string().parse(x) je pravilo veliki problem za debug
        (x) => parseInt(z.string().parse(x), 10), 
        z.number().min(1).max(100).optional()
    ),
    limit: z.coerce.number().min(1).max(100).optional(), // ovo je ok
    fromDate: z.coerce.date().optional(), // ok
    toDate: z.coerce.date().optional(),
    sortDirection: z.enum(['asc', 'desc']).optional(), // ok za uniju
});
  -------------------
// next-validations library has a flaw that it doesn't parse and doesnt cast req.query as it should
// i.e. {limit: '1'} into {limit: 1}, so Prisma fails
// so we need to call Zod schema again in the controller
// research for better library or fix within next-validations if possible
const result = businessGetCallsSchema.safeParse(req.query);
console.log('req.query', req.query, 'result', result);
if (!result.success) throw ApiError.fromZodError(result.error);
----
also doesnt log next-connect error in terminal
--------------
disable cache checkbox u browser da ne kesire query param u url, npr ?sortDirection=asc // fora, nije zod
---------------------
// zod schemas, extend, omit, merge
// merge je da pregazis polje, npr partial, .pick({ id: true })) koje polje
export const updateSchema = insertSchema
  .partial()
  .merge(selectSchema.pick({ id: true }))
// extend - dodaj polje, omit - skloni polje
export const insertSchema = createSchema(table)
  .extend({ access: z.record(moduleSchema).optional().nullable() })
  .omit({ id: true, createdAt: true, updatedAt: true })
------------------
// zod transform BEFORE parsing and validating -> preprocess
const inactiveToNull = (value: unknown) => value === 'inactive' ? null : value
const status = z.preprocess(inactiveToNull, z.enum([Status.active]).optional())
export const insertMessageSchema = createInsertSchema(messageTable)
  .omit({ id: true, createdAt: true, updatedAt: true, status: true })
  .extend({ status })
-------------
// bolje, na z.enum()... je
z.transform() - transform data after parsing
// odmah definise ulazne pa ih samo transformise za izlaz
const status = z.enum([Status.active, Status.inactive]).nullish()
.transform(status => status === Status.inactive ? null : status)
// daje bolji error message zato
"message": "Invalid enum value. Expected 'active' | 'inactive', received ''"
-------------
z.optional() je samo za undefined, kao typescript
z.or(z.literal('')) je za prazan string
z.nullable() je za null // eto, sve ima
z.nullish() je null | undefined, nullable() + optional()
---------------
// valibot - zod sa 100x manji bundle size
// pick fields from schema and make them all optional
myKey: {
  ...v.partial(v.pick(InitialSchema, ['name', 'title', 'color'])),
},
----------------
// extend vs merge
oba su extend ali merge() prima schemu, a extend() prima object litteral {}
meni najcesce treba merge, extendujes jednu semu drugom // to
extend za ad hoc dodatna polja
// merge
const schema3 = schema1.merge(schema2); // 3 = 1 + 2
// extend, prima {}
const schema2 = schema1.extend({ newField: z.string(),});
