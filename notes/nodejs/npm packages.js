// Publishing an npm Package

// 1. Initialize the Package
npm init -y

// 2. Test Locally
npm link
npm link <package-name>

// 3. Prepare for Publishing
// - Update package.json (name, version, description, main, etc.)
// - Create .npmignore (if needed) to exclude files from the published package.

// 4. Login to npm
npm login

// 5. Versioning
npm version patch   // For bug fixes
npm version minor   // For new features
npm version major   // For breaking changes

// 6. Publish the Package
npm publish --access public  // For public packages
npm publish                  // For private packages

// -------------------------
// Setting Up Changelog with Conventional Commits

// 1. Install Dependencies
npm install --save-dev @commitlint/cli @commitlint/config-conventional standard-version

// 2. Configure Commitlint
echo "module.exports = { extends: ['@commitlint/config-conventional'] };" > commitlint.config.js

// 3. Add Husky Hook for Commitlint
npx husky install
npx husky add .husky/commit-msg 'npx --no-install commitlint --edit "$1"'

// 4. Add Release Script to package.json
// Inside "scripts":
// "release": "standard-version"

// 5. Generate and Update Changelog
npm run release

// 6. Push Tags and Changelog
git push --follow-tags origin main
-------------------------
// Changeset CLI Cheatsheet 
-----
// Initialize Changeset in your repository
pnpm exec changeset init
// Create a new changeset (interactive prompt)
pnpm exec changeset
// Show the status of all pending changesets // taj
pnpm exec changeset status
// Apply version bumps based on pending changesets
pnpm exec changeset version
// Remove all pending changesets (use with caution)
pnpm exec changeset pre exit
// Enter pre-release mode (useful for beta releases)
pnpm exec changeset pre enter <tag>
// List all changesets in the repo // invalid?
pnpm exec changeset list
// Help command to see all available options
pnpm exec changeset --help
------------------
// changesets ok clanak, izraelac
// https://lirantal.com/blog/introducing-changesets-simplify-project-versioning-with-semantic-releases
------------------
------------------
// npm tags
// tag je alias za verziju
npm install my-package // default latest
npm install my-package@beta // beta tag
// You want developers to install the beta version (2.0.0-beta.1) without affecting the default latest tag. 
// You can add a beta tag for the beta version:
poenta: da 1.0.0 i dalje bude latest, a ne 2.0.0 // to
------------------
// npm link alternativa 
// --install-links kopira umesto symlink
npm install ../../other-package --install-links
------------------
// npm ECMAScript Modules (ESM) and CommonJS (CJS)
// https://dev.to/snyk/building-an-npm-package-compatible-with-esm-and-cjs-in-2024-88m
upstream ESM consumers - target project
-------
poente:
// package.json
1. "type": "commonjs" - je default, ako omit 
2. "type": "module" - je ESM, 
// mora i "main" field inace ne radi ni u esm target projektima
"module": "src/index.mjs", // na koji fajl se svodi paket
"type": "module", // esm, izbegavati "type": "module"
3. "main": "index.js" - je entry point 
ako nema "main" onda trazi server.js u root
4. compatible with both ESM and CJS
main points to a CJS export and the module points to an ESM export // to glavno manje vise
"main": "src/index.cjs",
"module": "src/index.mjs",
5. "exports" - granularna kontrola, opet radi za oba  CJS and ESM, ne treba "main", "type" i "module" // pitanje...
"exports": {
  ".": {
    "require": "./src/index.cjs", // za require(‘math-add’)
    "import": "./src/index.mjs" // za import mathAdd from ‘math-add’
  }
}
6. TypeScript - samo za CJS treba kompilacija // bezveze, nije tacno
// konacno
{
    "main": "./dist/index.js",
    "module": "./dist/index.mjs",
    "types": "./dist/index.d.ts", // ima jos i tipove
    "exports": {
        ".": {
            "require": "./dist/index.js",
            "import": "./dist/index.mjs",
            "types": "./dist/index.d.ts" // ima jos i tipove
        }
    },
}
------
"files": ["dist", "src"], // ima i ovo polje, nije objasnio
----------------
// Verdaccio moze da rutira svaki paket sa drugog registry-ja
// https://cheatsheetseries.owasp.org/cheatsheets/NPM_Security_Cheat_Sheet.html#6-use-a-local-npm-proxy
----------------------
----------------------
// Publishing a Private NPM Package on GitHub Registry
// 1. Set the package name with a scope in package.json
// scope je @username ili organizacija na Githubu
// Example: @username/package-name
{
  "name": "@username/package-name", // mora od test usera
  "publishConfig": {
    "registry": "https://npm.pkg.github.com"
  },
  "repository": {
    "type": "git",
    "url": "git+https://github.com/username/repo-name.git"
  }
}
// 2. Generate a personal access token (PAT):
GitHub Settings > Developer settings > Personal Access Tokens.
Generate new token (classic), then select write:packages and read:packages scopes, also repo if your repository is private
// 3. Log in to the GitHub Package Registry
npm login --registry=https://npm.pkg.github.com --scope=@username
// ovo upisuje u ~/.npmrc
`
@<username>:registry=https://npm.pkg.github.com
//npm.pkg.github.com/:_authToken=<YOUR_PERSONAL_ACCESS_TOKEN>
`
// Use your GitHub username and a Personal Access Token (PAT) as the password.
// 4. Publish the package
npm publish
// 5. Confirm the package is published on the GitHub registry at:
https://github.com/<username>?tab=packages
----------------------
//  Installing a Private Package from GitHub Registry
// Add this to your .npmrc file (global or project-specific)
@username:registry=https://npm.pkg.github.com
//npm.pkg.github.com/:_authToken=<YOUR_PERSONAL_ACCESS_TOKEN>
// Then install the package
npm install @username/package-name
// mozes i sa git repository direktno da instaliras (commit, tag, branch), ne mora registry
"dependencies": {
  "my-private-package": "git+https://<username>:<personal-access-token>@github.com/<username>/<repo-name>.git"
}
----------------------
// .npmrc Location Options
// 1. System-wide .npmrc
// Location: /usr/local/etc/npmrc (Linux/macOS) 
@username:registry=https://npm.pkg.github.com
// 2. Global .npmrc
// Location: ~/.npmrc (Linux/macOS)
`
@username:registry=https://npm.pkg.github.com
//npm.pkg.github.com/:_authToken=<TOKEN>
`
// 3. Project-specific .npmrc
// Location: ./project-folder/.npmrc
@username:registry=https://npm.pkg.github.com
// 4. Custom location via environment variable
// Example: Using a custom .npmrc file
export NPM_CONFIG_USERCONFIG=/path/to/custom/.npmrc
----------------
// ovo cesto popravlja build iz child dependencies
"skipLibCheck": true,
---------------
// objasnjen package.json entry points
https://medium.com/@kaljessy/publish-your-react-library-to-npm-using-vite-136dc81e368d
----------
// vite react npm
https://dev.to/receter/how-to-create-a-react-component-library-using-vites-library-mode-4lma
vite - dev server + rollup bundler
----------------
yarn why, pnpm why
npm ls react // gde ga sve ima, lokalno ne sme dva inace useState null error
-----------------
// ukljuci lokalan projekt kao dependency, radi identicno kao link
// npm install  --install-links // kopira umesto symlink
{
    "dependencies": {
        "my-local-package": "file:../relative-path-to-local-project"
    }
}
u principu mora verdaccio docker container za lokalno testiranje // to
ili remote Github registry private publish, radi user token za organizaciju, read/write pakages and repo
@my-org/my-package@0.0.0-test-1 // test ide u verziju, a ne u ime
private: true // ne zaboravi u package.json
npm publish // ne treba ni build ni node_modules, kao git push, trivial
-----------------
// github release and npm publish with github actions
// Publish to NPM with GitHub Actions - Jamie Barton
// https://www.youtube.com/watch?v=H3iO8sbvUQg
// bumping versions in package.json and create changelog.md from commit messages
changesets - for monorepos
semantic-release - for simple repo // on ovde radi sa ovim
run: npx semantic-release - radi sve sto treba 
----
next tag: (next ili staging branch)
my-package@1.0.1-next.0 // semantic-release ovako postavlja verziju
npm install moze sa verzijom ili tagom
npm i my-package@1.0.1-next.0 // version
npm i my-package@next // next tag, prerelease, release candidate
