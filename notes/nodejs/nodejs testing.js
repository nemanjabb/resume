// Testing Express REST API With Jest & Supertest - TomDoesTech
// https://www.youtube.com/watch?v=r5L1XRZaCR0
// https://github.com/TomDoesTech/Testing-Express-REST-API
jest config - test env = node
----
u backend testovima uvek trebas bazu
1. inmemory db
2. mock service
----
supertest - http client + expect()
ima i .expect() ali bolje koristi rezultat pa expect() od jest-a
----
yarn test --detectOpenHandles
posebna createServer() fja omogucava supertest-u da start i stop server
-----
test 404, 200
----
// popusti expect() za object, id, createdAt i dr dinamicki generated
{
    a: 123
    b: expect.any(String) // any string
}
------
// run only one test
describe.only(() => {...})
test.only(() => {...})
---
// skip test
describe.skip(() => {...})
test.skip(() => {...})
-------
// 2. mock service mesto db
// src/__tests__/user.test.ts
// mock service
const createUserServiceMock = jest.spyOn(UserService, "createUser").mockReturnValueOnce(userPayload);
// call api with supertest // system under test
const { statusCode, body } = await supertest(app).post("/api/users").send(userInput);
// assert status and response
expect(statusCode).toBe(200);
// toEqual() umesto toBe() za error: "Received: serializes to same string"
expect(body).toEqual(userPayload);
// assert mock
expect(createUserServiceMock).toHaveBeenCalledWith(userInput);
----
// clear mocks, vraca rezultat iz prethodnog testa
// 1. 
afterEach - jest.clearAllMocks()
// 2. 
clearMocks: true - jest.config.js
resetMocks: true // nije bas jasna razlika od clearMocks? - pitaj
restoreMocks: true // vrati originalnu implementaciju
------
kod testova je repetitivan, jer se samo menjaju inputi i expect statusi i rezultati
zato su i servisi, zbog testiranja i mockovanja
----------
jest extenzija za status, pokretanje i debugiranje testova
----------------
// supertest - http client + expect()
------------------------
// jest
1. in-built test runner
2. assertion library 
3. mocking support
-------------------------
// spy vs stub vs mock
// https://blog.risingstack.com/node-hero-node-js-unit-testing-tutorial/
// 1. spies
get information on function_ calls, like how many times they were called, 
or what arguments were passed to them
----
// 2. stubs 
like spies, but they replace the target function_
control a method behaviour to force a code path (like throwing errors) 
or to prevent calls to external resources (like HTTP APIs)
-----
// 3. mocks
mock is a fake method with a pre-programmed behavior and expectations
-------------------------
// services (db functions) and controllers (http req, res) must be separated for unit tests
// and communicate with standardized interface - args
-----------------------
// jest done() - ako hoces async test koji ne vraca promise, nego ima .catch()
// https://stackoverflow.com/questions/58713379/is-done-required-in-async-jest-tests
async test vraca promise
// implicitno
it("should return 200 OK for POST method", () =>
  request(app).post("SOMEENDPOINT")
    .attach("file", "file")
    .expect(200)
);
---
// explicitno
it("should return 200 OK for POST method", () => {
    return new Promise((resolve, reject) => {
      request(app).post("SOMEENDPOINT")
        .attach("file", "file")
        .expect(200, resolve)
        .catch(err => reject(err))
      ;
    });
  });
----
// ili done(), ako ima arg onda je greska, slicno kao next()
it("should return 200 OK for POST method", done => {
    request(app).post("SOMEENDPOINT")
      .attach("file", "file")
      .expect(200, done)
      .catch(err => done(err)) // za test je vazno samo da li baca exception ili rejected promise
    ;
  });
---------------------
// match part of the object
const anyObject = {
  complex: true,
  otherProperties: "yes",
  foo: "bar",
};
expect(anyObject).toEqual(expect.objectContaining({ foo: "bar" }))
---------------------
// unit controllers, services tests
- unit services: input - argument object, mock prisma, assert service output
- unit controllers: input - http supertest, mock service, assert http response and status, assert service mock calledWithArgs
- always mock one layer bellow
- any class_ can be unit tested
---------------------
// integration testing
// https://www.prisma.io/docs/guides/testing/integration-testing
entire parts of the application, almost like e2e except not clicking on interface_ 
but calling functions, supertest verovatno
-------
ceo test envoronment treba da je odvojen od dev, ne samo baza, znaci i Node.js, a ne dev
---------
za svaki suite create/destroy nova baza sa seed // ipak samo seed/trunc
za svako pokretanje testova create/destroy docker container
----
seed/trunc ide u svaki fajl/describe suite beforeAll/afterAll // zapazi
----
assert with database queries, logicno
-----------
// create/destroy container, migrate db prod, run tests u yarn scripts
// ili Github Actions CI
"docker:up": "docker-compose up -d",
"docker:down": "docker-compose down",
"test": "yarn docker:up && yarn prisma migrate deploy && jest -i"
--------------
// https://dev.to/eddeee888/how-to-write-tests-for-prisma-with-docker-and-jest-593i
// https://github.com/eddeee888/topic-prisma-testing
docker-compose node-server + database services
-------
start tests with command in docker-compose
--------
unit - call service, assert with return value from prisma.user.create(...)
integration - call controller with fetch, assert fetch http response
------------
test/suite/describe
beforeAll - new PrismaClient(), server.listen(80)
afterAll - server.close(), prism.$disconnect()
----------------------------


