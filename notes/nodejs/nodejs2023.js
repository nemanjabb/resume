// event loop
// https://dev.to/sarahokolo/understanding-the-event-loop-in-nodejs-easily-399m
// loop, petlja, ovo ide u jednoj iteraciji:
Microtask Queue (nextTick queue + promise queue) - process.nextTick(cb), promise.resolve(), promise.reject()
Timer Queue - setTimeout(callback, ms), setInterval(callback, ms)
I/O Queue - fs, http
Check Queue - setImmediate()
Close Queue - resource.close()
-----
// redosled u 1 iteraciji:
1. microtask queue
2. timer queue
3. microtask queue
4. I/O queue
5. microtask queue
6. check queue
7. microtask queue
8. close queue
nova iteracija
-------------------------
// event loop
// https://www.instagram.com/theavocoder/
// https://dev.to/lydiahallie/javascript-visualized-event-loop-3dif
// https://www.builder.io/blog/visual-guide-to-nodejs-event-loop - codevolution
----------
// codevolution Node.js playlist, and event loop
// https://www.youtube.com/playlist?list=PLC3y8-rFHvwh8shCMHFA5kWxD9PaPwxaY
// samo event loop
// https://www.youtube.com/playlist?list=PLC3y8-rFHvwj1_l8acs_lBi3a0HNb3bAN
// 42 - Event Loop
javascript - sincronous, blocking, single threaded, libuv za async
v8 engine - heap - for vars, call stack - for functions, global scope, pa ostale fje
libuv
-----
// poenta za async poziv: 
fs.readFile('file.txt', () => console.log('third'))
1. prvo na call stack ide outer fja readFile(), a callback ide u libuv thread pool
2. tek kad se call stack isprazni, callback ide u call stack
-----
event loop- design patter koji coordinates and orchetrates exec of sync anc async code in node.js, u C
-----
// slika, ovo i jeste prioritet, sa strelicama
// part of node.js
1. microtask - nextTick, ima prioritet nad promise
2. microtask - promise 
// part of libuv
3. setTimeout, setInterval
4. IO, fs, http
5. setImmediate
6. close
----
microtask ide posle svakog iz libuv
----
sync js code ima priority, async samo kad je call stack prazan // zapazi
zato setTimeout(0, () = console.log('nesto')) se izvrso posle sync console.log() koji je ispod
--------------
// 43 - Microtask Queues - trivial
// guranje cb u nextTick i promise queue
process.nextTick(() => { ... })
Promise.resolve().then(() => { ... })
// prioritet
sync code -> process.nextTick -> Promise.resolve.then
-----
nested nextTick ide kad se isprazni prethodni level // kao 2 dimension queue
-----
process.nextTick() can starve rest of the loop (IO, etc)
--------------
//  44 - Timer Queue
// guranje u timer queue
setTimeout(() => { ... }, 0)
ide iza nextTick i promise
-----
nested nextTick u setTimeout ide pre setTimeout iz parent level
zato sto je kvadrat unutar kruga na slici
---
ceo event loop je oko hijerarhije prioriteta
---
timer queue je min heap, nije queue
--------------------
// 45 - I/O Queue
fs.readFile(__filename, () => {...})
ide posle microtask
----
// ovde vazna poenta, setTimeout 0 ima random element, source code cpp
timer i IO queue - rabdom, nije garantovan, kako je timer implementiran max(...) // to, uzima sadasnji trenutak...
----
ipak je IO posle timer queue
---------------------
// 46 - I/O Polling
I/O events are polled and callback functions are added to the I/O queue only after the I/O is complete 
zato setImmediate ide pre IO
---------------------
// 47 - Check Queue
setImmediate(() => {...}) ide posle IO (ako nema pooling, nested u fs.readFile npr)
microtask idu pre setImmediate, i izmedju 2 setImmediate
setTimeout 0 i setImmediate redosled nije garantovan opet
dugacka for petlja mece setTimeout pre
----
poenta: ima 6 redova sa callbacks i loop koji racuna prioritete i gura ih u call stack za izvrsenje
---------------------
// 48 - Close Queue
readableStream.on('close', () => {...})
zadnji se izvrsava
----
rezime cele liste
----
// uglavnom poenta prioriteta
nextTick and Promise queues are executed in between each queue and 
also in between each callback execution in the timer and check queues 
-----------
setTimeout(() => {}, 0) pomera u novu iteraciju event loopa
------------------------
// validate environment variables with envalid or Zod
https://www.youtube.com/shorts/jNdJhEDiK6Q
https://github.com/af/envalid
https://github.com/ploskovytskyy/next-app-router-trpc-drizzle-planetscale-edge/blob/main/src/env.mjs
-------------------------
// resavanje problema sa verzijama dependencies u outdated projects // vazno, zapamti
// da nadjes KOJI paket pravi problem // ovo ces CESTO ubuduce da koristis
error log:
Expecting end of input, "ADD", "SUB", "MUL", "DIV", got unexpected "RPAREN" // znaci postcss 7.0.0
yarn why postcss // to
=> Found "postcss@7.0.4"
info Has been hoisted to "postcss"
info Reasons this module_ exists
   - Specified in "devDependencies"
   - Hoisted from "optimize-css-assets-webpack-plugin#postcss" // znaci ovaj je paket, a ne postss, peer dependency
update optimize-css-assets-webpack-plugin v5 to v6 and it works  
--------------------
uvek kad menjas verziju u package.json pa yarn install moras da obrises i yarn.lock // ZAPAZI
------------
// bin folder
bin folder u node.js paketu - specificira executable commandu
js ili script fajlovi 
// bin key u package.json odredjuje komandu
{
   "name": "your-package",
   "version": "1.0.0",
   "bin": {
     "your-command": "./bin/your-command.js"
   }
 }
-------
- g globally - local executables symlinks to folders that are in os path 
-----------------------
// purpose of yarn.lock, chatGpt
// to vazno, koje verzije su zapravo instalirane, package.json ima samo priblizne sa ^ i *
// fiksira sve verzije
Deterministic Dependency Resolution 
// moze fiksirane iz kesa da izvadi
Faster, Reliable Builds
Reproducibility
// poredi hasheve...
Integrity Checks
// vadi iz kesa
Offline Mode
Deterministic Builds
------------------------
// project root dir je najbolje uhvatiti ovako
// anchored to current file within project, just go up
process.cwd() se menja odakle pokreces skriptu
------
const resolve = require('path').resolve;

// __dirname - folder of current js file (this file)
const currentDir = resolve(__dirname);

// this way project root dir is related to folder structure
const projectRootDir = resolve(`${__dirname}/../..`);
----------------
// merge .env vars and yaml 
https://dev.to/emekaofe/how-to-pass-environment-variables-to-a-yaml-file-in-a-nodejs-application-using-ejs-templating-engine-ib7
-----------
// esm modules
package.json type: module ili .mjs
import lib from 'package';
----------------------
// nvm update current lts node
nvm help // uvek, reminder
nvm ls-remote // da vidis nazive verzija
nvm ls // list current
------
// odavde, install, switch, default, uninstall
nvm install --lts // install latest lts
nvm use v20.11.1 // switch version of node
nvm alias default v20.11.1 // set as default
nvm uninstall v20.10.0 // remove current, mora na kraju
------
npm install --global yarn // install yarn again
-------------------------
// test from terminal if node.js global var is defined
node -e "try { require.resolve('fs'); console.log('fs module is available'); } catch (e) { console.error('fs module is not available'); }"
----
fetch from node v21 
------------
// resolutions in package.json yarn
specify override the version of a package that will be used throughout the entire dependency tree
svi drugi paketi ce koristiti tu verziju, Astro, sharp 0.32.6
{
  "name": "your-project-name",
  "version": "1.0.0",
  "dependencies": {
    "some-package": "^1.0.0",
  },
  "resolutions": {
    "some-package": "1.2.3",
  }
}
-----------------------
// console log env vars object, finally, no typescript
// https://github.com/blackflux/object-treeify
// @ts-expect-error, js lib
import treeify from 'object-treeify';
const options = {
  spacerNoNeighbour: '',
  spacerNeighbour: '',
  keyNoNeighbour: '',
  keyNeighbour: '',
};
const stringData = treeify(parsedConfigDataObject, options);
-------------------------
// log deeply nested object without JSON.stringify()
console.dir(myObject, { depth: Infinity });
-----------------
// boolean env var
poenta: jeste string 'true', nije boolean true
// in .env
IS_BUILD=true
// in .ts
const isBuild = process.env.IS_BUILD === 'true'; // zod najbolje uvek
isBuild // true
--------
// sve env vars su uvek stringovi, zato zod // zapazi
// jeste string, logovao
console.log('cacheDatabaseDisabled', cacheDatabaseDisabled);
console.log('typeof cacheDatabaseDisabled', typeof cacheDatabaseDisabled);
cacheDatabaseDisabled true
typeof cacheDatabaseDisabled string // eto
------------------
// upgrade packages with yarn
// Update to the latest version
yarn upgrade package-name
// Update to a specific version
yarn add package-name@version
// Update all dependencies based on package.json version ranges
yarn upgrade
// Update all dependencies to their latest versions, ignoring package.json ranges // sve odjednom, bez pitanja
yarn upgrade --latest
// interactive, jedan po jedan
yarn upgrade-interactive --latest
// Check for outdated packages // dobar
yarn outdated
// Add a package (automatically installs the latest version)
yarn add package-name
---------------------
// changeset
changeset
https://www.youtube.com/watch?v=puQYAhpfpkA
https://www.youtube.com/watch?v=eh89VE3Mk5g
https://www.youtube.com/watch?v=vO80X5zM8_Y
// submodules, monorepo
https://www.youtube.com/watch?v=_mBHaLiWPb4
https://www.youtube.com/watch?v=McEc5yX6kf4
