// backend structure
controllers -> handle request reading, response, status codes etc (express handlers)
services -> handle authorization, business logic and/or multiple models or other svcs
models -> handle data integrity (zod) and transactions with db (drizzle)
routes -> routers for routing (express routers)
-----------
// zato su proxy-iji
// jer svaki paket ima svoj express server
// zapravo microservices, svaki paket je deployed na drugoj MASINI, kubernetes
app.use(MODULE_ROUTE, moduleRouter)

app.listen(port, () => {
  console.log(`${title} listening on port ${port}`)
})
----------------
// multer
multer prosledjuje sve args iz request u file object koji ide u sledece middleware
a mozes i ti na req.nesto  ili next(nesto) da prosledis sta oces, nikako Promise.resolve, bzvz
tipove pogledaj iz index.d.ts Options['fileSize'] itd 
-----------------
node.js filesystem functions, chain callbacks for success and error?
async, sync versions, error handling
-------
// express static folder 
moras da gadjas FAJL, a ne folder // to, zapazi
// folder listing
po defaultu nema listing, mora middleware paket za to 
var serveIndex = require('serve-index');
app.use('/http-path', serveIndex(__dirname + '/file-path'));
--------------------------
// cannonical crud
glavna fora je sto ja nisam imao model layer 
zod validacija u model 
---------
// naming:
// packages, folders, files, classes, functions, routes, types, validation schemas, database tables,
// roles, permissions
------------
// plural, singular, analytic
folder names and permissions follow the business domain naming for modules. 
Module != model. They dont necessarily follow the model that is used. 
The model used here named analytic so we can safely use singular and plural forms to signal single vs multiple
module - name in package.json and folder, monorepo package 
-----
// naming functions
1. controllers will use verbs for REST methods
2. models will use verbs for CRUD
3. service will use verbs for basic CRUD and other appropriate terms e.g. upload/download for more complex ops
----------------
model (db) - singular 
module (monorepo) - plural 
----------------
----------------
// singular bellow (model)
// controller naming:
getMessage, getMessages, postMessage, putMessagex, deleteMessage - from http methods 
// service naming:
createOne, readOne, readMany, updateOne, deleteOne - from CRUD 
// model naming - identical as service, from CRUD
createOne, readOne, readMany, updateOne, deleteOne - from CRUD 
----------
// router, singular:
messageRouter
---------
// types, singular:
InsertMessage, SelectMessage, UpdateMessage, Message - infer from drizzle - typeof messageTable.$inferInsert, for model
// zod schemas, singular:
// created from database table, drizzle util, createInsertSchema(messageTable), createSelectSchema(messageTable)
insertMessageSchema, selectMessageSchema, updateMessageSchema
--------------
--------------
// plural bellow (module):
// route, plural:
/messages 
-------------
// module (package), plural:
messages-module
------
// roles, plural (rest):
CREATE_MESSAGES_ROLE
--------------------------
// clear overview
// singular
database table
model - ORM class_
service methods and types
controller methods
router 

// plural
monorepo packages - modules
api routes - rest 
roles - permissions 