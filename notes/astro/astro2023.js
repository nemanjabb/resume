

--------------
// Astro Crash Course in 20 Minutes! - Coding in Public
fronmatter - code markdown, slicno kao ```...``` samo ---...---
kao jsx ali ne mora 1 root element, i ne treba key za map()
---
mozes da vratis cms (?), markdown, mdx, i async calls (za js deo)
---
// od node 19 create moze uz npm, ne mora npx?
npm create astro@latest
---
src/pages - file based routing, kao next.js
---
vars iz ---...--- dela mozes da stampas u html {var1}
---
<slot>default content</slot> - children prop
---
Astro.props.myprop - prosledjen iz <MyAstroComponen myprop="something" />
----
// styling
style="background-color: white;" ili style={{backgroundColor: 'white'}}
<style></style> default component scope, css modules, klasa - zapazi
<style is:global></style> - globalan, na tag, bez klase
lang="scss", moze za scss nesting, 
npm i -D sass // moras da instaliras
----
za import mora i extenzija jer ima vise frameworks 
import MyComponent.astro
import 'global.css' // importovan je globalan
-----
js u frontmatter ---...--- se izvrsava server side // zapazi, vazno
<script>...</script> // ide u html // izvrsava se client side, zapazi
moze i import 'file.js' u <script></script> client side, kao u html
--------
// integrations, tailwind
npx astro add tailwind
ovo instalira npm paket i dodaje u astro.config.mjs
---
// react
npx astro add react
export default defineConfig({integrations: [tailwind(), react()]}) // ovo dodaje u astro.config.mjs
astro by default salje na client samo html bez js // to
posalje (render) go html komponente ali state i events ne radi, nema interaktivnost
<MyReactComponent client:load /> // direktiva, da uljuci js na client - to su ISLANDS
client:visible - lazy load, code split
-----------------------------
-----------------------------
// Astro Blog Course - Coding in Public
// https://www.youtube.com/watch?v=F2pw1C9eKXw&list=PLoqZcxvpWzzeRwF8TEpXHtO7KYY6cNJeF
// #1 - Intro and Installation
salje go html na client, bez js
island architecture - komponente koje su hidrirane, za interaktivnost
island downloads js na load ili visible
-----
// file.astro
--- // ovo su linije u fajlu
frontmatter - js, pass props, await (build time)
---
jsx-like templating, html
--------
// #2 - Astro Basics
poenta:
u frontmatter server side js, i typescript
u <script/> client side js, moze import i tu
u html sve iz html, style, script..., jsx bez className, fragment, key 
salje go html na client po defaultu
---
astro radi samo u /src folderu
/public folder je server root, ne transformise nista
-----------------
// #3 - Layouts
slot(children), default val, named slots
<slot name="my-slot" /> <p slot="my-slot" />
-----
// named slots
// https://docs.astro.build/en/core-concepts/astro-components/#named-slots
vise children (sibling) elemenata, named koji child ide gde, a svi children koji su neoznaceni idu udefault slot 
prenosenje argumenata (chidlren content) koji ide gde
---
// ovde ce da zavrse, Wrapper.astro // ubacivanje sadrzaja
<div id="content-wrapper">
  <Header />
  <slot name="after-header"/> 
  <Logo />
  <h1>{title}</h1>
  <slot /> 
  <Footer />
  <slot name="after-footer"/>  
</div>
// oni sto je inicijalno u <slot>ovde</slot> je default content slota
----
// children poslati odavde // prosledjivanje sadrzaja
<Wrapper title="Fred's Page">
  <img src="https://my.photo/fred.jpg" slot="after-header" /> {/* named */}
  <h2>All about Fred</h2> {/* noznacen, ide u default slot */}
  <p>Here is some stuff about Fred.</p> {/* noznacen, ide u default slot */}
  <p slot="after-footer">Copyright 2022</p> {/* named */}
</Wrapper>
----------
// react, na ovo se svodi, trivial
// samo props ali prosledjeni kao content
<Wrapper>
  {prop1}
  {prop2}
  {prop3}
</Wrapper>
-----------
// drugi nacin za slot 
// https://astrowind.vercel.app/astrowind-template-in-depth
// hvatanje prop-a
const content: string = await Astro.props.render('default'); // default je default slot, moze i named
const content = props.children; // react verzija, trivial
---
// ubacivanje sadrzaja
<div set:html={content} />
<div>{children}</div> // react verzija, trivial
----------------
// shorthand za props, kao js object
<MyLayout title={title} /> <MyLayout {title} />
-------
// #4 - CSS & styling
inline - string, object, scoped ili is:global
<style /> - scoped za component 
scss
// jeste css vars prosledjuje, dynamic variables
<style define:vars={{var1}}> 
    p {background-color: var(--var1)}
</style>
// kao clsx
<p class:list={['class1', {class2: someBoolVar}]} />
-------
astro koristi vite by default
---
postcss-preset-env plugin
----
samo import css files je global by default
------------
// #5 - Astro Components, isto ko react, nista
----
ako child componenta ima <script /> to isto radi na clientu
----
astro-icons - integration (libs?), instalira se
local /icons file ili predefined name from pack
-----------
// #6 - Routing basics
file system routing
public folder - /slika.jpg
/ je root - public folder, absolute path
./ je relative path
---
404.astro
---
mdx - md + javascript (i jsx)
---
md isto ima ---...--- frontmatter header
page.md se ucitava na rutu
-------------------
// #7 - Markdown layouts
nadalje pravi blog, trivial, ne objasnjava astro features
---
layout: "../../layouts/BlogPostLayout.astro" // koja komponenta da cita md
title: ... // ostali postaju props u komponenti
...
---
// ovako se hvataju props, iz md + jos neki default
// ceo md su string props
const { frontmatter } = Astro.props;
content props su stringovi, on posle styluje
-------
Image component, isto kao Next.js
avif format, manji nego webp
-------------------
// #8 - Card component // trivial
top level await - znaci da je root file funkcija async, frontmatter astro
// citanje vise .md fajlova odjednom
const allPosts = await Astro.glob("./*.md");
hvata title svih postova
url je default Astro prop, nije u markdown
------
card html and props // trivial
--------------------
// #9 - Filter/sort posts // trivial skroz, nema veze sa astro
reduce za filter i sort po vise params
filteredPosts.sort(() => Math.random() - 0.5) // shuffle, random
-------------------
// #10 - Dynamic routes
[[...name]] - imenovanje routing fajlova, sve isto kao next.js
[...var] - catch all, za nested routes
[[var]] - optional, za folder, bez index.astro
// pokrivanje dynamic paths, getStaticPaths(), isto kao next.js
// zapravo ovde je getStaticPaths = getStaticPaths + getStaticProps
// vraca paths (i props za stranicu) stranica koje hoces da prerenderujes, array of routes
export async function getStaticPaths() {
    const allPosts = await Astro.glob("../blog/*.md");
    // od toga generise array ruta i props
}
---
// hvatanje params
const { category } = Astro.params;
// props, kao i uvek
const { name } = Astro.props;
---------------------
// #11 - Pagination - jeste astro, paginate() fja
// daje paginate() fju kao arg u getStaticPaths
export async function getStaticPaths({ paginate }) {
    // prerenderovace sve ovde formattedPosts
    return paginate(formattedPosts, { // sve ova fja racuna od allItems i pageSize, i vraca u props
        pageSize: 1, // prima options
      });
}
const { page } = Astro.props; // [...page].astro
page.data.map((post) => ()) // page.data
// daje prev, next cursore
<Pagination prevUrl={page.url.prev} nextUrl={page.url.next} />
-----------------------
// #12 - Tag cloud
...
------------------------
------------------------
// kolekcije, content folder
kolekcija - content folder, mdx blog clanci
content/config.ts - samo zod sema od mdx frontmatter exportovana, za tipove
// ovo je fja koja koristi - poziva definisanu kolekciju
// src/pages/blog.astro - lista
// src/pages/blog/[slug].astro - stranica
import { getCollection } from "astro:content";
bilo koja stranica koja koristi md content
------------
// SSG
export async function getStaticPaths() {
    const blogEntries = await getCollection("blog");
    return blogEntries.map((entry) => ({
      params: { slug: entry.slug },
      props: { entry },
    }));
  }
  
const { entry } = Astro.props;
// sto vraca getCollection() ima render() metodu
const { Content } = await entry.render(); 
-----------------
// frontmatter default SSG
------
// ako ima Async fju onda je SSR
import { getCart } from '../api';
...
const cart = await getCart(Astro.request);
---
// <script /> je CSR
--------
// i Astro ima api ko Next.js
/src/pages/api/...
redirect, itd
// ovo su server enpoints
export async function POST({ cookies, request }: APIContext) {}
----------
// good SSR example
// https://github.com/withastro/astro/tree/main/examples/ssr
examples/ssr/src/pages/index.astro
---------
mdx - md + jsx + js // i js, zapazi
--------------------
shiki, prism - syntax highlight for code snippets
--------------------------
// astro-paper docs articles, ovo na dole
https://github.com/satnaing/astro-paper#-documentation
-----
// https://astro-paper.pages.dev/posts/tailwind-typography-plugin/
tailwind base styles remove all user-agent (browser styles)
onda @tailwindcss/typography plugin '.prose' classa opet vrati defaults za tekst (ali sad u definisani u tw, ne u browser)
------
frontmatter u md blog postovima je yaml // zapravo jeste, ima i liste sa -
--------
astro optimizuje slike buildtime ili runtime, kao next.js Image 
https://docs.astro.build/en/reference/image-service-reference/
samo slike u /src folder, slike u /public su served kakve su takve su 
-----
vercel/satori paket za render blog naslova za OG image 
--------
Astro Content Collections - zod schema za blog frontmatter, getCollection() fja bez glob staze, to je to
---------------------
// https://github.com/flexdinesh/blogster
// markdoc
mdx = jsx + md - moze js code, docs as code
markdoc = templating blokovi kao asp, ne moze js code, docs as data // ok, jasno
// primer
{% tweet url="https://twitter.com/flexdinesh/status/1605685194312122370" /%}
------
// mapira se komponenta na markdoc tag
packages/shared/src/markdoc/markdoc.config.ts // i ovde mapiranje // eto ga config fajl
templates/bubblegum/src/components/Renderer.astro // mapping object
templates/bubblegum/src/components/YouTubeEmbed.astro // astro komponenta
-----------
/templates folder je dupliran built od /src
----------
// react integration 1 linija posla // zapazi
npm install @astrojs/react
// astro.config.mjs
import react from '@astrojs/react';
export default defineConfig({
  integrations: [react()],
});
----------------------
// astro docs clanci
// markdoc
https://docs.astro.build/en/guides/integrations-guide/markdoc/
// mdx
https://docs.astro.build/en/guides/markdown-content/
1. components
2. pages
3. content collection entries
----
// .vscode/settings.json za markdoc syntax
{
  "files.associations": {
    "*.mdoc": "markdown"
  }
}
----
{% aside type="tip" %}
// markdoc children, isto ima open and closing tags
Use tags like this fancy "aside" to add some _flair_ to your docs.
{% /aside %}
--------
// /src/content/markdoc.config.mjs
export default defineMarkdocConfig({}) // tu je config za markdoc
.mdoc je fajl ekstenzija
----------------------
// cannonical url
glavni url koji google search engine treba da koristi
ako je stranica dostupna na vise linkova koji je glavni, tj default
---------------------------
// getStaticPaths
samo to sto vratis u {params: slug} odredjuje na kojoj ruti ce stranica biti dostupna
slug matches [slug].astro file name 
slug u .md nema veze sa rutiranjem, tj da stranica bude tu dostupna
-----------
// https://docs.astro.build/en/core-concepts/routing/#static-ssg-mode
[...slug].astro i [slug].astro razlika je samo sto ... hvata sve nested /a/b/c segments u jedan string
zavisno kako je fajl imenovan [slug1]-[slug2].astro // moze i ovo
// primer
src/pages/[lang]-[version]/info.astro
{params: {lang: 'en', version: 'v1'}},
------
{params: undefined} ako hoces da pokrijes folder, tj root
----------------
// ovo mi pravilo veliki problem za getStaticPaths 404, izgubio ceo dan, zapazi
// trailing slash / option u astro.config.mjs
// https://docs.astro.build/en/reference/configuration-reference/#trailingslash
trailingSlash: 'ignore' // default, tako i treba
-------------
getStaticPaths inace vraca {params, props} // ok
----------------
// ovaj atribut pise koja je komponenta u Astro, odlicno za debugiranje
data-astro-source-file="/home/username/Desktop/nemanjam.github.io/apps/nemanjamiticcom/src/components/Content.astro"
--------
cant write function_ that returns jsx in frontmatter?
-------------------------
// Astro transitions
// Astro View Transitions (3.0 Release!) - Coding in Public
// https://www.youtube.com/watch?v=9T4N0cIlBUE
from v3.0, samo u Chromium browserima za sad 
ima fallback direktiva za nepodrzane browsere
isto kao css transitions - translate, scale, rotate, fade ... 
prelaz elemenata dok se menja ruta - stranica
transition: direktiva, atribut, iza :name, :persist, :animate ... , moze ih vise odjednom, objekat
emituje evente koji se hvataju u <script /> "astro:after-swap" npr 
:name je isto sto i ="transition-name"
transition:animate={fade(duration: '0.2s')} //funkcija sa options object
transition:persist - je da se zadrzi state izmedju stranica
---------
//  Advanced View Transitions in Astro - Leabs
// https://www.youtube.com/watch?v=E749WFtPojg
fade je default transition 
<ViewTransitions /> - component u jsx, da bi moglo ostalo
// dobri primeri u kodu za reuse
// https://github.com/igorm84/spotify-astro-transitions
----------------------
// astro and react components
astro components moraju da idu u root, tj. ne mogu da se pozivaju u react, solid, itd // vazno
mogu u react samo kao slots, ista prica kao next.js 
astro components - ssg by default, nemaju ni js (hydrated), mora <script /> tag 
-----
// client:load je hydration, za events
<ThemeSwitcher client:load />
-----
// astro state
za lokalni state (i globalni preko localStorage) mora nanostore
https://docs.astro.build/en/recipes/sharing-state/
https://github.com/nanostores/nanostores
cak i tu ne moze direktno u props da se ubaci nego samo postavi atribut u dom u <script />
------------------
// ikone
// ova biblioteka, moze i lokalni svg, moze i name iz pack
https://github.com/natemoo-re/astro-icon
yarn add -D @iconify-json/mdi // material design icons
<Icon name="mdi:hamburger-menu" />
------
// custom folder
integrations: [
  icon({
    iconDir: "src/assets/icons",
  }),
],
--------------
u .mdx moraju da se importuju komponente koje se koriste, kao u .jsx, kad updateujes imports // zapazi
-----------------------
// src/page.mdx direktne stranice, koje nisu content // zapazi
// frontmatter ima layout specificiran
---
layout: '../layouts/Page.astro'
title: 'About Me'
description: 'Lorem ipsum dolor sit amet'
---
// mdx INTEGRATION docs
https://docs.astro.build/en/guides/integrations-guide/mdx/
// general markdown and mdx docs
https://docs.astro.build/en/guides/markdown-content/
-----
markdown je general configuration, mdx je integration koja extends markdown 
frontmatter je YAML // ZAPAZI
mdx - JSX, JavaScript expressions and components in Markdown content // ok
------
// 1. content collections
src/content/ folder
validate frontmatter
TypeScript type-safety for content (frontmatter)
------------
// 2. ROUTED .md pages // to
// primer:
// fajl: src/pages/posts/post-1.md
---
layout: ../../layouts/BlogPostLayout.astro // layout komponenta u koju ce da se prosledi ovaj frontmatter i .mdx
title: Astro in brief
author: Himanshu
description: Find out what makes Astro awesome!
---
This is a post written in Markdown.
------------
// props koji se pojavi u layout komponenti
const {frontmatter} = Astro.props; // eto
---
<html>
  <!-- ... -->
  <h1>{frontmatter.title}</h1>
  <h2>Post author: {frontmatter.author}</h2>
  <p>{frontmatter.description}</p>
</html>
----
// poenta: 
astro ce da parsuje i renderuje markdown svojim parserom
frontmatter ce da se pojavi kao props u layout komponenti
----
// rendered headings automatski imaju id links
## Conclusion
<h1 id="conclusion" />
----
.mdx files se mogu importovati u .astro komponentama - fajlovima
-----------
// todo: procitaj ostatak
-------
// moze i remote markdown sa githuba // eto
// https://docs.astro.build/en/guides/markdown-content/#fetching-remote-markdown
mora poseban paket za parsing, npr: https://github.com/markedjs/marked)
// primer:
src/pages/remote-example.astro
---
// Example: Fetch Markdown from a remote API and render it to HTML, at runtime.
// Using "marked" (https://github.com/markedjs/marked)
import { marked } from 'marked';
const response = await fetch('https://raw.githubusercontent.com/wiki/adam-p/markdown-here/Markdown-Cheatsheet.md');
const markdown = await response.text();
const content = marked.parse(markdown);
---
<article set:html={content} />
----------------
// markdown styling
https://docs.astro.build/en/guides/styling/#markdown-styling // kratak pasus
standardno astro stilizovanje, globali i scoped styles, samo gadjas selektore
-----
opet nije objasnjeno koji su selektori u rendered markdown??
-----
// kako layout handluje md
https://docs.astro.build/en/basics/layouts/#markdownmdx-layouts
// props objekat shape u layout - Astro.props
{
  file - md file absolute path
  url - page relative url
  frontmatter: {
    file - md file absolute path // isto, oba
    url - page relative url
  }
  headings - polje objekata
  (Markdown only) rawContent() - md as string with \n newlines
  (Markdown only) compiledContent() - md rendered to html
}
----
markdown html ide u <slot /> layouta // eto
----
// jedan layout moze da handluje (fallback) .md, .mdx, and .astro fajlove
------------------
projects/[...page].astro - [...] je da uhvati root (index) rutu /projects/, bez toga 404 // zapamti
zapravo nije isto astro i next.js routing, [[page]].astro je nevalidno u astro // zapazi
pogledaj astro docs za route
----
// podsetnik za next.js
[[page]].tsx - next.js optional route, root ne baca 404
[...page].tsx - next.js catch all route, hvata nested /projects/a/b/c
----
// astro
// https://docs.astro.build/en/guides/routing/#rest-parameters
[...path].astro - match file paths of any depth - /projects/a/b/c, undefined matches index (root) // ok, eto

