// astro images, practical from projects
// astro-paper
// https://astro-paper.pages.dev/posts/adding-new-posts-in-astropaper-theme/#storing-images-for-blog-content
// 1. og image
ogImage url in frontmatter
dynamic OG image - render from article, fallback
-------
// md
// 1. import from src/assets - astro optimized, preferred // path
// only mdx imports
// zapravo moze i url ovde ![alt-text](url)
![something](@assets/images/example.jpg)
![something](../../assets/images/example.jpg)
// wont work
<img src="@assets/images/example.jpg" alt="something" /> 
// 2. public folder, used without optimization, as is // relative or absolute url
/public/assets/images/example.jpg
![something](/assets/images/example.jpg)
<img src="/assets/images/example.jpg" alt="something" /> // url, works
--------------
// billyle.dev
// local _images folder u collections
![Docker Desktop](./_images/docker-new-workflow/docker-desktop.png)
image u frontmatter mu je ogImage
import_ { Picture } from "astro:assets";  // ??
-------------
// paularmstrong.dev
// bolje ova komponenta nego <img />
import { Image } from 'astro:assets';
<Image {...props} width={width} height={height} src={src} transition:name={name} />
---
![alt-text](url) u md ce da uradi isto, pogledaj astro docs
----------------------
astro getCollection() moze da cita i json, ne samo frontmatter yaml i md // zapazi
export const projectCollections = defineCollection({
    type: "data", // type data je za json, inace default je 'content'
    schema: project,
  });
----
zod schema moze da bude fja koja prima arg (image npr)
// pass return type onda za infer
export type Project = z.infer<ReturnType<typeof projectSchema>>;
-----
any folder (_schemas) in src/collections fill be added to type_ CollectionKey 
underscore excludes only from routing, not from type // bug
------------
// astro.config.mjs relative imports
all imports and their subtree imports (config utils, schema, types) in astro.config.mjs must use relative imports
typescript path aliases arent supported in both .mjs and .ts 
// https://github.com/withastro/astro/issues/9782
// path aliases docs
// https://docs.astro.build/en/guides/aliases/
------------------
// astro links realtive paths
u stvari to je obican <a href="" /> tag, <Link /> je njegovo samo za css
realtivni linkovi su inteligentni pomalo // zapazi
ako pocinje od domena ponasa se kao apsolutna staza
----
na localhost:3000/blog/categories stranici:
herf=post-slug-1 ide na localhost:3000/blog/categories/post-slug-1
herf=/blog/categories/post-slug-1 ide na localhost:3000/blog/post-slug-1, gde i treba // zapazi
----------------
// Image vs Picture
// https://docs.astro.build/en/guides/images/#picture
<Picture /> - nekoliko formata, rezolucija (responsive, srcset), picture tag // zapazi, html
// generisan output:
<picture>
  <source srcset="/_astro/my_image.hash.avif" type="image/avif" />
  <source srcset="/_astro/my_image.hash.webp" type="image/webp" />
  <img
    src="/_astro/my_image.hash.png"
  />
</picture>
---------
// i Image podrzava srcset sa widths={[...]} i sizes={[...]}
// https://docs.astro.build/en/guides/images/#widths
<Image
  src={myImage}
  widths={[240, 540, 720, myImage.width]}
  sizes={`(max-width: 360px) 240px, (max-width: 720px) 540px, (max-width: 1600px) 720px, ${myImage.width}px`}
  alt="A description of my image."
/>
-----------------------
// type for Astro.props 
top level stranice u pages folderu nemaju type_ za props, vec default fronmatter Astro sto prosledi i md pluginovi
maybe with pagination in getStaticPaths??
-----------------------
// nije isti Dockerfile za static site i SSR i api
za SSR ti treba i node na serveru sa node_modules iskopiranim // zapazi
za static dovoljan samo nginx koji salje static client js i html // zapazi
https://docs.astro.build/en/recipes/docker/
------
node alpine nema openssl za prismu, inace moze za ostalo 
nginx alpine slim 
gledaj velicinu slike u tags tabu na docker hub, i source i readme za sliku na githubu
-----------------
// pass server vars to inline client script
<script is:inline define:vars={{ myVars }} />
// sve direktive na jednom mestu, class:list, is:inline, set:html...
// https://docs.astro.build/en/reference/directives-reference/
---------------
// type za props, nasledi od base html elementa
// class npr
export type Props = astroHTML.JSX.AnchorHTMLAttributes;
----------------------
// Astro moze SSR, vhatGpt
// ima integracija, astro.config.mjs
integrations: [ssr()],
komponentu definises u frontmatter delu, a u jsx delu je samo renderujes
-------------
// Astro remote markdown
// https://docs.astro.build/en/guides/markdown-content/#fetching-remote-markdown
opet je SSG
---
// frontmatter is executed on server // zapazi

// Example: Fetch Markdown from a remote API
// and render it to HTML, at runtime.
// Using "marked" (https://github.com/markedjs/marked)
import { marked } from 'marked';
const response = await fetch('https://raw.githubusercontent.com/wiki/adam-p/markdown-here/Markdown-Cheatsheet.md');
const markdown = await response.text();
const content = marked.parse(markdown);
---
// client accepts rendered html
<article set:html={content} />
--------
u Astro frontmatter --- --- se izvrsava na serveru, a jsx na clientu // zapazi i ne zaboravi
--------------------
// important, zapazi, izgubio pola sata
cant export function_ from Astro component, it will call component with Astro.props = undefined 
samo tipovi moze
-------------
all links in Astro are relative /blog/2, both in js and in DOM // zapazi
// astro.config.mjs
site: SITE_URL,
----------------
// default (fallback) content for slot // ok fora
// https://docs.astro.build/en/basics/astro-components/#fallback-content-for-slots
slot moze da ima open i closing tag // zapazi
onda content slota je fallback ako se slot izostavi
<slot name="my-slot">
  <p>This is my fallback content, if there is no child passed into slot</p>
</slot>
------
default slot je sav children sadrzaj osim named slotova pored
<slot name="default" /> je isto sto i <slot />
moze da se pozove sa <Fragment slot="default"> ... </Fragment>
Fragment ide kad hoces vise od jednog siblinga bez wrappinga
--------------
// Dynamic API Endpoints in Astro - Coding in Public
https://www.youtube.com/watch?v=Q1GJpcJLXhQ
GET endpoints su available i u SSG mode // zapazi, znaci og-image treba da radi
---
za POST, PUT... treba SSR adapter, node, vercel... 
------------------------
// Astro render porps, passed function // dobar
Astro.global ima mnogo funkcija // zapazi
// https://docs.astro.build/en/reference/api-reference/#astroslotsrender
// https://stackblitz.com/edit/github-kc4vjm?file=src%2FComponentWhichIteratesAList.astro,src%2Fpages%2Findex.astro
// parent - FilterList
<ul {...props} class={cn('flex flex-wrap gap-2', className)}>
{itemLinks.map(async (itemLink) => (
  <li class="inline-block" set:html={await Astro.slots.render('default', [itemLink])} />
))}
</ul>
// child
<FilterList class="mb-4" itemLinks={tagLinks}>
{
  ({ href, textWithCount, isActive }: FilterLink) => (
    <Tag href={href} size="md" class={cn({ 'underline font-bold text-accent': isActive })}>
      {textWithCount}
    </Tag>
  )
}
</FilterList>
------
// zapazi sva 3
// first arg 'default' je ime slota
// second arg su args [...args]
// vraca html koji se prosledjuje u set:html
const html = await Astro.slots.render('default', [itemLink])
---------------------
u yarn build log mozes da vidis SVE slike i stranice koje se generisu // zapamti
----------------
// destructure slot from props, prosto
// https://docs.astro.build/en/guides/framework-components/#passing-children-to-framework-components
// pass
<ul slot="social-links" />
// get from props
<footer>{props.socialLinks}</footer>
ALI: ovo je samo za frameworks koji imaju children prop, React, Preact, and Solid // zapazi
ne radi za Astro 
Astro komponente su samo serverside html, ne moze client: direktiva na njima, mora <script /> tag 
// https://docs.astro.build/en/guides/framework-components/#can-i-hydrate-astro-components
---------------------
// ovde je sve objasnjeno // TO
// https://cassidysmith.dev/posts/modify-astro-slot-children#grab-the-astro-slot-contents
u Astro ne moze da se prosledi Astro komponenta kao children kroz props 
mora Astro.slots.render() pa set:html={html} // globalno Astro, zapazi, a ne Astro.props
mora eksterna biblioteka za dom parsovanje linkedom
// onda moze setAttribute na elementima, JS DOM fje
import { parseHTML } from 'linkedom'

const html = await Astro.slots.render('default')
const { document } = parseHTML(html)
const children = document.children

children[0].setAttribute('open', '')
------------------
// class not applied to named slot // izgubio 2 sata, zapazi
ako pozivas slot.render()
const beforeIconHtml = await slots.render('before-icon');
onda klase ne rade na njemu 
<slot name="before-icon" />
nego mora pass taj html 
<Fragment set:html={beforeIconHtml} />
----
sve ovo da bi izbegao prazan wrapper elemenat kada slot nije prosledjen
{
  beforeIconHtml && (
    <span class="inline-block">
      <slot name="before-icon" />
    </span>
)}
-------------------------------
// astro nested dynamic routing, getStaticPaths()
// https://docs.astro.build/en/guides/routing/#nested-pagination
// glavna fora:
// ovo je najvaznije
mora ime fajla [...slug].astro ili foldera [...slug] 
da se matchuje sa key u params { pageSize, params: { slug: 'my-slug' } }
-------
[...page].astro page key je rezervisan za broj sa paginate() fjom // zapazi
params: { ...rootPagination[0].params, page: '1' }
-------
[...slug] hvata nested routes '/tags/react' // i to je to
params: { slug: undefined } hvata root routu
params: { slug: `tags/${tag}` } // nested ruta primer
-------
// primer hvatanja root routa
const slugs = [undefined, 'tags', 'categories'];
const rootPagination = slugs.flatMap((slug) => {
  const pagination = paginate(posts, { pageSize, params: { slug } });
  return pagination;
});
-------
rute se ne hvataju (to je SSR) nego PREGENERISU (to je SSG)
-------
ok, malo teze ali ne toliko
sve je ovo u getStaticPaths()
vidi se u yarn build koje rute si generisao, verovatno ima i laksi nacin da se loguju generisane rute // istrazi gpt
--------------------------
// params and pathname
const { filter, page } = Astro.params;
const { pathname } = Astro.url;
----
u params su tipovana imena fajlova i foldera, dynamic routes 
src/pages/blog/categories-and-tags/[...filter]/[...page].astro
----
pathname je sve posle domena 
----
// eto sta koji hvata
// za http://localhost:3000/blog/categories-and-tags/tags/react/2
filter = 'tags' 
page = 'react/2' 
pathname = '/blog/categories-and-tags/tags/react/2'
-----------------
// debug props u jsx // odlicna fora, zapamti
<div data-myProp={myProp} />
--------------
// github pages deploy
u Github settings 2 opcije:
1. Github Actions // sto sam ja
2. from gh-pages branch
----
// 2.
deployuje _sites folder kada push na gh-pages branch
ispod haube isto pokrece github akciju
----------------
// hiding elements with media query, css only, SSG
<style is:global>
  [data-id='post-card-tag-list'] > li:last-child {
    @apply hidden xs:list-item;
  }
</style>
--------------------
// frontmatter iz pages.mdx
sav frontmatter iz file.mdx se pojavljuje u Page layout u props.content
ne treba schema
a sam content ide u <slot /> u layout
----------------
500.mdx page ide samo za SSR, nema za SSG // zapazi
------------------
// sitemap integracija, prosto i dobro objasnjeno
// https://docs.astro.build/en/guides/integrations-guide/sitemap/
// official integracija, samo options object
import sitemap from '@astrojs/sitemap';
--------
// integracija at build time generise dva fajla
https://nemanjamitic.com/sitemap-index.xml // ovaj je za root samo, pokazuje na 0
https://nemanjamitic.com/sitemap-0.xml // ovaj zapravo ima linkove
-------
serialize(item) => {...} - fja options za svaki link posebno, item.changefreq npr
-------
moze api endpoint pages/robots.txt.ts za dynamic robots.txt // ok
----------------------
----------------------
// environment variables
// https://docs.astro.build/en/guides/environment-variables/
----- 
// default env vars
import.meta.env.MODE, import.meta.env.PROD, import.meta.env.DEV // za node_env
import.meta.env.SITE - config.site 
import.meta.env.BASE_URL - domain.com/pathname
to su sve vite vars, ne astro
-----
// env files
astro cita .env, .env.production, .env.development po defaultu
ali env vars mece u import.meta.env, ne u process.env // zapazi, glavna poenta
import.meta.env.MY_VAR
import.meta feature added in ES2020, fature u js  // to je glavno za astro
-----
// globalni env var tipovi
u src/env.d.ts
interface ImportMetaEnv {
  readonly DB_PASSWORD: string;
  readonly MY_VAR_1: string;
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv; // samo nested key
}
---
// mora duplirani tipovi za process.env za astro.config.mjs
declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: 'development' | 'production' | 'test';
    SITE_URL: string;
    PREVIEW_MODE: boolean;
  }
}
-----
u SSG SVE env vars su build time // zapazi i zapamti
-----
zod validacija?
----------------
.env file -> config -> citas iz config.my_var // to
jedino ako env var nije ukljucena u config, onda import.meta.env.my_var 
// NAJVAZNIJA FORA, zaboravio
// https://github.com/withastro/astro/issues/3897
import.meta.env nije dostupan u astro.config.mjs (i importovanim fajlovima)
tek NAKON config fajl je loaded, dostupno u ostatku koda 
za astro.config.mjs MORA process.env i dotenv - dotenv.config({ path: envFileName });
---------------
1. u astro.config.mjs - process.env 
2. u ostatku koda - exported CONFIG object
3. sta nema u CONFIG koristis is import.meta.env
--------------------
// Astro.url.pathname vs Astro.params.slug
const { pathname } = Astro.url;
const { slug } = Astro.params;
// primer:
pathname: /projects/2024-05-16-example-project-3 // ukljucuje sve segmente od base_url
slug: 2024-05-16-example-project-3 // samo zadnji
------
// Astro.params.slug zavisi sta si prosledio u getStaticPaths
const paths = sortedProjects.map((page) => ({
  params: { slug: page.slug }, // tu, prosledis sta god
  props: { page },
}));
-----------------------
// infer Astro props type from another component
// https://docs.astro.build/en/guides/typescript/#componentprops-type
import type { ComponentProps } from 'astro/types';
import Button from "./Button.astro";
type ButtonProps = ComponentProps<typeof Button>;
-------------------
// Astro web components
// https://docs.astro.build/en/guides/client-side-scripts/#web-components-with-custom-elements
----------------------
// api endpoints sa getStaticPaths
// https://docs.astro.build/en/guides/endpoints/
poenta: ima samo params iz getStaticPaths, nema props // zapazi
export function getStaticPaths() {
  return [
    { params: { id: "0"} },
    { params: { id: "1"} },
    { params: { id: "2"} },
    { params: { id: "3"} }
  ]
}
export const GET: APIRoute = ({ params, request }) => {
  const id = params.id;
  return new Response(
    JSON.stringify({
      name: usernames[id]
    })
  )
}
-------------
// api endpoints, zapazi
1. Static File Endpoints - on build time, ima getStaticPaths 
2. Server Endpoints (API Routes) - server ili hybrid mode, NEMA getStaticPaths, cita request context with headers
----------
// extenzija za fajl u imenu
[...route].png.ts // og image, [...] string | undefined
--------------------------
// <script /> scripts, prouci
// https://docs.astro.build/en/guides/client-side-scripts/
<script is:inline /> - kao html tag, tu gde jeste, ne procesira se dalje, ne moze import_ drugih .ts/.js fajlova 
// ima jos...
------------------
// open every component for class and props like this
import type { HTMLAttributes } from 'astro/types';
export interface Props extends HTMLAttributes<'details'> {
  title: string;
}
const { title, class: className, ...props } = Astro.props as Props;
<details {...props} class={cn('rounded-button bg-base-200 px-4 py-1', className)}>
  ...
</details>
---------------------------
---------------------------
// astro transitions
// Astro View Transitions Overview - Coding in Public
// https://www.youtube.com/watch?v=9MChTVlXbf8
// https://github.com/coding-in-public/astro-view-transitions-overview
inside <head /> tag, fade je default_
import { ViewTransitions } from 'astro:transitions';
<ViewTransitions />
---
// https://docs.astro.build/en/guides/view-transitions/#fallback-control
// fallback prop - za unsupported browsers
'animate' - default_, rade i dalje ali na drugi nacin, using custom attributes
'swap' - nece pages?
'none' - nece nijedne animacije
------------
transition:name - treba da ima unique id od te stranice (route) ili elementa
by default astro radi hard reload page na route channge - multi page app 
transition menja u soft reload 
-----
// js u <script /> tagovima mora u event callback (ran once...?)
document.addEventListener('astro:page-load' () => {
  // script code
})
-------
// transition client side state between pages
// renderovana komponenta na 2 stranice
<MyReactComponent 
  client:load 
  transition:perisist="transition-name" // zadrzava state na page change
  transition:perisist-props // zadrzava props, ali ne na hard refresh
/>
-----------
<tag transition:animate="..." />
'fade' - default_ 
'initial' - browser default_
'none' - removes browser default_ 
'slide' - sa leva na desno 
---
transition:name je default_ 'slide' mislim?
------
// prima options, jeste css transition style rules obican
<main transition:animate={fade({duration: '5s'})} />
// ili css objekat seda ovde
<main transition:animate={cssStyles} />
-----
// na linkovima moze da se disablira transition za tu rutu (href)
// data-astro-reload, data-astro-history="..."
// navigate(...) redirect bez linka plus transition
<a data-astro-reload href="" />
--------------
zasniva se na browser api
::view-transition - pseudo element
-------------
// nije podrzano u Firefox, current 128 // zapazi
// https://caniuse.com/?search=View%20Transition%20API
Chrome, Edge, Safari, Opera - podrzano, mobile i desktop
------------------------
// site: SITE_URL var + '/'
// url: full page url, cannonical url, per page
const { site, url } = Astro; // objects
---------------------
// import all images from folder
// https://docs.astro.build/en/recipes/dynamically-importing-images/
// https://cosmicthemes.com/blog/astro-dynamic-image-imports/
import.meta.glob je Vite funkcija 
--------
// glob za slike
poenta, ne sme da ima {} za jednu opciju
// ovo puca
'/src/assets/images/all-images/*.{jpg}',
// ovo radi
'/src/assets/images/all-images/*.jpg',
----------------
// ova lib samo za ssr slike, u okviru istog servisa
daje url on fly 
ne treba ako imas cdn, niti za ssg 
https://unpic.pics/lib/
-----------------
// Astro Image u React components
// https://docs.astro.build/en/guides/images/#generating-images-with-getimage
mora getImage() fja koja ce da generise src i props 
ne moze Astro Image ni kao slot, ni kao prop, ni kao fja u React component 
ne moze custom props da se prosledi 
Astro Image rendered once at build time, React runs in browser 
----------------
// dark theme without white flash
// Build a Dark Mode without a White Mode Flash! - Coding in Public
// https://www.youtube.com/watch?v=I6ynSVZdX04

// astro.config.mjs
output: hybrid - je za static by default, pojedine strane mogu SSR 
u SSG imas pristup cookie-u iako ruta nije SSR 
-----
ostale opcije, node, netlify, vercel 
-----------
// https://docs.astro.build/en/basics/rendering-modes/
astro islands - CSR embed, less than a page, js frameworks // ok
------
1. output: 'static' - SSG default u Astro, ista stranica za sve usere
2. output: 'server' - mostly SSR - page per user 
3. output: 'hybrid' - mostly SSG 
u SSR imas pristup Request, Response, cookeis, HTML streaming // zapazi
-----
// kad SSR
API endpoints - pages that return json 
Protected pages - handling user access on the server
Frequently changing content - SSR preprendered, fetch API calls and db
------------
// https://docs.astro.build/en/guides/server-side-rendering/#enable-on-demand-server-rendering
za oba server or hybrid mode, you need to add an adapter
adapter - node.js server runtime that generates html 
adapteri - node.js, cloudflare, vercel, netlify 
-------
// moraju oba da budu postavljena
import node from '@astrojs/node';
export default defineConfig({
  output: 'server',
  adapter: node({ mode: 'standalone' }),
});
-----
// Opting-in to pre-rendering in server mode
export const prerender = true;
// Opting out of pre-rendering in hybrid mode
export const prerender = false; // ssg je default
-----
Features that modify the Response headers are only available at the page level // ne u komponenti
------
// SSR features
// cookies
Astro.cookies.get("counter")
Astro.cookies.set("counter",counter)
// response
Astro.response.status = 404;
// headers
Astro.response.headers.set('Cache-Control', 'public, max-age=3600');
// response object
return new Response(...)
// request
const cookie = Astro.request.headers.get('cookie');
Astro.request.method
-------------------
// poenta, radi
// theme script mora u <head /> tag, da se izvrsi pre sajta, cela nauka, nema FOUC, white flash
// komentar u kodu ovde
https://tailwindcss.com/docs/dark-mode#supporting-system-preference-and-manual-selection
https://www.reddit.com/r/reactjs/comments/1exk4ty/with_dark_mode_stored_on_the_client_it_is/?sort=new
---------------------
// astro 4.10 env vars experimental
// https://astro.build/blog/astro-4100/#experimental-astroenv
// sam zakljucio iz koda
kadgod zaboravis da nijedan importovan fajl u astro.config.ts ne sme da importuje CONFIG 
sledi nekoliko sati teskog debugiranja
------
astro.config.ts eksportuje CONFIG, a ne sme da ga koristi, moze samo dotenv i process.env 
------
ne moze fajl sa <script is:inline /> da importuje 'astro:env/server'
ne moze ni klijent, ce da stampa log u browser
-----
// 3 nivoa config i env vars
1. process.env - one mogu svuda, i u astro.config.ts 
2. config.server.ts - ne mogu import u client scripts 
3. config.client.ts - mogu i na client i na server 
----
2. i 3. generise astro.config.ts, 1. generise dotenv 
----
import.meta.env su od Vite, ucitava iz .env.development fajla bez dotenv, nije dostupan u astro.config.ts 
----------------
// 2 kljucne greske koje sam napravio
1. I imported config file with 'astro:env/client' and 'astro:env/server' into one of the files that was again imported back in 'astro.config.ts'.
2. I had one server import 'astro:env/server' into component that has '<script is:inline />'
---------------------
// astro islands
// https://docs.astro.build/en/concepts/islands/
astro islands - partial or selective hydration
island - any interactive UI component on the page
-----
komponente su nezavisne, CSR, mogu biti u razlicitim framewrk na istoj stranici, dele state
inace zero javasript by default_
-----
client:* direktive znace astro island 
client:load, client:idle, client:visible
---------------------
// astro-remote, custom components
// https://github.com/natemoo-re/astro-remote#customization
// glavna fora je ovo
<Markdown
  sanitize={{ allowComponents: true }} // enable custom components
  components={{ Heading, a: Anchor }} // a - koji tag predefinises // glavna fora
  content={readmeContent}
/>
onda Anchor komponenta dobije samo potreban prop, href za <a/>
----
Heading kljuc pokriva h1-h6
----------------
astro Image.sizes je desktop first, max-width: 768px // zapazi
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#sizes
to je sve od <img srcset="urls" sizes="media quieries" />, a ne custom Astro, mdn
// media condition, moze i max- i min-, i svakakvi css uslovi
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_media_queries/Using_media_queries#syntax
zapravo za img max-width: ima najvise smisla // mada upitno
-------------
// moze vise od jednog [@media()], probao
-------
// ovaj bug, verovatno mora cn() za ove modifiere, zato nece dinamicke vars, nebitno
class: `border-8 [@media(max-width:${TW_SCREENS.XS}px)]:border-red-500 [@media(max-width:${TW_SCREENS.MD}px)]:border-green-500  [@media(max-width:${TW_SCREENS.XL}px)]:border-orange-500`,
-------------
// ovo je za Image debugiranje znaci
// ima problema sa specificity, mora ! za zutu
class: `border-8 border-blue-500 [@media(max-width:475px)]:!border-yellow-300 [@media(max-width:768px)]:border-orange-500  [@media(max-width:1280px)]:border-red-500`,
default border-blue-500 je za max rezoluciju
-----------
poenta: da imas samo sabijene slike, nikad razvucene
pun elitebook ekran na 1600x900 je jedva preko 1024, nadji extenziju za sirinu ekrana bez html element
zakljucak je: da ide proizvoljna slika, zavisno kolika je kolona, margine i padding
poenta je i koliku sirinu u contentu zauzima slika, ne samo sirina ekrana // to
--------
za slike sa {widths, sizes} stvarna sirina ide u css, max-w- // zapazi
----------------
// astro dynamic param route endpoint
https://docs.astro.build/en/guides/routing/
you can use a rest parameter ([...path]) in your .astro filename to match file paths of any depth:
----------------