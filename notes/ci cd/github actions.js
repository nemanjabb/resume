------------------------
------------------------
// GitHub Actions Tutorial - Basic Concepts and CI/CD Pipeline with Docker
// TechWorld with Nana 
// https://www.youtube.com/watch?v=R8_veQiYBjI

tool to automate developer workflows

github events
action - handler eventa
workflow - chain of actions
ci/cd pipeline - tipican workflow
---
to je folder
my-project/.github/workflows/ci.yml

uses:
github actions je user sa repositorijima, akcija je repo i ima verzije - release

run: je linux komanda

jobs: one virtual server per job
rade paralelno po defaultu osim ako nema needs: onda sekvencijalno

runs-on:
	matrix: [] // ako testiras na ubuntu, win, mac os

-------
release docker image umesto exe ili jar
guthub actions marketplace za gotove akcije
docker login je secret var koji se postavlja u settings tab u github
gotova akcija
uses: akcija
	with:
	proizvoljni argumenti kao funkcija

---------------------------
---------------------------
// Automatic Deployment With Github Actions
// Traversy Media
// https://www.youtube.com/watch?v=X3F3El_yvFg
// nije actions tutorijal


// .github/workflows/file.yml
on // git eventi push, merge pull_request
jobs -> steps -> run // yaml, shell komande

pm2 izvrsava node api
nginx redirect na localhost:3000
set https na digital oncuechange, install 1 python paket
---------------------------
---------------------------
// GitHub Actions Tutorial | From Zero to Hero in 90 minutes (Environments, Secrets, Runners, etc)
// CoderDave
// https://www.youtube.com/watch?v=TLB5MY9BBa4
// zbrdozdolisano

workflows -> actions
steps in yaml = actions
jobs in yaml = workflows

// .github/workflows/file.yml
uses: user/repo@v1 // zove akciju
// action.yaml
runs:
	main: dist/index.js // zove run() iz ovog fajla, akcija implementirana u js
---
edit on github
templates, featured actions with placeholders

needs: [build] // da ceka build da se zavrsi, sekvencijalno, default paralel
jobs (stages) -> steps

permisije koje akcije enabled on repository or organisation level
---
// debug
ACTION_STEP_DEBUG // debug steps
ACTION_RUNNER_DEBUG // redje, log fajlovi neki
vs code extension, nectos/act etc...
----
${{matrix.node-version}} // matrix.node-version je cvor iznad
---
// environments
ima svoje secrets i protections
---
// runners
- github hosted // placa se preko limita
- self-hosted
---
// secrets
organisation -> repository -> environment // override, fallback, env highest
secret.MY_SECRET // access in action yaml
---------------------
matrix - run job multiple time in different environments
-------------------------
-------------------------
// 2022
-------------------------
-------------------------
//  Kahan Data Solutions - playlist
// https://www.youtube.com/watch?v=Pwq7L9C9YyE&list=PLy4OcwImJzBKzWWb9K_WB3QzaxoiGmxyo&index=6
---
// Automatic Deployment With Github Actions
runs-on: ubuntu-latest // github akcija pocinje sa praznim linux containerom, ubuntu npr
---
on: // lista evenata, section kao event handler u javascriptu, event name i handler function
jobs: // handler function, run body
----
// include gotovu akciju, include, import js, C, php
// github.com/actions/checkout
name: opis // opis stavka koja se vidi u build logu posle
uses: actions/checkout@v2 
---
// Set environment variables // GitHub Actions
// def env var moze na svim levelima
levels:
1. workflow file
2. jobs
3. steps
---
// def
env:
    var_name: var_value
---
// koriscenje variable
// u run
run: echo "bla bla $my_var"
// if uslovi i druga mesta
if: ${{ env.my_var == 'nesto' }}
// ugradjene githubove, nebitno
$GITHUB_REF
----------------------
// How to set the order of your GitHub Action jobs
jobs:
    build:
    ...
    deploy:
build i deploy jobovi rade paralelno
---
// sekvencijalno
deploy:
    needs: build // kao depends_on u docker ili await u js
---
deploy:
    if: always() // radi deploy i ako je build failed
    needs: build
------------
// How to Release Code With Github
rucno new release u Github ui, na osnovu git taga
postoji github akcija za release
major.minor.patch
pre-release - alpha, beta - not production ready
------------------
// Use Outputs to Share Data Between Jobs // GitHub Actions
output - koristi rezultate iz jednog joba u drugom
sintaksa za dodeljivanje outputa i koriscenje u drugom jobu

run: | // multiline yaml sintaxa
    echo "bla bla"
---------------------
---------------------
// Building Docker containers with GitHub Actions
// https://www.youtube.com/watch?v=09lZdSpeHAk
// https://github.com/marcel-dempers/docker-development-youtube-series
runs-on: ubuntu-latest // already has docker installed i docker build radi
run: docker build -t ...
---
add secrets za docker hub nalog u Github settings na sajtu
DOCKER_USER, DOCKER_PASSWORD secrets se prebace u env vars koje mozes da ref u akciji
env:
    DOCKER_USER: ${{ secrets.DOCKER_USER }}
name: docker login
run: docker login -u $DOCKER_USER -p $DOCKER_PASSWORD
----
// push image to docker hub
run: docker push username/image-name:tag
---------------
// environments - production, staging...
// https://stackoverflow.com/questions/66113900/github-actions-not-receiving-secrets
// https://docs.github.com/en/actions/deployment/targeting-different-environments/using-environments-for-deployment
ako secrets definises u environment onda moras da ref taj env u job

jobs:
  myjobname:
    runs-on: ubuntu-latest
    environment: myenvironment  // THIS WAS MISSING
    steps:
---
inace repository secrets se vide u svim environments
------------------
// github actions are just functions with arguments, in yml
with: // su arguments
needs: je sinhronizacija, sekvenciajlno, inace svi jobs rade paralelno by default
-------
moze namerno paralelizacija testova na vise kontejnera, Cypress docs primer
akcija treba da ima parallel arg
------------------
// set UID in container
// container: prima iste argumente kao docker run
container:
    image: cypress/browsers:node16.13.0-chrome95-ff94
    options: --user 1001
-------------------
// only one dash per step
// error: step must have run or uses statement, action yaml error
// correct
steps:
  - name: Setup node
    uses: actions/setup-node@v3
    with:
        node-version: '16.03'
        cache: 'yarn'

  - name: Install dependencies
    run: yarn install --frozen-lockfile
------------
// incorrect
- name: Install dependencies
- run: yarn install --frozen-lockfile
------------------
// baze meces pod services: key
// env: moze biti za workflow ili os
// https://docs.github.com/en/actions/learn-github-actions/environment-variables
-----------------
// NODE_ENV vs APP_ENV - beleska u nodejs2021.js
---
CI-CD build job needs to set APP_VERSION var and tag
-----------------------------
// print commit id and message in GA
// https://stackoverflow.com/a/54413284/4383275
- name: Print commit id and message
  run: |
    git show -s --format='%h %s'
-----
// git log formats
// https://git-scm.com/docs/pretty-formats
------------------
// github actions
workflow (file.yml) -> job -> step (run, use)
---------------
// disable step
jobs:
  JOB_NAME:
    steps:
    - name: SOME NAME
      if: ${{ false }}
--------------------------
// run GitHub Actions locally
// https://github.com/nektos/act
----------------
// zapazi ova dva, mnogo lakse razumevanje fjla na brzinu
// u yml fajlu selektuj uses da brzo vidis koje akcije se koriste
uses: peaceiris/actions-gh-pages@v3
// i run za shell komande
run: zip -r deploy.zip . -x '*.git*'      
------------------------
// build docker arm image
// https://docs.docker.com/build/ci/github-actions/multi-platform/
steps:
- name: Checkout
  uses: actions/checkout@v4
- name: Set up QEMU
  uses: docker/setup-qemu-action@v3
- name: Set up Docker Buildx
  uses: docker/setup-buildx-action@v3
- name: Login to Docker Hub
  uses: docker/login-action@v3
  with:
    username: ${{ secrets.DOCKERHUB_USERNAME }}
    password: ${{ secrets.DOCKERHUB_TOKEN }}
- name: Build and push
  uses: docker/build-push-action@v5
  with:
    context: .
    platforms: linux/amd64,linux/arm64
    push: true
    tags: user/app:latest
-----
buildx je za build multiplatform images // eto
--------------
// kesiranje Docker slika
Use Registry-Based Caching - for cross-runner builds or multi-platform builds (amd64 and arm64).
Use Local Caching - for self-hosted runners where the same machine is used repeatedly.
Use actions/cache - for workflows that rely on GitHub-hosted runners with no external registry or local storage available.
Use Inline Caching - for simple setups where caching can be embedded in the image itself.
