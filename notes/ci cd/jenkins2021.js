// 65 jenkins Module Intro
sadrzaj i uvod

// 66 Intro to Build Automation

release process
build automation:
1. run tests
2. build app artifacts (compile)
3. push artifacts to repo (nexus)
4. deploy to server

jenkins - web app for build automation

plugins for integration with sw

// 67 Install Jenkins 

install:
1. direktno na os, rucno
2. kao docker container

1+ gb ram

firewal - whitelist ports and ips
inbound: 22 ssh, 8080 jenkins web ui

apt update // uvek pre apt install, i nova masina
apt install docker.io

docker run -p 8080:8080 -p 50000:50000 \ // \ multiline consle command
-v jenkins_home:/var/jenkins_home jenkins/jenkins:lts

-p host_port:container_port
moze da radi kao cluster na vise masina port 50000
named volume - ref na neku stazu na hostu
(:staza u kontejneru)
image name - user/image:version

init jenkins - one time pass in file in container
docker exec -it d3b566...id... bash
cat /var/jenkins_home/secrets/initialAdminPassword
// na hostu, mounted volume
docker volume inspect jenkins_home // named volume
Mountpoint: /var/lib/docker/volumes/jenkins_home/data // secrets folder
/var/lib/docker/volumes/jenkins_home/data:/var/jenkins_home // host:container

install suggested plugins
create admin user

// 68 Introduction to Jenkins UI

2 roles:
1. admin // manage jenkins
2. user // jobs

// 69 Install Build Tools in Jenkins

maven, npm...

1. jenkins plugins
2. install on server or in container

kad instaliras treba kao root user
docker exec -it -u 0 d3b566 bash // -u kao root user, docker ps za id
instalira node u container, bzvz

// 70 Jenkins Basics Demo - Freestyle Job
kliktanje po web panelu, run shell commands, tests, build

new job
build tab - maven, node... da iskompajliras svoju java, js aplikaciju

/var/jenkins_home/jobs // tu su jobs folderi
/var/jenkins_home/workspace // isto, target folder sa jar

// 71 Docker in Jenkins 

// docker cmd definisana i u kontejneru
mount docker sa hosta u kontejner

docker run -p 8080:8080 -p 50000:50000 \
-v jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $(which docker): /user/bin/docker jenkins/jenkins:lts

chmod 666 /var/run/docker.sock // u kontejneru kao root user

// build image from jar, dockerfile
from jre image
copy jar to container, set current work dir
entrypoint jar

// build step
docker bulid -t docker-username/image-name:tag . // gde je dockerfile, -t tag

// push img
login crdentials
docker login -u $USERNAME -p $PASSWORD
docker push docker-username/image-name:tag

nexus - repo za fajlove

// push to nexus
add ip to /etc/docker/daemon.json jer nije https
systemctl restart docker // restart service // kills running containers

docker build -t 167.99.248.163:8083/java-maven-app:1.1 .
echo $PASSWORD | docker login -u $USERNAME --password-stdin 167.99.248.163:8083 // pass na stdin bezbednije, server ip
docker push 167.99.248.163:8083/java-maven-app:1.1

// 72 Freestyle to Pipeline Job

freestyle - 1 step po jobu, ui
pluginovi imaju fiksna polja
pipeline jobs, scripted, pipeline as code, flexible

// 73 Intro to Pipeline Job

groovy script
1. scripted // advanced
2. declerative // predefined

pipeline { // root
	
	agent any // where to execute
	
	stages { // where is the work
		
		stage("bulid") {
			
			steps {
				'sh npm install'
			}			
		}	

		stage("test") {
			
			steps {
				
			}			
		}
		...
	}	
}
prednosti:
conditional logic, vars...

// 74 Jenkinsfile Syntax 
prosto al nabacano

pipeline { // attribute
	
	agent any 
	environment {
		NEW_VERSION = '1.1' // env var definition
	}
	tools {
		maven 'Maven' // enablira maven u stages
	}
	parameters { // input vars prakticno
		choice(name: 'VERSION', ...) // params.VERSION
		booleanParam(...) // koristi se u stages when expression
	}
	stages { 
		
		stage("init") {	
			steps {
				script {
					gv = load "script.groovy" // external groovy script
				}
			}			
		}	
		
		stage("bulid") {
			
			steps {
				'sh npm install'
				echo "building version ${NEW_VERSION}" // var u string
			}			
		}	

		stage("test") {
			when { // condition, if true run steps ispod
				expression {
					BRANCH_NAME == 'dev' // preskoci testove za feature					
				}
			}
			steps {
				
			}			
		}
		
		stage("deploy") {
			input {
				message "select env to deploy to"
				ok "env selected"
				parameters { // isto ko i gore, scoped to this stage
					choice(name: 'ENV', choices: ['dev', 'staging']) // select u ui
				}
			}
			steps {
				echo "deploy to ${ENV}"
			}			
		}	
		...
	}	
	
	post { // after all stages are done
		always {
			
		}
		success { // failure ...
			
		}
	}
}

// lista dostupnih env vars
139.59.140.177:8080/env-vars.html

credential vars...
[] // object u groovy

env vars
conditions
tools
parameters
external scripts
user input, prompt


// 75 Create complete Pipeline

// https://gitlab.com/nanuchi/java-maven-app/-/blob/jenkins-jobs/Jenkinsfile-simple-pipeline/Jenkinsfile

prevodi freestyle job u pipeline jenkinsfile i script
build jar, build docker image and push

// 76 Intro to Multibranch Pipeline 

pipeline za svaki branch
cim se kreira branch kreira i pipeline, regex filter
samo master ima deploy step

jenkinsfile u svakoj grani ili samo 1

// u svim granama
u stage dodas when conditional da ispitas granu

// 77 Jenkins Jobs Overview

rezime freestyle, pipeline, multibranch pipeline
replay - change without commiting
restart from stage - run samo 1 stage


// 78 Credentials in Jenkins

credentials scopes
1. system, jenkins server, nije available u jobs
2. global, svuda
3. credentials, only in multibranch, scoped to project

id je ref u jenkinsfile

// 79 Jenkins Shared Library
// bzvz oop code

reusable groovy library
lib u poseban git repo

folderi:
vars - functions caled in jenkinsfile, one per file
src - helper code
resources - non groovy files

// vars
filename je function name in jenkinsfile
import com.example.Docker // nasa klasa
call(String imageName){...}
#!/usr/bin/env groovy // za editor samo
$imageName

// jenkinsfile
@Library('shared-lib-name') // import lib

buildJar()
buildImage 'arg-value' // pozives fju, file, trivijal

kreira klasu sa reusable logikom, slicno ko java
Docker(script) // konstruktor

rasclani na single responsibility pa reuse, fje i klase
---
import lib in jenkinsfile ako nije glob potrebna

library identifier: 'shared-lib-name@branch', retriever: modernSCM([$class:... // sto pre u jenkins ui

// 80 Webhooks - Trigger Pipeline Jobs automatically

bez rucnog kliktanja build dugmeta

// rucno, auto, scheduled
dev i staging - auto
production - rucno
long tests - scheduled on time

conf u jenkins i gitlab
in jenkins - gitlab plugin
in gitlab - webhooks, ili integrations - jenkins ci
jenkins ip, project name, user pass


za freestyle i single branch pipeline
checkbox build triggers

// 81 Dynamically Increment Application version in Jenkins Pipeline - Part 1

version
build.gradle, pom.xml - maven, package.json

major.minor.patch-suffix
breaking changes...

mvn build-helper:parse-version versions:set \ // maven
-DnewVersion=\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.nextIncrementalVersion} \ // zadnji je next samo, \$ escape
versions:commit // replace pom.xml with new

plugin:command

1.2.2 ne resetuje na 1.2.0 // ??

def matcher = readFile('pom.xml') =~ '<version>(.+)</version> // groovy regex bzvz
def version = matcher[0][1]
env.IMAGE_NAME = "version-$BUILD_NUMBER"

// u dockeru
ENTRYPOINT
CMD //shell

mvn clean package // brisi stare fajlove

// 82 Dynamically Increment Application version in Jenkins Pipeline - Part 2

pom.xml nije komitovan u git

sh 'git status' // git komande prosto rade u shell
jenkins checks out commit a ne granu, zato mora push HEAD:branch
git push origin HEAD:jenkins-jobs

ignore plugin za prevent commit loop

gitignore target folder
git rm -r --cached target // kao delete pa add . , brise ga samo iz gita, a folder ostaje gde jeste

git pull -r // rebase, lokalni commit se kalemi na vrh
-------------------------
-------------------------
// School Of Basics | What is CI CD | What is CI CD Pipeline | Interview questions
// Automation Step by Step, indijac
// https://www.youtube.com/watch?v=k2aNsQKwyOo

CI - Continuous Integration - build, integrate work from dev team, run unit tests
CD - Continuous Delivery - dev - staging, deploy
CD - Continuous Deployment - dev - staging + production, release

build - integrate - test - deploy - release
-------------------------
// What is Continuous Integration?
// IBM Technology
// https://www.youtube.com/watch?v=1er2cjUq1UI

ci - automatizacija
- git konflikti cesto da se razresavaju dok su jos mali
- testable build
-----
// What is Continuous Delivery?
// IBM Technology
// https://www.youtube.com/watch?v=2TTU5BB-k9U

code -> build // turn code into software
continuous integration - integrate + build (unit tests) // samo to
code - build - qa - staging - prod
--------------------
// ci cd advantages, benefits
automate manual delivery
detects errors early
----------------
code - build - test - release - deploy - operate - monitor - plan // devops loop




