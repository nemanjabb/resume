// Microservices explained - the What, Why and How?
// TechWorld with Nana
// https://www.youtube.com/watch?v=rv4LlmLmVWk
// nista o arhitekturi i bazi nije rekla, devops perspektiva, a ne dev perspektiva

// monolithic
// osobine:
all components are in a single unit
one codebase in single language, runtime, deployment
all devs work is coupled
----
// lose strane:
big size, complexity, coupled
parts cant be scaled, veca cena, manja flexibilnost
single dependency version
must build and test entire app each time
---------
// microservices
// osobine
nekoliko manjih aplikacija
independent, loosely coupled - developed, deployed and scaled separately
moze svaki u drugom jeziku - glavna prednost
// komunikacija
async api endpoints
async message broker
service mash - hashicorp consul?
// lose strane
complex communication
complex monitoring
tool - kubernetes
--------
ci/cd cesto se deployuju delovi, dnevno
------
// development
1. monorepo
svi folderi u 1 git repo
single ci cd pipeline
2. polyrepo
multiple git repos
git group with secrets
---------------------------------
---------------------------------
// 10 Architecture Patterns Used In Enterprise Software Development Today
// Coding Tech
// https://www.youtube.com/watch?v=BrT3AO8bVQY
// nisu dobro objasnjeni i bezveze primeri totalno
// def
architectural pattern is a general, reusable solution to a commonly 
occurring problem in software architecture within a given context. 
----
// 1. layered pattern (n-tier architecture)
layers:
1. presentation, 2. application, 3. bussines logic, 4. data access
layer iznad koristi one ispod
mane:
performanse
primeri: desktop and web apps
----
// 2. pipe-filter pattern
processing steps - filters
chanels - pipes
primer: kompajler
----
// 3. client-server pattern
client - requestor
server - provider
prednosti: ...
mane: server bottleneck and single point of failure
primer: email, web
-----
// 4. model view controller pattern
ui - responsive na user input i na promenu app data
model - core functionality and data
view - display, more than one
controller - handles user input
primer: web frameworks, rails, django
----
// 5. event bus pattern
async messages from events
1. event source, 2. event listener, 3. channel, 4. event bus
prednost: lako se dodaju novi
primer: notifikacije
----
// 6. microservices architecture
each service independent deployable, scalable, api
poseban jezik i baza
----
// 7. broker pattern - redovi
rpc
client -> broker -> servers
broker kao ruter za service
primeri: kafka, rabbit mq
----
// 8. pear to pear pattern
svaki pear je client i server
decentralizovan, robustan, skalabilan
nema garancije kvaliteta servisa
primer: torrent, chats, bitcoin
-----
// 9. blackboard pattern
za nepoznato resenje
primeri: speach recognition, protein analysis 
-----
// 10. master-slave pattern
za dokomposed problems
primer: replicated databases
---------------------------
---------------------------
// The top 5 software architecture patterns: How to make the right choice
// https://techbeacon.com/app-dev-testing/top-5-software-architecture-patterns-how-make-right-choice
----
// 1. Layered (n-tier) architecture
abstraction, separation of concerns
primeri: OSI model networks, operating systems, MVC
-----
// 2. Event-driven architecture
cekanje na dogadjaje: user interaction, network
prednosti: scalable, extendable
mane: testiranje, 
primeri: async systems, user interfaces
----
// 3. Microkernel architecture
core set of operations
extra features - plug-ins
mane: kernel se tesko menja
primeri: OS,  fixed set of core routines and a dynamic set of rules that must be updated frequently
----
// 4. Microservices architecture
svaki task nezavisan service
----
// 5. Space-based architecture (cloud architecture)
bezveze opis skroz
baza bottleneck
primer: social networks
-------------------------------
// react, astro component hierarchy, chatGpt
// Naming conventions for component hierarchies 
1. Atomic Design:
    Atoms: Basic building blocks of UI.
    Molecules: Groups of atoms functioning together as a unit.
    Organisms: Combinations of molecules and/or atoms that form distinct sections of a UI.
    Templates: Page-level objects that place components into a layout.
    Pages: Specific instances of templates that serve as complete views.

2. Component-Based: // najlogicniji
    Components: Generic term for all UI elements.
    Partials: Partial components, often used for reusable pieces of UI.
    Widgets: Self-contained UI components that can be embedded within pages or other components.
    Views: Complete UI layouts, including components and page structure.
    Templates: Base layouts or scaffolding for views.

3. Functional:
    UI Elements: Basic building blocks of the interface.
    Components: Reusable UI elements or widgets.
    Layouts: Page or section layouts that include components.
    Pages: Complete views or screens displayed to users.
    Templates: Base structures or skeletons for pages.

4. Modular:
    Elements: Basic building blocks or smallest units.
    Components: Modular, reusable UI elements.
    Blocks: Larger sections of UI composed of multiple components.
    Layouts: Arrangement of blocks to form complete views.
    Pages: Full views or screens presented to users.

5. Semantic:
    Parts: Basic elements or smallest units.
    Sections: Collections of parts forming larger units.
    Modules: Reusable, self-contained components.
    Layouts: Structural arrangements of modules.
    Pages: Complete views or screens.
    
6. Abstract:
    Units: Basic building blocks.
    Assemblies: Collections of units forming larger structures.
    Modules: Reusable, self-contained UI components.
    Layouts: Arrangement of modules within a page.
    Pages: Full views or screens presented to users.


