

/**
* Productivity and time organization notes from Chris Diamond video lessons. Autumn 2015.
*
*
*
* References:
*	https://www.youtube.com/user/DoubleTimeToday/videos
*	http://doubletimetoday.com/blog/
*	http://www.slideshare.net/jonrwallace/negotiation-perception-cognition-emotion-13283480
*/



//greske
1. perfekcionizam, vreme nikad nije idealno, 2. odobrenje drugih, 3. nedovoljan trud, 4. multitasking, fokus, 5. neodlucnost
-----------
//tracitelji
1. odnosi (nerazumevajuci, konflikti), 2. odlaganje 3. umor
--------------
//kako se radi
1h rad, 10 min pauza, 2x, neiscrpljivanje organizma
---------------
//long term vs kratkor zadovoljst
longterm ispunjenje je mnogo isplatljiviji od shortterm zadovoljstvo
----------------
ideje dolaze kad si bez pritiska, vikend,Â  incubation phase uma, balans, oba vazna
----------------
//todo liste
todo liste, 1. pisi sve, 2. odbaci nesvrsishodne, 3. odredi prioritete, prvo uradi najvaznije, zavrsi nesavrseno - cv projekti
//bolje
not-to-do lista, identifikuj sta ti traci vreme i izbegni to
//zasto ne todo liste
todo lista - vreme da je napises, prokatirnacija pred gomilom posla
---------------
//robovi nesvesnih navika, kako se formira
aktivnost ima trashold kad postane automatizovana i nesvesna tj navika
------------
1 stvar upropasti dan, nedelju - momentum
------------
//ujutru sta ne radi
nema ujutru ili ikad mejl, social media, tv - time wasters od projekata kojite ispunjuju vise
-------------
//biologija najvaznija
spavanje, hrana, fizicko vezbanje, sta citas, drustveni zivot - emocionalni sistem
-------------
//do kraja, multitasking kontraproduktivan, nikad, gubis fokus
stvari zavrsi DO KRAJA, nesavrseno ali do kraja, ne zapicinji drugo dok ne zavrsis zapoceto

ne traci na tw nego brini o fizici i psihi, ujutru npr
------------
//navike
uspesni imaju nesvesne navike produktivne
------------
90/10 internal, eksternal distrakcije, navike
------------
vidi koliko su drugi amateri, neorganizovai, neuspesni i zapisteni, lose navike, bez plana i ambicije i volje, fizike, prokastiniraju, odustaju
------------
novac mozes da nadoknadis, vreme nenadoknadivo
---------------------
//moje
ne zanima me sta pricas, zanima me sta krijes
90/10 nesvesno, prebaci iz nesvesnog u svesno da bi mogao da kontrolises, racionalizujes
pocni odmah, zavrsi nesavrseno, nekad nastavi mozda
lista kao podsetnik da ne pamtis nekoliko dana unapred, a ne todo lista
cheatsheets
tek sad otkrio ovo, otkrivaj novo, google, sve izmisljeo, tudje greske
vidi kod ljudi sta dobro, vecina lose i zapusteno
ako treba da dokazujes, treba da odes
aktivno kontrolisi i istrazuj produktivnost, citaci za text, time managment osnove
svacji komfor i stres imaju trashold
engeniering tvog unutrasnjeg stanja, misli i raspolozenja, svesna kontrola
proso ti je trashold, nelinearni

---------------------
//goal
1. goal chasing i ostvarenje, emocionalni momenat, psiholoski mozak funkcionise, nagradjuje
2. pravac, u teskim vremenima
3. ne tracis vreme, todo list poredis sa goal
4. smanjuje stres, bez cilja stres od pogubljenosti, daje odlucnost
5. motivacija, mozak ne razlikuje fikciju i stvarnost (po raspolozenje), pravac i svrha
6. imas kontrolu u zivotu, svoju ne tudju, ti odlucis za sebe ili ce drugi (ili mediji nametnu) da odluci o tebi, kad drugi odluci nisi srecan, stres i nezadovoljstvo
7. cilj sloboda i vreme, distrakcija ljudi i situacije
----------------------
//activity vs productivity//radis ali bzvz, u mestu
aktivnost != produktivnost ako ne sluzi glavnom cilju, fokus na goal i ishod
reaction (iznudjena, urgent, ne sluzi cilju, neproduktivno), response (unapred planirano vreme u cilju ostvarenja)
produktivni responduju a ne reaguju
----------------------
//flow state
fizicki, emocionalni intelektualni nivo
rad se ne oseca kao rad (napor), uzivanje, repliciraj svesno tvoje unutrasnje stanje
talenat je kad radis sto volis
1. fokusiran odr vreme (obuzme paznju i misli)(mind wonders, luta, spreci lutanje misli) 2. uzbudjenje, strast 3. quardly? odvazan? 4. osecaj realne ostvarljivosti, rezultat vidljiv i izvesan 5. suverenity, nezavisnost od tudje potvrde (iznad), samodovoljnost, iznad ega 6. timeles, vreme leti 7. intrisic motivation, visa svrha
engeniering tvog unutrasnjeg stanja, misli i raspolozenja, svesna kontrola
-----------------------
hladne glave mnogo veca sansa da resis problem za 5 min, ne trosi sate na nesto ako ne ide
-----------------------
sve to se radi prilagodjava se stil rada mozgu, fizici i psihi, d abudu u najboljem stanju, biologija je sve
-----------------------
respond not react, ono sto je svesnim izborom planirano a ne rez emocije podsvesno
-----------------------
postavljaj kratkorocan cilj merljiv, samopouzdanje, zapisi ciljeve licne i poslovne pa se podsecaj da ne odlutas, ne odustaj, plati cenu, puno radi na tome a ne razmisljaj, strast izazov svrha, mentor ljudi, odredjivanje prioriteta, prakticni realisticni ostvarivi ciljevi, samodisciplina identifikuj - svrha fokus samodisciplina, samopouzdanje i volja veci od prepreka neminovne nadje nacin da prevazidje, snaga volje jacaj ishrana spavanje odmor, rokovi elim prokarstinac hitnost, slavljenje nagrada
-----------------------
fokus: vidi sto drugi previdjaju, misli u sadasnjosti proslost i buducnost smanjuju produktivnost, nadji zabavu u poslu, strast se nalazi u talentu, 
-----------------------
za kreativan intelektualan posao unutrasnja initrinisic motivacija strast, za mehanicki external motivation
-----------------------
planiraj i pripremaj se za uspeh ne za katastrofu
-----------------------
prioriteti da bi znao gde fokus, najvaznije prvo to, outsource nevazno da bi se fokusirao na vazno
-----------------------
poslodavac zeli selfmotivated radnika
-----------------------
1. ne zeli da napreduje, 2. moras to do da crtas, 3. izolacija samo on zna i radi nesto, 4. zasto radi, 5. drama, 6. rezultati a ne reci, 7. izgovori, odnosi se i na zene
-----------------------
pogledaj sta ima, ce da maksimalno ekspolatise to, power obuser
-----------------------
1. have fun, emocije su gorivo 2. imas izbor i slobodu, nemas pritisak 3. ocekuj probleme 4. zavrsavas taskove jedan po jedan 5. opcije 6. vera 7. selfkontrol samopouzdanje 8. uzrok a ne posledica - zrtva 9. daj bez ocekivanja, bez pritiska nad sobom

knowledge without implementation is meaningless
-----------------------
trosi novac da dobijes vreme, a ne vreme da dob novac 2. lovi veliku ribu, projekte vazne 3. prva stvar ujutru, odredi tok dana, ostane vreme na kraju
-----------------------
1. ne radi na problemima vec na resenjima i prilikama 2. radi manje, postigni vise , koja aktivn tacno ce me najefik pribliziti glavnom cilju
-----------------------
//motivacija
1. konkretni realni ovozemaljski taskovi, nista apstraktno, nerealno 2. prati i evaluiraj napredak, sta si postigo do sad 3. deli bzvz 4. ocekuj i hendluj neuspeh 5. ne zali se 6. optimizam 7. veruj u sebe, samopouzdanje 8. negativni ljudi uticu na svest 9. ortaci 10. meri rezultate, fidbek 11. deli bzvz 12. imaj misiju 13. ocekuj i hendluj negativ emocije 14. akcija bez odlaganja 15. slavi uspeh, lider moze samo primerom, ne recima 16. zapisuj beleske 17. idole i pozitiv primere i kopiraj 18. alternate tasks da manje dosadno i jednolicno 19. obavezi se 20. vizualizacija cilja, brejnstorming 21. uracunaj potrebe bioloske 22. produzeno planiraj, radi brzo 23. razlozi zasto 24. strpljenje
-----------------------
//zadrzi fokus
1. radi u cemu uzivas 2. elimin distract 3. udobna fotelja bzvz 4. podeli vreme za rad u blokove 30-60 min 5. pravi pauze, biologija 6. long term ciljevi prioritet 7. todo i notodo liste, vazno i hitno(lose) 8. proces bzvz 9. repetitiv taskovi kad nisi najspremniji, tad najv taskovi 10. ne radi nevazno na pocetku, to odredi tok celog dana
-----------------------
//job
svoj bizn mora u slob vreme, 6 meseci ustedjev
-----------------------
//zamke za biznis
1. radis novo, staro nisi zavrsio, gubis sve sto si utrosio na staro 2. za rezult treba vreme 3. previse istraz i analiziranja, ne mozes sve da predvid, gubis vreme 4. ljudi distrakcija 5. rad obavezan do rezultata, bez odustaj 6. lenjost i sumnja, ne blej 7.
-----------------------
//do more
1. slicni taskovi zajedno, fokus 2. prvo najvazn taskovi 3. ne previse detaljnih koraka u tasku, overhed 4. multitasking puno vremena nista do kraja
-----------------------
//optimizam
prokarstinacija najveci neprijatelj produktivnosti
sve sto ubija optimizam, mental stanje protiv produktiv
1. ne gledaj vesti, negativne 2. ne problemi, opsesija, vec resenja 3. nije destinacija vec put, proces 4. present, ne proslost (zaljenje i krivica) i buducnost (strah i neizvesnost) 5. pozitivni ljudi, najefikas ucenje od ljudi i njihov snalaz i primera 6. namera 7. chase dreams, dont settle for crap 
------------------------
fokusiraj na sebe, ne na druge, ono si o cemu razmisljas
------------------------
//deadlines
deadlines su mindtool, prokarstinac jer ne znas sta sledece, emocije u indirektnoj kontroli ne direktnoj, urgency osecaj pozitivan, sta ces da izgubis ako ne uradis, 
------------------------
//freedom, nezavisnost//odlican
sta te zadrzava
1. fizicka izgled 2. vreme kako hoces 3. sexual (emoc snaga kroz odbij) 4. finansijska 5. duhovna, svrha (lutanje bez svrhe) 6. politicka, vize, porez (novcem iskrivis zakone, zakoni pisani u korist bogatih)
fizicka, mentalna i emocionalna snaga za sve
ako vec imas znanje treba ti akcija (vise) to
snaga - fizicka vezba i znanje, biti u formi
uciti iz gresaka ili se ponavljaju
koje slobode ti fale
-------------------------
//zauzet a bez rezultata
1. internal: unutrasnji dijalog, pretpostavke, apsorbuje u svet i drzi zauzetim
2. proveravanje notifikacija email, tw, 3. los time managm, merenje, organizacija 4. prioriteti, prati progres, koja koji cilj ostvar, fokus moguc samo na jednu stvar 5. ne prepoznav prilika, ne shvatanje long term dobiti 6. fokus i koncentracija (vise razlicitih u toku perioda, menjanje dosade)
-------------------------
//distractions
objasni im, ne objasnjavaj sta
-------------------------
//moje
ulazi u svoj optimizam, raspolozenje i emot-mental kondiciju
lako potceniti ono sto imas
znanje imas treba akcija
citaci teksta za efikasnost, cheatshetovi, istrazuj google neuobicajeno, vec izmisljeno
identifikuj najvaznije, konkret rezultate i meri, navika svaki dan u isto vreme
-------------------------
//increase work results
identifikuj najvaznije
1. cisto (i desktop uredan, minimalan) 2. sam 3. jedan po jedan task 4. posetioci 5. notifikacije iskljuci 6. telefon 
svaki dan u isto vreme radi najvaznije, kad si najkoncentrisaniji, navika, mozak uvek radi u rutini, najefikasniji period ne trosi na tviter
-------------------------
//trap for prodictivity
racunar alat za rad, iskljuci notifikacije i ostalo
2 sata dnevno ode na neprodukt, mozak je single thread, lancani efekat sinergija sa nezadovolj i emotional frustrated
-------------------------
//time managment sta i zasto
vreme prolazi
vrednost samo od zavrsenih taskova, 1 sat pa pauza nema iscrpljivanja bioloski, i odmoran i uradjeno, 
nezavrsen task je trosak i vreme (vreme je nepovratno, najdragocenije) 
---------------------------
izguglaj plugin za pola sata mesto da radis 2 dana i napravis losije
sve vec izmisljeno
-----------------------------
-----------------------------

//http://www.slideshare.net/jonrwallace/negotiation-perception-cognition-emotion-13283480
//uvek nauka nikad kvazi nauka, prva ruka
//perceptive distortion
hallo effect - generalizacija -  If the observer likes one aspect of something, they will have a positive predisposition toward everything about it.
Selective perception is the tendency not to notice and more quickly forget stimuli that cause emotional discomfort and contradict our prior beliefs. sto se babi htilo to joj se i snilo
//cognitive bias
zakon malih brojeva - donosenje zakljucka na osnovu premalog uzorka, statistika
endowment effect - (zaduzbina) covek hvali ono sto vec ima, odlican
Winners curse - kletva, realna cena je prosek ponuda
Reactive devaluation - zakljucak ko kaze, a ne sta se kaze
framing - suzavanje, krivljenje
Hindsight bias - zabluda da si mogao da predvidis proslost
bias - predrasuda
fleka se ne brise, samo se jos vise razmazuje, tviter 1 greska blam, na duge staze nije drukciji od realnosti, previd
aktivno se bori da ne upadnes u naviku i monotoniju, ako se ne boris aktvno vec si u monotoniji
sve su to ljudski kognitivni biasi koje treba da prepoznas i hendlujes
---------------
in the middle of dificulty lies oportunity - tviter nema search po fejvovima, kad bi imao nista ne bi vredelo jer dostupno svima
identifikuj sta je vrednost i kreiraj, ne gubi vreme na ono sto nije vrednost
vrednost - izgled, zivotn stil (karijera, uspeh)
zapravo kad radis je zanimljivo
izvadi zakljuc kad je slobod pa radi
------------
vreme proleti kad radis
------------------
na primeru se boje i brze nauci nego iz objasnjenja, bolja izolacija problema, i zakljucivanje
----------------------
POWER OF NOW BY ECKHART TOLLE 
da te ne apsorbuju proslost i buducnost da gubis sadasnjost, samo sadasnjost zdrava bez pritiska, nemas sta da izgubis, zadrzi sadasnjost, opterecen povisicom 2 dolara i ne zivi u sadasnjosti
sreca je put (proces, napredovanja), ni start ni cilj ni ispunjenje jedne stvari
-------------------
HOW TO STOP WORRYING AND START LIVING BY DALE CARNEGIE
uzivaj u sadasnjem trenutku
1. sta je najgore sto moze da se desi (uglavnom trivijalno, 4 u osnovnoj)
mozak neizvesnost uvek pretpostavlja pesimisticki, briga
glpost pokvari raspolozenje za ceo dan
2. apsurdnost brige, u podmornici jos 2 sata seca se da se nervirao za sitnice, ljudima seku glave, da je na tvom mestu on bi bio najsrecniji
3. budi zauzet, apsorbovan idejom i sadasnjoscu i zivotom, mozak je singlethreaded, cim si zaludan mozak luta i nadje negativnost iz proslosti ili buducnosti, uhvati kad pocne i spreci svesno desava se nekoliko puta dnevno, cim se desi znaci da si zaludan
4. da li bi prodao noge za milijardu dolara, zaradices milion

poenta, uzivaj u sadasnjosti, 90% stresa je iracionalno i nepotrebno, eliminisi i racionalisi nepotreban stres, stres los
----------------
radi na prosirenju ogranicenosti, svako je ima u nekom iznosu, bugarska muzika
----------------
THE MILLIONAIRE FASTLANE BY MJ DEMARCO
3 trake: 1. stranputica (nema finansijsku inteligenciju) 2. spora - posao
3. brza - preduzetnik
ne gledaj filmove i serije nego uci, radi na sebi
koliko prave vrednosti mozes da das vecem broju ljudi toliko zaradjujes, preduzetnik
startup moze da raste 100%, plata ne moze
--------
THE 4-HOUR WORKWEEK BY TIM FERRISS 
80/20 eliminacija poslova koji nisu profitabilni a uzimaju isto vreme kao uspesni, samo 1 stvar dovoljno da uspe
eliminacija i outsourceing
vecina nije u kontroli reakcije na negativan stimulans
----------------
terba da imas rezime poglavlja sa lekcijama, kao index u bazi
posto su video klipovi sekvencijalni
summary klipovi na pluralsight
----------------------
// obsidian md knowledge base
// Use Obsidian (BEST Markdown editor) for note taking and tech docs! - Christian Lempa
// https://www.youtube.com/watch?v=cBzc5r-FNW0
-----------





