ddd - uml class diagrams, OOP 
Domain-Driven Design - Eric Evans, knjiga
----------------
// DDD Explained in 9 MINUTES | What is Domain Driven Design? - Marco Lenzo
// https://www.youtube.com/watch?v=kbGYy49fCz4
// phases:
1. Tactical - low level details
2. Strategic - big picture
-----
// tactical:
Entities - has id
Value Objects - no id, weak entity in sql
Associations - n:m, 1:n, 1:1, direction
Services - statless operations
Aggregates and Factories - grupe entities
Repositories - stores for querying entity instances
-----
// Strategic Design (microservices):
// boundaires - decompose into submodels
Bounded Context  - submodul sa svojim jezikom
Context Map - points of contact between models
Relationships between Contexts 
    customer / supplier 
    anti corruption layer 
    shared kernel 
----------------------
----------------------
// All architecture methodologies:
Layered Architecture (N-tier)
Microservices Architecture
Event-Driven Architecture
Service-Oriented Architecture (SOA)
Hexagonal Architecture (Ports and Adapters)
Clean Architecture
Onion Architecture
Monolithic Architecture
Serverless Architecture
Component-Based Architecture
Pipe and Filter Architecture
CQRS (Command Query Responsibility Segregation)
Client-Server Architecture
Peer-to-Peer Architecture
Space-Based Architecture

