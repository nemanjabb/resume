/**
* Lisp notes from university course .pdf material. Autumn 2015.
*
*
*
*/

name statements; nego sve jedan poziv fje
Nema sekvencijalnog niza naredbi, već se rezultat postiže ugnježdavanjem i komponovanjem poziva funkcija
atom (konstanta) i lista (podatak ili fja)
; je komentar
prefix notacija (ime_fje args...)
prvi elem liste je fja, osim '(podatak1, pod2...)
(imeFje arg1 arg2 ... argn)//prefix notacija zapazi

'(+ 9 3) => (+ 9 3)
(+ 9 3) => 12

predikat bool fja, imeFjeP, true=(t), false=('()) prazna lista
car = first()
cdr = lista sem prvog el
cons = push()
list = lista od atoma
append, length, nyh, last, reverse//fje sa poljima
atom arg = is_atom()
listp arg = is_list()
null arg = is_null() tj '(), '() je null, prazna lista

Argumenti su navedeni kao lista i prenose se po vrednosti//imutable
citas iznutra iz najugnezdjenije
bitno da zapazis da je if(=n0), a ne if(n=0) za sve fje operatore

assoc lista ((A 1) (B 3) (C 10))
(assoc 'B ((A 1) (B 3) (C 10))) => (B 3)

//global prom
(setq lista1 '(1 2 3 4 5)) => lista// setq dodeli listu u prom lista1
(lista) => (1 2 3 4 5)

//lokal prom//scope izraz
(let ((id val)...) exp)
(let ((x 3)) (+ x 3)) => 6 //dodeli x=3; pa x+3=6//x je lokalna
let paralelno dodelj, let* sekvenc...

eval podatke kao izraz
(eval '(+ 2 3)) => 5

(apply fun list)//invokuje fun(list)

do
(prog1 func1 func2 ... funcn)// func1(), sekv poziv naredbi

(mapImeFje func list) //foreach ImeFje() nad elem liste
(mapfun(lambda (var) func...) list)//map blok naredbi - vise fja nad elem liste



