prouci restful routes
/api/users/:id/accounts - slab tip, prouci, pitaj chatGpt
---------------
napravi kanonicki sve request, response tipove i naming, validacija, error messages
gledas u kod controller, service, unapred znas ulaz, izlaz argumente i tipove, sta tu moze da bude
------------
koji su rest adekvatni responses, user, account, {success: true}, message, boolean, void
public, private je isto za naming, struktuiran
kljucne reci za naming, i redosled, imenice, glagoli
-------------
translate exceptiona iz service layera u controllerima
-------------
sabloni za error messages
-------------
deljenje tipova react query, controlleri, servisi, trpc
transformisanje jsona
-------------
projektovanje zasticena i private polja, user.password u response
-------------
data transforemers - api responses, pogledaj na youtube
-------------
translate errors, service, controller layers
------------------
// pitaj ovo node.js reddit
// mutation hooks
useUpdateBusinessAssignedPhoneNumber
use-verb-model-field
// naming za types
BusinessUpdateAssignedPhoneNumberMutationData
model-verb-field-mutation-arg
---------------------
type_ responses with zod or trpc, pogledaj
------------------
// get user by email
'/api/users/profile/:email' // get users email
'/api/users?email=john@email.com' // get user by email, filter
-----------------
idempotence is a desirable property for all HTTP methods
GET, PUT, DELETE... vec jesu
POST - is inherently non-idempotent because it typically creates a new resource with each request // to
repeated POST requests should create just one object // uglavnom
repeated requests do not lead to unintended duplication of resources
-------
HATEOAS - map routes to objects (nested) // otprilike
-----------------
// Prefer to group webhooks into its own folder. Then separate by vendor. Then by action.
POST /api/webhooks/slack/notification
POST /api/webhooks/mailchimp/contact
------------------
// use curl to test api endpoints, zgodnije nego gui
// POST with file on path, prosto
// -X za method, GET podrazumevan
// -F '...' za file
curl -X POST \
    -F "file=@/home/username/Desktop/test-file.txt" \
    http://localhost:3000/users
------
// -H '...' za header
-H "Content-Type: multipart/form-data" \
-------
// -i - include headers response
// -v - verbose output response
-i -v \
-------
// GET, skroz prosto, sve default
curl http://example.com/api/data
// response u file
curl http://example.com/api/data -o response.txt
-----
// curl set origin header for cors middleware 
-H "Origin: http://example.com"
--------------------
--------------------
HATEOAS (Hypermedia as the Engine of Application State) - suggested linkovi u response
REST APIs should provide hypermedia links to guide the client on how to interact with the API, 
making it self-explanatory and discoverable.
// Example: 
// In a user response, provide links for related actions
{
  "id": 123,
  "name": "John Doe",
  "links": {
    "self": "/users/123",
    "posts": "/users/123/posts"
  }
}