// Top 7 Ways to 10x Your API Performance - ByteByteGo
// https://www.youtube.com/watch?v=zvWKqUiovAM
// samo ih nabrojao ukratko
1. Caching (db, prost)
2. Connection pool (serverless)
3. Avoid N+1 Query Problem (1 post, n comments)
4. Pagination
5. JSON Serializers
6. Payload Compression
7. Asynchronous logging (prvo u memory, novi thread za fajl)



