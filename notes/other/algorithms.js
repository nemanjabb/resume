// JavaScript Algorithms - Codevolution
// https://www.youtube.com/playlist?list=PLC3y8-rFHvwjPxNAKvZpdnsr41E0fCMMP
// 3 Time and space complexity 
multiple ways to solve one problem
mera efikasnosti resenja, algoritma
kako skalira u odnosu na velicinu ulaznog seta
----
time complexity - f(input)
space complexity - f(input) - memorory size
-----
asymptotic notation:
big O notation - worst case complexity - najvaznija
omega notation - best case complexity
theta notation - average case complexity
--------------
// 4 - Big-O Notation - worst case
big O, najvaznije:
1. fja inputa
2. ignores details - samo deo koji najbrze raste
---
// time complexity, uglavnom
ne vreme nego koliko puta se izvrsava statement
O(n) - linear - jedna petlja
O(1) - constant - jedna statement
O(n^2) - quadratic - 2 nested petlje
O(log n) - logarithmic - ako je ulazni set 2x manji u svakoj iteraciji - binary serach
------
// space complexity
O(1) - constant - rast memorije ne zavisi od ulaznog seta - sorting bez dodatnih struktura
O(n) - linear 
O(log n) - logarithmic - sporije raste od linearnog, more performant, za log10, log(10) = 1, log(1000) = 3
O(n^2) - quadratic je lose za space, za time moze da prodje
-----
// https://www.bigocheatsheet.com - graf
// https://en.wikipedia.org/wiki/Big_O_notation
O(1) - x osa
O(log n) - skoro ko O(1) - za ceo set nekoliko puta
O(n) - 15 stepeni prava - svaki el jednom
O(n log n) - skoro 45 stepeni - svaki el nekoliko puta
O(n^2) - parabola - svaki el n puta
O(2^n) - exponential, skoro y osa
O(n!) - factorial, y osa
-----
// vrlo retka, samo za skip list, nebitna skoro
O(n log n) = O(log n!) - linearithmic, loglinear, quasilinear (sumiran red za log n!)
----------------------
log u binary je ln za e, jer deli sa 2
log(10) = 1, iznad vece od 1, obicno vise od 10 elemenata
ln(2.71) = 1, iznad vece od 1
---------
// natural logarithm ln(x)
e = lim n->inf (1 + 1/n)**n
ln(x) - related to complex numbers and exponential functions
----------------------
// 5 - Objects and Arrays Big-O - time complexity
// zavisi i od operacije, ne samo od strukture
// object - hashtable, dictionary - u cheat sheet tabeli
O(1) - insert, remove, access
O(n) - Object.keys(), .values(), .entries() - mora celu listu da obidje
-----
// sortirano polje
O(1) - push(), pop(), insert, remove last el
O(n) - insert remove at beginning - mora da update indexe za svaki jer sortiran
O(1) - access
O(n) - search el, obilazak
O(n) - shift, unshift, concat, slice, splice
O(n) - forEach, map, filter, reduce - obilazak
--------------------
// B-tree
sortirani su u istom nivou, database index
--------
// heap
kompletno binarno stablo, samo zadnji nivo nepotpun, ali sleva na desno
top-down je sorted, moze biti min-heap i max-heap
---------------
kucaj u google "data strucures animated" da bi ih razumeo
https://visualgo.net/en
-------------------------
-------------------------
// BFS, DFS, Dijkstra, A star
// complexity
// https://www.baeldung.com/cs/graph-algorithms-bfs-dijkstra
BFS - O(V + E)
Dijkstra - prior queue O(log N), ukupno O(V + E(log V))
------
BFS nalazi shortest path jer expands gradually, paths are sorted, flood fill
DFS ne nalazi shortest path
---
dijkstra i A* su za weighted graphs
euclidian - kad moze dijagonalno
manhattan - kad moze samo horizontalno i vertikalno