
/**
* Regular expressions working notes.
*
*
*
*/


pocetna slova reci

(?:^|\s|\.)([a-z])

pattern: "[A-Za-z0-9,\.'-\\s]+" //za \\s treba dupli escape za ostali 1 npr \.

(?=.*\d)

?= positive lookahead, da grupa sadrzi

. bilo sta
* bilo koliko
\d iza cega ide cifra

<title> imenovana grupa, asoc polje
[^>] ne i taj

*? non greeedy

(.*?)"flights" sg split string po flights// eto ti za rec [^x]*
s utice na tacku da matchuje newline

multiline samo kad hoces da koristis pocetak i kraj ^ $ 

(?:.*?)standardTaxes do reci
(?:.*?standardTaxes) ako neces rec
-------------

ugnjezden json:

"iata"\s:\s"(\w{3})".*?"routes"\s:\s\[(?:[^\]]*?"iata"\s:\s"(\w{3})")*[^\]]*?\]

2 nezavisna upita, cist da capturing bude u istoj celiji, da znas koji je od koga

balanced:   (?:(?<Content>[^{}]*)|(?<Brace>\{)|(?<-Brace>\}))*

-----------------
9 3

code":\s*"(\w{3})"(?:[^\[]*?)\[(?:[^\]]*?"(\w{3})"[^\]]*?)*  //mora * na kraju


Regex allRegex = new Regex(@"code"":\s*""(\w{3})""(?:[^\[]*?)\[(?:[^\]]*?""(\w{3})""[^\]]*?)*", RegexOptions.Compiled);
var matches1 = allRegex.Matches(response);

var matches2 = matches1.Cast<Match>().Select(m => new {
		From = m.Groups[1].Value,
		To = m.Groups[2].Captures.Cast<Capture>().Select(c => c.Value).OrderBy(c => c)
	}).OrderBy(m => m.From);

po Select vidis koji tip se vraca
----------------------
tacno uhvati rec2 izmedju rec1 i rec3
(?<=prefix)mech pattern(?=sufix)")

(?<=sufix) Positive Lookbehind
(?=sufix) Positive Lookahead

ne prelazi taj deo
----------------------------
//bool na brzinu
//egzaktno kraj reci
Regex.Match( "GFDGFFkjh", @"\b[A-Z]{6}\b" ).Success 
false
Regex.Match( "GFDGFF", @"\b[A-Z]{6}\b" ).Success 
true
\b assert position at a word boundary smena (^\w|\w$|\W\w|\w\W)
caps i noncaps su suprotni w - word, W - nonword, d - digit, D - nondigit
----------------------------
//i staticki i instancirani vracaju isti tip MatchCollection 
//isto se iteriraju
MatchCollection matches = Regex.Matches(inputText, @"regex ovde");//staticki

Regex regObj= new Regex(@"regex ovde", RegexOptions.Compiled);//instanc
MatchCollection matches = regObj(response);

foreach (Match m in matches){string route = m.Groups[1].Value; m.Success...}
ili linq
results = matches.Cast<Match>().Where(route => Regex.Match(route.Groups[1].Value, @"\b[A-Z]{6}\b").Success)).Select(r=>r.Groups[1].Value).ToList().Select(r => OrderRouteName(r)).ToList();

ako se isti regex koristi vise puta onda instancirani jer kesira...

wildcard * je zapravo (?:.*)
------------------------
// lines that start with numbers in vs code
// match lines that start with numbers
\d+\..*
// lines that dont start with numbers, negation
^(?:(?!\d+\..*).)+$
------
// match empty lines
^\s*$
-------
// remove empty lines u vs code, zapravo ovo, treba karaktere da zamenim, a ne selekciju, odlican
\n+ // find
\n    // replace
-----
// 2. nacin
^(\s)*$\n  // find
prazno     // replace
----------------------
// regex test match in js, boolean
// https://stackoverflow.com/questions/6603015/check-whether-a-string-matches-a-regex-in-js
izostavi capturing groups, nisu za test, samo za capture (match) // to
const isMatch = regex.test(stringToCheck)
---------------------
// greedy vs non-greedy regex, ok
// Greedy: 
a+ would match all a characters in "aaa".
// Non-Greedy: 
a+? would match only the first a character in "aaa".
------------------
// zasto | ne radi kao multiple replace() calls
const stringData1 = initialString.replace(/[{}\t ]|n+,/gm, '');
const stringData2 = stringData1.replace(/\s+,/gm, ',');
zato sto rezultat posle replace ima opet matcheve 
replace() fja ne zna koji je rezultat, i cime si ti menjao matcheve 




