// Dependency Injection 
design pattern
passing the dependencies that a class_ needs from the outside (arguments)
rather than creating them within the class_ itself (constructor calls with new)
---
loose coupling 
---
// 3 types:
Constructor Injection - najcesca
Setter Injection - nakon kreiranja
Interface Injection (Method Injection) - u methods defined u interface_
----
// benefits:
Decoupling
Testability
Flexibility
Reusability
Readability and Maintainability
-----
// scopes or lifecycles - koliko cesto se poziva new
Singleton Scope - 1 instanca za ceo app
Transient (Prototype) Scope - svaki put nova instanca, new Constructor()
Request Scope - web app, http request
Session Scope - sesija usera
Custom Scopes
// svodi se na state na backendu // dobar
singleton scope - stateless services, without worrying about concurrent modifications
transient scope - stateful components, separate states in parts of the application
request or session scopes - web applications, state in user interactions
----------
// .NET libs
ASP.NET Core DI - simple
Autofac - feature rich
---
interception - adding behavior before or after a method is executed



