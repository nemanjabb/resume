// submodules
.gitmodules fajl u root gde su izlistani

// Add a new submodule // ovako clone submodul, cd nested-dir ako treba ili <path /> u komandi
git submodule add <repository_url /> <path />

// Clone a repo with submodules // ok
// onda ne treba git submodule init          
git clone --recurse-submodules <repository_url />

submodule - nested repo // ok

// Initialize and update submodules in an existing clone
git pull // u root, povlaci commit hash u .gitmodules fajl 
// Initialize submodules // ovo je nesto spec, kao activate 
// cita .gitmodules i postavlja .git/config za nested repo
// samo jednom posle clone
git submodule init          
git submodule update        // ovo radi pull u submodule
git submodule update --init --recursive // izjedno init i update

// pull all submodules to THEIR latest commit
git submodule update --remote
// to commit specified in root repo // zapazi
git submodule update
// pull only specified submodule to its latest // taj
git submodule update --remote -- path/to/submodule
// if pulled latest move to commit specified in root
git submodule update -- <submodule_path />

// pull se radi zapravo iz submodula i parent repo updateuje commit id // TO
// a ne iz root
git pull

// Check the status of submodules // ovo cesto
git submodule status

// Synchronize submodule URLs (after updating .gitmodules)
git submodule sync
-----------
root repo cuva samo .gitmodules fajl i folder od nested repo
ne vidi njegove fajlove, nested repo radi samostalno kao i obicno 
git status 
------
submodule je link na COMMIT u drugom repozitorijumu, a ne samo na repo 
-----
// za workspace:
folderi moraju da budu ukljuceni u workspace root package.json i npm install // i to je to, kao lokalni folderi
------------
// pull all, root and submodules
git pull                           // Pull latest changes in the root repository
git submodule update --remote      // Pull latest changes for all submodules
git add .                          // Stage submodule updates
git commit -m "Updated root and submodules to latest"  // Commit changes
-----
// na main grani nikad ne radis, jer mora commit // to, verovatno
git submodule update --remote
------------------
// posle git pull u root repo ide ovo za submodul, da pomeri njegov commit
git submodule update packages/my-package

