// Conventional Commits Format
// <type>[optional scope]: <description>
// [optional body]
// [optional footer(s)]

// Common Commit Types
feat:     Add a new feature
fix:      Fix a bug
chore:    Maintenance tasks
docs:     Documentation changes
style:    Code style changes (no logic)
refactor: Code refactoring (no features/bugs)
test:     Add or fix tests
perf:     Improve performance
BREAKING CHANGE: Major breaking changes

// Example Commit Messages
feat: add user login
- Added OAuth2.0 login support
- Updated API documentation

BREAKING CHANGE: database schema updated

// Multiline Commit Message (Method 1: Open Editor)
git commit

// Multiline Commit Message (Method 2: Use Multiple -m Flags)
git commit -m "feat: add user login" \
-m "- Added OAuth2.0 login support" \
-m "- Updated API documentation"

// U VS Code direktno
Shift+Enter // za novi red i pisi sta treba

// Abort commit
Nano: Pritisnite Ctrl+X, a zatim N // izlaz bez cuvanja
// ako je commit proces pokrenut ... ?
git commit --abort
// undo local commit
git reset --soft HEAD~

// postavljanje editora za commit poruke - VS Code npr
git config --global core.editor "code --wait"

// -------------------------
// Commitizen - postavlja ti pitanja za pravilno formatiranje commit poruke
npx cz ili git cz ili npm run commit

// 1. Install Commitizen
// Global installation (optional)
npm install -g commitizen

// Project-specific installation
npm install --save-dev commitizen

// 2. Configure Commitizen with Conventional Commits
npx commitizen init cz-conventional-changelog --save-dev --save-exact

// 3. Use Commitizen to Make a Commit
npx cz
// OR if Commitizen is globally installed
git cz

// 4. Answer the Prompts
// - Type: What type of change? (e.g., feat, fix, chore, etc.)
// - Scope: What is the scope? (optional, e.g., auth, api, etc.)
// - Short Description: Write a brief summary of the change.
// - Long Description: Provide detailed explanation (optional).
// - Breaking Changes: Mention breaking changes (if any).
// - Issues: Reference related issues (e.g., #123).

// Example Resulting Commit Message:
// feat(auth): add OAuth2 login
//
// Implemented OAuth2 login flow for secure authentication
//
// Closes #42

// 5. Optional: Add a Script in package.json for Ease of Use
// Inside "scripts":
// "commit": "cz"
npm run commit

// 6. Benefits of Commitizen
// - Ensures commits follow the Conventional Commits format.
// - Provides an interactive CLI to guide you through writing clear and consistent commit messages.
// - Works seamlessly with tools like standard-version and semantic-release.
