// github search operators
https://www.freecodecamp.org/news/github-search-tips/
-------------
// commitizen - template za sve git poruke // koristi ovo od sad
https://github.com/commitizen/cz-cli
// odavde primer
https://github.com/satnaing/astro-paper
-----
// jos jedan
https://github.com/conventional-changelog/commitlint
----------------
// print last commit
git show -s --format='%h %s'
aaea356 update todo
-----------------
// conventional commit messages // istrazi
// primer:
https://github.com/paularmstrong/paularmstrong.dev/commits/main/
prefixi:
chore:, blog:, fix:, feat:, ci:, refactor: ... 
---------------------
// brzi git log
// ovi su korisni zapravo
// -3 tri zadnja commita, jedna linija, br linija // najbolji, zapamti ovaj
git log --shortstat --oneline -3
// br linija po fajlovima
git log --stat --oneline -3
--------------------
// stat moze i za zadnji commit, odlicno
git diff --stat HEAD~
-----------------------
// remove all unstaged changes // eto ga, cesto treba // ni jedna ne cisti untracked files kako treba
git reset --hard HEAD // incorrect, vraca na prethodni commit
git restore --staged .
git checkout -- .  // ovaj ok, ali ne cisti untracked
git clean -fd   // ovaj cisti untracked kako treba zapravo
git stash
git stash drop
-----------------------
https://www.conventionalcommits.org/en/v1.0.0/
apps and packages in monorepo 
-------------------
// count number of tracked files in git (without .gitignored)
git ls-files --exclude-standard | wc -l
----
// lista fajlove
git ls-files --exclude-standard
// broji linije
wc -l
----------
| je pipe command output, > i >> je redirect u fajl 
-------------------------
// broj promenjenih linija zadnjih 5 komitova, .. je range, odlicno
git diff --stat HEAD~5..HEAD
git diff --shortstat HEAD~5..HEAD // samo broj linija, gornje bolje
-----
// print commit info, git log obican, -n 1 je log za 1 commit
// jedna linija
git log -n 1 --oneline HEAD~3 
70e0536 extract PostList
-------
// nije preobimno
git log -n 1 HEAD~3 
-------
HEAD~0 // zadnji, head
HEAD~0 === HEAD
HEAD~1 // predzadnji, zapazi
HEAD~ // isto predzadnji
HEAD~~ === HEAD~2
------------------
revert obrce JEDAN commit, zapazi, a ne sve do njega
git revert HEAD~1 // obrne samo predzadnji, a ne i zadnji i njega // zapazi
------------------
// git cherrypick
git fetch
git cherrypick 1bdbb0a39f635
--------------------
// eto kako backmerge bez prelaska na granu
merge sa remote branch origin/main npr 
git merge origin/main
----------------------
// reconcile divergent branches
// git pull ...
poenta: desava se kad radis pull na istu granu, a i lokalno i remote ima novih komitova
-----
You have divergent branches and need to specify how to reconcile them.
Need to specify how to reconcile divergent branches
// default sa merge, resenje
git pull origin refactor-t-rpc --no-rebase
-----
// set as global
git config --global pull.rebase false
----
// listanje
git config --list
git config --list | grep pull.rebase
-------------------
// list all non staged images in working directory
// da izbegnes da dodas slike u git
git ls-files --others --modified --exclude-standard -- '*.jpg'
-----------------
// count number of lines in all files in current folder
find . -type f -exec cat {} + | wc -l
--------------------
// see email in git terminal
git log -1 --pretty=full
----------------------
// rename repository on github
// remove and readd remotes
// remove remotes
git remote remove origin
git remote remove gitlab
git remote remove all
// add new remotes
git remote add origin git@github.com:nemanjam/new-repo.git  && \
git remote add gitlab git@gitlab.com:nemanjam/new-repo.git  && \
git remote add all git@github.com:nemanjam/new-repo.git  && \
git remote set-url --add --push all git@github.com:nemanjam/new-repo.git && \
git remote set-url --add --push all git@gitlab.com:nemanjam/new-repo.git && \
git remote -v
// set new upstream
git push -u origin main
-------------------------
// rebase je sekvenca komitova
// ne ides merge nego ovo:
git rebase --continue
------------------
// git bisect // odlicno
je binary search koji pokrece neku skriptu po komitovima (range) i locira u kom komitu se prvi put javlja
----------------
// checkout .
git checkout -- .
-----------------
// rename current local unpublished branch 
git branch -m feat/my-new-branch 