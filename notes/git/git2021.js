// git
---------------
u conflictu HEAD je current change
jer je HEAD pointer na current branch
------
tracking branch za push/pull bez imena grane
---------------
// revert to commit that is merge commit
that is a merge commit that has 2 parents, you will have to specify which parent you want to use

git revert -m 1 0f96089dfe34e79b415159f5a93c9b075c62 
or
git revert -m 2 0f96089dfe34e79b415159f5a93c9b075c62 

looking here you can see the parents

In general cases -m 1 is the one you should use.

------
this is also an option if there are too many
git merge --strategy-option theirs origin/develop
------------
kod u gitu je kvantizovan na liniju
------------
// unstage
use "git restore --staged <file>..." to unstage
----------------------
// rollback
point pipeline to specific commit or tag on master branch
------------------
git push origin grana-local or remote???
click github mr da vidis sta je conflict preliminarno na githubu
mozes da pull sa druge grane, ali ne moze push na drugu granu, radio bi merge na serveru, proveri?
ni push na istu granu ne moze ako local grana nije backmerged sa remote

-------------------
// listanje grana
git branch -a // sve
git branch -r // remote
git branch // local
---------------------
// log istorija fajla
git log --follow --oneline -- filename // -- nije spojeno zapazi
// npr
git log --follow --oneline -- notes\devops\docker.js
// outputs:
// 78a68aa Initial commit.
---------------
// log with date
// https://stackoverflow.com/questions/1441010/the-shortest-possible-output-from-git-log-containing-author-and-date
git log --pretty=format:"%h%x09%an%x09%ad%x09%s" --date=short
-------------
// log for file with date and author
git log --follow --pretty=format:"%h%x09%an%x09%ad%x09%s" --date=short -- notes\devops\docker.js
// outputs:
// 78a68aa Nemanja Mitic   2016-06-14      Initial commit.
-----------------------
// set ssh github
ssh-keygen -t rsa -b 4096 -C "nemanja.mitic.elfak@hotmail.com"
/home/username/.ssh/id_rsa.pub // ovde ga cuva
cat ~/.ssh/id_rsa.pub
git remote set-url origin git@github.com:nemanjam/nextjs-prisma-boilerplate.git
--------------------------
// log with total number of lines, odlican
git log --oneline  --shortstat
// npr
fb67516 add username to schema
9 files changed, 127 insertions(+), 28 deletions(-)
------
// full, po fajlovima, isto ko diff
git log  --stat
// npr
7f8c4da profile in progress
 .gitignore                      |  1 +
 components/Header.tsx           | 12 +++++++++++-
 pages/api/user/create.ts        |  1 +
 ---------------------
// remove file from tracking
// https://stackoverflow.com/questions/1274057/how-can-i-make-git-forget-about-a-file-that-was-tracked-but-is-now-in-gitign
// add u gitignore ne radi nista - sprecava pre nego sto je komitovan
git rm --cached core/traefik-data/acme.json // pa add . i commit i push
// ne brise fajl lokalno, brise ga iz gita
// mora i on tamo isto pre pull da mu ne izbrise lokalno fajl
git rm --cached core/traefik-data/acme.json
// plus dodas u .gitignore
--------------------
fork je samo drugi remote
// https://gitexplorer.com/
----------------
git push --force // overwrite anyones commits
git push --force-with-lease // overwrite only your commits
------------------
// na Githubu imas history za svaki fajl, zapazi
// korisno da nadjes sta je broke something
// ne samo za ceo repo - root folder
---------------------
// semantic versioning and release
// How to Release Code With Github - Kahan Data Solutions
// https://www.youtube.com/watch?v=Ob9llA_QhQY
major.minor.patch - impact convention
major - break existing interface
minor - add features without breaking existing
patch - fixes
----
kad se promeni minor promeni se i patch, sa 1.5.1 na 2.0.0, a ne 2.0.2, logicno
----
tag - v1.0.0 // has v? // jeste v, jer mozes da imas druge tagove sem za verzije
release - 1.0.0 // this number should be title on Github
----
issues ids and pull request ids in release description
release kreira zip arhive sa svim fajlovima, BEZ git foldera
----
Github Action to automate release
-----------
// rezime:
v1.0.0 // tag, bilo github ui bilo konzola
1.0.0 // release, moze i 2.0.0-preview, alpha, beta // pre-release checkbox
markdown opis promena sa issue and PR linkovima u release opis, i to je to
--------------
// tag i release, prakticno:
// tagovanje iz konzole
// moze komit da bude i pushovan vec, pa samo push tag
// za svaki slucaj pull i grana
git pull
git checkout prava-grana
---
git tag -a v1.0.0 -m "Release 1.0.0"
// vidim u poruku mecu poruku tog commita, nema potrebe, github vec daje link na commit
// samo git push nece da gurne tag
// mora explicit push tag, ovo ne gura komit, vec mora prethodno da bude gurnut
// gurni tag na svaki remote posebno, origin, gitlab
git push origin v1.0.0
// to je to, te 2 komande za tag, posle release u github gui
-----
// release:
// u title release-a stavi 1.0.0 ili 1.0.0-beta bez opisa 
// markdown opis ide u textarea ispod sa issue i PR linkovima
// moze i title i opis da se edituje u gui bez problema, i da se obrise // zapazi
// release kreira zip arhive sa svim fajlovima, BEZ git foldera
------------------
prouci multiline commit poruke
--------
// GIT JE SAMO VERZIONISANI fajl sistem
----------------------------
// pull request mistakes
// https://twitter.com/NikkiSiapno/status/1576810402633113600
------
// git commit message format
// conventional commits
// https://cheatography.com/albelop/cheat-sheets/conventional-commits/
-----------------
// git pull ako vec imas commit lokalno
// sve ovo moze u config za global
// fatal: Need to specify how to reconcile divergent branches.
// ovo nece hteti jer imas lokalni komit
git pull --ff-only
// mora rebase da lokalni izbaci na top
git pull --rebase
// za sve origine, bitbucket, gitlab, github
git pull --rebase --all
------
// git --all povlaci sa svih GRANA (samim tim i remotes), origin, gitlab, github
// ok, korisno, zapamti
git pull --all
----
// znaci isto i push, objavi sve lokalne grane, znaci NIKAD ovo ne radi
git push --all
----
// ovaj dovoljan za log
git log --oneline
// ovako treba
username@computer2:~/Desktop/bitbucket resume$ git log --oneline
b11c0f9 (HEAD -> master) linux nano
c090070 (origin/master, origin/HEAD, gitlab/master, github/master) object internals 5 - 8 // svi remote na isto
-------------------------------
// Branching Strategies Explained - DevOps Toolkit
// https://www.youtube.com/watch?v=U_IFGpJDbeU
1. trunk based
svi sve na master, samo jedna grana
moras u kodu da iskljucujes feature, jer idu na produkciju
kad: visoko samopouzdanje, ili te nije briga za produkciju
2. feature branches (github flow) // ne git flow, zapazi
master i feature branches
pull requests, features ne traju dugo - 1 dan npr
kad: za male jednostavne projekte sa malim featureima
3. fork strategy
menjas forkovan repo, pa pravis pr
ne treba write privilegije za glavni repo
kad: za open source (uvek) // zapazi
4. release branching
master i po grana za svaki release
release traju dugo, waterfall
kad: kad moras da podrzis vise zadnjih verzija
5. git flow
master, dev, feature, releases branches
ono sto sam ja imao, ono normalno
kad: ?
6. environment branching
isto sto i git flow + staging i prod branch
komplikovano
kad: ?
-------------------------------
git submodules - separate repositories, pointers to commits
git subtrees - isti repository
---------------------
// commit poruke, odlicno, prouci
https://www.conventionalcommits.org/en/v1.0.0/
--------------------------
// run pre-commit husky hook in advance, manually
"lint-staged": {
    "*.{ts,tsx,js,jsx}": "eslint --quiet --fix"
}
// manually
yarn eslint --quiet --fix
------------------
// pull request assignee, zaduzenik, odgovorni, ja
PR assignee - ja, ciji je pull request
---------------------
// brisanje lokalnih grana posle merge PR
// delete local branches i upstreams - prune, dobra fora
git fetch --prune
git fetch -p
-------
// listaj sve grane, i remote, provera
git branch -a
--------
// ima smisla samo za koje nisu public, inace treba prune
// radi merged u PR, nemerged lokalno
git branch -d fix/newsroom
-D za unmerged
------------------------------
// PR in progress - draft PR na Githubu
za postojeci, ispod reviewers -> convert to draft dugme
------------------------
// push tekucu i bilo koju granu
git push // granu na kojoj si
git push origin neka-druga-grana // bilo koja
-------------------------
// rename branch, odlicno
// local, eto -m
git branch -m oldBranchName newBranchName
// remote
git push remoteName :oldBranchName newBranchName
---------------------------
// git gui clients
// vs code extension, dobar
// Git Graph
// https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph
--------
// linux client, dobar
// SmartGit
// https://www.syntevo.com/smartgit
---------------------------
git je samo fajl sistem, za uprosceno razumevanje
--------------------------
// push to multiple remotes
// https://jigarius.com/blog/multiple-git-remote-repositories
// kreiras novi remote all i dodas mu sve url-ove // to, to
git remote add all first-url
git remote set-url --add --push all first-url-again
git remote set-url --add --push all second-url
-------
// npr
git remote add all git@bitbucket.org:nemanjabb/resume.git && \
git remote set-url --add --push all git@bitbucket.org:nemanjabb/resume.git && \
git remote set-url --add --push all git@gitlab.com:nemanjam/resume.git && \
git remote set-url --add --push all git@github.com:nemanjam/resume.git && \
git remote -v
---------------------
probaj cherry pick 1 commit sa grane na granu
---------------------
// samo git pull, git push
moze bez problema i na develop i na feature/my-branch, radi samo na tekucoj grani // eto
ne mora origin ime-grane svaki put
---------------------
// conflict u rebase
kad resolvujes conflict ne sme commit kao sa merge, nego // zapazi
git rebase --continue
------------------------
// You have divergent branches and need to specify how to reconcile them
remote grane se vec razdvojile
create temp branch, pull both remotes on it, merge temp to main, delete temp, main push all
-------------------------
// odlicno i prakticno
// Practical Git Guide
// https://github.com/sadanandpai/git-guide
--------------------------
// rename Github repo
1. rename in Github settings
// rename remote
2. git remote set-url origin git@github.com:nemanjam/new-name.git
---------------------------
prouci protected branches na github
---------------------------
git pull // pulls all branches, zavisi od config
git pull --all // isto all branches
-----
git config --get remote.origin.fetch // citanje
+refs/heads/*:refs/remotes/origin/* // za sve */
+refs/heads/<branch />:refs/remotes/origin/<branch /> // samo current <branch /> mora da zamenis sa ime
-----
// set primer, samo za pull na main, bzvz
git config remote.origin.fetch "+refs/heads/main:refs/remotes/origin/main" 
-----------------
svaki remote ima u lokalu svoju granu, zapazi
origin/main, gitlab/main, all/main
zato nastane diverge nekako
----------------------------
----------------------------
multiline commit poruka
// chatGpt
git commit 
// git otvori nano sa # komentarisan tekst
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch master
# Your branch is up to date with 'origin/master'.
#
# Changes to be committed:
#       modified:   md-test.md
#
// sad ovde pises prvu poruku u jednoj liniji, ostatak paragrafi odvojeni sa jedan newline
// komentarisane linije sa # se ne cuvaju nigde, brisu se
// nano - ctrl + O upis, ctrl + X izadje, ili pita za upis pa izadje
// odmah ctrl + X aborts commit, jer su samo komentari, prazna poruka, ok
// chatGpt primer poruke
Implement new feature: User authentication

This commit introduces a new feature to the application: user authentication. It includes the following changes:

- Added user login functionality
- Implemented user registration with email verification
- Created user profile page with basic information

This feature enables users to securely log in to the application and access personalized content. 

Closes #123
-----------------------------
// sto se desava diverged
svaki remote ima svoju lokalnu granu, cak i all
origin/main, gitlab/main, all/main
i jednu lokalnu main, naravno
--------------------
git pull sa develop nije updateovao main // zapazi
-------------------------
// git merge and rebase, procitaj
clanci:
// https://www.freecodecamp.org/news/git-rebase-handbook/
// https://www.freecodecamp.org/news/the-definitive-guide-to-git-merge/
youtube playlist:
// https://www.youtube.com/watch?v=eG9oAroMcPk&list=PL9lx0DXCC4BNashNittu7oU8n1CDtvDnu
lista sa git undo:
// https://www.youtube.com/watch?v=fWMKue-WBok&list=PL9lx0DXCC4BNUby5H58y6s2TQVLadV8v7
------------------------
// How to Fix Merge Conflicts in Git
// https://www.freecodecamp.org/news/how-to-fix-merge-conflicts-in-git/
lose i nepotpuno objasnio, screenshotovi
// 2 vrste konflikta
1. content conflict // iste linije u 2 grane, klasican
2. structural conflict // var, fja... nije lepo objasnio?
-----
// github interface:
rucno makni <<<<===>>>, mark resolved, commit merge
------
// vs code
<<<<HEAD (current change) // to je current grana
========
>>>>feature-branch (incomming change) // grana iz koje unosis
-------
// cancel merge
git merge --abort
-------
resolve, add, commit
-------
// vs code 3 way merge editor // nepotpuno objasnio
ovo ce da rebase conflict // zapazi
complete merge // button koji resolvuje
zatim add, commit
----------
// rebase u PR sa main // nesto kao...
git pull --rebase upstream main
git add .
git rebase --continue
git push --force
--------------------------------
// https://dev.to/jodaut/beyond-the-basics-guide-to-deeply-understand-merge-rebase-squash-and-cherry-pick-when-to-use-them-with-real-world-examples-3f2m
// posle reabse ide merge
git checkout feature4
git rebase main
---
git checkout main
git merge feature4 // nema merge commit, fast forward
------
rebase menja sadrzaj komitova, ali ne menja autora, vreme 
-------
// kad rebase
rebase samo na feature branches
if branch is going to be deleted after a merge, then you can rebase it
na main nikad
----------
posle rebase ne mozes da push na feature4 branch, mora force
// greska:
`Your branch and 'origin/<branch>' have diverged,
(use "git pull" to merge the remote branch into yours)`
if you force a git pull, you could lose your changes // zapazi
// resenje
git push -f origin feature4
-------------------
// cherry-pick 
use case: treba da uneses fix u legacy grane, ne moze merge, nego treba samo 1 commit
za grane koje se mnogo razlikuju pa ne moze merge // eto, "merge sa 1 commit"
---
na grani na koju unosis
git cherry-pick <commit-sha>
---
// primer, logicno
git checkout legacy-v1
git cherry-pick 8d84e7b6bf5ed8a4fee96dfa820e544567542785
dodaje taj komit pod novim id, ista poruka // novi commit
0c41007 (HEAD -> legacy-v1) (fix) feature 1
--------
// logovanje
git log --oneline
git log --stat // dodaje br linija i fajlove, ok
-------------------
// git squash
// squash with merge // dobar, nisam znao
// ubaci sve sa feature4 pod 1 commit
git checkout main // na main, zapazi, obican merge
git merge --squash feature4
git commit -m "feature4 commits"
------------------
// squash with rebase
git checkout feature4 // na feature4, zapazi
git rebase -i main
// git ti da ovo, vim
pick b86b56d feature4
pick 0c847bd (fix) feature4
# opis ... 
// ostavis ovo // nije bas lepo objasnio ovde
pick b86b56d feature4
squash 0c847bd (fix) feature4 // zamenio pick sa squash
ESC + :wq // sacuvaj u vim
sledeci vim da upises commit poruku
sad je feature4 rebasovan (squashed)
------
// merge squashed to main, normalno
git checkout main
git merge feature4
-------------------
// recover, prevention, pre pocetka
// download local branch again
git checkout main
git branch -D feature4 // delete (messed up) feature4 branch
git checkout origin feature4 // download branch again
------
// backup branch (ako unistis i local i remote grane sa git push -f)
git branch feature4-backup feature4 // kopira granu
// recover branch from backup
git branch feature4 feature4-backup
git push --set-upstream origin feature4
-------
// sacuvas commit ids od git log
// iako izbrises granu ili commitove, commitovi i dalje postoje (reflog) 
// i mozes da ih vratis ako znas ids
git checkout feature8
git merge 407f3e2 // stari head, pobrisao 3 commits sa git reset
--------------------
// https://dev.to/opensauced/keeping-your-branch-up-to-date-and-handling-merge-conflicts-while-waiting-for-pr-reviews-3b3h
----------------------
// undo last not pushed, local commit, radi
git reset HEAD~
-----------
// undo last commit, git reset vs git revert // ok
// git reset id // koji id da postane novi HEAD, tj. na koji da dodjes
git reset HEAD~
HEAD - current branch, last commit (not staging naravno)
~ - tilda je unazad (parent) commitovi // zapazi, lako
HEAD~1 // isto sto i HEAD~
HEAD~2 // 2 unazad ...
HEAD~3 // moze i HEAD~~~ // eto
---------
git reset prima parent commit, hoces 5 treba da prosledis 4
tj id commita na koji hoces da dodjes, novi HEAD // to, to
This will move the branch pointer to commit abcdef123456.
ne koji resetujes, nego na koji hoces da dodjes, da bude novi HEAD // ETO
----------
// primer
* abc1234 (HEAD -> main) Latest commit
* def5678 Another commit
* ghi9012 Yet another commit // dodji na ovaj, novi HEAD
* jkl3456 Initial commit
---
git reset ghi9012
---
* ghi9012 (HEAD -> main) Yet another commit
* jkl3456 Initial commit
-----------------
// git revert id // koji commit hoces da obrnes
kreira NOVI INVERTED commit od tog, zato moze bilo koji iz sredine, postojeca istorija netaknuta
id - koji komit hoce da obrnes
---
// primer
* abc1234 (HEAD -> main) Latest commit
* def5678 Another commit // obrni ovaj, iz sredine
* ghi9012 Yet another commit
* jkl3456 Initial commit
---
git revert def5678
---
* hij7890 (HEAD -> main) Revert "Another commit" // novi inverzni na vrhu
* abc1234 Latest commit // istorija netaknuta, novi commit gore samo
* def5678 Another commit
* ghi9012 Yet another commit
* jkl3456 Initial commit
---------------------------
// git patch
// https://dev.to/0xog_pg/git-patches-your-friend-to-less-copy-paste-3294
patch apply changes between different repositories or branches
// create patch
git format-patch commit-id // jedan komit
git format-patch HEAD~2 // ili vise njih, 3
// apply
git apply this-is-my-patch.patch
-----
// rej files
Rej files (rejection files) are generated during the patch application process when conflicts occur
// resolve conflict in rej files
git apply --reject // reapply patch
-----
// hunks
Hunks represent the isolated changes within a patch file
git apply --include --exclude // delove, verovatno diff ili fajl
git add --patch // interactive
-------------------
// remove all uncommited changes
// vrati na poslednji commit
git reset --hard HEAD // brise i nove fajlove
git clean -f -n // --dry-run, preview first
git clean -f // then apply, -f --force, -d i foldere // NE sklanja promene u tracked files, zapazi
git stash save --include-untracked // include untracked files
git stash drop stash@{0} // discard zadnji stash
git stash list // listaj stasheve
git checkout . -- // preview first, -- argument vs opcija, var ista kao komanda
git checkout . // zadrzi untracked files
git restore . // zadrzi untracked files
----
nijedna ne brise .gitignored files // zapazi, niti prati niti brise
---
// bas za to, brisanje uncommited i nepotrebnih fajlova
// NE DIRA promene u tracked files // zapazi
git clean 
-----------
git reset --soft // zadrzi promene, ostavi kao staged // undo commit
git reset --mixed // default, zadrzi promene, unstage // undo commit i add
git reset --hard // skloni sve promene
---------
HEAD  // current commit, branch
HEAD~ // predzadnji commit
HEAD~ i HEAD~1 su isto, a ne HEAD i HEAD~ // ok



