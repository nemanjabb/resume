

/**
* Git and Svn notes. Spring 2015.
*
*
*
*/


https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-12-04

//instal git clanak

sudo apt-get install git-core
------------
u config ubaci email

sudo nano ~/.gitconfig

You can use the follow commands to add in the required information.

git config --global user.name "NewUser"
git config --global user.email newuser@example.com
--------------------------------------------
--------------------------------------------
https://www.digitalocean.com/community/tutorials/how-to-set-up-automatic-deployment-with-git-with-a-vps

//deploy with git clanak

Your server live directory: /var/www/domain.com
Your server repository: /var/repo/site.git
---------
napravi live repozitor u site.git folder

cd /var
mkdir repo && cd repo
mkdir site.git && cd site.git
git init --bare
--------
ubaci post-receive hook site.git/hooks/post-receive

cat > post-receive //napravi fajl

//sadrzaj
#!/bin/sh
git --work-tree=/var/www/domain.com --git-dir=/var/repo/site.git checkout -f
//domain.com folder gde da kopira, site.git staza do repozitorijuma
//exec privilegije
chmod +x post-receive
-----------
kreiraj lokalni repozitorijum

cd /my/workspace
mkdir project && cd project
git init

linkuj na remote rep
git remote add live ssh://user@mydomain.com/var/repo/site.git
//live alias

ubaci projekat u folder i komituj u git

git add .
git commit -m "My project is ready"

//komituj na remote

git push live master
---------------------
beta repozitorijum, do sad bio live

//beta sajt
cd /var/www/
mkdir beta

//beta repo
cd /var/repo
mkdir beta.git && cd beta.git
git init --bare

//beta hook
cd hooks
cat > post-receive
#!/bin/sh
git --work-tree=/var/www/beta --git-dir=/var/repo/beta.git checkout -f
Press 'control-d' to save.

//iz lokalnog
exit
cd /my/workspace/project

//dodaj i beta kao remote
git remote add beta ssh://user@mydomain.com/var/repo/beta.git

//komit
git add .
git commit -m "New version"
//gurni na beta
git push beta master
//gurni na live
git push live master
--------------
//dodaj live kao remote na beta
cd /var/repo/beta.git
git remote add live ../site.git
//gurni sa beta na live
cd /var/repo/beta.git
git push live master

--------------------------------------
--------------------------------------
//setup ssh keys
https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2

ssh javni kljuc za kriptovanje, privatni za dekriptovanje

javni kljuc se kopira u fajl na serveru, privatni je u klijentu

//Create the RSA Key Pair
ssh-keygen -t rsa
//Store the Keys and Passphrase
uneses sta te pita dalje
Your identification has been saved in /home/demo/.ssh/id_rsa //privatni
Your public key has been saved in /home/demo/.ssh/id_rsa.pub //javni
The key fingerprint is: 4a:dd:0a:c6:35:4e:3f:ed:27:38:8c:74:44:4d:93:67//crc kljuca verovatno

//Copy the Public Key (na server) na lokaciju ~/.ssh/authorized_keys
ssh-copy-id user@123.45.56.78
//preko ssh
cat ~/.ssh/id_rsa.pub | ssh user@123.45.56.78 "mkdir -p ~/.ssh && cat >>  ~/.ssh/authorized_keys"

//iskljucivanje logina preko passworda, samo ssh kljuc
//u fajl
sudo nano /etc/ssh/sshd_config
postavi
PermitRootLogin without-password
//reload ssh sesrvis
reload ssh
---------------------------
---------------------------
etc/hosts je samo kad hoces da koristis string mesto IP
------------
ubuntu desktopi gnome. kde, mate
----------------------
paketi menadzeri nuget, npm, composer zavisnost stavis u json jer im nije mesto u git ili svn (vcs) repozitorijumu, nije tvoj kod
----------------------
Kod distribuiranih version sistema, 1 pribavljanje i 2 primena promena u fajlovima su odvojeni koraci za razliku od centralizovanih sistema
1 git fetch i 2 git merge
1+2 = git pull tj svn update
from remote origin
komit lokalni, update centralizovan
--------------
nesto = nesto tviter?
----------

//-------------------------------
//-------------------------------
//svn
tag je snapshot koji se ne dira, za dalji rad koristi se branch
-----------
switch menja granu
-----------
update - mora resolve conflicts, ako neko promenio (komitovao) isto u medjuvremenu
-------------
undo commit - show log (tajno), ili merge
------------------
------------------
konflikt moze samo na update
ne mozes da komitujes ako nisi updateovao na zadnju verziju, i kad neko promenio u medjuvremenu
----------------
patch je razlika izmedju dva fajla, kao alter u sql
ili kad nemas write dozvolu pa napises patch pa on vidi da li da ga primeni ili ne
patch_file.diff - sve receno
-------------------
git commit - commits locally
git push origin master  - pushes the master branch to the remote named "origin"
-------------------
-------------------
-------------------
//2019
git fetch origin
git status
git pull origin //je fetch + merge
git merge origin/master
vim
sredi kod rucno pa
git add .
git commit -m "fix merge conflicts"
git push
//--------------
//rebase
git pull
git checkout -b my_feature_branch //kreira granu
git commit -m "...
git checkout master //prebaci se na master granu
git pull //povuce sa master, ima obe grane updateovane
git checkout my_feature_branch
git rebase master //prebaci komitove sa feature grane na master
git checkout master //vrati se na master
git rebase my_feature //sad vrati na master u 1 liniji graf
git push
//----------------
ssh-keygen -t rsa -C "mojemail@email.com"
cd ~/.ssh
cat id_rsa.pub
kopiraj kljuc na github
git push origin ime_grane
-----------
major.minor.patch
breaking.feature.patch
-------
git clone repo_link
git checkout -b ime_grane //kreiraj granu
git commit ...
git remote -v
git push origin ime_grane //gurne granu na remote
github selektuje develop granu i klikne pull request
--------------
git branch //lista grane
git branch ime_grane //kreiraj granu
git checkout ime_grane //prebaci se na granu
git log //vidi komitove
git checkout -b ime_grane //kreiraj i prebaci se na granu//zavisno od grane na kojoj si vec
git commit ...
git checkout develop
-------------
git branch //listaj grane
git checkout develop //prebaci na glavnu
git merge feature_grana //spoji feature sa glavnom
git log
git show commit_hash //prikazi kod komitovan
master je produkcija
develop je staging
-----------
uvek pull master pre merge sa master
prebaci na master
git pull
git merge feature_grana
git push origin master
---
https://www.youtube.com/watch?v=QutwvKiRLLs
clone default granu, develop je default
master je samo za release verzije
git flow plugin je za to
--------------
//sync forked repo
original je upstream, tvoj forked je origin
radis pull sa upstreama, radis push na origin i kreiras pull request
git remote -v 
git remote add upstream git@remoteURL.github.com
git remote -v 
git fetch upstream
git merge upstream/master
git push origin master
------------
//pull request
syncujes sa fork, komitujes i push na origin, ili grana, i pull request dugme na github
izaberes na koju njuhovu granu sa koje tvoje grane
sluzi da dodas kod na repozitorijum na koji nemas pravo da radis push
---------------
---------------

Pull request

kreiras granu
radis, add, komit
switch to master i pull //jedino to zapazi, moras da updateujes kad zavrsis, da budes na latest version
switch to feature i merge master
edit conflicts, head tj gornje tvoje, donje origin
add. kommit
push granu to remote, git push -u origin feature
to je to, ides na github, selektujes granu
create pull request, naslov i koment, send pull request //samo zapazi da je master base grana a feature desno
odobre, obrisu remote granu
git checkout master
git pull
delete local branch

---------
//ne mozes da brises granu na kojoj si
Delete Local Branch

//merged, zapazi da je branch a ne checkout
$ git branch -d branch_name
//unmerged, force
$ git branch -D branch_name

---------

Delete Remote Branch

git push origin --delete branch_name

---------

Push branch to remote and set upstream, set tracking //-u (short for --set-upstream) 

git checkout -b branch_name
git push -u origin branch_name

-------------
-------------

Undoing commit

//da vidis hasheve, sa q izlazis

git log --oneline --decorate --graph

---------
//undo commit, 0766c053 je jedan pre njega
git revert --no-commit 0766c053..HEAD
git commit

--------
//undo zadnji komit, kao novi komit, ako je vec na remote
git revert HEAD
pa izadje vim commit, ides :wq da cuvas, esc ili i za insert ili command mode
----------------------
----------------------
//Git Tutorial: Fixing Common Mistakes and Undoing Bad Commits
//https://www.youtube.com/watch?v=FdZecVxzJbk
working directory - dok radis, pre add
staging area - posle add
working tree - posle commit?
--------
dopuni komit, pogresna poruka ili nije dodat fajl
git commit --amend -m "nova poruka" //nikad za javne komite, jedno m u amend
-------
komit na pogresnu granu, unesi na pravu granu, reset ili revert sa pogresne grane da maknes komit
git log za hasheve, 7-8 karaktera dovoljno
predji na pravu granu
git cherry-pick hash komita //unese
------
da maknes
git reset --soft hash_ovde //hash komita na koji hoces da dodjes
--soft staging area, added
--mixed working directori, nije added ali nije ni obrisano, izostavis --mixed jer je default
--hard brise, ali ostavlja untracked fajlove pa mora git clean -df
git clean -df //brise sve untracked fajlove i foldere, d folder, f fajl
reset samo za lokalne komite
git status i log iza svake komande
:wq sacuvaj i izadji vim za komit
------
git reflog //do mesec dana
git checkout hash koji hoces, dobijes kod ali u detached head modu, nisi ni na jednoj grani to znaci
git branch ime-grane// napravi granu od toga pa dobijes izgubljeni kod u grani
------
za javne revert
git revert hash //hash onog koji hoces da revertujes, a ne onog na koji hoces da dodjes, znaci zadnji uglavnom
:wq
git diff prethodni_hash posle_hash//za diff, donji hashevi stariji, gornji noviji u git log
------------------------
NE SMES DA SWITCHUJES GRANU AKO IMAS PROMENE NEKOMITOVANE, prenese promene na granu
------
by the way, if you have old repos visible: try this:
git remote update origin --prune
--------------------
--------------------
//stash
//https://www.youtube.com/watch?v=KLEDKgMmbBI
git stash save "poruka" // skloni promene i stavi u stek
git stash list //izlistaj ih
git stash apply stash{0} //stash{0} je id iz git stash list, primeni promene ali ne brise iz steka
git checkout -- . //?revertuje kao
git stash pop //primeni promene i makne iz steka
git stash drop stash{0} //samo ga skloni taj
git stash clear // skloni sve stasheve
-----
//prebaci nekomitovano sa pogresne grane
git stash save "poruka"
git checkout druga-grana
git stash pop
-------------------
by the way, if you have old repos visible: try this:
git remote update origin --prune
-------------------
//Git Tutorial for Beginners: Command-Line Fundamentals
//https://www.youtube.com/watch?v=HVsySz-h9r4
git --version //da li je instaliran
git config --global user.name "nemanjam"
git config --global user.email "email@email.com"
git config --list //stampaj config
git help <komanda> //za help
----
wokrking directory //pre add
stagging area //posle add
repository //posle komit
checkout //iz repozitorija u working directory
git reset //makne sve iz staging area,suprotno od add
ili git reset ime-fajla
----
git remote -v
git branch -a //i remote grane
git push -u origin ime-grane // da bi mogao samo push i pull, remote i local grana asociated
git branch --merged //grane koje su mergovane sa tom, master npr
git branch -d ime-grane //obrisi lokalnu granu
git push origin --delete ime-grane //delete remote granu
-------------------
// rebase feature branch continue
git rebase master
//resolve conflicts
git rebase --continue
git add .
git commit -m "rebase feature branch"
-----------------------------
-----------------------------
//rebase vs merge
//https://www.youtube.com/watch?v=dO9BtPDIHJ8
oba sluze da uneses promene sa jedne na drugu granu
merge za public branches, rebase za private branches
rebase menja istoriju, merge ne
rebase kreira nove komitove (od postojecih), merge samo zadnji merge komit
merge komit ima vise od 1 pretka
rebaseom dobijas linearnu istoriju kao da je radjeno sve na master, a feautre grana nije ni postojala
----------------------------
----------------------------
//productive git for developers egghead
//2 git log alias
//Make my git log look pretty and readable
u home .gitconfig lg alias za git log

//3 commit na master umesto na feature
//Move some commits to a separate branch that I have accidentally committed to master
ako nije pushovano
napravis granu i resetujes master
git reset --hard hash-na-koji-hoces-da-dodjes

//4 Update my feature branch with the latest changes from master
git pull na master
checkout feature
ako je grana na remote vec onda git merge master
ako je samo lokalna git rebase master // gura granu na vrh mastera

//5 Push a rebased local branch by using `--force-with-lease`
lokalnu rebaseovanu granu guras na remote sa --force-with-lease

//6 Polish my git feature branch before merging or submitting for review
squash commits sa rebase
git rebase -i hash-od-kog-kreces-jedan-pre // r da promenis poruku, f za squash da makne komit
:wq zatvori i sacuvaj kad menja poruku komita
clear

//7 Automate the cleanup of my feature branch with Git Autosquash
git rebase -i --autosquash hash-jedan-pre
git commit --fixup hash //sa kojim kasnije squash//nebuloza

//8 Squash all of my commits into a single one and merge into master
git merge --squash ime-grane
git commit //pa vim sa porukom 

//9 Change the commit message of my last commit
git commit --amend //pa promeni poruku//samo za lokalne komitove, menja se hash

//10 Add a file I’ve forgotten to add to my last commit
git commit --amend //dopuni zadnji komit promenom u fajlu

//11 Undo my last commit and split it into two separate ones
git reset HEAD~ //pa komit sta treba

//12 Wipe a commit from my local branch
u git log vidi se remote dokle je kao (origin/master)
nikad reset pre tog komita
git reset --hard hash-gde-je-origin-master-npr //vrati kao sto je na remote

//13 Undo a commit that has already been pushed to the remote repository
git revert hash-onog-kojeg-revertujes //pravi suprotan komit
na javnim ne sme ni jedna komanda koja menja hash, reset, amend itd

//14 Temporarily store some work in progress because I have to jump to another branch
git stash save "poruka"
git stash list // stash@{0} stack
git stash pop //apply i izbrise iz steka
git stash apply //apply i ostavi u steku
----------------------------
----------------------------
HEAD is the symbolic name for the currently checked out commit -- it's essentially what commit you're working on top of.

Normally HEAD points to a branch name (like bugFix). When you commit, the status of bugFix is altered and this change is visible through HEAD.
---------------------------
---------------------------
// Practical Git - Codecourse
//12 deailing with conflicts
2 pull requesta sa konfliktom, 1 prihvacen na msster
checkout master, git pull, checkout feature, merge master, resolve conflict, push feature, sad nemaju konflikt
znaci pull sa master, i merge master na feature pa push feature

//13 contributing to open source projects (forked repo)
fork pa
git remote add upstream https://github.com/blabla.git
git pull upstream master
git checkout feature
git merge master // fix conflict, add, commit
na pull request selektujes base fork repo i base granu, isto i head fork i granu
obrise lokalnu i remote feature granu

//14 amending commits and dangers
git status // clean
git commit -amend //
otvori vim, pa i za insert mode, promeni poruku
esc, :wq
ako amendujes komit koji je vec pushovan svi komitovi bazirani na njemu su obrisani

//15 amending part 2
git add .
git commit --amend //dopunis kodom poslednji komit umesto novog, :wq

//16 merging
svi komitovi hronoloski?, plus merge komit
a,d,b,e,c jeste vremenski bez obzira sa koje su grane

//17 rebase
MENJA HASHEVE, NIKAKO ZA REMOTE GRANE
kalemi feature granu na vrh mastera, d,e,a,b,c tj. master1,master2,feature1,feature2 //to to
bez merge kommita
na feature, rebase master //radis na feature grani, master ne diras
git checkout feature
git rebase master //konflikti, git add, git rebasse --continue
git log --all --decorate --oneline --graph -10 //stapaj graf

//18 squashing commits
NIKAKO ZA JAVNE KOMITOVE
git rebase -i HEAD~4 //koliko zadnjih
reword //za rename
squash //za squash sa prethodnim, ostavi poruku (dole novi, gore stari)
pick //ostaje isti komit
:wq
git rebase --edit-todo //edit ako si pogresio
git rebase --continue //edit poruku, :wq

//19 stashing basics
pre nego predje sa grane na granu
git stash
git stash pop //apply i brise sa steka//na bilo kojoj grani da si
git stash apply //apply i keep u stek, redje se koristi
git stash list //kaze ti na kojoj je grani stash
git stash pop stash@{1} //ref koji stash hoces

//20 stashing gotchas
git stash ne stashuje nove untracked fajlove
git stash -u //za untracked fajlove
git stash -a //i fajlove u gitignore
git stash drop //sve, ili 
git stash drop stash@{1} //za jedan

//23 untrack tracked files
git rm --cached fajl.env
git status

//24 git diff
git diff //za unstaged promene
git diff master..feature //izmedju 2 grane, sa bilo koje grane

----------------------
----------------------
// How Git Works - Pluralsight - Paolo Perrotta
//7 One More Thing: Annotated Tags
4 tipa objekata:
blobs - content, listovi
trees - folderi
commits
annotated tags
objects folder
git je filesystem sa verzionisanjem

//10 What Branches Really Are
branch je pointer na commit //refs folder

//11 The Mechanics of the Current Branch
head je reference(pointer) to a branch
pointer na pointer
head fajl
current branch pointer se pomera na nov komit, head pointer pokazuje na branch pointer
git checkout lisa //head pokazuje na lisa branch
checkout = move head and update working directory

//12 Let's Merge!
posle konflikta ide add
add znaci resolved conflict
merge se zavrsava sa komit
merge komit ima 2 ili vise parenta
i pomeri se pointer grane na merge komit

//13 Time Travel for Developers
pointeri izmedju komitova sluze za tracking history
ostali pointeri za tracking kontenta //refs, grane grafa
//graf - cvorovi komitovi, trees, blobs, a tags
git history state je podgraf koji polazi od tog komita

//14 Merging Without Merging
git checkout lisa //pomera head na lisa branch
git merge master //drugi merge
nema merge commita, fast-forward commit
samo pomeri pointer lisa grane na postojeci merge komit

//15 Losing Your HEAD
detached head
kad checkoutujes komit direktno
head ne pokazuje na granu
git branch ne pokazuje current branch
ako radis commit na detached, moras na kraju da napravis granu od toga sa git branch ime-grane
ako se vratis odmah na master, detached komitovi unreached i garbage collected

//16 Objects and References
repozitorijum je graf od 4 objekata
grane su pointeri na komitove, moze da ih ima vise
head je pointer na granu ili detached, ima ga 1

//18 What a Rebase Looks Like
on spageti, git rebase master
ide do prvog zajednickog komita, detach te komitove i puts on top of master
on master, git rebase spageti //radi fast forward, svejedno da li merge ili rebase

//19 An Illusion of Movement
kad se promeni parent komita promeni se i njegov sha1, lancano i sve dece
rebase kreira nove komitove sa istim sadrzajem i razlicitim parentom, i pomera pointer grane

//21 The Trade-offs of Merges
merge history je graf, preserve history

//22 The Trade-offs of Rebases
merge default strategija

//23 Tags in Brief
non annotated tags
git tag //lista
/refs/tags folder //tagovi
/refs/heads folder //grane
tag je isto sto i grana, pointer na komit, samo sto se ne pomera

//24 A Version Control System
content tracker -> revision system
branches, merges, rebases, tags

//27 Local and Remote
.git/config fajl, tu stoji remote
git branch --all //lokalne grane, remote grane, head
i lokalne i remote grane su referenca na komit, remote se updateuje

//29 The Chore of Pulling
git push -f //gurne tvoj na master, a postojeci detachuje, garbage collected
pull = fetch + merge

//30 Rebase Revisited
nikad rebase komit koji je bio na remote

//31 Getting Social
fork je remote clone
upstream i origin remotes
ovo nije feature gita nego githuba
pull from upstream, push to origin
pull request from origin to upstream

//32 The Whole Onion
onion - layered model
persistant map hashes to objects
stupid content tracker - trackuje promene u fajlovima i folderima
revision control system - branches, merges, rebases
distributed revision control system
---------------
//pull sa specific remote grane, logicno
git pull <remote /> <branchname />
-----
SACUVAJ FAJL POSLE EDITA KONFLIKTA
ide i add i commit
-----
koja grana odakle povlaci i gura, sve izlista
git remote show origin
------
undo git add .
// https://stackoverflow.com/questions/348170/how-do-i-undo-git-add-before-commit
git reset //je za undo add .
git rm --cached FILE // samo ako si dodao nov file
ili u vs code minus da ga maknes
------
//pull on specific branch
//https://stackoverflow.com/questions/4924002/git-pulling-from-specific-branch
na grani na kojoj zelis
git pull origin ime_remote_grane_sa_koje_zelis
------
compare two branches tortoise
//https://wikgren.fi/compare-diff-branches-in-tortoise-git-or-how-to-preview-changes-before-doing-a-merge/
browse reference -> compare selected refs
poredi bilo koja 2 komita na bilo kojim granama prakticno
ili
//https://stackoverflow.com/questions/9834689/comparing-two-branches-in-git
git diff branch_1..branch_2
git diff ..branch_2 //compares the checked out branch to branch_2 
---------------------------------
---------------------------------
// submodules

// Introduction to Git Submodules
// https://www.youtube.com/watch?v=UQvXst5I41I

// add child repo as submodule
git add submodule <child_repo_url.git> folder_name

// pull on submodule
git submodule update //sve submodule, prvi put --init
git submodule update samo-submodule-koji-hoces

parent trackuje samo referencu na komit hash childa, i to push i pull
child moze push i pull normalno
radi git status iz parenta i citaj

git log --oneline
git push/pull origin local-branch:remote-branch

----

// More about git submodules
// https://www.youtube.com/watch?v=w4AyLtIEibo

// da vidis da li su lokalno na komitu na kom treba da su
git submodule status

zamisljeno da parent treba da se menja a submoduli su slow motion dependencies // eto

-----------

// GIT Submodules - Git Series 7
// https://www.youtube.com/watch?v=Qe6_foSbWaQ

git checkout 1.0 // mozes da checkoutujes tag ovako
git submodule update //updateuje submodule commit ref to new one set locally ...

---------------------------
// git status broj promenjenih linija, eto, to to
// ovaj odgovor
// https://stackoverflow.com/a/41152332/4383275
git diff --stat | tail -n1

// zapravo ovo meni treba
git diff --stat

 src/components/ImageItem/ImageItem.module.scss | 10 ++++------
 1 file changed, 4 insertions(+), 6 deletions(-)
------------------------------
// mozes da revertujes bilo koji javni komit iz istorije iako nije zadnji
// https://stackoverflow.com/questions/2318777/undo-a-particular-commit-in-git-thats-been-pushed-to-remote-repos

git revert <commit>
-n flag samo da vrati promene bez da komituje, ako hoces dodatno da editujes pa rucno komitujes
git revert je suprotno od git cherry-pick, koji dodaje (patch), a ovaj sklanja, undo

git revert <commit> -m 1 // za merge komitove, koji parent da undo, ili -m 2
-------------------------
// git merge origin/master vs git merge master
lokalni master moze da ima jos neki komit koji si dodao a nisi pushovao
origin/master je lokalno koji stoji na istom komitu gde je na serveru
-------------------------
// vracanje iz detached head
git checkout neki-hash
git branch // pokaze ti privremenu granu
git checkout master // vrati te na head, ako nemas promene
--------------------------
// Git Branches for Well Ordered Code, code mochi
master je za produkciju samo, release working verzije, staging od master, feature je od staging
feature commits squashed opcija na pull request, staging ne, brise feature grane kad gotove i remote i lokalno
ako komit na staging git reset hard