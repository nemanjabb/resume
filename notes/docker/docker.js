/**
* Docker working notes. Summer 2016.
*
*
*
*/


docker-machine env default
eval "$(docker-machine env default)"
docker images //lista all images
docker ps -a //list all containers, daje ti ime kontejnera
docker-compose up //iz foldera gde imas docker-compose.yml

kontejner ima efemeral filesistem koji nije perzistentan, zato ima volume, koji se mapira na filesistem masine
kontejenr je bin + app
docker top <name ili id kontejnera> //iz docker ps -a
top lista top procese, zapazi kad je u ubuntu, a kad unutra u kontejneru root@userzapazi, pid razlicit, namespaces
lokalno kontejneri u /var/lib/docker/containers //tu su config fajlovi
tera razne linuxe na svom ubuntuu
-d //detach mod, pozadina
-t //terminal
-i //interactive

//persistant storage
volume je filesistem koji das kontejneru, writtable, masina readonly
volumes na lokalnoj masini su u docker instalaciji /var/lib/docker/volumes/ytrytr6565rry5y5ry/_volumename //default gde ih mece
docker attach imekontejnera //ulazi u kontejner pokrenut vec
linux lako sa apsolutnim stazama /a/b/c /je root
port mapiranje dodaje u iptables interface_ip_eth0_npr:lokalni_port:port_na_kontejneru //nije ip od bridge adaptera
iptables -L -t nat
docker bridge postoji

//this is from homestead
# Disable XDebug On The CLI
sudo phpdismod -s cli xdebug

//odakle se pokrece
krastavac: im guessing in a RUN statement in the Dockerfile
//dockerfile instalira i izvrsava shell, yml config za instalirano
KekSi	Dockerfile is the instructions needed to build an image and docker-compose.yml is the runtime configuration for those images to be run in a container

prouci RUN, ENTRYPOINT, CMD i ostale statemente u dockerfile, pozivaju shell
sve stalne promene na kontejneru moraju u dockerfile

image je fajl necega, kontejner ili masina
docker run pravi image od dockerfile-a



pixel6692	krastavac: docker compose is like package of "docker runs"
krastavac	"what to start from installed"
KekSi	krastavac: think of each service in the compose file as the yml-syntax form of "docker run" - it do "docker pull" for you and it will do "docker build" if you specify it a Dockerfile to build from
pixel6692	if you like your notation then yes from installed, we would "typically" say from built

krastavac	"docker compose is like package of "docker runs"" and reason they are not inside RUN statements in dockerfile but in yml is?
krastavac	to have many configs for same container?
KekSi	krastavac: like i just told you: a Dockerfile is the instructions that lead you from nothing (just the text of the Dockerfile) to an *IMAGE* to run that image in a *CONTAINER* you either "docker run" it for example or use something like compose to run it
KekSi	the parameters to "docker run" have equivalent directives in the docker-compose.yml file
pixel6692	krastavac: as first RUN in Dockerfile is not the same thing as docker run, and until you explicitly set another command: in docker run <container /> </command>; docker run will run CMD from Dockerfile (same applies for docker compose, since it just wraps docker runs more or less)

krastavac	for example if i would to add xdebug to this (its not included) i would go to php-fpm dockerfile and add cmd install statement, then rebuild image  https://github.com/LaraDock/laradock 
pixel6692	yes
pixel6692	well to be more specific, you dont need to modify their Dockerfile most of time
pixel6692	just take their image as FROM <image> and do your operations
krastavac	in my separate dockerfile
pixel6692	yes
krastavac	then in docker-compose.yml call mydockerfile instead php-vm dockerfile
krastavac	php-fpm*
krastavac	a bit confused when making changes, to see effect what you should restart/rebuild machine/container
pixel6692	if you want to have build block in docker-compose then yes, your Dockerfile, whole this is layer oriented so yoru Dockefile will strart FROM and so until FROM scratch
pixel6692	when you change something in Dockerfile you have to rebuild and then docker run
//rebuild
docker-compose build
docker-compose up //never rebuilds an image


krastavac	if i want to "GRANT ALL PRIVILEGES ON mydb.* TO 'myuser'@'%' WITH GRANT OPTION;" i should do that in mysql dockerfile or i can do that also in dockerfile of container that is using mysql container?
Khaytsus	krastavac: You can do it anywhere, yes?  Have a database container ready to accept databases and _something_ is using it; that something is what should set up databases.
krastavac	ok
pixel6692	krastavac: to correct your terminology, Image is something you make from Dockerfile and is ready to be run, container is running image. This is great article for starting: http://merrigrove.blogspot.com/2015/10/visualizing-docker-containers-and-images.html

----------------
//http://merrigrove.blogspot.rs/2015/10/visualizing-docker-containers-and-images.html

kontejner je top read-write sloj nad read slojevima (read slojevi su image)// to to
image-> create container-> run container

//slike su read only slojevi, kontejner je vrsni read-write sloj//poenta

//moguca pocetna stanja komandi na slici, i zavrno stanje
docker create <image-id>
docker start <container-id />
docker run <image-id>
docker ps
docker ps -a
docker images
docker images -a //sa istorijom
docker stop <container-id />
docker kill <container-id />
docker pause <container-id />
docker rm <container-id /> //removes the read-write layer, It must be run on stopped containers.  It effectively deletes files
docker rmi <image-id> //removes the read-layer 
docker commit <container-id /> //takes a container's top-level read-write layer and burns it into a read-only layer.
docker build //slozena komanda 
//uzima image iz FROM u docker-compose.yml ->docker run (kreira i startuje kontejner) -> RUN iz dockerfile (inicijalizuje ) -> docker commit
docker exec <running-container-id>//pokrece kontejner i proces u njemu
docker inspect <container-id /> or <image-id>//fetches the metadata that has been associated with the top-layer of the container or image.
docker save <image-id> //creates a single tar file that can be used to import on a different host system. This command can only be run on an image.
docker export <container-id />//flattens kontejner i sklanja metadata//creates a tar file of the contents of the "union view" and flattens it for consumption for non-Docker usages.  This command removes the metadata and the layers.  This command can only be run on containers.
docker history <image-id> //stampa roditelje slike//read-only layers


docker - fajlsistem plus namespace grupe procesa linux
dekapling i izolacija programa, portabilan deploy, deploy masina treba da ispuni samo 1 interfejs
dependancy manager za programe, verzioning fajsistema

KostyaSha	krastavac, docker info
programmerq	go into the settings for that VM, choose "storage", and disk1.vmdk is the disk image that holds your state-- containers, images, etc.
programmerq	it should include a Location
krastavac	C:\Users\username\.docker\machine\machines\default\disk.vmdk
===============================
//------------
//laraedit
//mount se izgubi kad ugasis masinu
mount -t vboxsf -o uid=1000,gid=50 VM /d/vm
docker run -p 80:80 -v /d/vm/zemke2:/var/www/html/app laraedit/laraedit
docker exec -it condescending_dubinsky bash //da udjes//menja se svaki put
docker-machine ssh default // to get to the vm//udjes u masinu
docker stop condescending_dubinsky
docker-machine ip default
php artisan migrate //iz /var/www/html/app jer je tu artisan //db_host localhost u env
php artisan db:seed


================================

https://www.sitepoint.com/docker-and-dockerfiles-made-easy/

docker-machine create --driver virtualbox docker-vm //kreiraj masinu
docker-machine env docker-vm // print the machine's configuration 
eval "$(docker-machine env docker-vm)" //Switch to the docker-vm machine//selektuje koju masinu koristi// u shellu, konzoli
docker run -tid -P nimmis/apache-php5 //-tid -P svi portovi
docker run -tid -p 80:80 -v ~/Desktop/www/laravel_demo:/var/www --name wazo_server nimmis/apache-php5
docker run --name <container name /> //postavis ime kontejnera
docker exec -it <container /> bash //ulazis ssh
docker start wazo_server//start stop kontejner
docker stop wazo_server
--------------------- 
//mysql
docker run -p 3306:3306 --name mysqlserver -e MYSQL_ROOT_PASSWORD=root -d mysql //-e option lets you set an environment variable on the container creation
//root password, zapazi MYSQL_ROOT_PASSWORD
docker-machine ip docker-vm
// .env
DB_HOST=192.168.59.103
DB_DATABASE=demo
DB_USERNAME=root
DB_PASSWORD=root
-------------------------
//--link opcija za povez kontejnera PREKO ENV VARS
docker run --link <container name or ID />:<alias /> //alias je samo prefix koji ce env vars da dobiju, trivijal
prenese env vars iz jednog kontejnera u drugi
// .env
DB_HOST="{$MYSQLDB_PORT_3306_TCP_ADDR}"	//appache-ov kontejner upisuje u laravel env, zato povezivao mysql kontejner
DB_DATABASE=demo
DB_USERNAME=root
DB_PASSWORD="{$MYSQLDB_ENV_MYSQL_ROOT_PASSWORD}"
----------------------
//namena dockerfile i docker-compose.yml
Dockerfiles to create personalized images 
Docker compose to manage our containers
------------------------
//dockerfile
FROM ubuntu:14.04 //ubuntu deo os-a nad kernelom//kao program //mozes ubuntu nad svakakvim kernelom da dizes?
ENV LARAVEL_VERSION ~5.1.0 //zapises promenljivu u kontejneru //PROSLEDJUJE U RUN POSLE $LARAVEL_VERSION
RUN apt-get update && \... //The RUN instruction allows us to run commands on the shell (not bash) //za bas RUN ["/bin/bash", "-c", "apt-get update"]
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf //kopira src_lokalni destination_u_kontejneru

WORKDIR /var/www    		//chdir menja folder za narednu RUN komandu, INSTALIRA LARAVEL SA COMPOSER GDE HOCE
RUN composer create-project...

EXPOSE 80 //otvara port
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"] //komanda, kao RUN bash,  RUN za instaliranje, //CMD instruction can be used to init the container ?? razlika?

COPY setup-laravel.sh /setup-laravel.sh 
RUN ["chmod", "+x", "/setup-laravel.sh"] //za chmod ide RUN
ENTRYPOINT ["/setup-laravel.sh"] //start up komanda u win registry//na (boot) pokretanje kontejnera
-------------------------
//docker-compose.yml
//sve sto si pokretao sa docker run komandom (opcije) ovde skriptujes //on ti je za to i reko, a ne za RUN, to to

web:							//defines the service name; this could be anything
  build: .						//image source, image from the Docker hub or a Dockerfile path 
  ports:
    - "80:80"
    - "443:443"
  volumes:
    - ./laravel:/var/www/laravel
  links:						//pokupi env vars iz mysql kontejnera i dodaj im prefix mysqldb
    - mysqldb:mysqldb

mysqldb:
  image: mysql
  environment:
    - MYSQL_ROOT_PASSWORD=root	// -e opcija iz run, postavlja env promenljivu, jeste nivo os-a, komunik izm kontejnera

docker-compose up	
-------------------------
docker ps //listanje kontejnera
docker images //listanje slika, ne masine, read only slojevi, ugasen kontejner, onaj clanak

======================================
// http://www.dylanlindgren.com/docker-for-the-laravel-framework/

We want to have one container for one process, and we will link each container (i.e. process) 
to a "data-only" container where all our app's files will be stored.

1 proces - 1 kontejner
Boot2Docker installers don't support mapping directories on the host inside the virtual machine they create //zato on napravio iso

Having separate containers for artisan and composer is a real advantage for us, as we can choose to only push the docker-laravel-data, 
docker-laravel-nginx, and docker-laravel-phpfpm containers to production when we go live without the possibility of breaking anything!

dev je nadskup, pa samo deploy deo kontejnera na produkciju, bez composer, artisan, xdebug itd

VOLUME ["/data"] //direktno u dockerfile jer ne koristi yml

// yml je scripting docker run komandi

--volumes-from myapp-data //nasledjivanje foldera //definises na 1 mesto pa reuse umesto na 5 mesta//from i link nije isto
--link myapp-php:fpm //ovo povezuje sve portove izm kontejnera, ne samo env vars?

na slici link povezani samo php i nginxs, ostali samo data, zapazi
========================================
========================================
-------------------
https://rominirani.com/docker-tutorial-series-part-8-linking-containers-69a4e5bf50fb#.cmhtgt3vx
//na osnovu iste slike pokrece kontejner koji se kaci na redis1 koji vec radi//svi citaju i upisuju u taj jedan i vide svacije promene
docker@boot2docker:~$ docker run -it --link redis1:redis --name client2 redis sh //client2 zavisi od redis1
--name pokrece drugi kontejner, --link kaze koji da reuse
--LINK JE REUSE AKO POSTOJI

php-fpm:
	links:
		- workspace //prvo se instalira ovo i ne zavisi od php-a//composer, cli...

ulazim u njega docker exec -it laradock_workspace_1 bash //to je dev sa alatima
logicnije bi bilo obrnuto, ovako ne mozes php bez alata da pokrenes na produkciji		
workspace i data kontejner nisu running jer nemaju pokrenute trajne procese vec samo instalirane programe composer itd		
		
----------------------------------	

docker-compose up -d  nginx phpmyadmin
docker-compose stop //svi
docker-compose rm phpmyadmin

vi i cat da vidis //etc/hosts fajl sadrzaj
//adresa hosta iz kontejnera
/sbin/ip route|awk '/default/ { print $3 }' //172.18.0.1
//2 nacin
ifconfig
with ifconfig in your host//linux
ipconfig /ALL

remote debug netbeans
app_env konfiguracija laravel
sta instalirano na produkciji php laravel

docker-compose build 	//za rebuild image jedno od ta dva
docker-compose up --build
docker exec -it laradock_php-fpm_1 bash



docker-compose stop web
docker-compose build web
docker-compose up -d --no-deps web

//delete svi kontejneri
docker rm $(docker ps -a -q) //$() je izlaz naredbe
//delete sve images
docker rmi $(docker images -q)

//host je mysql a ne 127.0.0.1 u env
php artisan migrate
php artisan db:seed 

nginx 404 nije ti montovan folder na pocetku, uvek zaboravis

docker-machine ssh default
cd /
sudo mkdir /d
sudo mkdir /d/vm
sudo mount -t vboxsf -o uid=1000,gid=50 VM /d/vm
exit
cd /d/VM/zemke2/laradock
docker-compose up -d nginx phpmyadmin
docker exec -it laradock_php-fpm_1 bash

//https://github.com/wsargent/docker-cheat-sheet
expose je listen
port je izlazni


//http://stackoverflow.com/questions/273159/how-to-determine-if-a-port-is-open-on-a-windows-server
On the server itself, to check to see which ports are listening use
netstat -an //svi
netstat -np TCP | find "10000" //filter na port windows
sudo netstat -plnt | grep ':10000' //filter linux

From outside, just telnet host port (or telnet host:port on Unix systems) to see if the connection is refused, accepted, or timeouts

connection refused means that nothing is running on that port
accepted means that something is running on that port
timeout means that a firewall is blocking access

192.168.99.1 //.1 gateway adresa, 255 broadcast adresa
	
kad menjas docker-compose.yml samo stop i up -d kontejner, ne mora build
kad menjas dockerfile docker build php-fpm	
	
masina je os, kontejner je proces
workdir je gde ti se otvori masina
	
php -d xdebug.remote_autostart=1 -d xdebug.remote_host=0.0.0.0 file.php
//----------
da restorujes prostor vmdk, obrises masinu iz konzole, i kad pustis opet on je napravi
docker-machine rm default

//volumes smisao
volume mapping (sa : )je mountovanje diska, foldera, lokacije, injektovanje foldera na lokaciju, 
podmetnes postojeci folder sa sadrzajem
kad je bez : onda je volume u samom kontejneru, na hard disku kontejnera, 
perzistentan, inace se sve brise na gasenje

//links
kontejneri su virtualni procesi, links znaci proces vidi parenta

eval "$(boot2docker shellinit)" ti je pajpovanje konzole iz vm na windowsovu


//--------------------------- ubuntu
sudo mount -t vboxsf -o uid=1000,gid=50 VM /home/name/Desktop/VM 
montuje ga ali za lokaciju definisanu u vbox /media/sf_VM	
-----------------------

cd /c/Users/username/Desktop/zemke2/laradock
docker exec -it laradock_php-fpm_1 bash	
 /etc/php/7.0/fpm/conf.d 20-xdebug.ini	
	
/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	
// -y je yes na promte 
add-apt-repository -y ppa:ondrej/php
-----------------------------------
-----------------------------------
// Learn Docker in 7 Easy Steps - Full Beginner's Tutorial
https://www.youtube.com/watch?v=gAkwW2tuIqE

// dockerfile - source za image, build/compile
svaka komanda u dockerfile kreira 1 layer u image // to, to // VRLO VAZNO, ZAPAZI

workdir app //, kao cd, inicijalno pozicioniranje za naredne komande
copy package.json . //hoces da instaliras node_modules u sloju ispod src aplikacije, jer se src cesto menja
run npm install //run je pokrece komandu kroz shell consolu
copy . . // copira src aplikacije u /app, node_modules ignorise
u .dockerignore node_modules da ne bi kopirao node_modules iz src hosta u kontejner i overvrite // to
cmd ['npm', 'start'] //exec forma, ne pokrece shell consolu za razliku od run // samo 1 per dockerfile
----
container je running proces od image
------------------------------
------------------------------
// Docker Volumes Demo || Docker Tutorial 13
// TechWorld with Nana
// https://www.youtube.com/watch?v=SBUCYJgg4Mk
volumes type:
1. bind mount ili host volume
2. anonimous volume
3. named volume, ima ref

named volume ima default stazu na hostu gde je docker instaliran
win: C:\ProgramData\docker\volumes
linux: /var/lib/docker/volumes ... 
za named service_name-volume_name/_data
za anon hash/_data
------------------------------
// Docker Networking
// https://towardsdatascience.com/docker-networking-919461b7f498
When you install docker it creates three networks automatically - Bridge, Host, and None. 
Of which, Bridge is the default network a container gets attached to when it is run.

// selecting network 
Selecting the Host network will remove any network isolation between the docker host and the containers

The containers can reach each other using their names.
Embedded DNS which runs on the address 127.0.0.11

-------------------------
// slike koje ilustruju komande
// http://merrigrove.blogspot.com/2015/10/visualizing-docker-containers-and-images.html
-----------------------
// docker on heroku
// How to Build and Deploy a Container in Heroku Using a heroku.yml File
// https://www.youtube.com/watch?v=Z9SJTEC0wEs
Dockerfile
// heroku.yml - je manifest (config) fajl za container stack, nije docker-compose.yml, moze samo 1 container
build:
  docker:
    web: Dockerfile
run:
  web: npm run start
release:
  image: web
  command:
    - npm run migrate up
---
stack ne heroku-18 nego container stack
heroku stack:set container app-name
kod vuce sa githuba
postgres kreira sa weba
--------------------------------
// https://newbedev.com/how-to-upgrade-docker-compose-to-latest-version
//
// remove existing dc
sudo apt-get remove docker-compose
---
sudo apt install curl jq
---
// install latest docker-compose ubuntu
#!/bin/bash
compose_version=$(curl https://api.github.com/repos/docker/compose/releases/latest | jq .name -r)
output='/usr/local/bin/docker-compose'
curl -L https://github.com/docker/compose/releases/download/$compose_version/docker-compose-$(uname -s)-$(uname -m) -o $output
chmod +x $output
echo $(docker-compose --version)
---
reboot
----------------
// kad promenis dependencies
docker-compose up --build -d
--------------
// obrisi sve slike i kontejnere
// Next.js | Multi-stage Docker builds | Optimizing production deployments
// https://www.youtube.com/watch?v=zRhjo_jRF0U
docker system prune -a
---
multi stage build - u install, build stage instaliraj deps i builduj app pa samo kopiraj COPY potrebno u prod stage
manja slika na kraju, 20MB ustedeo na 240MB nista specijalno
kompajlirani build vec povuko sve sto mu treba i iskopirao u konacan kod i ne treba mu vise node_modules
---
COPY komanda odakle gde kopira?
// https://www.youtube.com/watch?v=ClGmDq-3ujM
context stazu salje docker build komanda
docker build -t image-tag-name . // . je context, odakle radi COPY u Dockerfile
COPY host-fajl-ili-folder container-folder
npm install node_modules su na hostu
u container ide samo sto iskopiras sa COPY
Dockerfile ne kopira sam sebe
---
u docker-compose taj ist context je u:
build:
  context: . // relativna staza u kojoj je docker-compose
  dockerfile: Dockerfile
----------------
.devcontainer folder je od vs code extenzije Remote Containers
--------------
docker run -it // i - interactive, t - terminal
-----------------------
// Deploying Docker on Heroku
// https://www.youtube.com/watch?v=4axmcEZTE7M

// switch to container stack
heroku stack:set container --app app-name-here
pa add heroku.yml
---
poveze sa git repo i na svaki push master rebuild
ne treba binarni image da push, dovoljno je Dockerfile
---
port mora da cita iz env varijable, ne sme 80 hardcodiran
---
// da makne deafault apache index.html, heroku ne izvrsava sve layere iz Dockerfile gde je brisao html fajl u slici
// kad slika radi lokalno, a ne radi na heroku
heroku labs:enable --app=app-name-here runtime-new-layer-extract
---
slike rade do velicine 7000 na heroku, inace smanji Dockerfile
----------------------
docker-compose up -d // start your containers in the background (i.e., detached mode)
docker-compose down // tear down the Compose setup 
docker-compose logs // check the logs of services running in background mode
----------------------
// docker run vs start
// create
Create adds a writeable container on top of your image and sets it up for running whatever command you specified in your CMD. 
The container ID is reported back but its not started.
---
// start
Start will start any stopped containers. This includes freshly created containers.
docker stop CONTAINER_ID, relaunch the same container with docker start CONTAINER_ID
---
// run = create + start
Run is a combination of create and start. It creates the container and starts it.
docker run IMAGE_ID and not docker run CONTAINER_ID
---------------
// docker env variables buildtime, runtime tabela
// https://www.saltycrane.com/blog/2021/04/buildtime-vs-runtime-environment-variables-nextjs-docker/
docker ima env vars koje su build time, runtime
next.js ima env vars koje su build time, runtime, server, client, node,js, pa permutacije
ovde pregledna tabela
---------------
multistage build samo iz zadnjeg stagea layeri ostaju u slici, proveri...?
u prod buildu sve env vars mora da budu prosledjene u buildtime...?
- je lista u docker-compose yml, volumes npr, zapazi
--------------------
// docker volumes
// https://stackoverflow.com/questions/46166304/docker-compose-volumes-without-colon
1. Host volumes - /path/on/host:/path/in/container
2. Named volumes - name:/path/in/container - pa dole volumes:name - da ga dele vise kontejnera
3. Anonymous volumes - /path/in/container - nikad, guid, ne znas koji je koji posle

2 i 3 mountuju na /var/lib/docker/volumes/hash... sa HOSTA, al ne kaze sta i zasto, gluposti
// staza gde tacno
docker inspect --type container -f '{{range $i, $v := .Mounts }}{{printf "%v\n" $v}}{{end}}' $container-name
cisto da ga makne iz postojeceg host volume na neku levu lokaciju da container ne pise na host?
2. ne pocinje sa / ili ./ - abs or relative path
named volume u okviru host volumea, zasto?
----
// prakticno mountuje sa hosta project folder ali bez node_modules i .next
// njih overwrituje na levu lokaciju da kontejner ne pise po hostu, /var/lib/docker...
// sklanja child folder koji ne zeli da je syncovan izmedju hosta i kontejnera, a ostatak parenta zeli
// jeste fora
// da ih ne sklanja node_modules i .next bi bili sa hosta
// a ovako ostaju sa kontejnera, jer ih kontejner mountuje na bzvz stazu hosta
// host montuje projekt folder u kontejner, pa koji podfolder da ostane sa kontejnera
// node_modules i .next su sa kontejnera, na ovaj nacin
services:
  nextjs:
	...
    volumes:
      - ./nextapp:/app
      - nextjs-dev-node_modules:/app/node_modules
      - nextjs-dev-next:/app/.next
	...
volumes:
  nextjs-dev-node_modules:
  nextjs-dev-next:
---
// iz perspektive kontejnera:
// project folder -> host na container - folder od hosta
// node_modules, .next -> container na host - folder containera
// direction: ?
// bind mounts - host to container
// named and anon volumes - container to host - valjda zato sto je prazan folder na hostu
-------
// zasto instalira depenedencies za dev umesto da mountuje sa hosta ceo projekt folder?
da bi isti Node.js koji je instalirao i izvrsavao
------------------
// docker runtime and buildtime env vars
// https://stackoverflow.com/questions/49293967/how-to-pass-environment-variable-to-docker-compose-up
// in docker-compose.yml
env_file: - runtime // this is from container - services in yml
.env - buildtime // docker-compose.yml ga trazi default // custom sa --env-file 
-----------------
// --env-file flag for custom .env buildtime
// https://docs.docker.com/compose/environment-variables/
docker-compose config // stampa d-c.yml sa zamenjenim env vars, za debug
docker-compose --env-file ./.env.production up // eto kako da renameujes .env za buildtime
-----------------------
// pass hosts env var to container with docker-compose.yml
// https://stackoverflow.com/questions/30494050/how-do-i-pass-environment-variables-to-docker-containers
my-app:
  container_name: my-app
  image: node_image
  environment:
    - CONTAINER_VAR=${HOST_VAR} // to je to
----
// list all env variables in the container, za debug, dobar
docker exec -it container-name printenv
-------------------
// CMD vs ENTRYPOINT Dockerfile
// https://stackoverflow.com/questions/21553353/what-is-the-difference-between-cmd-and-entrypoint-in-a-dockerfile
// cmd je argument entrypointa
CMD sets default command and/or parameters, which can be overwritten 
from command line when docker container runs.
---
ENTRYPOINT command and parameters will not be overwritten from command line. 
Instead, all command line arguments will be added after ENTRYPOINT parameters.
-------------------
// ADD vs COPY
// https://stackoverflow.com/questions/24958140/what-is-the-difference-between-the-copy-and-add-commands-in-a-dockerfile
COPY is Same as ADD but without the tar and remote URL handling.
-------------------
command in docker-compose.yml je override za CMD iz Dockefile
--------------------
ne uzastopne RUN commande nego jedna da smanjis broj layera u slici
RUN cmd1 \
    && cmd2 ...
------------------
SSHFS mount remote fylesystem, zapazi
-----------
// docker slike node itd
Debian 10 (Buster)
Debian 11 (Bullseye) // za arm64/Apple Silicon
-----------------------
// prisma migrate production
// https://notiz.dev/blog/prisma-migrate-deploy-with-docker
RUN - on image BUILD
CMD - NE radi on build nego on container START (docker run ili docker-compose up) // TO TO
migrate - deploy - syncuje schemu sa db, deploy je migrate-prod
---
container_name | container_service je hostname za db, umesto localhost
---
// multistage build
// copy svaki folder posebno da bi se kesirali umesto COPY . .
// evo
// copy source from host and node_modules separately
COPY . .
// node_modules is .dockerignored on host, this is from dependencies stage
COPY --from=dependencies /app/node_modules ./node_modules
-------------------
// prisma node_modules permission error
// remove named volumes after each build
volumes:
- ./:/app
- np-dev-node_modules:/app/node_modules // ova dva
- np-dev-next:/app/.next // i taj
-------
// solution
// create volume folders with node:node
RUN mkdir /app/node_modules /app/.next
RUN chown ${ARG_UID}:${ARG_GID} /app /app/node_modules /app/.next
---
// create folder if not exists
mkdir -p folder-name

COPY . .
COPY --from=dependencies --chown=${ARG_UID}:${ARG_GID} /app/node_modules ./node_modules
RUN npx prisma generate
RUN chown -R ${ARG_UID}:${ARG_GID} /app
---
// docker-compose.yml
nextjs-prisma:
  container_name: nextjs-prisma-dev
  user: '${UID}:${GID}'
  build:
    context: .
    dockerfile: Dockerfile.dev
    args:
      ARG_UID: ${UID}
      ARG_GID: ${GID}
--------------------
// print all env vars
printenv
------
// list files qith user:group
ls -la
// with ids, namespaces
ls -lan
-----------------------
// docker-compose.yml sintaksa za env vars i args
build:
  context: .
  dockerfile: Dockerfile.prod
  args: // pod build
    - ARG_DATABASE_URL=$DATABASE_URL // var=$var
environment:
  - NEXTAUTH_URL=$NEXTAUTH_URL // var=$var
----
// zapazi kako su ARG pod build, a ENV u cvoru iznad, ARG se ignorisu runtime, ENV buildtime
// ni environment:..., ni env_file: , u docker-compose.yml se ne prenosi u build fazi
// samo na run container
------------------
// ENV, ARG, build time vs runtime
// ukratko ARG - buildtime, ENV - runtime ili hardkodirane buildtime
// buildtime
ARG: buildtime moze SAMO ARG, docker build i nema -env u options // zapazi
ENV: env vars build time se NE PROSLEDJUJU, mogu samo da se HARDCODIRAJU, ili ostave undefined, 
jer kod ih NE REFERENCIRA buildtime, ali ih poziva runtime
---
ARG je samo koliko da nije hardcodiran u Dockerfile, nego prosledjen kao argument
ali svakako mora da se renuilduje
---
// https://vsupalov.com/docker-env-vars/
ENV mogu buildtime da se postave na default sa ARG, pa se runtime overriduju sa pravim ENV
// ovako
ARG ARG_1 // value za ovo porosledjujes buildtime sa --build-arg ARG_1=foo
ENV VAR_1=$ARG_1 // runtime ARG je undefined, a VAR_1 je prosledjen
-------------
// next.js docker env vars, args, buildtime, runtime pregled i tabela
// zapravo odlican link, sve objasnio
// https://www.saltycrane.com/blog/2021/04/buildtime-vs-runtime-environment-variables-nextjs-docker/
ja u dev sliku nisam prosledio ni jednu env var buildtime, pogledaj u Portainer nema ni jedna env var
u prod sliku treba DATABASE_URL buildtime za next.js build, i ni jedna env var druga
-----------------
// kako docker zna da li si promenio neki packages.json ili source fajl kad treba da rebuilduje?
------------------
// ENV var je scopovana samo na taj FROM, proverio u Dockerfile
// isto i ARG
ARG ARG_DATABASE_URL
ENV DATABASE_URL=$ARG_DATABASE_URL
RUN echo "DATABASE_URL_3=$DATABASE_URL"
------------------------------
// velika greska, kopirao sadrzaj foldera prisma u .
// error cannot find table main.Post
COPY package.json yarn.lock ./
// treba
COPY prisma ./prisma
// uvek je greska gde si nesto olako napisao
-------------------
// folder mora da ima x permission da bi mogao da udjes u njega sa cd, zapazi
// a ne samo read
chmod +x /path/to/dir/
-----------------------
// debug layers size in image
docker history --human --format "{{.CreatedBy}}: {{.Size}}" image-name
portainer ima odlican sortabilni pregled
dive https://github.com/wagoodman/dive
docker run --rm -it \
    -v /var/run/docker.sock:/var/run/docker.sock \
    wagoodman/dive:latest nextjs-prisma-boilerplate_nextjs-prisma-prod:latest
--------------------
// shrink image size, multistage build
// https://medium.com/trendyol-tech/how-we-reduce-node-docker-image-size-in-3-steps-ff2762b51d5a
u prethodnim stageovima istaliras pakete, libs itd koje sluze samo da izbildujes za runner stage
i ne ostanu u runner stageu, prakticno pomocne masine, slike
1. mala pocetna slika
2. multistage build
3. npm prune --production, yarn install --production=true
4. node prune RUN /usr/local/bin/node-prune
-----------------------
// vs code remote containers 
port forward samo za Dockerfile, vec definisano u docker-compose.yml
inace vs code prosledi 3001 na 3002 jer docker-compose.yml vec forwarduje 3001
mora da rebildujes kad ti trazi, cak i d-c.yml, onda radi
https?
----
// docker pitanja
docker change detection for project files and yarn reinstall, question?
zasto i kad pravi nove kontejnere na docker-compose up, ili docker run?
named volume prezivi kad izbrises kontejner, da li i zasto?
// devcontainers pitanja
how git ssh works from ubuntu host?
ne treba mi git u containeru, jer vs code koristi git sa hosta?
volume bind mount project folder not needed kad moze da radi samo sa Dockerfile?
bez volume promene izgubljene kad restartujes kontejner?
dont yarn instal nego bind mount project folder with node_modules?
---------------
ne multistage build nego 2 odvojene slike, prva sa svim dependencies pa from, i nema rebuild prva
------------------------------
// d-c up, down, start, stop
// https://www.saltycrane.com/cheat-sheets/docker/
---
// BUILD, CREATE, and start all in the background
// kao docker run - i kreira, ne samo pokrece
docker-compose up -d
// stop and REMOVE containers and networks in docker-compose.yml. 
// (this does not remove volumes.) - to
docker-compose down
---
// OVI za rad, kad su kontejneri ok
// start all containers (Compose):
docker-compose start
// stop all containers (Compose):
docker-compose stop
-----------------------
// env vars in docker-compose.yml
env_file inserts into container, not in docker-compose.yml, those are from host (HOSTNAME)

env_file:
  - .env.production // only inside the container
  - .env.local
labels: ...${HOSTNAME} // undefined here from .env.local
------------------
// env_file vs --env-file
// to to, zapazi
env_file: u yml - ubacuje u container
docker-compose --env-file .env.something up - ubacuje u docker-compose.yml
---
da li --env-file ubacuje i u container? - ne, ne postoje u containeru, ok
-----------
// rezime, razdvojeno je - tacno
env_file:, environment: - samo container
--env-file - samo docker-compose.yml
-----------
// ZAPRAVO2 rezime:
// https://docs.docker.com/compose/environment-variables/#using-the---env-file--option
// Substitute environment variables in Compose files
env_file:, environment: ubacuju u kontejner
.env i --env-file=.env.nesto zamenjuju u docker-compose.yml
---
--env-file= je isto sto i .env sa custom imenom i relativnom stazom
---
// docker-compose debug sa config
docker-compose --env-file ./config/.env.dev config
---
// --env-file= ce da zameni TAG u docker-compose.yml
services:
  web:
    image: "webapp:${TAG}"
------------------------
// copy file from container to host
// https://stackoverflow.com/questions/22049212/docker-copying-files-from-docker-container-to-host
docker cp <containerId or name>:/file/path/within/container/file.txt /host/path/folder
-----------------
// docker-compose run - overriduje command/CMD from Dockerfile
Create a new container for my_service in docker-compose.yml 
and run the echo command instead of the specified command (inside the container)
docker-compose run my_service echo "hello"
---------------------
// database hostname for app container in docker-compose
// https://stackoverflow.com/questions/33357567/econnrefused-for-postgres-on-nodejs-with-dockers
localhost - localhost containera
host.docker.internal - localhost hosta
db-service-name - app container ima ip od db u /etc/hosts (not container_name)
---------------------
// environment: vs env_file: in docker-compose.yml
`environment:` has precedence over `env_file:` in docker-compose.yml
look resloved env vars in **Portainer**
-----
// $VAR vs ${VAR}
---
// expands later, remains same in container
NEXTAUTH_URL=$PROTOCOL://$HOSTNAME:$PORT
---
// expands imidiatelly, cannot override part
DATABASE_URL=postgresql://${POSTGRES_USER}...
----------
// env var expanding
// leave expanding to library (dotenv npr)
CYPRESS_baseUrl=$PROTOCOL://$HOSTNAME:$PORT
// expand it immidiatelly like this (host shell will expand it)
CYPRESS_baseUrl=${PROTOCOL}://${HOSTNAME}:${PORT}
----------------------------
// d-c.yml service name as localhost
// https://stackoverflow.com/questions/43579740/in-docker-compose-how-to-create-an-alias-link-to-localhost
it works for both other containers (on same network or same d-c) and itself (localhost)
----
// anotherservice can connect to someservice with http://someservice/
// because they're on a common network the_net.
---
// someservice can also reach itself at http://someservice/ // to to
services:
  someservice:
    image: someserviceimage
    networks:
      - the_net

  anotherservice:
    image: anotherserviceimage
    networks:
      - the_net

networks:
  the_net:
---------------
// https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach
network_mode: host // both container and host are on localhost (only on linux)
---
Use --network="host" in your docker run command, then 127.0.0.1 in your 
docker container will point to your docker host.
-----------------------
// VAZNA FORA:
// najbolje uputstvo kako da koristis sliku je readme na https://hub.docker.com/_/slika
----
// kako da teras postgres volume as current host user (non-root)
// https://hub.docker.com/_/postgres
// https://github.com/docker-library/docs/blob/master/postgres/content.md - on Github
Arbitrary --user Notes
1. use debian image, a ne alpine - najjednostavnije - postgres:14.3-bullseye
2. bind-mount /etc/passwd from host
3. initialize the target directory separately ...
-------
// postgres volume permissions solution
- **it works:** mount one dir above (`prisma/pg-data`) and set data dir as subdirectory (`prisma/pg-data/data-test`), add `prisma/pg-data/.gitkeep`
- Gitlab [example](https://gitlab.apertis.org/infrastructure/qa-report-app/-/merge_requests/39)
----
// maybe hardcode 1000:1000 for prod
image: postgres:14.3-bullseye
user: '${MY_UID}:${MY_GID}'
volumes:
  - ./prisma/pg-data:/var/lib/postgresql/data
environment:
  - PGDATA=/var/lib/postgresql/data/data-test

// .gitignore, .dockerignore
// ignore data, commit .gitkeep
prisma/pg-data/data-*
---------------------------
// default user u containeru je root, ako nije drugi prosledjen, cela poenta
// https://medium.com/@mccode/understanding-how-uid-and-gid-work-in-docker-containers-c37a01d01cf
host i kontejner koriste isti kernel, zato imaju i iste usere
// ovo je vazno
ako nije specificiran (Dockerfile, --user ili user:) kontejner default koristi root usera, uid=0 
user se identifikuje po uid, username moze biti razlicit za isti uid na hostu i kontejneru
nista vise od toga nije objasnjavao
sve ovo za linux host, mac i win imaju alpine vm
-----------------------------
// docker-compose stop and remove single service-container
// https://stackoverflow.com/questions/62063960/is-there-a-way-to-stop-a-single-docker-service-and-remove-its-assigned-volumes
docker-compose rm -fsv <service>
f - force, dont ask for confirmation
s - stop before remove
v - remove volume
----------
// docker-compose down ne moze single service zbog networks
// uvek ceo fajl, down prima iste args kao up koji je podigao
// https://stackoverflow.com/questions/41450741/why-doesnt-docker-compose-down-take-an-optional-service-argument
// tacno down BRISE kontejnere, pogledao u Portaineru, a ne samo stopira
------------------------------
// docker-compose run vs up
docker-compose run service command // is docker-compose up with command overriden
-------
docker-compose up service // selektuje service u okviru d-c.yml, vise servicea
docker-compose run service command // selektuje commandu u okviru servicea, uvek 1 service
---
docker-compose run moze da override sve kljuceve iz d-c.yml za service
------------------------------
// docker tag - rename image basically
// https://docs.docker.com/engine/reference/commandline/tag/
docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
---------------------
// Dockerfile.prod runtime vs build time env vars (docker ARGs)
DATABASE_URL - runtime var, buildtime samo ako imas SSG
NEXTAUTH_URL - runtime var, used in Head buildtime ako imas SSG, ali moras i react code da menjas


