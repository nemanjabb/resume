-------------------------
poenta sa dokku: ako buildujes sliku na serveru treba ti uvek 4gb ram prazno da mozes da izbildujes
ako vec imas sliku onda dokku ne treba, dovoljan traefik, sa slikom dokku je samo ruter
samo ako nemas sliku ima smisla, kao heroku
----
btw dokku moze i run image, Dockerfile, docker-compose.yml i aws lambda builders
-------------------------
// buildpacks - heroku tehnologija
// Cloud Native Buildpacks (CNCFMinutes 23)
// https://www.youtube.com/watch?v=nG7N6SLNO4Q
buildpack - kreira docker sliku samo na osnovu app code, bez Dockerfile, analizira js kod i libs
oci image - docker slike
faze:
1. detect - package.json
2. build - compile
------
builder (image) - stack + lifecycle
stack - build image + run image
buildpack
-----
buildpack.toml, builder.toml
-----
// u app src
// pack builder suggest
ubuntu@arm1:~/traefik-proxy/apps/mern-boilerplate$ pack builder suggest
Suggested builders:
Google:                gcr.io/buildpacks/builder:v1      
Heroku:                heroku/builder:22
Heroku:                heroku/buildpacks:20
Paketo Buildpacks:     paketobuildpacks/builder:base
Paketo Buildpacks:     paketobuildpacks/builder:full
Paketo Buildpacks:     paketobuildpacks/builder:tiny 
---
// you are using a pack builder that doesn't work on arm64
-----
// kad pokusas docker image da pokrenes koja nije podrzana na arm
// ova greska izadje 
exec user process caused: exec format error
-----------------------------
// run amd64 images on arm64 machine with dokku, solution - install emulator
-----
// can use main, no need for master
git push dokku main:main

// can use default
gliderlabs/herokuish:latest

// solves all arm64 dependencies, postgres, node.js...
https://featurist.co.uk/blog/hosting-rails-apps-for-free-on-oracle-cloud-with-dokku
https://www.reddit.com/r/docker/comments/ray2wc/comment/hnluex8
https://stackoverflow.com/questions/67017795/npm-install-is-failing-with-docker-buildx-linux-arm64
https://github.com/tonistiigi/binfmt
https://hub.docker.com/r/tonistiigi/binfmt

// this line, container installs and configures the host
docker run --privileged --rm tonistiigi/binfmt --install all
-----
// chatGpt
Yes, that command installs and configures the binfmt_misc kernel module on the host system, 
which enables running binaries for different architectures, including arm64, on a Docker container. 
This is required for running Node.js build and runtime images on arm64 architecture systems.

The --privileged flag provides access to all devices on the host, including the kernel module. 
This flag is necessary to run the command as it requires elevated privileges.

Overall, this command can be useful when building or running Node.js applications on an arm64-based system, 
particularly when using Docker containers.
---------------------
// route dokku through traefik so you can use portainer and not have collision on 80 and 443
// either dokku or traefik handle lets encrypt, only expose https
services:
  dokku:
    container_name: dokku
    # image: dokku/dokku:0.30.1
    build:
      context: .
      # install pack in Dockerfile
      dockerfile: Dockerfile
    ports:
      - '3022:22'
      # - '8443:443'
      # - '8080:80'
    environment:
      - DOKKU_HOSTNAME=dokku.${SERVER_HOSTNAME}
      - DOKKU_HOST_ROOT=/var/lib/dokku/home/dokku
    volumes:
      - ./dokku-data:/mnt/dokku
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - proxy
    labels:
      - 'traefik.enable=true'
      - 'traefik.docker.network=proxy'
      # traefik will handle letsencrypt
      - 'traefik.http.routers.dokku-http.rule=HostRegexp(`{subdomain:[[:ascii:]]+}.dokku.${SERVER_HOSTNAME}`)'
      - 'traefik.http.routers.dokku-http.entrypoints=web'
      # dokku default exposes 80 and 443
      - 'traefik.http.services.dokku-http.loadbalancer.server.port=80'
      # passthrough https
      - 'traefik.tcp.routers.dokku-https.tls=true'
      - 'traefik.tcp.routers.dokku-https.tls.passthrough=true'
      - 'traefik.tcp.routers.dokku-https.rule=HostSNIRegexp(`{subdomain:[[:ascii:]]+}.dokku.${SERVER_HOSTNAME}`)'
      - 'traefik.tcp.routers.dokku-https.entrypoints=websecure'
      - 'traefik.tcp.services.dokku-https.loadbalancer.server.port=443'

networks:
  proxy:
    external: true
--------------------
// nisu na istoj mrezi dokku i app kontejneri
// https://dokku.com/docs/networking/network/#attaching-an-app-to-a-network
// ovo je resenje
// VAZNO: pogledaj u portainer na kojim mrezama su app, dokku i traefik kontejneri, prva 3 broja // to, to
// radi, testiraj sa curl, ne browser http
eventi: attach-post-create, attach-post-deploy, initial-network
-------
// koji od ovih
// https://dokku.com/docs/networking/network/#when-to-attach-containers-to-a-network
// ovo, dodaj na proxy network gde si traefik i dokku
dokku network:set --global initial-network proxy
// create app
dokku apps:create node-app
// read networks for app to check
dokku network:report node-app
// list all networks
dokku network:list
-------
// remove app, nema remove all
dokku apps:destroy node-app
// bypass yes
dokku apps:destroy --force node-app
// list apps
dokku apps:list
--------------------
// shell alias
// .bashrc - non-login shell, ovaj
// .bash_profile - login shell
alias dokku="docker exec -it dokku bash dokku"
// reload shell to apply
source ~/.bashrc
// alias it as dokkuc da nemas konflikte za svaki slucaj
----
alias dcdown="docker compose down"
alias dcup="docker compose up -d"
---------------------
dokku postavlja za deploy granu na koju prvi put push by default
znaci moze main, postavi env var
Set main to DOKKU_DEPLOY_BRANCH
----------------------
// add pub ssh key directly from local dev
// add local pub key
// this works - docker cp version
// dokku container must exist, 
docker exec -i -> da ne zatvori stdin nego ceka druge komande i redirecte // potreban
docker exec -t -> terminal, za coveka, idu oba -it // ne treba ovde
docker exec -i dokku bash -c '...' fails, ne valja
flag must be used, no bash -c '' quotes
dokku ssh-keys:add admin < /tmp/dokku_docker__id_rsa.pub // < redirect na levu komandu
ne treba docker cp za duplo kopiranje na server i container // to
---------
// delete from container, za jednu komandu ne treba -i
docker exec dokku rm -rf /tmp/dokku_docker__id_rsa.pub
docker exec dokku ls -la
-------
// set local key in container
// ovaj radi i to je to
// set key, works multiline
scp ~/.ssh/oracle/dokku_docker__id_rsa.pub ubuntu@arm1:/tmp/ && \
ssh ubuntu@arm1 " \
  docker exec -i dokku bash dokku ssh-keys:add admin < /tmp/dokku_docker__id_rsa.pub && \
  rm /tmp/dokku_docker__id_rsa.pub \
"
----
// remove key
ssh ubuntu@arm1 "docker exec dokku bash dokku ssh-keys:remove admin"
// list keys
ssh ubuntu@arm1 "docker exec dokku bash dokku ssh-keys:list"
--------------------
// https://dokku.com/docs/deployment/application-deployment/?h=dokku_letsencrypt_email#deploying-to-subdomains
// deploy to subdomain - ima crtice u imenu - www.ruby-getting-started.dokku.domen.com
git remote add dokku dokku@dokku.me:ruby-getting-started
// deploy to domain - lici na domen, ima tacke, www.app.dokku.me
git remote add dokku dokku@dokku.me:app.dokku.me
-------
// moze vise domena za isti app -vhosts
// https://dokku.com/docs/configuration/domains/#default-site
// add a domain to an app
dokku domains:add node-js-app dokku.me
-----
// rebuild, redeploy app
dokku ps:rebuild ruby-getting-started
----------------
// plugin install in docker
// https://dokku.com/docs/getting-started/install/docker/
// install custom plugins, create a plugin-list file in the host's /var/lib/dokku // HOSTS, zapazi
znaci u kontejneru treba da bude u /mnt/dokku folderu
/var/lib/dokku na hostu je /mnt/dokku u kontejneru // to, reko u docs
environment:
- DOKKU_HOST_ROOT=${PWD}/dokku-data/home/dokku
volumes:
- ${PWD}/dokku-data:/mnt/dokku
- ${PWD}/plugin-list:/mnt/dokku/plugin-list
------------
// DOKKU_HOST_ROOT = path on host, volume path + /home/dokku
// https://stackoverflow.com/questions/70882322/failed-to-build-node-app-on-dokku-running-as-a-docker-image-on-macos
// https://github.com/dokku/dokku/discussions/5522#discussioncomment-4413356
// njegov originalni post
docker container run \
  --env DOKKU_HOSTNAME=dokku.me \
  --env DOKKU_HOST_ROOT=$PWD/tmp/home/dokku \ // to
  --name dokku \
  --publish 3022:22 \
  --publish 8080:80 \
  --publish 8443:443 \
  --volume $PWD/tmp:/mnt/dokku \ // to
  --volume /var/run/docker.sock:/var/run/docker.sock \
  dokku/dokku:0.29.0
---------------------
// nginx hsts
HSTS stands for HTTP Strict Transport Security. 
web security policy mechanism that instructs web browsers to only access a website 
using secure connections over HTTPS, even if the user types "http" instead of "https" in the URL
-----
prakticno disable http u browseru, server - nginx postavlja header
---------------------
// samo nginx podrzava custom port mapping, traefik, caddy... rade samo na 80 i 443
You can now configure host -> container port mappings with the proxy:ports-* commands. 
This mapping is currently supported by the built-in nginx-vhosts plugin.
---------------------
// github action, odlicno
// https://dokku.com/docs/deployment/continuous-integration/github-actions/
radi git push sa action runnera, private ssh key
--------
// zapravo je ovo deploy sa githuba, ne treba akcija
// https://dokku.com/docs/deployment/methods/git/#initializing-an-app-repository-from-a-remote-image-without-a-registry
dokku git:sync node-js-app https://github.com/heroku/node-js-getting-started.git
// od slike sa dockerhuba
// https://dokku.com/docs/deployment/methods/git/#initializing-an-app-repository-from-a-docker-image
dokku git:from-image node-js-app dokku/node-js-getting-started:latest
konvertuje u Dockerfile FROM dokku/node-js-getting-started:latest
-----------------
// uvek setuj node i npm version ili auto resolvuje na zadnji, cak iza lts
// package.json
"engines": {
  "node": "16.19.1",
  "npm": "8.19.3"
},
---------------
build time vs runtime env vars... // zapazi
-------------
// debugging dokku app
dokku report nextjs-app
dokku ps:inspect $APP
-------------------
// attach database container to network
// https://github.com/dokku/dokku-mongo#set-or-clear-a-property-for-a-service
dokku mongo:set my-database post-create-network my-external-network
// restart mongo container
--------------------------
build script u root package.json za custom build umesto predeploy, i radi
predeploy ne valja jer se izvrsava i na dokku ps:start
// https://github.com/dokku/dokku/discussions/5718#discussioncomment-5211219
to zavisi od buildpacka
verovatno ce i start da izvrsi bez Procfile
"scripts": {
  "start": "node index.js",
  "build": "webpack"
}
---------------------------



