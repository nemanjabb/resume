// docker-compose.yml bind container na host localhost
extra_hosts:
- 'host.docker.internal:host-gateway'
--------
// pod ovim container vidi hostov localhost
host.docker.internal // VAZNO, treba da se zapamti
----------
// docker-compose.yml service name vs localhost
ako je app na hostu, a samo baza u dockeru - localhost u app .env
ako je i app u d-c.yml - onda service_name u app .env 
------
slicno i adminer je u d-c.yml, zato mora service_name 
------
docker po defaultu binduje portove na 0.0.0.0, container slusa na svim adresama na hostu
network mode - bridge, ... 
----------------
// ports docker-compose.yml
ports:
  - '5080:80'
  - host:container
  - spoljni-zahtev:app
----------------------
// build multiplatform images buildx and qemu // prostije nego sto sam mislio
// https://drpdishant.medium.com/multi-arch-images-with-docker-buildx-and-qemu-141e0b6161e7
// listaj podrzane arhitekture
docker buildx ls
----
// ovi paketi treba
sudo apt install qemu qemu-user-static // samo qemu-user-static je trebao
// Register QEMU for Docker, chatGpt
// treba zaista, za docker buildx inspect --bootstrap da bi radilo
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
------
// proveri da li su instalirani
apt list --installed | grep qemu
apt list --installed | grep qemu-user-static
// ili
which qemu-system-x86_64
which qemu-user-static
-----
// listaj opet
docker buildx ls
----------
// intiate builder
docker buildx create --use --name mybuilder
// proveri opet, lista sve arhitekture
docker buildx ls
------
// Inspect the builder to ensure it's set up correctly
docker buildx inspect --bootstrap
------
// build and push image
docker buildx build \
    -t yourusername/yourimagename:tag \
    --platform linux/amd64,linux/arm64 \
    --push  .
----
// build sa docker-compose.yml
"docker:prod:arm:client:build": "COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 DOCKER_DEFAULT_PLATFORM=linux/arm64 docker compose build mdp-client",
-------
// test image architecture
docker inspect --format='{{.Architecture}}' nemanjamitic/mern-boilerplate-client:latest
--------------
// remove all volumes, jedino ovo radi
docker volume rm $(docker volume ls -q)
// list volumes
docker volume ls
----
portainer cant accept certificate is about cookies, local storage, volumes
u private tab radi 
to je do firefoxa nije do portainera, redirektuje http na https, zapamtio
// ovo je resenje
// https://stackoverflow.com/questions/30532471/firefox-redirects-to-https
CTRL+H paste in url, right click one entry and there is option to 'Forget about this site'. 
It clears all info about the website and preferences.
-----------------
za mapiranje portova uglavnom ne treba expose 
cak i u razlicitim docker-compose.yml fajlovima
dovoljno je da su containeri na istim mrezama, apps unutra vec exposeuju portove 
za razlicite docker-compose.yml external network i to je to
----------------------
// network_mode: "host" // to je ono sto mi treba
binduje kontejner za host mrezu
svi portovi se vide, ne treba mapping nigde 80:80
-----
za mreze container i host postaju isto, samo filesystem odvojen
radi samo na Linuxu potpuno
----
// docker-compose.yml
services:
  app:
    image: my-app-image
    network_mode: "host"
------------
// pros:
simplifies networking configuration
reduces network latency by eliminating the Docker network bridge
performance
// cons:
ecurity risks, host network
Mac and Windows have limited support
container less portable
------------------------------
// docker-compose.override.yml, chatGpt
poenta: samo da ne bi ponavljao zajednicki deo u oba yml fajla, -f option i dalje mora // to
override gazi ili dodaje nova polja i servise 
// run only base file
docker compose -f docker-compose.yml up -d
// run merged files
// znaci po defaultu tera oba
docker compose up -d
-----------
prod je base, a develop je mergeovani // ok
------------------------
// umesto up/down, jedna komanda
docker compose pull
docker compose up --force-recreate
----------------
// inspect manifest, za koje arhitekture postoji slika
docker manifest inspect nemanjamitic/nemanjam.github.io:latest
--------
// verbose logging za RUN komande u Dokcerfile sa buildx 
--progress=plain
export BUILDKIT_PROGRESS=plain // na hostu env var
progress: plain // u build-and-push github actions
--------------
// tar local image, download, tag and push to dockerhub
// run remote
docker save -o my-rathole-arm64-v1.0.tar core-rathole:latest
// download
// run local
scp pi:~/Desktop/my-rathole-arm64-v1.0.tar ~/Desktop/
// load tar in local docker
docker load -i ~/Desktop/my-rathole-arm64-v1.0.tar
// change tag image // must include dockerhub username, tag ide iza :
docker tag core-rathole:latest nemanjamitic/my-rathole-arm64:v1.0
// list images
docker image ls
// login and push
docker login 
// agan // must include dockerhub username
docker push nemanjamitic/my-rathole-arm64:v1.0
----------------
// resenje za Dockerfile RUN ls -la not logging
"dc:build": "DOCKER_BUILDKIT=0 docker compose build",
poenta: ovo disablira buildkit, buildkit build ne loguje
----------------
// ! important: cache, db, log files in volumes must be created by app non-root user so app can write to them, or exceptions