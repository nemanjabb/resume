// volumes in node.js containers and devcontainers
----
volumes:
- ./:/app
- npb-app-dev-node_modules:/app/node_modules
- npb-app-dev-next:/app/.next
----
// iz portainera
Destination /app/node_modules
Source /var/lib/docker/volumes/npb-dev_npb-app-dev-node_modules/_data
----
// poenta, dobra fora koja radi kako treba
tu ce biti node_modules iz kontejnera, samo perzistovani na hostu da prezive gasenje
inicijalno ih popuni kontjener (Dockerfile yarn install)
i posle ih opet cita i updateuje kontejner
host node_modules iz bind src foldera NIKAD ne upisuje tu
// sve ok, 2 para node_modules mogu da postoje paralelno na istom mestu i da se ne mesaju
----
// npb-app-dev-node_modules ti je alias za ovu stazu na hostu 
// /var/lib/docker/volumes/npb-dev_npb-app-dev-node_modules/_data
----
// kontejner uzima:
/app/src -> ./ // host
/app/node_modules -> /var/lib/docker/volumes/npb-dev_npb-app-dev-node_modules/_data // host
...
-----------------------------------
// Docker and Node.js Best Practices from Bret Fisher at DockerCon - Bret Fisher Docker and DevOps
// 2019.
// https://www.youtube.com/watch?v=Zgx0o8QjJk4
// https://github.com/BretFisher/nodejs-rocks-in-docker // nema sve bas
// dotako dobre teme ali ih nije bas objasnio kako treba
node images:
1. default - 900MB, build tools
2. alpine - mala 5MB, security, nodemon ne radi
3. slim - mala skoro kao alpine, bez build tools, ako ne treba kompajliranje prilikom npm install
-----
// node_modules
ne zelis da koristis node_modules sa hosta u kontejneru, doduse radi na linux hostu
js ce da radi ali binarni nece na razlicitoj arhitekturi, image libs npr
resenje:
dodas node_modules\ u .dockerignore, da ne ide u container, sve sto je inace u .gitignore
// user
sve node slike imaju user node, ali default je root, mora u Dockerfile USER node
----
USER node ide iza:
apt/apk
npm i -g // global zahteva root
ispred npm i // ok
----------
// shut down, process management - za zero down time
node, python ne slusaju linux shut down signale by default
na ctrl C docker ceka 10 sec default, node treba odmah da se ugasi
ne treba npm run script u CMD // zapazi, jer npm ne slusa linux signale
-----
// 3 opcije za ctrl C
1. docker run --init -d nodeapp
2. tini package u slici
3. app handles SIGINT
-----
// 3. ovako nesto, kod na slajdu slican
// https://yvonnickfrin.dev/shutdown-correctly-nodejs-apps
function handleExit(signal) {
    console.log(`Received ${signal}. Close my server properly.`)
    server.close(function () {
        process.exit(0);
    });
}
process.on('SIGINT', handleExit);
process.on('SIGQUIT', handleExit);
process.on('SIGTERM', handleExit);
----
// bolje resenje, posalji FIN paket da obavestis klijente da se server gasi
// https://github.com/hunterloftis/stoppable
za zero time deployments
-------------
// multistage builds
npm ci - cita package.lock
----
docker build -t myapp . // samo zadnji layer ostaje by default
docker build -t myapp:prod --target prod . // gadjaj layer koji hoces, target moze i u docker-compose.yml
-----
dev koristi source sa hosta - bind mount, ne kopira u kontejner
------------
//scanning - security audit, antivirus
npm audit --audit-level critical // critical flag da ne zaustavi build
-------------
// docker-compose.yml
sadrzaj node_modules nije isti na windowsu, linuxu i mac, arhitektura za binarne fajlove
-----
node_modules bind mount i named volume fora da mozes da imas node_modules za host i container istovremeno
------
// ode je interesantno 2 opcije...
1. opcija:
bind-mount /app sa sve node_modules
ne moze docker-compose up, mora prvo docker-compose run
node_modules se koristi samo u kontejneru // to, to, iako je na hostu
-----
2. opcija:
default node trazi node_modules u parent folderima do root
sa volume se sakrije node_modules sa hosta
------------
// ovo je vezano za Dockerfile, kako detektuje rebuild
npm install ne sme da se izvrsi osim ako nisi promenio package.json
----------
bind mount - u windows radi sporo preko sambe, na mac delegated write mode da radi brze
volumes:
    - .:app:delegated
-----------------
// file monitoring
nodemon - za windows mora enable pooling jer razlicit filesystem, da bi nodemon u kontejenru video promene na hostu, src
---------------
// deppends_on, d-c.yml v2, mozda zastarelo
node:
    depends_on:
        db:
            condition: service_healthy // eto

db:
    image: postgres
    healthcheck:
        test: /healthchecks/postgres-healthcheck // i eto
-----
healthchecks se koristi za zero time deployments
--------------------------
// interpolate env variable in docker-compose.yml
//important: no quotes, or quotes will be included literally
environment:
- DOKKU_HOSTNAME=dokku.${DOMAIN}
-------------------
// tag image with docker-compose build
// https://stackoverflow.com/questions/33816456/how-to-tag-docker-image-with-docker-compose
// postavis image prop, ok
image: nemanjamitic/mern-bolierplate-client:latest
---------------------------
// install docker 2 linije
// https://github.com/ChristianLempa/cheat-sheets/blob/main/docker/docker.md
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
----
sudo groupadd docker
sudo usermod -aG docker $USER
restart za apply user
docker version
-----------------
// install docker-compose
// https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-22-04
mkdir -p ~/.docker/cli-plugins/
curl -SL https://github.com/docker/compose/releases/download/v2.14.1/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
chmod +x ~/.docker/cli-plugins/docker-compose
docker compose version
-----------------
// install portainer
// https://docs.portainer.io/start/install-ce/server/docker/linux
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v portainer_data:/data portainer/portainer-ce:latest
// url
https://localhost:9443
----
// verzija ovde
https://hub.docker.com/r/portainer/portainer-ce/tags
docker pull portainer/portainer-ce:2.18.4
-----------------------------------
// ENTRYPOINT vs CMD - chatGPT
The CMD instruction specifies the (optional) default arguments for the ENTRYPOINT command
ENTRYPOINT zapravo pokrece process u containeru
----
verovatno je ENTRYPOINT bash pa onda sta hoces
------
// srpski chatgpt
ENTRYPOINT - starting program - ne moze override - uvek se izvrsava
CMD - argument - moze override
-------------------------
// stop all containers
docker stop $(docker ps -a -q)
// remove all containers
docker rm $(docker ps -a -q)
// remove all images
docker rmi $(docker images -a -q)
// remove all volumes
docker volume prune
// remove all unused data, networks? - da sklanja i unused networks by containers
docker system prune
// list networks
docker network ls
------------------
// mora oba ista porta zapazi, ne moze forward kao env var, daje random port
ports:
- '8080:8080'
- '8443:8443'
------------------------
// to avoid renaming networks
ako izostavis network u docker-compose.yml kreira network sa imenom folder-name_default
// rename network mora ovako
portainer-standalone:
    image: 'portainer/portainer-ce:2.17.0'
    networks:
    - default

networks:
    default:
        name: my-network-name // ovako ce biti imenovana u portaineru
---------
portainer ne sme na internal network, ne mozes na host:9443 da mu pristupis
-------
mreza se kreira i brise na d-c up/ d-c down
-------------------------
// linux disk usage
df -h --total
// listaj disk usage za docker
docker system df
// clear docker cache
docker builder prune
// clear dangling images
docker image prune
-------------------------
// dobar cheatsheet
https://github.com/wsargent/docker-cheat-sheet
--------------------------------
//  transfer a Docker image from your local machine to a VPS, bez Docker Hub, odlicno, chatGpt
// export to tar
docker save -o <image-name />.tar <image-name />
// copy to vps
scp <image-name>.tar user@your_server_ip:/path/to/destination
// load image from tar
docker load -i /path/to/<image-name />.tar
// verify loaded image
docker images
-------------
// za private image from docker hub mora docker login, user pass load from file
// https://stackoverflow.com/questions/30970591/automatic-docker-login-within-a-bash-script
cat ~/my_password.txt | docker login --username foo --password-stdin
--------
// free docker hub, private images
1 private repository, 1gb
----
// github container registry - neogranicen broj free privatnih images
// ghcr login
docker login ghcr.io -u USERNAME -p PERSONAL_ACCESS_TOKEN
// tag image, image-name space url/username/image-name
docker tag IMAGE_NAME:TAG ghcr.io/USERNAME/IMAGE_NAME:TAG
// push local image
docker push ghcr.io/USERNAME/IMAGE_NAME:TAG
--------------------------
// ports vs expose, d-c.yml
// https://stackoverflow.com/questions/40801772/what-is-the-difference-between-ports-and-expose-in-docker-compose
ports - exposes to both containers (services) and host
expose - only to other containers (services in d-c.yml)
ne pominje networks nesto
--------------------
// zasto chown i chmod zauzimaju mesto u slici
// https://stackoverflow.com/questions/58018658/why-does-chmod-cause-docker-build-to-run-out-of-disk-space
chmod - menja permisije na fajlu
chown - menja vlasnika
----
// pravi duple kopije fajlova
Filesystem layers in docker are implemented with a copy-on-write solution. 
Any change to a file results in a copy of that file first, and then the change is applied to that file copy, 
even a permission, owner, or timestamp.
----
// resenje:
mora u istoj liniji, cim kopiras fajl da ga modifikujes
COPY ... && RUN chmod ...
COPY --from=builder --chown=node:node /app/dist ./dist // npr
--------------------------
// iza run service ide command koja override CMD from Dockerfile, zapazi
docker compose run service command
docker compose run app alembic revision --autogenerate -m "your commit"
// exec je kad je container vec podignut, run je samo first command
docker compose -f docker-compose-dev.yml exec service command
-----------------
// d-c up --build vs --force-recreate 
// slike vs containeri, zapazi
// SLIKE, rebuild images kad se promeni package.json ili Dockerfile
docker-compose up --build
// CONTAINERI, prisilno zaustavi i rekreiraj kontejnere
docker-compose up --force-recreate
----------
// docker-compose.yml fajl moze da se zove kako hoces, samo -f
docker compose -f pgadmin.yml up
----------------------
// remove all dangling and unused Docker images // 1 line // good for github actions
docker image prune -af // all, force
----------------
docker-compose.override.yml je samo default merge za docker-compose.yml za docker compose up 
inace radi merge za sve fajlove koje navedes sa -f 
onda se  docker-compose.yml koristi kao base
//  A Docker Compose Override File Can Help Avoid Compose File Duplication - Nick Janetakis 
// https://www.youtube.com/watch?v=jGePPQFArwo
poenta: docker-compose.override.yml se ne mece u git, ide kao lokalna varijacija // to
ide docker-compose.override.example.yml u git, kao .env i .env.example 
docker-compose.override.yml mozei da extend i override postojece // zapazi



