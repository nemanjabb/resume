---------------------
// orange pi bootable sd card
ne radi sa gnome disks, samo sa dd // do toga je, zapazi
sudo dd if=~/Desktop/ubuntu-24.04-preinstalled-desktop-arm64-orangepi-3b.img of=/dev/sda bs=4M status=progress
sudo dd if=~/Desktop/Orangepi3b_1.0.6_ubuntu_jammy_desktop_xfce_linux5.10.160.img of=/dev/sdb bs=4M status=progress
// dd over network, ssh
// mora root user, orangepi default pass
dd if=~/Desktop/ubuntu-24.04-preinstalled-desktop-arm64-orangepi-3b.img | ssh root@192.168.1.132 "sudo dd of=/dev/mmcblk0 bs=4M status=progress"
-----------------
// backup emmc to usb drive, cela velicina emmc 256gb
// mount usb
sudo mount /dev/sda1 /mnt
// copy to image
sudo dd if=/dev/mmcblk0 of=/mnt/orangepi_emmc_backup.img bs=4M status=progress
// verify sha
sha256sum /dev/mmcblk0
sha256sum /mnt/orangepi_emmc_backup.img
// unmount usb
sudo umount /mnt
----------------
// ovo sutra, img samo koliko zauzeto
sudo partclone.ext4 -c -s /dev/mmcblk0p1 -o /mnt/orangepi_backup.img
--------
rdp ne radi bez hdmi ekrana
---------------------
// backup emmc as compressed image on external usb drive, chatGpt
// podesi prava imena diskova
// backup
// mount usb drive
sudo mount /dev/sdX1 /mnt/backup
// backup bootloader, first 4MB, mozda
sudo dd if=/dev/mmcblk1 of=/mnt/backup/bootloader_backup.img bs=1M count=4
// backup filesystem to .img
sudo partclone.ext4 -c -s /dev/mmcblk1p1 -o /mnt/backup/emmc_backup.img
----
// restore .img to emmc
// restore bootloader
sudo dd if=/mnt/backup/bootloader_backup.img of=/dev/mmcblk1 bs=1M
// restore main filesystem
sudo partclone.ext4 -r -s /mnt/backup/emmc_backup.img -o /dev/mmcblk1p1
// sync and reboot
sudo sync
sudo reboot
-----------
// moze i sa gzip i dd, samo used space, moze i rsync
// backup
sudo dd if=/dev/mmcblk1 bs=4M | gzip > /path/to/usb/backup_image.gz
// restore
gunzip -c /path/to/usb/backup_image.gz | sudo dd of=/dev/mmcblk1 bs=4M
------------------
// clone os from sd card to nvme // sporo 32gb umesto 8gb .img
lsblk
// unmount nvme
sudo umount /dev/nvme0n1*
// clone
sudo dd if=/dev/mmcblk1 of=/dev/nvme0n1 bs=4M status=progress
// ensure all is written
sudo sync
// resize nvme partition with gparted, nvme veci od sd card
// change boot configuration, bez emmc with os probably
Install/Update the bootloader on SPI Flash
------------
uuid u orangepi-config treba da pokazuje na root partition na nvme/emmc // tacno root, ne boot
-----------------
// clear disk 
// taj, trenutno
sudo wipefs -a /dev/mmcblk0
// drop partitions rucno
sudo fdisk /dev/mmcblk0
d pa Enter, broj if 1+ particija po particija
w pa Enter, write changes
// sporo, svih 256gb
sudo dd if=/dev/zero of=/dev/mmcblk0 bs=1M status=progress
--------
// flash .img to remote // radi, tested, root user
dd if=~/Desktop/Orangepi3b_1.0.6_ubuntu_jammy_desktop_xfce_linux5.10.160.img | ssh root@192.168.1.127 "sudo dd of=/dev/mmcblk0 bs=4M status=progress"
-------
// create username user and group
sudo adduser username
// add to sudo group
sudo usermod -aG sudo username
---------
// copy files, radi quotes for space
scp "/media/username/data/orange pi 3b/traefik-proxy-pi.zip" pi:/home/orangepi/Desktop/
// decompress 
unzip traefik-proxy-pi.zip
// move and rename
mv traefik-proxy ~/traefik-proxy-renamed
// bez rename
mv traefik-proxy ~/
--------------
// replace hawei with ubuntu
sudo nano /etc/apt/sources.list
deb http://ports.ubuntu.com/ubuntu-ports/ jammy-backports main restricted unive>
#deb-src http://repo.huaweicloud.com/ubuntu-ports/ jammy-backports main restric>
--------------
// install dcoker compose plugin on outdated docker
docker 
sudo apt install -y docker-compose-plugin
------
docker --version
Docker version 26.1.0, build 9714adc // 27.3.1, build ce12230 lokalno ubuntu
docker compose version
Docker Compose version v2.29.1


