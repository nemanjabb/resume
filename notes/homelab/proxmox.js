// configure wifi interface on proxmox
// How to Add A New Network Interface (NIC) to Proxmox - Barmine Tech
// https://www.youtube.com/watch?v=oiAHMcr-Q3g
// https://github.com/CarmineCodes/Adding-a-Network-Card-to-Proxmox
 bridge wifi with ethernet interface_
-------
// backup file
cp /etc/network/interfaces /etc/network/interfaces.backup
--------
// set wifi password for wifi bridge
// https://x88.in/proxmox-with-wifi/ - ovo NE radi
iface wlp2s0 inet manual
        wpa-essid WIFI-SSId
        wpa-psk WIFI-PASSWORD
----
// ovo ne radi takodje, ne dodeljuje 192.168.1.101 na wifi
auto wlp2s0
iface wlp2s0 inet manual
        wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
-----
// install wpasupplicant za connect wifi sa ssid pass
apt update
apt install wpasupplicant wireless-tools
----------
// staticka IP se dodeljuje SAMO na virtual bridge interface, ne na fizicki interface // zapazi, mnogo vazno
// a virtual bridge se binduje na fizicki port - karticu
root@pve:~# cat /etc/network/interfaces
auto lo
iface lo inet loopback

iface eno1 inet manual

auto vmbr0
iface vmbr0 inet static
        address 192.168.1.100/24
        gateway 192.168.1.1
        bridge-ports eno1
        bridge-stp off
        bridge-fd 0

auto wlp2s0
iface wlp2s0 inet manual
        wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

auto vmbr1
iface vmbr1 inet static
        address 192.168.1.101/24
        bridge-ports wlp2s0
        bridge-stp off
        bridge-fd 0

--------------
// da li je konektovan wifi mreza, pass
wpa_cli -i wlp2s0 status
wpa_state=COMPLETED // ovo znaci jeste
wpa_state=INTERFACE_DISABLED // za disconnect
-------------
// pali/gasi karticu
ip link set wlp2s0 down
ip link set wlp2s0 up
// connect manually
wpa_supplicant -B -i wlp2s0 -c /etc/wpa_supplicant/wpa_supplicant.conf
----------
// scan
wpa_cli -i wlp2s0 scan
// list networks after scan
wpa_cli -i wlp2s0 scan_results
-------------------
// check if virtaul bridge is up
ip link show vmbr1
// start vbridge
ip link set vmbr1 up
----------------
virtual bridge - softverski network interface_ kome se dodeli port - hardwerski interface_, apstrakcija 
bridge interfaces - povezati ih 
bond interfaces - da rade paralelno ili fallback 
-------------------
// test dns, when apt update doenst work
ping -c 4 google.com
------------
// postavi dns server
cat /etc/resolv.conf
// tu meces gateway adresu
nameserver 192.168.1.1
// ili googlovi dns serveri
nameserver 8.8.8.8
nameserver 8.8.4.4
-----------------------
-----------------------
// lxc - linux containers
lxc containers cuvaju fajlove posle gasenja, vise lice na virtuelne masine
---------
// Creating Container Templates - Learn Linux TV
// https://www.youtube.com/watch?v=J29onrRqE_I
// priprema postojeceg kontejnera za kreiranje templatea
kontejner vec ima usera koji se ne zove root da bi mogao da se loguje sa ssh
----
sudo apt update && sudo apt dist-upgrade
// save space, apt cache
sudo apt clean && sudo apt autoremove
----
// remove ssh keys from template, da budu unique za druge kontejnere
cd /etc/ssh
sudo rm ssh_host_*
// sad ne moze login sa ssh, drzi sesiju
----
// isprazni machine-id, da bude unique
sudo truncate -s 0 /etc/machine-id
----
// to je to, power off
sudo poweroff
------
// desni klik -> convert to template
-------
// desni klik na template -> clone
opcije -> full clone, id, hostname lxc1 npr, local-lvm
------
// pokreni i ssh u container
ssh my-user@ip 
---
// reset ssh keys, delete i recreate
// opet delete
cd /etc/ssh
sudo rm ssh_host_*
// regenerate ssh keys
sudo dpkg-reconfigure openssh-server
-------------------------
// docker u lxc
// Docker in Proxmox LXC with Turnkey Core - Noted - lose ovo, zanemari
// https://www.youtube.com/watch?v=79KiCBNbsbg
za docker lxc mora privileged, odcekiraj unprivileged checkbox // ovo je pogresno, vecina unprivileged
kreiraj bez pokretanja
options -> features (dupli klik) -> enable nesting
start container, user je root, ne treba sudo, kao u proxmox
apt update && apt upgrade
install and enable docker 
------------------------
// Docker on Proxmox LXC Zero Bloat and Pure Performance! - SmartHomeBeginner
// https://www.youtube.com/watch?v=-ZSQdJ62r-Q
// https://www.smarthomebeginner.com/ultimate-docker-server-1-os-preparation/
ostavio unprivileged // ovo
enable nesting 
static address 192../24, gateway
----
// create non root user 
sudo adduser username
// add u sudo group za sudo
sudo adduser username sudo
// exit i login
// logout
exit
-----
sudo apt update && sudo apt upgrade
-----
// change ssh port from 22
nano /etc/ssh/sshd_config
port 2053 // u fajl
sudo systemctl restart sshd
------
// install usual packages, opcionalno
sudo apt install ca-certificates curl gnupg lsb-release ntp htop zip unzip gnupg apt-transport-https ca-certificates net-tools ncdu apache2-utils
// firewall zabrani sav dolazni saobracaj
sudo ufw default deny incoming
sudo ufw default allow outgoing
// enablira ssh port za local mrezu, za spolja otvori samo ssh port
sudo ufw allow from 192.168.1.0/24 
// enable firewall
sudo ufw enable
-------------
// install docker sa 1 linija convenience script 
// https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script
// download script
curl -fsSL https://get.docker.com -o get-docker.sh
// make executable
sudo chmod +x get-docker.sh
// run install script
sudo sh get-docker.sh
// ovo instalira i docker compose // ok
// add user to docker group, da radi docker bez sudo
sudo usermod -aG docker $USER 
// logout
exit
// listaj grupe current usera
groups 

------------------
// za ping mora sudo 
sudo ping google.com 
sudo ping 192.168.1.1 
----------------
// bezveze 22.10 nije LTS, 22.04 je LTS // zapazi
// instaliras 22.04 i to je to
// lxc ubuntu 22.10 nece sudo apt update and upgrade
// https://help.ubuntu.com/community/EOLUpgrades#Upgrading
// promeni archive sa old-releases u svim linkovima
nano /etc/apt/sources.list
deb http://archive.ubuntu.com/ubuntu kinetic main restricted universe multiverse // pre
deb http://old-releases.ubuntu.com/ubuntu kinetic main restricted universe multiverse // posle, radi
--------------------------
// rathole
ovo je bila glavna fora, ne exposujes nista na clientu u docker-compose.yml nego 
rathole client direktno postavi na nginx port, on zna sam sa servera na kom portu stize, kontrolni kanal mu reko
// rathole.client.toml
[client.services.nginx]
token = "my-secret"
local_addr = "nginx-with-volume:8080" // to, docker-service.port
--------
local_addr = "nginx-with-volume:5080" // ovo NE, salje na istom portu sa koga prima, prvo ovo uradio, jedan dan izgubio na ovo
---------
// TO JE BILO NAJVAZNIJE
// rathole.client.toml
[client.services.traefik-http]
token = "my-secret"
local_addr = "traefik:80"

[client.services.traefik-https]
token = "my-secret"
local_addr = "traefik:443"  
// a traefik ne otvara nikakve portove u docker-compose.yml
// rathole will pass traffic through proxy network directly on 80 and 443
// defined in rathole.client.toml
// ports: <- OVO NE
//   - '80:80'
//   - '443:443'
--------
ne sme rathole client da salje na port sa koga mu stize, 5080
rathole clent zna sa kojih portova mu stize saobracaj, ne treba na clientu portovi da se postavljaju
samo controll channel port // to
---------
rathole server works without external network in docker-compose.yml 
------------------
//  How to setup Turnkey Linux Fileserver on Proxmox VE - ElectronicsWizardry
// https://www.youtube.com/watch?v=UnXxJMjW4LE
turnkey-fileserver lxc template
1 cpu, 512mb ram, ip/24
root pass (for webin), samba root pass (for http auth)
non-root linux user, group
create samba user from linux user, and pass 
samba - share permissions and file permissions 
fileshare - ownership and permissions
fileshare dalje - name (kako da se pojavi), path, 777, username1 ownership
security and access - writable, guests yes 
restart samba (ne container, nego samo samba service), za bilo koji config edit to apply
smb://ip
// webmin on, ip enough
https://192.168.1.121:12321
-------
// Configuring Storage in ProxMox - H2DC - How to do Compute
// https://www.youtube.com/watch?v=HqOGeqT-SCA&list=PLk3oVaFzBUufFbrE4Y0gnQcjzfmEmT93o&index=4
1. podsetnik format disk i kao directory u proxmox
2. add cifs, nfs share - datacenter -> add cifs 
to je za iso, proxmox, ja smb pristupam iz masina i kontejnera, terminal 
------------------------
// mount samba folder from terminal // ovo radi, probao
sudo apt install cifs-utils
// check if installed
apt list --installed | grep cifs-utils
// ovaj ne treba, nista ne znaci
sudo apt install smbclient
-----
// ovo je samo kao login mozda
smbclient //192.168.1.121/seagate2tb -U username1
-----------
// mount samba share na lokalni folder
mkdir ~/Desktop/smb-seagate2tb/ // moze -p &&, nuje komentar ispod
sudo mount -t cifs -o username=username1,uid=1000,gid=1000 //192.168.1.121/seagate2tb ~/Desktop/smb-seagate2tb/
-------
// ovo je vazno da nautilus ne mountuje as user root, group root
uid=1000,gid=1000 // radi
// user and group for local nautilus
id -u
id -g
// linux user on remote cifs server
// linux user kreiran za turnkey sambu kroz webmin, username1, group1
username=username1
---------
// list linux users
cut -d: -f1 /etc/passwd
cat /etc/passwd
---------
// test speed iperf
sudo apt install iperf
// run server on port 5001
iperf -s
// run client
iperf -c 192.168.1.121
---------
// dodaj u /etc/fstab (mounts on boot)
//192.168.1.121/seagate2tb ~/Desktop/smb-seagate2tb cifs username=username1,password=1,uid=1000,gid=1000 0 0
----------
// unmount //ok, radi
sudo umount -l ~/Desktop/smb-seagate2tb/
-------------------
// iz unprivileged lxc containera ne moze direktno mount
// https://forum.proxmox.com/threads/mount-error-1-operation-not-permitted.126898/
to access your CIFS share from within an unprivileged container, mount the CIFS share 
on the PVE host and use a bind-mount to access it in the container
------------------
// ssh u proxmox
ne mora monitor za upgrade i slicno, ne more ssh key
ssh root@192.168.1.100 // pass za root usera
---------------------
// rathole vs wireguard, cgnat bypass
wireguard - cela mreza (budu u lokalnoj mrezi), svi servisi - network layer (Layer 3), encapsulating IP packets
rathole - jedan servis (port) - application layer (Layer 7), exposing specific services
------
znaci wireguard bez subneta nije dobar za rdp jer ce i lokalni laptop ici preko vps // nije, biras ip:port kojim ces da se konektujes
------
wireguard - selektujes ip adrese (ili range) - subnet 
rathole - selektujes port
----------------
// tailscale
// Tailscale VPN - WireGuard was never so easy! - Christian Lempa
// https://www.youtube.com/watch?v=Kzyolu9yn0E
mash network - svaki sa svakim, poseban tunnel za svaka 2
star network - vpn gateway u centru, svi moraju preko njega
--------
tunnel - private, public key, ip adrese
------
instaliras tailscale client na svakom, i login with google 
p2p zapravo (clienti su client i server istovremeno)
100 devices, free plan
----------------
// wireguard docker
// Create your own VPN server with WireGuard in Docker - Christian Lempa
// https://www.youtube.com/watch?v=GZRTnP4lyuo
// moje beleske sve
// https://github.com/nemanjam/homelab/tree/main/wireguard-server
// server
wireguard u dockeru zahteva kernel modul na hostu
moze bilo koji port
sve su env vars u d-c.yml
tera i dns server u containeru PEERDNS=auto, CoreDNS server
------
// client
// peer1.conf
AllowerdIPs: - subnet klijenta koji da ide kroz tunnel, 0.0.0.0/0 za sve
client se instalira direktno na hostu klijenta, bez dockera
------
dodavanje novog clienta, edit d-c.yml PEERS=2 i recreate container
docker compose up -d --force-recreate
-------------------
// shared clipboard with VMs
resenje: use SPICE with virt-viewer, odlicna latencija
// Spice with Ubuntu - virtualize everything
// https://www.youtube.com/watch?v=Jw3qQb_38SU
sudo apt install virt-viewer // na laptopu
display - spice, 128, machine q35
console -> fajl, otvaras sa virt-viewer 
-----------------
// install wireguard gui - Wire GUI
https://github.com/Devsfy/wiregui
sudo apt install ./wiregui_1.8.1_amd64.deb
// fix for ubuntu 24
// https://github.com/Devsfy/wiregui/issues/35#issuecomment-1879635517
wiregui --no-sandbox %U
// ili za shortcut
// /usr/share/applications/wiregui.desktop
Exec=wiregui --no-sandbox %U
--------------
zapravo u ubuntu 24 wiregurad ima u ubuntu settings // TO
--------------
// ovo je najjednostavnije, iz terminala
// install client and dns for wg-quick
sudo apt install wireguard resolvconf
// copy as wg0.conf to client (run this from client terminal)
scp ubuntu@amd1:/opt/wireguard-server/config/peer1/peer1.conf  /etc/wireguard/wg0.conf
// run Wireguard on client
sudo wg-quick up wg0
// shut down
sudo wg-quick down wg0
// check if client is connected
sudo wg
--------------------
// ovo fali za rdp
// On the VPS, set up a NAT rule to allow traffic from the remote RDP client to reach the Ubuntu machine
sudo iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -o eth0 -j MASQUERADE
------
obe masine moraju da budu klijenti do wireguard servera na vps, i laptop i rdp server, ne samo rdp server
onda su svi u lokalnoj mrezi // zapazi, probaj
-------------
// remote access, rdp to CGNAT, chatGpt
1. TeamViewer, AnyDesk
2. VPN with a Cloud Server // ovo sto ja pokusavam sa wg
3. Using SSH Tunneling with a Public Server // odlicno
// ssh tunnel, from server
ssh -R 3389:localhost:3389 user@public-server-ip
// rdp from laptop
ssh -L 3389:localhost:3389 user@public-server-ip
4.  Using a Reverse Proxy with a Cloud Server
ssh + nginx, nginx forwards rdp // bzvz
----------------
// wireguard rdp, radi
fora je da su svi peer-ovi u lokalnoj mrezi, u peer.conf im je dodeljena lokalna adresa
i onda mozes da ih gadjas medjusobno na taj lokalni ip
Address = 10.13.13.1 // server
Address = 10.13.13.2 // peer1.conf
Address = 10.13.13.3 // peer2.conf
----
vidi se interface_ i ip sa ifconfig // zapazi
-----------------------
// portainer, docker-compose.yml
// portainer run 
// https://docs.portainer.io/start/install-ce/server/docker/linux

docker run -d \
  -p 8000:8000 \
  -p 9443:9443 \
  --name portainer \
  --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v portainer_data:/data \
  portainer/portainer-ce:latest

----
// https://earthly.dev/blog/portainer-for-docker-container-management
// docker-compose.yml
version: '3.8'

services:
  portainer:
    image: portainer/portainer-ce:latest
    container_name: portainer
    restart: unless-stopped
    ports:
      - "9000:8000"
      - "9443:9443"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer_data:/data

volumes:
  portainer_data:
------------------
// amd2 no route to host, nedostupan portainer, *.local.nemanjamitic.com
sudo iptables -F // nije pomoglo
mora restart servera 
firewal port zatvoren, mozda samo docker compose down up 
--------------------------
// VM templates for dev Ubuntu
// Proxmox VE Full Course: Class 6 - Creating Virtual Machine Templates - Learn Linux TV
// ovo je sve za Ubuntu
1. nadji ili instaliraj cloud-init
apt search cloud-init 
// ako nema [installed]
sudo apt install cloud-init 
2. clear host ssh keys 
cd /etc/ssh // zapazi stazu, drugacija
host keys pocinju sa ssh_host_*
// delete ssh host keys
sudo rm ssh_host*
3. empty machine-id file 
cat /etc/machine-id
sudo truncate -s 0 /etc/machine-id
3a. symlink na ovaj fajl mora da postoji i da je symlink ili da se kreira
ls -l /var/lib/dbus/machine-id
// l - symlink, -> na koji fajl symlink pokazuje
lrwxrwxrwx 1 root root 15 дец 30  2022 /var/lib/dbus/machine-id -> /etc/machine-id
// kreiraj symlink ako ne postoji
sudo ln -s /etc/machine-id /var/lib/dbus/machine-id
4. clean template 
sudo apt clean 
sudo apt autoremove
5. instaliraj zajednicki software // zapazi
vs code, git, qemu agent, docker
6. poweroff, desni klik -> covert to template, tad 
7. remove cd drive, add cloud init drive 
set username, password, ssh public key opcionalno, klik regenerate image
8. desni klik -> create clone
full clone, target storage, name vm-2
9. promeni hostname // to, trivial
// promeni hostname (computer5) u ovim fajlovima
sudo nano /etc/hostname 
sudo nano /etc/hosts
-----------------
// Perfect Proxmox Template with Cloud Image and Cloud Init - Techno Tim
cloud-init je kao cookiecutter da postavis argumente u linix slici pre nego je kreiras // zapazi
cloudimage u imenu ubuntua za download
komplikovane skripte, ne ovaj klip
----------------
// android x86 qemu quest video resolution
//  Change Screen Size of Android X86 Virtualbox VM - Linux Leech
// https://www.youtube.com/watch?v=VHwOyrWodS4
1. u boot menu izaberes 2. opciju - debug mode 
2. remount da mozes da edit boot menu
mount -o remount,rw /mnt
3. cd /mnt/grub
4. ls 
5. vi menu.lst , i - insert mode, esc, :wq 
6. vga=917 (decimal) ili vga=ask i "scan" (opcionalno) da vidis dostupne rezolucije
7. reboot -f 
------
1600x900x32 395 hex = 917 decimal // taj
1920x1080x32 392 hex = 914 decimal
ispisani su hex
u izbornom ekranu se stavlja hex vrednost, a samo u ask=[decimal]
-------------
// gasis android iz termux terminala
reboot -p 
-----------------
// generate ssh keys on location, radi, i permissije su tacne
ssh-keygen -t ed25519 -C "myemail@gmail.com" -f ~/.ssh/homelab/ubuntu24pve_template__id_ed25519
------------------
// copy qcow2 from samba to vm disk
// mount samba and copy
mkdir /mnt/my-samba-share
mount -t cifs -o username=username1,uid=1000,gid=1000 //192.168.1.121/seagate2tb/sa-crucial1tb/homelab/qemu-vms /mnt/my-samba-share
// check mount
ls /mnt/my-samba-share
// create vm disk folder, 108 vm ID
mkdir /var/lib/vz/images/108
// copy from samba to qcow temp folder za diskove, NE gde je destination machine
// pa ga odatele importuj, on ce da ga stavi u vm folder
cp /mnt/my-samba-share/win10-blank.qcow2 /var/lib/vz/template/qcow/win10-blank.qcow2
-------
// ne boota ovo...
// import qcow in win vm
// https://www.vinchin.com/vm-backup/import-qcow2-proxmox.html
// print config
qm config 108
// import disk, creates raw file..., for what?
// mogao sam iz samba share direktno
qm importdisk 108 /var/lib/vz/template/qcow/win10-blank.qcow2 local
// to kreira raw fajl
/var/lib/vz/images/108/vm-108-disk-0.raw
// attach qcow2 disk
qm set 108 --scsi0 local:108/vm-108-disk-0.raw
// change boot order
disk on top, deselect others
-------------------
// install windows in proxmox
// Launching a Windows VM in Proxmox - Learn Linux TV
// https://www.youtube.com/watch?v=eyNlGAzf-L4
mora virtio.iso disk sa drajverima da bi 
1. detektovao disk 
2. install drajvere u device manager 
3. qemu agent 
-------------------------
// astrometa firmware koji radi
// http://palosaari.fi/linux/v4l-dvb/firmware/MN88473/01/2016-02-07/
// https://forums.openpli.org/topic/35710-astrometa-dvb-tt2c-mn88472mn88473/page-6
// https://forum.libreelec.tv/thread/11553-astrometa-t2hybrid/
// location
ls -la /lib/firmware/dvb-demod-mn88473-01.fw
-rw-r--r-- 1 root root 2271 Oct 12 19:52 /lib/firmware/dvb-demod-mn88473-01.fw
// scan
w_scan -ft -c HR -L
// objasnjen video, vlc
https://www.youtube.com/watch?v=asmCMmq06R0&list=LL&index=9
// napravi vlc listu
w_scan -ft -c HR -L > ~/Desktop/channels.conf
// run in vlc
vlc ~/Desktop/channels.conf
----------------

