---------------
// vs code
// files to include, drugo polje, ne glavno
npr trazis sve functionOne u FolderOne folderu koji nije u root
vraca linije, a ne fajlove
*/Fold*
// case sensitive jbg, i * na kraju
**? u path
--------------------
// recommended extensions vs code
add list in `.vscode/extensions.json`
{
    "recommendations": [
      "bradlc.vscode-tailwindcss",
      "PKief.material-icon-theme",
      "dsznajder.es7-react-js-snippets",
      "esbenp.prettier-vscode",
      "Prisma.prisma",
      "stylelint.vscode-stylelint"
    ]
  }
//list extensions:
code --list-extensions
--------------------------
// eto F1 najlakse
VS Code command palette (Ctrl/Cmd + Shift + P or F1)
---------------------------
// extenzije
// diff za slicne fajlove
ryu1kn.partial-diff
--------
// git log - git graph
mhutchie.git-graph
// probaj i ovu za git
donjayamanne.githistory
---------------------------
// VAZNI SHORTCUTI
// select multiple lines
Shift + up or down arrows // jbt prosto
// na pocetak linije
Home key - na elitebook Fn + left arrow  = Home
// na kraj linije
End key // logicno
// select line
Ctrl + L // odlicno
// delete line
Ctrl + Shift + K
// duplicate line - copy line up/down // resi ovo
Ctrl + Alt + Shift + up/down // ne radi na ubuntu
// selectuje sve pojave reci
Ctrl + F2 // radi
--------
// za duplicate line // eto
ne diras ubuntu settings
u vs code prvo remove postojeci na Alt + Shift + up/down
pa edituj Ctrl + Alt + Shift + up/down na Alt + Shift + up/down
redosled zapravo nije bitan Alt + Shift = Shif + Alt
---------
// shortcuts za 2 ekrana // odlicni
Super = Win key
Super + up // maximize
Super + down // srednja velicina
Super + left/right // leva, desna polovina, za pozive 
Shift + Super + left/right // levi/desni ekran // odlicno
-----------------------------
// obavezno, jednom za uvek, args i format comments
docs i comments extenzija, vs code,
-----------------------------
// dobre extenzije, lista:
aaron-bond.better-comments
// docblocks args
jeremyljackson.vs-docblock
// comment dividers, dobar
// Alt + X, Alt + Shift + X
stackbreak.comment-divider
// bolji mnogo, headers, dividers, fn, about
akmarnafi.comment-headers
// diffs
ryu1kn.partial-diff
// spellchecker
streetsidesoftware.code-spell-checker
// sql client
mtxr.sqltools
mtxr.sqltools-driver-pg
// postman, HTTP, WebSockets, and gRPC
Postman.postman-for-vscode
// pojednostavljene typescript errors, nije lose
yoavbls.pretty-ts-errors
// github actions
GitHub.vscode-github-actions
// back forward button visual studio
grimmer.vscode-back-forward-button
// lorem ipsum, f1, text, paragraph, page
tyriar.lorem-ipsum
// jednostavnije typescript greske
yoavbls.pretty-ts-errors
// ovaj je najmocnija za git, 29 miliona
// vazan jer otkriva puno opcija gita
eamodio.gitlens
// ovo da disable current line in editor, .vscode/settings.json
"gitlens.currentLine.enabled": false
--------
// code bookmarks, obavezna // odlicna
alefragnani.bookmarks
F1 -> boo li
Ctrl + Alt + L - go to next // manje vise
bookmarks ima u sidebar // <- TO
---------------
// for lit
runem.lit-plugin
-----
// brad traversy nabrojao, mozda u komentarima jos
https://www.youtube.com/watch?v=bBTgcO6CKv0
--------
// mermaid - markdown uml dijagrami, za baze npr. // zapazi, odlican
vstirbu.vscode-mermaid-preview 
------------
// rest client to send from code, curl, fetch, axios
// moze iz bilo kog fajla, samo selektujes kod, vrh
humao.rest-client
// i ovaj mozda
rangav.vscode-thunder-client
-----------------
// vs code profiles - configurations
https://dev.to/joshuamyers/vscode-profiles-optimize-your-coding-environment-m2a
-----------------
// october 2023
// code --list-extensions
aaron-bond.better-comments
akmarnafi.comment-headers
bradlc.vscode-tailwindcss
csstools.postcss
dsznajder.es7-react-js-snippets
esbenp.prettier-vscode
flydreame.docblocker
jeremyljackson.vs-docblock
mhutchie.git-graph
mikestead.dotenv
ms-azuretools.vscode-docker
ms-python.black-formatter
ms-python.isort
ms-python.python
ms-python.vscode-pylance
ms-toolsai.jupyter
ms-toolsai.jupyter-keymap
ms-toolsai.jupyter-renderers
ms-toolsai.vscode-jupyter-cell-tags
ms-toolsai.vscode-jupyter-slideshow
ms-vscode-remote.remote-containers
mtxr.sqltools
mtxr.sqltools-driver-pg
mtxr.sqltools-driver-sqlite
nick-rudenko.back-n-forth
Prisma.prisma
ryu1kn.partial-diff
streetsidesoftware.code-spell-checker
stylelint.vscode-stylelint
tyriar.lorem-ipsum
------------------------
obavezno koristi outline na desno za pregled klasa i fja u vs code // zapamti ovo
files -> views -> open view -> outline
------------------------
resi shortcuts za push all i double line
--------------------
// akmarnafi.comment-headers, u praksi
sm, md, lg, xl, xxl
/ -------
// ========
------------------
/divider-lg, 
/*-------------------------------- onUrlChange ------------------------------*/
------------------
/header-lg
/**------------------------------------------------------------------------
 *                           SECTION HEADER
 *------------------------------------------------------------------------**/
------------------
/block-lg
/**------------------------------------------------------------------------
 *                             COMMENT BLOCK
 *  
 *  
 *  
 *  
 *------------------------------------------------------------------------**/
-------------------
/function_-block-lg
/**========================================================================
 **                           FUNCTION NAME
 *?  What does it do?
 *@param name type  
 *@param name type  
 *@return type
 *========================================================================**/
 -------------------
/end-lg
/*---------------------------- END OF SECTION ----------------------------*/
------------------
// .prettierignore whitelist // dobra fora
// https://github.dev/satnaing/astro-paper
# Ignore everything
/_*  // makni _

# Except these files & folders
!/src
!/public
!/.github
-----------------------
// file explorer exclude files, node_modules, dist...
// search exclude files
// 1. User settings scope
vs code node_modules exclude from user tab
File -> Preferences -> Settings -> User tab -> Files: Exclude
**/node_modules
// 2. in local settings.json
// file explorer
// applies to search too // zapazi
"files.exclude": {
  "**/.git": true,
  "**/.svn": true,
  "**/.hg": true,
  "**/CVS": true,
  "**/.DS_Store": true
}
// exclude from search
"search.exclude": {
  "**/node_modules": true,
  "**/bower_components": true
},
--------
.eslintrc.js radi relativno po folderima, kao .gitignore // zapazi
------------
// vs code copilot
github.copilot
github.copilot-chat 
---------------------
// prettier ignore line, block, file
// https://prettier.io/docs/en/ignore.html
----
// line
// komentar za jezik, js, html, md, yml...
// prettier-ignore
----
// block
// html, markdown
<!-- prettier-ignore-start -->
block
<!-- prettier-ignore-end -->
----
// file
.prettierignore u root 
---------------------
// cspell ignore comments
// https://cspell.org/configuration/document-settings/
-----------------
// format json
eriklynd.json-tools
ctrl + alt + M // format
alt + M // minify
-------
// inspect and navigate json as tree in sidebar
zainchen.json
