// events
postCreateCommand - after the container is built (only once)
postStartCommand - every time the container starts - startup tasks
postAttachCommand - when you attach (connect) to the container, interactive, on VS Code attach 
------
// forward env vars from host to container
// localEnv - devcontainers keyword
"containerEnv": {
  "MY_ENV_VAR": "${localEnv:HOST_ENV_VAR}"
}
--------
// features - package + configuration, versioned
"features": { ... }
--------
// port forwards
"forwardPorts": [3000], // expose container:3000 to host // 3000:3000 default
"appPort": ["3000:3000"] // isto, mapping, oba porta mozes da specificiras
--------
// direnv - postavlja env vars za folder, cita .envrc file // moze za moje projekte
sudo apt install direnv
kad udjes u folder ucita env vars iz .envrc, kad izadjes iz foldera skloni ih // zapazi 

