# chatGpt cheat sheet
# poenta: isto ko yarn za node.js

# Installation: Install Poetry package manager
pip install poetry

# Project Setup: Create a new Poetry project and navigate to it
poetry new project_name && cd project_name

# Dependency Management: Add, remove, update, install, lock, or display dependencies
poetry add package_name  # Add a package
poetry remove package_name  # Remove a package
poetry update  # Update dependencies
poetry install  # Install dependencies
poetry lock  # Generate a lock file
poetry show  # listaj instalirane pakete

# Virtual Environment: Set up and activate a virtual environment
poetry env use python3.x  # Set up virtual environment
poetry shell  # Activate the virtual environment
exit  # Exit the virtual environment

# Building & Publishing: Build and publish your project to PyPI
poetry build  # Build the project
poetry publish --build  # Publish to PyPI

# Testing: Run tests using pytest
poetry run pytest  # Run tests

# Versioning: Set a new version for your project
poetry version <version_number>  # Set version

# Additional Commands and Help: Get more information on Poetry commands
poetry --help  # Display help and additional commands
-------------
# Print active environment
poetry env info
# fora, da ide prvo install pa activate, jer poetry kreira environment onInstall by default
poetry install 
# activate
poetry shell

# install global dependency
poetry global add <package-name>
# list global dependencies
poetry global list
---------------
# primer poetry scripts, kao yarn
# pyproject.toml
[tool.poetry.scripts]
start = "my_project.main:start_server"
migrate = "my_project.db:migrate"
# pokretanje
poetry run start
poetry run migrate
-----------
# pokretanje app bez poetry scripts section
# poetry run mora umesto bash da bi pokretao komand u envrionment, a ne globalno
poetry run python main.py
poetry run uvicorn app.main:app --reload
poetry run python scripts/start_app.py
---------------
# in Dockerfile
# prod dependencies 
# main group - to je samo [tool.poetry.dependencies] cvor
# --no-root: Installs the dependencies but excludes installing the current project itself as a package
poetry install --no-root --only main
# i dev dependencies
poetry install --no-root
-------------------
-------------------
# How to Create and Use Virtual Environments in Python With Poetry - ArjanCodes
# https://www.youtube.com/watch?v=0f3moPe_bhk
venv - official since 3.3, most standard, samo za virtual environment, no dependency, pip
virutalenv - 3rd party, najstariji tool, ukljucen u venv 
pyenv - for python versions
pipenv - pip + virtualenv + pyenv
conda - data science, python + non python packages
--------
poetry - dependency + venvs + publish packages to pypy, lock 
--------
# install
pip install poetry
# poetry init
pyproject.toml
# instalira pakete i kreira venv, zapazi
poetry install  
# all info, python path, version, vurtualenv
# OVO se cesto koristi da proveris da jesi u venv
poetry env info 
# only virtualenv path
poetry env info -p 
-----
# set local venv folder in config, za poetry install # NAJVAZNIJE
poetry config virtualenvs.in-project true
# evo gde je default venv u ubuntu
/home/username/.cache/pypoetry/virtualenvs/
/home/username/.cache/pypoetry/virtualenvs/poetry-test-ZSvU5Wn_-py3.12/bin/python
# posle je local
/home/username/Desktop/poetry-test/.venv
------
# activate env, run code in venv
poetry shell
# add package, dok je shell activan
poetry add requests
# exit shell
exit 
# which venvs are active
poetry env list 
# exit shell and deactivate venv
deactivate
# publish packages, manje vazno za sad
install kreira venv, shell ga aktivira # eto, glavna fora
poetry dodaje dependencies u pyprojects.toml, pip ne, mora freeze > requirements.txt
------
neki paket zahteva OS pakete u C 
----------------------
# Python Poetry in 8 Minutes - ArjanCodes
# https://www.youtube.com/watch?v=Ji2XDxmXSOM
# listaj instalirane pakete
poetry show  
poetry show requests # info o paketu  
# add specific versions
major.minor.patch
poetry add requests@2.12.1 # tacna verzija
poetry add requests^2.12.1 # major tacna, ostale latest, default
poetry add requests~2.12.1 # minor tacna, ostale latest
-----------
# escape current package folder warning, kada je tvoj projekat paket
poetry install --no-root 
u pyproject.toml[tool.poetry].name koji je definisan, njega escapeuje # ok, eto


