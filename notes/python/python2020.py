# Python Crash Course For Beginners - Brad Traversy
# https://www.youtube.com/watch?v=JJmcL1N2KQs

'''
This is a 
multiline comment
or docstring (used to define a functions purpose)
can be single or double quotes
'''
moze i " i ' za string 
-------
# Arguments by position
print('My name is {name} and I am {age}'.format(name=name, age=age))

# F-Strings (3.6+)
print(f'Hello, my name is {name} and I am {age}')
--------
len(nesto), type(nesto), True, del undefinise, not(uslov)
: pa indentacija je blok {}
--------
# sortirano polje
# A List is a collection which is ordered, indexed and changeable. Allows duplicate members.

# Create list
numbers = [1, 2, 3, 4, 5]
fruits = ['Apples', 'Oranges', 'Grapes', 'Pears']

# Use a constructor
# numbers2 = list((1, 2, 3, 4, 5))
---------
# fiksna duzina i elementi, const polje prakticno
# A Tuple is a collection which is ordered, indexed and unchangeable. Allows duplicate members.

# Create tuple
fruits = ('Apples', 'Oranges', 'Grapes')

# Using a constructor
fruits2 = tuple(('Apples', 'Oranges', 'Grapes'))

--------
# skup iz diskretne
# A Set is a collection which is unordered, changable and unindexed. No duplicate members.

# Create set
fruits_set = {'Apples', 'Oranges', 'Mango'}
---------
# dictionary, hashtabela, object litteral iz javascripta
# A Dictionary is a collection which is unordered, changeable and indexed. No duplicate members.

# Create dict
person = {
    'first_name': 'John',
    'last_name': 'Doe',
    'age': 30
}

# Use constructor
person2 = dict(first_name='Sara', last_name='Williams')

# Get value
print(person['first_name'])
print(person.get('last_name'))

# Add key/value
person['phone'] = '555-555-5555'

# Get dict keys
print(person.keys())

# Get dict items
print(person.items())

# Copy dict
person2 = person.copy()
person2['city'] = 'Boston'

# Remove item
del(person['age'])
person.pop('phone')

# Clear
person.clear()

# Get length
print(len(person2))
---------
# A function is a block of code which only runs when it is called. In Python, we do not use curly brackets, we use indentation with tabs or spaces


# Return values
def getSum(num1, num2):
    total = num1 + num2
    return total
	
# A lambda function is a small anonymous function.
# A lambda function can take any number of arguments, but can only have one expression. Very similar to JS arrow functions

getSum = lambda num1, num2: num1 + num2

---------
# ifs, conditionals 
# Comparison Operators (==, !=, >, <, >=, <=) - Used to compare values

# elif, else if
if x > y:
  print(f'{x} is greater than {y}')
elif x == y:
  print(f'{x} is equal to {y}')  
else:
  print(f'{y} is greater than {x}') 

---

# Nested if
if x > 2:
  if x <= 10:
    print(f'{x} is greater than 2 and less than or equal to 10')
    
---

# Logical operators (and, or, not) - Used to combine conditional statements

# and
if x > 2 and x <= 10:
    print(f'{x} is greater than 2 and less than or equal to 10')
	
	
---
# Membership Operators (not, not in) - Membership operators are used to test if a sequence is presented in an object

numbers = [1,2,3,4,5]

#  in
if x in numbers:
  print(x in numbers)

# not in
if x not in numbers:
  print(x not in numbers)
---
# referenca ===
# Identity Operators (is, is not) - Compare the objects, not if they are equal, but if they are actually the same object, with the same memory location:

# is
if x is y:
  print(x is y)
  
---------
# A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).

people = ['John', 'Paul', 'Sara', 'Susan']

# Simple for loop
for person in people:
  print(f'Current Person: {person}')

# Break
for person in people:
  if person == 'Sara':
    break
  print(f'Current Person: {person}')

# Continue
for person in people:
  if person == 'Sara':
    continue
  print(f'Current Person: {person}')
---
# range
for i in range(len(people)): #range(10)
  print(people[i])

for i in range(0, 11):
  print(f'Number: {i}') #0...10, ne 11
  
---
# While loops execute a set of statements as long as a condition is true.

count = 0
while count < 10:
  print(f'Count: {count}')
  count += 1
  
----------
# kao u js module je FAJL sa fjama ili promenlj
# A module is basically a file containing a set of functions to include in your application. 
# There are core python modules, modules you can install using the pip package manager (including Django) as well as custom modules

# Core modules
import datetime
from datetime import date #obrnuto nije import from
import time
from time import time

# Pip module
from camelcase import CamelCase

# Import custom module
import validator
from validator import validate_email

# today = datetime.date.today()
today = date.today()
timestamp = time()

c = CamelCase()
print(c.hump('hello there world')) #Hello There World

pip3 freeze #da izlistas module
environment je lokalno, default instalira globalno

--------
#__init__ je konstruktor, self je this i explicitno se prenosi uvek, nasledjivanje Klasa(BaseKlasa) umesto extends
# A class is like a blueprint for creating objects. An object has properties and methods(functions) associated with it. Almost everything in Python is an object

# Create class
class User:
  # Constructor
  def __init__(self, name, email, age):
    self.name = name
    self.email = email
    self.age = age

  def greeting(self):
    return f'My name is {self.name} and I am {self.age}'

  def has_birthday(self):
    self.age += 1


# Extend class
class Customer(User):
  # Constructor
  def __init__(self, name, email, age):
    self.name = name
    self.email = email
    self.age = age
    self.balance = 0

  def set_balance(self, balance):
    self.balance = balance

  def greeting(self):
    return f'My name is {self.name} and I am {self.age} and my balance is {self.balance}'

#  Init user object
brad = User('Brad Traversy', 'brad@gmail.com', 37)
# Init customer object
janet = Customer('Janet Johnson', 'janet@yahoo.com', 25)

janet.set_balance(500)
print(janet.greeting())

brad.has_birthday()
print(brad.greeting())
--------
# Python has functions for creating, reading, updating, and deleting files.

# Open a file
myFile = open('myfile.txt', 'w')

# Get some info
print('Name: ', myFile.name)
print('Is Closed : ', myFile.closed)
print('Opening Mode: ', myFile.mode)

# Write to file
myFile.write('I love Python')
myFile.write(' and JavaScript')
myFile.close()

# Append to file
myFile = open('myfile.txt', 'a')
myFile.write(' I also like PHP')
myFile.close()

# Read from file
myFile = open('myfile.txt', 'r+')
text = myFile.read(100)
print(text)

--------
# JSON is commonly used with data APIS. Here how we can parse JSON into a Python dictionary

import json

#  Sample JSON
userJSON = '{"first_name": "John", "last_name": "Doe", "age": 30}' #string sa "

# Parse to dict
user = json.loads(userJSON)

# print(user)
# print(user['first_name'])

car = {'make': 'Ford', 'model': 'Mustang', 'year': 1970} # dict

carJSON = json.dumps(car)

print(carJSON)
-----------------------
-----------------------
# Core-Python-Getting-Started Pluralsight by Austin Bingham and Robert Smallshire
-------------
# 5 modularity
__name__ dunder promenljiva koja sadrzi ime modula da li je modul izvrsen kao skripta
ili importovan u drugi modul, require_once fora

!# shebang definise koji interpreter
----------
# 6
dobar, pass by ref i vrati by ref
---------
# 7 collections
bind je assign
--------------
# 8 exceptions
pass sluzi da se sintakticki ispostuje blok, inace je no-op
-------------
# 9 iterable
comprehensions, sintaksa za map() direktno u petlji
filtering comprehensions, imaju predikad sa if
generatori lazy, elemenata samo koliko je potrebno, za beskonacne nizove
generator comprehensions () umesto []
summary poglavlja su dobra

--------------
--------------
# pipenv virtual environments, kao package.json
# Pipenv Crash Course
# https://www.youtube.com/watch?v=6Qmnh5C4Pmo

# https://gist.github.com/bradtraversy/c70a93d6536ed63786c434707b898d55

# Pipenv Cheat Sheet

# Install pipenv
pip3 install pipenv

# Activate
pipenv shell

# Check version of Python
python --version

# Check path
python
>>> import sys
>>> sys.executable
# quit()

# Install a package
pipenv install camelcase

# Check local packages
pipenv lock -r

# Uninstall a package
pipenv uninstall camelcase

# Install a dev package
pipenv install nose --dev

#Install from requirements.txt
pipenv install -r ./requirements.txt

# Check dependency graph
pipenv graph

# Ignore pipfile
pipenv install --ignore-pipfile

# Set lockfile - before deployment
pipenv lock

# Exiting the virtualenv
exit

# Run with pipenv
pipenv run *
pipenv run python myopengl.py

------------------------
------------------------
# Python Tutorial for Programmers - Python Crash Course by Programming with Mosh
# https://www.youtube.com/watch?v=f79MRyMsjrQ

primitivni tipovi immutable
liste mutable
ternary
message = "elugible" if age >= 18 else "not eligible"
for i while petlje mogu da imaju else blok, kad se zavrsi
[lista]
(tuple) read only list
---
u pozivu fje je keyword arg, u definiciji je default arg
---
*args ga konvertuje u tuple
**args konvertuje keyword args u dictionary
---
funkcijski i global scope, nema blok scope
---
shortcuti home, end, ctrl home, ctrl end, pocetak kraj linije i fajla
----------------------------
----------------------------
"from tkinter import *" <-- importujes sve klase i funkcije iz tkinter modula

"import Tkinter as tk" <-- importujes sve klase i funkcije iz tkinter modula ali pod imenom tk, dakle ako hoces neku tkinter funkciju da pozoves moras pozvati preko tk: tk.color("blue")

"from tkinter import ttk" <-- ovde se importuje SAMO jedna klasa ili funkcija iz tkinter modula pod imenom ttk
----------------
-----------------
# python tuple, ntorke, readonly
(1, 2, 3)
https://www.youtube.com/watch?v=5e5be-8supg&list=PL-UTrxF0y8kI8iBHB2NBArbaHb11LVnGv&index=15
--------------------------
--------------------------
# decorator
# https://twitter.com/mathsppblog/status/1424412170826272774
decorator je sintactic sugar za funkciju koja vraca funkciju (factory fja) i na taj nacin je modifikuje
---------------------
# dunder, magical, data model methods
# Expert Python Tutorial #2 - Dunder/Magic Methods & The Python Data Model
# Tech With Tim
# https://www.youtube.com/watch?v=z11P9sojHuM
default implementacija za operatore *, (), len...
operatorske fje # bukvalno
-----------------------------
# https://twitter.com/reuvenmlerner/status/1551107904182407169
# Having trouble with #Python comprehensions?

First, break them into separate lines:

[x**2 # expression
 for x in range(10) # iteration
 if x < 5] # condition

If you know SQL, think of things this way:

[x**2 # SELECT
 for x in range(10) # FROM
 if x < 5] # WHERE
