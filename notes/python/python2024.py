# Protocols vs ABCs in Python - When to Use Which One? | Revisited - ArjanCodes
# https://www.youtube.com/watch?v=dryNwWvSd4M&t=1s
# https://github.dev/ArjanCodes/examples/tree/main/2024/protocol
# komplikovanija i duza beleska nego sto ovo jeste zapravo
protocols, abstract base classes (ABC) - interfaces in python
interface === abstraction # zapazi
-----
ABC - oduvek, nasledjivanje
protocols - from 3.8, zasnovani na ducktyping
------------
# ABC primer
serialize, deserialize su abstract methods
from abc import ABC, abstractmethod # eto sta import

class SerializedFileHandler(ABC):
    def __init__(self, filename):
        self.filename = filename

    # ABC ima abstract methods uvek
    @abstractmethod
    def serialize(self, data):
        pass
    
    @abstractmethod
    def deserialize(self, data):
        pass
    
    # ima implementaciju
    def write(self, data):
        with open(self.filename, 'wb') as file:
            file.write(self.serialize(data))
            
    def read(self):
        with open(self.filename, 'rb') as file:
            return self.deserialize(file.read())

# podklasa
class PickleHandler(SerializedFileHandler):
    def serialize(self, data):
        return pickle_dumps(data)
    
    def deserialize(self, data):
        return pickle_loads(data)
-------
# ABC poneta:
sve isto kao u C++, apstraktna, nepotpuno implementirana klasa, koja se nasledi i implem u podklasama
nema cist interfejs, ok
abstract class se ne instancira, mora implement absstract methods u podklasi
---------------
# protocol poenta:
protocol je tip, proverava se at RUNTIME, pokusava da pristupi property-ju
samo definises (najavis) koje metode objekat ima, a moze da bude tipa bilo koje klase (sto ne moze sa ABC)
samo taj jedan method (signature, sa args) ga interesuje, a ne class_ hierarchy # to je sve
# protocol, primer
from typing import Protocol

# i protokol JE klasa i nasledjuje klasu
class Writable(Protocol):
    def write(self, data: dict) -> None:
        """This method should write dictionary data."""

class Readable(Protocol):
    def read(self) -> dict:
        """This method should return a dictionary."""
        
class ReadWritable(Readable, Writable):
    def __str__(self):
        return f'{self.__class__.__name__}()'
    

def do_write(writer: Writable, data: dict)
    # moze jer je tipa Writable (protocol), trivial
    # i ime write i args (data), ceo func signature
    writer.write(data) 


class Author: 
    def __init__(self, name: str): 
        self.name = name 

    def write(self, data: dict) —> None: 
        print(f"{self.name} is writing {data}")

def main():
    data = {'name': 'John Doe', 'age': 30}
    # instanciras Author-a
    author = Author('John Doe') 
    # author seda u tip, dovoljno sto ima write(data: dict) method, nema veze sto je nezavisna klasa Author, to je sve, trivial
    do_write(author, data)

# protocol moze da se nasledi, klasa ko klasa
# onda ce da zove write iz base class
class Author(Writable): 

# protocol moze da bude abstract class
class Writable(Protocol):
    @abstractmethod
    def write(self, data: dict) -> None:
        """This method should write dictionary data."""
----------------
# iako obj prolazi protokol, nije tog tipa jer je to runtime check
def write(handler: Writable, data: dict) -> None:
    handler.write(data)

pickle_writer = PickleHandler('../data.pkl')
write(pickle_writer, data) # uobj prolazi kao arg, jer match protocol

# ali nije tipa protocol, trivial
# isinstance je operator koji proverava pravu hijerarhiju
assert isinstance(pickle_writer, Writable) 
-----
# da prodje instanceof dodaj @runtime_checkable decorator from typing
from typing import Protocol, runtime_checkable
@runtime_checkable
class Writable(Protocol):
    def write(self, data: dict) -> None:
        """This method should write dictionary data."""
# @runtime_checkable to ce samo da prodje typecheck, mozda radi ako se poklapa 
# ali nije garancija, ako je implementacija drugacija ce da puca
-----------
# caveats poenta:
slabi i jaki tipovi prica
postoji normalna hijerarhija klasa, nasledjivanje - compile time check 
protocol je runtime, ad hoc, nema hijerarhije, ducktyping - runtime check 
------
ABC se koriste kad zelis zajednicke fje implementirane u base klasi
protocoli - kad?
---------------------
#  Why You Need Custom Exception Classes - ArjanCodes
# https://www.youtube.com/watch?v=ebZB8dPrrog
# https://github.com/ArjanCodes/examples/tree/main/2024/tuesday_tips/custom_exceptions
u puthonu exception classes imaju hijerarhiju, nasledjuju se, BaseError, kao u javascriptu
# imenovanje
class WeightLimitExceededError(Exception):
    """Raised when the weight limit of a container is exceeded."""
    # prima dodatni args data
    def __init__(self, limit: int, current: int):
        super().__init__(f"Weight limit exceeded: limit= {limit}, current= {current}") # ne mora, ali bolje
        self.limit = limit
        self.current = current
        # alternativa message prop
        self.message = f"Weight cannot be negative: weight= {weight}"
--------
raise_, try:, except IOError as e:, else:, finally:
--------
# passing extra data
class CustomError(Exception):
    def __init__(self, message: str, extra_data: dict[str, str]):
        super().__init__(message)
        self.extra_data = extra_data
# poziv
raise_ CustomError("An error occurred", {"key": "value"})
-----------------
# exception hijerarhija chatGpt
BaseException
 +-- SystemExit
 +-- KeyboardInterrupt
 +-- GeneratorExit
 +-- Exception
      +-- StopIteration
      +-- StopAsyncIteration
      +-- ArithmeticError
      |    +-- FloatingPointError
      |    +-- OverflowError
      |    +-- ZeroDivisionError
      +-- AssertionError
      +-- AttributeError
      +-- BufferError
      +-- EOFError
      +-- ImportError
      |    +-- ModuleNotFoundError
      +-- LookupError
      |    +-- IndexError
      |    +-- KeyError
      +-- MemoryError
      +-- NameError
      |    +-- UnboundLocalError
      +-- OSError
      |    +-- BlockingIOError
      |    +-- ChildProcessError
      |    +-- ConnectionError
      |    |    +-- BrokenPipeError
      |    |    +-- ConnectionAbortedError
      |    |    +-- ConnectionRefusedError
      |    |    +-- ConnectionResetError
      |    +-- FileExistsError
      |    +-- FileNotFoundError
      |    +-- InterruptedError
      |    +-- IsADirectoryError
      |    +-- NotADirectoryError
      |    +-- PermissionError
      |    +-- ProcessLookupError
      |    +-- TimeoutError
      +-- ReferenceError
      +-- RuntimeError
      |    +-- NotImplementedError
      |    +-- RecursionError
      +-- SyntaxError
      |    +-- IndentationError
      |         +-- TabError
      +-- SystemError
      +-- TypeError
      +-- ValueError
      |    +-- UnicodeError
      |         +-- UnicodeDecodeError
      |         +-- UnicodeEncodeError
      |         +-- UnicodeTranslateError
      +-- Warning
           +-- DeprecationWarning
           +-- PendingDeprecationWarning
           +-- RuntimeWarning
           +-- SyntaxWarning
           +-- UserWarning
           +-- FutureWarning
           +-- ImportWarning
           +-- UnicodeWarning
           +-- BytesWarning
           +-- ResourceWarning
--------------------------
--------------------------
# keyword arguments u pozivu
# https://github.com/fastapi/full-stack-fastapi-template
# zapzi ovo je poziv sa prosledjenim keyword args, a ne definition sa default values kao u js
app = FastAPI(
    title=settings.PROJECT_NAME,
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
    generate_unique_id_function=custom_generate_unique_id,
)
----
# '*,' keyword-only argument separator, svi args u fji moraju biti keyword args
def create_user(*, session: Session, user_create: UserCreate) -> User:
