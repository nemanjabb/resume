# Python Programming Tutorial for a JavaScript Developer - Google Colab
# https://www.youtube.com/watch?v=povMqm4cttM

# list select range, access, slice
# moze i na stringove
list1 = [0, 1, 2, 3, 4]
list1[1:3] # [1, 2]
# start inclusive, end exclusive (n-1), ili 0-ti + ...
# ako izostavis 0 se podrazumeva [:3] [1:]
# negativan broji od kraja
[1:-1] # otkrati po jedan sa oba kraja
-----
# moze i assign range
list1[1:3] = [1, 2] # will put 1, 2 in [0, 1, 2, 3]
# moze i vise
list1[1:2] = [5, 5] # [0, 5, 5, 2, 3, 4], ubacio sve elem podliste, nije nested list
-----
# concat lists
list1 + list2
-----
# console.log
print()
-----
blokovi su indentacija, syntax error, kao yaml
----------
if x > 10 and x < 20: # && je and, || je or, : je blok
--------
for i in range(0, 5): # 0..4 # for loop
for name in names: # foreach loop
for key,value in dictionary.items(): # loop through object, unpacking - destructuring
-----------
list - []
dictionary - {} # object
tuple - () # immutable (readonly, const) list, and fixed length, count(), index()
----------
const jsObject = { name: "linda"}
dictionary = { "name": "linda"}
# access
dictionary["name"]
dictionary.get("name", "default value")
dictionary.keys() # list
dictionary.values() # list
dictionary.items() # list of tuples
----------
# unpacking - destructuring
a, b = list_or_tuple # 0th and  1st
# * is ... za rest
a, *b, c = [0, 1, 2, 3] # a=0 - number, b=[1, 2] - list, c=3 - from end # zapazi end
------------
# map list
jsArr.map(callback)
map(callback, list1) # map je global fn, lista 2nd arg, vraca map, mora list(map)
# lambda - js arrow function
list2 = list(map(lambda arg: arg + "_", list1)) # append "_" string
# filter isto ko map, samo predicat cb
list2 = list(map(lambda arg: arg[-1] == "a", list1)) # filter koji ends with "a", [:] moze i na stringove
-----
# list comprehension je isto sto i map()
list2 = [returnVal + "_" for item in list1] # isto append "_" string, map() sa desna na levo
# filter, ima i if
list2 = [returnVal for item in list1 if item[-1] == "a"] # isto filter koji ends with "a",
----------
# Python 101 for JavaScript Developers
# https://python.plainenglish.io/python-101-for-javascript-developers-e7f1987f825e
# boolean operators - &&, ||, !
and , or , not
----
# in python variable cannot be unassigned
None - null i undefined
-------------
# primitive types
# python 4 primitive data types: 
 int , float , bool, BigInt
 # JavaScript 7 primitive data types: 
 number , string , boolean , bigint , symbol, undefined, null
---------
# get type of var python
type(x)
# JavaScript
typeof(x)
--------------
# ordered - definisano mesto elem, moze da se pristupa indexom
list, tuple
# unordered - orders are not guaranteed
set, dictionary
----
# construktori
str(letters), list(letters), tuple(letters), set(letters)
------------------------------
# Python for JavaScript Developers - Valentino Galigardi, dobar, dug clanak
# https://www.valentinog.com/blog/python-for-js/

# Arithmetic operators
sve isto kao js osim floor division // - celobrojno deljenje, nanize
5 // 3 = 1
Math.floor(89 / 4) # js ekvivalent
----
# string mnozenje
"a" * 9 = "aaaaaaaaaa"
----------
# inc, decr
a = 34
a += 1 # nema a++
------------
# comparison - isto sem ===
9 == "9" # False
------------
# shortcircuit - isto
2019 > 1900 and print("I am cool")
not 2019 > 151900 and print("I am cool") # negacija, flip
2019 > 1900 && console.log("I am cool")
--------------
# primitive types
# both Python and JavaScript basic types are immutable
null - None
undefined - nema
-------------
# regex
import re
regex = re.compile(r"\d\d\d\d")
text = "Your id is 4933"
match = regex.search(text)
start, end = match.start(), match.end() # start is 11 and end is 15
found = text[start:end] # 4933
---------------------------------------------------------------------------
---------------------------------------------------------------------------
# dovde pocetnicki, pre pluralsighta
---------------------------------------------------------------------------
---------------------------------------------------------------------------
# expression vs statement in python
# https://stackoverflow.com/a/4728162/4383275
an expression evaluates to a value. A statement does something.

x + 2         # an expression
x = 1         # a statement 
y = x + 1     # a statement
print y       # a statement (in 2.x)
------
expression - reduced to a value
statements - 1+ lines
----
expressions are statements as well
---------------------------------------
# virtualenv vs venv
venv je builtin koji je nastao iz virtualenv-a, dakle nije stvar da je bolji. 
ako moras podrzavati 2.x, onda koristis virtualenv. Ako si na 3.3+ (ili tak nesto), onda venv.
----
znaci venv da koristim
-------------------------------
# Multiprocessing in Python - Part 1 
# https://www.youtube.com/watch?v=CRJOQtaRT_8&list=PLMOobVGrchXPeBgrrnQN6pimrmpTsWFiZ&index=13
4 klase:
Process, Lock, Queue, Pool
-----
# Process klasa - thread zapravo, prima funkciju i args
import multiprocessing as mp
print(mp.cpu_count())
-----
proc = Process(target=myfunc, args=(5,)) # kreira proc objekt koji ne radi
proc.start() # pokreni proces
proc.join() # terminate proc
-------
# Lock klasa
aquire(), release() # zakljucavanje i pustanje resursa
-------------
# Multiprocessing in Python | Part 2
Queue - redovi za medjuprocesnu komunikaciju, fifo
----
def sqrt(x, q):
    q.put(x * x)

q = mp.Queue()
p = Process(target=sqrt, args=(4, q))
p.start()
p.join()

result = q.get() # precess je pozvao fju koja je stavila u red
------
SharedMemory od 3.8
-------
procesi su kao kucista za funkcije, koje se pustaju i alociraju na jezgrima
--------------
# Pool
data parallelism - kao map reduce nesto
distribute data na jednu fju u nekoliko procesa
da mozes data da podelis na vise nodes, horizontalno
----
p = Pool(2)
p.map(fja, data) # mapira data na fju
----------------
izgleda da su u pythonu ipak procesi, a ne niti
----------------------------------------
# pip wheel - binarne zavisnosti umesto python src
# https://realpython.com/python-wheels/
wheel je binarna distribucija formata za Python pakete koji omogućavaju brže instaliranje paketa 
jer se ne moraju kompajlirati prilikom instaliranja
----------------
pip wheel komanda instalira pakete i njihove zavisnosti kao wheel-ove (pre-kompajlirane pakete) u određenu direktorijum
----------------------
# manage.py
za db migracije uglavnom
a moze jos stosta, dev server, testivi, skripte...
----------------------
# https://dev.to/lucaslm/python-virtual-environments-4o5g
globalni paketi se ne vide u venv
close terminal deactivates venv
----
# venv je samo za dev
u ci/cd, docker i prod se ne koristi venv, samo local dev
----
u vs code python extenziji moras da selektujes python interpreter iz venv # zapazi
---------
# odlican clanak za venv, ima sve za nedoumice
# https://realpython.com/python-virtual-environments-a-primer/#what-other-popular-options-exist-aside-from-venv
venv dolazi uz python 3.3+
virtualenv is a superset of venv, i stariji je od venv
---
virtualenv prednosti:
1. brzi
2. updated pip
3. radi sa python 2.7, venv ne radi
----
# poenta:
venv za pocetnike i defualt # eto, taj koristis znaci
virtualenv napredan i za legacy
-------
# 3rd party tools:
virtualenvwrapper - high level, on virutalenv
poetry - dependency management, deterministic, kreira SVOJ virtual env
pipenv - dependency, high level, deterministic, based on virtualenv
-------------------
# rezime iz fastapi:
1. concurrency - async, IO neblokiranje
2. multithreading - GIL, python interpreter
3. multiprocessing - OS, komunikacija
-------------------
# https://www.youtube.com/playlist?list=PLyb_C2HpOQSBsygWeCYkJ7wjxXShIql43
coroutine - funkcija koja moze da pause i resume svoj execution
u pythonu je to async def fja(): - coroutine object
await je pause
---
awaitable object: 
1. coroutine
2. task - asyncio.create_task()
3. future
----------------------------------
# venv, rad, chatGpt
# prvo mora paket na ubuntu
sudo apt install python3-venv
# ovo kreira folder my-venv
python3 -m venv my-venv
# activate
source my-venv/bin/activate
# packages pip
pip install package_name
pip list
pip freeze > requirements.txt
pip install -r requirements.txt
deactivate
# current interpreter
python --version
which python
# local cookiecutter
pip install cookiecutter
-------
venv ti je u terminalu, (install) komande pod venv
project source je u drugom folderu
---------------------------
# __init__.py u folderu, paket, chatGpt
# namena, ok
1. Označavanje direktorijuma kao Python paketa
organizujes kod u vise fajlova (modula) - python ga vidi kao 1 fajl, modul
2. Inicijalizacija paketa
definisanje config i pozivi fja, default values
3. Kontrola izvoza simbola
sta export, sta private, preimenovanje
---
per python 3.3 bio obavezan
---
najcesce je prazan jer ne treba inicijalizacija ili export
-----------
# kako menja import
fajl (module), folder (package) je objekat (dict), kao u javascriptu, node.js
ide objekat folder(package).fajl(module).vars # eto, to je to
---
# primer
moze staza do foldera, a ne do fajla, i objekat..., trivial
# struktura
my_package/
    __init__.py
    module1.py
    module2.py
# import
from my_package import module1, module2 # import radi kao destruktuiranje objecta(dict), ok
result1 = module1.function1()
# bez __init__.py, ne moze 2 fajla import u 1 liniji
from my_package import module1
from my_package import module2
-----------------------
# prazan __init__.py - folder je python paket, chatGpt # beleska od pre
kad ima kod unutra, za export, import other packages, set default values, preimenovanje aliases
podseti se u Pluralsight beleskama
iako je prazan mozes da dohvatis fajlove (module) # eto
-----
__init__ , sluzi da tretira folder kao modul # ok
-------
# cita ugnjezdene foldere sa . kao dict
from app.db.init_db import init_db
--------------
u pythonu se klase (i lokalne za nested) koriste umesto object literals # zapamti
---------------------
# __all__ = lista
definise sta se importuje sa * iz modula (fajla) (ili foldera - paketa sa __init__.py)
from module import *
__all__ = ["public_variable", "public_function"]
default su exportovani svi osim koji pocinju sa '_', private, _my_var # zapazi, nisam znao
-----------------------
# vise python verzija na linuxu, za globalne pakete, inace rusi gnome
pyenv - to install/manage different Python versions.
plugin pyenv-virtualenv - easy to use virtual environments with the different Python versions
