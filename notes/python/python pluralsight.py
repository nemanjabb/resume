---------------------------------------------------------------------------
---------------------------------------------------------------------------
# python path Pluralsight
# https://www.pluralsight.com/paths/core-python
----
### Big picture
## why python
# 1. big picture - boza, trivial
nista, uvod u kurs
--------
# 2. simple to learn
nista, bezveze
-----
# 3. simple to use
one way to do things
-----
# 4. great community
PEP - python enhancement proposals - razvoj jezika
------
# 5. high demand
nista, popularan, grafici, ankete
-------
# 6. widely used
upotreba u web, data, education, scripting
-------
# 7. web development
flask za api, django i template, cms... netacno
-------
# 8. data science
big data
2012. - 2.4 exabytes dnevno generated, hadoop...
---
machine learning
email spam detection, optical character recognition, computer vision
--------
# 9. education and learning
biologija, fakultet, raspberry
jupyter notebook - interaktivni editor...
--------
# 10. scripting
OS scripting - files, config, processes, applications
app scripting - games, 2d-3d modeling, photo manipulation
----------------------------
# what is python
# 1. what is python
uvod u poglavlje
-----
# 2. unique syntax
significant whitespace
-----
# 3. general purpose
general purpose
high level - relacija jezika sa hw instrukcijama
-----
# 4. multi-paradigm
structured - control structures, subroutines - procedures, functions, methods
oop - algoritmi i strukture upakovani u objekte, obj komuniciraju
functional - matematika
-----
# 5. interpreted
interpreted, compiled, nista novo
-----
# 6. garbage collected
von neuman architecture
-----
# 7. dynamically typed
static languages - compiled uglavnom
istu var reassign sa broj pa sa string
numer + string - type error u python
---
duck typing - moze bilo koji arg fje koji ima dati method
------
# 8. summary
nista ponovio 20 sec
---------------------------
# python pros and cons
# 1. pros and cons
uvod u poglavlje, nista
------
# 2. comprehensive standard library
ili minimal ili comprehensive
python has comprehensive std lib
---
collections, files, dates and time, compression, user interfaces...
-------
# 3. community driven
od 2000. community vodi razvoj
PEP - python enhancment proposals
zen of python - glavni principi
release - detalji features i odluka
community - lista chatova na sajtu
python software foundation - novac za open source
--------
# 4. 3rd party libraries
pypi - python pacage index - trazis pakete
pip - ugradjen u jezik - pip install paket-name
--------
# 5. 3rd party tools
editori: pydev (eclipse), pycharm (jetbrains), vs code, spyder
----
flake8 - styleguide...?
pylint - code analysis
black - formatter
performance analyzers
--------
# 6. drawbacks - mane
interpreted - slow
not native - malo na mobile i desktop
dynamic - runtime errors
-----------
# 7. summary
zakljucak za ceo kurs, ponovio sve
--------------------------------------
--------------------------------------


--------------------------------------
--------------------------------------
### core python - getting started
## 1. Course overview
# 1. course overview
pregled kursa samo
------------------------
# installing and starting python - boza, trivial
# 1. version check
nista
------
# 2. overview
2.7 napusten 2020, kurs snimljen sa 3.6, nista
------
# 3. installing python
add to path - windows
python - za repl na win kad nema 2.7, pyhton3 na mac
python3 preinstalled na ubuntu
--------
# 4. interactive python
REPL
print() u 3, u 2.7 bez (), sideefect, ne vraca
exit repl - win ctrl z, enter, linux - ctrl d = eol char?
ctrl z u linux suspends, fg to return
---------
# 5. significant whitespace
... u repl - ocekuje blok
blok indent je 4 spaces
: je block
4 spaces je preferred, nikad ne mesaj tabs i spaces
----------
# 6. python culture
pep8 - formatting i indentation
pep20 - zen of python - import this u repl
-----------
# 7. python standard library
std library organizovana u module, import
import moze u repl
help(module_name) - da vidis fje u modulu
----
import fju from module
from math import factorial
from math import factorial as my_factorial # rename import
// - celobrojno deljenje, int
python radi sa velikim brojevima by default
------------
# 8. summary
detaljan pregled, dobar
-----------------------------
# scalar types, operators, control flow
# 1. overview
scalar types:
int 
unlimited precision
10 - decadni
0b10 - binary
0o10 - octal
0x10 - hexa
contructori:
int(3.5), int("3.5") # zaokruzuje ka 0 - kao floor()
int("1000", 3) - osnova
----------
float
sa zarezom, 15-16 celobrojnih cifara
3e8 - eksponent 10
float(7), float("7.1"), float("nan"), float("inf")
3.0 + 1 = 4.0
--------
None
None is None = True
--------
bool
constructor:
bool(42)...
falsy:
0, 0.0, [], ""
bool("False") # True
----------------------
# 2. relational operator
# trivial, nista to je to
==, !=, <, >, <=, >=
----------------------
# 3. control flow
if expression:
    block
----
if bool("eggs"): # je isto sto i:
if "eggs": # pa se bool() i ne koristi, nepotrebna napomena kao Boolean() ts
-----
uvek elif - umesto else pa nested if else
----------------------
# 4. while loops
while expression:
    block
nema do while, mora break
break izlazi samo iz prve inner most loop
-----
while True: # dead loop
    pass # prazna petlja, ctrl c
-----
newline zatvara block u repl
------
# primer
c = 5
while c: # 0 je falsy za izlaz
    print(c)
    c -= 1 # c--
-----
input() # console.read()
-----------------------
# 5. summary
nabrojao sve iz poglavlja
-----------------------
# introducing string, collections and iteration
# 1. overview
str, bytes, list, dict, for loops
---------
# 2. string, trivial
immutable
moze i single i double quotes
bolje upotrebi suprotnu opciju nego escape
---------
# 3. string literals
multiline strings - moze i double i single quotes
"""first line
second line"""
'first line\nsecond line' - sa escape
\n - universal newline - all OS, win too
\\ escape \ itself
r'raw string' - disable escape, sve doslovno
print() da vidis sta je unutra zapravo
----
jedno slovo nije char nego string isto
type(s[4]) - string
----
c.capitalize() - svi metodi su immutable, rezultat je vracen, nova instanca
----
python source je utf-8
\u pa unicode hexa kod za utf slova
\x pa hexa broj za string
\345 za octal
sve ovo u str izmedju navodnika
"str1" "str2" se concat u repl u "str1str2"
---------
# 4. bytes
za binarne data, slicno kao string, slicne i fje
b'data' ili b"data"
bytes() - contructor
-----
data[0], data.split()
-----
moras unapred da znas koji encoding string koristi
encode - str to bytes
decode - bytes to str
data = norsk.encode('utf8')
string1 = data.dencode('utf8')
------
fajlovi i http response su bytes type
---------------
# 5. list 
mutable, list of objects, moze razl tipovi odjednom
['apple', 7, 'peer']
list("this is string") - constructor
[1, 2, 3,] - zadnji moze zarez iza
---------------
# 6. dict - map, associative array
{k1: v1, k2: v2}
od 3.7 zadrzan order
d['key1'] = 1 # assign
d['key1'] # access
------------------
# 7. for loop - foreach
for item in iterable:
    body
for city in city_list:
    print(city)
for color in colors_dict: # daje key
    print(color, colors_dict[color]) # key, val
-------------------
# 8. putting it all together
from urllib.request import urlopen
from stdlib.module import function
response vraca listu bajtova b'word'
line.decode('utf8') - byte u string
--------------------
# 9. summary
nabrojao sve iz poglavlja
-----------------------------------
# modularity
# 1. overview
pregled narednih klipova
-------
# 2. modules
1 tab = 4 spaces
utf-8 source files
----
ptyhon # start repl
import words # import word.py u repl
code se izvrsi kad ga importujes, ako je global (not in function)
----------
# 3. functions
def square(x):
    return x * x
funkcije vracaju vrednost ili izvrsavaju sideeffects
----
samo return vraca None
pa void fja vraca None
----
specially named functions
__feature__
---------------
# 4. __name__ - dobro objasnio
kad importujes fajl sa fn def bez poziva naravno ne izvrsi
import my_file
my_file.my_function()
from my_file import my_function # drugi nacin
my_function()
-----
# u shell ne moze da izvrsi fju iz fajla
python my_file.py
da bi fajl mogao da se import kao module
i izvrsio kao script
__name__ # detektuje da li je fajl importovan ili pozvan kao skripta
print(__name__) # ima razl vrednost zavisno kako se fajl poziva
1.
import my_file
my_file # na import SAMO prvi put, require_once() fora
2.
python my_file.py
__main__
---------
# vrlo jasno
# zovi fju samo ako je fajl pozvan kao skripta
# na import ne zovi fju
if __name__ = '__main__'
    my_function()
--------------------
# 5. python execution model
# stavi debugger u global namespace
# def nije samo definicija nego i statement, debugger staje, poziva se (ne fja naravno)
def my_function(): #
tada se fja bound na module namespace
------
python module - .py fajl, import u drugi fajl ili repl
python script - poziva se iz koonzole
python program - composed from many modules
---------------------
# 6. command line arguments - ok sa main()
import sys

def fetch_words(url):
    ...

def print_items(items):
    ...

def main(url):
    words = fetch_words(url)
    print_items(words)

# invokuje tvoj custom entry point za script mode
# za import ignored
if __name__ == '__main__':
    main(sys.argv[1])

# poziv
1.
# glavni nacin za import
# zagrade samo za multiline
from words import (fetch_words, print_items)
2.
# imp u global namespace, moze polute, samo za repl i zezanje
from words import * 
# sve fje su definisane sad
main("http://site.com/words")
3.
# poziv kao script
python words.py http://site.com/words
-----------
# poenta
napravi main() fju za fajl (script) i tu pozovi sta treba
i tu prosledi args
-------------------------
# 7. moment of zen
formatting:
2 newlines izmedju funkcija - definicija
1 newline za logicke statements
-----------------------
# 8. docstrings
"""i za single line"""
moze za module ili za funkcije
help(my_module) ih stampa
------
# primer
def fetch_words(url):
    """Fetch list of words from URl.

    Args:
        url: The URl of a UTF-8 text document.

    Returns:
        A list of strings containing the words from a document.   
    """
    x = 5
    telo fje dalje
-------------------------
# 9. comments - trivial
nista, koristi #
-------------------------
# 10. shebang
#! /usr/bin/env python3
env je linux program koji trazi na path env var
# za shebang mora na mac i linux
chmod +x words.py
$ ./words.py http://site.com/list
----
# windows, pylauncher, parsuje shebang
# cmd
words.py http://site.com/list
# powershell, samo \ za stazu
PS> .\words.py http://site.com/list
----------------------------
# 11. summary
*.py fajl je modul
implicit return na kraju isto vraca None
sys.argv[0] - script filename
sys.argv[1] - prvi arg
-------------------------------
# objects and types
# 1. overview - dobar i vazan, refs ko javascript, skroz isto mislim
x = 1000 # x name (reference) -> int object (immutable)
x = 500 # x pokazuje na 500 int object, 1000 garbage collected (niko ne pokazuje na njega vise)
y = x # obe ref x, y pokazuju na isti 500 obj
x = 3000 # x -> 3000, y -> 500
-----
id() - returns unique identifier that is constant for life of obj - ref pointer, samo za debug se koristi
a is b - da li obe ref pokazuju na isti obj
t += 2 # ne mutira value nego pravi novi obj, menja id()
-----
= assign operator only binds to name, not to value
python has references to objects, not boxes with values
----
r = [2, 4, 6]
s = r
s[1] = 17 # obe promenjene
s = [2, 17, 6]
r = [2, 17, 6]
s is r # True
-----
p = [4, 7, 11]
q = [4, 7, 11]
p == q # True, same value
p is q # False, different reference
-----
value equality i identity equality are fundamentally different
value equality - definisana na nivou (custom) tipa
identity equality - def na nivou jezika, ne moze se menjati
-----
ako je is True onda je i == True, pokazuje na isto
--------------------------
# 2. passing arguments and returning values - dobar, vazan
function arg passed by reference i modifikovan i unutra i spolja
m = [9, 15, 24]
def modify(k):
    k.append(39)
    print("k = ", k)
modify(m)
m = [9, 15, 24, 39] # k modif unutra
m
m = [9, 15, 24, 39] # i m modif spolja
-----
f = [14, 23, 37]
def replace(g):
    g = [17, 28, 45] # = ne mutira, g reassigned samo
    print("g = ", g)
replace(f)
g = [17, 28, 45] # unutra dodeljena
f
f = [14, 23, 37] # spolja netaknuta
-----
dodeljuje ref od actual arg ref (iz poziva) to formal arg ref (iz fn def)
g[1] - mutates
g = [nova] - doesnt mutate
----
pass by object reference
isto vazi i za return
----------------------------
# 3. function arguments
formal args - u definiciji funkcije
# bilo koji arg moze i by postion i by name, zapazi
def banner(message, border='') # default arg, mora posle required args
# poziv
# positional argument (by position), keyword argument (by name)
def banner("some string", border="*") 
# moze oba kao keword args, order onda nije bitan (javascript obj), mora posle positional isto
def banner(border="*", message="some string") 
------------
# default args se izvrsavaju JEDNOM kad se def fn statement binduje
# vazno za mutable default args
def show_default(arg=time.ctime()): # jednom, bind time
    print(arg)
show_default()
show_default() # uvek isto
----
def add_spam(menu=[]):
    menu.append("spam")
    return menu
add_spam() # koristi default arg
add_spam()
add_spam() # koristi jednu istu ref ["spam", "spam", "spam"]
# zakljucak: uvek koristi immutable types (int, str) for default values
# definisi pravi default u telu
----------------------
# 4. python type system
python has dynamic and strong type system
dynamic - type resolved runtime, nije definisan compiletime
----
def add(a, b):
    return a + b
add(5, 7)
add("news", "paper") # bilo koji tip za koji je + definisan
add("news", 7) # type error, python ne radi implicit type coercion
# izuzetak: u bool u if i while
--------------------------
# 5. scopes
x = 5
x = "str" # moze reassign na drugi tip
----
4 levels scope:
local - inside current function # most narrow
enclosing - inside enclosing function
global - at top level of the module
built in - builtins module - core language
----
LEGB - prvo se gleda most narrow scope
-----
nema block scopes - for loops, if-s...
-----
global count # ako hoces da ref var iz parent scope
------------------------
# 6. moment of zen
sve vars su references to objects (i primitivni tipovi)
sve je object - int, functions, modules
------------------------
# 7. everything is object
type(var1) # vraca tip promenljive, funkcije, modula
dir(var1) # vraca attributes of object
attributes - za module: fn names, imports, __nesto__ (special attrs)
words.fetch_words.__name__ # 'fetch_words' - ime fje kao string
words.fetch_words.__doc__ # docstring intelisense
-----------------------
# 8. summary
references, not value boxes
is - if ref na isti obj
strong typing - no coercing to match
import and def - bind names to objects
-------------------------------
## 7. built in collections
# 1. overview - nista
string, list, dict, tuple, range, set, protocols
---------------------
# 2. tuples
immutable sequences of objects
ne moze replace, remove, append
t = ("norvay", 4.93, 3)
t[0] # access
len(t) # count
t + (1, 2) # concat
t * (1, 2) # multiply
nested - multidim - matrix
k = (391,) # sa 1 elem mora ,
p = 1, 1, 4 # moze i bez (), zapazi
----
# tuple unpacking - destructuring into vars
first, second = my_func(args) 
(a, (b, (c, d))) = (4, (3, (2, 1))) # moze i nested
# swap
a, b = b, a
# constructor
tuple(list or string)
# includes
5 in (3, 5, 6)
5 not in (3, 5, 6) # negacija
----------------------
# strings
immutable
len(string)
"new" + "found" # concat, + ko js, pravi temp unused vars
my_str = "separator".join(["a", "b"]) # join
my_str.split('separator') # split
"unforgetable".partition('forget') # split podniz
('un', 'forget', 'able') # tuple
_ - za unused vars
"the age of {0} is {1}".format('jim', 32) # template strings
"the age of jin is 32"
"the age of {} is {}".format('jim', 32) # po redu se podrazum, moze da se izostave
# moze named args, tuple, dict in {}
"the age of {name} is {age}".format(name='jim', age=32) 
------
# f strings - literal string interpolation
# ovo je zapravo js template string tacno
f'the value is {value}' # moze i f""
----------------------
# 4. ranges
arithmetic progression of integers
range(5) # 0-4, 5 - end required, not included
list(range(10, 15)) # za start sledeceg
[10, 11, 12, 13, 14]
---
list(range(0, 10, 2)) # step
[0, 2, 4, 6, 8]
---
ne moze keyword args
------
# za index
t = [5, 372]
for p in enumerate(t):
    print(p)
(0, 6) # tuple sa indexom
(1, 372)
-------------------------
# 5. lists
# negative indexing
x[-1] # negative index last = [-1]
positive indexing - 0 based
negative indexing - 1 based # to, to
-----
# slicing - deo liste
my_list[start:stop]
s = [3, 186, 4431, 74]
s[1:3] # 1, 2 indexe, start included, stop je excluded
[186, 4431]
-----
s[1:-1] # svi osim prvi (nulti) i zadnji
s[2:] # od treceg do kraja
s[:2] # prva dva
s[:2] + s[2:] # select celu listu, zato je takav start, stop (half open range)
-----
r = s[:] # cela lista, koristi se za clone list
r is s # False
r == s # True
# shallow copy
elementi u listi su i dalje zajednicki (refs), zapazi
menjanjem jedne, menjas i drugu, shallow copy
----
u = s.copy() # drugi nacin za clone
v = list(s) # moze i ovo
----
elem1 is elem2 # True, mutable, kad je elem lista npr
----------
# deep clone - copy module u std lib, retko treba
-----
[0] * 9 # [0, 0, 0, ...] # multiply SAME ref opet
-----
# find elem, return index
# ako ne nadje exception
# list is mutable
index = f.index("fox")
w.count("the") # broj ponavlj elem u listi
37 in [1, 2, 37, 5] # includes(), True
37 not in [1, 2, 37, 5] # exists(), False
----
del u[3] # remove at index, by position
u.remove("jack") # remove by value, exception isto
-----
li.insert(index, elem)
---
k = m + n # concat, new list, ne mutira operande
k += [1, 2] # mutira k
k.extend([3, 4]) # isto concat
-----
g.reverse()
d.sort(key, reverse=False) # optional args
key callable object - func + drugo...
h.sort(key=len)
----
# out of place equivalents
reversed() - return reverse iterator
sorted() - return new list
za kreiranje sortirane kopije...
-------------------------
# 6. dictionaries 
# js object, php assoc array, hashtabela
my_dict = { 'google': 'https://google.com' }
keys - unique, immutable (string, number, tuple), not list
values - can be mutable
unordered - random
----
# constructor
# from list of key val tuples
dict([('alice', 32), ('bob', 21)]) 
# from named args
dict(a='alfa', b='bravo')
-----
# shallow copy by default, kao liste
e = d.copy()
f = dict(d)
-----
# merge two dict, override same keys kao js spread
dict1.update() # dict1 zelis da updateujes
-----
# iterate, vraca keys
for key in colors:
    print(f"key:{key}, val:{colors[key]}")
# iterate only on values, cant get key from val
for value in colors.values():
    print(value)
# iterate only on keys
for key in colors.keys():
    print(key)
# iterate only on key val tuple and destructure it
for key, value in colors.items():
    print(key, value)
-------
# in, not in
'key1' in my_dict
-----
# delete item
del z['key1']
------
# modify
+= augmented assignment operator
m['h'] += [1, 2] # edit item, concat list
m['g'] = [3, 4] # add new item
------
# pretty print
# fn and module same name pa mora rename da ne override, bzvz
from pprint import pprint as pp
pp(my_dict)
------------------------
# 7. set - matematicki skupovi
unorderd, unique
set is mutable, but elems must be immutable
my_set = {6, 27, 496} # kao dict ali nisu key val parovi
----
# constructor
d = {} # dict
e = set() # empty set mora ovako
f = set([1, 2]) # from list, unique
-----
# in, not in - membership
3 in {1, 2, ,3}
----
k.add(12) # add elem, add existing ignored
k.update([4, 5, 6]) # add multiple elems
-----
k.remove(97) # sklanja ako postoji ili exception ako ne postoji
k.discard(97) # isto sklanja ali bez exception
-----
# shallow copy
j = k.copy()
m = set(j)
------
# set operations
# unija, razlika, presek
# predikati - podskup, nadskup, disjoint - disjunkcija
s1.union(s2) # commutative
s1.intersection(s2) # commutative
s1.difference(s2) # u s1 i ne u s2
s1.symmetric_difference(s2) # commutative
----
# predikati
s1.issubset(s2)
s1.issuperset(s2)
s1.isdisjoint(s2)
--------------------------
# 8. protocols - C# interfejsi
skup fja tipa, kao interface
protocols:
container - in, not in
sized - len(collection)
iterable - yeld elements, for loops
sequence - sequence[index], s.index(item), s.count(item), reversed(s)
--------------------------
# 9. summary - pregled
tuple - optional ()
unpacking - destructuring
f-strings - python 3.6
shallow clone - nove ref na postojece objekte
--------------------------
## 8. exceptions
# 1. overview
raising - throwing
handling, unhandled - call stack start of the program
object - info
-----------
# 2. exceptions and control flow - nista
trivial example, non existing index in dict
ex propagates through call stack
------------
# 3. handling exceptions - primer sintakse
def my_fn():
    try:
        # block
    except (KeyError, TypeError): # moze tuple tipva
        # block
        return -1 # moze return iz catch
    return 1
---------------------
# 4. exceptions and programmer errors
pass - no op - samo za sintaksu, empty blocks arent permitted in python
# programmer errors - greske programera koje se ne hvataju/handluju
IndentationError, SyntaxError, NameError
-----
except (KeyError, TypeError) as e: # exception obj
    # ovako debug err
    print(f"printed error: {e!r}", file=sys.stderr)
------------------------
# 5. re-raising exceptions, lose objasnio
def my_fn():
    try:
        # 
    except (KeyError, TypeError) as e:
        # print(...) # da bi ovo odstampao, eto
        raise # re-throw e
--------------------------
# 6. exceptions are part of the api (signature of function)
linija ispod one koja baca exception se ne izvrsava
# validacija arg fje
if x < 0:
    raise ValueError("x ne sme negativno")
# docstring za exception
"""
Raises: 
    ValueError: If x is negative.
"""
----
gracefully handled - elegantno obradjen
--------------------
# 7. exceptions and protocols
common exc:
IndexError - index out of range my_list[9]
ValueError - dobar tip ali invalid value int("slovo")
KeyError - lookup in map failed (key not in dict)
----------------------
# 8. avoid explicit type checks
TypeError - ne hvataju se, neprakticno
------------------
# 9. its easier to ask forgiveness than permission - dobar
LBYL - look before you leap - proveri sve unapred i izbegni, mnogo slucajeva za proveru, ne koristi se
EAFP - pusti da se desi pa handluj posledice - ovo se koristi u python
fokus na happy path - umesto polute main flow with checking
exception logika je odvojena, moze biti izmestena u parent func
----
LBYL je if else bez exceptiona, error codes, silent
EAFP try catch, cant be ignored
-----------------
# 10. cleanup actions - finally block
def my_fn():
    try:
        # 
    finally
        # runs i za success i za exception
        # vrati u pocetni dir npr

# ili
def my_fn():
    try:
        # 
    except OSError as e:
        #
    finally
        # runs i za success i za exception
errors must not pass silently
errors must be silenced explicitly
---------------------
# 11. platform specific code - OS
import win or linux lib
----
# moze import u telu fje, eto, kao require()
try:
    import msvcrt # windows
    # ...
except ImportError:
    import sys # za linux
    # ...
---------------------
# 12. summary 
# dobri podsetnici ovi zadnji klipovi
samo raise - re-raises existing ex
--------------------------
## 9. iteration and iterable
# 1. overview
comprehensions - syntax for creating iterable objects
iterators
generators
--------------------
# 2. list and set comprehensions
lists, sets, dicts
----
# list
[expr(item) for item in iterable] # moze i {} za set
-----
# ekviv petlja - js map() prakticno
[len(word) for word in words]
----
lengths = [] # lengths je izlaz
for word in words:
    lengths.append(len(word))
----
# set
s = {len(str(factorial(x))) for x in range(20)}
-------------------
# 3. dictionary comprehensions
# isto kao set samo key and val
# tacno u 2 celine, zapazi
{
    key_expr(item): value_expr(item) # ovde imas objekat koji budzis
    for item in iterable # ovde imas petlju koja proizvodi item
}
----
# swap capital and country
capital_to_country = {capital: country for country, capital in country_to_capital.items()}
# for na dict default radi na keys, pa koristi dict.items() i unpacking
----------------------
# 4. filtering comprehensions - js array.filter()
filtering clause - optional
----
# primer prosti brojevi
def is_prime(x): ...
# x je izlaz bez transform - levo
# for x in iterable je uvek petlja - sredina
# if condition je filter predikat - desno
[x for x in range(101) if is_prime(x)]
------------------------
# 5. moment of zen
comprehensions should be pure, no sideeffects
ako treba sideeffect use for loop, print() npr
------------------------
# 6. iteration protocols
comprehensions prolaze kroz celu kolekciju, map()
----
iterable - iter() vraca iterator
iterator - next() vraca item
----
svaki next(iterator) vraca sledeci item
posle zadnjeg baca exception StopIteration
----
petlje implementirane preko ovoga
-----------------------
# 7. generator functions - poenta lazy i fn
iterable kroz funkciju
lazy evaluation - izracunavaju sledeci item tek na zahtev
can model infinite data - streams
compose into pipelines
----
fja je generator ako ima 1+ yeield statement
----
generator function - 1+ yield, 0+ return, impicit return at end of fn
----
# generator fn primer
def gen123():
    yield 1
    yield 2
    yield 3
----
# poziv rucno
g = gen123() # g - gen obj - iterator
next(g) # 1
next(g) # 2
next(g) # 3
next(g) # StopIteration ex
-----
# poziv u petlji
for v in gen123(): # svaki v je novi gen obj, krece od 1
    print(v)
----------
# detaljni primer, kreni stani - dobar
def gen246():
    print("about to yield 2")
    yield 2    
    print("about to yield 4")
    yield 4
    print("about to yield 6")
    yield 6
    print("about to return")
---
g = gen246() # nista jos nije pozvano, zapazi
next(g)
"about to yield 2"
next(g) # krece odakle je stao, i staje na sledeci yield, zapazi
"about to yield 4"
next(g)
"about to yield 6"
next(g) # na impl return exception
"about to return"
StopIteration exception
---------------------------
# 8. maintaining state in generators
maintain state in local vars, debugger for preview
continue preskace ostatak tela petlje i ide na sledecu iteraciju od pocetka
-----
# brzo prosao debugger, nista se ne razume
linq.ToList() fora da bypass lazy u debugger
# distinct - inner gen, take - outer gen
# list() force distinct() da sracuna sve u listu
take(3, list(distinct(items)))
# poenta - inner gen sracuna samo jedan sledeci elem umesto celu listu
# bilo bi logicno da se samo inner vrti dok sve ne izracuna sto ima
# u fju jednom udje i izadje za celu kolekciju
# u generator jednom udje i izadje za svaki element
---------------------------
# 9. lazyness and infinite
generatori izracunavaju 1 elem
pogodni za: beskonacna izracunavanja, sensor streams, matematicke redove, velike fajlove
ne treba im velika struktura - memorija
----
# fibonaci red primer, nista spec, kratko
-----------------------------
# 10. generator expressions
mix izmedju comprehension i generator functions
# () umesto [] za list comprh
(expr(item) for item in iterable)
----
# poenta kad instanciras generator on ne pozove nista, samo se spremi za next() koji racuna 1 po 1
million_squares = (x*x for x in range(1, 1000001))
-----
# poziv
million_squares # instanc gener obj
-----
# koriscenje gen comprh
# list() zovi do kraja
# uzmi zadnjih 10
list(million_squares)[-10]
----
# kad istrosis iterator vrati praznu listu []
list(million_squares)
------
# generators are single use objects
# moras novi da kreiras za opet
------
# stedi memoriju mnogo - poenta
# sum() je fja, ne treba nove () za gen expr, mada moze (())
sum(x*x for x in range(1, 10000001))
----
# moze i filter part sa predicat
sum(x for x in range(1001) if is_prime(x))
-----------------------
# 11. iteration tools - primeri, komplikovani
iteration - compreh, generators
range() - bound on 2 ends, generator on 1
ugradjene fje itertools module za rad sa iteratorima - kao Linq in C#
-----
# cela poenta generatora - za strimove i beskonacne podatke, i ustedu memorije na velikim, lazy
--------------------
# 12. summary
iterable object - can be iterated item by item
iterator = iter(iterable_object) - to get itreator from iterable object
next_item = next(iterator)
-----
generators are iterators
lazy - can model infinite series of data
---
# novo tumacenje
iterators se prosledjuju u next(iterator)
iterable == iterable object # verovatno
iterable object je ono sto seda u comprehension ili for loop
iterable object can be iterated item by item
iterator je kad je loaded, instanciran za item = next(iterator)
item = next(generator) # do sledeceg yield
generator = generator_fn() # instanciranje generatora, opet generator == generator object
generator expression = comprehension sa () # tuple, vraca generator object
------------------------------
## 10. classes
# 1. overview
custom types
---------------------------
# 2. classes - teorija samo
type(5) # vraca klasu
<class 'int'>
----
klasa - struktura i ponasanje
init state, attributes, methods
-----
klase dobre za complex, za prosto overkill
python oop ali ne primorava kao java i C#
----------------------------
# 3. defining classes - trivial
class MyClassName: # pascal case, zapazi, a vars snake_case
----
# minimal
class Flight: # defined once like fn...
    pass
<class 'filename.Flight'> # i klasa je objekat jbt
f = Flight() # classname je default constructor
type(f) je opet klasa
-----------------------------
# 4. instance methods
methods - fje def u klasi, ok
----
class Flight:
    
    def number(self): # self - identicna prica kao this
        return "sn060"
f.number() # isto sto i:
Flight.number(f) # pass self, radi ali ne koristi se
-------------------------------
# 5. instance initializers
class Flight:

    def __init__(self, number):
        if not is_valid_regex(number):
            raise ValueError(f"not valid")

        self._number = number # ovo kreira attribut _number takodje
    
    def number(self):
        return self._number

self je 100% identicno kao this u javi
----
# __init__ - initializer method
__init__ je vise setter nego constructor
ne kreira objekat vec konfigurise obj koji vec postoji
pravi construstor je u python runtime
-----
# ovo kreira attribut _number takodje
# isto kao sto i za variables ne treba dekleracija pre dodele # dobar, zapazi
self._number = number 
----
# naming _number
1. fja (chsharp prop) se vec zove number
2. private
-----
# poziv
f = Flight.number("sn060") # setter se poziva u constructoru
f.number()
f._number # radi, ali koristi se samo za debug
------
u pythonu je sve public
ne postoji private, protected, public
_my_attribute je dovoljna konvencija za private
--------
# invariants - ogranicenja nad objektom - setteri
# validacija u __init__ kao u setter
def __init__(self, number):
    if not is_valid_regex(number):
        raise ValueError(f"not valid")
-----------------------------
# 6. second class - bez neke poente
# attributi snake_case, klasa PascalCase, zapazi

class Aircraft:

    def __init__(self, registration, model, num_rows, num_seats_per_row):
        self._registration = registration
        self._model = model
        self._num_rows = num_rows
        self._num_seats_per_row = num_seats_per_row

    # getter
    def registration(self):
        return self._registration

    def seating_plan(self):
        return calc(_num_rows, _num_seats_per_row)
-----
# instanciranje
# pola pozicioni, pola named args u constructor, zapazi
a = Aircraft("g-eupt", "airbus a123", num_rows=22, num_seats_per_row=6)
------------------------------
# 7. collaborating classes - kratko, nebitno za python
law of demeter - OOP - least knowledge, ne treba da znas za klasu2
ne referenciraj objekat druge klase kroz obj prve klase
ne object1.object2 nego object1.object1_fja() koja zove object2.fja()
-----
moze decstring na klasu
f = Flight("ba75b", Aircraft("g-eupt", "airbus", num_rows=22))
f.aircraft_model()
--------------------------------
# 8. moment of zen
complex is better than complicated
--------------------------------
# 9. booking seats
nested comprehension 2 x for
matrica komplikovan primer, nebitno za python
----
attributi klase se definisu kao args __init__(), slicno kao Typescript
ne nego na self.attr1, mada mozda...?
--------------------------------
# 10. methods for implementation details
nastavak bezveze OOP primera sa matricom, bez poente
from file import method # importuje i klasu, ne samo fju
--------------------------------
# 11. object oriented design with function objects
OOP with function instead classes
nastavak komplikovanog primera, stampa pretty
# concat strings without newlines
output = f"first word" \
         f" second word"
---------------------------------
# 12. polymorphism and duck typing
# u pythonu polymorphism impl kroz duck typing
vise objekata razlicitog tipa kroz isti interfejs
svaki objekat (bilo kog tipa) sa istim interfejsom moze da seda na ista mesta
-----
# python
duck typing - ako pliva i hoda kao patka...
pogodnost objekta za upotrebu je odredjena samo u runtime
poenta: samo interfejsi da se poklapaju, nasledjivanje nebitno # to
----
staticki jezici - to kompajler odredjuje, tipovi
nasledjivanje tipova bitno
-----
# primer
2 klase bez nasledjivanja samo imaju isti interfejs (constructor i methods) i koristis koju hoces
---------------------------------
# 13. inheritance and implementation sharing
java - polimorfizam preko nasledjivanja
python - polimorfizam preko late binding, at runtime
-----
inheritance - za share implementation in python
common functionality in base class
fora -> base klasa "vidi" metode izvedenih, jer je runtime, base impl se zove samo u izvedenim, abstract class
-----
zbog duck typing inheritance se manje koristi u python, sto je dobro, tight coupling
-------------------------------
# 14. summary
9, 10, 11 komplikovan primer bez poente
----
svi tipovi su preko klasa u pythonu
constructor call - MyClass(args), nema new
prvi arg u metodima je self, isto ko this
nema modifikatora u pythonu - public, private, protected
moze vise klasa po fajlu, modulu # ok
ima jos mnogo o OOP u pythonu
-----------
# python klase, fore
# https://twitter.com/treyhunner/status/1567223537848532992
# https://www.pythonmorsels.com/screencasts/
# https://treyhunner.com/blog/archives/
---------------------------------
## 11. file io and resource managements
# fajlovi su bili prosti
# 1. overview
files - resources
resources - must be rleased or closed after use
text and binary mode
context managers (with), resources, kao using u C#
-----------------------------
# 2. opening files
open(file, mode, encoding) # encoding in text mode
binary mode - nema decoding, raw
text mode - encode, decode # default utf-8
---------------------------
# 3. writing text
f = open("my-file.txt", mode="wt", encoding="utf-8")
mode: r - read, w - write, a - append
selector (second letter): b - binary, t - text
f - file like object
f.write("this is my text\n") # returns number of codepoints, chars
# broj bytes ce biti razlicit na win i linux zbog \n npr
f.close() # ovo upisuje u fajl
# nema writeline
----------------------------
# 4. reading text
g = open("my-file.txt", mode="rt", encoding="utf-8")
g.read(32) # broj chars, postavlja file pointer na sl char, returns string
g.read() # cita do kraja
----
g.seek(0) # pomeri file pointer na pocetak
# za text mode tell() vraca validne vr za file pointer, ne moze bilo koji
-------
g.readline() # cita liniju ili do kraja
# \n python ima universal newline independant from os
-------
g.readlines() # cita sve linije fajla i vraca listu
g.close()
--------------------
# 5. appending text
mode="at"
h.writelines(["first line\n","second line\n"]) # arg iterable, list npr
--------------------
# 6. iterating over files
file objects support iterator protocol
iterac - sledeca linija, u for petljama
----
sys.stdout.write() # stream is file like object
# print() dodaje newline default
# umesto print() da izbegne duple newlines u std out
--------------------
# 7. closing files with finally
# otvaraj fajl u try catch
try:
    f = open(...)
    # return lines
finally:
    f.close() # i na error i na success mora da zatvoris
-----------------------
# 8. with blocks
# poenta ne moras rucno da zoves f.close() na svaki code path
open()
# work with file
close() # svaki open must be matched with close()
----
with - control flow structure for managing resources (using)
any objects that support context-manager protocol (disposable interface)
----
with open(filename, mode='rt', encoding='utf-8') as f:
    # stvara block koji automatski poziva f.close() na kraju
    f.writelines(...)
----------------------------
# 9. moment od zen
# beautiful is better than ugly - sintactic sugar
with je sintactic sugar za slozeniju try except strukturu, naveo je ovde
----------------------------
# 10. binary files
# primer sa rucno upisanom bitmapom, matricom
bitmapa - matrica pixela - int[][]
binarni fajlovi nemaju encoding
zabelezi pointere sa tell() pa ih posle upise na seek()
header, color palette i data
------------------------------
# 11. bitwise operators - ok, primer bin aritmetika
def _int32_to_bytes(i): # primer fja
conerts int to 4 bytes little endian
& - binarni and operator, rec and je boolean operator
>> - right shift, pomera udesno za n bitova
------------------------------
# 12. pixel data
mandelbrot fractal primer fja
sacuvao data u image.bmp i otvorio sliku
------------------------------
# 13. reading binary data
cita nekoliko bajtova iz fajla da procita dimenzije bitmape, primer
| - binarni or
-------------------------
# 14. file-like objects
files imaju previse varijacija za protokol
protokol - interface C#
EAFP pristup - probaj u try except
----
flo - file like object ovde u primeru samo
----
cita txt fajl na url, i http response je file like object, broji reci
flo.readlines() - zato je dobio listu linija
-------------------------
# 15. context managers
upotreba with sa tvojim class koje implem context manager protocol
# custom method u tvojoj klasi impl protocol
with closing(RefrigeratorRaider()) as r:
# ako je bio exception u drugoj metodi da se ipak zatvori fajl
------------------------
# 16. summary
# prevod
yield - produce, roditi (plod)
notion - pojam, predstava o necemu
rezime fajlova
ovo je kraj pocetnickog kursa
------------------------------
------------------------------


------------------------------
------------------------------
### Python beyond the basics - bolji path zapravo
## 1. prerequisites
python je veliki jezik
prerequisites - ponovio sva poglavlja iz osnovnog kursa
------------------------------
## 2. organizing larger programs
# 1. packages - folder
# ovo, zapazi, zaboravio
module - fajl - import daje objekat klase module
package - folder - stablo modula
-----
kad je nesto <class 'nesto'> znaci da je objekat tipa nesto
----
package ima __path__ install stazu, module nema
--------------------------
# 2. imports from sys path
sys.path - lista staza foldera - gde trazi module
sys.path[0] - current dir
----
random projekt folder nije u path pa ImportError
sys.path.append('my_proj_folder') # fix
----
PYTHONPATH - os PATH koje se default dodaju u sys.path
windows - ; separator medju stazama
linix, mac - : separator
-----
export PYHTONPATH=my_folder # linux, doda ga u sys.path
--------------------------
# 3. implementing packages
create module - create file.py
create package:
path_entry/ # must be in sys.path
    my_package/ # package root
        __init__.py # package init file, makes package module, kao index.ts
---------
reader.__file__ - ./reader/__init__.py # index fajl za folder, cini folder module, izvrsava se na import
-------
import folder.fajl # tako ide direktno za fajl
import nesto - definise nesto kao var (object)
--------------------------
# 4. subpackages
# ugnjezden folder koji ima __init__.py
# folder struktura:
reader/
    __init__.py # from reader.reader import Reader (klasa), prvi reader dir je u path
    reader.py
    compressed/
        __init__.py
        gzipped.py
-------
# importi
import reader # __init__
import reader.compressed # __init__
import reader.compressed.gzipped # fajl.py
--------------------------
# 5. example full program
bezveze kratak primer bez poente
--------------------------
# 6. relative imports
# pocinje sa . ili ..
do sad absolute imports - koriste PATH var
# relative imports
moze da import samo module (fajlove) u okviru istog paketa
mora from file import nesto sintaksa
----
# izostavlja se '/', ../, ./
from ..a import A # parent dir
from .b import B # same dir
----
farm/
    bovine/
        cow.py
        common.py # ruminate() ovde
from farm.bovine.common import ruminate # abs import, mora od package, dotle je path
from .common import ruminate # relative, same dir
from . import ruminate # relative, ceo dir u obj, ali mora common.ruminate()
-----
# poenta:
za izbegavanje nested a.b.c importa
relative treba izbegavati
-------------------------------
# 7. controlling imports with all
# poenta: koje vars su exportovane iz modula (fajla) sa *
locals() stampa dict {name: value} svih vars iz local scope
-----
__all__ - from module import * # lista importovanih vars
poenta: da importujes ne ceo modul nego samo neke fje iz modula, kad koristis *
-----
# primer
from reader.compressed.bzipped import opener as bz2_opener
from reader.compressed.gzipped import opener as gzip_opener
__all__ = ['bz2_opener', 'gzip_opener'] # export samo ove dve fje
---
# sad import foldera importuje samo ove dve fje
from reader.compressed import *
-----
cesto mora ctrl+d da izadje iz repl kad testira import za nove promene
-------------------------------
# 8. namespace packages - nije bas skroz jasno objasnio
pep420
----
package podeljen na vise foldera
nema __init__.py
-----
# ako ne nadje:
__init__.py - znaci normalan package
foo.py - module
znaci svi folderi u sys.path mogu biti delovi namespace package-a
-----
svi root sibling folderi moraju da budu u path
-----
# primer
path1/split_farm/bovine
path1/split_farm/bird
import split_farm # path je lista od 2 dir
# samo prvi child nema __init__.py, zapazi
-------------------------------------
# 9. executable directories
define entry point za folder
python3 folder_path
----
u root package-a kreiras __main__.py # entry point i moze run folder
za isporucivanje programa (package-a) koji su vise od jednog fajla
-------
python izvrsava zipovane foldere isto kao normalne foldere
-------------------------------------
# 10. recommended layout - ok
# konvencija za folder strukturu
----
project_name # project folder
    __main__.py # ako treba da bude executable
    project_name # package folder, same name as project folder
        __init__.py
        more_source.py
        subpackage1
            __init__.py
        test # testovi, subpackage isto
            __init__.py
            test_code.py
    setup.py
-------------------------------------
# 11. duck tails modules are singletons
# singleton u pythonu
moduli se pozivaju samo jednom i po redosledu pa mogu da sluze za singleton
privatna variabla definisana u modulu - singleton
----------------------------------
# 12. summary
python3 -m p1.mb # -m execute module, kad je rekao?
---------------------------------
---------------------------------
## 3. beyond basic functions
# 1. function review
# positional, keyword, default fn args
callable objects - functions, callable instances, lambdas
----
# argumenti
# definicija
def function_name(arg1, arg2, arg3=1.0):
    """Function docstring"""
    print("Function body")
    return (arg1 + arg2) / arg3
# poziv
function_name(arg1, arg2=1.618)
# positional, keyword odredjeni u pozivu, fora zapazi
positional argument - associated by order, moraju po redu
keyword argument - assoc by name, ne moraju po redu, ali posle positional
----
# keyword arg
formal_arg=actual_arg
def_var=local_var
---
# fora, zapazi
i positional moze da bude prosledjen kao keyword, samo match var name iz def, odredjeno u pozivu 
moze razlicito u razlicitim pozivima
-----
# default args
evaluated only once, at module from logging import exception
from platform import python_branch
import time, vazno, zapazi
mutable value ce da fail
----
function are first class, objects that can be passed or returned
----
def binds name za function object
------------------------------------
# 2. callable instances
# poenta: __call__ sluzi za kreiranje klasa cije instance mogu da se pozivaju kao fje
__call__ - omogucava svakom objektu da bude callable
------
# funkcije sa state, koji pamte od prethodnog poziva
# kesiranje primer
# __call__ metod je (pre)definisan pa resolver instanca moze da se zove sa () kao fja
import socket

class Resolver:
    def __init__(self):
        self._cache = {}

    def __call__(self, host): # self je prvi arg
        if host not in self._cache:
        self._cache[host] = socket.gethostbyname(host)
        return self._cache[host]
-----
# poziv
resolve = Resolver()
resolve('site.com') # instanca moze da se zove sa ()
resolve.__call__('site.com') # je syntax sugar za ovo zapravo
-----
_ je prethodna vrednost u repl
----
prvi poziv duzi http, posle brze iz kesha, ok nebitno
----
clear() i has_host() metode, nebitno
----
# poenta, rekao na kraju:
__call__ sluzi za kreiranje klasa cije instance mogu da se pozivaju kao fje
koristi se kada zelis fje koje cuvaju state izmedju poziva i query ga
----------------------------------------
# 3. classes are callable - constructor
resolve = Resolver() # klasa se poziva, constructor, ocigledno
---
u pythonu klasa je objekat class object tipa
---
args constructora prosledjeni u __init__() method
---
cls - var name umesto reserved class, trivial
----------------------------------------
# 4. conditional expressions - ternary expression
pep 308
----
# conditional statement - if naredba jbt
if condition:
    result = true_value
else:
    result = false_value

# conditional expression - ternary expression
result = true_value if condition else false_value
----
# poenta: zapazi uslov je U SREDINI, truthy je napred, a falsy pozadi, smesno
namera, da bi zvucalo ko engleski "to ako ovo inace"
---------------------------------------
# 5. lambdas
anonimna funkcija bez def, imena i bloka
obicno kao arg fje
-----
lambda calculus - osnova za func jezike
-----
praktican razlog, za fju koja se jednom koristi da ne pises definiciju
----
# primer
# sortiraj naucnike po last_name, split()[-1] hvata zadnju rec
# name - arg, : - body of lambda
sorted(scientists, key=lambda name: name.split()[-1])
----
# dodeli u promenljivu, kao arrow func
last_name = lambda name: name.split()[-1]
# moze da se zove sa ()
last_name('nikola tesla')
# identicno kao normalna func
def last_name(name):
    return name.split()[-1]
------
# funkcija vs lambda
funkcija sa def name() - statement, name, args - () i ,
mora return za vracanje (osim None)
docstrings
moze testiranje
----
lambda - expression, anonymous, arg1, arg2: - zavrsava :, razdvojeno zarezima, bez ()
bez args - lambda:
no block, single expression, no statements
ne sme return rec, value of expression
ne sme da ima docstrings
ne moze da se testira, nema ime ref, mora biti jednostavna
-----
# razluci statement vs expression???
--------------------------------------
# 6. detecting callable objects
callable() - boolean helper, da li je obj callable
----
# callable objects:
1. funkcije (sa def name():)
2. lambde
3. class objects - constructors
4. methods - func in class
5. obj instances with __call__() defined
---
is_odd = lambda x: x % 2 == 1 # primer lambda sintaksa
--------------------------------------
# 7. extended formal argument syntax - def extended(*args, **kwargs)
# slicno kao fn(...args) pa args[] u javascriptu
# poenta:
*args - uhvacen kao tuple - js polje
**kwargs - uhvacen kao dictionary - js object
nema veze sa POINTERIMA
----
# formal args - u definiciji
def extended(*args, **kwargs):
-----
# actual args - u pozivu
extended(*args, **kwargs)
-----
# primeri
print() - prima 0 - n args
"{a}<===>{b}".format(a="Oslo", b="Stavagner") - format() fn, positional, keyword args
-----
def hypervolume(*args): # args var_name konvencija
    print(args)
    print(type(args))
# log:
hypervolume(3, 4)
(3, 4) # args fje su tuple, slicno kao args[] u js
<class 'tuple'>
-----
# primer: povrsina kvadrata, zapremina kocke i hiperkocke
# hypervolume() primer samo mnozi sve args
def hypervolume(*lengths):
    i = iter(lengths)
    v = next(i) # pomesao iterator i petlju, zasto?
    for length in i:
        v *= length
    return v
-----
# moze da pocinje sa positional, pa hvata ...rest args
def hypervolume(length, *lengths):
------
# eto, jasno
*args - hvata postional args
**kwargs - hvata keword args
-----
# primer log:
def tag(name, **kwargs):
    print(name)
    print(kwargs)
    print(type(kwargs))
# poziv
tag('img', src="monet.jpg", alt="some description", border=1)
img
{'border': 1, 'alt': 'some description', 'src': 'monet.jpg'} # dictionary, js object
<class 'dict'> # tip
poenta: **kwargs - uhvacen kao dict - js object
arg var names u pozivu su keys
------
# primer **kwargs
def tag(name, **attributes):
    result = '<' + name
    for key, value in attributes.items():
        result += ' {k}="{v}"'.format(k=key, v=str(value))
    result += '>'
    return result
# poziv
tag('img', src="monet.jpg", alt="some description", border=1)
'<img border="1" alt="some description" src="monet.jpg">' # u dict naravno redosled nije odredjen
-----
# mixovanje positional, keword i *args, **kwargs (star args)
# sintaksa, pravila
1. nije dozvoljeno, **kwargs ne sme prvi
def print_args(**kwargs, *args):
2. svi ispred *args su obicni positional
def print_args(arg1, arg2, *args):
3. svi posle *args su obavezni keyword args; *args hvata ...rest positional args
def print_args(arg1, arg2, *args, kwarg1, kwarg2):
4. **kwargs mora biti zadnji (ako ga ima); **kwargs hvata ...rest keyword args
def print_args(arg1, arg2, *args, kwarg1, kwarg2, **kwargs):
----
plus pravila za optional default args
-----
*args, **kwargs - vazi za funkcije, lambde i sve callable
---------------------------------
# 8. extended actual argument syntax - poziv
# spread args u pozivu - fn([...args]) ili fn({...args})iz js
# poenta
fn(*t) - fn([...t])
fn(**k) - fn({...k})
------
extended(*args, **kwargs)
spread tuple za positional, iterable, moze i lista verovatno
spread dict za keyword
------
# positional primer
def print_args(arg1, arg2, *args):
t = (11, 12, 13, 14)
print_args(*t) # * means unpacking here (destructuring) [...t]
11
12
(13, 14)
*args moze samo u pozivu, a da nije u definiciji
--------
# keyword primer
def print_args(arg1, arg2, arg3, **kwargs):
k = {'arg1': 21, 'arg2': 68, 'blue': 120, 'alpha': 52}
print_args(**k) # unpacking with **, zapazi - samo spread k {...k}
arg1 = 21
arg2 = 68
arg1 = 120
{'alpha': 52}
----
isto **kwargs moze samo u pozivu, a da nije u definiciji
------
# dict contructor to create dict from keyword args
k = dict(red=1, green=2, blue=3)
{'red': 1, 'green': 2, 'blue': 3}
-----------------------------------------
# 9. forwarding arguments - all args
pass all arguments from fn1 to fn2 bez potrebe da znas signature of fn2
-----
# primer
def trace(fn, *args, **kwargs):
    return fn(*args, **kwargs)
# poziv, int()
trace(int, "ff", base=16)
args = ('ff',)
kwargs = {'base': 16}
pohvatao sve args i prosledio u fn
---------------------------------------
# 10. duck tail transposing tables - transpose matrix
# poenta: zip(*x) primer
zip() mapira dve paralelne liste u parove tuple (list1[i], list2[i])
moze i 3 paralelne liste, duzina listi mora da se poklapa
zip() prima promenljiv broj args
------
transposed = list(zip(*lists)) # mora list() jer parovi nisu lista pa mora for
zip(*arg) - transpose matrix
---------
poenta: sve ovo sa *args, *kwargs je spread i rest args iz javascripta, nista specijalno
---------------------------------------
# 11. summary
callable - ima () operator, moze da se pozove
klase zove class object
lambda - single expression body
callable() - :boolean
*args, **kwargs - ne moraju u def ako ih ima u pozivu
unpacking - destructuring
*args, **kwargs - NISU POINTERI nego SPREAD, REST args
------------------------------------
------------------------------------
## 4. closures and decorators
# 1. local functions
def my_fn(): # def se izvrsava at runtime
zato se lokalna fja predefinise na svaki poziv parent fje, nova instanca
kao i svaka var u fji
----
zapazi: globalna fja se definise at runtime, lokalna redefinisana na svaki poziv spoljne fje # to, to
----
# scopes
LEGB - local, enclosing, global, built-in
scope fje - local vars + arguments
----------------------------------
# 2. returning functions from functions, trivial
# fja moze da se vrati, dodeli i pozove
local_fn = parent_fn()
local_fn()
----------------------------------
# 3. closures and nested scopes - koristi var iz parent fje
# closure - localna fja koja ref vars ili args iz parent fje
def outer():
    x = 3 # ovo je poenta 
    def inner(y):
        return x + y # local fja koristi var is parent fje  
    return inner # moras da vratis lokalnu fju, zapazi, eto

i = outer() # local fja u i var, eto poziv
------
closure - svi scope-ovi koje je local fja koristila se drze alive, prevent garbage collection
maintain references to objects from earlier scopes
------
lf.__closure__ - postavljen na vars refs iz parent fje, mora 2 fje
----------------------------------
# 4. function factories - ok
function factory - function that returns new specialized functions, state...
pozivom parent fje sa args kreiras nove varijante lokalne fje i vracas je, pa nju koristis
jeste state, cuva se vrednost local var (ili arg) parent fje u vracenoj lokalnoj # to, to
----
# primer, dobar
def raise_to(exp):
    def raise_to_exp(x):
        return pow(x, exp)
    return raise_to_exp
----
# poziv
square = raise_to(2)
square(5) # 2 je zapamcen u local var kao state
square(9)
cube = raise_to(3) # 3
cube(3)
----------------------------------
# 5. nonlocal keyword, ok - vars in scopes

# primer, kada imas poklapanje isto ime u vise scopea
# nova var, ne menja se parent var
message = 'global' # netaknuta od obe
def enclosing():
    message = 'enclosing' # netaknuta
    def local():
        message = 'local' # nova var u novi scope

    print('enclosing message', message) # 2
    local() # pozvao fju da proba da promeni parent var
    print('enclosing message', message) # 3

print('global message', message) # 1
enclosing() # pozvao fju da proba da promeni parent var
print('global message', message) # 4
----
# poziv
global message: global
enclosing message: enclosing
enclosing message: enclosing
global message: global
------
global - keyword, from global namespace into local (koje god da je)
i u normalnoj funkciji i u lokalnoj funkciji, ali original iz global (module) scope
----
message = 'global' # ovaj
def enclosing():
    message = 'enclosing'
    def local():
        global message
        message = 'local' # bindovan za global namespace
------
nonlocal - keyword za enclosing (parent fja) namespace
syntax error ako var ne postoji u parent funkciji
----
message = 'global' 
def enclosing():
    message = 'enclosing' # ovaj
    def local():
        nonlocal message
        message = 'local' # bindovan za parent scope
-------
global i nonlocal ne treba cesto da se koriste
-------
komplikovan i nevazan primer sa tajmerima koji drze state u closure
------------------------------------
# 6. function decorators - syntax sugar za higher order functions
decorators - modify or enhance functions without changing their definition
decorator - callable that accepts callable and returns callable - higher order function
mora da primi i vrati funkciju
-----
@my_decorator
def my_function(x, y):
    return x + y
------------------------------------
# 7. first decorator example - hof
decorator - normalna fja koja prima fju
----
def vegetable():
    return ascii('blomkål') # da ne bi svaku fju modifikovao, pipe output...

def animal():
    return ascii('bjørn')
----
# decorator
def escape_unicode(fn): # ovo je decorator - fja, prima fju
    def wrap(*args, **kwargs): # sve args forward
        x = fn(*args, **kwargs) # pozovi
        return ascii(x) # umotaj

    return wrap # vrati lokalnu fju, rezultat decoratora mora da bude fja koju zelis
# poziv
@escape_unicode # propusti fju kroz pipe
def northern_city():
    return 'Tromsø'
-----
1. napravi lokalnu fju, u njoj pozovi originalnu fju
2. modifikuj args ili rezultat
3. vrati lokalnu fju za upotrebu
------------------------------------
# 8. what can be a decorator
any callable, class npr - constructor
dekorisana fja se menja klasom
fja se prosledjuje u constructor i initializer
__call__ - implement da instanca bude callabilna, podsetnik
__call__ - pretvara instancu u funkciju, tu implem final fju, tj zoves original fju i modifikujes args ili rezultat
__call__ - tu implementiras lokalnu fju iz primera gore, args res poziv
-----
# klasa je decorator, fja ide u constructor
class CallCount:
    def __init__(self, fn): # ovde je fja prosledjena
        self.fn = fn # i constr i ovde kreira atribut klase, kao typescript
        self.count = 0

    # ovde implementiras lokalnu fju iz primera gore
    # final fju koja ce biti vracena
    def __call__(self, *args, **kwargs): # forward all args
        self.count += 1 # side effect, state
        return self.fn(*args, **kwargs) # poziv
----
# poziv
@CallCount # fju dekorises klasom, moze, fja prosledjena u __init__()
def hello(name):
    print('Hello, {}'.format(name))
------------------------------------
# 9. instances as decorators - @Klasa() ili @instanca
pozvana klasa (), instanca, zapazi, umesto klasa to sad
# poenta: vrati lokalnu fju iz __call__()
----
class AnotherDec:
    def __call__(self, f):
        def wrap():
            . . .
        return wrap # koristi lokalnu fju vracenu iz __call__, ok
---
@AnotherDec() # pozvan constructor (), zapazi, instanca
def func()
----
svrha - collection of decorated functions
-------
atributi klase cuvaju state koji menja ponasanje postavljenoog decoratora tokom upotrebe
postavis atribut kad hoces da promenis
-----
# primer
class Trace:
    def __init__(self):
        self.enabled = True # state

    def __call__(self, f):
        def wrap(*args, **kwargs):
            if self.enabled:
                print('Calling {}'.format(f))
            return f(*args, **kwargs) # poziv
        return wrap # vrati lokalnu fju iz __call__()

tracer = Trace() # instanciraj
@tracer # dekorisi instancom, koristi je kasnije za config
def rotate_list(l):
    return l[1:] + [l[0]]
----
# poziv
l = [1, 2, 3]
l = rotate_list(l)
tracer.enabled = False # decorator instanca
l = rotate_list(l)
------------------------------------
# 10. multiple decorators - pipe-ing
decoratori samo MIDDLEWARE express
decorator vrati kroz originalnu fju
-----
# na gore, prvo 3, pa 2, pa 1 vrati kroz orig fju
@decorator1
@decorator2
@decorator3
def some_function():
------------------------------------
# 11. decorating methods - samo fja u klasi
# decorate method klase, globalan decorator, trivial
class IslandMaker:
    def __init__(self, suffix):
        self.suffix = suffix

    @tracer # eto
    def make_island(self, name):
        return name + self.suffix
------------------------------------
# 12. functools wraps - forward metadata sa orig fje na vracenu lokalnu fju
decorator menja funkciju drugom fjom
metadata funkcije - vrednosti za help(obj) - __name__, __doc__ i druge ugradjene vars
vide se metadata vracene fje, a ne originalne
------
prosledjenu fju naziva f umesto fn u javascriptu
-----
# @functools.wraps(f1) decorator za forward metadata sa f1 na f2
import functools

def noop(f):
    @functools.wraps(f) # forwards metadata from f na noop_wrapper
    def noop_wrapper():
        return f()
    return noop_wrapper
---
@noop
def hello():
    "Print a well-known message."
    print('Hello, world!')
---------
ne koristiti decoratore previse
------------------------------------
#  13. duck tails validating arguments - fja vraca decorator
upotreba dekoratora npr: validacija args
decorator uvek prima i vraca fju
-----
# primer
def check_non_negative(index): # nije decorator, prima index, vraca fju
    def validator(f): # prima fju, ovo je decorator, koji je vracen
        def wrap(*args): # index i f su closure vars iz spoljnih scope-a
            if args[index] < 0: # validira arg veci od 0
                raise ValueError(
                    'Argument {} must be non-negative.'.format(index))
            return f(*args)
        return wrap
    return validator

# 1 - drugi arg > 0
@check_non_negative(1) # zapazi poziv (), vraca decorator
def create_list(value, size):
    return [value] * size # mnozi listu 3 puta, nebitno
------
# poziv
create_list('a', 3) # 3 se validira, 1 je u state - closure
['a', 'a', 'a']
------------------------------------
# 14. summary
def - executed at runtime
local function - def fn unutar druge fje
lokalna fja se rekreira na svaki poziv spoljne fje, kao i sve vars
fja vidi vars iz parent scope
---
closure - vars is parent scope koje ref local fja, zive dok i local fja, ne da garbage collection
var.__closure__ - refs vars
----
factory functions - fja koja kreira drugu fju
----
decorators - menjaju ponasanje fje bez menjanja orig fn definicije
decorator - prima 1 fju i vraca 1 fju, zapazi
@ je poziv decoratora
-----
vise decoratora - odozdo na gore, donji prvi
-----
# to, to, zapazi
dodatni args za decorator - fja koja VRACA decorator - kao onClick u React 
decorator samo 1 arg - fja
-----
closure moze za var iz bilo koji parent scope
------------------------------------
## 5. properties and class methods
# 1. class attributes - static attribute
instance attributes - per instance basis, self.nesto u __init()__
class attributes - per class, isti za sve instance (static u javascriptu)
--------
# primer
class Foo
    class_attribute = 1 # eto

    def __init__(self):
        self.instance_attribute = 2 # eto
-----
# primer
class ShippingContainer:

    next_serial = 1337 # postavljena vrednost ovde

    def __init__(self, owner_code, contents):
        self.owner_code = owner_code
        self.contents = contents
        # mora klasa, ne moze self
        self.serial = ShippingContainer.next_serial
        ShippingContainer.next_serial += 1
# pristup
obj.serial # isti za sve instance, vezan je na klasu, static js
------------------------------------
# 2. static methods - @staticmethod decorator
sto hoces static kacis na klasu, uglavnom poenta
@staticmethod decorator
-----
class ShippingContainer:

    next_serial = 1337

    # static getter, _private
    @staticmethod
    def _get_next_serial():
        result = ShippingContainer.next_serial
        ShippingContainer.next_serial += 1
        return result

    def __init__(self, owner_code, contents):
        self.owner_code = owner_code
        self.contents = contents
        # sad getter ovde
        self.serial = ShippingContainer._get_next_serial()
-----
# pristup - i na klasi, i na instanci
obj.serial
ShippingContainer.next_serial
------------------------------------
# 3. class methods - staticka fja koja ref klasu, ili ne uopste
methods - static, class, instance # class je novo
----
@staticmethod vs @classmethod, ok
@classmethod - ako u fji ref klasu (cls - ShippingContainer) - ovo je staticka fja iz C#
@staticmethod - ni self, ni cls - bas prazna fja koja moze i van klase u module scope
class object - klasa - class name
instance object - instanca
----
named constructors - factory function - constructor sa razlicitim imenom od klase
sluze za instanc objekata sa razlicitim args, bez da clutter __init__()

----
# primer
class ShippingContainer:

    next_serial = 1337

    # ovo je classmethod, prima i koristi klasu
    @classmethod
    def _get_next_serial(cls): # cls - class - ShippingContainer
        result = cls.next_serial
        cls.next_serial += 1
        return result

    # ovo je named constructor - prima klasu cls i poziva je kao constructor
    # i vraca instancu
    @classmethod
    def create_empty(cls, owner_code, length_ft, *args, **kwargs):
        return cls(owner_code, length_ft, contents=None, *args, **kwargs)

    @classmethod
    def create_with_items(cls, owner_code, length_ft, items, *args, **kwargs):
        return cls(owner_code, length_ft, contents=list(items), *args, **kwargs)

    def __init__(self, owner_code, length_ft, contents):
        self.contents = contents
        self.length_ft = length_ft
        self.serial = ShippingContainer._get_next_serial()

-----
# poziv je na klasu, kao staticka fja
# cls se ne prosledjuje, zapazi, tu seda self
obj = ShippingContainer.create_with_items("mae", ["abc", "def"])
------------------------------------
# 4. static methods with inheritance - mora self u __init__ base klase
static methods can be overriden in subclasses, u drugim jezicima ne moze
# base klasa
class ShippingContainer:

    next_serial = 1337

    # ovu staticku fju ce da overriduje
    @staticmethod
    def _make_bic_code(owner_code, serial):
        return iso6346.create(owner_code=owner_code,
                              serial=str(serial).zfill(6))

    def __init__(self, owner_code, length_ft, contents):
        self.contents = contents
        self.length_ft = length_ft

        # ovde mora self._make_bic_code() da bi ta fja mogla da se overriduje
        # sa ShippingContainer._make_bic_code() ne moze
        self.bic = self._make_bic_code(
            owner_code=owner_code,
            serial=ShippingContainer._get_next_serial())

# izvedena klasa
class RefrigeratedShippingContainer(ShippingContainer):

    # overriden staticka fja
    @staticmethod
    def _make_bic_code(owner_code, serial):
        return iso6346.create(owner_code=owner_code,
                              serial=str(serial).zfill(6),
                              category='R')
-------------------------------------------
# 5. class methods with inheritance
poenta: named constructors iz base ce da pozivaju __init__() iz izvedene
i mora da hendluju agrs koje moze da posalje
----
super() vraca base klasu
---
# mozes da zoves named constructor iz base klase pozivanjem izvedene
# definisan je i u izvedenoj
r1 = RefrigeratedShippingContainer.create_empty("yml")
-----
__init__() iz base klase se ne zove automatski, ti moras da ga pozoves
-----
# base klasa
class ShippingContainer:

    # cls poziva i __init__
    # *args, **kwargs mora da hendluje args koje moze da posalje
    # __init__() iz izvedene klase
    @classmethod
    def create_empty(cls, owner_code, length_ft, *args, **kwargs):
        return cls(owner_code, length_ft, contents=None, *args, **kwargs)

# izvedena klasa
class RefrigeratedShippingContainer(ShippingContainer):

    def __init__(self, owner_code, length_ft, contents, celsius):
        # poziv parent initalizera
        super().__init__(owner_code, length_ft, contents)
        self.celsius = celsius
-------------------------------------------
# 6. properties - @property getter i @name.property setter
@staticmethod, @classmethod, @properties - sve implementirano preko decoratora
----
property - isto ko C# fja koja se poziva kao atribut, getteri i setteri
r4.celsius # citanje
r4.celsius = 5 # dodela
-----
# getter i setter preko decoratora
class Example:

    # getter
    @property
    def p(self):
        return self._p

    # setter, p se odnosi na ime gettera
    @p.setter
    def p(self, value):
        self._p = value
----
setter je property na property objektu, pogledaj sliku, jasno
jer getter originalno kreira property, decorator vraca property object - fju
--------------
# dodela i citanje property-ja unutar klase
# celsius je originalan property, fahrenheit je novi property
self.celsius - citanje
self.celsius = nesto # dodela, sve logicno
----
_c_to_f() je samo staticka fja
----
@property
def fahrenheit(self):
    return RefrigeratedShippingContainer._c_to_f(self.celsius)

@fahrenheit.setter
def fahrenheit(self, value):
    self.celsius = RefrigeratedShippingContainer._f_to_c(value)
-----
# validac ogranicenja su u setterima, nista spec
@celsius.setter
def celsius(self, value):
    if value > RefrigeratedShippingContainer.MAX_CELSIUS:
        raise ValueError("Temperature too hot!")
    self._celsius = value
-------------------------------------------
# 7. properties and inheritance - override props u izvedenoj
# nasledjivanje
class HeatedRefrigeratedShippingContainer(RefrigeratedShippingContainer):
------
# override property getter
# samo isto ime u izvedenoj klasi
# ref super().volume_ft3 prop iz parent ako treba
@property
def volume_ft3(self):
    return super().volume_ft3 - RefrigeratedShippingContainer.FRIDGE_VOLUME_FT3
--------
# override property setter - nista speciajlno
# @prop.setter - prop (getter) ne postoji u izvedenoj
# mora klasa.prop.setter - ceo namespace
@RefrigeratedShippingContainer.celsius.setter
def celsius(self, value):
    # ovo je samo ogranicenje
    if value < HeatedRefrigeratedShippingContainer.MIN_CELSIUS:
        raise ValueError("Temperature too cold!")
    # access setter fju iz parent klase
    RefrigeratedShippingContainer.celsius.fset(self, value)
----
# chained operator
a < b < c umesto (a < b) and (b < c)
-------------------------------------------
# 8. duck tail template method pattern - delegate override from prop to function
template method - to je samo predefinisanje fje u izvedenoj klasi - polimorfizam, trivial
-----
# _part1(), _part2(), _part3() - ili nedefinisane, ili exception ili trivial implementtion
class AbstractClass:

    def template_method(self):
        self._part1()
        self._part2()
        self._part3()

    def _part2(self):
        raise NotImplementedError("Override this method")

    def _part3(self):
        # Optionally override this
        print("Done!")

# _part1(), _part2(), _part3() - implementiras samo, to je to
class ConcreteClass(AbstractClass):

    def _part1(self):
        print("About to perform action")

    def _part2(self):
        perform_action()

    def _part3(self):
        print("Action performed!")

-----
property getter ili setter samo zove privatnu fju u base klasi, a ta fja moze biti overriden u izvedenoj
poenta: override fju, a u prop u base samo pozivaj fju
delegate implementation from property to function koja se lakse overriduje sintaksno
-----
method - prima (self) kao prvi arg, ne zaboravi
-----
# abstract class, ne moze da se instancira
# implem fja postoje samo u izvedenoj
# definise property-je
class ShippingContainer:

    #  getter
    @property
    def volume_ft3(self): # property
        return self._calc_volume() # prazan PRIVATAN template method, bice implem u izvedenoj klasi

    # setter
    @celsius.setter
    def celsius(self, value):
        self._set_celsius(value) # prazan PRIVATAN template method,

class RefrigeratedShippingContainer(ShippingContainer):

    #  obican privatan metod koji gadja getter u base
    def _calc_volume(self):
        # super()._calc_volume() poziva method u base
        return super()._calc_volume() - RefrigeratedShippingContainer.FRIDGE_VOLUME_FT3

    #  gadja setter u base
    def _calc_volume(self, value):
        # method u base
        return super()._set_celsius(value)
------------------------------------------
# 9. summary
class (static, ne moze kroz self.nesto =) i instance attributes
@staticmethod - niti ref class niti instance
@classmethod - ref class
named constructors, factory fn - je @classmethod jer zove cls constructor
# properties - za read i assign attributes
@property - getter 
@prop_getter.setter - setter
lakse override private method nego prop, sintaksno
-----------------------------------------
## 6. strings and representations
# 1. two strings representations
# convert objects to string - toString(), JSON.stringfy()
str() - poziva __str__()
repr() - poziva __repr__()
----
__str__() - simple format
__repr__() - kompletniji, nedvosmislen format
-----------------------------------------
# 2. repr()
complete representation of object, cak i src code objekta
namena - debugging, logging
---
repr() for developers
str() for clients, users
----
__repr__() za klasu implementiras (override default) za debug
slicno kao JSON.stringfy(obj)
------------------------------------------
# 3. str()
citljiv, human friendly representation of an object, koncizan
constructor za string
------------------------------------------
# 4. when are representations used
print(obj) stampa str() izlaz
ako __str__() nije implementiran - default str() zove repr() 
ako __repr__() nije implementiran - stampa ref pointer python
-----
# collections
ako je deo liste, dict itd - both str() i repr() zovu repr()
------------------------------------------
# 5. interaction with format
"this is a point: {}".format(Point2D(1, 2))
__format__() se zove - default zove str() - moze da se predefinise
----
"{!r}".format(Point2D(1, 2)) - ! zove repr() default, nije mnogo bitno
{!s} - str()
-----
# primer
# short
def __str__(self):
    return '({}, {})'.format(self.x, self.y)

# verbose
def __repr__(self):
    return 'Point2D(x={}, y={})'.format(self.x, self.y)

# f je format options
def __format__(self, f):
    if f == 'r':
        return '{}, {}'.format(self.y, self.x)
-------
repr() najvazniji (uglavnom dovoljan), pa str(), pa format()
-------
podsetnik - __nesto__() su default operatori na klasi
------------------------------------------
# 6. reprlib - custom module
alternativa za repr()
ogranicenje broja linija u outputu
reprlib.repr()
----
2 x for u comprehension je nested loops
stampa prvih 100-nak chars pa ... elipsis
----
deo Repr.reprlib klase, singleton fja
------------------------------------------
# 7. ascii, ord and chr
ostale string fje, builtin python
ascii() - replaces non-ASCII characters with escape sequences
x = "haelo" - ae unicode npr
y = ascii(x) # stampa "'h\\xe6llo'" utf-8 je string, nije ascii
----
# suprotne jedna od druge
ord() - Converts an integer Unicode codepoint to a single character string
x = 'unic'
ord(x) - 190 - int code za unicode - codepoint
---
chr() - converts a single character to its integer Unicode codepoint
chr(190) - 'unic' - vratio ga nazad
------------------------------------------
# 8. bigger isnt always better
izuzetak kad je repr() kraci od str()
za tabelu samo header, nebitno
------------------------------------------
# 9. summary
repr() - unambiguous, precise, include type, for devs, uvek impl u tvojoj klasi
str() - for humans
------------------------------------------
## 7. numeric and scalar types
# 1.reviewing int and float
int - unlimited precision signed integer, drugi jezici je fiksan na 32 npr
factorial(1000) velik broj a radi
-----
float - 64 bits, double in C
1 - znak
11 - exponent
52 - fraction, mantisa - decimale
-----
53 binary precision, 15-17 decimal precision
15 int cifara u float i nazad bez gubitka
----
sys.float_info - pozitivni brojevi granice
-sys.float_info.max - negativni brojevi granice
-sys.float_info.min
-----
int se ne moze konvertovati u float bez gubitka informacija
moze mali
2**53 - int
float(2**53 + 1) - ne radi tacno 
0.8 - 0.7 = 0.000000009 jer 0.8 i 0.7 nisu potpuno precizni
float point arythmetics
------------------------------------
# 2. decimal module and decimal type
decimal.Decimal klasa za tacno zaokruzenu aritmetiku sa osnovom 10
------
decimal floating point
configurable finite precision
default 28 cifara decimal precision
-----
decimal.getcontext() - citaj config
Decimal('0.7') - constr prima broj ili string
----
Decimal('0.8') - Decimal('0.7') = 0.1
Decimal(0.8) - Decimal(0.7) = 0.1000000000000000888178419700 opet
bez '' convertuje iz base 10 float u base 2 float pa isto
uvek koristi string, ok
------------
dosta detalja...
-----------
Decimal('Infinity')
Decimal('-Infinity')
Decimal('Nan')
----------
(-7) % 3 = 2 - prema minus beskonacno
Decimal(-7) % Decimal(3) = -1 - prema nuli
# zato mora
def is_odd(n):
    return n % 2 != 0 # umesto n % 2 == 1
-----
x == (x // y) * y + x % y # celobrojni + ostatak
----
# opet detalji, celobrojno deljenje
(-7) // 3 = -3
Decimal(-7) // Decimal(3) = Decimal('-2')
----
math.fje() ne mogu na Decimal
------------------------------------
# 3. rational number with the fraction type
2/3 - razlomci 0.66666...
numerator/denominator
from fractions import Fraction
two_thirds = Fraction(2, 3)
ne sme deljenje nulom
Fraction(Decimal('0.1')) # moze razni constructori, float, string
+-*/%// operatori
----------------------------------------
# 4. complex type and cmath module - kompleksni brojevi
# u jezik ugradjen tip za kompleksne brojeve, matematika
j - koren iz -1, const
3 + 4j - complex type, single object
complex(-2, 3) - -2 + 3j - constructor
complex('-2+3j') - moze i string, bez space
c.real
c.imag
c.conjugate() - konjugovana vrednost
----
math.sqrt(-1) - exception
cmath.sqrt(-1) - ok
----
polarne koordinate
cmath.phase(1+1j)
abs() - moduo
cmath.polar()
# sve operacije sa complex brojevima podrzane
primer kolo sa cond i kalem, nebitno
----------------------------------------
# 5. built-in numeric functions abs and round - ugradjene fje
abs(-5) - apsolutna vrednost, daljina od nule
round() - decimalno zaoukruzivanje - 5
round(0.2812, 3) - 3 - broj decimala - 0.281
----
0.5 zaokruzuje prema parnom broju, zapazi
round(1.5) = 2
round(2.5) = 2
samo za float, na int nista ne menja
round(Decimal('3.25'), 1) = Decimal('3.2')
round(Fraction(57, 100), 1) = Fraction(3, 5) # zapazi
round za complex ne postoji
round nepredvidiv za float, koristi Decimal
----------------------------------------
# 6. number base conversion - brojna osnova
# bin(), oct(), hex(), int(x, base)
----
0b10101 - 0b binarni
0o52 - 0o oktalni
0x2a - hexadecimalni
bin(42) = '0b101010'
oct(42) = '0o52'
hex(42)[2:] = '2a', makne 0x
int('2a', base=16) = 42
arg1 - 0-9, a-z, arg2 - 2-36
bez prefixa default decimal, logicno
----------------------------------------
# 7. datetime module and date type
date, time, datetime, timezone, timedelta
immutable all
-----
datetime(year=2014, month=1, day=16)
datetime(2014, 1, 16)
-----
mogu biti 1 i 0 based - iso
d = datetime.date.today()
d.year
d.month
d.day
d.weekaday()
-----
d.strftime('%A %d %B %Y') # Thursday 27 February 2014
"{%A %d %B %Y}".format(d) # moze i str.format, zavisi od OS
-------------------------------------------
# 8. time type
datetime.time(hour=23, minute=59, second=59, microsecond=999) # moze i positional args naravno
t.hour, t.minute, t.second, t.microsecond
t.isoformat(), tstrftime(), str.format() # u string
-------------------------------------------
# 9. datetime type - razne fje, kao docs 
datetime.datetime - module.class - isto se zovu
datetime(), datetime.today(), datetime.now(), datetime.utcnow()
datetime.combine(date, time)
# zajedno datum i vreme, date i time
datetime.strptime(dt_str, format_str)
-------------------------------------------
# 10. durations with timedelta type
# vremenski interval
timedelta() - constructor, koristi named args
args - days, seconds, microseconds, miliseconds, minutes, hours, weeks
days, seconds, microseconds - samo ovo instanca cuva
str(td) - samo za debug
-------------------------------------------
# 11. arithmetic with datetime - kratko
datetime_a - datetime_b
timedelta1.total_seconds()
datetime.date.today() + timedelta1 * 3
na time ne moze +- itd
-------------------------------------------
# 12. time zones
politika i moze da se menja
time + tzinfo - abstract class
cet = datetime.timezone(datetime.timedelta(hours=1), "CET")
datetime(year=2014, month=1. day=7, hour-13, minute=5, tzinfo=cet)
-------------------------------------------
# 13. duck tail floating point vs rational numbers
# poenta za exact calc Fraction umesto float
geometrija
3 tacke na liniji, determinanta, primer
----
# sa int operatorima
True = 1
False = 0
---
a = (0, 0) - tacka, tuple
----
float finite precision problem
Fraction() constructor - exact arithmetic
----
odstampao bmp sa oba, dobar
-------------------------------------------
# 14. summary
int
float - binary, finite precision
Decimal - base 10 configurable precision, finansijske aplikacije
% i // prema 0 za Decimal
Fraction - razlomci
-------------------------------------------
## 8. iterables and iteration - map, filter i dr fje za rad sa poljima - kolekcijama
# 1. multi input comprehensions
comprehensions - short-hand syntax for creating collections and iterable objects
(nested) petlje koje vracaju, drugacije napisane
----
# list
l = [i * 2 for i in range(10)] # norm lista, spoljne zagrade odredjuju return tip compreh
# dict
d = {i: i * 2 for i in range(10)}
# sets
s = {i for i in range(10)}
# generators
g = (i for i in range(10))
----
single input sequence
multiple input sequences
multiple if-clauses
-----
# 2 inputs - NESTED loops
[(x, y) for x in range(5) for y in range(3)] # spoljna, unutrasnja petlja
[(0, 0), (0, 1), (0, 2), ...]
prva petlja, x - spoljna
druga petlja, y - unutrasnja
------
# tacno ekviv ovome
points = []
    for x in range(5):
        for y in range(3):
            points.append((x, y))
------
# comprehension benefits
ne treba da kreiras list var - points = []
------
# multiple if i for loops
nested sa leva nadesno, expression se odnosi na sve nadesno
---
# primer, dve compreh razbi u multiline za citljivost
values = [x / (x - y) # return value
          for x in range(100)
          if x > 50 # ovo deluje na sve nadesno
          for y in range(100)
          if x - y != 0]
# ekvival
values = []
for x in range(100):
    if x > 50: # ovo deluje na sve ispod
        for y in range(100):
            if x - y != 0:
                values.append(x / (x - y)) # ovo je prvi izraz koji vraca, zapazi
------
# unutrasnja koristi x iz spoljne, ok
[(x, y) for x in range(10) for y in range(x)]
-------------------------------------------
# 2. nested comprehensions
kada je output var (prvi izraz) comprehension - stvara multidim polje
# lista listi
# zapazi [[ na pocetku
# sada je leva petlja inner
vals = [[y * 3 for y in range(x)] for x in range(10)]
# ekvival to
outer = []
for x in range(10): # pravi row[]
    inner = []
    for y in range(x): # pravi 1 row
        inner.append(y * 3)
    outer.append(inner)
# desna izbacuje u novu listu ono sto leva napravi - listu
----
i multiple i nested compreh mogu za sve struct - list, dict, set, generator
samo se [] menja sa {} ili (), samo to
# dict multiple
{x * y for x in range(10) for y in range(10)}
# generator nested. ((
g = ((x, y) for x in range(10) for y in range(x))
# invoke lazy result u listu
list(g)
-------------------------------------------
# 3. map function - lazy, ok
iteration, iterables - sequences jedan po jedan el
funkcionalno prog u pythonu
------
map() - apply a function to every element in a sequence (kolekcija), producing a new sequence
mapira fju na elemente kolekcije, foreach into fn
map(ord, 'The quick brown fox')
u js 'The quick brown fox'.map(ord)
map() is lazy - it only produces values as they are needed # vazno
map() ne zove fju kad je map() pozvan, nego kad se iterira kroz ret obj
vraca iterator object
----
# primer log
result = map(Trace()(ord), 'the quick brown fox')
nikakav output, samo returned iterator <builtin.map at ...>
next(result) # 84 , sad pozvao na 1 elem
next(result) # ... manualno iteriranje
-----
obicno list(result) ili for petlja da izbaci sve elemente
linq... ok
-------------------------------------------
# 4. multiple input sequences - lista na svaki arg
fja u map ima vise args, na svaki arg se mapira odvojena lista
ako liste nisu iste duzine map zavrsava cim iscrpi najkracu
umesto jednog arg objecta u javascriptu, onako bzvz
----
# primer
list(map(fn, list1, list2, list3))
-------------------------------------------
# 5. map vs comprehensions
list compreh [ ... ] and list(map(...)) su isto, ne lazy
generator expression ( ... ) i map su isto, lazy
-----
nema jasan kriterijum, stvar ukusa, isto su
-------------------------------------------
# 6. filter function - isto ko js
filter() prolazi kroz sve, vraca one za koje fja vraca True
isto lazy, vraca filter object, iterable, mora list() da ih izbacis sve
fja moze samo 1 arg, zapazi
----
filter(None, list1) # vraca samo truthy elements
trues = filter(None, [0, 1, False, True, [], ''])
# zapisi falsy values u pythonu, ne zaboravi
----
u python 2 map() i filter() nisu lazy, zapazi
-------------------------------------------
# 7. functools reduce function
functools.reduce() - functools je std module
functools.reduce() - repeatedly apply a function to the elements of a sequence, reducing them to a single value
linq - aggregate(), fold() - func langs, std:accumulate() - cpp
# tipican primer - sumiranje niza
reduce(operator.add, [1, 2, 3, 4, 5])
----
reduce(mul, []) # TypeError
reduce(mul, [1]) # vraca 1 bez pozivanja fje mul()
-----
initial value, 3rd arg, 0 za sum, 1 za mul
samo se dodaje na vrh liste koja je arg
reduce(add, values, 0)
-------------------------------------------
# 8. combining map and reduce
map() i reduce() u pythonu jesu map-reduce algoritmi
# primer brojanje reci u dokumentima
total_count = reduce(combine_counts, counts)
-------------------------------------------
# 9. iteration protocols
iterator vs iterable... - podseti se opet i razjasni
----
iter() - create an iterator
called on iterable object to retrieve iterator
-----
next() - get next element in sequence
called on iterator to get elems from iterable
-----
StopIteration - signal the end of the sequence
--------
iterable - object koji ima __iter__() implementiran
__iter__() vraca iterator
-------
iterator - impl iterable protocol - ima __iter__()
plus impl iterator protocol, ima __next__()
------
# poenta:
iterable protocol: __iter__()
iterator protocol:  __iter__() + __next__()
-------------------------------------------
# 10. putting protocols together
# razjasni ove iz primera, koje fje koji treba i sta radi
class ExampleIterator:
    def __init__(self, data):
        self.index = 0
        self.data = data

    def __iter__(self):
        return self

    def __next__(self):
        if self.index >= len(self.data):
            raise StopIteration()

        rslt = self.data[self.index]
        self.index += 1
        return rslt

class ExampleIterable:
    def __init__(self):
        self.data = [1, 2, 3]

    def __iter__(self):
        return ExampleIterator(self.data)
----
implementiras protocole (interfejse) 
i mozes te klase da koristis u for loops i comprehensions
-------------------------------------------
# 11. alternative iterable protocol - __getitem__()
alternative iterable protocol - consecutive integer indexing via __getitem__()
mora da baci IndexError za kraj
# iz liste vec dobija access i IndexError
class AlternateIterable:
    def __init__(self):
        self.data = [1, 2, 3]

    def __getitem__(self, idx):
        return self.data[idx]
-------------------------------------------
# 12. extended iter format
# druga forma sem next()
iter(callable, sentinel)
sentinel - iteration stops when callable produces this value
callable - fja that takes zero arguments
----
namena: creating infinite sequences from existing functions
-------------------------------------------
#  13. duck tail iterator for streamed sensor data
# ovo je primer za extended iter format
sensor producer klasa
# __iter__() + __next__()
class Sensor:
    def __iter__(self):
        return self

    def __next__(self):
        return random.random()

sensor = Sensor()
timestamps = iter(datetime.datetime.now, None) # alter forma

# timestamps, sensor upotreb ovde
for stamp, value in itertools.islice(zip(timestamps, sensor), 10):
    print(stamp, value)
    time.sleep(1)
-----
nije bas jasno, al nebitno
-------------------------------------------
# 14. summary
comprehensions: 
multiple input sequences - nested loops
if-clauses
comprehension can also appear in the result expression of a comprehension - multidim result
----
map() - returns iterable, lazy
filter() - returns iterable, predicate fn
reduce() - summation generalization, init val on top
----
# iterator protocol:
next(iterator) zove iterator.__next__() - vraca sledeci elem ili stopIter exception
# iterable protocol:
iter(iterable) zove iterable.__iter__() - vraca iterator
# iterable protocol za uzastopne indexe od 0:
iterable.__getitem__(index) - isto se poziva sa iter(), dok ne IndexError
# iterable from callable - extended form of iter():
iter(callable, sentinel) - zove callable() dok ne dodje do sentinel val, vraca rezultate callablea
-----
summary u pdf-ovima su dobri podsetnici, zapazi
-------------------------------------------
## 9. inheritance and subtipe polymorphism
# 1. inheritance overview
class SubClass(BaseClass) # subklasa ima sve metode base klase i moze da ih override, plus jos neke
__init__() from base will be called only if in subclass is undefined
----
# primer
class Base:
    def __init__(self):
        print('Base initializer')

    def f(self):
        print('Base.f()')

class Sub(Base):
    def __init__(self):
        print('Sub initializer')

    def f(self):
        print('Sub.f()')
-----
b = Base()
'Base initializer'
s = Sub()
'Base initializer' # samo ako je __init__() u Sub undefined
s = Sub()
'Sub initializer' # ako je __init__() u Sub defined, ne zove i Base.__init__() zapazi, mora super()
s.f() # ako je pregazen u Sub, logicno...
-------------------------------------------
# 2. realistic example sorted list
# primer SimpleList i SortedList
class SimpleList:
...
# super() u __init__() i add()
class SortedList(SimpleList):
    def __init__(self, items=()):
        super().__init__(items)
        self.sort()

    def add(self, item):
        super().add(item)
        self.sort()
-------------------------------------------
# 3. built-in isinstance function
isinstance(obj, type): boolean - runtime type checking
---
isinstance(3, int) - True
vraca True ako je obj exact tip ili SubClass od prosledjenog
isinstance(subObj, BaseClass) - True
-----
isinstance(x, (float, dict, list)) # drugi arg moze tuple tipova - or -> bilo koji = True 
-----
# primer IntList - baca except ako item u add ili init nije int
@staticmethod
def _validate(x):
    if not isinstance(x, int):
        raise TypeError('IntList only supports integer values.')
-------------------------------------------
# 4. built-in issubclass function - trivial
issubclass(tip1, tip2) - oba args tipovi, ne instance
direct or indirect subtype - True
-------------------------------------------
# 5. multiple inheritance
vise od 1 direktna base klasa, logicno
-----
nezgodno - vise base klasa impl isti metod
-----
# sintaksa, trivial
class SubClass(Base1, Base2, . . .)
SubClass ima metode svih base klasa
MRO - method resolution order, kad vise base klasa ima metod sa istim imenom
-----
# primer, sortirana int lista, dovoljno da nasledi samo
# i sve radi, nije lose
class SortedIntList(IntList, SortedList):
-------------------------------------------
# 6. details of multiple inheritance
samo __init__() od prve base klase se zove
class Sub(Base1, Base2):
'Base1.__init__'
------
Klasa.__bases__ - tuple sa svim base klasama, tipovi, po redu
-------------------------------------------
# 7. method resolution order
MRO - redosled pretrage inheritance graph-a
Klasa.__mro__ - tuple klasa po redu
Klasa.mro() - isto samo lista
-----
# primer, zapazi - A -> B, C -> D
class A:
    def func(self):
        return 'A.func'

class B(A):
    def func(self):
        return 'B.func'

class C(A):
    def func(self):
        return 'C.func'

class D(C, B):
    pass
-------
D.mro() - [module.D, module.B, module.C, module.A, builtins.object]
sve klase nasledjuju object
----
d = D()
d.func()
'B.func' # zbog redosleda u class D(C, B):
----
MRO poenta: ide nanize prema base, sibling base koji je prvi naveden u Klasa(C, D), ok logicno
----
za primer SortedIntList add() je pozvan u obe base klase, super()...
-------------------------------------------
# 8. how is MRO calculated
C3 - algoritam za izracunavanje mro
ne moze bilo koji redosled nasledjivanja, mora konzistentan sa base klasama
-----
# 
class A:
class B(A):
class C(A):
class D(B, A, C): # greska, A mora pre B i C
TypeError
-------------------------------------------
# 9. built-in super function
super() vraca proxy object
super() ne radi nuzno na base class nego na ceo MRO
----
bound proxy - to class and instance
unbound proxy - nebitan
-------------------------------------------
# 10. class bound super proxies
1. class bound
2. instance bound
-------
# class bound proxy
super(base-class-arg, derived-class-arg)
---
# algoritam trazenja super().add()
1. nadje mro za derived-class-arg
2. nadje base-class-arg u tom mro
3. trazi fju u klasama u mro posle base-class-arg
-------
# primer
super(SortedList, SortedIntList)
# mro od SortedIntList
SortedIntList 
IntList
SortedList
SimpleList # ova je posle SortedList, super().add vraca add() iz nje
object
----
super() vraca klasu
-------------------------------------------
# 11. instance bound super proxies
drugi arg je objekt subklase prvog arg koji je klasa
super(class, instance-of—class)
----
# poenta: 
sve isto kao za class bound samo sad moze da se zove super().add()
sad super() vraca instancu SimpleList, a ne klasu
algoritam je isti kao za class
------
sve ovo petljanje koje fje ce da se pozovu u base classes 
za visestruko nasledjivanje
tj. sta super(class, class-or-instance) vraca
-------------------------------------------
# 12. calling super without arguments - default args
podrazumevani args ako zoves samo super() bez args
# ako je super() u:
# instance method
super(class-of-method, self)
# class method
super(class-of-method, class)
----
znaci zove klasu i prvi arg metoda u kom je pozvan, ok
--------
u single inheritance super() jednostavno zove base class
-------------------------------------------
# 13. SortedIntList explained
SortedIntList.mro()
[<class 'sorted_list.SortedIntList'>,
<class 'sorted_list.IntList'>,
<class 'sorted_list.SortedList'>,
<class 'sorted_list.SimpleList'>,
<class 'object'>]
sil = SortedIntList()
sil.add(6)
# super() ne zove nuzno iz base class nego iz mro
IntList pozvao iz SortedList... kako?
-------------------------------------------
# 14. object class
root class za sve tipove, javlja se u svaki mro
kad klasa ne nasledjuje nista nasledjuje od object
NoBaseClass.__bases__ # (<class 'object'>,)
----
dir(object) # svi propertiji klase
----
python object model
-------------------------------------------
# 15. inheritance for implementation sharing
tip ne odredjuje da li object moze kao fn(arg) ili obj.fn(), nego at runtime
duck typing - je to, odredjivanje at runtime
----
# poenta: 
nasledjivanje u pythonu se ne koristi za type hiearchy nego za share implementation - reuse code
type hiearchy - da moze arg da sedne u fju, C++
-------------------------------------------
# 16. summary
single inheritance, isinstance(), multiple inheritance
__bases__, MRO, __mro__, C3, super(), bound proxy, object
inheritance za reuse implementation
-------------------------------------------
## 10. implementing collections
# 1. collection protocol overview
pravi novu kolekciju koja ce da podrzava protokole
container - membership
sized - num of items - len()
iterable - iterator
sequence - random access to elem
set - set operations
-------------------------------------------
# 2. collection construction
protocol - set of __methods__
pravi SortedSet, tdd
----
napisao nekoliko testova i trivijalan kod
# posebna klasa za testove, construction
# describe, suite, on kaze test case
class TestConstruction(unittest.TestCase):
-----
# pokrece test - pokrece fajl
python test_file.py
----
# _items je lista, sorted() vraca listu
def __init__(self, items=None):
    self._items = sorted(set(items)) if items is not None else []
-------------------------------------------
# 3. continer protocol
in, not in
constructor u setUp()
class TestContainerProtocol(unittest.TestCase):
prisutan, nije prisutan x in, not in
# implementacija, smesno, samo uzeo in od liste
def __contains__(self, item):
    return item in self._items
-------------------------------------------
# 4. sized protocol - len(), trivial
testovi za 0, 1 i 10
# impl da prodje
def __len__(self):
    return len(self._items)
# set() da filter duple items
self._items = sorted(set(items))
-------------------------------------------
# 5. iterable protocol - next(i) i for loop
impl __iter__()
poenta: next(i) i for loop # tacno to
1, 2, 7, 9 jer je sorted set
---
self.assertEqual(next(i), 9)
# assert exception
self.assertRaises(StopIteration, lambda: next(i))
----
def test_iter(self):
def test_for_loop(self):
----
# implem, samo zove od liste, opet
def __iter__(self):
    return iter(self._items)
# moze i generator custom, objasnio...
# generator je iterator, __iter__() mora da vrati iterator
def __iter__(self):
    for item in self._items:
        yield item
-------------------------------------------
# 6. sequence protocol indexing - [i] i __getitem__()
prethodno mora container, sized, iterable
---
# fje i operatori sequence protocola
item = seq[index] - __getitem__() je metod
item = seq[start:stop] - [:] je slicing
r = reversed(seq)
index = seq.index(item)
num = seq.count(item)
+, * operatori
----
test fixture - dummy object (lista) u testu
----
with - using() u C# podseti se
----
# za [i] opet samo delegirao na listu 
def __getitem__(self, index):
    result = self._items[index]
-------------------------------------------
# 7. sequence protocol slicing
testovi za sve kombinacije [x:y]
[] prosledjuje slice objekat, koji moze da se dobije sa slice()
----
# resenje za sortiranje
return SortedSet(result) if isinstance(index, slice) else result
-------------------------------------------
# 8. comprehensible test results with repr
implem repr() za laksi debug
----
# kao ternary
repr(self._items) if self._items else ''
self._items ? repr(self._items) : '' # js ekviv
-------------------------------------------
# 9. implementing equality and inequality
default != i == je reference equality, a ne value eq
SortedSet([1, 2]) == SortedSet([1, 2]) # False
SortedSet([1, 2]) is SortedSet([1, 2]) # False, identity test
list([1, 2]) == list([1, 2]) # True, lista overrides default == od object
------
assertTrue, assertEqual helperi iz pytest
------
# override == i != operators
lhs == rhs
__eq__(self, rhs) # self je lhs
----
lhs == rhs
__ne__(self, rhs) # self je lhs
------
# opet samo delegira == listi
# NotImplemented nije exception nego spec object...?
def __eq__(self, rhs):
    if not isinstance(rhs, SortedSet):
        return NotImplemented
    return self._items == rhs._items
-----
# ako overr __eq__() mora i __ne__() iako prolazi test, za object
-------------------------------------------
# 10. sequence protocol reversing
__reversed__() # treba ovaj
__getitem__() i __len__() # ako su ova dva impl __reversed__() ce imati default impl
-----
generalno eto za sta su __method__() metodi, za override default operators
-------------------------------------------
# 11. sequence protocol index
[] je impl sa __getitem__()
index = seq.index(item) # nadje index na osnovu value
---
# default impl postoji u abc.collections - abstract base classes
samo __getitem__() i __len__() su abstract i na osnovu njih impl sve ostale iz sequence
samo nasledis
---
from collections.abc import Sequence
class SortedSet(Sequence):
-------------------------------------------
# 12. sequence protocol count
num = seq.count(item) # koliko puta se item pojavljuje u seq
i ovo radi default od preth nasledjivanja
set pa testovi samo za 0 i 1
-------------------------------------------
# 13. improving performance from ON to Olog n
sorted - binary search
set - 1 elem, nije optimalno
25x ubrzao vreme
samo count() impl u ovom klipu
----
# custom overrides
def index(self, item):
    index = bisect_left(self._items, item)
    if (index != len(self._items)) and (self._items[index] == item):
        return index
    raise ValueError("{} not found".format(repr(item)))

def count(self, item):
    return int(item in self)
------
int(True) # 1
int(False) # 0
-------------------------------------------
# 14. refactoring to avoid dont repeat yourself
# koristi impl iz index(), trivial
# in operator, count() zove ovo
def __contains__(self, item):
    try:
        self.index(item)
        return True
    except ValueError:
        return False
-------------------------------------------
# 15. checking protocol implementations
dodao asserte u testovima da li je podklasa
ne mora da nasledi, dovoljno je duck typing
----
# issubclass Container, Sized, Sequence, Set
self.assertTrue(issubclass(SortedSet, Container))
# stavio nasledjivanje, nije neophodno, ali preporuka
class SortedSet(Sequence):
-------------------------------------------
# 16. sequence protocol concatenation and repetition
testovi set operacije - razlika, jednako i presek
__add__() - impl + operator nad set
----
# impl, chain() izbegava temp vars
return SortedSet(chain(self._items, rhs._items))
----
# * multiplication
testovi za 0 i n
# mora levi i desni, set je desni ili levi operand
__mul__() - s * 100
__rmul__() - 100 * s
isti result vracaju
-------------------------------------------
# 17. set protocol
specificno za setove, nebitno
immutable set
set protocol, subset, superset operations
# relational operators
<=, <, ==, !=, >, >=
# algebraic operators - od binarnih
&, |, ^, -
----
dodao testove za sve ove operatore
# nasledio Set
class SortedSet(Sequence, Set):
-----
# testrao s.issubset()
# uzeo impl od <= operatora nad Set
# samo mora oba isti tip, zato SortedSet()
def issubset(self, iterable):
    return self <= SortedSet(iterable)
-----
# test da li je ceo set protocol impl
def test_protocol(self):
    self.assertTrue(issubclass(SortedSet, Set))
-------------------------------------------
# 18. duck tail making mutable set - nebitno
nasledi od MutableSet i impl dodatne metode add(), discard()
-------------------------------------------
# 19. summary
implementirao custom SortedSet strukturu sa svim protocolima (interfejsima) i metodama
# protocoli:
container, sized, iterable, sequence, set
string representation, value equality
-------------------------------------------
## 11. exceptions and errors
# 1. always specify exception type
kill python program
ps - list processes
kill 1199
-----
# uvek filtriraj tip exceptiona, nikad go except:
def main():
    number = randrange(100)
    while True:
        try:
            guess = int(input("? "))
        except ValueError:
            continue
        if guess == number:
            print("You win!")
            break
----
cak i unknown var moze da se uhvati u exception, ctrl C, itd...
-----
poenta: nikad go except: nego uvek except SomeClass:
-----
 : je blok generalno u pythonu, mesto {}
-------------------------------------------
# 2. standard exception hierarchy
built-in exception klase
except: hvata se ta klasa ili dete klasa
----
IndexError - list, nepostojeci index
KeyError - dict, nepostojeci key
-----
IndexError.mro()
IndexError, LookupError, Exception, BaseException, object
----
LookupError hvata i IndexError i KeyError
-----
# hierarchy
https://docs.python.org/3/library/exceptions.html#exception-hierarchy
-----
BaseException je root exception
system exiting exceptions - sys.exit(), keyboard interupt
Exception - hvata i SyntaxError, nevalidan kod, ne treba
----
uvek uhvati sto specificniji exception
OSError - za file system, missing file, permission itd
-----
exception object sadrzi original exception type i payload
-------------------------------------------
# 3. exception payloads
payload - uzrok
built-in exceptions accept string in constructor
-----
# najcesci exception koji se baca
# uvek na vrhu fje proveri validnost args, generalno
ValueError - nevalidan argument
-----
string iz constructora se cita sa e.args, UVEK single string arg message
try:
    median([])
except ValueError as e:
    print("Payload:", e.args) # tuple ('message',)
    print(str(e)) # drugi nacin, repr()
-----
ostali args u payload su named args
-------------------------------------------
# 4. defining new exceptions
custom exceptions kad built-in ne opisuju dobro
----
primer povrsina trougla, herons formula
# custom exception, nasledjuje se Exception
# ne mora implem, sasvim dovoljno
class TriangleError(Exception):
    pass
-----
# ili implementacija
class TriangleError(Exception):

    # sides moze da se cita iz exc objekta
    def __init__(self, text, sides):
        super().__init__(text)
        self._sides = tuple(sides) # tuple, read only

    # only getter, read only
    @property
    def sides(self):
        return self._sides

    def __str__(self):
        return "'{}' for sides {}".format(self.args[0], self._sides)

    def __repr__(self):
        return "TriangleError({!r}, {!r}".format(self.args[0], self._sides)
-------------------------------------------
# 5. chaining exceptions - novi exceptions u catch
asociate jedan exc sa drugim
zadrzavanje ref originalnog exceptiona
-----
1. implicit chaining - u catch bloku prvog exceptiona javio se novi exception # to, to
__context__ - ref na originalni exception u obj zadnjeg exceptiona
message: during handling of the above exception another exception occured
----
# primer
try:
    a = triangle_area(3, 4, 10)
    print(a)
except TriangleError as e: # exc1
    try:
        print(e, file=sys.stdin)
    except io.UnsupportedOperation as f: # exc2
        print(e)
        print(f)
        print(f.__context__ is e) # exc2.__context__ == exc1
------
2. explicit chaining - kad ti bacis nov exception iz catch
translating from ex1 to ex2
----
# primer, from keyword, explicit exception chaining
try:
    return math.degrees(math.atan(dy / dx))
except ZeroDivisionError as e:
    # from - explicit exception chaining
    raise InclinationError("Slope cannot be vertical") from e
----
povezuje exc1.__cause__ = exc2 # umesto __context__
-------------------------------------------
# 6. traceback objects - call stack, stack trace
print(e.__traceback__) # stampa samo ref
-----
# print_tb(), format_tb()
import traceback # za stampanje
traceback.print_tb(e.__traceback__) # stampanje
s = traceback.format_tb(e.__traceback__) # render u string
----
ne smes da ref exc object ili exc.__traceback__ van catch
jer sprecava garbage collecting iz svih scopeova fja u stacku
poenta: koristi format_tb() render u string, pa string koristi
-------------------------------------------
# 7. assertions internal invariants - specijalizovan exc za proveru logike, nema u js
# nov koncept - da baci jasan exception za neispunjenu pretpostavku
assert booleanCondition [,message]
False - baca AssertionError
True - nista se ne desava
----
invariants - uslovi u programu koji moraju biti True
----
if () throw prakticno, specijalizovan za logiku sa AssertionError exception
-----
assert - osiguraj, potvrdi, pretpostavka - runtime
samo da je implementacija tacna, ne validacija args
umesto comment, da baci exception
-----
isto kao expect() ili assert() u jest testovima, baci exception za false # to, zapazi
-------------------------------------------
# 8. assertions class invariants - proveri attr klase da je sortiran i unique
# private boolean helper method u klasi samo za assert u ostalim
def _is_unique_and_sorted(self):
    return all(self[i] < self[i + 1] for i in range(len(self) - 1))
----
# proveri da je sortiran za binary search
def index(self, item):
    assert self._is_unique_and_sorted()
    index = bisect_left(self._items, item)
----
# proveri da je unique
def count(self, item):
    assert self._is_unique_and_sorted()
    return int(item in self._items)
-------------------------------------------
# 9. assertion performance, -0 option
# disable assertions da izmeris da nisu mnogo degradirane performanse
python3 -0 file.py 
vecinom se u produkciji assertions drze ukljuceni
-------------------------------------------
# 10. duck tail preconditions and postconditions
postconditions - da fja vraca sto treba
assert - treba da hvata programerove greske, a ne korisnikove greske (args)
raise ValueError('invalid args') za args
----
# pre return, all() - svi su true
assert all(len(line) <= line_length for line in result.splitlines())
----
poenta: assert pre return, raise ValueError na pocetku # ok
-------------------------------------------
# 11. summary
nikako handle svi exceptions ie. SystemException, KeyboardInterrupt
ili Exception koji handluje netacnu sintaksu
----
custom exc, stack trace, asserts
-------------------------------------------
## 12. defining  context managers - object iza with, using(context-manager) {}
# 1. what is context manager
context-manager - object iza with, ima 2 metode koje alociraju i oslobadjaju resurs
isto kao var u using(someStream = new Stream()) u C#
-----
with context-manager:
    context-manager.begin(), setup(), construction(), allocation(), enter()
    body
    context-manager.end(), teardown(), destruction(), deallocation(), exit()
------
obe metode se uvek izvrse
teardown() se izvrsava i za exception
-------
with open('file.txt', 'w') as f:
    f.write('some text')
# f.prepare() i f.close() se automatski izvrse pre i posle write, svaki put
# jer file impl context protocol
-------------------------------------------
# 2. context manager protocol - IDisposable C#
samo 2 metode
__enter__(self)
__exit__(self, exc_type, exc_val, exc_tb)
----
# expression mora da vrati object koji ima ove dve metode
with expression as x:
    body
---
onda se zove obj.__enter__() i ako baci exception ne ulazi se u block
ako ima as onda return value od __enter__() je u x (a ne rez od expression, mada uglavnom jeste, tj vrati self)
----
onda se izvrsava block
----
na kraju obj.__exit__(args), args su postavljeni samo ako bio exception
-------------------------------------------
# 3. first context manager example
__enter__() i __exit__() su callbacks prakticno, onStart() i onExit()
-----
# basic primer
class LoggingContextManager:
    def __enter__(self):
        print('LoggingContextManager.__enter__()')
        return "You're in a with-block!"

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            print('LoggingContextManager.__exit__: '
                  'normal exit detected')
        else:
            print('LoggingContextManager.__exit__: '
                  'Exception detected! '
                  'type={}, value={}, traceback={}'.format(
                      exc_type, exc_val, exc_tb))
-----
exception izlazi iz with bloka by default
args u __exit__(args) postavljeni samo za exception, to je to, trivial klip
-------------------------------------------
# 4. __enter__()
zove se pre body
vraca var za as x
najcesce vraca self, tj context-manager
----
# file.__enter__() je vratio self, dokaz
# sto je vratio open() zavrsilo u g
f = open('file.txt', 'r')
with f as g:
    print(f is g)
-------------------------------------------
# 5. __exit__()
cleanup, oslobadja resurs i hendluje exception ako ima
-----
__exit__(self, exc_type, exc_val, exc_tb)
exception type # ovaj se proverava da li je bio exception
exception object
exception traceback
-----
bez exception svi su postvljeni na None
----
# primer
def __exit__(self, exc_type, exc_val, exc_tb):
    # evo je provera da li je bio exception
    if exc_type is None:
        print('LoggingContextManager.__exit__: '
                'normal exit detected')
    else:
        print('LoggingContextManager.__exit__: '
                'Exception detected! '
                'type={}, value={}, traceback={}'.format(
                    exc_type, exc_val, exc_tb))
-----
poenta: nista spec, samo detektuje da li je ili nije bio exception
-------------------------------------------
# 6. __exit__() and exception propagation - return value
__exit__() by default re-raises (propagates) exception that happens in with block
----
# return value
if __exit__() returns False exception is propagated
if __exit__() returns True with swallows exception
-----
python void fn implicit return je None -> falsy
-----
__exit__() ne sme da re-raise exception iz blocka
moze samo da baci novi svoj exception, onda je to drugo, i nema veze sa with block
-------------------------------------------
# 7. with statement expansion
implementacija with statement u pythonu, ok
prouci kod, try, catch, finally i 2 poziva __exit__(), __enter__()
-------------------------------------------
# 8. contextlib contextmanager - decorator, teze malo
# jednostavnije kreiranje context managera od  __enter__() i __exit__() - kreiranje decoratorom
contextlib - package in std lib, utils za with statement
contextlib.contextmanager - decorator za kreiranje new context managers, tj alternativni nacin
-----
u pythonu generator se od fje razlikuje vidljivo samo po yield, ok, js *
-----
# definicija
@contextlib.contextmanager
def my_context_manager(): # mora generator, tj yield
    # <ENTER> # __enter__()
    try:
        yield [value] # value bound za as var, kao return val od __enter__(), zatim ide with blok sa ili bez exception
        # <NORMAL EXIT> # ovo ako nema exception
    except:
        # <EXCEPTIONAL EXIT> # ovo ako ima exception
        raise
----
# poziv, tj upotreba
with my_context_manager() as x:
    block
--------
poenta: sa decoratorom impl je normal control flow sa yield statement, umesto podeljen u 2 funkcije
generator pamti stanje izmedju 2 yield, ne treba klasa za stateful context manager, nije skroz jasno
-----
# isti primer, zapravo je jednostavno, samo nije u 2 fje
@contextlib.contextmanager
def logging_context_manager():
    print('logging_context_manager: enter')
    try:
        yield "You're in a with-block!"
        print('logging_context_manager: normal exit')
    except Exception:
        print('logging_context_manager: exceptional exit',
              sys.exc_info())
        raise # ovo je re-raise za propagaciju van with
----
razlika: exception se ne propagira van with by default, tj swallows
jer je handlovan u normalan catch tj except
----
za propagaciju ili re-raise ili ga ne hvataj (bez try catch?, nije pokazao)
------
ovo moze-mora preko generatora jer vraca vise od jednom, pa je kao 2 fje
2 slicne fje mogu kao jedan generator sa args
-------------------------------------------
# 9. multiple context managers
# jedan iza drugog je isto sto i ugnjezdeni
sve logicno, vars se vide iz spoljnog, exceptions iz unutrasnjeg, mogu biti swallowed ako ne re-raise
----
# jedan iza drugog
with cm1() as a, cm2() as b:
    BODY
-----
# ugnjezdeni
with cm1() as a:
    with cm2() as b:
        BODY
-----
# primer da ne mora catch blok
@contextlib.contextmanager
def nest_test(name):
    print('Entering', name)
    yield name
    print('Exiting', name)
-----
# vars example
# as n1 iz spoljnog upotrebljen u unutresnjem, ok
with nest_test('outer') as n1, nest_test('inner nested in ' + n1):
    pass
-------------------------------------------
# 10. dont pass a list
ne sme zagrade [] ili (), with ne unpackuje
---
# error
with [nest_test('a'), nest_test('b')]:
    pass
AttributeError: __exit__() # ne nalazi prop na listi, ok
----
# multiline context managers ide sa line continuation \
with nest_test('a'),\
     nest_test('b'),\
     nest_test('c'):
    pass
-------------------------------------------
# 11. duck tail context managers for transactions - commit, rollback, onSuccess, onError
# realistican primer, baza connection i transaction
with blok ide gde god imas neki onStart i onExit callbacks svaki put za objekat
-----
# primer
# connection.py
class Connection:
    def __init__(self):
        self.xid = 0 # samo cuva id u klasi

    def _start_transaction(self):
        print('starting transaction', self.xid)
        rslt = self.xid
        self.xid = self.xid + 1
        return rslt # vraca tekuci

    def _commit_transaction(self, xid):
        print('committing transaction', xid)

    def _rollback_transaction(self, xid):
        print('rolling back transaction', xid)

# koristi Connection klasu
class Transaction:
    def __init__(self, conn):
        self.conn = conn
        self.xid = conn._start_transaction()

    def commit(self):
        self.conn._commit_transaction(self.xid)

    def rollback(self):
        self.conn._rollback_transaction(self.xid)

# commits transaction on success, rollback on exception, ok
@contextlib.contextmanager
def start_transaction(connection):
    tx = Transaction(connection)

    try:
        yield tx # as var, prosledi transaction obj u with block
    except Exception:
        tx.rollback() # onException
        raise # re-throw

    tx.commit() # iza with bloka, onSuccess
---
# probao za exception i success
try:
    with start_transaction(conn) as tx:
        blok
except ValueError:
    assert False # throw assert exc ako udje ovde
-------------------------------------------
# 12. summary
context manager - object u with statement, impl protocol
before, block, after
resource management, file npr
__enter__() and __exit__()
PEP343 za with
----
contextlib - utils za with statements
---
@contextlib.contextmanager
def generator_fn():
----
with moze da primi vise managera
jedan iza drugog isto sto i ugnjezdeni
-------------------------------------------
## 13. introspection
# 1. object types in depth - type()
introspection - sposobnost programa da ispita svoju strukturu i state
jeste reflection from C#, tamo je kompajliran binaran, ovde moduli i src fajlovi
-------
# generalne filozofije
EAFP - easier to ask forgiveness than permission - probaj ali umotaj u try catch, i handluj u catch
LBYL - look before you leap - validiraj args na pocetku funkcije pa baci exception
-----
u pythonu EAFP dominantna, optimistic
-----
introspecting:
1. types
2. objects
3. scopes
-----
types - type() fja
i = 7
type(i) # <class 'int'>
int # <class 'int'>, int jeste klasa
repr(int) # <class 'int'>, sve je ovo a is b, ===
----
type(type(i)) # <class 'type'>, tip od klase int je tipa type, klasa je tipa type
type() vraca i.__class__ attribut, svaki objekat ima ovo, i moze type(obj)
---
i.__class__.__class__ # <class 'type'> - type(int)
i.__class__.__class__.__class__  # <class 'type'> - type(type), i class i type su tipa type, ok
----
issubclass(type, object) # type je nasledjen iz object
type(object) # object je tip
----
zakljucak object je tip i tip je object - samo zajedno mogu da postoje
----
isinstance(i, int) # da li je var obj neke klase
-----
izbegavati ispitivanje tipova u pythonu, ako mora onda issubclass() i isinstance()
a ne type(x) == 'nesto'
-------------------------------------------
# 2. introspecting objects - dir()
i = 7
dir(i) - lista[] attribute (js propertije) instance, puno ih ima __nesto__ i obicni
---
# get obj.property
getattr(i, 'denominator') == i.denominator, AttributeError ako ne postoji
callable(getattr(i, 'conjugate'))  - callable() da li je funkcija
---
hasattr(i, 'bit_length') # True, hasOwnProperty() js, redje se koristi jer je LBYL
-----
# primer mixed_numeral() fn za celobrojne razlomke
proveri args, ili proba pa handluje exception
baceni exception iz catch loguje i originalni exception
-------------------------------------------
# 3. introspecting scopes
1. globals() - dict {}, sve vars iz global namespace
'__name__': '__main__'
if __name__ == '__main__':
    # runing as a program, pokrenut iz konzole, a ne imported u drugi fajl
----
globals()['tau'] = 6.283 # return value je read write
------
2. locals() - vraca isti dict, vraca sve vars za local scope fje
"{name} some text...".format(**locals()) # ** je rest za named args, samo za debug, nebitan primer
-------------------------------------------
# 4. python standard library inspect module
# primeri iscitao clanove modula i funkcije
inspect - module za introspectiong objects 
----
inspect.ismodule(sorted_set) # True, klasa iz preth primera
inspect.getmembers(sorted_set) # lista name val pairs - tuples, vrati veliki output
inspect.getmembers(sorted_set, inspect.isclass) # drugi arg predikat za filter 
vracena svaka klasa, definisana ili importovana
----
# moze import global modul koji je reexport iz lokalnog fajla, kao u js, ok
from sorted_set import chain 
----
# vrati sve funkcije
inspect.getmembers(sorted_set, inspect.isfunction)
----
# uhvati __init__() fju SortedSet klase, sorted_set modula
init_sig = inspect.signature(sorted_set.SortedSet.__init__)
# uhvati args fje
init_sig.parameters # opet lista tuple parova
str(init_sig) # default value za arg
----
ovo za fje ne radi za fje import iz C koje nemaju metadata
-------------------------------------------
# 5. duck tail an object introspection tool
def dump(obj) primer fja za log, introspector.py
----
# stampa tip, docstring, polja i metode objekta
# koristeci fje iz prethodnih klipova celog intermediate poglavlja
# slozeno prilicno, puno detalja
def dump(obj):
    print("Type")
    ...
    print("Documentation")
    ...
    print("Attributes")
    ...
    print("Methods")
    ...
----
fin izlaz za int npr
----
{} je pozicioni kao %s u str.format(), ok
-------------------------------------------
# 6. summary
obj.__name__ vraca name klase ili fje
# ponovio poglavlje, standardno, ok
-------------------------------------------
-------------------------------------------


-------------------------------------------
-------------------------------------------
### Advanced Python
## 1. course overview
# 1. course overview
descriptors - control over attribute access
metaclasses - intercept class object construction
virtual subclasses - control over class relationships
-------------------------------------------
## 2. advancced flow control
# 1. advanced python
pregled celog advanced kursa
-------------------------------------------
# 2. introducing advanced flow control
# nabrojao samo, 30 sekundi
- else clauses on loops
- else clauses on try blocks
- emulating switch statements
- dispatching function calls on type
-------------------------------------------
# 3. loop else clauses and while else
else na while i for, retko se vidja u drugim jezicima
-----
# while primer
break pravi razliku, ako si izasao sa break da ne izvrsis else granu
else je related za condition, a ne za break i return
-----
else se izvrsi samo jednom, inace bi se vrtelo beskonacno
-----
while condition:
    flag = execute_condition_is_true()
    if flag:
        break
# ima !condition unutra zapazi
# problem sto se izvrsi i za break
execute_condition_is_false()
-----
# svodi se na ovo
# dupla provera conditiona, nije dry
while condition:
    flag = execute_condition_is_true()
    if flag:
        break
if not condition:
    execute_condition_is_false() # 2x false, wtf?
-----
# ovo je while else sintaksa
# nema dupla provera conditiona, i to je sve
while condition:
    flag = execute_condition_is_true()
    if flag:
        break
else: # nobreak
    execute_condition_is_false() # ne izvrsava se ako break, ok
-------------------------------------------
# 4. while else for evaluating stack programs
while else nikad se ne koristi...?
# stack primer, poljska notacija, bezveze nepotrebni detalji
----
and isto radi shortcircuit kao && u js
----
else se izvrsi samo jednom kad prestane uslov, isto kao u if
----
else clause is the no-break clause
only useful when break is present
-------------------------------------------
# 5. for else clauses and handling search failure
# else se izvrsi jednom kad se iterable iscrpi
for item in iterable:
    if match(item):
        result = item
        break
else: # nobreak
    # No match found
    result = None # izasao iz for bez break
------
kao default slucaj
------
# primer append to list if no elem is deljiv sa 12, trivial
-----
vecina ispitanih ne zna za loop else clause, retko se koristi
-------------------------------------------
# 6. alternatives to loop else clauses
# alternativni nacin preko fje da izbegnes for else
def ensure_has_divisible(items, divisor):
    for item in items:
        if item % divisor == 0:
            return item # break part
    items.append(divisor)
    return divisor # else part
-------------------------------------------
# 7. try else clauses
# else se izvrsi ako nije bilo exceptiona, ok
try:
    # This code might raise an exception
    do_something()
except ValueError:
    # ValueError caught and handled
    handle_value_error()
else:
    # No exception was raised
    # We know that do_something() succeeded, so
    do_something_else()
----------
# do_something_else() ne moze odmah ispod jer se u catch ne zna da li je
# do_something() ili do_something_else() bacio exception
try:
    # This code might raise an exception
    do_something()
    do_something_else()
except ValueError:
    # ValueError caught and handled
    handle_value_error()
-----------
# poenta
except - izvrsi se kad JE bilo exceptiona, i taj tip exc
else - izvrsi se kad NIJE bilo exceptiona
finally - izvrsi se i kad jeste i kad nije bilo exceptiona
----
# korisno kada dve linije bacaju isti exception type pa u
# else ide linija ciji exception ne zelis da handlujes
# open() i for line in f bacaju isti exc, samo open() zelis da hendlujes
try:
    f = open(filename, 'r')
except OSError: # OSError replaces IOError from Python 3.3 onwards
    print("File could not be opened for read")
else:
    # Now we're sure the file is open
    print("Number of lines", sum(1 for line in f))
    f.close()
-----
try else je korisnija od while else i for else
-------------------------------------------
# 8. emulating switch statements
# 2 nacina
1. if, elif, elif, else
2. fje u objekat pa motaj i invoke # komplikovano jbg
# svaki case u funkciju (ili lambda), one u objekat pa obj[key](), invokujes ih
while position:

    # kljucevi mogu da budu tuple
    # uslovi su kljucevi, tela su fje
    locations = {
        (0, 0): labyrinth,
        (1, 0): dark_forest_road,
        (1, 1): tall_tower,
        (2, 1): rabbit_hole,
        (1, 2): lava_pit,
    }

    try:
        location_action = locations[position] # nije pozvao odmah ovde () jer moze 2 exceptiona
    except KeyError:
        # switch default case
        print("There is nothing here.")
    else:
        # nema exc, pozvao ovde
        position, alive = location_action(position, alive)
------
dobra poenta, while else, for else ima smisla samo kad ima break, za no-break case
------
komplikovan primer ovde, nebitno
-------------------------------------------
# 9. dispatching on type - polimorfizam overloads, zanimljivo
veliki polimorfizam primer, a prost, shape base, circle, parallelogram, triangle, draw() fn
prvo u podklasama draw() definisana
onda izmestio fje van klasa pa u switch case pa pita tip argumenta pa zove podklasu
-----
generic functions - multiple implementations based on args type
overload - specificna implementacija
------
# konacno resenje, decorator
from functools import singledispatch
----
podsetnik - decorator hof uzme fju doda svoju implem i vrati kroz orig fju
originalna fja postane drugacija
-----
# sada draw postaje objekat koji ima register prop koji je decorator, ok
@singledispatch
def draw(shape):
    raise TypeError("Don't know how to draw {!r}".format(shape))

# register prima tip kao arg i na osnovu njega se okida
# draw_circle() preimenovana u _ jer ime ne sluzi nicemu
@draw.register(Circle)
def _(shape):
    print("\u25CF" if shape.solid else "\u25A1")

# sve su ovo sada overloads
@draw.register(Parallelogram)
def _(shape):
    print("\u25B0" if shape.solid else "\u25B1")


@draw.register(Triangle)
def _(shape):
    # Draw a triangle
    print("\u25B2" if shape.solid else "\u25B3")

# poziv
def main():
    shapes = [Circle(center=(0, 0), radius=5, solid=False),
              Parallelogram(pa=(0, 0), pb=(2, 0), pc=(1, 1), solid=False),
              Triangle(pa=(0, 0), pb=(1, 2), pc=(2, 0), solid=True)]

    for shape in shapes:
        draw(shape) # na osnovu tipa je odlucio
----
nije bas skroz jednostavno ali ok
-------------------------------------------
# 10. double dispatch with methods
@singledispatch moze samo za globalne funkcije, a ne i za metode klase
zato sto je prvi arg self, koji je klasa, pa su svi pozivi za isti tip
----
# resenje
# pomeri dekorisanu fju van klase
@singledispatch
def intersects_with_circle(shape, circle):
    raise TypeError("Don't know how to compute intersection of {!r} with {!r}"
                    .format(circle, shape))
----
# u klasi zameni mesta argumentima, swap
def intersects(self, shape):
    # Delegate to the generic function, swapping arguments
    return intersects_with_circle(shape, self)
----
# u globalnim pozvan vraceni decorator
@intersects_with_circle.register(Circle)
def _(shape, circle):
    return circle_intersects_circle(circle, shape)
----
komplikovan primer, a nebitan
-------------------------------------------
# 11. summary
while else, else se ne izvrsava za break i return izlaz iz petlje
retko se koristi, samo sa break
----
for else, izvrsi se samo kad se iscrpi kolekcija
koristi se samo uz break
----
try else - else se izvrsi samo kad nije bilo exceptiona
sluzi da se smanji scope try bloka, tj sta da se ne hendluje
----
switch - if elif elif else ili objekat (dict) sa fjama
----
@singledispatch decorator - razliciti overloadi za svaki tip
samo na module scope fje, ne za methods
ako za metode hack onda double-dispatch, prvi tip self, drugi je arg
-------------------------------------------
## 3. byte oriented programming
# 1. everything is bits and bytes
non-text data
od python3 separacija str - utf-8, i bytes tip
----
# pregled poglavlja
bitwise operators, integer representation, bytes type, bytearray type
packing, unpacking binary data with struct module, memoryview object
memory mapped files
-------------------------------------------
# 2. bitwise operations on integers - BITOVI
& - and
| - or
^ - xor
~ - not
<< - left shift
>> - right shift
------
# binary litteral, kostanta
0b11110000 # 0b prefix, nula, ne o
bin(240) # vraca isto 0b11110000, decadni broj unutra
-----
# ^ xor - daje 1 za neparan broj jedinica, dva ista daju 0, moduo 2
bin(0b11100100 ^ 0b00100111)
'0b1100011'
-----
# ~ negacija
python ne stampa leading zeros, ok
bin(~0b11110000)
'-0b11110001' 
rezultat nije samo obrnut nego u dvojcinom komplementu
----
dvojcin komplement sluzi samo za negativne integere, zapazi
dvojcin komplement - obrnu se bitovi pa + 1, ok secam se
two's complement
da bi se zadrzale leading zeros
-----
# 8 bitova 
-127 <= x <= 128
primer - 58 -> 58 (00111010) -> flip (not operator) 11000101 (197)
-> +1 (11000110) = 198 sto je van 128 opsega, zapazi, zato se zna da je negativan
to je -58, ok
-----
dvojcin komplement ima prednosti nad oznacenim integerom i magnitude (abs value, amplituda)
prednosti: jedna nula, sabiranje i oduzimanje lako se rade
-----
u pythonu 3 integers su unlimited precission, nije 1, 2, 4 bytes
dvojcin komplement radi sa fixed width # to
---
bin(-4)
'-0b100' # ne stampa sta je stvarno unutra, pravu reprezentaciju u 2 compl
nego znak i magnitude - abs value
----
# resenje...?
treba samo jedna leading 0
nije mnogo komplikovano, nije bas nasjno sta pokusava
podseti se binarno i, ili, xor tablice i  principi
---
trazi negaciju od 240 0b11110000, treba da dobije 15 tj 0b00001111
-241 + 256 = 15
sabere sa 256 tj 2**8, tj to je levi shift za 8 pozicija
dvojcin komplement od poziticnog integrea je isti kao original
---
# sve vreme objasnjava kako da izracunas dvojcin komplement broja
# fja za racunanje dvojcinog komplementa
def twos_complement(x, num_bits):
    # ako je pozitivan - nista vrati ga samo
    if x < 0:
        # negativan - sabiranje sa velicinom registra, shift 8 je samo pojednostavljeno
        return x + (1 << num_bits)
    return x
----
# drugi nacin
& (and) sa maskom sa svim 1
and -> 1 i 1 = 0, 1 i 0 = 1
bin(~0b11110000 & 0b11111111)
'0b1111'
---
int(32).bit_length() # fja izracunava koliko bitova je potrebno za broj, iskljucujuci znak
6
int(-241).bit_length()
8
int(256).bit_length() # 2**n+1, npr 8=1000, 2**3, nulta pozicija desno zapravo
9
-------------------------------------------
# 3. byte-wise operations on integers - sad BAJTOVI, zapazi
# to_bytes() - fja za bytes representation
# 1. arg length result, 2. arg redosled, endian
int(0xcafebabe).to_bytes(length=4, byteorder='big')
b'\xca\xfe\xba\xbe'
# zadao hexa da bi rez bio isti kao ulaz
int(0xcafebabe).to_bytes(length=4, byteorder='little') # little obrnuo rezultat
b'\xbe\xba\xfe\xca'
-----
big endian - bajt najvece tezine prvi
little endian - bajt najmanje tezine prvi
-----
# citanje native byteorder za masinu
import sys
sys.byteorder
'little' # i kod mene na intel isto, probao
------
# obrnuto, byte object to integer from_bytes()
int.from_bytes(bytes_obj, byteorder=sys.byteorder)
340569182 # decadni int
# decadni to hex hex() fja
hex(10)
-----
# signed=True da bi rez stao u 2 bajta, prethodni negac 2cin komplement
int(-241).to_bytes(2, byteorder='little', signed=True)
b'\x0f\xff' # f = 15 = 1111, zasto duplo ff?
-------------------------------------------
# 4. bytes type in depth
str - immutable sequence of unicode code points
bytes - immutable sequence of bytes
----
# bytes literal - b'...' ili b"..."
b'literal byte string'
chars u b'' ograniceni na 7bit ascii, 0-127
za utf8 SyntaxError
za 128-255 mora escape \x
b"norvegian chars like \xc5 and \xd8 are not 7-bit ascii"
ovo su samo bajtovi, da ih prebacis u utf8 moras da znas-prosledis encoding
norsk.decode('latin1')
----
norsk[0] # index vraca int, a ne 1 bytes sequence
78
----
norsk[21:25] # slicing vraca novi bytes object
b'like'
----
# bytes() constructor
bytes() # prazan obj
b''
----
bytes(5) # popunjen nulama
b'\x00\x00\x00\x00\x00'
----
bytes(range(65, 65+26)) # ne sme negativne i vece od 255
b'ABCD...XYZ'
----
bytes('norveski', 'utf16') # drugi arg encoding
----
# classmethod od stringa sa hexa od 2 cifre spojenih
bytes.fromhex('1234...cdef')
b'neki ascii chars'
# nema ugradjeni za kontra nego
''.join(hex(c)[2:] for c in b'the quick brown fox')
-------------------------------------------
# 5. bytearray type
bytes - immutable
bytearray - mutable, kao list, pa slicne fje
-----
# constructors, isti kao bytes()
bytearray()
bytearray(b'')
-----
bytearray(5) # nule
bytearray(b'\x00\x00\x00\x00\x00')
----
bytearray(b'ascii text')
bytearray(b'ascii text')
-----
bytearray('norvegian chars', 'utf16')
-----
bytearray.fromhex('1234...')
bytearray(b'ascii text')
------
# mutable
bytearrayObj.extend(' some additional text')
bytearrayObj[40:43] = 'dodeli ovo'
bytearrayObj.upper()
bytearrayObj.split() # u listu reci
-----
# str i bytes se donekle preplicu, oba mogu text da imaju
bytes stringovi - samo ascii, nema encoding, 
str stringovi - utf8
-------------------------------------------
# 6. interpreting binary structures - konvertovao iz C u python
python cita C strukture
-----
C float 32 (single), python float 64 (double)
----
# reader.py, read first vertex from file
def main():
    with open('colors.bin', 'rb') as f: # 'b' - open in binary mode
        buffer = f.read() # reads entire file

    # read raw bytes sequence into friendy type
    # format string, how to interpret bytes
    items = struct.unpack_from('@fffHHH', buffer)
    print(repr(items)) # vraca tuple

@ - native byte order and alignment, @ je default, first char
fff - svaki f jedan single precision C float, 32 bits, moze i 3f
HHH - unsignet C short int, 16 bits
postoji tabela sa ovim mapiranjem tipova C na python
-----
floati se priblizno konvertovali od C decimal 32 na python 64 hexa
int se tacno preneli
----------
atributi klase se navode u __init__() kao args, kao u Typescript, zapazi
Vector, Color i Vertex klase u python
cita fajl i kreira Vertex[] u petlji sa helper fjom
----
fn(*fields) - rest za positional args unpacking
kada debugiras binarne podatke nisu citljivi za coveka, mora da decodiras u ascii
---
C kompajler dodavao 2 bajta nule "padding" da bude umnozak 4
'@3f3Hxx' - x - 2 pad bytes
C i python koristili razlicit C kompajler, zato razlicit default struct padding
-----
# memmap.py
def main():
    with open('colors.bin', 'rb') as f:
        buffer = f.read()

    # debugiranje, binary to ascii
    print("buffer: {} bytes".format(len(buffer)))

    indexes = ' '.join(str(n).zfill(2) for n in range(len(buffer)))
    print(indexes)

    hex_buffer = hexlify(buffer).decode('ascii')
    hex_pairs = ' '.join(hex_buffer[i:i+2] for i in range(0, len(hex_buffer), 2))
    print(hex_pairs)
    # debugiranje dovde

    vertices = []
    for fields in struct.iter_unpack('fffHHHxx', buffer): # xx popravilo
        vertex = make_colored_vertex(*fields)
        vertices.append(vertex)

    pp(vertices)
-------------------------------------------
# 7. memoryveiw type - ugradjeno polje bajtova
built-in tip za array of bytes - kolekcija bajtova, sequence of python objects
implements buffer protocol u C 
-----
cita se kao polje, lista
mem[12:18] - slice npr
mem[12:18].cast('H') - isto kao struct, svi elem istog tipa, H - unsigned short int, isto je memoryview
mem[12:18].cast('H').tolist() - u listu
cast() kastuje u C struct tip
-----
# debugiranje, staje u repl i izbaci sve local vars
# ctr d linux, ctrl z win da izadjes, end of file char
code.interact(local=locals()) - immediate window u C#
-----
# zamenio atribute (bez tipa - self.nesto) u klasama sa memoryview delovima polja sa indexima i binarnim tipom iz C struct
da ne bi dva puta alocirao bufere - promenljive za podatke iz fajla
------
buffer = f.read()
mem = memoryview(buffer)
-----
iscitao bin fajl u klase sa memoryview i odstampao
-------------------------------------------
# 8. memory mapped files
memory mapped file - OS feature, fajl kao u memoriji
mmap - python implem - obj kao bytearray and file
-----
# reader.py primer
mmap je file-like object, mora biti zatvoren posle upotrebe - with blok
with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as buffer:
----
release baca exception je Vertex klasa drzi refs na mmap
obrisi refs posle stampanja da ih pokupi garbage colector, rucno
del vertices
del mem
-------------------------------------------
# 9. summary
xor, not operatori
negativni integeri u twos complement
and sa maskom jedinica
bytes - immutable sequence
bytearray - mutable sequence
read C struct
hexlify() za stampanje
byte boundaries zavise od C compilera
memoryview - delovi bytes sequence bez kopija u vars
code.interact() - pause and debug u repl
memory maped files - impl buffer protocol
------
# dodatno, bytes in python
writable memoryview
shared memory with mmap
interfacing native C/C++ code, mesanje verovatno
-------------------------------------------
## 4. object internals and custom attributes 
# 1. introducing object internals
object in python - internally implemented with __dict__ dictionary
__dist__ moze da se menja, mutira?
----
# custom atribute access
__getattr__()
__getattribute__()
__setattr__()
__delattr__()
----
# objects more memory efficient
__slots__
-------------------------------------------
# 2. how are python objects represented
# Vector klasa, valjda Point... vector_01.py
class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y
---
# poziv
v = Vector(5, 3)
dir(v) # stampa dosta __attr__...
v.__dict__ # obican dict
{'y': 3, 'x': 5}
v.__dict__['x'] # 5, create, read, write, delete, moze direktno
# koriste se helper seteri, geteri
getattr(), hasattr(), delattr(), setattr()
----
matematicki vektor u razlicitim koordinatnim sistemima
----
# sakrio p, q u privatne attr sa _p, _q, i repr() za toString()
class Vector:
    def __init__(self, **coords): # ...rest all named args
        private_coords = {'_' + k: v for k, v in coords.items()}
        self.__dict__.update(private_coords) # add attrs like this
        
    def __repr__(self): # toString(), nebitno
        return "{}({})".format(self.__class__.__name__,
                               ', '.join("{k}={v}".format(k=k[1:], v=self.__dict__[k])
                                         for k in sorted(self.__dict__.keys())))
----
v.p baca exception sad
-------------------------------------------
# 3. overriding getattr
# onBeforeNotFoundError(), samo taj, ne bilo koji exception, AttributeError
__getattr__() - invoked after requested attribute property not found by normal lookup 
__getattribute__() - invoked instead of normal lookup
----
# vector_05.py, stao na ovome, read ok, mutable not ok, tu stao u ovom klipu
# rekurzija ovde? getattr(self...) zove __getattr__()? - jeste
def __getattr__(self, name):
    private_name = '_' + name
    return getattr(self, private_name) # zapazi getattr() na self, podseti se getattr()
-------------------------------------------
# 4. overriding setattr
# vector_06.py, baci AttributeError exception iz __setattr__(), trivial 
def __setattr__(self, name, value):
    raise AttributeError("Can't set attribute {!r}".format(name))
-------------------------------------------
# 5. pitfalls with getattr
getattr i hasattr pozivaju __getattr__() za exception
ako pozivas nepostojeci prop ide infinite rekurzija
----
# resio ovako, look before you leap
if private_name not in self.__dict__:
# ili easier to ask forgivenes than permision
try:
    return self.__dict__[private_name]
except KeyError:
    raise AttributeError # translate to another exeption
-----------
exception je skup samo kad udje u catch, zapazi, samo try nije
-------------------------------------------
# 6. overriding delattr
spreci slucajno brisanje attr klase
delattr(v, '_p')
del v._q
----
# spreci brisanje public i private atributa klase
def __delattr__(self, name):
    raise AttributeError("Can't delete attribute {!r}".format(name))
-------------------------------------------
# 7. customizing attribute storage
# napravio izvedenu klasu sa listom, mutable
class ColoredVector(Vector):

    COLOR_INDEXES = ('red', 'green', 'blue')

    def __init__(self, red, green, blue, **coords):
        super().__init__(**coords)
        self.__dict__['color'] = [red, green, blue] # ovde direktno u __dict__
----
# u __dict__['color']
self.__dict__['color'] = [red, green, blue]
# direktno u __dict__
self.__dict__.update(private_coords)
------
popravio repr() u base class Vector
-------------------------------------------
# 8. direct vs indirect access to dict
vars() - bez args vraca isto sto i local()
sa arg vraca arg.__dict__
-----
# poenta
obj.__dict__ === vars(obj)
-------------------------------------------
# 9. overriding getattribute
nesto slozenija lekcija, poenta LoggingProxy(obj) klasa loguje svaki pristup obj.prop
----
__getattr__() - samo onBeforeAtributeError - fallback
__getattribute__() - getter, on bilo koji pristup - kompletan override
-------
__getattribute__() - root implementacija (obj.prop) je u object.__getattribute__()
-------
# logging_proxy.py
# loguje svaki pristup attributu objekta
class LoggingProxy:

    # target je obj bilo koje klase
    # target se cuva u base klasi
    def __init__(self, target):
        super().__setattr__('target', target)

    # evo ga override
    def __getattribute__(self, name):
        target = super().__getattribute__('target') # ovde poziv, zapazi
        ...
        # print()
        # vraca zahtevani prop
        return value
-----
# poziva u repl, a ne u pycharm run, jer bi on access mnoge props
cv = ColoredVector(...)
cw = LoggingProxy(cv) # eto prosledio objekat druge klase
cw.p # sad loguje
---
# write fails jer postavlja na object.__setattr__ LoggingProxy klase, a ne ColoredVector
cw.p = 19 # immutable
cw.red = 5 # mutable
-----
# resenje override i __setattr__()
def __setattr__(self, name, value):
    target = super().__getattribute__('target')
-------
podseti se super() detalja sta vraca, base class vraca, samo za visestruko nasledjivanje...
-------
cw.p = 19 # write immutable prop baca exception
-------------------------------------------
# 10. attribute lookup for special methods
poenta:
__getattribute__() se poziva samo za pristup sa tackom, obj.prop
za npr specijalni method repr(), len(), iter()
repr(cw) # ne poziva se
cw.__repr__()  # poziva se ali ovo nije uobicajena sintaksa
-----
# resenje - override i __repr__()
def __repr__(self):
    target = super().__getattribute__('target')
    repr_callable = getattr(target, '__repr__')
    return repr_callable() # ok, logicno, nije lose
-------------------------------------------
# 11. where are methods stored
obj.__dict__ - stampa samo attrs, a ne methods
obj.__class__.__dict__.__getattr__() - methods su na __class__.__dict__
-----
v = Vector(x=3, y=7)
v.__dict__
{'x': 3, 'y': 7}
----
v.__class__.__dict__
mappingproxy({...refs...})
----
# zapazi prosledio self u (), slicna fora kao js bind(this)
v.__class__.__dict__['__repr__'](v) # pozvao repr()
'Vector(x=3, y=7'
-----
# _class__.__dict__ je tipa mappingproxy koji ne dozvoljava dodelu, class dictionary
v.__class__.__dict__['__some_vector_class_attribute__'] = 5
# mora setattr() za dodelu, ovako
setattr(v.__class__, '__some_vector_class_attribute__', 5)
----
ovo 4. poglavlje sa objektima je slicno kao bind(), call(), __proto__ ... u javascriptu
-------------------------------------------
# 12. trading size for dynamism with slots
slots - python feature da se ustedi ram
d = {} # dict
sys.getsizeof(d)
288 # bajtova, prazan dict
------
# primer klase
class Resistor:

    # class attribute, sa listom instance attributa koje definises, isti kao na self
    __slots__ = ['resistance_ohms', 'tolerance_percent', 'power_watts']

    def __init__(self, resistance_ohms, tolerance_percent, power_watts):
        self.resistance_ohms = resistance_ohms
        self.tolerance_percent = tolerance_percent
        self.power_watts = power_watts
-----
sada r1.__dict__ vise ne postoji
ne moze vise dinamicki da se dodaju props na instancu, r1.nesto = 5
smanjio 152 na 64 bytes
----
koristi se ponekad d ase izbegne pisanje u C
-------------------------------------------
# 13. summary
__dict__ - stores instance attributes
moze da se cita i upisuje u njega
__getattr__() - on prop not found
__setattr__(), __delattr__() - mogu da se override
hasattr() poziva __getattr__(), moze rekurzija
__getattribute__() - on bilo koji attribute pristup, object, super()... 
__class__.__dict__ - tu su methodi
__slots__ - za ustedu memorije, ali less dynamic
-------------------------------------------
## 5. descriptors
# 1. introducing descriptors - C# getters and setters ekvivalent, eto
# in other programming languages, descriptors are referred to as setter and getter
# https://www.geeksforgeeks.org/descriptor-in-python
----
Descriptors are the mechanism to implement properties in python
create property with property constructor
---
create specialized property with custom descriptor
----
descriptors:
1. data descriptors
2. non-data descriptors
----
attribute lookup...
----
# primer klasa pluto = Planeta()
u propertijima vrsi validaciju, pa u __init__() dodeljuje property i tako validira constructor
self.name = name
-------------------------------------------
# 2. properties are descriptors
@property je decorator za getter
property object ima fget i fset koji su refs na getter i setter fje
descriptor je property object # eto
------
# decorator je samo sintax sugar za higher order fju
# gde je vracena fja rebindovana na ime originalne fje
# 1. decorator syntax
@decorator
def f():
    do_something()
-----
# 2. regular function syntax
def f():
    do_something()

f = decorator(f) # rebinds on same name f
----
decorator je funkcija # zapazi
prima i vraca funkciju, hof
------
# help(property) - izbaci implementaciju
# property je klasa
class property(object)
 |  property(fget=None, fset=None, fdel=None, doc=None)
 |  
 |  Property attribute.
 |  
 |    fget
 |      function to be used for getting an attribute value
 |    fset
 |      function to be used for setting an attribute value
 |    fdel
 |      function to be used for del'ing an attribute
 |    doc
 |      docstring
 |  
 |  Typical use is to define a managed attribute x:
 |  
 |  class C(object): # lokalna klasa, ko zna...
 |      def getx(self): return self._x
 |      def setx(self, value): self._x = value
 |      def delx(self): del self._x
 |      x = property(getx, setx, delx, "I'm the 'x' property.") # zove constructor ovde i vraca class attribute
 -----
 properties create objects called descriptors which are bound to class attributes
------
# sada pravi primer sa class Planet() gde menja property-je definisane preko decoratora
# sa obicnom raw implementacijom preko fje
-----
# radius_meters je property object
# property() je constructor iz help, tj decorator rucno pozvan
radius_meters = property(fget=_get_radius_meters, fset=_set_radius_meters)
----
property is a function koja vraca object - descriptor bound to a class attribute
class attribute - staticka var na klasi
-------------------------------------------
# 3. implementing a descriptor - teska lekcija prilicno
descriptor protocol
----
# primer, ogranici na pozitivne vrednosti
sve __nesto__() su deo protocola generalno u pythonu? # to, uglavnom, zapazi
----
# planet.py, ova klasa je descriptor
class Positive:

    def __init__(self):
        self._instance_data = WeakKeyDictionary() # zapazi weak dict...

    def __get__(self, instance, owner): # ovi args se menjaju naravno
        if instance is None:
            return self
        return self._instance_data[instance]

    def __set__(self, instance, value):
        if value <= 0:
            raise ValueError("Value {} is not positive".format(value))
        self._instance_data[instance] = value # instanca je kljuc u dict

    def __delete__(self, instance):
        raise AttributeError("Cannot delete attribute")
------
# zamenio sve getters, setters i property objects sa pozivom descriptora
# ovo su class attrs u klasi Planet i descriptori, zapazi
radius_metres = Positive() # descriptor je class attr
mass_kilograms = Positive()
orbital_period_seconds = Positive()
surface_temperature_kelvin = Positive()
-----
# ova dodela je class attr u instance attr, eto
# zapravo radius_metres je poziv gettera na descriptoru Positive()
self.radius_metres = radius_metres
---------
class Planet:

    def __init__(self,
                 name,
                 radius_metres, # a ovi argumenti sta su?
                 mass_kilograms,
                 orbital_period_seconds,
                 surface_temperature_kelvin):
        self.name = name
        self.radius_metres = radius_metres
        self.mass_kilograms = mass_kilograms
        self.orbital_period_seconds = orbital_period_seconds
        self.surface_temperature_kelvin = surface_temperature_kelvin

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if not value:
            raise ValueError("Cannot set empty Planet.name")
        self._name = value

    radius_metres = Positive() # eto ih descriptori, class attrs
    mass_kilograms = Positive()
    orbital_period_seconds = Positive()
    surface_temperature_kelvin = Positive()
-----------
# prilicno slozeno
# kako se pozivaju metodi descriptora na citanje i dodelu prop
# stvarni args, pluto - instance, Planet - owner descriptora, ovo je descriptor, class attr mass_kilograms = Positive()
m = pluto.mass_kilograms (citanje prop) -> m = Positive.__get__(self, pluto, Planet) 
# pluto - instance, m - value, vidis levo i desno sta je sta
pluto.mass_kilograms = m (dodela prop) -> Positive.__set__(self, pluto, m) 
-----
descriptor pripada klasi, a ne instanci, ok class attr, a ne  instance attr
------
WeakKeyDictionary ne zadrzava vrednosti koje...???
ako je kljuc (pluto instanca) (weak KEY) pobrisan na svim drugim mestima, a ostao samo u WeakKeyDictionary i on je brise, to je to
weak references - ??? Youtube, heap, garbage collector, istrazi, strong, weak, soft ref
jeste instanca key, a masa npr value - {pluto: 1.31e22}
-----
slika oslikava klase iz gornjeg koda, nije ni tesko
cela frka da u Positive klasi zna koji attr iz Planet se cita ili upisuje
-------------------------------------------
# 4. calling desriptors on clases - kratko
descriptor je class attribute
pluto.radius_meters # pristupljeno na instanci
Planet.radius_meters # pristupljeno na klasi, instance arg je None
----
# vratio descriptor, self
def __get__(self, instance, owner):
    if instance is None: # hendlovao if i to je to
        return self
-------------------------------------------
# 5. data vs non-data descriptors
data descriptor - __get__(), __set__(), __delete__()
non-data descriptor - samo __get__(), read-only # ok
------
sav attr lookup zavisi od __getattribute__()
obj.attr se svodi na:
type(obj).__dict__['attr'].__get__(obj, type(obj))
instance attrs su bese u __dict__['attrs']
-----
precedence (prioritet) za attr sa istim imenom:
1. data descriptor in class
2. instance attr in __dict__[]
3. non-data descriptor in class
--------
# primer, precedence.py
class DataDescriptor:
    def __get__(self, instance, owner):
    def __set__(self, instance, value):

class NonDataDescriptor:
    def __get__(self, instance, owner):

class Owner:
    a = DataDescriptor() # ovo je obj.a i gazi obj.__dict__['a']
    b = NonDataDescriptor() # ovo je obj.b i ne gazi obj.__dict__['b']
-------------------------------------------
# 6. summary
descriptor je property sa custom ponasanjem, npr validacija # ok
# iskopirao slajd:
Reviewed built-in property decorator
Create property descriptors without decorators
Implement the descriptor protocol
Store per-instance attributes
Class versus instance descriptor lookup
----
metaclasses - descriptor instance to know name of class attrs to which they are bound # rastumaci posle pazljivo
----------------------
# https://realpython.com/python-descriptors
# getteri i setteri, samo sto su u posebnoj klasi
# descriptors.py, u posebnojj klasi
class Verbose_attribute():
    def __get__(self, obj, type=None) -> object:
        print("accessing the attribute to get the value")
        return 42
    def __set__(self, obj, value) -> None:
        print("accessing the attribute to set the value")
        raise AttributeError("Cannot change the value")

class Foo():
    attribute1 = Verbose_attribute() # descriptori se kace na class attr

my_foo_object = Foo()
x = my_foo_object.attribute1 # ovo okida __get__()
print(x)
-------------------------------------------
## 6. instance creation 
# 1. instance creation 
invariance - validacija, ogranicenja propa, atributa
-----
instance 1. allocation and 2. initialization
-----
# chess.py
class ChessCoordinate:

    # file - x = 'a-h', rank - y = 1-8
    def __init__(self, file, rank):

        # ogranicenja
        # length === 1
        if len(file) != 1:
            raise ValueError("{} component file {!r} does not have a length of one."
                             .format(self.__class__.__name__, file))

        if file not in 'abcdefgh':
            raise ValueError("{} component file {!r} is out of range."
                             .format(self.__class__.__name__, file))

        if rank not in range(1, 9):
            raise ValueError("{} component rank {!r} is out of range."
                             .format(self.__class__.__name__, rank))
        # privatni
        self._file = file
        self._rank = rank

    # immutable, readonly
    @property
    def file(self):
        return self._file

    @property
    def rank(self):
        return self._rank
-------
u __init__() self arg vec postoji
ima tip (klasu), __dict__ je prazan
__init__() ne vraca self, nego ga mutira
inicijalicuje self, kao setter
-----
dir(ChessCoordinate) - __new__, iz object dolazi
-------------------------------------------
# 2. allocating with __new__()
# class method, static method, cls - klasa, mesto self, jer self jos ne postoji naravno
def __new__(cls, *args, **kwargs):
    # args su iz constructora ChessCoordinate()
    print(args, kwargs)
    obj = super().__new__(cls)
    return obj # obj je self koji se prosledjuje u __init__()
-----
# alociranje novog objekta, new kao u C++
object.__new__(cls)
__new__() vraca kreirani self, prima args constructora
-------------------------------------------
# 3. customizing allocation
overriduje __new__() - customizuje alokaciju
----
interning - storing only one copy of each distinct value
moguce samo za immutable types
-----
# primer, samo inicijalne figure na tabli, ne cela partija, niti pomeranje figura
dict - key jednoznacno odredjen pijun npr
value - polje na tabli
-----
kreira 10 000 tabli i gleda memoriju u task manager
------
# ok, zapravo trivijalan primer sa unique cache
# ustedeo memoriju
class ChessCoordinate:

    # unique cache
    _interned = {}

    def __new__(cls, file, rank):

        # validacija iz __init__() sad ovde
        if len(file) != 1:
            raise ValueError("{} component file {!r} does not have a length of one."
                             .format(cls.__name__, file))

        if file not in 'abcdefgh':
            raise ValueError("{} component file {!r} is out of range."
                             .format(cls.__name__, file))

        if rank not in range(1, 9):
            raise ValueError("{} component rank {!r} is out of range."
                             .format(cls.__name__, rank))

        key = (file, rank)
        # kreiraj novi samo ako ne postoji
        if key not in cls._interned:
            # super() je object ionako
            obj = super().__new__(cls)
            # inicijalizacija moze i ovde
            obj._file = file
            obj._rank = rank
            cls._interned[key] = obj

        # vrati stari
        return cls._interned[key]
------
poenta: primer overridovanja __new__()
-------------------------------------------
# 4. summary
poenta: celo poglavlje je bilo o __new__()
------
razlika:
__new__() - alokacija, alocira i vraca new instances
__self__() - inicijalizacija
----
__new__() je class-method ali ne zahteva @classmethod ili @staticmethod decorator
object.__new__() se zapravo uvek poziva, ultimate allocator
-----
interning - za immutable type sa zajednickim vrednostima, ili ogranicen broj vrednosti
----
moguce jos kontrolisati instance creation at class level - metaclasses 
-------------------------------------------
## 7. metaclasses
# 1. metaclasses
metaclasses - class of a class object
slicno kao base class ali za class object - tip
-----
class Widget:
    pass
----
# object
w = Widget()
type(w)
<class '__main__.Widget'> # u repl
-----
# klasa
type(Widget)
<class 'type'>
-----
# constructor i tip su isti
a = list()
type(a)
<class 'list'>
--------
type is metaclass of Widget, kao parent ali za klasu, tip klase (tipa)
type is default metaclass
object za instance, type za klase
------
type(type)
<class 'type'>
--------
# isto samo __class__ umesto type()
# object
w.__class__
<class '__main__.Widget'>
# klasa
w.__class__.__class__
<class 'type'>
# tip klase, default metaclass, rekurzija
w.__class__.__class__.__class__
<class 'type'>
--------
# how class objects are created
class Widget:
    pass
# je skraceno od:
# gde je object base class - object - default base class
# type metaclass - type - default metaclass
class Widget(object, metaclass=type):
    pass
-------------------------------------------
# 2. class allocation and initialization - dugacka lekcija
kako se definise klasa # to, to
sve ovo zato sto je klasa objekat - class object # ok
namespace je dict
-----
class Widget:
    pass
ova definicija je sintax sugar za kreiranje namespace dictionaryja, class block je value
koji se prosledjuje metaklasi da konvertuje dict u class object
-----
metaclass kreira klasu (class object)
-----
name = 'Widget' # name
metaclass = type # default
bases = () # nema parent klasu, default object
kwargs = {}
# __prepare__() method kreira namespace dict
# runtime populates dict with class block from definition
namespace = metaclass.__prepare__(name, bases, **kwargs)
# __new__() allocira se class object
Widget = metaclass.__new__(metaclass, name, bases, namespace, **kwargs)
# __init__() inicijalizuje class object
metaclass.__init__(Widget, name, bases, namespace, **kwargs)
------
poenta: klasa je objekat, kreira se sa __new__() i __init__() kao i drugi obj, samo specifican
------
name, bases, namespace - telo klase, attributi i metodi
--------
# customize class creation
# TracingMeta - logs args
class TracingMeta(type): # nasledjuje type

    # svi su class methods implicitno, ali samo ovaj je dekorisan
    @classmethod
    def __prepare__(mcs, name, bases, **kwargs):
        ...
        namespace = super().__prepare__(name, bases)
        ...
        return namespace

    def __new__(mcs, name, bases, namespace, **kwargs):
        ...
        cls = super().__new__(mcs, name, bases, namespace, **kwargs)
        ...
        return cls

    def __init__(cls, name, bases, namespace, **kwargs):
        ...
        super().__init__(name, bases, namespace)
--------
# poziv, u repl
class Widget(metaclass=TracingMeta):
    def action(message):
        ...
    the_answer = 42
----
__prepare__(), __new__(), __init__() se izvrse cim je klasa definisana
----
# __prepare__()
mcs - metaclass object, ekvivalentan to self i cls u instance i class methods
u ovom slucaju <class 'tracing.TracingMeta'>, prosledjena
----
Name - ime klase
base - base classes - () empty tuple - object implicitly
kwargs - 
namespace - dict {}, ovde je prazan
-------
# __new__()
mcs
name
bases
namespace - primljen kao arg, sad je runtime popunio ovaj dict
sadrzi action method i the_answer class attribut
__module__ - modul gde je definisana klasa Widget, builtin za repl
__qualname__ - fully qualified name, samo Widget za repl
kwargs - empty {}
cls = super().__new__() kreira klasu <class 'Widget'> i vraca je, super() je base class_ type (kao object za instancu)
pre ovoga je mesto za menjanje namespace dict argumenta
-------
# __init__()
sluzi da configurise kreirani class object
__init__() ovde je instance method, a ne classmethod kao preth 2, jer ima cls, a ne mcs
mcs - cls - self -> metaklasa, klasa, instanca # zapazi nivoi, dobar
---
ostali args su isti
__init__(cls, name, bases, namespace, **kwargs):
namespace arg je prosledjen ali menjanje ovde nema efekta, mora cls da mutiras direktno
mogao si da customizujes bases, namespace (vars and methods) i cls (vratis drugu klasu skroz)
---------------
# koji method da overridujes?
__prepare__() - ako hoces da menjas namespace dict
uglavnom se overriduju __new__() i __init__(), isto za metaklase kao i za regular classes
__new__() utice pre nego je class obj allocated
__init__() za konfiguraciju umesto __new__()
-------------------------------------------
# 3. metaclass keyword arguments
# malo poznato, dodatni args koji mogu da se proslede u poziv constructora
class Widget(object, metaclass=type, more=1, keyword=2, args=3):
    pass
---
pretvaraju poziv klase u class factory
dodatni args z konfigurisanje kreiranog class object-a
# zapravo drugi i na dalje args
class SomeClass(metaclass=TracingMeta, tension=496):
----------
# entries.py primer
obe __new__() i __init__() moraju da imaju isti signature, da hendluju iste args
-----
def __new__(mcs, name, bases, namespace, num_entries, **kwargs):
# __init__() mora da ima default implementaciju, ne sme biti izostavljen
# u telu se dodatni arg ne upotrebljava
def __init__(cls, name, bases, namespace, num_entries, **kwargs):
    super().__init__(name, bases, namespace)
-------------------------------------------
# 4. metaclass method visibility
# tracing.py primer
TracingMeta primer klasa je metaklasa sve vreme
u metaklasi kroz instance methode je prosledjen cls, a ne self # zapazi
class TracingMeta(type):
    def metamethod(cls):
----
# poziv, Widget je obicna klasa sad
class Widget(metaclass=TracingMeta):
    pass
ova definicija odmah izvrsi metaklasu
sada je instance method metaklase postao classmethod u obicnoj klasi, i moze mu se pristupiti # ok, logicno
Widget.metamethod() # moze na klasi
cls = <class 'Widget'> # obican classmethod, ok
----
w = Widget()
w.metamethod() # ali ne moze na instanci, a obican classmethod bi mogao, ok
---
za ista imena - override obican classmethod ima precedence nad metamethod-om
--------
# paralela, obicna klasa i metaklasa
mcs -> cls -> self
# obicna klasa
class MyClass(object, metaclass=MetaClass):

    # classmethod obicne klase
    @classmethod
    def my_class_method(cls): # zapazi argumente svuda
        pass

    # instance method obicne klase
    def my_instance_method(self):
        pass
# metaklasa
class SomeMetaClass(type):
    
    # classmethod metaklase
    @classmethod
    def my_meta_class_method(mcs):
        pass
    
    # instance method metaklase
    def my_meta_instance_method(cls): # klasa je instanca metaklase, poenta celog poglavlja
        pass
------
# ok, logicno, nista specijalno
# metaklasa   # obicna klasa
mcs       ->    cls
cls       ->    self
-----
sve ovo je slicno kao call, bind fore u javascriptu samo jos gore # hm, ipak OOP pre...
-------------------------------------------
# 5. metaclass call the instance constructor
override __call__() in metaclass
__call__() je metamethod
__call__() je definisan u metklasi i:
makes objects callable - ()
metaclass.__call__() invokes regular class __new__() and __init__()
-----
class Widget(object, metaclass=type):

    # __new__() cls, zapazi
    def __new__(cls, *args, **kwargs):
        return type.__new__(cls)
    
    # __init__() self, zapazi
    def __init__(self):
        pass
----
# in metaclass
def __call__(cls, *args, **kwargs):

    # na cls koji je prosledjen, instanca u metklasi
    obj = cls.__new__(*args, **kwargs)

    # na obj instanci koja je vracena u prethodnoj liniji
    obj.__init__(*args, **kwargs)
    return obj
-----
na ovaj nacin klase postaju callable - w = Widget()
-------
# metaklasa, poziva se kad je obicna klasa definisana
class TracingMeta(type): 
    def __call__(cls, *args, **kwargs):
        # super() vraca type ovde, type.__call__()
        obj = super().__call__(*args, **kwargs)
        return obj

# poziv, obicna klasa sa override __new__() i __init__()
class TracingClass(metaclass=TracingMeta):
    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls)
        return obj

    def __init__(self, *args, **kwargs):
        pass
------
# poziv obicne klase, jedan positional i jedan keyword arg
t = TracingClass(42, keyword="clef"):
ovo poziva __call__() na meta klasi
znaci __call__() se poziva kad i constructor obicne klase # to, zapazi
type.__call__() onda poziva __new__() i __init__() na obicnoj klasi - TracingClass
----
type.__call__() orkestrira allokaciju i inicijalizaciju # sustina klase u pythonu
------------
# drugi primer keywordmeta.py
# sprecava positional args, baca exception, ok trivial
class KeywordsOnlyMeta(type):
    # override
    def __call__(cls, *args, **kwargs):
        if args: # eto
            raise TypeError("Constructor for class {!r} does not accept positional arguments.".format(cls))
        return super().__call__(cls, **kwargs)
-----
# metaclass se prosledjuje u definiciju obicne klase uvek, kao keyword arg, zapazi
class ConstrainedToKeywords(metaclass=KeywordsOnlyMeta):
----
# podseti se
*args - skupi sve postional args
**kwargs - skupi sve keyword args
-------------------------------------------
# 6. practical metaclass example
python dozvoljava vise methoda u klasi sa istim imenom, zadnji ima precedence
class Dodgy(metaclass=ProhibitDuplicatesMeta):
    def method(self):
        return "first definition"
    def method(self):
        return "second definition"
--------
# primer, pravi klasu koja baca exception za dupli method
# duplicatesmeta.py
dict koji baca exception za dupli key
----
# nasledjuje dict i pravi custom dict
class OneShotDict(dict):

    def __init__(self, existing=None):
        super().__init__()
        if existing is not None:
            for k, v in existing:
                self[k] = v

    def __setitem__(self, key, value):
        if key in self:
            raise ValueError("Cannot assign to existing key {!r}".format(key))
        super().__setitem__(key, value)

# sada pravi metaklasu koja koristi OneShotDict za namespace dict
class ProhibitDuplicatesMeta(type):

    @classmethod
    def __prepare__(mcs, name, bases): # vraca namespace dict
        return OneShotClassNamespace(name)

# i iskoristi metklasu, ovo sad baca exception
class Dodgy(metaclass=ProhibitDuplicatesMeta):
    def method(self):
    def method(self):

# dodao name arg za deskriptivniju error message
class OneShotClassNamespace(dict):
    def __init__(self, name, existing=None):

# name prosledjen iz metaklase
def __prepare__(mcs, name, bases):
-------------------------------------------
# 7. naming descriptors using metaclasses
nastavak descriptor primera iz prethodnog poglavlja
hoce u exception poruci da odstampa ime atributa koji nije prosao validaciju
bez metaklasa nemoguce
# planet.py
introduce metaclass to associate descriptors with class attributes
-----
# pravi klasu koja ima name attr i cuva name
class Named:
    def __init__(self, name=None):
        self.name = name

# nasledjuje tu klasu
class Positive(Named):
    def __init__(self, name=None):
        super().__init__(name) # zove base constructor

    # stampa name u exception message
    def __get__(self, instance, owner):
    def __set__(self, instance, value):

# metaklasa
class DescriptorNamingMeta(type):
    # modifikuje namespace da cuva name
    def __new__(mcs, name, bases, namespace):
        for name, attr in namespace.items():
            if isinstance(attr, Named):
                attr.name = name # ovde naravno, menja namespace dict po referenci
        return super().__new__(mcs, name, bases, namespace)

# samo upoterbio metaklasu, samu klasu ne dira
class Planet(metaclass=DescriptorNamingMeta):
------
izgleda malo teze ali zapravo nije
-------------------------------------------
# 8. metaclasses and inheritance
hijerarhija:
obicne klase polaze iz object
metaklase polaze iz type # to je glavna razlika
i imaju default __metode__
i prosledjuju se kao metaclass=MyMeta u definiciju obicne klase
------
# metainheritance.py, metaklasa se nasledjuje
# metaklase
class MetaA(type):
    pass
class MetaB(type):
    pass

# obicne klase
class A(metaclass=MetaA):
    pass
class B(metaclass=MetaB):
    pass

# obicna nasledjena
class D(A):
    pass
-----
# poziv
from metainheritance import * # ovako importuje klasu u namespace
type(D)
<class 'metainheritanc.MetaA'> # D je nasledio metaklasu od A, ok
-----
# visestruko nasledjivanje
# ne moze, A i B imaju razlicite metaklase
class C(A, B):
    pass

# ovako moze
# metaklase mogu da se nasledjuju, i visestruko
class MetaC(MetaA, MetaB):
    pass

# prosledi zajednicku metaklasu 
class C(A, B, metaclass=MetaC):
    pass
-----------
do sad prazne metaklase
kombinovanje - mergeovanje __metoda__ metaklasa
----
nasledjivanjem metaklasa opet metaklasa
metaklase se izvrsavaju u vreme definisanja obicne klase
----
ne mogu uvek da se kombinuju metaklase visestrukim nasledjivanjem, zavisno od logike i smisla
vaze pravila visestrukog nasledjivanja, super(), mro, itd
slozeno prilicno
-------------------------------------------
# 9. summary
customizacija kreiranja klasa - zato sluze metaklase # zapazi
sve klase imaju metaklase
metaklasa je type of class object # zapazi
default metaclass je type
metaklasa konvertuje definiciju klase u class object # zapazi
----
__prepare__() must return namespace dict od definicije klase
__new__() must allocate and return a class object
__init__() can configure a class object, and must have same signature as __new__()
__call__() on metaclasses is the instance constructor, poziva se kada kreiras obj obicne klase, makes class callable
visestruko nasledjivanje ima stroga pravila u vezi sa metaklasama
super(), mro, samo koje mogu da se merguju...
-------------------------------------------
## 8. class decorators
# 1. class decorators
class decorators - jednostavnija i manje mocna alternativa za metaklase, preferred
ista namena - customize class creation
----
rade slicno kao function decorators, hof - primaju gotovu definiciju klase, modifikuju je i vracaju novu pod istim imenom
----
# class_decorators.py
# stampa sve obicne atribute klase
# mora da prima JEDAN arg - klasu, i da je vrati, ok
def my_class_decorator(cls):
    for name, attr in vars(cls): # vars() vraca __dict__, object dict poglavle
        print(name)
    return cls

# poziv
@my_class_decorator
class Temperature:
----
izvrsi se kad je importovan modul, definition time
to je definition time zapravo - import module, zapazi
-----
invariant - validacija atributa klase
validirao sve atribute klase tako sto stavio decorator na celu klasu
-------------------------------------------
# 2. enforcing class invariants
# decorator factory - funkcija koja vraca decorator, zato dodatne ()
@(decorator_factory(argument))
class Decorated:
    pass
------
# class_decorators.py
# solidno zakomplikovao, ne previse
invariant - validacija klase, kao html forme, zapazi

# invariant() - decorator factory
# predicate - fja za validaciju koja se izvrsava na svaki attr
def invariant(predicate):

    # ovo je class decorator, koji se vraca, prima class object cls
    def invariant_checking_class_decorator(cls):

        # filtrira methods
        method_names = [name for name, attr in vars(cls).items() if callable(attr)]
        for name in method_names:
            _wrap_method_with_invariant_checking_proxy(cls, name, predicate)

        property_names = [name for name, attr in vars(cls).items() if isinstance(attr, property)]
        for name in property_names:
            _wrap_property_with_invariant_checking_proxy(cls, name, predicate)


        return cls

    return invariant_checking_class_decorator

# spoljna funkcija
def _wrap_method_with_invariant_checking_proxy(cls, name, predicate):

    # kreira function decorator
    @functools.wraps(method)
    def invariant_checking_method_decorator(self, *args, **kwargs):

        # poziva method, default logika
        result = method(self, *args, **kwargs)

        # poziva predikat, tj validira
        if not predicate(self):
            raise RuntimeError("Class invariant {!r} violated for {!r}".format(predicate.__doc__, self))
        return result

    # vraca modifikovanu klasu
    # mora setattr() jer cls.__dict__ je immutable
    setattr(cls, name, invariant_checking_method_decorator)

# poziv

# predicate function
def not_below_absolute_zero(temperature):
    return temperature._kelvin >= 0 # pristupa privatnom attr

# pozvan decorator factory, i prosledjen predikat
# ovo je klasa ciji se attributi validiraju, zapazi
# zapravo samo jedan attribut - kelvin, modifikovan na vise nacina
@invariant(not_below_absolute_zero)
class Temperature:
    def __init__(self, kelvin):
        self._kelvin = kelvin
    def get_kelvin(self):
        return self._kelvin
    def set_kelvin(self, value):
        self._kelvin = value
    ...
------
t = Temperature(-1.0) # testira da li je wrappovan __init__()
s.set_kelvin(-1.0) # testira setter
----
dodao propertyje i validacija fails, nisu handlovani
-------------------------------------------
# 3. detecting and wrapping properties
property decorator produces descriptor that wrapps getter and setter methods # podseti se
celsius.__get__(), celsius.__set__()
----
descriptori nisu funkcije, zato nisu detektovani, treba da se hendluju bolje, wrappuju kako on kaze
----
pp(vars(Temperature)) # debug ovako, vars() ?
----
# resenje,prilicno prosto zapravo
# vraceni class decorator
def invariant_checking_class_decorator(cls):
    property_names = [name for name, attr in vars(cls).items() if isinstance(attr, property)] # property je tip instance ovde
    for name in property_names:
        # wrapper fja
        _wrap_property_with_invariant_checking_proxy(cls, name, predicate)

    return cls

# handler, wrapper fja
def _wrap_property_with_invariant_checking_proxy(cls, name, predicate):
    # hvata property
    prop = getattr(cls, name)
    assert isinstance(prop, property)

    # constructor
    invariant_checking_proxy = InvariantCheckingPropertyProxy(prop, predicate)
    # menja property proxy-jem
    setattr(cls, name, invariant_checking_proxy)

# descriptor - custom property - getter, setter, 5. poglavlje
class InvariantCheckingPropertyProxy:
    # cuva property i prediacate u klasi
    def __init__(self, referent, predicate):
        self._referent = referent
        self._predicate = predicate

    def __get__(self, instance, owner):
        if instance is None:
            return self._referent
        # poziva property, da bi ga vratio
        result = self._referent.__get__(instance, owner)
        # evo ovde izvrsava predicate validaciju da baci exception
        if not self._predicate(instance):
            raise RuntimeError("Class invariant {!r} violated for {!r}".format(
                self._predicate.__doc__, instance))
        return result
    # i u ove dve slicno
    def __set__(self, instance, value):

    def __delete__(self, instance):
-------------------------------------------
# 4. chaining class decorators
moze vise class decoratora, logicno kao i function decoratora ili hof
-----
# dodao dva decoratora
@invariant(below_absolute_hot) # ovaj
@invariant(not_below_absolute_zero)
class Temperature:
-----
sada propertyji opet nisu hendlovani za below_absolute_hot
# ovaj uslov ne prolazi - if isinstance(attr, property)
property_names = [name for name, attr in vars(cls).items() if isinstance(attr, property)]
u below_absolute_hot prolazu vise nisu instanca property-ja
resenje - abstract base class mechanism - iz sledeceg poglavlja
-------------------------------------------
# 5. summary
class decorators - jednostavnija i slabija alternativa za metaklase
sluze za customizaciju definicije klase pre nego sto je vracena u variablu
koristi class decorators kad god mozes, metaklase kad prethodno ne moze
-------------------------------------------
## 9. abstract base classes
# 1. introducing abstract base classes
PEP 3119
u srednjem kursu korisceno collections.abc.Sequence abstr base class
sad kreiranje svojih ABC
----
ABC su mnogo fleksibilnije u pythonu nego u Java, C++, C#
----
base - nasledjena negde
abstract - ne moze base da se instancira, samo nasledjena
concrete - suprotno od abstract # zapazi ne nuzno od base, ne nuzno subklasa tj
---
base mora da definise interface koje subclasses moraju da implement
liskov substitability - sve izvedene klase moraju da rade sa base interface, mogu da se zamene
----
razlikuju se od pure interfaces u Java jer mogu da imaju deo implementacije, a ne samo interface, trivial
-----
suprotno od duck typing, da ipak unapred znas interfejs
----
# ovo su apstraktne klase prakticno
-------------------------------------------
# 2. abstract base classes in python
cemu sluze ABC:
1. definisanje protocola i interfacea
2. da li instanca ili klasa zadovoljavaju protocol
------
from collections.abc import MutableSequence
issubclass(list, MutableSequence) # True, jeste subklasa, kako...
list.__mro__ # list, object - nema MutableSequence u lancu
---
ms = MutableSequence() # exception, jeste apstraktna klasa
5 od 16 metoda moraju biti implementirani
base class ima impl 16 na osnovu 5 iz subklase
----
issubclass(list, MutableSequence) poziva __subclasscheck__(list) na tipu MutableSequence, tj na metaklasi
return type(MutableSequence).__subclasscheck__(list) - ova fja definise da li je list subclass, a ne stvarna hijerarhija # to
takva base klasa je virtuelna (nema veze sa C++)
sta god __subclasscheck__() odluci - implementira - glupost zapravo...?
-------------------------------------------
# 3. abstract base classes in practice
metaklasa ime se zavrsava na KlasaMeta po konvenciji
abstract class uopse ne zahteva pravo nasledjivanje vec samo implementaciju ovih metoda # to
-----
# weapons.py
# metaklasa
class SwordMeta(type):
    # samo proverava da li ima method sharpen()
    def __subclasscheck__(cls, sub):
        # ovde zapravo treba hendlovati i prave subklase
        return hasattr(sub, 'sharpen') and callable(sub.sharpen)

    # za isinstance()
    # samo delegira na gornji method, vraca isto sto i on
    def __instancecheck__(cls, instance):
        return cls.__subclasscheck__(type(instance))

# ovo je virtual base class
class Sword(metaclass=SwordMeta):
    pass

# ne nasledjuje nista
class BroadSword:
    def swipe(self):
    def sharpen(self):
-----
# poziv
# za klase
issubclass(BroadSword, Swoord) # True
metaklasa od Sword proverava postojanje methoda sharpen(), a BroadSword ga ima pa je ispunjeno
# za instance
broad_sword = BroadSword()
isinstance(broad_sword, Sword)
-----
# 2. primer za ugradjenu abs klasu
from collections.abc import Sized

class SizedCollection:
    def __init__(self, size):
        self._size = size # ova fora uvek, self._size zapravo kreira attribut, a size je samo arg, zapazi
    def __len__(self):
        return self._size
# postojanje methoda __len__() je dovoljno da 
issubclass(SizedCollection, Sized) bude True
-------------------------------------------
# 4. non transitive subclass relationships - logika u hijerarhiji nasledjivanja ne vazi
1. issubclass(), issuperclass() ne moraju biti simetricne
2. ni tranzitivnost ne vazi, hijerarhija
A -> B -> C ne znaci da je A -> C
----
# primer hashable
Hashable -> object -> list
list.__hash__() = None, zato nije Hashable -> list
-----
cirkus napravljen sa ovim
-------------------------------------------
# 5. method resolution with virtual base classes
1. virtal base classes
2. regular base classes
-----
nije moguce zvati methode virtual base klase, jer virtual base class nije u mro # ok
class Sword(metaclass=SwordMeta):
    # method na virtual base class
    def thrust(self):
        print('thrusitng')
----
# poziv
broad_sword = BroadSword()
isinstance(broad_sword, Sword) # True
broad_sword.swipe() # moze svoj method
broad_sword.thrust() # ne moze method virtual base klase
BroadSword.__mro__
<class BroadSword> <class object> # nema Sword virtual base klase u mro
----
pa po cemu je to base klasa onda, bzvz?
-------------------------------------------
# 6. library support for abstract base classes
__instancecheck__() i __subclasscheck__() se ne implementiraju direktno vec preko std lib abc module
abc module:
ABCMeta metaclass
ABC base class
@abstractmethod decorator
-----
# primer
from abc import ABCMeta
# zamenio rucnu SwordMeta sa ABCMeta
class Sword(metaclass=ABCMeta):
# sada se logika iz __instancecheck__() i __subclasscheck__() seli u __subclasshook__()
# hendluje i klasu i instance
class Sword(ABCMeta):
    @classmethod
    def __subclasshook__(cls, sub):
----
__subclasshook__() vraca True, False ili not implemented, da li jeste ilii nije virtual base class
-----
ovo je registrovanje virtual base class
sve klase koje imaju method swipe(), njima je sada Sword virtual base class
-----
# poziv
issubclass(SamuraiSword, Sword) # True
isinstance(s, Sword) # True i za instancu
-------------------------------------------
# 7. virtual subclass registration
sada registrovati class as a virtual subclass
register(sub) metamethod, vraca sub isto # metamethod - fja u metaklasi
sada i ugradjeni tipovi mogu da se zakace za virtual base class
-----
from abc import ABCMeta
class Text(metaclass=ABCMeta):
    pass:

Text.register(str) # sada je Text virtual base od str, i vraca <class 'str'>
issubclass(str, Text) # True
isinstance('some string', Text) # True
----
# as class decorator, sada je Text virtual base od Prose
@Text.register # base
class Prose: # sub
    pass

issubclass(Prose, Text) # True
-------------------------------------------
# 8. combining subclass detection and registration
__subclasshook__() ima precedence nad register()
ako __subclasshook__() vrati True ili False to je final, ako vrati NotImplemented onda moze register()
-----
# primer ovoga sto je ispricao
# ovaj zakacen preko register decorator, ostale subclase preko __subclasshook__()
@Sword.register
class LightSaber:
    def swipe(self):

class Sword(ABC):
    # sada ovaj mora da vrati NotImplemented
    # or je shortcircuit boolean, kao js ||
    @classmethod
    def __subclasshook__(cls, sub):
        return ((hasattr(sub, 'swipe') and callable(sub.swipe)
                 and ... and 
                or NotImplemented)
----
u praksi ne treba previse kombinovati
-------------------------------------------
# 9. the ABC convenience base class
# helper klasa za kreiranje virtual base classes obicnim nasledjivanjem
class ABC(metaclass=ABCMeta):
    pass
-----
# sada umesto:
from abc import ABCMeta
class Sword(metaclass=ABCMeta):
# moze samo
from abc import ABC
class Sword(ABC):
-------------------------------------------
# 10. declaring abstract methods - mora biti impl u subclassi
abstract method - samo dekleracija bez definicije u base klasi, mora biti impl u subclassi
base class sa njim ne moze da se instancira
----
@abstractmethod - anotiran decoratorom
----
from abc import (ABC, abstractmethod)

class AbstractBaseClass(ABC): # metaclass is ABCMeta, mora
    @abstractmethod
    def an_abstract_method(self):
        raise NotImplementedError # Method body syntactically required.
---------
# zapazi, nije isto:
NotImplemented - boolean value za return u predicatima, znaci nije ni True ni False
NotImplementedError - exception, za praznu fju
-------
# primer
# ni base ne moze da se instancira
class Sword(ABC): # mora metaclass ABCMeta
    @abstractmethod
    def swipe(self):
        raise NotImplementedError # mora ovo, ne moze prazno telo fje, sintaksa

# subclass ne moze da se instancira dok ne implem sve abstract methods iz base
# PRAVA nasledjena klasa, zapazi
class BroadSword(Sword):
    def swipe(self):
        print("...")
----
concrete class - suprotno od abstract class # to, a ne nuzno subclass
-----
# virtual subclass moze da se instancira
class SamuraiSword: # nema nasledjivanja
    def swipe(self):
        print("...")
-------------------------------------------
# 11. combining method decorators
@abstractmethod decorator moze da se kombinuje sa 
@staticmethod, @classmethod, getter i setter
@abstractmethod mora biti unutrasnji (donji)
------
class AbstractBaseClass(ABC):
    @staticmethod
    @abstractmethod
    def an_abstact_static_method():
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def an_abstract_class_method(cls):
        raise NotImplementedError

    @property
    @abstractmethod
    def an_abstract_property(self):
        raise NotImplementedError

    @an_abstract_property.setter
    @abstractmethod
        def an_abstract_property(self, value):
            raise NotImplementedError
----
# propagate @abstractmethod u custom descriptorima
mora imati attribut ili property __isabstractmethod__() koji vraca True
kao property za runtime
ovo je abstract descriptor jer __get__()... su abstract 
# cim ima __get__(), __set__(), __delete__() to je descriptor, descriptor protocol

class MyDataDescriptor(ABC):
    @abstractmethod
    def __get__(self, instance, owner):
        # ...
        pass

    @abstractmethod
    def __set__(self, instance, value):
        # ...
        pass

    @abstractmethod
    def __delete__(self, instance):
        # ...
        pass

    @property
    def __isabstractmethod__(self):
        return True # or False if not abstract
----
@abstractmethod na @property postavlja
AbstractBaseClass.my_abstract_property.__isabstractmethod__ na True
-------------------------------------------
# 12. improving invariant with ABCs
class decorator primer iz 2 poglavlja ranije
2 nested class decorators, spoljni nije radio setter, nije bacao exception za +1e34 temperaturu
----
# ovo
@invariant(below_absolute_hot)
@invariant(not_below_absolute_zero)
class Temperature:
-------
# class_decorators.py, fix
# abstract base class
class PropertyDataDescriptor(ABC):

    @abstractmethod
    def __get__(self, instance, owner):
        raise NotImplementedError

    @abstractmethod
    def __set__(self, instance, value):
        raise NotImplementedError

    @abstractmethod
    def __delete__(self, instance):
        raise NotImplementedError

    @property
    @abstractmethod
    def __isabstractmethod__(self):
        raise NotImplementedError

# virtual subclass
PropertyDataDescriptor.register(property) # postavlja abs base class za property na PropertyDataDescriptor

# real subclass
class InvariantCheckingPropertyProxy(PropertyDataDescriptor):

    def __isabstractmethod__(self):
        return self._referent.__isabstractmethod__ # _referent bese original za wrappovani


# ogranicenje za attribute, factory class decorator
def invariant(predicate):
    def invariant_checking_class_decorator(cls):
        # koriguje uslov ovde na virtual base class if isinstance(attr, PropertyDataDescriptor)
        property_names = [name for name, attr in vars(cls).items() if isinstance(attr, PropertyDataDescriptor)]
        for name in property_names:
            _wrap_property_with_invariant_checking_proxy(cls, name, predicate)

        return cls

    return invariant_checking_class_decorator
-----
testirao, sad radi
-------
@invariant nije ugradjeni decorator nego ga on definisao, zapazi
------
ovaj fajl je komplikovan primer sa:
decorators, metaclasses, abstract base classes, descriptors
-------------------------------------------
# 13. summary
issubclass() - delegates to metamethod __subclasscheck__()
isinstance() - delegates to metamethod __instancecheck__()
---
abc module - ABCMeta and ABC classes
__subclasshook__(sub) - ABCMeta ima definisan default, True, NotImplemented
False - disable register subclasses
----
register() - definisi virtualnu subklasu # base klasu zapravo, parent od string npr
-----
@abstractmethod - decorator - spreci instanciranje abstract subklasa, ali samo pravih nasledjenih
virtual subclasses i dalje mogu da se instanciraju
----
moze se kombnovati sa drugim decoratorima ali mora biti innermost, donji
-----
descriptors - propagate abstractness with __isabstractmethod__ attr
metaklase customizuju prevodjenje class definition u class object
-------------------------------------------
-------------------------------------------
## end of python fundamentals, beyond the basics i advanced python
-------------------------------------------
-------------------------------------------
## 18. managing python packages and virtual environments
# 1. course overview - samo pregled
pip
virtual environments
virtualenvwrapper
current developments
-------------------------------------------
## 2. managing python packagegs with pip
# 1. introduction
ne obradjuje objavljivanje paketa
----
package:
1. folder/__init__.py, pa import
2. distribution package - versioned archive file
-------------------------------------------
# 2. getting started
pip - V # pip version and location
-------------------------------------------
# 4. installing pip on mac and linux
ugradjeni python u OS nije za development vec za system scripts and libs
na linux python i pip se instaliraju preko linux package manager
-------------------------------------------
# 5. demo, linux installation
konzola ce ti reci da instaliras pip preko pythona, ali pravi nacin je linux package manager
# debian
sudo apt install python-pip # (python 2.7) znaci da ce install pakete samo za 2.7, install pravu verziju za OS
python3 -V
----
sudo apt install python3-pip # za python 3
pip3 -V # poseban pip ide uz svaku verziju pythona
---
ovo samo na pythonu koji ide uz linux, inace pip ide uz python
-------------------------------------------
# 6. demo, package management with pip
pip: install packages, remove, list, inspect, help, search
-----
uvek install u virtual env, inace sve globalno, kao npm --global, ovde je default
----
pip install requests # install
pip list # list
sve peer dependencies isto instalira globalno # zapazi
pip uninstall requests # remove, ali NE sklanja peer dependencies
---
pip uninstall urlib3 chardet idna # moze vise paketa u jednoj liniji
-------------------------------------------
# 7. demo, getting package information with pip
pip help # za sve pip komande
pip list help # list komanda, list -o - outdated packages
---
pip show six # info o package six
---
pypi.org # python package index, npm.org, cheese shop
-------------------------------------------
# 8. review - rezime
pip install, pip uninstall, pip list, pip show requests, pip help, pip help install
-------------------------------------------
# 9. demo where are packages installed, to
gde instalira pakete, lokacija
python # udje u repl
import sys
sys.path # ovde su sve staze (folderi) na kojima trazi module
----
pip show requests # staza je na Location: ...
----
python3 ne vidi pakete koji su instalirani sa pip (od 2.7), za svaku verziju posebni
pip3 show requests # /home/username/.local/lib/python3.5/site-packages
-------------------------------------------
# 10. demo, a better way to call pip
pip install flask # pyhton version zavisi od OS config, nije jednoznacno svuda
python2.7 -m pip install flask # specify i python version, -m module
python2 -m pip install flask
-----
pip install --upgrade pip # ne nikako, upgrade itself, ali instalira novi pip
ne koristi pip da menjas sistemske pakete
-----
summary
-------------------------------------------
## 3. setting up projects with virtual environments
# 1. about virtual environmennts - trivial
pip - default system wide, global
collision with other projects or system # zapazi
------
virtual environments
isolated per project uvek ovo koristi, nikad global
-------------------------------------------
# 2. creating virtual environment - and explore folders
virtualenv je samo obican module
sudo python -m pip install virtualenv # virtualenv se instalira globalno, mozda treba sudo
virtualenv --version
----
# create dir in home for all virtualenvs
mkdir ~/virtualenvs # odatle pokrece konzolu nadalje
virtualenv rates # creates novi env rates, kreira folder rates u ~/virtualenvs
# folderi: lici na celu instalaciju pythona
bin - dedicated python interpreter and pip for project, activate script
include
lib - ovde su paketi
local
-----
# virtualenv for python3
virtualenv -p python3 rates_py3 # -p option for python interpreter, kreira folder rates_py3
# folderi:
lib
bin - python3, pip, activate
-------------------------------------------
# 3. working inside virtual environment - ok, prakticno
# activate environment
# windows:
rates_py3\Scripts\activate.bat
# linux, mac:
. rates_py3/bin/activate # . - import shell script
(rates_py3) user@comp:~$ # prefix (rates_py3) se pojavi u console prompt
------
# run python and pip
python -V # sada zove lokalni python iz virtualenv
pip -V # isto lokalni
python -m pip list # list installed packages, samo local
-------
# install a package
sada izadje u drugi folder u home ~/demos/m3_venv i tu pip install
i instalirace u virtualenv, vidi se po aktivnom (rates_py3), zapazi
python -m pip install requests # install package
python -m pip show requests # print stazu gde je install requests
/home/user/virtualenvs/rates_py3/python3.5/site-packages # to je staza
------
# deactivate
deactivate # izgubi se (rates_py3) prompt u konzoli
sada je opet globalni python i pip
# provera
python -V
pip -V
python -m pip list
python -m pip show requests
-------------------------------------------
# 4. review
# venv nije obradio, ovde ga pomenuo malo
install virtualenv globally
virtualenv env_name # kreira folder, separate python i pip...
-----
ukljucen uz python 3.3, noviji package venv, ne treba install
python -m venv env_name
-----
# pyenv, zastareo, ne koristi se
pyenv env_name
-----
actvate...
za brisanje environment brises folder, zapazi
-----
lokalni python i pip
--------
# ok, projekti su u jednom home folderu, environments u drugom home folderu
~/projects/my_project
~/virtualenvs/my_env
-------------------------------------------
# 5. managing project requirements - ok
share dependencies
# 2 packages, requests, python-box
python -m pip install requests python-box
------
# export requirements.txt
python -m pip freeze # stampa ih u konzolu, its pip command
python -m pip freeze > requirements.txt # redirect in file
-----
# install requirements.txt u drugi env
python -m pip install -r requirements.txt
-------------------------------------------
# 6. projects, requirements and versions
# ono sto sam i ja zakljucio, project i env folderi su odvojeni
# nije project/node_modules
# ne mora u gitignore
projects
    my_game
    my_library
    my_website
virtual_envs
    my_game
    my_library
    my_website
-----
isti projekat moze da se pokrene sa vise virt envs
1 venv per project
moze i vise venvs per project, test, prod... 
-----
# requirements.txt
docopt == 0.6.1 # must be version 0.6.1
keyring >= 4.1.1 # minimum version 4.1.1
coverage != 3.5 # anything except version 3.5
----
# versions and pip
# install version
python -m pip install flask==0.9
python -m pip install 'Django<2.0' # mora '' zbog <> u konzoli
# update to latest version
python -m pip install -U flask
# update pip itself
python -m pip install -U pip # ne sme sudo da ne gadja sistemski pip
-------------------------------------------
# 7. real world example
clone flask
nema requirements.txt, to ima samo gde kontrolises tvoj environment...
flask predvidjen za distribuciju preko pip, a ne git - lib, a ne app # ok
----
setup.py # ovde su requirements
setup() fja sa keyword args
install_requires=[dependencies ovde]
----
# editable pip install - dev mode - requirements from setup.py - yarn install -dev prakticno
cd .. # u parent folder
python -m pip install -e flask # flask je sad lokalni folder, a ne paket
running setup.py develop for Flask # log poruka da je flask instaliran u dev mode
pip show flask # /home/username/flask - lokalni folder, potvrda dev mode
-------------------------------------------
# 8. another example, testing with tox - github matrix
tox.ini fajl
tox - paket za testiranje koda sa razl python verzijama u razl virt envs
----
python -m pip install tox # install u virtual env
# tox.ini:
run unit tests in multiple python versions in separate virt envs
ima i test dependencies
----
tox # command u project folderu, kreira multiple venvs, kao CI yaml matrix
-------------------------------------------
## 4. using virtualenvwrapper to make your life easier
# 1. intro and installation
unofficial tool for venvs
---
user-friendly wrapper around virtualenv
easy creation and activation
bind projects to virtualenvs
for large numbers of projects
----
# install globally
sudo pip install virtualenvwrapper
# configure it - loaduje ga na OS startup prakticno, ok
which virtualenvwrapper.sh # find where is installed, linux
/usr/loval/bin/virtualenvwrapper.sh # ovde je
----
nano ~/.profile
source /usr/local/bin/virtualenvwrapper.sh # load on OS start
# export env vars obicne, zapazi
export WORKON_HOME="/home/username/virtualenvs" # ovde postavlja custom stazu za envs, da nije default ~/.virtualenvs
export PROJECT_HOME="/home/username/projects" # projects folder
logout # logout/login again to restart shell
-----
~/.virtualenvs # default folder u kome su skripte
--------
koci ovaj klip
-------------------------------------------
# 2. working with virtualenvwrapper - utility program, nista spec ni vazno
# isti koraci kao za obican virtualenv
activating a project
switching projects
creating projects
removing projects
linking projects with virtualenvs # folderi
-----
# list existing venvs
workon
# activate
workon rates_py3
# switch to another venv, isto
workon flask
# create new project, and activate, and binds proj i env folders
mkproject new_project # kreira i project folder i venv folder sa tim imenom
mkproject -p python3 new_project # spec python version, kao virtualenv, taj python mora da postoji vec na OS
# bind folders, u project folderu ovo, i active
setvirtualenvproject
bind - kad workon project_1 da zameni i proj i venv foldere, nebito toliko
# deactivate, isto
deactivate
-------------------------------------------
# 3. review - malo nove komande, malo rezime
virtualenvwrapper-win # za windows
# create virtualenv
mkvirtualenv some_env
# remove venv
rmvirtualenv some_env
# temp env bez imena, single use
mktmpenv
-----
bezveze helper tool ovo verovatno
-------------------------------------------
## 5. choosing right tools
# 1. introducing pipenv and poetry
pip, virtualenv - standard tools
pypa.io - working group for packaging
-----
poenta: u pythonu nema standardan nacin za dependencies nego haos
-----
# installation:
pip - wheels, eggs - formats (file format verovatno)
easy_install - ne koristi se
----
# development:
# dependency management:
requirements.txt
# project isolation - local deps:
virtualenv
venv
# new tools:
pipenv
poetry
-----
# anaconda - data science
poseban: pyhton, package manager, conda (venv), linux packages
----
# requirements
# pip
requirements.txt
not deterministic - svaki install drugaciji, redosled itd, za 2 iste peer a razl version
----
# pipenv - tool
pipfile - fajl, custom format
deterministic
----
# poetry
pyproject.toml - standard format pep-518
deterministic
-------------------------------------------
# 2. pipenv - lici na yarn, npm
# install - pip, globally
sudo pip install pipenv
# install packages
pipenv install requests python-box
1. creates venv - /home/username/.local/share/virtualenvs/my_proj_folder-hash
2. install deps in venv
3. creates Pipfile i Pipfile.lock
----
# Pipfile
[[source]] # online repository, pypi.org
[dev-packages] # dev-dependencies
[packages] # dependencies
[requires] # python version
----
# run script in venv, npm run
pipenv run python my_file.py
# activate
pipenv shell # isto izadje (my_proj)
python my_file.py # run file
# deactivate
exit
# switch python version
pipenv install --three # destroys venv and creates new with py3, and installs deps
mora da se poklapa sa [requires] u Pipfile
-----
kombinuje pip, virtualenv i virtualenvwrapper
-------------------------------------------
# 3. poetry
# install, https://python-poetry.org/docs/
curl -sSL https://install.python-poetry.org | python3 -
# reload it in shell
source $HOME/.poetry/env
-----
# create new project
poetry new currencies # kreira currencies folder, to je ime projekta
README.rst currencies pyproject.toml tests
# install packages
poetry add requests python-box
# run file
poetry run python currencies/my_file.py
# activate env
poetry shell
# run package currencies with __init__.py file
python # start repl
from currencies import my_file # jeste file, ne fn
# deactivate
exit
-----
# pyproject.toml
[tool.poetry] # project info
[tool.poetry.dependencies]
[tool.poetry.dev-dependencies]
[build-system] # za package
# poetry.lock
cim ima lock file - deterministic
# switch python version
pyenv, nije jednostavno
-------------------------------------------
# 4. conclusion
# standard
pip, virtualenv
# novo
pipenv # popularniji
poetry # supports packaging
# data science
anaconda
----
kraj virtualenv kursa
-------------------------------------------
-------------------------------------------
### python best practices for code quality
## 1. course overview
format code - pep8
docstrings
typings
-------------------------------------------
## 2. following python style guidelines pep8 and pylint
# 1. introduction, what is pep
pep8 - format
pep - rfc, design document
# pep index
https://peps.python.org/pep-0000/
-------------------------------------------
# 2. overview of pep8
formatting code and comments
whitespace, quotes, operators
naming - classes, functions, variables
-------------------------------------------
# 3. demo, applying pep8 rules too your code
https://pep8.org
formatting rules
----
indentation - spaces, a ne tabs
-------------------------------------------
# 4. review pep8
4 spaces, 79 chars
2 newlines between top level functions and classes
1 newline between methods
----
# imports, 1 newline separation
standard library
third party libs
local modules
-----
# naming
modules - short, lowercase names
classes - CapitalizedNaming
functions - lowercase_with_underscores
constants - ALL_CAPS
_private - non-public names start with underscore
-----
# docstrings
# za sve public:
modules
functions
classes
methods
-------------------------------------------
# 5. demo, commandline tools, pylint 
pylint - pep8 + codesmell
pycodestyle - pep8
black - fix problems automatically, CI
-----
codestyle = pep8
-----
# pylint
# install, locally in venv
pip install pylint
# check lint
pylint my_package
# disable line
# pylint: disable=invalid-name
# pylint config, rcfile
pylint --generate-rcfile > pylintrc
good-names=some_name,... # disable isti rule za liniju gore
-------------------------------------------
# 6. demo, commandline tools, pycodestyle and black - trivial klip
# install pycodestyle, in venv, locally opet
pip install pycodestyle
# run lint - samo pep8, manje warninga
pycodestyle package_name
------
# install black, obican paket, kao eslint
pip install black
# run lint, popravio greske automatski
black module_name
-------------------------------------------
# 7. module summary - trivial, 30 sec
pep8
lint - editor, console tools
-------------------------------------------
## 3. documenting your project
# 1. introduction, docstrings and sphinx
sphinx - generate html from docstrings
pep257 - za docstrings
------
# docstrings
docstrings - string as first statement of a module, function, method or class
parsed into __doc__ attribute objekta modula, fje, meth, or class
----
# singleline primer
def function(a, b):
    """Do X and return a list."""
------
"doublequotes zbog escape special chars"
gramaticki korektne recenice, veliko slovo i tacka jer ce biti dokumentacija u html
introspection moze da vidi args fje, a li ne i return value, kao tipovi
zato u docstring ide return value
----
# mutline primer
def complex(real=0.0, imag=0.0):
    """Form a complex number.

    Keyword arguments:
    real -- the real part (default 0.0)
    imag -- the imaginary part (default 0.0)
    """
    if imag == 0.0 and real == 0.0:
        return complex_zero
---
3 doublequotes cuva newline pa je deo dokumentacije
-------------------------------------------
# 2. demo, getting started with sphinx - .rst to html
# .rst to html converter, .rst je .md alternative
standard za documentation generation u html, pdf...
-----
# installation, local in venv, obican paket
pip install sphinx
# run
mkdir docs # create folder
sphinx-quickstart
index.rst # ovde md text - reStructuredText, novi markup
# generate html u build folder
make html
-------------------------------------------
# 3. demo, introducing reStructuredText - markup language, kao md, lako
header ------, =====, ~~~~~, +++++
codeblock ::, newline i identation
list -, +, *
link `Link text <http://example.com/>`_ # mora _ na kraju, moze i samo go link
komentar .. i indentation
-------------------------------------------
# 4. view, sphinx and reStructuredText
ponovio prethodne lekcije samo
-------------------------------------------
# 5. demo, python project with docstrings
apidoc - sphinx extension that extracts docs from python code
sphinx je samo za go file.rst
----
# poenta: ide .rst sintaksa u docstrings i linkovi na fje
:meth: `klasa.method` # link na fju
:param arg1: opis # za args
:return: opis # za return value
u __init__.py ide docstring za ceo paket
-------------------------------------------
# 6. demo, using apidoc to generate documentation from python code
# docstrings -> .rst -> html
cd .. # u root projekta, ne docs folderr
# generise ciste file.rst od python source
sphinx-apidoc -o docs package_name/
# podesi conf.py, enable extensions, autodoc...
# create html
cd docs # ide u docs folder opet
make clean html # recreate html
html ima linkove ka python source
# publish docs
readthedocs.org
sphinx github pages, static docs kao jekyl
-------------------------------------------
# 7. review apidocs
obnovio apidoc, i nove informacije kako linkovi ka source
# generate html in a single step, --full fag
sphinx-apidoc --full -o docs mypackage
-------------------------------------------
## 4. improve your code with type checking
# 1. introduction, static typing and type hints - trivial
static typing - java, C#, type checking compile time
dynamic typing - python - type checking runtime
-------------------------------------------
# 2. demo, type hints
# avg.py, primer, trivial
# slicno kao typescript za js
def average(a: int, b: int, c: int) -> float:
    return (a + b + c) / 3
print(average(1, 5, 7))
---
-> return type, zapazi
-------------------------------------------
# 3. review, type hints
bas kao ts, js
----
type hints:
optionally add type information
ignored by Python interpreter # to, zapazi
running gives the same results
-----
type checker: mypy (or Pycharm - editor), static typing
-----
python 3.6+
# variable hints
age: int = 1
-----
# prednosti tipova, namena:
find errors compile time
maintenance - documentation, intelisense
-----
poenta: opcionalno, moze samo u delu projekta, uglavnom se ne koristi
-------------------------------------------
# 4. demo, adding type hints to our project
mypy - typescript
# sintaksa za tipove ovde
# https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html
# npr.
x: list[int] = [1]
x: set[int] = {6, 7}

x: List[int] = [1]
x: Set[int] = {6, 7}

x: dict[str, float] = {"field": 2.0}  # Python 3.9+
x: Dict[str, float] = {"field": 2.0}
-----
# player.py, weapons.py - tipovane, game.py bez tipova, ti treba da dodas 
# tipovi se importuju, isto kao moduli, isto kao typescript
from typing import Tuple
# i obicna klasa je tip
from gamedemo.weapons import Weapon
-----
poenta: tipovao weapons.py klasu sa mypy 
-------------------------------------------
# 5. demo, mypy
mypy - command line type checker, kao tsc
do sad se oslanjao na Pycharm editor
-----
# install mypy, in venv
pip install mypy
# run
mypy my_package/
-----
type checker, kao typescript, ne moze da se dodeli int u str npr, ok
ispise greske u konzolu
-------------------------------------------
# 6. summary
# sta je bilo u poglavlju
gentle intro into type hints
adding simple type hints
----
static type checking with mypy/Pycharm
----
# namena
prevent errors
improve maintainability
-------------------------------------------
-------------------------------------------


