pyenv - multiple python versions on ubuntu
isto kao nvm
--------
# zapravo ovo mnogo jednostavnije, install pyenv, docs # ovako treba
https://github.com/pyenv/pyenv
https://github.com/pyenv/pyenv-installer
---
curl https://pyenv.run | bash
------
# How to Install and Run Multiple Python Versions on Ubuntu/Debian | pyenv & virtualenv Setup Tutorial - k0nze
# install pyenv ubuntu - bezveze zapravo
https://k0nze.dev/posts/install-pyenv-venv-vscode/#linux-debianubuntu
# 1. install dependencies ubuntu, mnoge vec ima
sudo apt install -y make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python3-openssl \
git
---
# 2. clone u ~/.pyenv folder, tu ce biti sve
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
---
# 3. add pyenv to PATH i start in every terminal, bash console
# dodaj rucno sa razmak
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc && \
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc && \
echo 'eval "$(pyenv init --path)"' >> ~/.bashrc
# restart terminal session
--------
# using pyenv
# list available versions, zapazi install
pyenv install -l
# install desired python version
pyenv install 3.12.2
# remove
pyenv uninstall 3.12.5
# list installed versions on system (shows path too)
# koji je system python zapravo? system jeste OS zapravo redirektovan?
pyenv versions
# set global python version
pyenv global 3.12.5
# za povratak na sistemski
pyenv global system
# print global python pyenv
pyenv global
# git pull mora za noviji python
# git pull iz bilo kog foldera
git --git-dir=/home/username/.pyenv/.git --work-tree=/home/username/.pyenv pull
-----------
mora prvo da instaliras drugi python pa njega da switch na global inace ostane warning za stari 
ne smes da uninstall python version dok je selektovan kao global 
# onda mora da obrises ovaj fajl da maknes warning 
rm /home/username/.pyenv/version
-----------
system je OS python, global je globalni # eto
# set python version for local folder
# dodaje .python-version fajl sa 1 linijom
pyenv local 3.12.2
# print python version
which python
# confirm that pip is from ~/.pyenv
which pip
---
sa pyenv moze i python3 i python command # ok
-----
# i dalje ide venv, jer razliciti projekti koriste razlicite verzije paketa
# init venv in local .venv folder
python -m venv .venv
# activate
source .venv/bin/activate
-----
# activate venv on each run, debug in vs code
# .vscode/settings.json
"python.terminal.activateEnvironment": true
-----
# u vs code trebas da selektujes binary za python verziju koju koristis, ok
# zapazi ima versions segment u stazi
/home/username/.pyenv/versions/3.12.2/bin
-------------------
# install cookiecutter
# pise u docs kako i gde se instalira
https://pypi.org/project/cookiecutter/
# znaci cookiecutter instaliras kao global_ za python u pyenv, nema potrebe u venv
# 1. install pipx (kao npx)
pip install pipx
# 2. install cookiecutter
pipx install cookiecutter
# 3. install project with cookiecutter
pipx run cookiecutter https://github.com/mongodb-labs/full-stack-fastapi-mongodb --no-input project_name="my-project-folder"
-----------------------
# pyenv chatGpt
# update pyenv, git repozitorijum, mora za noviji python, zapazi
cd $(pyenv root)
git pull
---
reload shell, reopen terminal ili exec $SHELL
----------
# list sve verzije, cpython, pypy, anaconda
pyenv install --list
# list samo cpython, taj mi treba, filter regex
pyenv install --list | grep -E '^\s*[0-9]+\.[0-9]+\.[0-9]+'
# jednostavniji regex
pyenv install -l | grep '^  [0-9]'
-------
cpython - standardni python
pypy python - jit python, brzi, nije 100% kompatibilan
anaconda python - dolaze ml, data science biblioteke uz njega, numpy, pandas, scikit...
-----------
# list all pythons in path, pyenv and os
type -a python3
python3 is /home/username/.pyenv/shims/python3
python3 is /usr/bin/python3
python3 is /bin/python3
-----------
# deactivate any pyenv python for the current shell session
pyenv shell --unset
-----------
1. Global (pyenv global) - za sve foldere, nije OS python, nego i dalje pyenv python, taj mi treba
pyenv global 3.12.2
2. Local (pyenv local) - za folder, ima prioritet
pyenv local 3.12.2 - komanda
.python-version - ili fajl 
3. Shell (pyenv shell) - current terminal session, najvisi prioritet
pyenv shell 3.12.2
------
# listaj lokalne verzije
pyenv versions
  system
* 3.12.2 (set by /home/username/.pyenv/version)
