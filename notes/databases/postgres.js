// postgres full text search, kao elasticsearch
// 
//  PostgreSQL Full Text Search Tutorial - Redhwan Nacef
// https://www.youtube.com/watch?v=4c0vjXR9Ip4
preprocess fje, to_tsvector(), izbaci nebitne reci, veznike, cuva rec i pozicije, tip i fja, prima and or operatore
tsvector() na kolonu
lexem - root rec bez padeza, itd
websearch_to_tsquery() - prirodni jezik za operatore
@@ - match operator, primeni query na tsvector, vraca boolean
tsvector @@ tsquery
----
// sql query sintaksa
WHERE 
  body_search @@ to_tsquery('PostgreSQL');
-----
ts_rank() kreira tezinu 0-1, sortiras opadajuce
// index, ili 
// on fly 
gin index fja za fts
// ili sacivas index kao kolonu u tabeli
--------------
// https://dev.to/nightbird07/full-text-search-in-postgresql-a-comprehensive-guide-3kcn
index je na tsvector koloni, ona se brzo pretrazuje // tacno
CREATE INDEX articles_search_vector_idx ON articles USING gin(search_vector); // evo
store pre-processed text data
na koloni koju pretrazujes zapravo (posredno preko ts_vector())
// https://www.postgresqltutorial.com/postgresql-indexes/postgresql-full-text-search/
tsvector je zapravo index nad oriinalnom kolonom
-------------
i ts_vector() i to_tsquery() fje primaju jezik kao prvi arg 
imaju recnik jezika za lexeme, normalization on root form, stemmer
--------------------
// https://www.postgresqltutorial.com/postgresql-indexes/postgresql-full-text-search/
// highlighting
PostgreSQL can generate snippets or summaries of documents 
containing the matching words from the search query
vraca ceo isecak, sa <b>recima</b>
------------
postgres document - spojeni tekst (stringovi), iz jedne ili vise kolona u istoj ili odvojenim tablama
samo spojeni sa || '' ||, npr. name_column || '' || description_column
------------
// ts_vector sortiran alphabetically, samo pozicija reci u tekstu i root rec
'brown':3 'dog':9 'fox':4 'jump':5 'lazi':8 'quick':2
----------
// tsquery() operatori
Boolean operators AND (&), OR (|), and NOT (!)
Phrase search (“”): Double quotes (“”) - reci unutar po redosledu
Prefix search (:) : A colon (:) - match prefix
Negation (-) - bez te reci 
Grouping ()
-------------
i ts_vector i ts_query su fje koje vracaju iste svoje tipove
-------------------
// ne koristis ts_vector direktno nego index
CREATE INDEX body_fts
ON posts
USING GIN ((to_tsvector('english',body)));
// index je nad body kolonom
SELECT 
  id, 
  body 
FROM 
  posts 
WHERE 
  body @@ to_tsquery('basic | advanced');
-------------
https://youtu.be/k95cE5nsMu8?t=275
za index u posebnu kolonu moras za postojeci data da regenerises
generated always as -  je trigger ispod haube
ako je gin(ts_vector(my_column)) sporo onda mora ovako, 10k x brze
execution plan time and execution time // zapazi
----------------
cuvas tsvector kao kolonu ili ne 
-----------------
migrations are transactional so thats one way to decide — is it better if the changes are transactional or not
and i think yes => 1 tx => 1 migration
-----------
// postgres
The COALESCE function_ returns the first non-null value from the list of its arguments
kao default value 
----------
kad god imas foreign keys treba i cascade on update and delete
-----------------
// truncate table, delte data, leave schema
DO $$ DECLARE
  r RECORD;
BEGIN
  FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
    EXECUTE 'TRUNCATE TABLE ' || quote_ident(r.tablename) || ' CASCADE';
  END LOOP;
END $$;
--------------
npx dbmate up
// this will rollback only the last migration in database, ne sa gita
// sa gita su mozda stigle jos neke, zapazi
npx dbmate rollback
-----------
posle git pull ide i npm install i apply migrations // zapazi
------------------
// postgres docker and adminer
// https://github.com/nemanjam/nextjs-prisma-boilerplate/blob/main/docs/postgres.md
-----
// ovo je vazno, uvek zapamti, poenta:
containers access other containers with service name (on same network, internal or external)
from host access containers via `localhost`
// zasto adminer ne moze da se poveze na remote database, vazno inace ne radi, zapazi
adminer-dev conatiner must be on external network external-host
// adminer custom port
access on `http://localhost:8080/`
set e.g. `localhost:5433` in the server text input field (html form)
// run postgres volume bind mount as non root user for git
// to set user must use postgres:bullseye, and NOT postgres:alpine
user: '${MY_UID}:${MY_GID}' 


