/**
* SQL working notes. Autumn 2014.
*
*
*
*/


CREATE TABLE [dbo].[UserRoles](

 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (/*opcije za konkurentnost*/ PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

/*
WITH se odnosi na create table sa kojim opcijama
[dbo] je shema. ispod baze
[kljucna rec] je escape za kljucne reci


*/

	--komentar
	/*komentar*/
	
	--GO izvrsava prethodnu naredbu?
	--da, ili batch od vise naredbi (od pocetka skripte ili od prethodnog GO)

dump shemu - desni klik na bazu, tasks, generate scripts
-------------
strani kljuc samo kaze da to u referenciranoj tabeli mora da postoji

A references B, u B mora da postoji
vezna tabela references original
-----
strani kljuc u drugoj relaciji moze imati jedinstvenu vr ili null - ogranicenje stranog kljuca, znaci nema duplih
unique - kljuc kandidat

torka referencira torku

strani kljuc se definise u owned relaciji, u originalu ne
-------------
mysql TIMESTAMP je univerzalni trenutak u vremenu po utc, a DATETIME je datum kao datum, dakle za tw timestamp

The main difference is that DATETIME is constant while TIMESTAMP is affected by the time_zone setting.

--------------
kljucevi u glavnoj tableli ka veznoj oneToMany - da ne moze ni jedan user da postoji koji nije rasporedjen

addUserIntoRole radi insert na veznoj
---------------

select * from radnik where radnik_id IN 				//treba operator koji radi nad poljem a ne skalarom kao =
	select radnik_id from radnikSektor where sektor_id = 123//in zamenjuje vise = i or

select radnik from radnik, radnikSektor
	where r.radnik_id = sr.radnik_id and sektor_id = 123

SELECT * FROM TableA NATURAL JOIN TableB	
----------------
cross join svaki sa svakim, ne vodi racuna o kljucevima, nije svestan kljuceva
full outer join on x=y uvek ima uslov, za nepoklapanja ubacuje null, matchuje po kljucevima, nisu isti cross i full outer
inner join on, left outer join on
natural join radi i projekciju kolona, nema duplih kolona kao inner, i nema on nego podrazumeva nad kljucevima, u osnovi isti spoj
strani kljuc u sql je ogranicenje a ne referenca

Ogranicenja stranog kljuca - CASCADE JE OGRANICENJE STRANOG KLUCA
FOREIGN KEY (<lista_tributa>)REFERENCES <ime_tabele>[(<lista_atributa>)]
[ON DELETE { NO ACTION | CASCADE|SET DEFAULT | SET NULL}]
[ON UPDATE { NO ACTION | CASCADE | SET DEFAULT | SET NULL}]
DROP TABLE <ime_tabele> [RESTRICT | CASCADE] //restrict brani brisanje vraca exception
------------------
//leni
Equijoin (ekvi spoj) je spoj gde je uslov spoja oblika (operator jednakosti =)
Ai = Bj
Natural join (prirodni spoj) je ekvi spoj gde je iz rezultata
isključen jedan od dva jednaka atributa (Ai ili Bj)

Takodje se oznacava sa beskonacno, bez uslova
Uslov je da oba atributa u uslovu spoja imaju isto ime

Zasto spoljašnji spoj?
Da bi se dobila unija torki u slucaju kada relacije nisu
unijski kompatibilne
--------------
jedinicni komplement, oduzimas od cifre najv tezine, u bin obrnes 0 i 123
dvojcin komplem jedinicni + 1

nemas specijalnu cifru koja kodira znak
smanjis opseg do pola, do 500, 0 nije jednoznacna 0-500 pozitivni, 500-999 negativni, transliras opseg
direktno radis sa brojevima bez specijalnog bita

neoznacen tip povecan opseg
-----------------
com lib za vise jezika, acitvex isto za browser
-----------------










