
// login
mysql -u username -p
mysql -u root -p
----
// list all users
SELECT user, host FROM mysql.user;
----
// list databases
SHOW DATABASES;
----
// list permissions
SHOW GRANTS FOR 'username'@'localhost';
SHOW GRANTS FOR 'root'@'localhost';
----
// grant all privilegies
GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost' IDENTIFIED BY 'password';
----
// create databse
mysql -u your_username -p -e "CREATE DATABASE IF NOT EXISTS your_database_name;"
-------------------
// docker-compose.yml
// init.sql - create user, grant all privileges for all IP addresses
CREATE USER 'user'@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'user'@'%' WITH GRANT OPTION;
----
'%' je fora za sve IP adrese // vazno inace user ima privilegiju samo nad jednom adresom
-----
// ovako mountujes kroz volume
services:
  mysql-dev:
    image: mysql:8.3.0
    container_name: mysql-dev
    restart: unless-stopped
    environment:
      - MYSQL_ROOT_PASSWORD=password
    ports:
      - '3306:3306'
    volumes:
      - ./db-data:/var/lib/mysql // fix docker root user chown
      - ./init.sql:/docker-entrypoint-initdb.d/init.sql // tu
    networks:
      - internal-dev
-----------
// bind mysql to 0.0.0.0 - zapravo ovo radi po default
MYSQL_ROOT_HOST='%'
// ili
command: --bind-address=0.0.0.0
--------------
localhost - problem je DO ADMINERA, on ne zna za localhost, a 127.0.0.1 vodi na njegov container
----------------
// ping mysql from ubuntu terminal
telnet localhost 3306
mysql -u username -p -h localhost
-----
\q je izlaz iz mysql prompt 
--------------------
// trigger
trigger u mysql je event, on insert, select, update...
// stored procedure
stored procedure je funkcija, ulazni args
poziva se na event - trigger, kao handler
