﻿
/**
 * databases course slides notes
 * 
 */

//6. analiza seme
dekompozicijom se relacije razbijaju do nivoa funkcionalnih zavisnosti, inace fz su na manjem nivou od relacije
svaka fz se izoluje u posebnu tabelu
fz - relacije kljuceva, ko koga odredjuje, ko je kome kljuc

anomalije azuriranja - unosa, brisanja, modifikacije
dekompozicija - Dekompozicija je dobra ako je skup šema relacija D ekvivalentan šemi R i ako su isključene anomalije 
ocuvanje atributa, spoj bez gubitaka (informacije), ocuvanje zavisnosti 


// 7. normalizacija
1. normalna forma - sve kolone atomicne, znaci nije vise rowova stavljeno u 1 row, postoji row i on je jedan, uslov za relacioni model, za obj-rel ne
2. norm forma - nema parcijalno zvisnih kolona od (kompozitnog) kljuca, ne zavisi od dela kljuca, ili potpuno odredj ili nimalo, dekompoz u novu tabelu gde je pk taj deo
eliminise vertikalnu redundansu
3. norm forma - nema tranzitiv zavisnosti, sve kolone DIREKTNO zavise od pk, a ne posredno, dekompozicija, nema redundanse
4. Boyce-Codd-ova normalna forma (BCNF) - 3nf + izabrani pk (i svi kandidati) je ujedno i super kljuc, ne postoji veci super kljuc, 
nema redundanse, anomalija modifikacije i brisanja, unosa ima ako ne dodelis pk not null (trivijal), kandidat = unique - svi moguci pk-ovi

denormalizacija - prvo dekompozicija, pa eventualno denormalizacija sa jasnim ciljem , ustedom

------------------------
// What is Relational Algebra? Relational Algebra Queries | Relational Calculus
// MechanicaLEi
// https://www.youtube.com/watch?v=HHC3x3W2icE
relation algebra - input relations, output relation
---------
6 operations:
selection, projection, union, interssection, set difference, cartesian product, rename
--------------------------
// https://www.youtube.com/watch?v=rcVXazo7TDE&list=PL5EKcCIY-_B8j5K4IytCpX5bFNGQqfTqj&index=1
// 1. Introduction to Relational Algebra | Database Management System
ulaz relacija i query language - izlaz relacija (relacije medjuproizvodi)
operacije
1. set operation (binary): union, interssection, set difference, cartesian product, rename
2. relation algebra operations: selection, projection, joins
--------------
// 2. Select Operation - trivial
select - row = tuple (eto), torka from relation
sigma - select
condition -theta
r - relacija (tabela)
----
vraca relaciju sa istom shemom (kolonama)
komutativna, nebitan redosled r1, r2
------------
// 3. Project Operation
projection - pi- selektuje kolonu, vertikalna selekcija
uslov - A (ime kolone)
---
vraca set (skup bez ponavljanja)
komutativna isto
-------------
// 4. Illustration on Project
strani kljuc moze da uzme samo postojece vrednosti orig kolone u originalnoj tabeli
treba da moze i null...
--------------
// 5. Union Operation
preduslov union compatibility
1. isti broj kolona
2. ith kolone r1 i r2 same domain...?
---
domain je smisao, znacenje kolone
npr id i studentId isti domen, razlicito ime
----
union - row postoji u r1, r2 ili u oba, logicno unija, trivijal
rezultat nema ponavljanja
-----------------
// 6. Intersection - trivial
rows (tuples) koji su u r1 i r2 - sve kolone ili manje
tuples - sve kolone ili manje do jedna
------------------
// 7. Cross Product
x - oznaka
dekartov proizvod, svaki sa svakim, sve kombinacije
obicno ide selekcija iza
to je kad navedes vise tabela posle FROM
pa where  key1=key2 and praviUslov // onaj losiji nacin
----
// npr student[ime, predmetId], predmet[predmetId, ocena] 
imena studenata koji imaju ocenu 5
PI(name)[sigma(ocena=5)(student x predmet)]
------------------
// 8. Rename Operation
ro - novo ime za result relaciju
moze rename relaciju ili samo kolonu
AS
-----------------
cross join = cartesian product = from r1 r2 = dekartov = svaki sa svakim // ok
inner -> theta bilo koji uslov, equi ===, natural equi sa istim imenima kolona
------------------
// 9. Introduction to Join
spaja dve relacije - tabele // DVE RELACIJE zapazi
ekviv - cross product followed by selection
uslov spoja
----
1. inner join - result samo matching rows
    1. theta join
    2. equi join
    3. natural join
2. outer join - result sve iz r1, sve iz r2, ili sve iz obe
    1. left outer
    2. right outer
    3. full outer
--------------------
// 10. Inner Join
theta - znak je beskonacno theta - theta je uslov spoja
uslov spoja se odnosi na kljuceve // to to
----
// primer
odeljenje[id, name, radnikId]
radnik[id, odeljenjeId, ime, telefon]
uslov spoja je pk1=fk1 and pk2=fk2
id=odeljenjeId and id=radnikId
ako je neki fk=null ceo row izostavljen iz result
------
equi join - uslov spoja je jednakost (=) // trivial (u theta moze bilo sta)
------
natural join spaja kolone koje imaju isto ime (id=id npr) po defaultu
i u result samo jedna kolona (id)
---------------------
// 11. Outer Join - pitanje kad koji treba?
r1 + result equi spoja prakticno + nulls
r2 + result
r1 + r2 + result
---
// primer levi, result
kolone su unija osim kljuceva spoja
leva tabela, desna tabela po fk, plus null za missing match // to
---
kako to se slaze sa slikama, samo r1 npr?
--------------------
// 12. Division Operation - kad je ovo korisno?
kolona se odnosi na SVE kolone
result = kolone r1 kojih nema u r2
elem u r1 ima match u r1 za svaki fk u r2
--------------------
// 13. Illustration on Division Operation
primer, ok ali nebitno za sql
podtabela tabele r2 sa smislim, jedna kolona
pa podeli r1 sa podtabelom
--------------------
// 14. Illustration | What will be the total number of conflicts serializable schedules?
bezveze, nepotrebno
--------------------------------
--------------------------------
// SQL Joins Explained |¦| Joins in SQL |¦| SQL Tutorial
// Socratica
// https://www.youtube.com/watch?v=nWyyDHhTxYU&list=PLi01XoE8jYojRqM4qGBF1U90Ee1Ecb5tt
spoj - kad ti trebaju kombinovani podaci iz dve tabele
spoj je operacija mnozenja rowova leve i desne tabele
tacno mora 2 loops + filter fk=pk
----
// poenta spoja da dobijes vecu tabelu koja ima sve sto ti treba
spajas rowove koji fk=pk u veci result row - result table // jednostavno nalepis row1 na row2
onda iz result table select and project sta ti treba
ili spajanje na fk
---
// ovo mi treba na intervjuu, left_table je glavna tabela, trivial zapravo
SELECT * // projekcija na kraju resultujuce table
FROM left_table
INNER JOIN right_table
ON left_table.fk = right_table.pk // uslov spoja, razl where...?
-----
INNER - kako da handleuje left_table.null i/ili right_table.null
ON - uslov samo za spoj
WHERE - dodatni uslovi (row filteri) nevezani za spoj
-----
// inner
INNER - postoji match u obe tabele
// outer
// drugacija interpretacija:
// svi rows iz leve i kad nema match u desnoj
// onda sve kol u desnoj su postavljene na null
LEFT - matchovani rows + right_table.null - null u svim kolonama right_table
RIGHT - matchovani rows + left_table.null
FULL - (full outer join) - decart sa null gde nema match
---
// decart = cross
// veneovi dijag - slike skupovi, nista ne pokazije zapravo
// problem broj rows result table
--------
sqlite i mysql ne podrzavaju full?
-----
// result_table
// note: ovo nije tacno uglavnom, razmisljanje samo
right_table ima sve moguce vrednosti jer je pk
// LEFT
n_rows(result_table) = n_rows(left_table.fk != null)
// INNER
n_rows(left_table.fk != null && right_table.pk in left_table.fk)
// FULL, m x n
n_rows(left_table.fk != null) + n_rows(right_table.pk not in left_table.fk)
---------
// broj rowova u result spojeva
// https://www.reddit.com/r/SQL/comments/wta8rh/joins_number_of_rows_in_the_result_and_use_cases/
// zavisi da li su 1:1, 1:n ili m:n relacije
// 1:n i m:n imaju vise matching rowova jer je 1 ili 2 petlje
// za 1:1
TableA INNER JOIN TableB = m records
TableA LEFT OUTER JOIN TableB = a+m records
TableA RIGHT OUTER JOIN TableB = b+m records
TableA FULL OUTER JOIN TableB = a+b+m records
TableA CROSS JOIN TableB = (a+m)*(b+m) records
-----------
// meni treba uvek INNER u principu
n_rows(result_table) je m x n - nesto za LEFT, RIGHT, INNER
// kad koji? -  ako ti trebaju i sa null iz r1 ili r2, logicno
------------------------
------------------------
// SQL Joins Examples
// LEFT, RIGHT, FULL kad hoces da ukljucis null iz leve, desne ili obe, logicno, zavisi od smisla
// u principu ti imas sve podatke iz jedne velike ekviv result table, gde su podaci integritetni
// ali je zgodnije da bude u vise malih tabela
// postgres sintaksa ovde
ON je umesto WHERE za fk=pk i to je to
AS radi i na tabeli i na koloni
SELF JOIN - moze tabela sama sa sobom, opusteno, ako ima fk na svoj pk
----
// left join na samom sebi
// da vidis gazde marsovcima
SELECT m.first_name AS fn, m.last_name AS ln,
       s.first_name AS super_fn, s.last_name AS super_ln
FROM martian AS m
LEFT JOIN martian AS s
ON m.super_id = s.martian_id
ORDER BY m.martian_id
--------
// subquery, slozen upit
SELECT s.supply_id, i.quantity, s.name, s.description
FROM (SELECT * FROM inventory WHERE base_id = 1) AS i
RIGHT JOIN supply AS s
ON i.supply_id = s.supply_id
ORDER BY s.supply_id
----------
// kad pises upit project kolona radis na kraju
// kad poredis u WHERE znas da FULL postavlja na null
SELECT v.first_name AS visitor_fn, v.last_name AS visitor_ln,
       m.first_name AS martian_fn, m.last_name AS martian_ln
FROM visitor AS v
FULL JOIN martian AS m
ON v.host_id = m.martian_id
WHERE m.martian_id IS NULL OR v.visitor_id IS NULL;
---------------------------
---------------------------
// SQL Index - trivial
index - sadrzaj knjige
// 4 min da kreira index na 100M
CREATE INDEX index_name_idx
ON table_name(column_1_name);
---
ubrzava trazenje SAMO po column_1_name, moze i dodatne kolone uz nju, redosled navodjenja nebitan 
po ostalim kolonama brzina nepromenjena
---------
// multicolumn index
CREATE INDEX index_name_idx
ON table_name(column_1_name, column_2_name); // redosled bitan, primary, secondary sort
// ubrza zesce za query sa oba
-----
index usporava upis i zauzima mesto
--------------------------
--------------------------
// Data Normalization vs Denormalization - IT k Funde
// https://www.youtube.com/watch?v=W_5vn8TBLys
normalization:
more tables, more joins, less redundancy, less duplication
slow reads (zbog spojeva), fast writes
----
denormalization:
one big table, redundancy, duplication
fast reads (no joins), slow writes
-----------------------
-----------------------
// 1NF 2NF 3NF DBMS - Saghir School
// https://www.youtube.com/watch?v=xPzgK6sOCfg
3NF podrazumeva 2NF i 1NF, 2NF podraz 1NF
----
1NF kriterijum:
atomicne vrednosti
1. ne sme vise vrednosti u 1 koloni
2. ne sme vise kolona za istu stvar
-----
2NF kriterijum:
1. mora biti u 1NF
2. no partial dependency
partial dependency - kada neka non-key kolona NE zavisi od svih pk kolona kompozitnog kljuca (u 1 tabeli)
nastavnik zavisi od predmeta, ali ne i od ucenika
---
moras da premestis tu kolonu u novu tabelu sa pk koji je deo composite kljuca
------
3NF kriterijum:
1. 1NF i 2NF
2. no transitive dependency
non key column odredjuje neku kolonu // to
imas tabelu u tabeli ocigledno // to to
---
// 1 tabela
stidentId -> examType -> maxMarks
// 2 tabele
stidentId -> examType
examType -> maxMarks
----
ili kada calc(col1, col2) odredjuje neku kolonu
----------------------------
----------------------------
// 1st, 2nd and 3rd Normal Form (Database Normalisation) - dobar, jednostavan
// Learn Learn Scratch Tutorials
// https://www.youtube.com/watch?v=J-drts33N8g
1NF:
1. all rows must be unique, no duplicate rows (makar pk)
2. svaka celija must contain single value (not a list)
3. value must be non divisible, atomicna (ime, prezime)
----
2NF:
1. 1NF
2. no partial dependency - sve non-key kolone moraju potpuno da zavise od composite pk
kada neka kolona nema nikakve veze sa kolonom koja je deo composite pk tabele // to
neka kolona zavisi samo od dela pk // ok
----
3NF:
1. 1NF i 2NF
2. no transitive dependency
sve kolone zavise od pk, a ne od drugih non-key kolona
-------------------------
-------------------------
// SQL VIEWS + Complex Queries, Cross Joins, Unions, and more
// Socratica
// https://www.youtube.com/watch?v=8jU8SrAPn9c
VIEW - virtual table, mozes bilo koji SELECT da pretvoris u VIEW
jednom kada ga kreiras on postoji kao tabela koju mozes da query
moze za privacy gde su prave tabele (neke kolone) poverljive
1. privatnost, 2. slozen select, 3. spoj
---------
// primer za privatnost
CREATE VIEW martian_public AS 
SELECT martian_id, first_name, last_name, base_id, super_id
FROM martian_confidential;
--------
// primer za slozen view kasnije prosti selecti
// view je tabela za kasniji upit
// UNION  - 2 selecta moraju isti broj kolona i tip
// concat jedna ispod druge rows
CREATE VIEW people_on_mars AS 
SELECT CONCAT('m', martian_id) AS id, first_name, last_name,
        'Martian' AS status // ubacuje const string 'Martian' u celije
FROM martian_public
    UNION
SELECT CONCAT('v', visitor_id) AS id, first_name, last_name,
        'Visitor' AS status
FROM visitor;
----
// primer view nad spojem 2 tabele
// kad cross join, a kad full outer?
// cross ne ubacuje null, za sve moguce kombinacije
// coalesce radi isto sto i u javascript, null ?? 0
CREATE VIEW base_storage AS
SELECT b.base_id, s.supply_id, s.name
    // zamenjuje null sa 0
    COALESCE(
        (SELECT quantity FROM inventory
        WHERE base_id = b.base_id AND supply_id = s.supply_id), 0)
        AS quantity // rename default coalesce
FROM base AS b
CROSS JOIN supply AS s; // svaki sa svakim, kao 2 tabele u FROM
----------------------
----------------------
// Normalization in DBMS | Insertion, Updation & Deletion Anomalies
// making IT simple
// https://www.youtube.com/watch?v=UF0UHCX-z0E
normalizacija je proces uklanjanja redundanse iz tabele
anomalije:
1. insertion
department podtabela u tabeli radnik
ne mozes da ubacis novi department jer je radnik null
2. deletion
kada obrises jedinog radnika iz departmenta, obrises i department
3. updation
promeni sefa departmenta, ako ne izvrsi na svim rows
dobije inkonzistentnost
-----
svi primeri su mu za podtabelu department u tabeli radnik
3NF, transitive dependencies
-----
razbije na dve tabele, zaista ogromna prednost
-----
pk ne moze biti null
-------------------------
redundancy = repetition
--------------------------
--------------------------
// create statements for 1:1, 1:n, m:n relations
https://stackoverflow.com/questions/7296846/how-to-implement-one-to-one-one-to-many-and-many-to-many-relationships-while-de
----
join with m:n pivot table (3 tables) ?
multiple tables, multiple different joins, smisao joina ?
// https://stackoverflow.com/questions/12824087/sql-select-in-n-to-m-relationship
// zapravo je prosto, iza ON ide uslov spoja
// isto kao cross join sa FROM t1, t2, t3 pa WHERE sa tim uslovima 
SELECT
  DISTINCT a.id
FROM
  author a
  JOIN book_author ba ON a.id = ba.authorId // samo JOIN je kao FROM t1, t2, t3
  JOIN books b ON b.id = ba.bookId
WHERE b.category = 'Fantasy'
--------------------
// kada koristiti relational vs document model
// relacioni za
Struktuirani podaci
Složeni upiti
ACID transakcije
------
// document model za
Nestrukturirani podaci
Fleksibilnost sheme
Skaliranje horizontalno
Brza razvojna agilnost


