// sqlite query json, D1
-> vraca json objekat za key 
->> vraca string za key 
SELECT sum(value->'jsonKey1'->>'jsonKey2' = 'jsonValue') asColumnName
--------------
// common table expression (CTE) - WITH
WITH MonthPairs AS ( // to je CTE
    SELECT 
        m1.name AS forMonth, 
        m2.name AS comparedToMonth
    FROM month m1
    JOIN month m2 ON m1.name < m2.name
    WHERE m1.name <= ? AND m1.name >= ? AND m2.name <= ? AND m2.name >= ?
)
CTE that allows you to define a temporary result set (like a temporary table) 
that can be reused in subsequent parts of the query
razlika od view je da cte postoji samo za 1 query, a view je sacivan u bazi
-------------
// upsert on conflict
poenta: mora posebni ON CONFLICT za svaki PK i UNIQUE sa kolonom koju overwrite-ujes
`INSERT INTO company (name, commentId, createdAtOriginal, monthName)
    VALUES (?, ?, ?, ?)
    ON CONFLICT(name, monthName) DO UPDATE SET updatedAt = CURRENT_TIMESTAMP
    ON CONFLICT(commentId) DO UPDATE SET updatedAt = CURRENT_TIMESTAMP`
---------------
// group by sc.name obuhvata vise sc instanci, name nije unique or pk, zato mora MAX() da se uzme max instanca
GROUP BY sc.name
ORDER BY MAX(sc.createdAtOriginal) DESC -- sort companies