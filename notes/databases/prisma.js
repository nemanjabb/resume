prisma client - entity manager, data mapper pattern
prisma cli - je npx prisma nesto, kao artisan,samo za konzolu
--------------
--------------
// migrate db
// https://www.prisma.io/docs/concepts/components/prisma-migrate
migrira shemu, zadrzava podatke u bazi
---
// migracije su vazne za test i prod dev gde shema evoluira sa sve podacima
npx prisma migrate deploy 
---
npx prisma migrate dev --name ime-migracije
// skip seed flag
npx prisma migrate dev --skip-seed
---
// reset - brise bazu i ponovo je kreira - to mi treba, nikad u produkciji
npx prisma migrate reset
------
// migrate u produkciji i test env, u ci-cd ide
npx prisma migrate deploy
--------------
--------------
// seed db
// https://www.prisma.io/docs/guides/database/seed-database
npx prisma db seed

// pokrece ovo u package.json
"prisma": {
    "seed": "ts-node prisma/seed.ts" // bilo koja shell komanda ovde
}
-------------------
-------------------
// prisma db admin
// otvara admin na http://localhost:5555/
npx prisma studio
------------------------------
// truncate all tables postgres
// https://stackoverflow.com/questions/2829158/truncating-all-tables-in-a-postgres-database
// (works, tested), stored procedure per database level

// first create stored procedure in db

CREATE OR REPLACE FUNCTION truncate_tables(username IN VARCHAR) RETURNS void AS $$
DECLARE
    statements CURSOR FOR
        SELECT tablename FROM pg_tables
        WHERE tableowner = username AND schemaname = 'public';
BEGIN
    FOR stmt IN statements LOOP
        EXECUTE 'TRUNCATE TABLE ' || quote_ident(stmt.tablename) || ' CASCADE;';
    END LOOP;
END;
$$ LANGUAGE plpgsql;
---
// then call with POSTGRES_USER=postgres_user from .env*.local
SELECT truncate_tables('postgres_user');
---------------------------------
// id mora gornji, prvi definisan, zapazi, vazna fora
// ovo je Post tabela, User je jak entity
// inace prisma.user.create().posts bude nedostupan u tipu
userId String?   @map("user_id")
user   User? @relation(fields: [userId], references: [id])
---------------------
// to use import in prisma seed
// error: 
(node:34420) Warning: To load an ES module, set "type": "module" in the package.json or use the .mjs extension.
SyntaxError: Cannot use import statement outside a module
-----------
// resenje:
"prisma": {
  "seed": "ts-node --compiler-options {\"module\":\"CommonJS\"} prisma/seed-run.ts"
}
----------------------------
// mora Prisma namespace da se importuje // to
// https://www.prisma.io/docs/concepts/components/prisma-client/advanced-type-safety
import { Prisma } from '@prisma/client';
const data: Prisma.BusinessUpdateInput = {...}
// ovo ne radi
import { BusinessUpdateInput } from '@prisma/client';
-------------------------
// prisma reset, radi
prisma resetWarningCache, ovo radi bez ponovo seed
"prisma:reset": "npx prisma migrate reset --skip-seed",
greska je bila, ne treba dev
"prisma:reset": "npx prisma migrate reset dev --skip-seed",
------------------------
// 1:n:m relations u prisma
https://www.prisma.io/docs/concepts/components/prisma-schema/relations
1:N i 1:1 su zapravo IDBObjectStore, samo 1:1 ima unique na FK
iscitaj ovo
-------------------------
// prvo stavi Prism.tip pa dalje autocomplete, to
import { Prisma } from '@prisma/client'
const whereCalls: Prisma.CallWhereInput =
fromDate || toDate
  ? {
      AND: [
        ...(fromDate ? [{ startTime: { gte: fromDate } }] : []),
        ...(toDate ? [{ startTime: { lte: toDate } }] : []),
      ],
    }
  : {};
-------------------
// Json tip i operacije u Prisma
https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields
samo jedan Json tip ima, Json[] je vs code default predlagao kao za sve tipove
ka procitas json polje iz prisma client kastujes u Prisma.JsonArray
const petsObject = user?.extendedPetsData as Prisma.JsonArray
--------------------
istrazi prisma migrations kad imas data u database, verovatno mora da se cuvaju migration fajlovi
------------
naming for migrations?
-------------------------
// AND nije obavezno, default, samo ih navedes // ok
const account = await prisma.account.findFirst({
  where: { AND: [{ provider }, { providerAccountId }] },
});
// isto sto i
const account = await prisma.account.findFirst({
  where: { provider , providerAccountId },
});
-----------------
// next-auth secret
openssl rand -base64 32
jguV1FHKz0yWN+uKfFep4G4REhzkiPuyA7rOaTXL7tw=
----------------------
// prisma ima python client, radi sa fastApi
https://github.com/RobertCraigie/prisma-client-py
-------------------
Dbeaver je dostupan na Windows, MacOS i Linux, java // zapazi
i za sqlite, postgres, mysql, za sve // nema sta dalje