---------------
// drizzle migrations
// https://orm.drizzle.team/docs/migrations
1. set drizzle config, schema path, migrations path, driver, db connection 
1. define/update schema 
2. generate migration - ads .sql file in /migrations folder
drizzle-kit generate:mysql
"db:generate": "NODE_OPTIONS='--import tsx' drizzle-kit generate:mysql", 
-----
// tsx je dev paket umesto ts-node, bez config, NIJE npx
https://github.com/privatenumber/tsx
npx tsx ./script.ts
---
npm install --save-dev tsx
"dev": "tsx ./file.ts"
-------
3. run migrations
// run obican migrate.ts file
npm tsx src/migrate.ts
// migrate.ts
import { migrate } from 'drizzle-orm/mysql2/migrator';
await migrate(db, { migrationsFolder: './src/migrations'});
----------
migrate() - idempotent, izvrsi samo nove migracije // ok, zapazi
----
"db:migrate": "dotenv -e .env-file -- tsx ./src/migrate.ts",
---------------
// prakticno
yarn db:generate
yarn db:migrate
----------------
// drizzle transactions
transaction - grupisanje nekoliko sql statements u 1 unit (commit) i potencijalni rollback
----------------
// dobra fora, i tipovi
Message leftJoin User
leftJoin - User | null 
innerJoin - User
------------------
null je izostavljen iz unique check
moze da se koristi unique 'active' | null 
--------
// inline sql drizzle, za 1 column
const createdBy = sql<string>`NULLIF(TRIM(CONCAT(IFNULL(${userTable.firstName}, ''), ' ', IFNULL(${userTable.lastName}, ''))), '')`
const status = sql<Status>`IFNULL(${users.status}, 'inactive')`