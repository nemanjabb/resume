
/**
* Angular notes from official v1.x docs. Spring 2015.
*
*
*
*References:
*	https://docs.angularjs.org/guide
*	https://docs.angularjs.org/guide/external-resources
*	http://kirkbushell.me/when-to-use-directives-controllers-or-services-in-angular/
*	http://blog.thoughtram.io/angular/2015/07/07/service-vs-factory-once-and-for-all.html
*
*/

//angular controller vraca this tj modul (base objekat) za ulancavanje
angular.module('demo', []).controller('MainCtrl', function($scope)
-----------
//dependency injection

//law of demeter - objekti trebaju d akomuniciraju samo sa neposrednim susedima
-----------------
3 ekvivalentna nacina za inject

1. function MyController($scope, greeter) {//ne moze za minifikaciju
2. var MyController = function(renamed$scope, renamedGreeter) {}
	MyController['$inject'] = ['$scope', 'greeter'];//mora redosled args d ase poklapa//kao arrray
3. someModule.factory('greeter', ['$window', function(renamed$window) {...}]);//inline  array
------------------
var injector = angular.injector(['myModule', 'ng']);//injektor sam sebe pribavlja// angularov metod
var greeter = injector.get('greeter');//pribavljanje servisa u promenljivu
-----------
injector.instantiate(MyController);//poziva se na ng-controller direktivu
-----------------
We can specify functions to run at configuration and run time for a module by calling the run() and config() methods za modul
----------------
Controllers are "classes" or "constructor functions" that are responsible for providing the application 
behavior that supports the declarative markup in the template
Controllers are special in that, unlike services, there can be many instances of them in the application. 1 za ng-controller
controller - $scope, $route
services - samo $rootScope
---------------------
//templates

Directive — An attribute or element that augments an existing DOM element or represents a reusable DOM component.
Markup — The double curly brace notation {{ }} to bind expressions to elements is built-in Angular markup.
Filter — Formats data for display.
Form controls — Validates user input
---------------------
//expressions
{{ expression }}
1+2    a+b    user.name    items[index]
-----------
context - $scope, ne vide se globalni js window document itd
forgiving - ang generates undefined i null
nema if for itd
filteri
-------------
One time binding: {{::name}}
--------------
//filters
//nista, statefull neki
----------------
----------------
//forms
binding radi na inputima koji imaju ngModel direktivu
---------
custom trigger 
ng-model-options="{ updateOn: 'blur' }"
odlozeni trigger - debounced
ng-model-options="{ debounce: 250 }"
------------
//custom validacija - direktiva

var INTEGER_REGEXP = /^\-?\d+$/; //konstanta velikim slovima
---------
ng-show je uslov za pojavu tog taga
<span ng-show="form.length.$error.float">
--------
<input type="text" ng-model="length" name="length" smart-float-direktiva />
----------
//custom controla preko direktive
custom control to work with ngModel and to achieve two-way data-binding it needs to:
implement $render method
call $setViewValue method

elm.on('dogadjaj', function() {//izvrsi fju na dogadjaj
	scope.$apply(function() { //nema dogadjaj, izvrsi uvek
		ctrl.$setViewValue(elm.html());
	});
});

direktiva vraca funkciju koja fraca objekat sa fjom
-----
   require: 'ngModel',
    link: function(scope, element, attributes, controller) {//uvek ovo prenosi
//element - tag u kojem je direktiva kao atribut
--------
console.log()
ispisan objekat ciji su propertiji objekti, zapazi pros, isto je izlistano desno kad ga kliknes, propertiji
Object { $attr: Object, $$element: Object, contenteditable: "true", title: "Click to edit", ngModel: "content", $$observers: Object }
-------------
10 10
-------------
//direktive

For AngularJS, "compilation" means attaching event listeners to the HTML to make it interactive.
---------
camelCase - u reci pocetno slovo veliko
u htmlu neka-direktiva, u js se mapira na nekaDirektiva
data-neka-direktiva - ako se koristi validacija html-a
----------
kontroler odredjuje scope u okviru koga bindizi i ostali rade
-----------
//pozivi direktiva u html

<my-dir></my-dir>
<span my-dir="exp"></span>//koristi ovo
<!-- directive: my-dir exp -->
<span class="my-dir: exp;"></span>

---------------------
direktiva vraca objekat sa nekim propertijima

.directive('myCustomer', function() {
	return {
	  template: 'Name: {{customer.name}} Address: {{customer.address}}'
};
	
<div my-customer></div>//ubacuje templejt ovde
-------------
restrict: 'AEC' //property atribut, tag, class
---------------
Domain-Specific Language for parts of your template - tag - od nule, logicno
Use an attribute when you are decorating an existing element with new functionality - dodatak - atribut
-------------
Isolating the Scope of a Directive
//default je scope kontrolera
scope: {
	customerInfo: '=info'
},
<my-customer info="naomi"></my-customer>//info//pre je islo <my-customer></my-customer>
<div bind-to-this="thing">, youd specify a binding of =bindToThis

{{customerInfo.address}}//templejt
//This is helpful when building reusable components because it prevents a component 
//from changing your model state except for the models that you explicitly pass in.
-------------
scope i closure vidi promenljive iznad sebe, a ispod ne, crockford enkapsulacija prica
----------------
//direktive koje manipulisu DOM-om
Directives that want to modify the DOM typically use the link option function link(scope, element, attrs) { ... } where:

scope is an Angular scope object.
element is the jqLite-wrapped element that this directive matches.
attrs is a hash object with key-value pairs of normalized attribute names and their corresponding attribute values.
-------------------
<input ng-model="format"> //ugradjena direktiva koja binduje input za scope
----------------
.directive('myCurrentTime', ['$interval', 'dateFilter', function($interval, dateFilter) {//ubaceni DI servisi

timeoutId = $interval(function() {//ovo se poziva kao hendler na svaku milisekundu, klik recimo
	updateTime(); // stampa vreme
}, 1);

function updateTime() {
	element.text(dateFilter(new Date(), format));//stampa string samo
}
-------
scope.$watch(attrs.myCurrentTime, function(value) {//gleda promene koje uneo korisnik pa updateuje scope
  //format = value;
  //updateTime();
});
<span my-current-time="format"></span>

---------------------
When a DOM node that has been compiled with Angulars compiler is destroyed, it emits a $destroy event. 
Similarly, when an AngularJS scope is destroyed, it broadcasts a $destroy event to listening scopes.

By listening to this event, you can remove event listeners that might cause memory leaks. Listeners registered 
to scopes and elements are automatically cleaned up when they are destroyed, but if you registered a listener on 
a service, or registered a listener on a DOM node that isn't being deleted, you'll have to clean it up yourself 
or you risk introducing a memory leak.

element.on('$destroy', ...) or scope.$on('$destroy', ...) to run a clean-up function when the directive is removed.

------------------
.controller('Controller',//veliko slovo - konstruktor
-----------
link: u direktivi je propertu za fju koja treba da se izvrsi
----------
//Directive that Wraps Other Elements
The transclude option changes the way scopes are nested. It makes it so that the contents of a transcluded directive 
have whatever scope is outside the directive, rather than whatever scope is on the inside. In doing so, it gives the 
contents access to the outside scope.
---------------
<div ng-transclude></div>//child template
----------------
directive that reacts to events on its elements
 .directive('myDraggable', ['$document', function($document) {//servis
----------------
//Directives that Communicate
require: '^myTabs',
The ^ prefix means that this directive searches for the controller on its parents
--------
Directives can specify controllers using the named controller property// ili prenesen kao arg Ctrl
use controller when you want to expose an API to other directives. Otherwise use link. 
----------------
----------------
//animacije

1 css 2 jquery animate
---------------
css-class osnovna klasa
css-class-add izvedena iz css-class, css-class-add-active //angular dodao klasu
-----------------
-----------------
//moduli

Configuration blocks - get executed during the provider registrations and configuration phase. 
Only providers and constants can be injected into configuration blocks. 

Run blocks - get executed after the injector is created and are used to kickstart the application. 
Only instances and constants can be injected into run blocks. 
---------
angular.module('myModule', []).config()
------------
Modules can list other modules as their dependencies. Depending on a module implies that required module 
needs to be loaded before the requiring module is loaded.
--------------
angular.module('myModule', []) - create
angular.module('myModule') - retrieve
----------------
modularna organizacija i testiranje da bi mogao da debugiras velike aplikacije
-----------------
-----------------
//html compiler

angular compiler radi sa DOM-om. DOM instanca je vezana u bindingu, ostaju event handleri nanjoj ako postavis
-------------------
//obican templejt u koji se ubacuje objekat kontekst-scope, ovde ima jos i evente i watch
ovako radi compiler

var $compile = ...; // injected into your code
var scope = ...;
var parent = ...; // DOM element where the compiled template can be appended

var html = '<div ng-bind="exp"></div>';

// Step 1: parse HTML into DOM element
var template = angular.element(html);

// Step 2: compile the template
var linkFn = $compile(template);

// Step 3: link the compiled template with the scope.
var element = linkFn(scope);

// Step 4: Append to DOM (optional)
parent.appendChild(element);
---------------------
Link means setting up listeners on the DOM and setting up $watch on the Scope to keep the two in sync. 
$watch je listener na promenljivoj u scopeu
----------------
//How Scopes Work with Transcluded Directives
This makes the transcluded and widget isolated scope siblings
-----------------
-----------------
//providers
mogu da se injektuju sa DI gde god su potrebni
--------------
The injector creates two types of objects:
1. services 
2. specialized objects - controllers, directives, filters or animations
---------------
recipe: value, factory, service, provider, constant
---------------
//value
var myApp = angular.module('myApp', []);
myApp.value('clientId', 'a12345654321x');//smesan
-----------------
//factory
fja sa fjama i obradom
The Factory recipe constructs a new service using a function with zero or more arguments
The return value of this function is the service instance

All services in Angular are singletons , injector then caches the reference

ability to use other services (have dependencies)
service initialization
delayed/lazy initialization
-------------------
//service
sa new
vraca obj sa new neke prethodno definisane klase
myApp.factory('unicornLauncherService', ["zavisnost", function(zavisnost) {
	return new UnicornLauncher(apiToken);
}]);
//skraceno
myApp.service('unicornLauncher', ["apiToken", UnicornLauncherConstructor]);
-------------------
//provider
impl $get metod, u config se poziva, u cofig se injektuju samo provideri
is the core recipe type and all the other recipe types are just syntactic sugar
//tad
You should use the Provider recipe only when you want to expose an API for 
application-wide configuration that must be made before the application starts.

Provider recipe is syntactically defined as a custom type that implements a $get method.
------
//boot frameworka, boot() i register() laravel
1. configuration phase of the application life-cycle - it configures and instantiates all providers
services arent accessible because they havent been created yet
2. application life-cycle the run phase - interaction with providers is disallowed and the process of creating services starts
---------------------
//constant recipe
kao value ali u obe faze
simple values, like URL prefixes, that dont have dependencies or configuration make them available in both the configuration and run phases
moze da se injektuje gde god ti treba, controller npr
---------------------------
//Special Purpose Objects
//interfaces are Controller, Directive, Filter and Animation
plugins and therefore must implement interfaces specified by Angular

use the Factory recipe behind the scenes, osim controllera
controllers are not singletons, instantiated via its constructor, every time the app needs an instance//ng-controller
-----------------------------
//conclusion
Service recipe works better for objects of a custom type, while the Factory can produce JavaScript primitives and functions.
//service klase sa new, factory fje closure
-----------------------------
-----------------------------
//bootstrap
da ne blokira html 
<script src="angular.js"></script>
</body>
---------------
Automatic Initialization - ng-app
Manual Initialization - angular.bootstrap(document, ['myApp']);
--------------------------
--------------------------
//$location

1. application needs to react to a change in the current URL 
2. you want to change the current URL in the browser.
---------
regular url: http://foo.com/bar?baz=23#baz //default mode
hashbang url: http://foo.com/#!/bar?baz=23#baz //html5 mode
------------------------
//gotova dokumentacija
------------------------
Yo, a scaffolding tool, Grunt, the build and testing tool, and Bower, the package management tool
---------------------
---------------------
//learn angular org
Values are great, but wouldnt it be nice if we could inject a function instead of a variable? 
This is the purpose of the factory recipe.

value kad oces da injektujes vrednost
factory kad oces da injektujes funkciju
service injektujes objekat, ili factory koji vraca objekat

service - singleton, jedna referenca
moduli moraju da se prenesu u drugi modul
---------------------
u servis mogu da se prenesu drugi servisi
-----------------
//service

angular.module("services", [])
    .service("myService", ["dependency", MyObject]);

// Declared elsewhere...//tacno to
function MyObject(dependency) {
    this.value = dependency.value;
}
//isto sa factory
angular.module("services", [])
    .factory("myService", ["dependency", function (dependency) {
        return new MyObject(dependency);
    }]);

// Declared elsewhere...
function MyObject(dependency) {
    this.value = dependency.value;
}
//service je sintactic shugar trivijalno
-------------------
//stringovi i argumenti se mapiraju po redosledu
.controller("index", ["$scope", "multiplier",
	function ($scope, multiplier) {
		$scope.product = multiplier.multiply(2);
	}]);
--------------------
//onChange event trivijalno
$watch se binduje na promenljivu u scope i poziva se fja kad god se prom promeni

$scope.$watch("factor", function (newValue, oldValue) {
	$scope.product = newValue * 2;
});

---------------
//filteri

1. transformisanje vrednosti
2. fitriranje kolekcija (polja)

filtrira funkcija koja je VRACENA

angular.module("root", [])
    .filter("doSomething", function () {
        return function(input) {
            // Transform the input and return a value.
        }
    });
//filter fja vrti sve stringove u petlji i poziva fje tj closure	
//injektovanje dodaje se rec "Filter"
.controller("index", ["doSomethingFilter", function(filter) {
        // ...
    });
------------------
//$resource pravi pretpostavke

$resource("url/:var");
object.$save(function () {done}) //zapazi dolar $, save je post
--------
//CRUD
1. get
2. save - post
3. query get sa return
4. remove - delete
-----------------------------

validacija formi

pola aplikacije napises u html-u jer vec postoje direktive za sve

-----------------
servisi, di, globalne fje koje ne sprezu
----------------
require: 'direktiva1', //znaci da direktiva 2 zahteva postojanje direktiva1 na istom ili parent dom elementu
controller - direktiva moze da ima kontroler
---------------
treba da znas kako radi angular digest ciklus da bi zznao, a ne black box
------------
recipe: value, factory, service, provider, constant

const, fja, objekat, config
-------------------------
soa aplikacija nema forme, ima objekte, webapi
-------------------------

----------------------------------------------
----------------------------------------------
----------------------------------------------
//docs 2016



direktive - pravljenje custom kontrola u wpf
di - instance, namespace definicije tipova, autoloading, resolving

//moduli
moduli - booting aplikacije, ng-app="moduleName"


    A module for each feature
    A module for each reusable component (especially directives and filters)
    And an application level module which depends on the above modules and contains any initialization code.


angular.module('myModule', []).
config(function(injectables) { // provider-injector
  // This is an example of config block.
  // You can have as many of these as you want.
  // You can only inject Providers (not instances)
  // into config blocks.
}).
run(function(injectables) { // instance-injector
  // This is an example of a run block.
  // You can have as many of these as you want.
  // You can only inject instances (not Providers)
  // into run blocks
});

//Data Binding
single-source-of-truth, ne pises kod za syncing
controller is completely separated from the view and unaware of it

-----------------------------
-----------------------------
//HTML Compiler
Compile: 
Link:
----------
A directive is just a function which executes when the compiler encounters it in the DOM.
view = link fja + scope model
---------
Compiler $compile is an Angular service which traverses the DOM looking for attributes.

compiler sorts the directives by their priority
Each directives compile functions are executed
compile function returns a link function
These functions are composed into a "combined" link function, which invokes each directives returned link function
The result of this is a live binding between the scope and the DOM.

//odlican pros

var $compile = ...; // injected into your code
var scope = ...;
var parent = ...; // DOM element where the compiled template can be appended

var html = '<div ng-bind="exp"></div>';

// Step 1: parse HTML into DOM element
var template = angular.element(html);

// Step 2: compile the template
var linkFn = $compile(template);

// Step 3: link the compiled template with the scope.//RENDER
var element = linkFn(scope);

// Step 4: Append to DOM (optional)
parent.appendChild(element);

LINK FJA JE RENDER, SPOJI DATA I TEMPLATE, samo kompajlira u dom a ne u string
----------------
//ng-repeat primer
compile menja strukturu dom, a link postavlja listenere
why the compile process has separate compile and link phases?

The short answer is that compile and link separation is needed any time a change in a model causes a change in the structure of the DOM.

the compile phase where all of the directives are identified and sorted by priority, and a linking phase where any work which "links" a specific instance of the scope and the specific instance of an <li> is performed.
--------------------------------
--------------------------------
//scopes
scopei organizovani u stablo
kontekst templejta, tj data
------------
($watch) to observe
($apply) to propagate any model changes// render, refresh view, kao i svaka grafika, invalidate
------
//nested scopes
"isolate scopes". 
A "child scope" (prototypically) inherits
----------
During the template linking phase the directives set up $watch expressions on the scope
---------
Both controllers and directives have reference to the scope, but not to each other. 
This arrangement isolates the controller from the directive as well as from the DOM. 
This is an important point since it makes the controllers view agnostic, which greatly improves the testing story of the applications.
----------
The scope is the single source-of-truth 
------------
//scope hijerarhije
scopovi stablo, izvrsava se prvo list - prioritet, override, do root scope, dok ne nadje
scopovi javascript klase prototipsko nasledjivanje
some directives create new child scopes
dom stablo i scope stablo se podudaraju, paralelno
ng-scope klasa na elementima koji ref neki property
-----------
Scopes are attached to the DOM as $scope data property
ng-app je root scope
//debug scope
access the currently selected element in the console as $0
To retrieve the associated scope in console execute: angular.element($0).scope() or just type $scope
----------
//events
The event can be broadcasted to the scope children or emitted to scope parents.
-----------
//scope zivotni ciklus
To properly process model modifications the execution has to enter the Angular execution context using the $apply method
In the $digest phase the scope examines all of the $watch expressions and compares them with the previous value.This dirty checking is done asynchronously. 
watch radi sa unit of work, skupi na gomili pa izvrsi, ne sme 2 watch paralelno, zakljucavanje, rade asinhrono
$digest je watch na modelu
$apply da prerenderuje kad promenis model
1 Creation
2 Watcher registration
3 Model mutation
4 Mutation observation
5 Scope destruction
---------
//scope i direktive
Observing directives
Listener directives
----
n most cases, directives and scopes interact but do not create new instances of scope. 
ng-controller and ng-repeat, create new child scopes 
--------
//performanse
Dirty checking 3 strategies (dubina):
1. Watching by reference (scope.$watch (watchExpression, listener)) //najplice, da li je pointer na nesto drugo, ne detekt prom elem polja
2. Watching collection contents (scope.$watchCollection (watchExpression, listener)) //prvi nivo samo
3. Watching by value (scope.$watch (watchExpression, listener, true)) //najdublje //ceo objekat ili polje obidje
----------
//browser event loop
Angular modifies the normal JavaScript flow by providing its own event processing loop
An explicit call to $apply is needed only when implementing custom event callbacks, or when working with third-party library callbacks.
callbackovi kaceni na angular evente ili na js
slozeno, koraci kako se procesira event i syncuje dom posle promene

----------------------------------
----------------------------------
//E2E Testing
bugove integracije
sometimes issues come up with integration between components which cant be captured in a unit test. End-to-end tests are made to find these problems.
protractor je node.js (kao gulp) program - tool koji koristi selenium web driver (headles browser)
testiras scenarije kliktanja po sajtu, mesto da rucno gledas kad je velika app
it blokovi, jasmine sintaksa

test je trivijalni mali posao da pozoves da radi ono sto si implementirao, poziv i assert
 ne moze angular.bootstrap mora ng-app directiva za protraktor

----------------------------------
----------------------------------
//Unit Testing

javascript ne izbacuje kompajlerske greske skoro
isolate the unit of code under test //IZOLACIJA, separacija
mix concerns resulting in a piece of code which does everything. It makes an XHR request, it sorts the response data, and then it manipulates the DOM.
----
//Separation of Concerns
For your XHR requests, we provide dependency injection, so your requests can be simulated. 
For the DOM, we abstract it, so you can test your model without having to manipulate the DOM directly
----
dependency injection da bi mogao da prosledjujes mock
----------
karma za unit, protractor e2e
Karma is a JavaScript command line tool, run against a number of browsers 
display the results of your tests on the command line once they have run in the browser.
jasmine: describe blocks, it blocks, assertions
------
ngMock module
$httpBackend fejk ajaxi
-------
//Testing a Controller
inject controller beforeEach(inject(function(_$controller_){
var controller = $controller('PasswordController', { $scope: $scope });// $controller ima sve kontrolere
beforeEach(function() { zajednicki kod
-------
//Testing Filters
transformisu podatke u pogodne za pregled, i rasterecuju logiku aplikacije
-------
//Testing Directives

// Compile a piece of HTML containing the directive
var element = $compile("<a-great-eye></a-great-eye>")($rootScope);
// fire all the watches, so the scope expression {{1 + 1}} will be evaluated
$rootScope.$digest();
// _$srvice_ fora, tu ubaci globalne angularove servise koji su uvek definisani
beforeEach(inject(function(_$compile_, _$rootScope_){
transclude - transform + include
// testing transcluded directives
transclude direktiva vraca zakomentarisan original i umetnutu novu direktivu
za test mora da se umota u <div> jer se vraca komentar
$q - promises
------------
//beforeAll()
//za jednu instacu servisa u svim testovima
module.sharedInjector(); i beforeAll()
------------------------------
------------------------------
//2016 2
//https://docs.angularjs.org/guide/external-resources
//http://kirkbushell.me/when-to-use-directives-controllers-or-services-in-angular/

//servisi singletoni za sta
So. When do we use services? Whenever we want to share data across domains
//controlers za sta
Controllers should be used purely to wire up services, dependencies and other objects, and assign them to the view via scope.
//direktive
reusable komponents, dom manipulation
-----------------
//https://docs.angularjs.org/guide/concepts
Compiler 	parses the template and instantiates directives and expressions
Service 	reusable business logic independent of views
-----
kompajler parsuje templejt i transformise ga (kompajlira) u view
hvatanje dom-a van direktiva onemogucava testuranje
to move view-independent logic from the controller into a service
------------------
//http://blog.thoughtram.io/angular/2015/07/07/service-vs-factory-once-and-for-all.html
service je fja na modulu, modul.service()
service je konstruktor, a factory fja, pa vraca objekat literal
event notacija ("stringIme", function(){...} znaci vrti stringove kroz loop pa invokuje fje
service ne vraca nista, fje definise u this, angular ga poziva sa new, a factory fju samo poziva
service je wrapper oko factory
factory moze da izvrsava kod pre nego vrati
moze i service isto i da vrati
es6 klasa moze u service, tj konstruktor
--------------------------
//mvvm
https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel






































