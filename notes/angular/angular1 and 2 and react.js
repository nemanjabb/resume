/**
* Angular and React notes from Egghead.io video lessons (Angular 214 lessons, React 43 lessons). 
* Also notes from Minko Gechev's blog (all articles) and Rx.js article from Andre Staltz blog.
* Summer 2015.
*
*
*References:
*	https://egghead.io/technologies/angularjs
*	https://egghead.io/technologies/react
*	http://blog.mgechev.com
*	https://github.com/mgechev/angularjs-style-guide
*	http://www.sitepoint.com/understanding-angulars-apply-digest/
*	https://www.youtube.com/watch?v=lil4YCCXRYc - jafar husain async programing in es7 
*	https://gist.github.com/staltz/868e7e9bc2a7b8c1f754 - The introduction to Reactive Programming you've been missing  
*	http://www.slideshare.net/nzakas/scalable-javascript-application-architecture/62-Application_Core_Jobs_Manage_module
*
*/

//07 unit testing

failed test ti kaze sta sledece treba da uradis

//15 directive communication
direktiva moze da ima svoj kontroler koji ce d aima svoj ili zajednicki scope ={}
direktiva je element, pa kontroler kao da si stavio ng-controller na elem

//16 isolate scope
ako ne postavis scope:{} sve direktive (pozivi) dele isti scope
direktiva je objekat, enakpsulac elem, sa propertijima, prosledjuju se scope, elem, attrs, plus n tvojih args
{scope :{done:"&"}} ulancava invoke fja iz kontrolera...?
-----------
manji bufer, vise prekida procesoru, vise vremena otpada na svichovanje procesa
--------------
/msg alis list *google* //pretraga na freenode
-----------------
//17 isolate scope @
@ salje string
//objektna enkapsulacija taga
app.directive("drink", function(){ return {objekat}})
//direktiva je fja cije ime zadatao kao string, a telo kao anonim fja, verovatno zbog minifikacije//KAO EVENT PRAKTICNO//asinhrono izvrsavanje?//jeste event notacija, string je ime eventa
//isto i kontroler
app.controller("imeCtrl", function($scope){})
------------------
//18 isolate scope =
ocekuje objekat u tag atributu
------------------
//19 isolate scope &
poziv controlera iz same direktive recimo
komunikacija podataka iz direktive u glavni kontroler, nespregnuta, scope same direktive je izolovan
izolirani scope direktive znaci kad ponovis tag direktive vise puta na stranici sve imaju odvojene podatke
-------------------
//isolate scope review//rastumacio 99%
<phone></phone>
u direktivin prop scope: number, network, makeCall //atributi hrml-a se binduju na propertije scope:{} u direktivi
u html atributi number, network, make-call

@ salje string // number atribut
= binduje isti scop u svim instancama direktive //network atribut
& poziv fje u glavnom kontroleru//make-call atribut//nije ugradjena rec


//poenta da ne updateuje promenljive u instancama direktive i parent scope gde ne treba
//poluting scope - binding gde ne treba
-----------------
//21 transclusion basics//trivijalno
//prosto znacenje komplikovanog imena
postojeci html iz markupa da ne bude overriden nego ukljucen u template: definiciju, ng-transclude atribut, apend na kraj
-----------------
//22 alternative approach to controllers
angular da iz html-a vidis sta se desava a implem pozadin u js
ImeCtrl.nekaFja() u ng-click da odmah znas odakle je ta fja// ceo this kontrolera stavis u scope i to vratis iz kontrolera
kontroler je klasa cim ima this, instancira se negde sa new
this.nekaFja = function(){}
return $scope.ImeCtrl = this;//sta je ocekivano da kontroler vrati uposte? 
//fja i klasa izjednacene u js, fja vraca vr, sta onda klasa vraca?//ce da vrati to sto si vratio u prom kad pozivas sa new
krastavac: it returns that value, if the value is an object. Otherwise, it returns the freshly created object//zove konstruktor bazne klase
krastavac: thats not an object, so result will be equivalent to Object.create(abc.prototype)
krastavac: new is literally function new(Ctor, ...args){ var i = Object.create(Ctor.prototype); var o = Ctor.apply(i, args); return Object(o) === o? o : i }
krastavac: the actual code of new is implementation specific, and likely not written in JS (and likely very different from that when the JIT optimiser kicks in). That code example is just a summary of the specification you can see in http://es5.github.io/#x11.2.2 and http://es5.github.io/#x13.2.2
------------------
//23 - Thinking Differently About Organization//trivijalno

definises kontroler kao objekat odvojeno, pa ga prosledis u app.controller();
var cintrollers = {};
controllers.AppCtr = function ($scope){ telo kontrolera}
app.controller(controllers);

restrict:"E" prop u direktivi kako ce da bude pozvana kao tag ili atribut...trivijal
//gura u objekte kao namespace, trivijal
------------------
//24 - Building Zippy
link: fja u direktivi radi tacno sta?//link(scope, elem, attrs, args) telo direktive prakticno sta treba da radi
zippy - show hide na klik
transclude da zadrzi iz htmla, transcludovani se evaluira iz parent scope-a
cim ima scope:{} u direktivi ne koristi parent nego kreira novi
---------------
//25 - angular.element
replace:true brise tag direktive iz izlaznog doma//trivijal
compile: fja u direktivi prima angular.element obj koji kreira dom od stringa, tj renderuje templejt
var dom1 = angular.element("<div>...</div>");
iz nje vracas link: fju da bi u link imao scope
//vracanje fje - vracanje obj koji ce da se invokuje sa param//vracenje fje kao vracanje prostog objekta sa 1 fjom

//direktiva obicna klasa (njen clojure) koji ima privatne var i javne this, i vraca objekat { template:, link: itd...}
-----------------
van konteksta
//dekompozicija na trivijalne probleme moment.js
-----------------
//26 - $scope vs. scope//trivijal
$scope u kontroler// provider, rezervisani di objekat
scope u link() direktive//obican arg bzvz
ako nema scope{} ista instanca

------------------
//27 - templateUrl
templateUrl:"imefajla.html" u <script> tag u html, <script type="text/ng-template" id="imefajla.html" u okviru ng-app="" taga
poseban fajl na isti domen zbog ajaxa
------------------
//28 - $templateCache //trivijal
injektuje $templateCache provajder
-------------------
//29 - ng-view
<ng-view> tag, ruta u config, kontroler i template u rutu
--------------------
//30 - The config function
$nestoProvider dostupni u app.config(), $obicni ne, u app.contrler() obrnuto
config mora da se izvrsi pre kontroler jer on vec koristi $route
app.controller("stringIme", function(){}) svuda redom, znaci asinhrono izvrsavanje
-------------------
js paterni za oop stvari, xml confige itd
-------------------
//31 - $routeProvider api, pros
ruta radi iza #. app/#/nesto
$routeProvider.when("ruta1", {}).when().otherwise();
-------------------
//32 - $routeParams, trivijal
.when("/:message", {}) u kontroler pristupa sa $routeParams.message, link/{uhvatiOvoKaoPromenlj}, templejting u rutama
/:country/:state/:city
--------------------
//33 - redirectTo , trivijal
.otherwise({redirectTo:"/home"})
ili redirectTo:function(routeParams, path, search){return "/";}
---------------------
//34 - Promises
$q je promis provajder
var defer = $q.defer(); defer.promise.then(function(arg){return arg1;}) promise desice se u buducnosti//arg1 se prenosi kao arg u sledeci .then()//fora
defer.resolve(arg) u sadasnjosti
---------------------
//35 - Resolve , tezi malo ali nije nejasan
resolve: {app: function(){...}} property u ruti filter (pipe) koji se izvrsava pre kontrolera i templejta
---------------------
//36 - resolve conventions , isto tezi malo
u ruti resolve:{nekiProp1: ...} mapira na defer.resolve("loadData")
---------------------
//37 - resolve $routeChangeError, tezi malo
$rootScope postoji provider za root
defer.reject() neuspeh, AppCtrl sa rootScope i ViewCtrl
---------------------
//38 - Directive for Route Handling
$rootScope koristi u direktivi, $rootScope ima svoje evente
$routeChangedError ugradjen angular event (pocinje sa $)
----------------------
//39 - Route Life Cycle
redosled kojim se ispaljuju eventi, debuger//nije tesko iako izgleda, samo redolsed evenata
kontroler jeste handler za rutu event, ime kontrolera je event//to
ruta when("/ruta", { template:, controller:, resolve: promise}
----------------------
//40 - Providers, solidan
provider objekat sa vrednostima i fjama, geterima i seterima, koji mozes da injektujes kao arg u anonimne fje, njihov scope//to dobar

app.factory("game", function(){return{title:"StarCraft"}}) //injektujes game gde hoces, je isto sto i
$provide. mesto app, isto sto i $provide.provider(...), isto sto i
$provide.provider("game", function(){return{ $get: function() {return title:"StarCraft"}}})//$get je redefinicija $get fje, i cloruje iza $get: jeste definicija fje $get
{imeFje: function(){definicija fje imeFje}}

provider definises u app.provider("ime", function...), pa ga konfigurises u app.config(function(ime){ ime.setNesto()})//to
------------------------
//41 - Injectors

angular.injector(["app"]).invoke(function(game){ imas game ovde}), ili ubacis u bilo koji clojure kao arg, npr controller
injektor je objekat koji ima invoke metodu koja ubacuje service u funkciju koju prosledis, invoke(fja())
postoji i $injector provider ugradjen
-------------------------
//42 - Components and Containers, 2 vrste direktiva
ng-transclude atribut meces u tag gde hoces da iskopira postojece iz htmla
componente - proste direktive
containers - sadrze transkluzije tj druge direktive u sebi
---------------------------
//43 - ng-repeat and $index, $event, $log
u ng atribut pises javascript (i promenljive i fje()) kao string koje mozes da koristis u {{bindizima}}, a to je scope u kontroleru
info, warn, debug helper fje za log u konzoli
---------------------------
//44 - Experimental Controller as Syntax //alias za kontroler i koristis tacku
scope iz kontrolera sluzi za komunikaciju sa htmlom
ng-controller="RoomCtrl as room" implicitno u ctrl je $scope.room = this;//koristis this umesto $scope
ugnjezdeni kontroleri, room.foo, other.foo, znas iz kog dolazi foo, inace scope inheritance
----------------------------
//45 - Directive Communication
require: "^country" ^ je parent direktiva// da bi ubacio kontroler iz nje u link fju kao arg, inject
app.directive(...)
app.directive(...)//ne izvrsava se kod odozgo na dole nego asinhrono, blokovi
require:["^direktiv1", "^direktiv2"], onda arg u kink je polje kontrolera tih direkt tim redosledom
koristis fju defin u direkt 1 kontrolera u nekoj drugoj tako sto requir direkt i inject kontroler
direktive koje komuniciraju imaju parent-dete strukturu, stablo
-----------------------------
//46 - ngmin //za minifikaciju, trivijal, znao vec, ponavljaju 100x
stringovi umesto promenljive za injektovane
-----------------------------
//47 - ng-repeat-start //for endfor blok bzvz
ng-repeat-start ng-repeat-end
-----------------------------
//48 - animation basics // koristi stilove, trivijal
angular-animate.js
angular.module("app",["ngAnimate"])
css enter leave opacity
------------------------------
//49 - Animating with JavaScript // koristi js//trivijal
TweenMax.js
mesto css zadaje kao js objekte
done brise element iz doma
------------------------------
//50 - Animating the Angular Way  //reuse animaciju, ne koristis id u html i selektor u js, coupled
newVal je prosledjen iz html hide-me="isHidden", isHidden je bindovan u scope u kontroler, hide-me direktiva atribut
app.animation(".klasa", function(){addClass:, removeClass:})
u direktivi $animate servis
------------------------------
//51 - angular.copy
angular.copy - deep copy
u eventima u html pozivas fje definisane u kontroleru, kontroler je klasa, ima scope promenljive i scope fje, this je scope
binduje za kopiju pa je asignuje na original
------------------------------
//52 - Introduction to ui-router
ruter sa githuba umesto defaultnog
-------------------------------
//53 - Using angular.bootstrap to Initialize Your App
app.run() je prkticno jquery load event
angular.bootstrap(dom, modul) je prakticno ng-app iz js, nova aplikacija//doslovno jeste
-------------------------------
//54 - No Custom Markup
da nemas ng- po html, samo klase, a imas direktive
--------------------------------
//55 - Unit Testing Hello World
ako fja vraca link() fju treba da je pozoves() sa args// proguglaj linking fja js
compile() element, zove digest() pa expect - assert//prouci $scope.digest() fju
--------------------------------
//56 - Unit Testing a Directive
beforeEach()
injektuje providere koji se kopiraju u globalne, i compile()($rootScope)
it() i describe() assertuju uslove
--------------------------------
//nevezano
.directive('myCustomer', function() {
  return {
    templateUrl: 'my-customer.html'
  };
});
od directive() fje zavisi da le je to hendler ili sta, i kako ce 'myCustomer' da se registruje
anonim fja je klasa, ima svoje privatne var, javne this prom i fje
----------------------------------
//57 - Unit Testing Directive Scope, malo slozeniji ali se tumaci
izoliranje scopea direktive scope:{}

.directive('myCustomer', function() {
  return {
    link: linkFn123
  }
  //definises lokalno pa je vratis gore, mozes
  function linkFn123(scope, elem){
	  
  }
});
expect($scope, elem...) injektuje u link() direktive
ne mozes u dir da injektujes globalni scope (u testu) ako ona kreira svoj izolovani sa scope:{}
lokalni_scope_direktive = element.scope().
---------------------------------
//58 - Unit Testing Directive Scope Binding, malo i lako
element.scope() je izolirani scope direktive
$scope = $rootScope je globalni scope
scope:{ime1: "="} ime1=vrednost iz htmla de je bindovano//parent scope nesto ima ...
---------------------------------
//59 - Testing Underscores, trivijal
function(_$compile_ , _$rootScope_) angular ignorise _ pa mozes $compile = _$compile_, a var $compile je definisan van fje kao globalni, nece da se prenese na te args
---------------------------------
//60 - Speed Testing with WebStorm and Jasmine
webstorm autotest 1s
iit() ddescribe() testira samo 1 // t je iz jasmine
xit() exclude from tests
ako testtime da izvrsi sve, vece od 100ms
---------------------------------
//61 - Testing a Service, trivijal
servis je registrovani obj koji ima fje recimo i moze da se injektuje kao arg po fjama, I SINGLETON
napravio service i asertovao, 1 minut
---------------------------------
//62 - Testing a Controller, kratak
beforeEach(module("app")) //za modul
beforeEach(inject(function($controller))) //za injection

$controller provider vraca kontroler kao hashtabela appCtrl = $controller("NekiCtrl")
u describe() koristi globalni var appCtrl, asertuje vrednosti//definisan globalno, postavljen u beforeEach()
---------------------------------
//63 - Design Patterns - Mixin, bzvz mixin biblioteka neka
fina kontrola za kloniranje objekta, kopiranje propertija, extend()
In object-oriented programming languages, a mixin is a class that contains a combination of methods from other classes //wiki
---------------------------------
//64 - $http, $ajax iz jqueryja
//radi post i get i to je to
$http.get() obican $ajax.get() iz jquery, ili post()
get() vraca promise obj koji ima .success() fail() itd
promenljive u scope controllera su bindovane u html u ng-model i ng-click(arg)
---------------------------------
//65 - Design Pattern - Simple Mediator, kratak trivijal
emit i on() hvatanje eventa
pravi $emit servis i injektuje njega da ne injektuje ceo $rootScope
---------------------------------
//66 - transformResponse, pros
pattern da trimujes suvisne propertije iz objekta koji stigo ajaxom
kreira factory koji je obicna fja koja moze da se injektuje di, enkapsulirao trimovanje
---------------------------------
//67 - Chained Promises, ne vidi se lepo
3 asinhrona poziva, ali jedan zahteva podatke drugog kao ulaz, pa moraju redosledom
ugnjezdeni .then()
refaktorisao u loadDepart(user).then(loadFlightFja).then(loadForecastFja)//blokirajuci ceka dok ne dobije ulaz arg
---------------------------------
//68 - $provide.decorator, bzvz baje i ispise
---------------------------------
//69 - transformRequest
transformRequest: property u $http
tdd - prvo napises testove, sta ulazi, sta ocekuje da izadje
na tw da posanje umesto hash #hash, da kiti uvek
---------------------------------
//70 - ui-router Named Views
u state() definise rute
app.alt-one stanje.podstanje
one@app.alt-three
named_view@modul tj state, ruta
ui-view="content" atribut, named view
---------------------------------
//71 - Getting Started With Protractor //dobar za pocetak funkcionalnih testova//install, configure, first func test
end to end test, pritiska dugmice i gleda html
node, npm, instal protractor globalno, i selenium webdriver
npm install protractor -g
webdriver-manager update

exports.config() lokacija testa i url
describe() it() ptor.get() toBe() expect()
---------------------------------
//72 - Testing Controllers With Dependencies //mora pogledas kod u skriptu natenane
testiranje ugnjezdenih kontrolera
spec je if() jedan test ili asert
---------------------------------
//73 - Accessing Data in HTML //dobre i lose prakse
servisi - globalne promenljive na pravi nacin, reupotrebljive
app.run() je load - run hendler, kontroler umesto toga
NekiCtrl as ctrl1 , ctrl1.fja(), this umesto $scope u kontroler
---------------------------------
//74 - Test Simple Binding With Protractor, kratak trivijal
protraktor ima selektore za html, by.id("button1")
by.binding("messageText") //za angular
button.click()...trivijal
---------------------------------
//75 - $q.all //dobar asinhroni primer
all([promise1, promise2, promise3]).then() // izvrsava ih redosledom kojim su navedeni
asinhrono - paralelno, neblokirajuce, nesekvencijalno
---------------------------------
//76 - Inject Jasmine Spies With $provide, kratak
mocking provajdera servisa
ubacio polje podataka
---------------------------------
//77 - $interval, kratak
kreiranje promisa sa tajmerom
ima ukupni interval i korak, success, errorr i notify hendlere
---------------------------------
//78 - Testing With Protractor Page Objects, trivijal
this.nesto - obicni atributi klase
enkapsulirao stranu u page klasu, poseban fajl, pa njene fje zove u if() testa, svaku stranu u poseban page obj, citljiviji testovi
pokrece test sa protractor test/protractor.conf.js
skripta mu je index.spec.js
---------------------------------
//79 - Application Wiring - JQuery vs AngularJS//opisni
angular nema selektore nego provide data i api (attr i fje)
ng-show, ng-hide je show() hide() iz jqueryja
vise html i attr nego js
---------------------------------
//80 - Using $resource for Data Models//nije spominjao $resource
pravi crud
---------------------------------
//81 - Custom Jasmine Matchers, kratak
toBbeTypeOf() da zameni typeof operator u if() asertima
---------------------------------
//82 - Accessing Scope from The Console//inspektor u chrome, kratak, nije los
$0 je selektovani el u inspektoru
var $scope = angular.element($0).scope()
$scope.yourName = "Bill";
$scope.apply();//za digest
---------------------------------
//83 - Accessing Services from Console
//uhvati servise
var $injector = angular.element($0).injector();
var Projects = $injector.get("Projects");
Projects.$add({...});
---------------------------------
//84 - HTML with ngSanitize and SCE, html escaping kao {{{}}} u handlebars//za xss
import u js src=angular-sanitize.js i u module["ngSanitize"]
$sce.trustAsHtml(html ovde)//iskljuci, default ukljuceno
---------------------------------
//85 - Build a Debug Directive //odlicno za debug bindinga
ng-non-bindable="" da izadju templejti {{}}//ugradjena direktiva
priority: redosled izvrsavanja direktiva
pravi direktivu da ispise i binding i renderovano
---------------------------------
//86 - Create a Model Base Class//na brzinu
dodaje fju u base klasu bzvz nesto
---------------------------------
//87 - Directive with Transcluded Elements
link() - link faza
compile() - compile faza
transclude: "element"

ubaci direktivu u kontejner
link(scope, element, attrs, ctrl, transclude)
---------------------------------
//88 - File Uploads //nije lose
zakljucak - ima gotovih biblioteka za ang upload
---------------------------------
//89 - Model Caching
da se updateuje repliciran objekat svuda, MORA biti ista js referenca
napravio klasu gde kesira i dodeljuje id objektima
---------------------------------
//90 - Create a Scope Decorator, slozeno
factory("decorator",["AfterMethod"] ...) {return function...}
_createWrapper(fn) lokalna klasa, zove apply() tj invoke na prosledjenoj fji
newFn i newFn.toString()
ddescribe - poziva samo taj test
da prodje testove
---------------------------------
//91 - Add Caching to the Model Base Class//ima dosta nabacano da se tumaci
factory("BCBase",["BCCache", function(Cache){ functionBase(attributes){...}}])//injectovao BCCache
BCBase ima privatne _constructor, _prototype, cache(), _constructor.new i .cached
//gledaj sta pise u if("") testovima opisno
definise i .factory("BCCache", function(){function Cache(){...privateVariable() 3 overloada}})
koristi _ objekat neki
kreira ga kao factory servis da bi mogo da ga injektuje gde oce kao arg
u cinfig() postavi stanje servisu
---------------------------------
//92 - Build Your Own ng-controller Directive, kratak, pros
directive("customController", function(){ return {scope: true, controller: "@", priority:500}})// i to je ng-controller
---------------------------------
//93 - Compile Pre and Post Link
redosled pozivanja controller, pre-link i post-link fja ugnjezdenih direktiva
compile ka unutra, pa controller, pre-link, post-link
---------------------------------
//94 - Rails Todo API Part 1//rubi samo bzvz
---------------------------------
//95 - Rails Todo API Part 2
2. polovina malo angular bzvz
---------------------------------
//96 - Advanced Filtering with the Filter Filter, nije los
poenta ljudi prave custom filtere, a ne koriste ugradjeni u potpunosti
filter:{name: filter.name}:true (doslovno) ili max njega definise kao lokal fju u controller
ime filtera, property scopea po kome, komparator - bool fleg
---------------------------------
//97 - Get Started with Firebase and AngularFire//live sinkuje data sa serverom//realtime
impoert firebase u <script src=> i module["firebase"]
firebase url kao constant service(), vraca lokal fje iz servisa, expouse//da bi ga menjao na 1 mestu
firebase live sinkuje sa serverom podatke
---------------------------------
//98 - Updating Real-Time Data with Firebase Forge//forge je baza sajt za firebase
samo pokazao sajt, trivijal
---------------------------------
//99 - Firebase Event Handling
$on("loaded, change") - firebaseov, ne angular event provider, suprotno $off()
firebase loaduje podatke inkrementalno 1 po 1
---------------------------------
//100 - Using $anchorScroll, lib za scroll, trivijal, kratak
disableAutoScrolling() iskljuci default (brows link), pa $anchorScroll() ukljuci gde hoces (link)
tinycollor lib za dugine boje
---------------------------------
//101 - Write Your First Directive//direktiva od nule, pocetni hello world//redosled klipova?
direktiva nasledjuje scope iz kontrolera
return function(in scope, in element, in attrs)
---------------------------------
//102 - First Step - Adding Angular to the HTML Page//trivijal hello world
---------------------------------
//103 - Firebase Basic Authentication Part 1
loginService na firebaseio//bzvz
---------------------------------
//104 - JavaScript Function Scope and $scope//dobar pros
$scope exposuje promenljive iz kontrolera u html
$scope kreira javni interfejs koji exposujes htmlu
ako ne nadje promenljivu kod sebe trazi u parent, html odredjuje kako su poredjani scopeovi
ng-app je root scope $rootScope, ng-controller dete itd
ako kontroler nema svoj scope uzima rootScope
kontrola sme da zavisi od druge koja je manje verovatno da se menja
html dom se cesto menja
scope u js ne bi trebalo da zavisi od html
scope u angular je isto kao u js
---------------------------------
//105 - Firebase Basic Authentication Part 2, necu da gledam
---------------------------------
//106 - Search Directive with Rails, dosta koda komentarise
query server, search, da ne zove na svaki keyup
anonimna fja ili clojure, lokalno definisana fja koja se koristi samo 1 - I TO JE SVE, da ne pises ime i prenosis
direktiva a ne filter
search - prop po kome a ne ceo obj
---------------------------------
//prouci javascript paterne adi osmani, da bi razumeo angular module itd
po izgledu poziva fje da zakljucis da je asinhrona? vraca promise objekat
njena asinhronost se zasniva na asinhronij fji koju ona poziva, vraper oko asinh ajax ili dr objekta (native) koji vec vraca promise, izgleda da da
js 1 nit nema niceg asinhronog, asinh je red niti
ako je asinhrona moras da obrnes poziv, tj continuation
kontroler, direktiva, servis... ima koje fje, koje propertije, vraca sta?//decidno izuci//klase obicne mapirane na html
direktiva vraca obj{controller:, link: , scope:...}
---------------------------------
//107 - Basic Animation with Greensock's TweenMax, animiran meni
koristi TweenLite.to({options objekat}), trivijal
---------------------------------
//108 - 3d Animations with Greensock TweenLite
kontroler direktive?
app.animation() vraca obj sa dve fje, fja u js je objekat sa 1 fjom plus default props and fje
3d grafika transformacija
args za pozive samo objasnjava
---------------------------------
//109 - Multiple HTTP Requests with $q//nije los
$q za promise
$q.all([promise1, promise2, ... redosled]).then() vraca objedinjen promise
.all() kao threadJoin() ceka svi da se izvrse, objedini u 1 promise po zadatom redosledu
---------------------------------
//110 - Firebase Data Relationships
denormalised, sve 1 objekat
foreighn key
budzi rucno reference u js u nestruktuirano skladiste

---------------------------------
//111 - Refactor The Model Base Class with Mixins
["BCCache", function(Cache){ ... }] u Cache arg ubacuje provider iz stringa "BCCache", zapazi trivijalno, da nije slozenije nego sto je, minifikacija...
samo string notacija//TRIVIJAL
---------------------------------
//Build Your own Simplified AngularJS in 200 Lines of JavaScript - Minko Gechev
//http://blog.mgechev.com/2015/03/09/build-learn-your-own-light-lightweight-angularjs/

scope.$digest(); je invalidate() ili refresh() da propagira promenu na sve zavisne direktive u domu, $apply() u angularu
ngl-bind/ngl-model //ng-bind binduje iz modela na element//watch na promenljivu
//ng-model binduje iz elementa na scope.promenlj1 kontrolera//watch na expression
//odvojeni smerovi 2-smernog bindinga//ne
controller direktiva kreira novi scope
$eval() izvrsava kod u markapu, exxpressioni
kontroleri su handler klase sa vise handler metoda, enkapsulacija mogucih interakcija na elementu
scope je kontekst templatea, exposovane promen i fje htmlu, expresioni i args direktiva
direktive - enkapsulacije dom manipulisanja //separation of conserns
js objekti zamena za oop tipove i klase, oop impl portovana u js, clojure necitljive definicije
filteri- formatiranje podataka
sva ostala logika ide u servise // globalne promenljive, objekti koji mogu da se DI kao args
compiler, provider, scope - separation of conserns, imaju interfejse i odgovornosti
zavisnosti mogu biti cirkularne - graf sa ciklusima - circular dependency - take care of them by using topological sort
scope-i imaju strukturu stabla
//ngl-bind jednosm binding promenlj na expression
//ng-model dvosmerni binding - ima 2 fje el.onkeyup = function(){...} i scope.$watch(exp, function(){...})
dfs se pozove za sve susedne roota, sve susedne svakog ispod roota na dole, zato izgleda na gore, prvo izvrsi sve susede lista
separaton of conserns, single responsibilities, dekompozicija problema, divide and conquer
provider - graf globalnih objekata
global dependencies - factory, service, filter
local dependencies - $scope, $delegate
------------------------
minko gecev
kad je modularna aplikacija rastom velicina applikacije kompleksnost ne raste, ili sporije, kad nije raste eksponencijalno
pogledaj GRASP
.call() u js poziva invokuje fju apply() isto
------------------------
//AngularJS in Patterns
ucenje:
1. napravi ga sam
2. na osnovu onoga sto vec sam znas kako radi
application resources (data, templates, scripts, styles) should be loaded with the initial request or better - the information and resources should be loaded on demand.
//controller
kontroler - klasa sa hendlerima, di zavisnosti args, imaju scope koji je model za partial expressione, to je view-model scope kontrolera, iskesirani model
//scope
In AngularJS scope is a JavaScript object, which is exposed to the partials
fje scopea mogu da se pozivaju iz htmla ili js-a gde ima referenca na scope
scope povezani u prototypical chain, child moze da zove fje parenta (ugnjezdeni kontroleri), osim isolated scope
__proto__ je ceo this prototype propertija
//direktive
where all DOM manipulations should be placed
ima ime i logiku, ime se poziva u html
postLink: fja logika direktive
//filteri
enkapsulacija formatiranja podataka
pozivaju se iz htmla, mozi i iz kontrolera, direktiva, servisa, drugih filtera, prenose se DI kao arg
helper iz handlebarsa
//servisi
logika koja se ponavlja u kontrolerima ide u servis
prenose se kao DI arg u komponente koje podrzavaju DI: kotroler, servvisi, filter, direktive
-----------
//Services
//Singleton
cache is a singleton manager
//misko havery tekst blog 
ne poziva getInstance() u metodi nego prima singleton obj kao arg u konstruktor, pa prosledis mock obj kao arg umesto getInstance() kome ne mozes nista//smesno trivijal
ne instanciras nista (singletone ili konstruktore) de ti treba, nego prenosis kao arg (u constr u laravel, fji generalno)
IoC kontrola dobijanja objekta je u tvojoj klasi, a ne u konstruktoru potrebnog objekta//stackoverflow http://stackoverflow.com/questions/3058/what-is-inversion-of-control
//non ioc
public class TextEditor
{
    private SpellChecker checker;
    public TextEditor()
    {
        this.checker = new SpellChecker();//ovaj odredjuje sta ces da dobijes
    }
}
//ioc
public class TextEditor
{
    private ISpellChecker checker;
    public TextEditor(ISpellChecker checker)
    {
        this.checker = checker;
    }
}
//http://stackoverflow.com/questions/6550700/inversion-of-control-vs-dependency-injection
IoC is a generic term meaning rather than having the application call the methods in a framework, the framework calls implementations provided by the application.//pravis zavisnost pozivom konstruktora ili instanciranjem frmaeworka da bi zvao njeg metode//framework daje interfejs (nasledjivanje ili eventi) koje implement, pa on poziva ili emituje
DI is a form of IoC, where implementations are passed into an object through constructors/setters/service look-ups, which the object will 'depend' on in order to behave correctly.//as args
//mef anotacije u NET

//Factory Method
u oop provideri su interfejsi, u ang su $provider
provider je object which factory method, $get()
factory - izbor klase koja se instancirana izmestena iz tvog koda u factory klasu, enkapsulirana selekcija jednom, preneses kao string arg koji constr zoves npr
tvoj kod bi morao da linkuje sve klase, ovako samo factory klasu
fn znaci taj arg je fja, kao tip delegat
fn.apply(this1, args) poziv delegata
provider je factory klasa, ceo di je factory interfejs
config() sluzi da definises novi provider. Provider is an object, which has a method called $get//primer ilustruje kreiranje custom providera
anotate() parsuje providr iz argumenta

izolacija u factory klasu benefits:
The most appropriate moment, when the component needs to be instantiated
Resolving all the dependencies required by the component
The number of instances the given component is allowed to have (for services and filters only a single one but multiple for the controllers)
//Decorator
klasa kao arg svog konstrukt prima interfejs koji implementira, pa moze da se ugnjezdi, laravel
is a design pattern that allows behavior to be added to an individual object, either statically or dynamically, without affecting the behavior of other objects from the same class (instanci)

$provide.decorator()//ugradjena fja za dekorisanje klasa
$provide.decorator('klasaKojuDekorises', function (klasaKojuDekorises) {}//dobar primer i lak, ocigledan//dekorisao servis
barBackup.apply($delegate, arguments);//apply() i call() sluzi za pozivanje delegata

myModule.factory('klasa', function () {})//factory je definisanje klase koju mozes da di
useful to modify the functionality of third party services//dobar
----------------------
//uml
sve zavisnosti klasa mora da linkuje zavisnu (include)
zavisnost(asocijacija) - koristi tip negde, arg ili lokalna (linkuje klasu)//A----->B
agregacija - atribut klase		//------<>
agregacija, kompozicija su podtipovi binarne asocijacije
relacije na:
1.nivo instance, objekta
2. nivo klase (generalizac, realizacija(impl interfejsa))
pitaj na stackoverflow zasto crtaju asocijaciju, a u kodu pisu agregaciju
interfejs vise implementacija, i primorava klasu da ima fje, nema dublji smisao od toga
----------------------
//Facade
sakrivanje kompleksne funkcionalnosti iza jednostavnog interfejsa
Each time you want to provide higher level (apstrakcije) API to given functionality you practically create a facade.//ajax xmlhttprequest i jquery
sakrivanje kompleksnosti, i prekonfiguracija default
primenjeno u angularu $http i $resource

//Proxy, nista specijalno obradjeno
proxy je klasa koja functioning as an interface to something else. The proxy could interface to anything: a network connection, a large object in memory, a file//posrednik
proxi klasa i original klasa implementiraju isti interfejs 
var User = $resource('/users/:id');//$resource je neki $http
var user = User.get({ id: 42 });
console.log(user); //{}
//inicijalno prazan proxy objekat (i cuva referencu) koji popuni tek kad stigne rezult preko mreze

//Active Record, nista specijalno obradjeno
objekat koji ima podatke, komunicir sa bazom (load(), save()) i logiku
crud je mrezni interfejs prema bazi
$resource u ang, crud, rest, preko http

//Intercepting Filters
pipeline model, pre i posle
http filteri iz jee
u angular $httpProvider.interceptors.push(request:, response: callbackovi)

//Directives
//Composite
composite, stablo, html, transclude rekurzivno nesto malo
MVC is nothing more than combination of:
								Strategy
								Composite
								Observer
laravel migracije up() down() command obrazac, transakcije
//Interpreter
interpreter pattern is a design pattern that specifies how to evaluate sentences in a language
u angularu $parse servis
svodjenje angularovih operatora na javascriptove, expresioni u templejtima - DSL - jezik konkr aplikacije

//Template View, templejti, server pages, trivijal
handlebars, medjuformat svoj, nit html nit js
AngularJS templates su validan html

//Scope
//Observer
subject objekat cuva listu posmatraca(observera) i obavestava ih o promenama (pozivajuci neku njihovu metodu)
$scope.$on, $scope.$emit (samo na gore, deci), $scope.$broadcast (na gore i na dole u scope-ovima)

//Chain of Responsibilities
scope - hijerarhija - stablo, ili izolovano
svaki scope moze da hendluje, propusti ili stopira event
izolovana ne nasledjuju prototipski ali imaju $parent referencu

//Command
object encapsulate all the information needed to call a method at a later time
command - transakcije stanja aplikacije
ng-bind - promena modela reflektuje u html, 1-smerno
ng-model - dvosmerno
$scope.$watch
watcher object - command
$scope - Client
$digest - invoker
listener - receiver

//Page Controller
object that handles a request for a specific page
duplicate behavior between the different pages (footers, headers, session...) page controllers can form a hierarchy
common actions can be isolated to the base controllers
kontroler hendluje evente, $route hendluje zahteve, ng-view direktiva rendering

//Module Pattern, js pattern
main goal is to provide encapsulation and privacy
defining services in AngularJS. Using this pattern we can achieve privacy
building a reusable library


//Data Mapper, nema veze sa ovim
goal of the pattern is to keep the model i baza independent of each other, dekapling
na server RESTful API - $resource Active Record 
User service zove 2 api odjednom prilagodjava modelu aplikacije

//Observer Pattern as an External Service
pattern to communicate between 2 controllers that use the same model but are not connected in anyway
ObserverExample.$inject= ['ObserverService', '$timeout'];//$inject prosledjuje providere na argumente
ObserverService.detachByEvent('let_me_know')//izvrsi samo 1, odjavi se posle 1
prikazan poziv, implementacija je u biblioteci


//https://stackoverflow.com/questions/13512949/why-would-one-use-the-publish-subscribe-pattern-in-js-jquery
good idea to use publish/subscribe for the Model-View communication
publish subscribe interfejs samo te 2 metode
autonomni objekti koji slusaju umesto da se neko brine o njima (cuva njihove refer)
tviter interfejs od blokova, kolaz, koji mozes da reuse u drugoj app, publish event kad uradis nesto, reusable logicki moduli isto kao bootstrap css
iife svuda klase
drawbacks - previse evenata, unit testing ne zna se redosled


//https://github.com/mgechev/angularjs-style-guide //odlicno prakticno//na dole

//Directory structure

2 main approaches:
	1. Creating high-level divisions by component types and lower-level divisions by functionality.//logicno
	2. Creating high-level divisions by functionality and lower-level divisions by component types//modularno reusable

u istom folderu i test fajl cache1.spec.js//spec je test fajl	
The app.js file should contain route definitions, configuration and/or manual bootstrap (if required).	
jedna komponenta po fajlu
1. npr
.
├── app								
│   ├── app.js
│   ├── controllers
│   │   ├── home
│   │   │   ├── FirstCtrl.js
│   │   │   └── SecondCtrl.js
│   │   └── about
│   │       └── ThirdCtrl.js
│   ├── directives
│   │   ├── home
│   │   │   └── directive1.js
│   │   └── about
│   │       ├── directive2.js
│   │       └── directive3.js
│   ├── filters
│   │   ├── home
│   │   └── about
│   └── services
│       ├── CommonService.js
│       ├── cache
│       │   ├── Cache1.js
│       │   ├── Cache2.js
│		│	└── cache1.spec.js //test
│       └── models
│           ├── Model1.js
│           └── Model2.js
├── partials
├── lib
└── test

//css
app
└── directives
    ├── directive1
    │   ├── directive1.html
    │   ├── directive1.js
    │   └── directive1.sass
    └── directive2
        ├── directive2.html
        ├── directive2.js
        └── directive2.sass
		
		
//direktive
Create an isolated scope when you develop reusable components

//servisi
definises obicnu lokalnu fju, pa je dodelis, tj registrujes kao kontroler ili servis, ne mora clojure
function MainCtrl($scope, User) {
  $scope.user = new User('foo', 42);
}
module.controller('MainCtrl', MainCtrl);//registrujes
function User(name, age) {//obicna definicija
  this.name = name;
  this.age = age;
}
module.factory('User', function () {//registrujes
  return User;
});
//Encapsulate all the business logic in services. Prefer using it as your model
servis obicna klasa, module pattern return public obj
clojure fja je samo scope

If given service requires configuration define the service as provider and configure it in the config(), ima $get() da resolvuje registrovani servis

//performance
$digest obilazi celo dom stablo, evaluira expressions - templejte
-------------
//Introduction to Angular 2 by Minko Gechev slajdovi - https://speakerdeck.com/mgechev/introduction-to-angular-2

angular 2 recap:
1. typescript
	strong and static typed superset
2. component based (web components web standard) - direktive podskup
3. one-way data flow
4. novi ruter
5. moduli

ES5 recap:
property deskriptor get() set() _name
klase sintaksne preko prototipa
arrow functions - lambde (ulaz_args => izlaz{})
let - block scope
promises - kontejneri za future values
moduli - lang level
module loaders

ES6:
decorators - anotacije modify properties at design time

TypeScript recap:
static tipovi
	lakse greske, ide, brze
interfejsi
type inference - ?
import postojecih biblioteka
type definit ref ?

angular 2:
anotacije
no front end state, event based, difference based, dirty checking

controler as koristis this umesto $scope
-----------------------
//Functional programming with JavaScript http://blog.mgechev.com/2013/01/21/functional-programming-with-javascript/

//prednosti funkc program
safe fje, izolovane i idepotentne
Concurrency - data is immutable
Unit testing - nema stanja
Debugging - Simple stack trace 
theoretical base

//Anonymous functions
The anonymous function is a function which is defined without being bound to an identifier

//High-order functions
High-order functions are functions which accepts functions as arguments or returns functions.

//Closures
scope, lokalna fja - ugnjezdena

//Managing the state (Monads)
?

//Schönfinkelization (or simply Curring) - promenljivi broj argumenata - overload
rekurzivno se poziva sa jednim novim arg, kad dodje do zadnjeg vrati rezultat
bind(this, args) se razlikuje od apply() sto vraca tu fju, a ne poziva je - invoke//IE9+

//Pattern matching
?
za razlicite argumente zoves razlicite fje - (implement)
-----------------------
//Aspect-Oriented Programming with AngularJS  http://blog.mgechev.com/2013/08/07/aspect-oriented-programming-with-javascript-angularjs/

cross-cutting concerns cannot be cleanly decomposed from the rest of the system, in either scattering (code duplication), tangling (significant dependencies between systems), or both.
------------------------
//The magic of $resource (or simply a client-side Active Record) http://blog.mgechev.com/2014/02/05/angularjs-resource-active-record-http/

$apply gura promenljivu u scope kao da dolazi iz expressiona, trigeruje update, rerender
u $apply guras finction(){}, prosledjujes definiciju $apply kroz argument prakticno
kad prosledjujes fju kao arg znaci da ce cela ili deo implem fje biti to sto si prosledio, mini-override
$digest updateuje samo na current scope, $apply na rootScope i svu decu
$scope.$apply(function () {
  users.push({
    name: 'foo'
  });
});
//vraca promise u okviru users objekta, polje je i objekat, probao u konzolu//promise je objekat sa then() i always() fjama
User.query = function () {
   var users = [];
   users.$promise = db.process('SELECT * FROM User')
     .then(function (collection) {
        collection.forEach(function (user) {
          users.push(user);
        });
      });
   return users;
};

var User = $resource('/users/:id');//User sad ima user.query() = load() user.save()

//ruta .when('/users/:userid', {...
------------------------------
//Understanding Angular’s $apply() and $digest() http://www.sitepoint.com/understanding-angulars-apply-digest/

When the $digest cycle starts, it fires each of the watchers. 
e.g. ng-model, $timeout, etc) and automatically trigger a $digest, za ng-click zoves rucno
Angular doesn’t directly call $digest(). Instead, it calls $scope.$apply(), which in turn calls $rootScope.$digest()
When Do You Call $apply() Manually? if you change any model outside of the Angular context//van angul direktiva, nemaju watchere
$scope.$apply(function() {$scope.message = 'Fetched after 3 seconds'; })//dodaje watcher na $scope.message 
you should use $timeout service whenever possible which is setTimeout() with automatic $apply()

//razlika izmedju $scope.$apply() i $scope.$apply(function(){})
$apply(), the function call is wrapped inside a try...catch block, and any exceptions that occur will be passed to the $exceptionHandler 

What if a listener function itself changed a scope model?
So, the $digest cycle keeps looping until there are no more model changes, or it hits the max loop count of 10. It’s always good to stay idempotent and try to minimize model changes inside the listener functions.
minimum 2 puta, once more to make sure the models are stable and there are no changes.

The most important thing to keep in mind is whether or not Angular can detect your changes. If it cannot, then you must call $apply() manually. 
$digest will only fire watchers on the scope from which it is called.
-----------------------
//What I get from the JavaScript MV* frameworks - http://blog.mgechev.com/2014/02/12/what-i-get-from-the-javascript-mv-mvw-frameworks/

Reusability
	Boilerplates
	Reuse of your own code
Level of abstraction
Testability
Implicit conventions

//3 nacina kreiranja objekta u js
var obj = { foo: 42 };

//or the module pattern

var obj = (function () {
  var bar = 42;
  return {
    foo: bar
  };
}());

//or constructor function

function Foo() {
  this.foo = 42;
}
var obj = new Foo();
--------------------------
//Aspect-Oriented Programming in JavaScript 2015 http://blog.mgechev.com/2015/07/29/aspect-oriented-programming-javascript-aop-js/

OO design je decomposition of the problem, objekti komuniciraju

---------------------------
//Boost the Performance of an AngularJS Application Using Immutable Data - http://blog.mgechev.com/2015/03/02/immutability-in-angularjs-immutablejs/

Immutable je objekat sa fjama za strukture Immutable.List([1, 2, 3]);

apstrakcija - izolacija relevantnog
----------------------------
//Angular2 - First Impressions http://blog.mgechev.com/2015/04/06/angular2-first-impressions/

3 vrste direktiva:

1. component - vidget, deo stranice, 1 po elem
2. viewport - parent direktiva..., 1 po elementu
3. decorator - jednostavna direktiva za ponasanje, vise po elementu

//Angular2 has no Controllers
removal of the controllers - premestteni u direktivu da postane reusable

//No Two-Way data-binding
flux

//WebComponents
razlika izmedju biblioteke i frameworka

library - a collection of functions which are useful when writing web apps. Your code is in charge and it calls into the library when it sees fit. E.g., jQuery. 
frameworks - a particular implementation of a web application, where your code fills in the details. The framework is in charge and it calls into your code when it needs something app specific. 

//No more $scope
Instead of binding to properties in the scope inside our templates, we directly bind to properties of our "components".

//Errors in the Template Expressions compile time, i18n jezici, Ultra Fast Change Detection, Filters, Improved DI

//Conclusion
not backward compatible with AngularJS 1.x
unidirectional data flow
takes advantage of some of the web components APIs
You can use some of the libraries in  non-AngularJS projects


//Flux in Depth. 1 Overview and Components. http://blog.mgechev.com/2015/05/15/flux-in-depth-overview-components/
flux je mikroarhitektura
Dispatcher - js objekt, hashtabela evenata i callbackova, singlton
Action - jednosmeran
Unidirectional Data Flow - more coherent and less coupled
Stateless Components - funkcionalno program, functions as black boxes, which accept input and return output, pure and impure function (spoljasnje deljivo stanje)
pure fja - ne zavisi od i ne mnenja deljive objekte

//Immutable Data
1. Use wrappers around the standard JavaScript primitives, which makes the data immutable (for example with library like Immutable.js) let list = new Immutable.List([1, 2])
2. Use ES5 Object.freeze - svaki item
Object.freeze doesnt do deep freeze of the objects, samo 1. nivo


//Flux in Depth. Store and Network Communication 2 http://blog.mgechev.com/2015/07/18/flux-in-depth-store-network-communication-services/

application cant be completely stateless. We have at least business data, UI state and an application configuration

//jafar husain async programing in es7 video https://www.youtube.com/watch?v=lil4YCCXRYc
funkcije block - pull(moze try catch) ili wait - push (callbacks piramide)//promenljiva izvlaci vrednost iz fje
mis ne radi dok se ne izvrsi blokirajuca fja http npr
ideja da sinhr i asinhr se pise isto pa prosledis kao param koju opciju od te 2 hoces, pull i push je isto

generator fje - yield (vrati ali radi i dalje) - je fja koja moze da return vise vrednosti (vise puta)//trivijal pros

function* getNumbers(){ //function* je generator
	yield 32;
	yield 76;
	return 56;
}
//vraca iterator (obican js objekt) koji ima next() i vraca tuple obj sa rezult yield i bool da li ima jos

var iterator = getNumbers();
console.log(iterator.next());
//{value:32, done:false}
console.log(iterator.next());
//76 etc...done:true na kraju

generator koristi push interno i daje ti uobicajenu pull sintaksu
izvlacis dok ne naidjes na done
iterator interfejs je kad imas .next() fju
generator je iterator + observer//mozes da guras vrednosti u njega kao arg kroz next generator.next(5) ili generator.throw("greska") ili generator.return(7)
3 stvari moze da vrati 1. vrednost, 2. done i 3. gresku - exception
2-smerna komunikacija izm 2 fje - generator
asinhrona iteracija - producer vraca promise objekte, consumer ih resolvuje i vraca generatoru kao argument, i tako do done:true kad consumer dobije promise koji ima konacnu vrednost

//producer
var symbol = yield getStockSymbol(name);
//consumer
var result = spawn(getStockPrice.bind(null, "filter"));//await prakticno//Task.js u es6
u es7 async fje u jezik await ,isto ovo impl, jeste iz C#
//to
//ideja podrska za block (nista) i await (wait ako da cekas) ravnopravni u jeziku, za push i pull
await ako cekas 1 vrednost
es6 kolekcije implementiraju iterable interfejs
for of ili for on petlje za push i pull, petlja na stream
//observable
observable interfejs, return i arg obrnuti od iterable

interface Iterable {
	Generator iterator(void);
}
interface Observable {
	void observer(Generator);
}
//razlika je da i je producer ili consumer u kontroli
generator je objekat sa 3 fje, callbacks next(), throw(0 i return()
consumer prosledi generator produceru on mu vrati rezultate
nums().observer({
	next(v) {console.log(v);},
	return(v) {console.log(v);},
	throw(v) {console.error(v);},
});
podrska za push izvore podataka
async function vraca Promise
function* vraca Iterator
async function* vraca Observable

//Flux in Depth nastavak
//Chain of Responsibility pattern - dinamicko dodavanje procesnih objekata koji vrse obradu i ili prosledjuju dalje
event bubbling i capturing su ovaj pattern
i aplikacija je stablo
have separation between the UI state and the model, odvojena stanja za podkomponente
//ili uradi ili prosledi i ne pipa, uvek samo 1 obradjuje i zavrsi
//postoji lista objekata i bira se 1 objekat iz liste koji ce da izvrsi obradu
---------------
//uopste
sta god da je ima jednostavnu minimalnu logiku
da izbacis source fje ides fja.toSource() ili toString()
---------------
selekcija i projekcija su iste nad bazama i u strukturama (projekcija - kolone, selekcija je filter)
flatten stablo u array
reducing because we reduce many values to a single value - max elem niza

//Code Reusability in Angular minko gechev https://speakerdeck.com/mgechev/code-reusability-in-angular
sta tacno radi Object.create(arg) i sta prima kao arg?
clone maltene
//classical inheritance, pros, prototip parenta dodajes u prototip childa da imaju isti, sve fje u prototip
function Parent(age){
	this.age = age;
}
Parent.prototype.getAge = function() {
	return this.age;
}

function Child(name, age){
	Parent.call(this, age);//i imas super() ovde
	this.name = name;
}

Child.prototype = Object.create(Parent.prototype);
Child.prototype.getName = function() {
	return this.name;
}

//prototypal inheritance, pros, obj literal pa to u obj.create() pa dodajes propertije
var Parent = {
	age: 27,
	getAge: function() {
	  return this.age;
	}
}
var Child = Object.create(Parent);
Child.name = "foobar";
Child.getName = function() {
	return this.name;
}

//rp5 primer
ReportsNavView.prototype = new AtsBaseView();//ReportsNavView mora da bude klasa, tj tipa function, tj ReportsNavView=function(){};
ReportsNavView.prototype.constructor = ReportsNavView; //u konstruktor dodeli bilo koju fju
Klasa.prototype.nesto je referenca na scope za definiciju klase gde si imao this, mozes da dodajes u this van klase, zato se vidi u svim instancama

----------
AOP - aspect oriented prog - separation of cross-cutting concerns - razdvajanje nerazdvojivog oop-om
//Optimize Yourself 5 Key Traits of High Performing Humans – Sylvana Rochet 
1. radoznao 2. iskren 3. fokusiran 4. svestan sebe i drugih 5. selektivan
softver postaje kompleksan kad jedna klasa treba da radi dve stvari - single responsibility
sve asinhrono je proizvodjac/potrosac u sustini
----------------
//React
komponente - direktiva, defin klasu koju pozivas u html kao tag npr
JSX je javescript sa html-om
ugnjezdeni html tagovi pregledniji od js fja
//osnova reacta uvek 2:
1. React.createClass({render:...}) //sa hendlerima predefinises
2. React.render(<mojaKomponenta/>, document.body) //a ispod poziv 
valueLink - 2 way binding, inace sa update()
react dev tools chrome, mali, inspektor za html atribute, event listenere
//flux
view nema logiku - zove fju actiona, dispatcher sadrzi red akcija, hendluje race, store slusa akcije na koje je pretplacen
merge = extend - angular ili jQuery
Dispatcher.prototype = merge(Dispatcher.prototype ,{prosirenje propert...})
.property znaci dodajes u sve instance (u klasu), ne samo tu//pros
//32 react in 7 minutes
<nekaKompon attrArg={arg1Obj} /> prosledjujes args u komponentu
funkcija je scope, this
izvuci sve moguce react hendlere
mixin:[ReactFireMixin] linkujes firebase.js lib u komponentu, kao modul u angular//objekat koji mixinujes u svoju komponentu
new firebase objekat je proizvodjac i potrosac
//39 - Setting up a React Playground Dev Environment, odlican js osnove ide i git i npm//odlican sve za js workflow
npm init kreira paket package.json
instalira neko sa npm install
instaliraj webstorm mozda
//40 - Connecting to a Reflux Store in React
Reflux.createStore(...) neki?, koristi fje objekta koji je preneo kroz mixin[], to demonstrira
//41 - Reflux - Loading data with Superagent
scope react komponente je this.state.mojProp, inicijalno postavljas sa getInitialState()
//42 - Static Methods in React
var App = React.createClass({ statics: { add: function(x, y) { return x + y;}}, ...})
add() se vidi samo u App.add(), ne u var a = React.render(...), niti u this.add() unutra
//43 - Custom propType validation
propertiji se prosledjuju iz render kao html attrs, this.props, mogu da se validiraju u propTypes: function(props, propName, componentName){ ovde validacija}
----------------------
//WebRTC chat with React.js  http://blog.mgechev.com/2014/09/03/webrtc-peer-to-peer-chat-with-react/
razbijes aplikaciju u stablo webkomponenti (ugnjezdene)
webrtc - p2p u browseru
jsbin nije sam izmislio vec video da ga koriste u drugim klipovima
validacija u viewmodelu da ne moras da je ponavljas za svaki view
'use strict' exceptioni i scope drugaciji...
----------------------
//staticke i instanc fje u js

//Constructor
var Person = function (name, age){ //this.constructor je ref na Person fju
    //Public methods
    this.sayHi = function(){
        alert('hello');
    }
}
// A static method; 
//this method only exists on the class and doesn't exist on child objects //na toj instanci//ne vidi se u drugim instancama napravljenim sa new
Person.sayName = function() {
    alert("I am a Person object ;)");//vidi se this, kako je staticka...?//ne treba ti new da bi je zvao
};

// An instance method; 
// All Person objects will have this method //na svim instancama
Person.prototype.setName = function(nameIn) {
    this.name = nameIn;  
}
----------------------
//beleske desktop

namespace pattern - da dobijes tacke.nesto.nesto
object literal ili fja sa stringovima

modulepatern, privatno, javno
return javno, lokalno privatno, pros
fju mozes iife da zoves var x = (function(){odma})();
return vraca interfejs
-------------
prvo trebas da imas procenu stvarne kompleksnosti problema i sta realno postizes time, da nemas preuvelicane

mongodb, premestis strane kljuceve (ogranicenja, spojeve) u kod, a ne u bazu, u bazi samo podaci, onda nemas spojeve nad bazom, mozes da skaliras bazu
-------------------------
//napravi task listu, ili todo app android pa idi po njoj
-------------------------
services - najobicnije klase koje mogu da se ubace gde treba sa DI
-----------------------


------------------------
------------------------
polymer vs angular direktiva - direktiva nema css
------------------------
googlaj cheatshetove za sve, angular, react
------------------------
//Angular 2 Hot Loader  http://blog.mgechev.com/2015/10/26/angular2-hot-loader-hot-loading-tooling/
watch nad fajlovima, osvezavanje u browseru po ugledu na redux//iz supljeg u prazno, lov u mutnom, slihtara
ciste komponente bez deljivog stanja mozes da imas transkcije i versioning stanja aplikacije
-------------------------
virtual dom
kad view izmeni dom mora da nastavi da prati i pamti taj element za izmene
react detektuje razlike pa se ne pamti nista
monad - prosireni tip
scope highlight za npp potrazi
Nan je rezultat deljenja nulom
prosirenje tipa da i postojece fje budu podrzane
// wiki
monad is a structure that represents computations defined as sequences of steps

// http://stackoverflow.com/questions/2704652/monad-in-plain-english-for-the-oop-programmer-with-no-fp-background
implement methods that represent the three operations you need: 
1. turning a value into an amplified value, 
2. turning an amplified value into a value, 
3. and transforming a function on unamplified values into a function on amplified values.

analogno izvedena klasa sa svim fjama overajdovanim

SelectMany radi flatten za multidim niz, za jednodim niz je identican sa Select, koji je projekcija (neki propertiji itema i fja nad njima)
selekcija je range polja

Nullable<int> Z(int s) { return Bind(Y(s), X); }// bind je dif fja koja dodatno hendluje sta treba

// https://gist.github.com/briancavalier/3296186
// To be a Monad, it must provide at least:
// - A unit (aka return or mreturn) operation that creates a corresponding monadic value from a non-monadic value.
// - A bind operation that applies a function to a monadic value
----------------------
znaj core koncepte koje oni pokusavaju da implem, umesto da ih juris random po implement, znas coveka
cover leter custom za koga saljes, rezime genericki - biografija
gledaju tok misli
----------------------
//https://github.com/mgechev/angularjs-style-guide/blob/master/README-sr-lat.md

Kreirajte izlovoano područje kada razvijate direktive za višestruku upotrebu.
-----------------------
logika u servise https://ravened.files.wordpress.com/2014/08/architecture.png
servisi laravel - klase enkapsulir logiku plus DI, ako nema vise view moze obicne klase jer se koriste na jednom mestu, ne treba DI
-------------
win servisi linux daemoni - proces prilakodjen dugom izvrsav, podklasa, pitaj na stoverflow
--------------
https://github.com/oranda/libanius-play/blob/master/README.md
angular model ($scope) - povezuje kontroler i html view
ang servisi - pozivaju ajax kontrolere (ruter) na serveru, izmedju kontrolera i servera
ang kontroler poziva servise, a kao rezult dobija promise tj callback i razresava ih

app.factory() - factory DI kontejnera, konstruktor 
Abstracting business / data providing logic into services - logika u servise //http://nathanleclaire.com/blog/2014/01/04/5-smooth-angularjs-application-tips/

google slike - laravel,angular, react architecture diagram
react ima komplikov strukturu (flux arhitektura), jednosmerna, prouci yt npr
uci arhitekturu, a ne frameworke, arhitekt je samo jedna
i bugar prepricava druge clanke
https://facebook.github.io/react/blog/2014/10/17/community-roundup-23.html
---------------------------
jquery proxy() je za prosledjivanje this, tj bindovanje obj na this, kao call, apply, bind //za jquery instance
http://stackoverflow.com/questions/4986329/understanding-proxy-in-jquery
---------------------------
---------------------------
//The introduction to Reactive Programming you've been missing  https://gist.github.com/staltz/868e7e9bc2a7b8c1f754

//promise
A stream is a sequence of ongoing events ordered in time. It can emit three different things: a value (of some type), an error, or a "completed" signal
------
We capture these emitted events only asynchronously, by defining a function that will execute when a value is emitted, 
another function when an error is emitted, and another function when 'completed' is emitted. 

The "listening" to the stream is called subscribing. 
The functions we are defining are observers. 
The stream is the subject (or "observable") being observed. //Promise is an Observable//promise = stream = observable = RED, utupio
This is precisely the Observer Design Pattern.
------
immutability - fja nad streamom vraca sta vraca ali originalni stream ne menja po refer, (select() i arraymap() vraca new umesto foreach()
asinhroni stream - zna se koji eventi idu niz njega, ali ne i kad
-------
  clickStream: ---c----c--c----c------c-->
               vvvvv map(c becomes 1) vvvv
               ---1----1--1----1------1-->
               vvvvvvvvv scan(+) vvvvvvvvv
counterStream: ---1----2--3----4------5-->
//brojanje klikova - lejeri, map() pretvara evente u 1, scan() prihv rezult od map i sabira ih, agregira
//brojanje zdruzenih klikova - opet lejeri pajpovani

//Implementing a "Who to follow" suggestions box
Whenever a request event happens, it tells us two things: when and what.
The official terminology for a stream is "Observable"
------------
A Promise is simply an Observable with one single emitted value. Rx streams go beyond promises by allowing many returned values. (1 success callback, ovamo moze vise za razl uspesne ishode)
//umesto callbackhell - transforming and creating new streams out of others,
map(f), which takes each value of stream A, applies f() on it, and produces a value on stream B
-------------
A metastream is a stream where each emitted value is yet another stream. You can think of it as pointers:
flatMap() umesto map() - razresava promise, vraca json umesto promisea

1. //On startup, load accounts data from the API and display 3 suggestions
//dobijanje request streama
var requestStream = Rx.Observable.just('https://api.github.com/users');
//dobijanje response streama od request streama
var responseStream = requestStream
  .flatMap(function(requestUrl) {
    return Rx.Observable.fromPromise(jQuery.getJSON(requestUrl));
  });
//razresavanje response streama
responseStream.subscribe(function(response) {
  // render `response` to the DOM however you wish
});

2. //The refresh button

//merge() streams
stream A: ---a--------e-----o----->
stream B: -----B---C-----D-------->
          vvvvvvvvv merge vvvvvvvvv
          ---a-B---C--e--D--o----->
//pros		  
var requestStream = Rx.Observable.merge(
  requestOnRefreshStream, startupRequestStream
);
moze i stream3 = stream1.merge(stream2)
moze i startWith() 
-----------
model a suggestion as a stream - sve modelujes u stream
//STREAM JE RED FIFO
------------------
telling what something is by defining relationships between streams
pull based and force it to be push based var x = func()//x radi pull
var x = await func() // x ceka da func push
------------
hot observables such as mouse move events or stock tickers which are already producing values even before a subscription is active
Cold observables start running upon subscription, i.e., the observable sequence only starts pushing values to the observers when Subscribe is called
-------
The Reactive Extensions for JavaScript unifies both the world of Promises, callbacks as well as evented data such as DOM Input,
----------------------
//http://www.slideshare.net/nzakas/scalable-javascript-application-architecture/62-Application_Core_Jobs_Manage_module
samo ako znas arhitekturu aplikacije mozes da razumes, top-down, lejeri apstrakcije, separated
sandbox je interfejs koji vide moduli, sadrzi definicije odgovornosti svih modula
application core - di kontejner u kome se registruju moduli, pali i gasi module, modul menadzer







































