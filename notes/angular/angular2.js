@Input, @Output su propertiji, je za property binding od parent na child component
dekorator postavlja metadata
.subscribe je .then()
of() Observable
 heroes$: Observable<Hero[]>; $nesto je Observable
 <input #searchBox je promenljiva...
 async u templejtu onda nema poziva u komponenti
 ---------------
export je public class
dekoratori javascript
https://medium.com/google-developers/exploring-es7-decorators-76ecb65fb841 
metadata kao resursi
komponenta je prosirena direktiva
*ngFor *ngIf strukturne, ngModel atribut direktive
tvoj servis treba da radi isto kao paket servisi log, twitter fasade itd
You can register providers in modules or in components
---------------------
<button [disabled]="isUnchanged">Save</button>
<img [src]="heroImageUrl"> [atribut] [dom property] No. It's the name of an image element property.
<app-hero-detail [hero]="currentHero"></app-hero-detail> property binding

//atribut binding, attr.
<tr><td [attr.colspan]="1 + 1">One-Two</td></tr>

//class binding
[class]="badCurly">Bad curly</div>
<div [class.special]="isSpecial"> add or remove boolean
//style binding
<button [style.font-size.em]="isSpecial ? 3 : 1" >Big</button>

//isto
<p><img src="{{heroImageUrl}}"> is the <i>interpolated</i> image.</p>
<p><img [src]="heroImageUrl"> is the <i>property bound</i> image.</p>

<p><span>"{{title}}" is the <i>interpolated</i> title.</span></p>
<p>"<span [innerHTML]="title"></span>" is the <i>property bound</i> title.</p>
---

<!-- `myClick` is an event on the custom `ClickDirective` -->
<div (myClick)="clickMessage=$event" clickable>click with myClick</div>

`` multiline string template javascript

//custom event
deleteRequest = new EventEmitter<Hero>();
delete() {  this.deleteRequest.emit(this.hero);}

//template reference za ostatak templejta #var
<input #phone placeholder="phone number">

@Input() kad ima znaci property binding
@Input, @Output aliasi...

//debug filtera pipea
<div>{{currentHero | json}}</div>

//za null
The null hero's name is {{nullHero?.name}}
?, !, $any() operatori za nullable

-----------------
content projection je transclusion
property binding je uvek u instanciranju komponente <app-nesto></>

Component Interaction  komunkacija komponenti
-------------------

Components�directives with a template.
Structural directives�change the DOM layout by adding and removing DOM elements.
Attribute directives�change the appearance or behavior of an element, component, or another directive.


@Input('appHighlight') highlightColor: string;
Inside the directive the property is known as highlightColor. Outside the directive, where you bind to it, it's known as appHighlight.

@Input  public property za binding
--------------------
vise atributnih i jedna strukturna direktiva moze na element
strukturna ima <ng-template>. * se prevodi u <ng-template>
<ng-container> za motanje {}
----------------------
pipe pure impure, deep object diff checking
----------------------
<input #box (keyup)="onKey(box.value)"> template reference
Use template variables to refer to elements
prenosi stringove u event a ne event objekte
-----------------------
observable mali objekat sa foreach
observable je polje u vremenu, iste metode ima map, flatmap itd, stream
.subscribe() 3 callbacka prima, next, error i complete
Multicasting is the practice of broadcasting to a list of multiple subscribers in a single execution.
-----------------------
fromPromise, from counter, from event, from ajax
pipe(f1, f2)
error() je u subscribe(), a catchError() u observable recipe
zavrsava se sa $ konvencija, stopwatchValue$: Observable<number>;
lambde operatori u pipe() za htttp, elegantno
------------------------
observable defined at decleration
slicno sa promise, events (najvise), arrays
The reduce(fja) method applies a function against an accumulator and each element in the array (from left to right) to reduce it to a single value.
-----------------------
dekorator anotacije
------------------------
entry component bootstraped ili routed, imperativne, ne linkuju se iz templejta (deklerativne)
-----------------------
providedIn: 'root', root module
root, NgModule, component, scope
------------------------
singleton forRoot()
---------------------------
get message() { return The light is ${this.isOn ? 'On' : 'Off'}; }
pravi public message prop
--------------------------
test doubles (stubs, fakes, spies, or mocks)
tests themselves should not make calls to remote servers. They should emulate such calls
A cold observable doesn't produce values until you subscribe to it.
This pipe is a pure, stateless function so no need for BeforeEach
------------------------
factory - fja koja instancira nesto sa new i vraca ga

-----------------------------------
-----------------------------------
...objekat spread, kopira property po property objekta i pravi kopiju, a ne taj
-------------------------
selectors delovi statea, strong type store i actions za compiletime proveru,
pure impure functions za reducer, effects za asinhrone http operacije sideefects, 3 akcije umesto jedne load, success, fail
index.ts za javni api iz reducera,
reducer by feature, action type i payload, reducer state i action, vraca novi state, kopiju,
subscribe to store da se reflect changes
-----------------------------------
-----------------------------------
//max schwartzmuller complete guide
//5 component deepdive folder
//4 binding to custom properties
@Input('alias') property, bindable from outside, <app-some-component [prperty]='expresion' />
//6 binding to custom events
@Output() listenable from outside new EventEmitter<type>(); (myEvent) = onMyFunc(args); this.serverCreated.emit(myArgs)
--------------------
//13 projecting content into component
<my-component>sadrzaj</my-component>//poziv
<ng-content></ng-content> ovde da ubaci samo sadrzaj
--------------------
--------------------
//observable
observable je data source sa 3 handlera, handle, error i complete, proizvodjac potrosac
subscribe() prima ove 3 fje
-------
subject je istovremeno i observer i observable, ima i next() i subscribe() fje
komunikacioni kanal
behaviourSubject pocinje sa default value
--------------------
--------------------
//direktive
atribut direktive menjaju samo element na kome su
strukturne * menjaju dom oko elementa
----------
[ngClass] i [ngStyle] daju klasu ili stil na boolean
----------
attributske direktive
@Input() property bindable property spolja kroz templejt [property]="nesto"
@HostBinding(elemProperty, npr 'style.background' 'class.open') property; bindujes prop elem za promenljivu
@HostListener('event') handler fja
--------
strukturna *direktiva se transformise u <ng-template> [ng-if] = true
@Input() moze i na setter, set() je onChange propertyja
appNesto moze da bude i ime direktive i ime njenog propertyja
binding je nesto iz templejta = neka promenljiva
--------
uzeo samo bootstrap css, ne i js
----------------------
----------------------
//services
@Injectable() stavlja se samo u servis koji prima neki drugi servis
data store i ponavljani objekat kandidat za servis
----------------------
----------------------
//router
u appModule u imports3 RouterModel.forRoot([{path: users, component: komponenta},...]
<router-outlet></router-outlet> gde da ubaci komponentu
routerLink na linkovima, routerLinkActive za active klasu na li, routerLinkOptions({}) da odselektuje default
routerLink, queryParams, fragment za prenos promenljivih
za link kroz kod inject Route pa this.route.navigate()
/apsolutna staza od localhost:4200, ostalo relativne
----------------------
----------------------
//forms
<form> element je angular direktiva
(ngSubmit)=onSubmit(f) #f=ngForm
<input ngModel #email="ngModel" 
<span *ngIf="!email.valid && email.touched">Please enter valid email</span>
----------------------
----------------------
//swartzmuler ngrx
u komponentama samo handleri onNesto() okidaju iz templejta, zovu metode servisa, i poneki property atribut za binding
u servisima crud metode data source, http i emituje event subject da prosledi propertije
ngrx store mesto servisa
observable je samo polje u vremenu, otuda i metode operatori
-----------------------
-----------------------
//hamedani unit testing
arrange, act, asert
spyOn(objekat, 'metod').and.callFake() ubacuje tvoj metod (sa istim potpisom) u objekat, mockuje
-------------
//integration
TestBed za kreiranje komponenti fixture i registrovanje modula, TestBed.get(komponenta ili servis), Angularova klasa
stub prazna klasa sa jednim praznim metodom koji treba, i prosledjuje mesto servisa
da proveris da li je samo prazna metoda pozvana
i u providers useClass ActivatedRouteStub, pa spyOn na praznu metodu
Subject nasledjuje Observable, next() metoda
----------------------
----------------------
//stackblitz
do/tap je za svaki kao foreach, map
getHeroNo404<Data>(id: number): Observable<Hero> {} <Data> ???
-----------------------
-----------------------
//schwartzmuler moduli
za rute samo u appModule ide forRoot(), svuda ostalo RouterModule.forChild()
u shared module ne provider[] servisi jer nisu singletoni, niti u component
moduli feature, shared, core
sharedModule ide u sve ostale
kad je @Injectable({ providedIn: 'root' }) ne treba u providers[] u module
---
moduli, servisi, rx, komunikacija, bindinzi, templatei, forme, ngrx
@Input() property bindable iz templejta
----------------------
----------------------
//pluralsight component comunication deborah kurata
dupli binding long way
[ngModel]="listFilter" (ngModelChange)=onFilterChange($event)
geter i u setter za onChange property
@ViewChild(selektor) ref, pa valueChanged subscribe, u afterViewInit() hook
--------------
parent to child comunication
[childProperty]="parentProperty" @Input childProperty u child
onChange() hook za na promenu
--------------
observable je obicno polje, nema veze sto je u vremenu, polje evenata, linq
---------------
child to parent, child notifiyng parent
child component @Output() valueChange: EventEmitter<string>; this valueChange.emit(value)
parent template (valueChange)='onValueChange($event)', parent component onValueChange(value: string) {this.performFilter(value);}
-----------------
communicating trough service, property bag
za komunikaciju komponente sa buducom sobom ili sa drugim komponentama
ako servis u providers modula vidljiv u celoj aplikaciji, zivot aplikacije
ako u lazy loaded module samo u komponentama declarations[] tog modula, lifetime aplikacije
ako u providers komponente u toj i u deci (tag ili router outlet), ne mora singleton, zivi koliko i komponenta, navigated away
log service created, destroyed u konstruktor i u ngOndestroy()
-------------------
state managment service
products: IProducts[] kesirana instanca plus crud
http crud plus update instance
u templejt bind getter komponente, u getter cita property servisa
--------------------
state service with notifications
EventEmitter komunikacija samo child to parent
(click)='onSelected(product)' //template
onSelected(product){ this.productService.changeSelectedProduct(product) }//listcomponent
private selectedProductService = new Subject<IProduct>(); //service
selectedProductChanges$ = this.selectedProductSource.asObservable();
changeSelectedProduct(selectedProduct){ this.selectedProductSource.next(selectedProduct) } //readonly
this.productService.selectedProductChanges$.subscribe(selectedProduct => this.product = selectedProduct); //detailComponent
ono sto je schwartzmuler radio u servisima broadcast
-----
BehaviourSubject ima defaultnu prvu vrednost i drzi poslednju stiglu vrednost, servis pamti stanje kad se vratis na stranicu
-----
za aktivan li *ngFor="let product of products" [ngClass]="{'active': product?.id === selectedProduct?.id}" //ngClass klasa = bool izraz
-----
ako ne menjas samo bindovane proertije {{}} onda mora subject, inace geter dovoljan
BehaviourSubject ako notifikacija potrebna i nakon momenta sto je pristigla
kad sta ide
-----
redosled ruta, izvrsava prvi match na koji naidje
------------------------
"@angular/core": "^5.0.0",^major, ~minor, patch//^5 je do 5.9, ~5 je do 0.5.9
------------------------
//deborah kurata routing
rute iz feature modula idu pre forRoot() ruta iz appModula, tj iz svih modula ukljucenih u import[]
zato AppRoutingModule ide na dno
komponente iz declarations[] imaju pristup rutama
imenovanje ruta products, products/:id, products/:id/edit
iz koda this.router.navigate(['/home']); ili navigateByUrl('/home')
forRoot() kreira singleton router servisa, svuda ostalo ide forChild()
-------------
//parametri
/products/:id/edit :promenljivi deo
[routerLink] = "['/products', product.id, 'edit']" //templejt
this.router.navigate(['/products', this.product.id]); //component
---
let id = +this.route.snapshot.paramMap.get('id') u ngOnInit() ActivatedRoute route
za edit id=0
this.route.paramMap.subscribe(params => getProduct(params['id']) //na promenu url
snapsot cita param samo jednom, observable radi watch
---
optional params [routerLink] = "['/products', {name: productName, code: productCode ... }]"//za jednom samo
query params [queryParams]="{ filterBy: 'er', showImage: true }" //ostaju posle prvog zahteva
queryParamsHandling="preserve"
----------------
rute resolver da skine podatke pre nego renderuje komponentu
da ne bi bilo polovicno ucitanih stranica
u ruti resolve: {product: ResolverService}
export class ProductResolver implements Resolve<IProduct>{}
CITAS GA IZ ActivatedRoute
this.product = this.route.snapshot.data['product']
ili this.route.data.subscribe(data => this.product = data['product']) observable
resolver servis skine podatke umesto data servisa, i stavi ga u ruter
----------------
children rute se kaleme na root rute
/products apsolutna staza, bez / relativna 
children rute idu u UNUTRASNJI <ROUTER-OUTLET />
 path: '',
 component: CrisisCenterComponent,//komponenta koja sadrzi router outlet
   children: [...
----------------
componentless ruta samo path bez komponente i prikazuju se u parent router outletu
----------------
routerLinkActive = "active" za active klasu na tab, moze i na li parent
[ngClass]={} : isValid() za glyph na nevalidan tab
----------------
//route animations
enableTracing:true
loading spiner na evente u appComponent, navigationStart, navigationEnd
this.router.events.subscrube(...)
css animacije html elemenata ili angular animacije
----------------
//secondary routes
named router outlet
u ruta definition outlet: "name outleta"
<router-outlet name="popup"></router-outlet>
RouterModule.forChild([
{
  path: 'messages',
  component: MessageComponent, // sve je od secondary rute
  outlet: 'popup'
}])

http://localhost:3000/products(popup:messages)
routerlink ili this.router.navigate([{outlets: {popup: ['messages']});
navigate() prosledjujes promenljive, navigateByUrl() sam pravis link
--------------------
//route guards

canDeactivate
 canLoad
   canActivateChild
    canActivate
     resolve
 
da ga vrati na prethodnu stranicu posle logina
----------------------
 //lazy loading
 preduslov da moze je da su komponente pod istom rutom i pripadaju jednom feature module
 //samo ovo
 {path: 'products', loadChildren: 'app/products/product.module#ProductModule'}//u parent ruti
 preloadStrategy kad da skida feature module i komponente
 canLoad blokira preloading
------------------------
child rute relativno idu od modula u koji je importovan taj modul, ...i lazy load string...
-------
komponenta ispred children oznacava gde je router outlet, a '' prazna ruta iza je ruta i komponenta
-----------------
property servisa JE STANJE
u servisima kada se u getProducts() umesto polja vracaju Observable<Products> kada se menjaju sa add, update
delete onda se u komponentama automatski menjaju jer su bindovani
u komponenti su samo hendleri fje templejta, koji pozivaju servise
-------------
pathMatch: 'full' za redirekt kad cela staza se poklapa
pathMatch: 'prefix' kad prfix sa leva, '' matchuje sve
-------------------
@Self or @Optional @Host Angular DI decorators, podvuceno na slici
https://medium.com/frontend-coach/self-or-optional-host-the-visual-guide-to-angular-di-decorators-73fbbb5c8658
-------------------
[property html elementa]="property klase"
-------------
//https://blog.chai-jay.com/angular-core-vs-shared-modules/
//core
CoreModule u import[] samo 1 u AppModule
The clearest and most important use of the CoreModule is the place to put your global/HTTP services.
Important single use components/classes can also go in the CoreModule. 
The CoreModule could also be used to export any third party module that is required in the AppModule.

To summarize, your CoreModule should definitely contain your singleton services, single-instance components, 
and export any third-party modules needed in AppModule.

//shared
You SharedModule similarly contains code that will be used across your app and Feature Modules. 
But the difference is that you will import this SharedModule into the specific Feature Modules as needed. 
You DO NOT import the SharedModule into your main AppModule or CoreModule.

Common templates components should also go in the SharedModule
Commonly used pipes (ie filters) and directives should go in your SharedModule
In your SharedModule main file (eg shared.module.ts), you might also export the commonly used Angular modules (eg CommonModule or the FormsModule 

The SharedModule should contain common components/pipes/directives and also export commonly used Angular modules 
------
//https://angular.io/guide/ngmodule-faq#what-kinds-of-modules-should-i-have-and-how-should-i-use-them
SharedModule - components, directives, and pipes that you use everywhere in your app
Import the SharedModule in your feature modules

CoreModule - providers for the singleton services you load when the application starts.
Import CoreModule in the root AppModule only. Never import CoreModule in any other module.

Feature - specific application business domains, user workflows, and utility collections
It would contain all of the components, routing, and templates that would make up functionality.

shared - puno, zajednicki, core jedan u aplikaciji, shared u svaki feature, core u appModule
----------------
u hamedani oshop svi servisi fasada prema firebase CRUD i 2 authGuard interfejsa za admin rute
u debora kurata jedan property bag service za back, i jedan sa BehaviourSubject
u schwartzmuller svi servisi crud i 2 sa Subject<> changed, to je state managment
----------------
prouci angular.json config, ceo setup and deployment deo u docs
ngrx
forme
animacije
testiranje
---------------
zakaci setter property za binding [(ngModel)]='listFilter', setter poziva emit, event zakacen u templejtu i druga komponenta hendluje
ime (eventa) ime @Output propertija
ngOnChanges() gleda promenu @Input()
[property elementa] = "property klase"
@Input() da parent komponenta binduje preko templatea
------
export class ProductService { //servis cuva stanje
    private products: IProduct[];
    currentProduct: IProduct;
komponenta hendluje evente templejta i menja stanje koje je u servisu
getteri obicne fje sa get koje se binduju na templejt
geterom komponente bindujes property servisa za templejt //to
-------------------
//httpclient
.subscribe() je .kao then()
.subscribe() ide u komponentu
this.config = { ...data } //spread dodela svih key value
hendlovanje greske u servisu, ne u komponenti
server - error responses
klijent ili mreza - JavaScript ErrorEvent

//2. callback je error
    .subscribe(
      (data: Config) => this.config = { ...data }, // success path
      error => this.error = error

//error handler
private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) { //klijent
    console.error('An error occurred:', error.error.message);
  } else { //server
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
  }
  return throwError( // return an observable with a user-facing error message
    'Something bad happened; please try again later.');
};      
//prosledjivanje handlera
    .pipe(
      catchError(this.handleError)
    );    
--------
Re-subscribing to the result of an HttpClient method call has the effect of reissuing the HTTP request
http.get(url, options objekat)
tap operator (as in "wiretap") lets the code inspect good and error values passing through the observable without disturbing them. 
httpOptions = {headers: new HttpHeaders({...
addHero (hero: Hero): Observable<Hero> { //add i prosledi novog, i server ga vrati
  return this.http.post<Hero>(this.heroesUrl, hero, httpOptions) //post

//set() jer immutable
httpOptions.headers.set()
{ params: new HttpParams().set()
-------
private searchText$ = new Subject<string>();
this.searchText$.pipe() //observable je dok mu neko ne pozove subscribe()

switchMap()//dobar
It takes a function argument that returns an Observable//prima fju koja vraca observable
If a previous search request is still in-flight it cancels that request and sends a new one.
It returns service responses in their original request order, even if the server returns them out of order.
-----
interceptors
{..., key: val} spread operator je slican kao mass asignment propertija objekta
auth-interceptor menja req i prosledije u next.handle(authReq)
logging-interceptor samo cita req i loguje, ne prosledjuje u next.handle()
caching-interceptor vraca kesirane i netaknute
intercept() and handle() methods to return observables of HttpResponse<any> Instead they return observables of HttpEvent<any>.
The HttpResponse class itself is actually an event, whose type is HttpEventType.HttpResponseEvent.
next.handle(noHeaderReq).pipe() vraca Observable<HttpEvent<any>>, next: HttpHandler
observable uvek gledas kao polje
svi interceptori se izvrsavaju, u samom interceptoru filtriras zahtev koji hoces

Like intercept(), the handle() method transforms an HTTP request into an Observable of HttpEvents 
which ultimately include the server's response

you must provide them in the same injector (or a parent of the injector) that provides HttpClient. 
Interceptors provided after DI creates the HttpClient are ignored.

multi: true option. This required setting tells Angular that HTTP_INTERCEPTORS is a token for a multiprovider 
that injects an array of values, rather than a single value

const newReq = req.clone({ body: newBody });//clone() klonira i postavlja propertije
for HttpClient, you need to import the Angular HttpClientModule

HttpClientModule je u import export SharedModule, HttpTokenInterceptor je u CoreModule
pa je CoreModule pre SharedModule u AppModule
-----
HttpRequest<any> je kao polje necega array<int>
const entry = { url, response, lastRead: Date.now() }; je skraceno od { url: url, response: response...}
curried function fja koja vraca fju sa 1 argumentom add:: number -> (number -> number)
povratni tip je fja i to je to, postepeno pozvana fja

// A curried function
let add = (x: number) => (y: number) => x + y;
// Simple usage
add(123)(456);
export type HandleError = <T> (operation?: string, result?: T) => (error: HttpErrorResponse) => Observable<T>;
createHandleError = (serviceName = '') => <T>(operation = 'operation', result = {} as T) => this.handleError(serviceName, operation, result);
this.handleError = httpErrorHandler.createHandleError('HeroesService');//poziv iz vise koraka

A curried function is a function of several arguments rewritten such that it accepts the first argument and returns a function 
that accepts the second argument and so on. 
This allows functions of several arguments to have some of their initial arguments partially applied.
-----------
return this.http.post<IProduct>(this.productsUrl, product,  { headers: headers} )
post<IProduct> je CAST
----------------------
//services
non-factory/class dependency (like string for your configs), you usually use @Inject
---------------------
---------------------
//di providers
[{ provide: Logger /*token*/, useClass: Logger/*provider definition object*/ }]
useClass, useExisting, useValue, or useFactory
useExisting - aliasing, { provide: OldLogger, useExisting: NewLogger}
//value providers
useValue - gotov objekat, [{ provide: Logger, useValue: silentLogger }]

Sometimes you want to inject a string, function, or object
export const HERO_DI_CONFIG: AppConfig = { //AppConfig je interfejs, on ne moze token
  apiEndpoint: 'api.heroes.com',
  title: 'Dependency Injection'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
providers: [{ provide: APP_CONFIG, useValue: HERO_DI_CONFIG }]
constructor(@Inject(APP_CONFIG) config: AppConfig) { //eto ga prica o @Inject()
//Factory providers
based on information you wont have until run time
class HeroService {
constructor(
  private logger: Logger,
  private isAuthorized: boolean) { }

let heroServiceFactory = (logger: Logger, userService: UserService) => {
  return new HeroService(logger, userService.user.isAuthorized);
};

export let heroServiceProvider =
  { provide: HeroService, useFactory: heroServiceFactory, deps: [Logger, UserService] };
  
  providers: [ heroServiceProvider ],
//Predefined tokens and multiple providers
multi: true - which you can use with APP_INITIALIZER to register multiple handlers for the provide event.

//Tree-shakable providers
Tree shaking refers to a compiler option that removes code from the final bundle if that code not referenced in an application
services provided at the NgModule or component level are not tree-shakable.
To make services tree-shakable, the information about how to construct an instance of the service (the provider definition) 
needs to be a part of the service class itself.
@Injectable({
  providedIn: 'root',
})
export class Service {//tree-shakable
}
-----------------------
-----------------------
//Hierarchical Dependency Injectors

@Injectable-level configuration
@NgModule-level injectors
@Component-level injectors
An injector does not actually belong to a component, but rather to the component instances anchor element in the DOM
Injector bubbling - na gore
cap the bubbling by adding the @Host() parameter decorator 

Component injectors //scenariji kad treba provide service u komponenti
Scenario: service isolation
Scenario: multiple edit sessions
Scenario: specialized providers
---------------------------
---------------------------
//DI in Action

//parameter decorators
@Optional - da vrati null umesto exception ako ne nadje
@Host - stop propagate prema root
------
@Inject()//za string ili factory
export const BROWSER_STORAGE = new InjectionToken<Storage>('Browser Storage', {
constructor(@Inject(BROWSER_STORAGE) public storage: Storage) {}

@Self()
@SkipSelf()
da li da uzme provider definisan na sebi (komponenti) ili neki drugi
  constructor(
    @Self() private sessionStorageService: BrowserStorageService,//sebi
    @SkipSelf() private localStorageService: BrowserStorageService,//parentu
  ) { }
-----
//inject html element u direktivu
private el: HTMLElement;
 
constructor(el: ElementRef) {
  this.el = el.nativeElement;
}
-----
useValue - za string ili objekat, runtime ili test mock
useClass
useExisting - alias, 2 tokena za istu klasu
useFactory - factory function whose inputs are a combination of injected services and local state.
------
apstraktna klasa token umesto interfejsa
'InjectionToken' objects -  simple values like dates, numbers and strings, or shapeless objects like arrays and functions.
------
Inject into a derived class
  constructor(heroService: HeroService) {
    super(heroService);
These complications argue for avoiding component inheritance.
------
Break circularities with a forward class reference
providers: [{ provide: Parent, useExisting: forwardRef(() => AlexComponent) }],
----------------------
i @Input i @Output idu na templejt
<app-hero-tax-return *ngFor="let selected of selectedTaxReturns; let i = index" [taxReturn]="selected" (close)="closeTaxReturn(i)">
</app-hero-tax-return>                                                   @Input() taxReturn    @Output() close = new EventEmitter<void>();
-----
za dekleraciju fje moze () => povratni tip
*ngFor za polje.length=0 kao ngIf, pa gura u polje i pojavi se u dom
primeri su objasnjeni U TEKSTOVIMA
//ovo NIJE http
    return new Observable<Hero[]>((observer: Observer<Hero[]>) => {
      observer.next(this.heroes);
      observer.complete();
    });
--------------------------
//Observables
new Observable() prima fju koja ima observer arg koji ima next(), error(), complete() i vraca unsubscribe() fju
Observable(observer).subscribe()
creation, operators, error handling
Observables in Angular - Event emitter, HTTP, Async pipe, Router, Reactive forms
slicni sa promises, events, arrays
--------------
<ng-content></ng-content> ide u child komponentu i tu ide sadrzaj koji je izmedju tagova child komonente, pros
projekcija, transclusion
----------------------------
//Navigate the component tree with DI
injektuje komponentu kao servis
preko Base (extends) klase ne moze
preko Parent apstrakne klase (interfejsa) moze
[{ provide: Parent, useExisting: forwardRef(() => BarryComponent) }]//alias
forwardRef(() kad pokazuje na samu sebe
@SkipSelf() da pocne da trazi od parenta, sebe preskoci
@Self() samo na sebe
-------------------------------
According to VirtualBox manual, these are the network possibilities for your VMs:
NAT: Internal network between VM and host. Besides you can access to the Internet. Two or more VMs in NAT mode don't have connectivity among them, only with the host and Internet.
Internal: All VMs with this configuration will have connectivity among them but not with the host nor Internet.
Host only: A virtual network will be created between the host and all the VMs with this configuration, but you will have no Internet access.
NAT Network: You can reach all VMs in the same pre-defined NAT network and the host, but you won't be able to connect to Internet.
Bridged: Your VM will be in the same network that your host as if it were other computer/server in this network, and thus, it will have Internet acces if your host has.
Generic: Advanced and more specific configuration rarely used.
-------------------------------
//Getting JSON data
return this.http.get<Config>(this.configUrl);//je cast response.body koji je inace Object
Observable<Object>, Observable<Config>, Observable<HttpResponse<Config>>
u subscribe error je 2. fja error => this.error = error
u get() ide error operator catchError(this.handleError)
handleError(error: HttpErrorResponse) klijent i server loguje u console.log(), a throwError(string), vraca Observable<never>
------
//Sending data to the server
addHero - POST
updateHero - PUT
--------------------
linkovi u rutama i u servisima nemaju veze
kad ima index.ts moze import iz foldera import { ArticleListConfig, TagsService, UserService } from '../core';
krenes od home komponente root, pa prema listovima komponenta po komponenta
query(config: ArticleListConfig): Observable<{articles: Article[], articlesCount: number}> {//vraca Observable<> od 2 propertija
---------
(toggle)="onToggleFavorite($event)"//toggle je custom event, nije click//parent templejt//fja u PARENT
@Output() toggle = new EventEmitter<boolean>();//child komponenta
this.toggle.emit(true);//child//na neki klik event iz child templejta, zapazi
onToggleFavorite(favorited: boolean) {}//parrent
child se obraca parentu
---------------------
//lazy loading MODULE
loadChildren: './settings/settings.module#SettingsModule'
staza do module fajla #module klasa unutra
koja komponenta ima children ta ima i <router-outlet>
-----------
//reactive forms
<form [formGroup]="settingsForm"
<input formControlName="username"
Object.assign() i ... razlike rastumaci
slug dodeljuje server
componentless routes - share guards and resolvers
------------------
u resolve() samo dovuce usera ili article preko servisa, pa u ngOnInit(){ this.route.data.subscribe(...

pogledaj interceptors i guards i preloadingStrategy, CanDeactivateGuard

canActivate() vraca Observable<boolean> ili boolean
canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> {}
canActivate: [AuthGuard] u rutama i to je to

protect child routes with the CanActivateChild guard, ruta iznad children[] u rutama i to je to
vise ruta odjednom
canActivateChild: [AuthGuard],
children: [...]
canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

canLoad ide kod lazy load, blokira i resolver
canLoad(route: Route): boolean {
  loadChildren: './admin/admin.module#AdminModule',
  canLoad: [AuthGuard]

component: CrisisDetailComponent,
canDeactivate: [CanDeactivateGuard]
Called when the Url changes to a different route
----------
preloadingStrategy
da li da dovuce komponente fajlove za lazy loaded rutu na load tj pre nego je kliknuta
na lazy loaded rutama
data: { preload: true } se cita u if (route.data && route.data['preload']) { u preload()
preload vraca load() ili Obsevable.of(null) ucitava ili ne ucitava
doda service u providers[] AppRoutingModule
------------
{ path: '',   redirectTo: '/heroes', pathMatch: 'full' }, pathMatch: 'full' ide u redirect uglavnom
------------------------------
//ngrx

//effects
switchMap
Cancels the current subscription/request and can cause race condition
Use for get requests or cancelable requests like searches

concatMap 
Runs subscriptions/requests in order and is less performant
Use for get, post and put requests when order is important

mergeMap
Runs subscriptions/requests in parallel
Use for put, post and delete methods when order is not important

exhaustMap
Ignores all subsequent subscriptions/requests until it completes
Use for login when you do not want more requests until the initial one is complete
---------------
//selectors
products: {
 showProductCode: true,
 currentProduct: {...},
 products: [...]
}
const getProductFeatureState = createFeatureSelector<ProductState>('products');//hvata products kljuc, feature
export const getShowProductCode = createSelector( //prima feature selector i fju za podkljuc selector
 getProductFeatureState,
 state => state.showProductCode //hvata showProductCode podkljuc
);
-------
//composable
export const getCurrentProduct = createSelector(
    getProductFeatureState,//vraca state
    getCurrentProductId,//vraca currentProductId
    (state, currentProductId)

----
//spread za objekat razjasnjen

var o1 = {key1:1,key1:2};//ponovljeni kljuc pregazi, tako radi spread
o1
Object { key1: 2 }
---
return {
 ...state, //samo clone uradi
 showProductCode: action.payload//ovaj pregazi
};
----
//effects
  @Effect()
  updateProduct$: Observable<Action> = this.actions$.pipe(
	//filter
    ofType(productActions.ProductActionTypes.UpdateProduct), //enum.string
    map((action: productActions.UpdateProduct) => action.payload),//project payload, akcija tip
    mergeMap((product: Product) =>
      this.productService.updateProduct(product).pipe( //zovi service
        map(updatedProduct => (new productActions.UpdateProductSuccess(updatedProduct))),// dispatch akciju success//reducer hvata
        catchError(err => of(new productActions.UpdateProductFail(err))) // dispatch fail, zbog new konstruktor
      )
    )
  );
---------------------
//handle error
catchError(this.handleError)//u service prepise error objekat u error message i to prosledi komponenti da prikaze ili upise u state
return throwError(errorMessage);
-----------------------
//containers i presentational components
container komponenta koristi store, inject store u konstruktor, store select i store dispatch
presentational ima samo @Input, @Output i event handlere, i emit()
(@Output event, property od taga na kome su)= onEvent() fja od komponente ciji je template
<tag [njegov atribut]=nesto
------
ako je lazy load modul ide extend state i to je to
state podeljen po feature-ima
-------
//selector, actions creator i reducer
export type ProductActions = klase, a ne enume

export class DeleteProductFail implements Action {
  readonly type = ProductActionTypes.DeleteProductFail;//je enum

switch (action.type) { //type prop klase koji je enum
  case ProductActionTypes.ToggleProductCode://enum
    
return {
 ...state, //kloniraj
 showProductCode: action.payload//pa pregazi, trivial
};  

products: [...state.products, action.payload],//kloniraj pa append na array
------
Effects take an action, do some work and dispatch a new action
reducer jedini hvata akcije

  @Effect()
  createProduct$: Observable<Action> = this.actions$.pipe(
    ofType(productActions.ProductActionTypes.CreateProduct),//filter
    map((action: productActions.CreateProduct) => action.payload),//project
    mergeMap((product: Product) =>//iterate
      this.productService.createProduct(product).pipe(//zovi servis
        map(newProduct => (new productActions.CreateProductSuccess(newProduct))),//dispatch success or fail actions
        catchError(err => of(new productActions.CreateProductFail(err)))
      )
    )
  );

  @Effect()
  follow = this.actions.ofType<Follow>(ProfileActionTypes.FOLLOW).pipe(//filter
    map(action => action.payload),//project
    concatMap(slug =>//iterate
      this.actionsService.followUser(slug).pipe(//zovi servis
        map(results => ({
          type: ProfileActionTypes.FOLLOW_SUCCESS,//vraca objekat, kao new
          payload: results
        })),
        catchError(error =>
          of({
            type: ProfileActionTypes.FOLLOW_FAIL,
            payload: error
          })
        )
      )
    )
  );

jedna linija u map() podrazumeva return
selectori citaju state i pozivaju se u komponenti vracaju observable
this.displayCode$ = this.store.pipe(select(fromProduct.getShowProductCode));


--------------------------
//reactive forms
value changed: pristine, dirty
validity: valid, errors
visited: touched, untouched


-----------------------------
//state managment service
@Injectable()
export class ProductService {
 private products: IProduct[]; //bez ovoga je data access service
 getProducts() {}
 getProduct(id: number) {}
 ...}
 promeni ga jedna komponenta promeni se i u ostalim, jer binduje na property servicea
 --------
Property Bag Service
  Retain view settings
  Pass data between components
--------
Encapsulates retrieve and store operations
Retains and shares state values
Minimizes hits to the backend server, Improves performance
Provides change notifications for bound values using a getter //SAMO ZA BINDOVANE SA GETEROM, bind na service property

Considerations:
  Stale data
  No explicit change notifications
  State is not immutable
getProducts(): Observable<IProduct[]>//getter
{
   if (this.products) {
   return of(this.products);
 }
...
}
------------
//communicating-through-service-notifications-slides
//behaviour subject
Requires an initial value
Provides the current value on a new subscription

//subject
Requires no initial value
Broadcasts items as they are pushed

//behaviour subject
Requires an initial value
Provides the current value and then broadcasts items as they are pushed

//Does the Service Need a Subject?
Notifications are required
Those notification are more than just changes to bound properties 
------
//na change u istom templejtu
onSelected(product: IProduct) {
  this.productService.changeSelectedProduct(product);
}
Service
changeSelectedProduct(selectedProduct: IProduct) {
  this.selectedProductSource.next(selectedProduct);//next()
}
------------------------
//httpclient
return this.http.get<Config>(this.configUrl); //typechecking na get()<>
-----
return this.http.get<Config>(
    this.configUrl, { observe: 'response' });
// resp is of type HttpResponse<Config>    
resp.headers
resp.body 
//inace je 
.subscribe((data: Config) => this.config = { ...data });
------
//error handling
//u servisu
private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
  ...
  // return an observable with a user-facing error message
  return throwError('Something bad happened; please try again later.');//observable za komponentu, error path u subscribe
  
//isto u servisu
  return this.http.get<Config>(this.configUrl)
    .pipe(
      catchError(this.handleError)
    );
//u komponenti
error => this.error = error
-------------------
animacije pros
-------------------
//mosh testing
describe('system under test, ime fje ili klase', ()=>{}); // test suite
it('ime testa', ()=>{}); //test, opisno ime
----
testiranje komponente, stanja
beforeEach(), afterEach(), beforeAll(), afterAll()//inicijalizacija system under test, arrange
unit test ne sme da ima sideffects, kao da je jedini
-----
//konstruktor objekta sa null, pa ubaci 1 fju koju poziva
// fja za kontrolu nad funkcijom klase, servisa npr, da li je pozvan, throw exception, prosledis tvoju implementaciju
spyOn(objekatKlase, 'ime fje klase').and.callFake(()=>{tvoja implementacija});

spyOn(service, 'getTodos')and.callFake(()=> {
 return Observable.from([1, 2, 3]);
 });
 
 component.ngOnInit();
 
 expect(component.todos.length).toBe(3);
 ----
 let fja = spyOn() moze i na ugradjene javascript fje
 returnValue
 -----
 testira komponente kao obicne klase sa new, bez templejta
 lako testira, objasnjeno
 state changes
 forms
 events (output properties)
 services
 ----
 ne moze router, template bindings
 -------
 ng test
 ng test --code-coverage
 sa xit i xdescribe() disabeleujes test ili suite
 code coverage crveno highlight sta nije pokriveno
 70% ok
 ----------------
 this.route.data.subscribe(... ima resolver za prefetching znaci pa izvlaci http podatke iz rute