next-connect - method not allowed, jeste nepostojeca ruta
-------------------
za next.js logging koristi pino
-------------------
dok radis sa next-auth drzi debug: true, lakse za rad
mada je previse verbose, gusi terminal
------------------
next-auth callbacks.redirect() fires za mnoge druge rute /api/users, /api/business, koje nemaju veze sa next-auth
------------------
// getStaticPaths ide za dynamic routes [postId] // zapazi, eto prosto
// /pages/posts/[postId].js
export async function getStaticPaths() {
    return {
      paths: [
        {params: {postId: '1'}}, 
        {params: {postId: '2'}}, 
        {params: {postId: '3'}}, 
      ]
    }
  }
------------------------------
// https://chirag-gupta.hashnode.dev/how-to-solve-hydration-error-in-nextjs
// What is Hydration?
In Next.js, the first render of the page takes place on the server. 
This pre-rendered HTML is then served to the client. 
On the client side, React takes over this HTML page to make this page interactive
(Basically it attaches event handlers to the dom nodes). This is Hydration.
-----
razlikuje se html na serveru i klijentu
------
// scenarios:
1. using browser APIs to conditionally render (localStorage,sessionStorage)
2. HTML syntax is not correct <p><div>logout</div></p>
3. Some browser extensions change the HTML on the client side
-----
// resenja:
1. Reconfigure Logic with useEffect // ??
2. Disable SSR on some components which may differ on the client side
import dynamic from 'next/dynamic';
//code for YourComponent
export default dynamic(() => Promise.resolve(YourComponent), { ssr: false }); // disable SSR
3. suppressHydrationWarning={true} for timestamps
------------------------