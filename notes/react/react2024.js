// kriterijumi za ekstrakciju u react komponentu, chatGpt
// crucial for UI
// Reusability:
If a piece of UI is intended to be reused in different parts of your application or across multiple components, 
it's a good candidate for encapsulation within its own React component. Reusable components promote 
a DRY (Don't Repeat Yourself) codebase.

// Maintainability:
If a section of your UI becomes complex or requires specific logic, it's often beneficial to isolate that c
omplexity in its own component. This makes the code more maintainable and easier to understand, 
as it allows you to focus on a smaller, well-defined piece of functionality.

// Separation of Concerns:
Each component should ideally have a single responsibility or concern. If a part of your UI involves 
distinct behavior or functionality, consider creating a separate component to encapsulate that concern. 
This follows the principle of separation of concerns and improves code organization.

// Readability:
Breaking down your UI into smaller, focused components enhances code readability. It's often easier 
to understand and reason about smaller components with specific responsibilities than a monolithic and complex structure.

// State Management:
Components that manage their own state or have specific state-related logic may be good 
candidates for encapsulation. This helps to isolate and manage the state locally within the component, 
making it easier to reason about and test.

// Event Handling:
Components that handle specific user interactions or events can benefit from being encapsulated. 
This allows you to define event handlers and related logic within a smaller scope, making the code more modular.

// Testability:
Smaller components are generally easier to test in isolation. If a piece of UI has 
its own specific behavior or functionality, creating a separate component allows you to 
write focused unit tests for that component.

// Props Variation:
If you find yourself passing a set of similar props to a group of elements or a section of UI, 
it might be a sign that those elements should be extracted into a separate component. 
This can make your code more declarative and easier to manage.

// Semantic Markup:
If a section of your UI represents a semantic unit, such as a header, footer, or a specific widget, 
it makes sense to create a React component for it. This promotes a clear structure and semantics in your application.

// Collaboration:
If different team members or teams are responsible for different parts of the UI, 
breaking down the UI into components that map to ownership boundaries can facilitate parallel development and collaboration.
------------------------
// normalizing redux state
// https://redux.js.org/usage/structuring-reducers/normalizing-state-shape
izbegava duplicated i nested data
tesko za update i menja vise objekata i izaziva rerender i nepromenjenog dela
immutable update zahteva sve nove parent keys za nested
-----
po ugledu na sql baze, odvojene nezavisne tabele sa referencama, samo id
redosled se pamti preko array
normalizacija u sql - dekompozicija u vise tabela
------
keep that data in a normalized form - u odvojenim tabelama // eto
------
// not normalized
// sve je podredjeno postu, post.properties
const blogPosts = [
    {
      id: 'post1',
      author: { username: 'user1', name: 'User 1' },
      body: '......',
      comments: [
        {
          id: 'comment1',
          author: { username: 'user2', name: 'User 2' },
          comment: '.....'
        },
        {
          id: 'comment2',
          author: { username: 'user3', name: 'User 3' }, // autor komentara se ponavlja u svakom postu
          comment: '.....'
        }
      ]
    },
    {
      id: 'post2',
      author: { username: 'user2', name: 'User 2' },
      body: '......',
      comments: [
        {
          id: 'comment3',
          author: { username: 'user3', name: 'User 3' }, // isti autor
          comment: '.....'
        },
        {
          id: 'comment4',
          author: { username: 'user1', name: 'User 1' },
          comment: '.....'
        },
        {
          id: 'comment5',
          author: { username: 'user3', name: 'User 3' },
          comment: '.....'
        }
      ]
    }
    // and repeat many times
  ]
-----------
// normalized
// nezavisne (ravnopravne) tabele posts, comments, users
// odlicno, jasno
// manje ugnjezdavanja obj1->obj2->obj3
// nema ponavljanja, objekat definisan samo na 1 mestu
{
    posts : {                          // tabela
        byId : {
            "post1" : {
                id : "post1",
                author : "user1",
                body : "......",
                comments : ["comment1", "comment2"]
            },
            "post2" : {
                id : "post2",
                author : "user2",
                body : "......",
                comments : ["comment3", "comment4", "comment5"] // redosled se cuva u polje ids
            }
        },
        allIds : ["post1", "post2"]     // redosled se cuva u polje ids
    },
    comments : {                        // tabela
        byId : {
            "comment1" : {
                id : "comment1",
                author : "user2",
                comment : ".....",
            },
            "comment2" : {
                id : "comment2",
                author : "user3",
                comment : ".....",
            },
            "comment3" : {
                id : "comment3",
                author : "user3",
                comment : ".....",
            },
            "comment4" : {
                id : "comment4",
                author : "user1",
                comment : ".....",
            },
            "comment5" : {
                id : "comment5",
                author : "user3",
                comment : ".....",
            },
        },
        allIds : ["comment1", "comment2", "comment3", "comment4", "comment5"]
    },
    users : {                               // tabela
        byId : {
            "user1" : {
                username : "user1",
                name : "User 1",
            },
            "user2" : {
                username : "user2",
                name : "User 2",
            },
            "user3" : {
                username : "user3",
                name : "User 3",
            }
        },
        allIds : ["user1", "user2", "user3"]
    }
}
--------------
// redux state struktura, konvencija
{
    simpleDomainData1: {....},  // non-relational data
    simpleDomainData2: {....},
    entities : {                // relational data, tables
        entityType1 : {....},
        entityType2 : {....}
    },
    ui : {                      // ui data
        uiSection1 : {....},
        uiSection2 : {....}
    }
}
---------
// relacije
// vezna tabela za M:N relacije
// vezna tablea - join table or associative table
// da bi tabele mogle da ostanu nezavisne
{
    entities: {
        authors : { byId : {}, allIds : [] },
        books : { byId : {}, allIds : [] },
        authorBook : {                          // authorBook, ovo je vezna tabela
            byId : {
                1 : {
                    id : 1,
                    authorId : 5,
                    bookId : 22
                },
                2 : {
                    id : 2,
                    authorId : 5,
                    bookId : 15,
                },
                3 : {
                    id : 3,
                    authorId : 42,
                    bookId : 12
                }
            },
            allIds : [1, 2, 3]

        }
    }
}
----
// normalizr - paket za transformisanje json api data u normalized form
https://github.com/paularmstrong/normalizr
normalizr(schema, jsonData) => normalized data
----------------------
// react-hook-form ima devtools // zapazi
https://www.react-hook-form.com/dev-tools/
-------------------------
//  All about memoization in React - Advanced React course, Episode 5 - Developer Way
https://www.youtube.com/watch?v=huBxeruVnAM
primitive and reference types in js 
---------
{a: 1} === {a: 1} // shallow comparison, poredi reference, a ne vrednosti, 2 objekta i 2 ref u memoriji
-----
useCallback memoizes function_ itself 
useMemo calls function__ and memoizes return value
-----
their callback is cached on forst run
--------
// 2 use cases za useMemo i useCallback
1. memo prop from parent that is used in childs useEffect 
2. memo props for React.memo, objects, functions, jsx...
3. memo expensive calculations
-------
onScroll i onMouseMove se cesto triggeruju, callbacks treba memoized 
-------
memo sluzi za optimizaciju samo kad ima puno rerenders 
---------------------------
// Mastering React Reconciliation - Advanced React course, Episode 6 - Developer Way
https://www.youtube.com/watch?v=cyAbjx0mfKM
 u virtual dom object tags have type string, components have type function_ koju poziva i vraca tags...
reconciliation - change detection, diff, kreira virtual dom pre i posle kopiju i poredi
krece odakle se promenio state
1. shallow comparison, ref changed
2. type
3. array of siblings, pamti index, linije sa uslovom su isto item
// prakticno redosled sta poredi
reference -> type -> poziciju u jsx i array -> key (ima prioritet)
// posledica:
react ne razlikuje elemente i gura state i props u pogresne 
-------
// local component unmounts/mounts svaki put, antipatern
// bugs: losing focus npr
const Component = () => {
    const Input = () => <input />; // nije samo fja nego dodeljeno u var pa <var />

    return <Input />; // var u <var />, zapazi 
}; 
------
// resenja za:
state ? <Input placeholder="1" /> : <Input placeholder="2"/> // jedna linija, zato problem, state prelazi iz jedan u drugi, react ih ne razlikuje
1. 
state ? <Input /> : null // svaka linija je item u array of siblings
!state ? null: <Input /> 
2.
sa key
state ? <Input placeholder="1" key="my-key-1" /> : <Input placeholder="2" key="my-key-2"/> 
-------
// key rezime, sve
key - za identifikaciju elemenata, ima prioritet kad se postavi 
key radi na svim elementima, ne samo na array of siblings
----------
ako promenis key mozes da remountujes komponentu, resetujes state, lose performase remount
----
react zahteva key samo za dynamic arrays elements.map() (jer moze redosled da se promeni), a ne za hardkodirane u jsx (static) // ok
data.map(() => <Input />)
// vs
<Input />
<Input />
<Input />
-------------------------
// state libraries overview
// Top 10 React state libraries, sorted by downloads:
1. Redux (single store, typically with Redux Toolkit)
2. Apollo (for GraphQL - includes caching)
3. Zustand (Redux, simplified)
4. Tanstack query (For server/async state - includes caching)
5. swr (Tanstack query alternative)
6. Mobx (observables, actions, and derivations)
7. Jotai (State atoms. Recoil, simplified. A “drop-in” context replacement)
8. XState (For complex logic)
9. Recoil (State atoms. For highly dynamic UIs)
10. Valtio (Mobx, simplified to 2 APIs)
------------------------
// cory house react state breakdown, pluralsight course
https://x.com/housecor/status/1809257707695751677
-----------------
// FormData to js object 
const formData = new FormData(form)
const formDataObject = Object.fromEntries(formData)
---------------
// useEffect vs useLayoutEffect
useLayoutEffect blocks the browser painting process until it finishes running
useEffect runs after the browser is done repainting changes, absolute end
---------------
// variable as component
fora: mora veliko slovo, da ne bi bio defailt html tag 
import type { FC, ElementType } from 'react';
interface CardProps {
    icon: ElementType; // taj tip
}
const Card: FC<CardProps> = ({ icon: Icon }) => { // to
    return <Icon className="h-32" />
}
--------------------
componente organizovane po stranicama (View, Fragment), packages/ui 
/src/fragments/Profile/Sites/SiteDropdown.tsx
folder struktura komponenti prati route path segment strukturu // to
da znas gde da trazis komponentu
Layout i Elements nodes u exports 
ui za base components 
