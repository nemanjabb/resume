2 levels of skeletons - 3 tiers, layout, component with skeleton, components with props
-----
--------------
A class component becomes an error boundary if it defines a new lifecycle method called componentDidCatch(error, info)
-----
hook je samo komponenta koja ne vraca template (jsx) nego promenljivu.
-----
// https://itnext.io/centralizing-api-error-handling-in-react-apps-810b2be1d39d
render prop, children je samo prosledjena i pozvana funkcija, a ne jsx
const Query = ({ url }) => {
  const { data, statusCode } = useQuery({ url });
  
  return children({ data });
}
---
const DogPage = () => {
  const { breed } = useParams();
  return_ (
    <Query url={`https://dog.ceo/api/breed/${breed}/images/random`}>
      {({ data }) => {
		...
      }}
    </Query>
-----------------
setState(value)
state //ne mozes da ga koristis odmah ispod u handleru
// asinhrono, promenice se u bududcnosti, nepoznato kad
// mora u useEffect da pokupis value kad se promeni
// state je variabla koja se koristi u render, vezana za render, trigeruje render
--------------
// only re-render image if the src changed
export default React.memo(BackgroundImage, (prevProps, nextProps) => {
  return prevProps.src === nextProps.src;
});
--------------
// useSWR
mutate je write to cache
revalidate je refetch
------
useSomething hook je samo za GET
za POST, PUT etc je useApi
-------------
function_ createPortal(children: React.ReactNode, container: Element, key?: string): React.ReactPortal
prakticno container.appendChild(children)
---------------
uncontrolled - controlled error in console, initialized state to undefined somewhere
---------------
portal - renderovan element gde treba u jsx - react, a u dom-u preseljen u drugi kontejner
imas react scope koji ti treba
props mu radi i eventi
---------------
// https://www.robinwieruch.de/react-usecallback-hook

useMemo is used to memoize values // hook
React.memo is used to wrap React components to prevent re-renderings // zapazi, nije isto sto i useMemo hook

Whenever the App component re-renders after someone types into the input field, the handleRemove handler function_ in the App gets re-defined. 
// dobar
We can use useCallback to memoize a function_, which means that this function_ only gets re-defined if any of its dependencies in the dependency array change:

// why React's useCallback Hook isn't the default
Often the computation for this comparison can be more expensive than just re-defining the function_.
-----------------
// componentWillReceiveProps
// https://stackoverflow.com/questions/46113044/when-to-use-componentwillreceiveprops-lifecycle-method	
componentWillReceiveProps is required if you want to update the state values with new props values, this method will get called whenever any change happens to props values.
kad trebas da inicijalizujes state sa props, samo ovde update ne init

componentWillMount is essentially the constructor

in react i don't think there is another way to run a function on mount
like to replace the good old componentWillMount
--------------------
portal - komponenta u jsx/reactu je gde treba i prosledjujes props, reaguje, a u html-u je premestena na drugom mestu
----------------
svg ikonu bojis sa color:red, kao text
tw-text-success-500
-------------
// svg kao slika
svg moze kao slika u background-image: url('./image.svg') bez problema
ili se importuje direkt kao komponenta u react pa <SvgImage /> i to je to
import MySvg from './image.svg';
<SvgImage />
treba da ima webpack svg loader
------------------
// https://stackoverflow.com/questions/60652656/why-0-is-rendered-on-short-circuit-evaluation-of-array-length
sortedUserPages?.length > 0 && 'render' ili renderuje nulu, broj
React render ignorise samo: false, null, undefined or true
------------------
// ?
getDerivedStateFromError()
componentDidCatch()
suspense
----------------
ReactDOM.createPortal(children, container, ...) zapazi, isto kao appendChild()
---------------
// store function in the state
// https://medium.com/swlh/how-to-store-a-function-with-the-usestate-hook-in-react-8a88dd4eede1

initial value and value in setter needs to be wrapped with anonymous function
setter inace prima fju za prev state, kako bi razlikovao
const [getButtonsFn, setGetButtonsFn] = React.useState<Function>(() => () => null);
getButtonsFn={getButtons => setGetButtonsFn(() => getButtons)} // samo jednom, nemoj 2 puta, pazi

don't store jsx in a state
------------------
// createPortal Target container is not a DOM element
// additional check
{modalFooter && ReactDOM.createPortal(skeletonButtons, modalFooter)}
-----------------
// useApi() cached state
SWR caches the content based on the URL // to to
------
Is there a way to clear swr cache from browser? ctrl + F5 does nothing
devtools -> application -> session
---------------
// useCallback
kesira definiciju funkcije da se ne redefinise na svaki render, nego samo kad se dependency menja, filter
------------------
devault value za prop najblize do gde se koristi, a ne gore u nekom parentu
------------------
------------------
// korsni hookovi za projekat zaista
https://github.com/streamich/react-use
-------------------
// clean timeout in useEffect
  useEffect(
    () => {
      let timer1 = setTimeout(() => setShow(true), delay * 1000);
      return () => {
        clearTimeout(timer1);
      };
    },
    []
  );
------------------
// uncontrolled form controls
uncontrolled components use ref's to get access dom node and value
------------------
// typescript version
tsc --version
------------------
?
jasna predstava koji props su opcionalni koji nisu, values, onChange?:
------------------
kursor bezi na kraj controlled inputa - javascript runtime error negde, broken app
------------------
razlika izmedju komponente i funkcije je sto komponenta moze da ima hookove i state
obe mogu da vrate jsx
------------------
sablon u svakoj komponenti, happy path and error path
if no data return null
-----------------

// IIFE za if i switch inside JSX
// https://stackoverflow.com/a/60502190

<View style={styles.container}>
	{(() => {
	  if (this.state == 'news'){
		  return (
			  <Text>data</Text>
		  )
	  }
	  
	  return null;
	})()}
</View>
-----------------
skeletoni moraju da se pokazuju u jednom sloju, moze u vise komponenata
organism level
----------------

sto bi pravio komponentu koja vraca null i zvao je u render, 
napravi use hook fju i pozovi je kao fju i to je to
------------------
//call hook inside the handler
the problem is you should have the hook outside of the handler, and the hook returns a function you can call inside the handler
iz hooka vracas async fju koja hendluje kontinuaciju, a ne state

export function useCollectRecipientInfo(): () => Promise<boolean> {

	const collectRecipientInfo = async (): Promise<boolean> => {
		await something1();
		return false;
		await something2();
		return true;
	}
	
	return collectRecipientInfo;
}

// usage
const collectRecipientInfo = useCollectRecipientInfo();

async function handleAddRequest() {
	await collectRecipientInfo();
	CalendarUtil.add();
}

funkcija je scope za kontinuaciju
-----------------
uncontrolled input ne sme da ima defined value, ili ce biti harkodiran na tu vrednost, empty string npr
only undefined
---------------
// types of state
// https://leerob.io/blog/react-state-management

1. UI State – State used for controlling interactive parts of our application (e.g. dark mode toggle, modals).
2. Server Cache State – State from the server, which we cache on the client-side for quick access (e.g. call an API, store the result, use it in multiple places).
3. Form State – The many different states of a form (e.g. loading, submitting, disabled, validation, retrying). There's also controlled & uncontrolled form state.
4. URL State – State managed by the browser (e.g. filter products, saving to query parameters, and refreshing the page to see the same products filtered)
5. State Machine – An explicit model of your state over time (e.g. a stoplight goes from green → yellow → red, but never green → red).

Redux Toolkit, Immer...
----------------
// error boundary
// ReactJS Tutorial - 32 - Error Boundary
// Codevolution
// https://www.youtube.com/watch?v=DNYXgtZBRPE
// Failing Gracefully with React Error Boundary
// https://www.youtube.com/watch?v=hszc3T0hdvU
class component
bar 1 od 2 lifecycle metoda:
getDerivedStateFromError(error) - postavlja stanje kad detektuje gresku
componentDidCatch(error, errorInfo) - for loging
umotas sta hoces tom komponentom i ona prikaze fallback za gresku
ogranienja: ne moze za event handlere, async code, za gresku u samoj sebi, i server side rendering
-----
react-error-boundary paket moze i za ogranicenja
fallback prop prosledis komponentu sta da prikaze za gresku
fja = useError hook pa fju zoves u catch u event handler, i onda prikaze fallback i za exception iz handlera
------------------
// 7 React Beginner Mistakes and How to Fix Them
// https://www.youtube.com/watch?v=Y1jysjm4c1Q
// key prop za jsx array - odlican demo bez objasnjenja
1. Naming components in lower-case - lowercase tag u jsx baca gresku
2. Importing Named exports as Default exports
3. Incorrect useState setter-function usage - invoke handler umesto fn ref
4. Mutating objects & arrays - novi object uvek, nikad obj.x = 2
5. Asynchronous useState - state mozes da citas samo u useEffect i render
6. Using Index as keys for list elements - odlican demo bez objasnjenja
7. Memoizing objects with useMemo - treba i React.Memo i useMemo na prop da spreci rerender childa
------------------------
redux - u applikaciji imas 1 error state, a ne 1 error state po reduceru
----------------------
----------------------
// clasnames and bem
const blockClasses = classnames([
  blockName,
  { [`${blockName}--${variant}`]: variant },
  { [`${blockName}--size-${size}`]: size },
  { [`${blockName}--checked`]: isChecked },
  { [`${blockName}--disabled`]: disabled },
  { [`${blockName}--focus`]: focused || focus },
  { [`${blockName}--has-error`]: hasError },
  { [`${blockName}--has-warning`]: hasWarning },
  { [`${blockName}--has-success`]: hasSuccess },
  className || '',
]);

// https://stackoverflow.com/questions/45601999/issue-with-m-in-bem-with-react-css-module
// https://github.com/azproduction/b_
--------------------
forma ima state // zapazi
-------------------
// useReducer - kad je state objekat, i kad state zavisi od prethodnog state // i to je to
const [actualState, dispatch] = useReducer(reducer, initialState);
const initialState = SomeValue;
const reducer = (state, action) => {
    switch(action)
      case 'toClick':
         return newState
    default:
      throw new Error("Unexpected action");
}
---
const initialState = {
  color: 'blue',
  text: 'hello',
  theme: 'light',
};
dispatch({type: 'blue'}); // type je samo ime akcije, nezavisno od state, moze i da se poklapa
const reducer = (state, action) => {
  switch(action.type)
    case 'blue':
       return {
        color: 'blue',
        text: 'something',
        theme: 'dark',
      };
  default:
    return state;
};
--------------------------
// povratne vrednosti hooka trigeruju rerender, to su stateovi u hooku
const { data: session, status } = useSession();
session, status // kad se menjaju rerenderuju komponentu
---------------------------
// Why custom react hooks could destroy your app performance
// https://www.developerway.com/posts/why-custom-react-hooks-could-destroy-your-app-performance
state u hooku trigeruje rerender komponente u kojoj je hook
---
// useMemo, useCallback imaju samo dependency array da filtriraju na koji render se izvrsava
// to unutra, i to je to, ok
// funkcija
const close = useCallback(() => {
  setIsOpen(false);
}, []); // eto
// value
const Dialog = useMemo(() => {
  return () => <ModalBase onClosed={close} isOpen={isOpen} ref={ref} />;
}, [isOpen, close]); // eto
// component
React.memo()
---------------------------
// react state is async
const [counter, setCounter] = useState(0);
const handleClick () => {
  setCounter(counter + 2)
  setCounter(counter + 2) // inkrementira za 2, ne za 4 jer state je async
}
const handleClick () => {
  setCounter(prevState = prevState + 2)
  setCounter(prevState = prevState + 2) // inc za 4, prev arg jedini nacin da dobijes stvarni value
} // jer state je async, pa callback
--------------------
// pass props to {children}
// https://stackoverflow.com/questions/32370994/how-to-pass-props-to-this-props-children
// pass to multiple children components
function Parent({ children }) {
  function doSomething(value) { }
  const childrenWithProps = React.Children.map(children, child => {
    // Checking isValidElement is the safe way and avoids a typescript error too.
    if (React.isValidElement(child)) {
      return React.cloneElement(child, { doSomething });
    }
    return child;
  });
  return <div>{childrenWithProps}</div>
}
---
<Parent>
  <Child value={1} />
  <Child value={2} />
</Parent>
----------------------
// pass single onClick prop to single child
const children1 = React.cloneElement(children, { onClick });
<span className={b('content')}>
  {children1}
</span>
----------------------------
// useImperativeHandle
useImperativeHandle allows you to determine which properties will be exposed on a ref.
---
// https://stackoverflow.com/questions/37949981/call-child-method-from-parent
// The component instance will be extended
// with whatever you return from the callback passed
// as the second argument
useImperativeHandle(ref, () => { 
  return {
    getAlert: () => {
      alert("getAlert from Child");
    }
  }
},[deps]);
// ne mora na DOM da kacis sa useImperativeHandle, pokazuje dorektno na component.fja()
<Child ref={childRef} />
// zoves sa
childRef.current.getAlert()
------------------
// call the function from child in a parent
------------------
// useEffect without dependencies array
// https://www.reddit.com/r/reactjs/comments/t5icvj/what_is_the_purpose_of_useeffect_without_a/
1. useEffect runs after the component render. So the component will be 
fully rendered before the side effect is applied.
2. With useEffect, you can also do a clean up.
-------------------
// useEffect vs useLayoutEffect
// https://kentcdodds.com/blog/useeffect-vs-uselayouteffect
// useLayoutEffect ako menjas dom iz efekta
However, if your effect is mutating the DOM (via a DOM node ref) and the DOM mutation 
will change the appearance of the DOM node between the time that it is rendered and your 
effect mutates it, then you dont want to use useEffect. Youll want to use useLayoutEffect
------------------------
// https://blog.whereisthemouse.com/good-practices-for-loading-error-and-empty-states-in-react
// https://codesandbox.io/s/react-suspense-example-ievxn?file=/src/Table.js
ui ima 4 states: success, loading, error, and empty state // zapazi empty state
---
const TableComponent = () => {
  const { data, isLoading, isError } = useCats();

  if (isError) {
    return <ErrorState />;
  }

  // dont show spinner if there is old data
  if (isLoading && !data) {
    return <Spinner />;
  }

  // empty state
  if (data?.length === 0) {
    return <EmptyState />;
  }

  // success state
  return (
    <>
      {/* show old data and progressbar */}
      {isLoading && (
        <Progress size="xs" isIndeterminate w="100%" position="fixed" top="0" />
      )}
      <Table colorScheme="blue" overflow="none">
        ...
      </Table>
    </>
  );
};
---------
// Suspense je komponenta sa loading fallback (jedan globalni) kao error boundary, react 18.
import React, { Suspense } from "react";
import ErrorBoundary from "./ErrorBoundary";

<ErrorBoundary fallback={<ErrorState />}>
  <Suspense fallback={<LoadingState />}>
    <Table />
  </Suspense>
</ErrorBoundary>

// onda ostaje samo empty state i success
if (cats?.length === 0) {
  return <EmptyState />;
}
return (
    <Table colorScheme="blue" overflow="none">
      ...
    </Table>
);
--------
// naravoucenije: u svakom scenariju jasno identifikuj stateove, odatle sve polazi
---------------
// Warning: Cant perform a React state update on an unmounted component. 
// This is a no-op, but it indicates a memory leak in your application. 
// To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function.
//
// https://stackoverflow.com/questions/64201465/how-to-resolve-cant-perform-a-react-state-update-on-an-unmounted-component
// 2 fore, ili global isMounted, ili try {} catch() {} ako je axios, zapazi
// 1.
export default function App() {
  let isMount = false;
  const [info, setInfo] = useState([]);

  useEffect(() => {
    isMount = true;
    async function loadNews() {
      const response = await api.get('/arts.json');

      const { results } = response.data;

      console.log(results);
      setInfo(results);
    }

    if (isMount) loadNews();
  return ()=>{isMount = false};
  }, []);
  ----
  // 2.
  useEffect(() => {
    const loadNews = async () => {
      try {
        const response = await axios.get("arts.json");
        const { results } = response.data;
        console.log(results);
        setInfo(results);
      } catch (err) {
        // Handle Error Here
        console.error(err);
      }
    };
  
    loadNews();
  }, []);
-------------------------
// custom hook je komponenta koja ne vraca jsx nego promenljive - vrlo jasno
--------------------
// useTransition() react 18
// React 18 Fundamentals Crash Course 2022 - Codevolution
// https://www.youtube.com/watch?v=jLS0TkAHvRg&t=5810s
nema kod na github
slicno kao debounce, SAMO za performanse kod heavy computation
da razdvoji input field i listu da se input brze renderuje a lista ima isPending kao loading
----
// prekucano rucno
// dupli state za 1 input
const [query, setQuery] = useState(''); // heavy
const [inputValue, setInputValue] = useState(''); // samo input decoupled
const [isPending, startTransition] = useTransition();

function changeHandler() {
  setInputValue(event.target.value); // lagan
  startTransition(() => { setQuery(event.target.value); }) // tezak
}

// heavy computation function, referencira query state
const filteredNames = allNames.filter(item => { return item.firstName.includes(query)});

<input type="text" value={inputValue} onChange={changeHandler}/>
{isPending && <p>Updating list...</p>} // loading...
// render
{
  filteredNames.map(item => <p key={item.id}>{item.firstName}</p>)
}
------
// ovako nesto iz docs
function App() {
  const [isPending, startTransition] = useTransition();
  const [count, setCount] = useState(0);
  
  function handleClick() {
    startTransition(() => {
      setCount(c => c + 1); // state koji uzrokuje heavy computation, i referenced in heavy computation function
    })
  }

  return (
    <div>
      {isPending && <Spinner />}
      <button onClick={handleClick}>{count}</button>
    </div>
  );
}
-----------------
// useDeferredValue isto kao startTransition ali samo direktno value debounced
// https://academind.com/tutorials/react-usetransition-vs-usedeferredvalue
const deferredProducts = useDeferredValue(products);
-----------------
// ReactElement vs JSX.Element vs ReactNode
// https://stackoverflow.com/questions/58123398/when-to-use-jsx-element-vs-reactnode-vs-reactelement
prouci to jednom za uvek ili pitaj
----------------
// prevent Firefox for prepopulating saved password
<input autoComplete="new-password" />
--------------------
// ReactElement vs ReactNode vs JSX.Element - jednom za uvek
// https://stackoverflow.com/questions/58123398/when-to-use-jsx-element-vs-reactnode-vs-reactelement
1. ReactElement - an object with a type and props - komponenta prakticno, ili html tag
2. ReactNode - ReactElement + ReactFragment, string, number, array of ReactNodes, null, undefined, boolean
type ReactText = string | number;
type ReactChild = ReactElement | ReactText; // children
type ReactFragment = {} | ReactNodeArray;
type ReactNode = ReactChild | ReactFragment | ReactPortal | boolean | null | undefined; // eto
3. JSX.Element - ReactElement (1), with the generic type for props and type being any
global namespace, ne treba import, + drugi frameworci
interface Element extends React.ReactElement<any, any> { }
-------
class_ component returns render(): ReactNode;
functional component returns ReactElement | null;
// fn komponenta, evo tip
(props: P & { children?: ReactNode }, context?: any) ReactElement | null;
-------
// sustina, ok
JSX.Element - komponenta ili tag, generalno (za framework)
ReactElement - JSX.Element za React
ReactNode - ReactElement + primitivni = null, undefined, boolean, string, number...
-----------------
// Suspense
// https://17.reactjs.org/docs/concurrent-mode-suspense.html // dobar clanak, procitaj fetching primere
Suspense lets your components WAIT for something before they can render. 
---
// ok
Fetch-on-render (for example, fetch in useEffect) // 1. render 2. fetch - waterfalls - sekvencijalno
Fetch-then-render (for example, Relay without Suspense) // 1. fetch 2. render - Promise.all([...])
Render-as-you-fetch (for example, Relay with Suspense) // paralelno fetch i render
-----
sta triggeruje suspense??? ajax dovoljan?
------------------
// virtual dom
reconciliation - proces sinhronizovanja virtualnog i realnog dom-a
diffing - poredjenje sta je promenjeno izmedju 2 virtualna dom-a
drze se 2 kopije virtuelnog DOMa, jedna polazna i jedna koja se menja
----------------
// zapazi
render should be a pure function, no side effects
--------------------
// useMemo vs useCallback
// https://dmitripavlutin.com/react-usememo-hook/
// https://dmitripavlutin.com/dont-overuse-react-usecallback/
const factorial = useMemo(() => factorialOf(number), [number]);
const memoizedCallback = useCallback(callback = () => { return 'Result';}, [prop]);
// useMemo caches RETURN VALUE of expensive (external) function
// useCallback caches function DEFINITION (function reference) of expensive function
use case for useCallback - cache function_ prop for memoized component with React.memo(MyBigList);
------
treba kesirati samo sto profiler pokaze
--------------
// profiler tutorial
// https://developer.chrome.com/docs/devtools/evaluate-performance/
------------------
// stale closure 
// kad ne ukljucis promenljivu ili funkciju u dependency array, nego radi sa starom vrednosti
// https://tkdodo.eu/blog/hooks-dependencies-and-stale-closures
useCallback, useEffect, React.memo
------------------
// fora za resavanje bugova
// https://github.com/facebook/react/issues/24476#issuecomment-1127800350
kad nesto ne mozes da resis sa {uslov && children} zbog treptanja (navbara npr)
kesiraj to sa useMemo pa ce uvek biti sadrzaja

const MeProvider: FC<ProviderProps> = ({ children }) => {
  const { data } = useMe();
  const me = data ?? null;
  const memoChildren = useMemo(() => children, [me]);

  return <MeContext.Provider value={{ me }}>{memoChildren}</MeContext.Provider>;
};
----
// https://www.amitmerchant.com/the-new-starttransition-api-react-18/
startTransition(setState(value)) // prima SETTER koji izaziva veliku promenu
// poenta je STATE koji IZAZIVA veliku promenu
----------
// zapravo poenta je da izostavis iz dependencies to sto kesiras, nego u [] samo trigeri kad da updateujes
// ovde [children] izostavljen
const memoChildren = useMemo(() => children, [me]);
---------------------
// zasto funkcija mora u dependencies u useEffect
// jer moze razlicite rezultate na svaki render da vrati
In that case, it’s absolutely necessary to add it to the dependency array because 
what it returns may be different from one render to another as far as useEffect knows
------------------
// create type from zod schema
// https://www.flavienbonvin.com/amazing-nextjs-libraries-that-makes-coding-easier/
export type User = z.infer<typeof userParse>
-----------------------
// React Hook Form - uncontrolled inputs
// Formik - controlled, rerenders entire form on every key type
React Hook Form uses uncontrolled inputs which means it doesnt use states 
to observe change in input fields so it doesnt re render on every change 
you make in the input field.
---
On the other hand Formik re renders the whole form when you are typing in 
a single field because it uses controlled inputs, that is, using react state to 
observe changes in input field.
-----
This is the reason RHF is way more faster than Formik.


