vazan je build.gradle u app folderu, onaj u spoljnom folderu manje vazan
u settings.gradle idu relativne staze od android foldera do android foldera u node_modules koji unutra imaju build.gradle, to su lib projekti
----------
TouchableOpacity je link tj <a> tag, umotas text ili bilo sta i postane navigabilno, link
----------
//graphql
ima Query. Mutation i Subscription timpove, a sve ostalo su mu podtipovi da napravi ove glavne tipove
//apollo
reducer je funkcija koja transformise objekat iz jednog oblika u drugi, i vraca kopiju da bi bio pure, kao projector kojisamo cita
//resolver
fieldName: (parent, args, context, info) => data;
parent - rezultat koji je vratio prethodni resolver, tj za taj tip, ili root;//ne nego taj tip za koji je resover???

//https://medium.com/paypal-engineering/graphql-resolvers-best-practices-cd36fdbcef55
parent type je parent u queryju query { nestoParent{nestoDete{email...}}} jer se to parsuje u ast stablo, query je root stabla
    root — Result from the previous/parent type
    args — Arguments provided to the field
    context — a Mutable object that is provided to all resolvers
    info — Field-specific information relevant to the query (used rarely)
resolveri su prosto funkcije sa 4 default argumenta koje citaju i pisu sta treba u bazu, tj rade sa bazom
upisuju u bazu sta treba i vracaju objekat koji treba apiju tj tip objekta po schemi
//default resolver
export default {
	Event: {
		title:(root, args, context, info) => root.title
	}
}
The root argument is for passing data from parent resolvers to child resolvers.
Query je root, u njemu fetchujes objekat, taj objekat se prosledjuje nanize niz stablo i fje kao root, 
ako taj objekat sto si vratio vec ima property title onda je on by default title, bez da sam fetcujes title na osnovu api, 
ali bolje sve do listova sam da fetchujes
ako je u schemi chat: Chat, tip=property, a ne myChat: Chat, ili chat: String, pros, trivijalan, eto
---
query GetLaunchById { # uvek pocinje sa query ili mutation, GetLaunchById je ime samo ovog queryja ovde konkretno, trivijal
  launch(id: 60) { # launch je query iz scheme, id je argument
    id				# polja...
    rocket {
      id
      type
    }
  }
}
-----
sa query promenljivom umesto 60 hardkdirano
query GetLaunchById($id: ID!) { # paste { "id": 60 } into the Query Variables section below 
  launch(id: $id) {
    id
    rocket {
      id
      type
    }
  }
}
-----
mutation identicna sintaksa ka query osim kljucne reci mutation
mozes header u graphiql da postavis dole negde
paste our authorization header into the HTTP Headers box at the bottom:

{
  "authorization": "ZGFpc3lAYXBvbGxvZ3JhcGhxbC5jb20="
}
-------------
1:n userId ide u group tabelu, a ne obrnuto, fora, gresio 2 puta, i u seed ide 
firstUser.setGroup(secondGroup),
//connectors
GroupModel.belongsTo(UserModel);
UserModel.hasOne(GroupModel);
//schema
type Group {
    owner: User
//resolver
owner(group) {
  return group.getUser();
----
za css koristi backgroundColor:'red' za flex da vidis element dokle ide, i u debuger tools direktno
--------------
//postgres
i imena kolona case sensitive groupuser."userId"
left join, inner, outer na 2 KOLONE, natural join iz relacione algebre
------------------
subskripcija ide samo kad nemas event, inace ide samo refetch, kad imas event ili klik, ili pooling samo stavis interval ms
npr drugi user salje poruku na chet
---
ne moras cache da updateujes posle mutacije, dovoljno je da refetchujes query
//https://github.com/apollographql/apollo-client/issues/1900
mutate({
  mutation: LOGIN_MUTATION,
  variables: {
    ...values
  },
  refetchQueries: [`currentUser`]
})
---
da menjas sadrzaj na susednom screenu jedino da write cache posle mutacije za query koji je na tom screenu, cache je globalni state
da bi updateovao moras variables da ponovis u cache.writeQuery
---
ovo ime queryja se koristi npr u refetchQueries mutacije
refetchQueries: ['GroupsQuery'],
export const GROUPS_QUERY = gql`
  query GroupsQuery($userId: Int!) {
    groups(userId: $userId) {
      id
   ...
    }
  }
`;
---
ime funkcije je ime kljuca u object literalu, zato izgleda nejasno
const pageInfo = {
  async hasNextPage() {
je isto sto i
const pageInfo = {
  hasNextPage: async function() {
--------------------
--------------------
graphql union, jedan od nekoliko, da ne moraju ostali da budu null
za error npr
//https://www.youtube.com/watch?v=wBrSXBpAd10
... on Character {
//samo 1 ce da se vrati
{
  search(contains: "") {
    ... on Book {
      title
    }
    ... on Author {
      name
    }
  }
}
------------
interface
title zajednicki i ili classes ili colors
//https://www.apollographql.com/docs/apollo-server/schema/unions-interfaces/
query GetBooks {
  schoolBooks {
    title
    ... on TextBook {
      classes {
        name
      }
    }
    ... on ColoringBook {
      colors {
        name
      }
    }
  }
}
--------------
//to learn
apollo docs local state managment
jest, apollo, react testing library, testing
git reset, merge, branch, stash
