// https://vercel.com/blog/how-react-18-improves-application-performance
50ms+ -> long task
60fps - 16ms, 33ms ostaje za druga izracunavanja
-------
visual update in React: // OVE FAZE SU JAKO VAZNE
1. render phase 
2. commit phase.
------
// render phase:
pure computation phase 
create virtual dom
update virtual dom
calc difference (reconciliation, change detection)
new virtual dom
------
// commit phase:
React apply calc updates to actual DOM
-------
// synchronous render
ceo tree same priority
all-or-nothing operation
main thread is blocked, unresponsive UI
--------
// chrome profiler
red corner tasks are considered "long tasks"
dole levo - total blocking time of 4425.40ms
--------
// React 18, concurrent renderer
can stop render, do other (unblock ui), resume render // to, zapazi
-----
mark certain renders as non-urgent
deo tree marked non-urgent, tad vraca na main thread za computations (ili render dr components), render lower priority
----
nije vise single non-interruptible task nego yields control back to the main thread (za user interaction)
----
concurrently render multiple versions of the component tree in the background without immediately committing the result
rendering tasks with priority
React can pause and resume the rendering of components based on external events such as user interaction // ovo
---------------
// transitions - less important state updates, da su sync bio bi ui lag
poenta: u startTransition cb stavis setState() sto stvara lag a nije vazno // to
o states se prica
const [isPending, startTransition] = useTransition();
onClick={() => {
    urgentUpdate(); // seState()
    startTransition(() => {
        nonUrgentUpdate() // tu
    })
}}
render za startTransition() ce da radi u background thread paralelno // zapazi
poenta2: startTransition otkazuje commit fazu, render faza u background thread
total blocking time: 105ms
// poenta3: 
renders (prepares) concurrently - parallel
priority
---
multithreading prakticno da oslobadja main thread
---------------------
// react server components - RSC
kao websocket na "/rsc" ruta
salje specijalan format (nije html i js), react zna da update tree sa tim
nema window, useState, useEffect
-------
// client components - CC
"use client" - iz react
dodaje u js bundle, i enable hydration
next.js will prerender client components u html, default react will use csr
--------
RSC kao children u CC ("use client") umesto import, da se izbegne RSC u bundle
-----
u next.js CC je SSG sa client hydration, ako je bar jedan parent RSC // linkedin, zapazi
-------
RSC - performance
CC - interactivity
-----------------------
// suspense - optimized loading
delay rendering and show fallback
suspense handles loading for RSC // zapazi
<Suspense fallback={<Skeleton />} />
dok suspense renders fallback react radi drugo u background, ne ceka data
---
react slusa interakciju pa render gde se klikce...?? // nejasno
-----------------------
// data fetching
cache() fja,  within the same render pass // zapazi
import { cache, useCallback, useDeferredValue, useEffect, useLayoutEffect, useMemo } from 'react'
export const getUser = cache(async (id) => {})
fetch je wrappovan sa cache by default
-----------
reuse difinition da kesira u svim komponentama
----
gde je queryKey (kao react-query)? default ime fje i args verovatno
-------------
sve radi u next.js app folder
----------------------
//  8 React Js performance optimization techniques YOU HAVE TO KNOW! - xplodivity
// https://www.youtube.com/watch?v=CaShN6mCJB0
windowing // nije lose, vidljiv deo liste
lazy loading images 
memoization // memo, useMemo, useCallback
throttling and debouncing Events // vs, razlika?
code-splitting 
react fragments // bzvz
web workers 
useTransition hook 
--------
// za memo, useCallback
check if same js instance? // ===
get obj pointer value in js? // ne moze
------
throttling - jednom u periodu // nije vazno kad je bio zadnji poziv // ok
debouncing - jednom posle proslog poziva
---------------------------
---------------------------
// george moller React slides
useMemo, useCallback se koriste da naprave stable js references za memo() kad se pustaju kao props
----
// useMemo vs useCallback // eto, jasno
useCallback stabilise function_ prop skupe komponente SA memo, onClick npr, u parent fn se redefinise na render, new js ref
useMemo stabilise rezultat skupe fje
----------
render - poziv fje, komponente
-----
useEffect(() => { ... }) // after every render
-----
// useDeferredValue isto ko debounce
// za spor <input onChange={...} />
const deferredSearch = useDeferredValue(search) // search je state
React will abandon the current render (with expensive calc) and restart with the new value
---------
ne zaboravi react ima trigger, render i commit faze // vrlo vazno kako radi, podseti se
---------
redux reducers - omogucava da jasno napises prelaze stanja konacnog automata
---------
// grupisanje state u object
kada se vrednosti zajedno menjaju (setState()) onda ide objekat {...}
const [position, setPosition] = useState({x:0, y:0})
--------
// reset state with key
<Form key={Math.random()} />
---------
// dont read/write ref values in render but in useEffect i event handlers // zasto?, dole objasnjenje
myRef.current = 123; // write
return <div>{myRef.current}</div> // read
---
// https://react.dev/reference/react/useRef - pitfal pasus
da bi komponenta ostala pure i zavisi samo od input (props, state, context)
za isti ulaz vraca isti izlaz - jsx
ref je mutable, sideeffect
------------
// builtin hooks
useEvents(callback) - extract non-reactive effect logic into an event function_ //... ??
const deferredValue = useDeferredValue(value) // debounce
const [isPending, startTransition] = useTransition() // less important state updates
startTransition(() => { setCount(c => c + 1) })
const id = useId() // unique id...??
useInsertionEffect(() => styles) // insert styles into dom ??
const state = useSyncExternalStore(subscribe, getSnapshot) // subscribe to external state ??
useEffect() // synchronize external state, zanimljivo
useLayoutEffect() // read dom state
useMemo() - skip recalc on render, stabilize prop for React.memo(), stabilize dependency for useEffect or useMemo...
const searchOptions = { text } // novi obj ref na svaki render (isto ko fn), zapazi, mora 2 useMemo
useCallback() - stabilize function_ ref for React.memo()
----------
// OVE FAZE SU JAKO VAZNE
// phases 
1. trigger, queue render (initial render or state update)
2. render, exec component
3. commit, apply rendered, virtual dom to real dom
---------
// nikad broj levo od && ili 0
number && ... // lose
number > 0 && ... // ok
----------
// fn props iz parent
nikad u useEffect vec direktno u event handler, da izbegnes parent render
// kraj react slajdova
------------------------------
------------------------------
// Introducing the React Profiler 2018
// https://legacy.reactjs.org/blog/2018/09/10/introducing-the-react-profiler.html
// ovo je react dev tools, a ne chrome profiler, zapazi
za profiler tab record mora dev build or profiler prod build
pre snimanja u elements tab selektujes root elem applikacije
------
// react radi u 2 faze:
1. render
2. commit - apply, react-dom paket, effecti rade u ovoj fazi
----
bar chart su commit-ovi // zapazi
filter ostavlja samo duge commits
---------
// flame chart
bar je komponenta, pa hijerarhija cele app, tree, parent - childern
----
sirina - time in last render
boja - time in selected commit (bar u chart)
---
zuto dugo, plavo kratko, sivo nije render u selected commit
---
click na komponentu zumira nju, a desno pokaze state i props
shift commits, vidis koja promena state triggered rerender
---------
// ranked chart - radio za sortiranje
sortira komponente opadajuce, za 1 commit
---------
// component chart
dupli klik na komponentu ili plavo dugme desno dok je componenta selktovana
koliko puta 1 komponenta se renderovala u celom snimku 
---------
// interactions
uzrok za rerender
munja radio
loguje se desno za komponentu
-----------------
// docs video 2018, bezveze
-------
// novi docs video
// React Developer Tooling
// https://www.youtube.com/watch?v=oxDfrke8rZg
gore commits, dole components
tooltip pokazuje zasto je rerender
profiler snimak moze da se snimi u fajl i posalje, strelice levo, load i save
-----
// timeline
profiler prikazuje samo commits, ignorise non-react javascript, data fetching (suspense)
zato timeline tab
zapravo u ta 2 moze da bude vecina vremena, a ne u commits (profiler)
------------
nova dokumentacija nema o profiler, samo stara
-------------------------
// https://www.developerway.com/posts/react-project-structure
nema na jednom mestu cela struktura, iz delova
ovo nije toliko kompletno
// https://github.com/developerway/example-react-project
-------
package - folder sa package.json
monorepo package je linked with Node symlinks to node_modules gde su obicni paketi - workspaces // eto
----
// package.json u workspace
{
    "name": "@project/my-feature", // ime paketa
    "main": "index.ts" // ovaj fajl se importuje kad importujes paket, import { Something } from '@project/my-feature';
}
---
// razlika, workspace vs folders
// package
import { Button } from '@project/button';
// folders
import { Button } from '../../components/button';
-------------------
// root package.json, sve sto treba za monorepo
{
    "private": true,
    "workspaces": ["packages/**"]
}
---------
// za private repos, koji se ne objavljuju na npm
iste versions u local are hoisted to root
local package moze da koristi dependency iz root package.json, a u local bude undefined
----------
// https://www.developerway.com/posts/positioning-and-portals-in-react - dobar clanak
// stacking context
elem sa position relative ili absolute izlazi na vrh
z-index default 0, moze negativan
position, transform (animations) kreiraju novi stacking context
---
kombinacija position relative, absolute, overflow:hidden klipuje element
---
problem: modal ima position absolute, ali neki izmedju parent ima position:relative, pa nije u odnosu na root
---
position: absolute - parent
position: fixed - viewport, zapravo Containing Block (default viewport), transform kreira novi, i drugi...
------
nezeljeni stacking contexti
sticky headers - position + z-index ili transform:translate
----
jedino resenje:
modal element nema parent sa stacking context, zakaci ga na root - portal, da izbegne
-----------
// portal dolazi iz react-dom
import { createPortal } from 'react-dom';
// createPortal(sta, gde) - prima 2 args
1. sta - component (modal)
2. gde - parent - kao appendChild, elem node, ne id
{isVisible && createPortal(<ModalDialog />, document.getElementById("root"))}
u jsx je ovde, u dom je zakacen na root // ok, ono sto znam
// rezultat:
modal je centered i iznad header
// poenta: 
modal node moze da bude gde god jer se pozicionira sa position:absolute dok god nije u SC, root najbolje
-----
// poenta: 
// sav react nepromenjen, sve dom promenjeno (css, native events)
1. sa React strane sve nepromenjeno // to
<Modal /> jeste tamo gde je u jsx, props, events, context, unmount (show && <Component />), rerender, itd
sto ga je React zakacio na drugo mesto nema veze
synthetic events "bubble" through React tree, not through DOM tree // zapazi
2. sa DOM strane sve promenjeno // zapazi
css scoping, dom events sa element.addEventListener(), form submit event
form submit event je native dom event, not React // zapazi
<form /> tag mora da dodje u modal, ne u main elem
---------
// njen rezime
position: absolute positions an element relative to a positioned parent // jeste zapravoz, !== static dodje na to
position: absolute elements will be clipped inside the overflow: hidden elements // ok za clip
nothing can escape the Stacking Context
--------------------------
// https://www.developerway.com/posts/pure-components-vs-functional-and-hooks
poenta: kako da skip nepotreban rerender child ako se parent rerender
----
// class components
// shouldComponentUpdate, za false skip render
// compare current and next props and state
shouldComponentUpdate(nextProps, nextState) {
    // re-render component if "someprop" changes
    if (nextProps.someprop !== this.props.someprop) return true;

    // re-render component if "somestate" changes
    if (nextState.somestate !== this.state.somestate) return true;
    return false;
  }
poziva se cak i kad local state stoji na isto
-------
// pure component implements shouldComponentUpdate by default
class PureChild extends React.PureComponent {}
-------
// fn components
// ekvivalent to extends React.PureComponent
za primitive props
export const PureChild = React.memo(Child);
-----
ako je prop funkcija onClick ref se menja, mora drugi arg, comparison fja
samo props, nema state da se poredi
// exclude onClick from comparison
const areEqual = (prevProps, nextProps) => prevProps.someprop === nextProps.someprop;
export const PureChild = React.memo(Child, areEqual);
---
React.memo = PureComponent + shouldComponentUpdate
---
// kad je u klasi fja ref je stabilna, bound to class // zapazi, za class
class Parent extends React.Component {
    onChildClick = () => {
      // do something here
    }
}
// fn component equivalent, [] - zivot komponente
const onChildClickMemo = useCallback(onChildClick, []);
-------
updater function_ - callback u setState // eto
sluzi da izegnes state u dependencies u useCallback (ili useEffect)
-------
// zar nije lakse useMemo na dependecies of useCallback?
ref je kao state, drzi vrednost kroz rerender ali ne trigger rerender, i mutable je
// mirror state in ref, and include ref in []
const [counter, setCounter] = useState(1);
const mirrorStateRef = useRef(null);

useEffect(() => {
  mirrorStateRef.current = counter; // ovde dodeli
}, [counter])

const onChildClick = () => {
  if (mirrorStateRef.current > 100) return; // ovde koristi ref
  // do something here
}

const onChildClickMemo = useCallback(onChildClick, []);
---------
// stabilisanje ref types u kombinaciji sa React.memo()
dobra poenta:
za functions - useCallback
za array i objects - useMemo
za primitive types - nista
---
// this recreates [] and rerenders always
<PureChild someArray={[1,2,3]} />
// fixed
const someArray = useMemo(() => ([1,2,3]), [])
<PureChild someArray={someArray} />
--------
bailing out from state updates - setState na postojecu vrednost ne trigger rerender u fn components (za rzliku od class)
supported natively in useState hook
---------------------------
// https://www.developerway.com/posts/fetching-in-react-lost-promises
clanak je o promise race conditions sa bare fetch ili axios
// rerender, moze race
const [page, setPage] = useState("1");
<button onClick={() => setPage("1")}>Issue 1</button>
<Page id={page} /> // rerender
// remount
const [page, setPage] = useState('issue');
{page === 'issue' && <Issue />} // remount &&, state is lost
---------------
// solve promise race
poenta: sve se svodi kako da cancelujes sve promise, a ostavis samo zadnji
// 1, nacin - prop (or url) in ref and compare
// update state samo ako je ispravan (zadnji) id prop
const ref = useRef(id);
useEffect(() => {
  // update ref value with the latest id
  ref.current = id;

  // ovo sad traje neko vreme
  fetch(`/some-data-url/${id}`)
    .then((response) => response.json())
    .then((response) => {
      // only update state if the result actually belongs to that id // eto
      if (ref.current === response.id) {
        setData(r);
      }
    });
}, [id]);
--------------
// 2. nacin, isActive closure in cleanup function
ovo sam radio kod rolanda a nisam ni znao zasto tacno
// only the latest run, that one that hasn’t been cleaned up yet, will have the variable set to true
useEffect(() => {
  let isActive = true;

  fetch(`/some-data-url/${id}`)
    .then((r) => r.json())
    .then((r) => {
      if (isActive) { // check here
        setData(r);
      }
    });

  return () => {
    isActive = false;
  }
}, [id]);
------------
// 3. nacin - AbortController
useEffect(() => {
  const controller = new AbortController();

  // pass controller as signal to fetch
  fetch(url, { signal: controller.signal })
    .then((r) => r.json())
    .then((r) => {
      setData(r);
    })
    .catch((error) => {
      // error because of AbortController
      if (error.name === 'AbortError') { // moras gresku da handlujes
        // do nothing
      } else {
        // do something, it's a real error!
      }
    });

  return () => {
    // abort the request here
    controller.abort();
  };
}, [url]);
------------------------------
// https://www.developerway.com/posts/how-to-fetch-data-in-react
sadrzaj clanka: spreci waterfall, spreci ceo app rerender and props drilling with context
-------
browser moze samo 6 requests in parallel // eto
-------
// waterfall
// 1. Promise.all() - in parallel, ali mora svi da se sacekaju
useEffect(async () => {
  const [sidebar, issue, comments] = await Promise.all([
    fetch('/get-sidebar'),
    fetch('/get-issue'),
    fetch('/get-comments')
  ])
}, [])
------
// isto parallel ali moze odvojeno da se prikazuju, ali 3 rerender
fetch('/get-sidebar').then(data => data.json()).then(data => setSidebar(data));
fetch('/get-issue').then(data => data.json()).then(data => setIssue(data));
fetch('/get-comments').then(data => data.json()).then(data => setComments(data));
---------
// context providers to avoid rerender all app
const Context = React.createContext();

export const CommentsDataProvider = ({ children }) => {
  const [comments, setComments] = useState();

  useEffect(async () => {
    fetch('/get-comments').then(data => data.json()).then(data => setComments(data));
  }, [])

  return (
    <Context.Provider value={comments}>
      {children}
    </Context.Provider>
  )
}

export const useComments = () => useContext(Context);
// poziv
const Comments = () => {
  const comments = useComments();
}
-----------
// fetch u fajl, pre komponente, global scope, a resolve u useEffect
const commentsPromise = fetch('/get-comments');
1. critical config data
2. lazy loaded components
// lazy-loaded components - downloaded and executed when used
lazy-loaded components javascript will be downloaded and executed only when they end up in the render tree, 
so by definition after all the critical data is fetched and rendered. So it’s safe.
-----------------------------
// https://www.developerway.com/posts/how-to-handle-errors-in-react
----
// poenta:
1. try/catch ne hvata u react
2. ErrorBoundary ne hvata async i event handlers
1 + 2 -> ErrorBoundary, uhvati sa try catch pa throw u react
-------------
// try/catch
primeri try/catch u react oko useEffect, ChildComponent // ne moze
------
ovo ne poziva komponentu, samo dodeljuje objekat, poziva je react kaad je renderuje u return_
const  child = <Child /> // definition object
--------
setState() u render izaziva endless loop
----------
// ErrorBoundary, declerative motanje
<ErrorBoundary fallback={<>Oh no! Do something!</>}> // obican prop
  <SomeChildComponent />
</ErrorBoundary>
----
// moras sam da je implementiras, samo class component
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false }; // error state
  }

  // if an error happened, set the state to true
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    log(error, errorInfo); // send error to somewhere here, onError
  }

  render() {
    if (this.state.hasError) {
      return <>Oh no! Epic fail!</>
    }

    return this.props.children;
  }
}
---
Error boundary catches only errors that happen during React lifecycle
errors in resolved promises, async code with setTimeout, various callbacks and event handlers - ignored
---
// primeri, dobar
useEffect(() => {
  // this one will be caught by ErrorBoundary component
  throw new Error('Destroy everything!');
}, [])

const onClick = () => {
  // this error will just disappear into the void
  throw new Error('Hulk smash!');
}

useEffect(() => {
  // if this one fails, the error will also disappear
  fetch('/bla') // async
}, [])
----
// resenje, try/catch i state
const [hasError, setHasError] = useState(false);

const onClick = () => {
  try {
    // this error will be caught by catch
    throw new Error('Hulk smash!');
  } catch(e) {
    setHasError(true);
  }
}
------
// forward async error to catch with ErrorBoudary
const [state, setState] = useState();

const onClick = () => {
  try {
    // something bad happened
  } catch (e) {
    // trigger state update, with updater function as an argument
    setState(() => {
      // re-throw this error within the updater function
      // it will be triggered during state update
      throw e;
    })
  }
}
----
// reusable resenje, hook
const useThrowAsyncError = () => {
  const [state, setState] = useState();

  return (error) => {
    setState(() => throw error)
  }
}
// poziv
const throwAsyncError = useThrowAsyncError();
throwAsyncError(e) // throw
----
// reusable wrapper, hof, decorator
const useCallbackWithErrorHandling = (callback) => {
  const [state, setState] = useState();

  return (...args) => {
    try {
      callback(...args);
    } catch(e) {
      setState(() => throw e);
    }
  }
}
// poziv
const onClick = () => {
  // do something dangerous here
}
const onClickWithErrorHandler = useCallbackWithErrorHandling(onClick);
<button onClick={onClickWithErrorHandler}>click me!</button>
----
// lib za ovo
https://github.com/bvaughn/react-error-boundary
----------------------------------
// https://www.developerway.com/posts/how-to-write-performant-react-apps-with-context
poenta:
1. split context na stati i callbacks, i dalje state ako treba
2. koristi useReducer, da moze update state sa dispatch bez state dependency u use Memo
svodi se na state management lib, state - selectors, dispatch - actions
-------
// primer:
state u root, props i callbacks nanize, sve rerender on input type
-------
components will re-render when: // SAMO u ova 2 slucaja
1. state of a component changed
2. parent component re-renders
----------
// context
const FormContext = createContext<Context>({} as Context);
// hook from context
export const useFormState = () => useContext(FormContext);
// provider - componenta
export const FormDataProvider = ({ children }: { children: ReactNode }) => {}
// upotreba
export const NameFormComponent = () => {
  const { onNameChange, state } = useFormState();
  // ...
}
----
// koje komponente se rerenderuju:
every consumer of this context will re-render, regardless of whether they use the changed value or not
zato bolje vise manjih contexta
medjukomponente se ne rerenderuju
---
// dalje, razdvoj state i callbacks u 2 contexta
const FormDataContext = createContext<State>({} as State);
const FormAPIContext = createContext<API>({} as API);
----
useMemo ima state dependency, pa zameni useState sa useReducer, dispatch ne treba state
const api = useMemo(() => {

  const onDiscountChange = (discount: number) => {
    dispatch({ type: 'updateDiscount', discount });
  };

  // ...

  return { onSave, onDiscountChange, onNameChange, onCountryChange };
  // no more dependency on state! The api value will stay the same
}, []);
moze i setState callback umesto useReducer - setState(state => ({ ...state, newValue: 'foo' }))
-------
// na kraju, dobar redux primer:
https://codesandbox.io/s/form-implementation-redux-2-t5w30?file=/src/form-api.tsx
-------------------------------
// https://www.developerway.com/posts/how-to-write-performant-react-code
// sadrzaj:
1. bezveze useCallback ne radi
2. razdvoj state i useMemo components koje zavise od delova state
3. never local componentes, remount // nepotrebno
4. useMemo context (provider) value, if not string, number, boolean
-----
useCallback umesto inline fje ne radi // eto
----
// memoize child to prevent rerender on parent rerender // works
const list = useMemo(() => {
  return <CountriesList />;
}, []);
-----------
// split state and memoize
const list = useMemo(() => {
  return (
    <CountriesList
      countries={countries}
      onCountryChanged={(c) => setSelectedCountry(c)}
      savedCountry={savedCountry}
    />
  );
}, [savedCountry, countries]);
--------------
// lokalna komponenta, nepotrebno
vrlo pogresno naravno, remount na rerender
1 mount kosta kao 10 rerender
-----------
// context
svaki put kad se value promeni svi consumers se rerenderuju
// resenje:
context value property should always be memoised if its not a number, string or boolean
----------
// rezime:
// samo 3 razloga za rerender:
1. state changed
2. parent component re-renders
3. when a component uses context and the value of its provider changes
eto, nema na props change
------------
// kad se ZAPRAVO koristi useCallback
1. za function_ props za React.Memo()
const MemoisedItem = React.memo(Item);
const List = () => {
  // this HAS TO be memoised, otherwise `React.memo` for the Item is useless
  const onClick = useCallback(() => {console.log('click!')}, []);
  return <MemoisedItem onClick={onClick} country="Austria" />
}
----
2. za function_ props koji su dependencies of useEffect, useMemo ili drugi useCallback
const Item = ({ onClick }) => {
  useEffect(() => {
    onClick(data);
  // if onClick is not memoised, this will be triggered on every single render
  }, [onClick])
  return <div>something</div>
}
const List = () => {
  // this HAS TO be memoised, otherwise `useEffect` in Item above will be triggered on every single re-render
  const onClick = useCallback(() => {console.log('click!')}, []);
  return <Item onClick={onClick} country="Austria" />
}
-------------------------------------
// https://www.developerway.com/posts/higher-order-components-in-react-hooks-era
opet se svodi na hof, odlozeni apply arguments 
withRedux, withRouter - they accept a component, inject some props into it, and return it back
----
hooks zamenili 90% hoc, 3 izuzetka
// 1. enhancing callbacks and React lifecycle events
type Base = { onClick: () => void };
export const withLoggingOnClick = <TProps extends Base>(Component: ComponentType<TProps>) => {
  return (props: TProps) => {
    const onClick = () => {
      console.log('Log on click something');
      props.onClick();
    };
    return <Component {...props} onClick={onClick} />;
  };
};
----
// 2. intercepting DOM events
event.stopPropagation();
----
// 3. context selectors i prevent rerender sa React.memo()
export const withFormIdSelector = <TProps extends unknown>(
  Component: ComponentType<TProps & { formId: string }>
) => {
  // memo
  const MemoisedComponent = React.memo(Component) as ComponentType<
    TProps & { formId: string }
  >;

  // nova komponenta
  return (props: TProps) => {
    const { id } = useFormContext();

    return <MemoisedComponent {...props} formId={id} />;
  };
};
------------------
// https://www.developerway.com/posts/debouncing-in-react
throttle i debounce su fje koje imaju timer, primaju fju i vracaju fju 
debounce ceka interval otkad stanes pa onda zove
throtle zove jednom u intervalu garantovano
-----
debounce is lodash, ista je logika i za throttle
----
// problem je sto su state i debounce() zajedno pa rerender i debounce se ponovo poziva
// ispada samo delay fja, r, re, rea, reac, react
const [value, setValue] = useState();
const onChange = (e) => { // debounced fja sets state
  setValue(e.target.value);
};
const debouncedOnChange = debounce(onChange, 500);
const sendRequest = (value) => {
  // send value to the backend
  console.log("Changed value:", value); // callback koristi zadnji state
};
-----
problem sa useMemo i useCallback sto imaju state kao dependency pa se pozivaju na svaki rerender
-----
// resenja: 
debounce van komponente, useCallback i useMemo, ref mutable
----
// konacno resenje, izvuci kompleksnost u hook, pa ga samo koristi
const useDebounce = (callback) => {
  const ref = useRef();

  useEffect(() => {
    ref.current = callback;
  }, [callback]);

  const debouncedCallback = useMemo(() => {
    const func = () => {
      ref.current?.();
    };

    return debounce(func, 1000);
  }, []);

  return debouncedCallback;
};
----
// poziv
const [value, setValue] = useState(); // state
const debouncedRequest = useDebounce(() => { // poziv hook
  // send request to the backend
  // access to latest state here
  console.log(value);
});
const onChange = (e) => {
  setValue(e.target.value);
  debouncedRequest(); // poziv debounced fn
};
-------------------------
// Fantastic closures and how to find them in React - Developer way
// https://www.youtube.com/watch?v=AhAGA5LUxek
sve fje u react componenti su closures
// stale closure (uzima vrednost iz prvog poziva)
const [value, setValue] = useState<string>();
const onClick = useCallback(() => {
  console.log(state) // stale closure, zamrznuta vrednost iz prvog rendera, mount
}, []); // bez state ovde, stale
-------
// stale closure sa ref, kad ima callback
const [value, setValue] = useState<string>();
const ref = useRef(() => {
  console.log(state) // stale closure, zamrznuta vrednost iz prvog rendera
});
// resenje, mutate ref in useEffect
useEffect(() => {
  ref.current = () => { console.log(value); };
});
-------
// resenja, 1. isolate state, 2. debounce, 3. ref
// resenje ref
// poenta: da se izbegne dependency u useCallback, ref ne ide u dependency i prezivljava render
// kad imas state u useCallback, pa [state] dependency rekreira fju
// closure zamrzava vrednost iz nekog (prvog) poziva
const HeavyComponentMemo = React.memo(HeavyComponent);
export default function App() {
  const [value, setValue] = useState<string>();
  const ref = useRef<() => void>();

  useEffect(() => {
    ref.current = () => { // fja sa state koja treba da se prenese
      console.log(value); // ovde hvata zadnji state
    };
  }, [value]); // ovde ima

  const onClick = useCallback(() => {
    ref.current?.();
  }, []); // ovde nema

  return (
    <div className="App">
      <HeavyComponentMemo onClick={onClick} title="Welcome closures" />
    </div>
  );
}
-----
// eto
closure zamrzava samo referencu na objekat, a obj.prop je i dalje mutable
------------
MeProvider sto sam pravio u nextjs-prisma-bilerplate je beskoristan jer useMe() vec kesira me usera
samo jedan context u drugi, react-query QueryClientProvider u moj context
------------
// react performance optimizations, chatGpt, intervjui
Server-Side Rendering (SSR) or Static Site Generation (SSG)
Code Splitting - loading only the code needed for the current view, and lazy-loading the rest when required
Tree Shaking - eliminate unused code
Minify and Uglify Code - reduce size
Optimize Images and Assets
Use PureComponent or React.memo - Avoid Unnecessary Render Cycles
Memoization - useMemo, useCallback
Virtualization - react-window to render only visible 
Performance Testing - lighthouse
Profiler - react profiler
CDN for static assets
Optimize Network Requests - graphql, pagination
Caching - client side
---------------
state ima jednu vrednost u toku jednog rendera, snapshot state
------------------
// ref.current se ne mece u useEffect
https://twitter.com/_georgemoller/status/1710281426468319602
https://epicreact.dev/why-you-shouldnt-put-refs-in-a-dependency-array/
---------------------------
// React useTransition: performance game changer or... // tezak i nejasan clanak
// https://www.developerway.com/posts/use-transition
state update jeste synchronous, blokira sledece events
primer - renderuje tab sa velikom listom, sledeci tab blokiran
--------
concurent react, non blocking, js je singlethreaded
state updates - default critical, moze mark as non critical
----
const [isPending, startTransition] = useTransition();
// u event handleru
startTransition(() => {
  setTab('projects'); // mark set state as non critical
});
sad vise ne blokira druge evente
loading state za computing heavy on client
-----
useTransition - 2 rerendera, jedan za isPending
ne moze sve da se umota u useTransition njen primer...?
------------
// useDeferredValue 
isto mark non-critical kao useTransition, samo kad nemas pristup setState
za props
const tabDeffered = useDeferredValue(tabProp);
----
// poenta, nije bas jasno
Mark updates as non-critical only if:
1. everything that is affected is memoized
2. or we are transitioning from "nothing" to "heavy", and never in the opposite direction
---------
startTransition ne moze umesto lodash.debounce(), previse brzo
----------
// njen zakljucak:
concurrent rendering hooks cause double re-renders. So, never use them for all state updates
requires a very deep understanding of the React lifecycle, re-renders, and memoization
inace kontra-produktivno, ona ih ne koristi, zanimljivo
------------------------
// Render props - Advanced React course, Episode 4 - Developer Way
// https://www.youtube.com/watch?v=pNaW0Md2o0g
render props je funkcija koja vraca element (jsx) koja je prosledjena kao prop // eto jednostavna definicija
render prop je isto kao komponenta samo nju react poziva, a ovo mi rucno pozivamo
render props je samo odlozena fja koja moze odlozeno da prosledi args - props, returning a function_, continuation // to
99% zamenjeno sa hooks
applies args - props at later point // cela poenta
korisno sad kad je state attached to DOM element, <div onScroll={() => {...}} />
lose za rerender i performance, mora React.memo, useMemo, useCallback
------------
