// Intro to SolidJS reactivity (in 5 minutes) - SolidJS
// https://www.youtube.com/watch?v=cELFZQAMdhQ
----
// effect - watch ...
// effect, stack
function createEffect(fn) { 
    const execute = () => { 
        observers.push(execute);

        try { 
            fn();
        } 
        finally { 
            observers.pop();
        }
    }
    execute();
}
----
// signal - notifies observers on update, reactivity, Rx.js
// signal
function createSignal(value) {
    const subscribers = new Set();

    // getter
    const getValue = () => { 
        const current = getCurrentObserver();

        if (current) subscribers.add(current);

        return value;
    }

    // setter
    const setValue = newValue => {
         value = newValue; 
         
         for (const subscriber of subscribers) {
            subscriber();
         }
    }

    return [getValue, setValue]; // state, setState, state je fja ovde
}
--------------------
// Solid in 100 Seconds - Fireship
// https://www.youtube.com/watch?v=hw3Bx5vxKl0

