https://www.reddit.com/r/reactjs/comments/1d12fqc/nextjs_weekly_51_nextjs_15_authjs_on_edge/?sort=new
https://nextjs.org/blog/next-15-rc
https://rc.nextjs.org/docs/app/building-your-application/upgrading/version-15
--------------
// typescript path aliases not working in docker
// https://stackoverflow.com/questions/76676456/next-js-v13-in-docker-does-not-respect-path-alias-but-works-locally/77621486#77621486
RUN yarn install --production=false --frozen-lockfile
poenta: must use devDependencies to include typescript to read tsconfig.json
---------------
// loading.tsx vs Suspense
// https://nextjs.org/docs/app/building-your-application/routing/loading-ui-and-streaming
loading.tsx fajl - page level, loading za celu susednu page.tsx stranicu
Suspense - component level, za server component
------
loading.tsx je fallback UI 
------
// https://nextjs.org/learn/dashboard-app/streaming
componenta umotana u Suspense ima async call 
<Suspense fallback={<LatestInvoicesSkeleton />}>
    <LatestInvoices />
</Suspense>
----------------
// usePathname	vs useSelectedLayoutSegment for active nav item
usePathname - /products/item/123	
useSelectedLayoutSegment - products or item
-----------------
// fonts in shadcn
// https://nextjs.org/docs/pages/building-your-application/optimizing/fonts
svi google fonts su preinstalirani u import * from 'next/font/google'
import {
    JetBrains_Mono as FontMono,
    IBM_Plex_Sans as FontSans,
} from 'next/font/google';

export const fontSans = FontSans({
    subsets: ['latin'],
    variable: '--font-sans',
});
----
// variable fonts google
variable - ne treba weight, or 400 default 
weight je default weight na <body /> or  <p />
// https://fonts.google.com/variablefonts#font-families
-----------------
// next standalone, copy public folder
// https://nextjs.org/docs/app/api-reference/next-config-js/output#automatically-copying-traced-files
cp -r public .next/standalone/ && cp -r .next/static .next/standalone/.next/
node .next/standalone/server.js
-----------------
// next.js error boundaries
// https://nextjs.org/docs/app/building-your-application/routing/error-handling
// https://nextjs.org/learn/dashboard-app/error-handling
error.tsx je error boundary, nije component, fallback UI // zapazi
unutra je client component uvek 
error.tsx - catch all
not-found.tsx - samo za 404 exception, podize se sa notFound()
--------------
// search and pagination with url query params for SSR
https://nextjs.org/learn/dashboard-app/adding-search-and-pagination
----------------
// build Next.js app in docker without database connection
// https://github.com/vercel/next.js/discussions/35534#discussioncomment-11385544
da nemas db, cache ili bilo koji new Nesto() poziv u global scope nego singleton factory function_ getDb()
-------------------
// important: build time env vars for Docker must pass in Dockerfile as build-arg, and github actions
env vars used in next.config.js are build time 


