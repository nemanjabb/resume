// react query essentials - tanstack - official tutorial
// all code here: 
// https://github.com/tannerlinsley/react-query-essentials
//
// 1 - 4
data, isError, isLoading
------
// 5 react query devtools
yarn paket i komponenta
jos stanja, retrying on error, polling, stale (resolved), itd
-------
// 6 refetchOnWindowFocus option
useQuery(queryKey, fetcherFunction, {options})
-------
// 7 refetching indicator
isFetching - network call se izvrsava, a ui se ne renderuje zbog cache
-------
// 8 stale time
fresh, fetching, stale, inactive - states u dev tools
staleTime: ms - koliko dugo data se smatra fresh, bez refetch
-------
// 9 cacheTime option
koliko dugo se dovucen data cuva u memoriji - cacheu
cacheTime: 0 - ms - nema kesa, sve se dovlaci
Infinity - ne brise se nista
--------
// 10 query key
key nije url, url je u fetcher fji, key je samo key za cache // zapazi
key - kao relative url, kljuc u hashtabeli gde je data vrednost
sve pod jednim key koristi isti cache objekat, i ima 1 network call
samo prvi key pravi network poziv, ostali koriste cache
---------
// 11 reuse query in custom hook
dedupe - de-duplikacija, koristi samo jednu instancu i eliminisi nepotrebne kopije
---
Data deduplication - in computing, data deduplication is a technique 
for eliminating duplicate copies of repeating data
---
u dev tools, zapazi broj - br instanci subskribovan na isti query, jer imaju isti key
----
usePokemon() hook oko useQuery
--------
// 12 parallel queries
razliciti keys fetchuju paralelno
--------
// 13 props and state in queries
text input za key
svaki uspesan fetch kreira cache
hendluje inicijalan undefined
trivijal
-------
// 14 disabling queries
enabled option, da ne vuce bezveze '' na api
--------
// 15 multipart query keys
key nije string nego array, da grupise slicne keys
---------
// 16 automatic retries
default 3 retry pokusaja za 404
retry: number - option, false ili 0 - ne pokusavaj ponovo posle prvi fail
exponential backof, sve sporije pokusava
retryDelay - ms ili funkciija za interval
dok pokusava je loading state, error tek posle zadnjeg
-----------
// 17 query cancelation - dobar, malo tezi, umesto debounce
otkazi requeste dok user kuca u polje, pre nego resolvuju na 404
----
import {CancelToken} from axios // slicno http client u C#
const source = CancelToken.source();
const promise = axios.get(url, {cancelToken: source.token}) // option u call
promise.cancel = () => { source.cancel('my message') } // nije Promise js klasa nego var sto je vratio axios
sve je to fetcher fja u react query, second arg
-----
isti primer sa fetch ide sa AbortController
-------------
// 18 dependent queries
ulancani pozivi, prvi api vraca result koji drugi salje kao param, npr id
----
uhvati sve postove usera sa emailom
1. get userByEmail(email).userId
2. get allPostsFromUser(userId)
----
2. poziv ima {enabled: userQuery.data?.userId} option // tako je ulancan da saceka prvi, ok
----
2. poziv polazi iz isIdle state, a ne isLoading
---------------
// 19 initialData za query, option, trivijal
nema veze sa dependent queries prethodnim klipom, jedan poziv samo
initialData option da prosledi ako ima da ne vuce network bezveze
bez loading state
-----------------
// 20 initialStale: boolean option, malo tezi
prvi mount da krene da refetch ako je initialData outdated, nece by default
initialData is not stale by default
initialStale: true - da krene da refetchuje posle mount
---
initialStale kontrolises da li ili ne da refetch initialData posle mount
-----------------
// 21 querying related lists and items
master detail pattern, posts, postById
nista specijalno, posts, setPosts state u parent
kesirano nema loading
isLoading, isFetching - indikator dok vuce u pozadini da update cache
--------------------
// 22 seeding initial query data from other queries
ako si ga vec dovuko iskoristi ga iz cache za initialData
initialData: () => queryCache.getQueryData(key).find(post => post.id === postId)
initialStale: true // da refetch
getQueryData(key) // pool
---------------------
// 23 seed future queries
setQueryData(key, data) // push
stale je true, pa refetchuje by default
u dev tools se vidi da je nagurao gomilu kljuceva u cache
----------------------
// 24 query sideefects - callbacks
koliko puta useQuery(posts) se izvrsi
const [count, increment] = useReducer(d => d + 1, 0) // reducerFn, initial state, ok
---
// onSucess sideefect - callback
useQuery(key, fetcherFunction, {onSucess: (data) => {...}})
onError: (error) => {...}
onSettled: (data, error) => {...} // success or error, ok
----
ako se usePosts() zove u 4 components ovi handleri se okinu 4x, ne 1 kao fetch
ako hoces samo 1 poziv zovi sideefect u fetcher fji // jer je deduped
-------------------------
// 25 scroll restoration
back dugme vraca scrollovanu stranicu gde je bila
radi automatski ako ima data u cache
--------------------------
// 26 query pooling and refetch intervals
primer vreme api, menja se
refetchInterval: 5000 //ms, option, radi samo kad je tab aktivan
refetchIntervalInBackground: true // i kad si na drugom tabu
----
use case: chat, status, kao socket
---------------------------
// 27 query invalidation basics
queryCache.invalidateQueries(key) // oznacava cache kao stale (invalid) i radi refetch
stale state je resolved
----------------------------
// 28 invalidating without refetching
queryCache.invalidateQueries(key, {refetchActive: false}) // option, samo mark cache stale bez refetch
ali sledeci focus ce refetch jer je stale
----------------------------
// 29 invalidating and refetching inactive queries
komponenta sakrivena na ekranu sa state && <Component />
queryCache.invalidateQueries(key, {refetchInactive: false}) // option, refetch i za sakrivene komponente
da su svezi kad ih prikazes ponovo
-----------------------------
// 30 invalidating multiple queries with similar key
ponovljena komponenta, izmeni key da ne dele isti query
[key, subKey] // complex key
invalidateQueries(key, subKey) // invalidira sve queryies jer imaju isti prefix key
ili subKey za samo 1 instancu - query
---
grupisanje queryja po key, subKey
-------------------------------
// 31 basic query prefetching
useReducer ima dodatnu reducer fju umesto setState za finije podesavanje promene
---
queryCache.prefetchQuery(key, fetcherFn, options) // isti args kao useQuery
napuni cache unapred na mount jer zna da ce mu trebati
i izbegne loading state na show koji zove useQuery
--------------------------------
// 32 hover based pre fetch queryies
zove prefetchQuery onMouseEnter
--------------------------------
// 33 prefetching and stale time
staleTime: Infinity // prefetch samo jednom, bice refetch on render za query opet
default non stop zove na hover
--------------------------------
// 34 forced prefteching
force: true // 4 arg options
glavni query ima staleTime, pa bypass za prefetch
---------------------------------
// 35 mutations overview
queries:
    read

mutations:
    create
    update
    delete
---
uvod, create sa axios.post()
mora focus out/in za refetch
------------------------------------
// 36 useMutation hook
const [createPost, createPostInfo] = useMutation((values) => axios.post('/api/posts', values))
createPost prakticno zove ovu anon fju sa axios
createPostInfo.isLoading, isError, isSuccess
ternari ? : ? : ? je if, else, else, else
---
da se pojavi novi item, refetch
useMutation(fn, {
    onSucess: () => {
        queryCache.invalidateQueries('posts') // invalidate cache, uradi refetch
    }
}
isto ko Apollo graphql query i mutacije
--------------------------------------
// 37 mutation sideeffects
axios vraca gresku u error.response
onError callback
---
createPostInfo.isError
error handling system je povezan sa ui elementima, alerti i toast
---
onSettled = onSucess or onError // refetch on both success or error
------------------------------------
// 38 updating query data with mutation responses - edit post
axios.patch('api/posts/id')
onSucess: (data, values) => {  // data response, values request
    queryCache.setQueryData(['posts', String(values.id)], data) // upise response u cache
    queryCache.invalidateQueries(['posts', String(values.id)]) // refetch
}
1 i '1', key mora po tipu da se poklapa za cache key
---
invalidate - mark cache stale, sto trigger refetch
----------------------------
// 39 optimistic updates for list-like queries - optimistic updates
// okida se pre network poziva
onMutate: (values) => {
    queryCache.setQueryData('posts',(oldPosts) => {
        return [...oldPosts, { ...values, id: Date.now() }] // refetch ce da ubaci pravi id kad stigne
    })
}
-----------------------------
// 40 rollback for list like queries - rollback on error
onMutate: (values) => {
 queryCache.cancelQueries('posts') // sprecava race sa drugim querijima koji su u toku
 const oldPost = queryCache.getQueryData('posts') // sacuvaj cache pre poziva
 ...
 return oldPosts; // ovo vraceno dostupno u onError kao rollbackValue
},
onError: (error, values, rollbackValue) {
    queryCache.setQueryData('posts', rollbackValue)
}
---------------------------
// 41 optimistic queries for single entity queries
sve lekcije su za 
1. liste 
2. single object
----
single object, a ne lista
iz prethodnog primera dodao onMutate optimistic update pre network call
-----------------------------
// 42 rollbacks for single entity queries - rollback optimistic update

const [createPost, createPostInfo] = useMutation(
    (values) => axios.post('/api/posts', values),
    {
      onMutate: (values) => {
        queryCache.cancelQueries('posts') // prevent race
        const oldPosts = queryCache.getQueryData('posts') // backup before network call
        queryCache.setQueryData('posts', (oldPosts) => { // optimistic update cache
          return [
            ...oldPosts,
            {
              ...values,
              id: Date.now(),
            },
          ]
        })
        return () => queryCache.setQueryData('posts', oldPosts) // passed rollback fn for error
      },
      onError: (error, values, rollback) => {
        if (rollback) {
          rollback() // called
        }
      },
      onSuccess: (data, values) => {
        // put response in cache
        queryCache.setQueryData('posts', String(values.id), data) 
      },
      // refetch on both error and success
      onSettled: (data, error, values) => queryCache.invalidateQueries('posts', String(values.id)),
    }
  )
----------------------------
// 43 paginated queries
// usePaginatedQuery je v2, useQuery sa keepPreviousData je v3
axios(url, {params: {pageSize: 10, pageOffset: 0}}) // axios params pravi string pageSize=10&pageOffset=0
---
useQuery skace jer je svaka stranica nezavisan cache key
// usePaginatedQuery drzi kesirane stranice za complex key pod jednim cache key
const postsQuery = usePaginatedQuery(['posts', { page }], () =>
axios
  .get('/api/posts', {
    params: {
      pageSize: 10,
      pageOffset: page,
    },
  })
  .then((res) => res.data)
)
---
umesto postsQuery.data
// resolvedData kesiran data, dok se ne dovuce novi
{postsQuery.resolvedData.items.map((post) => (
// next btn, latestData nije kesiran
disabled={!postsQuery.latestData?.nextPageOffset}
----
api vraca objekat {items: ..., nextPageOffset: 2}
------------------
// https://react-query.tanstack.com/guides/paginated-queries
// Next Page btn, isPreviousData - jos nije stigla nova stranica a kliknuto next btn, kao...
// disable the Next Page button until we know a next page is available
disabled={isPreviousData || !data?.hasMore}
---------------------------
// 44 prefetching paginated queries
prefetch next page, jer user klikce next dugme
ovo je problem koji sam resavao sa infinite scroll na Redux Ecommerce
---
usePaginatedQuery(['posts', { page }], fetchPosts)
// fetcherFn prima key ['posts', { page }] kao ...args
const fetchPosts = (_, { page }) => // ovde
  axios
    .get('/api/posts', {
      params: {
        pageSize: 10,
        pageOffset: page,
      },
    })
    .then((res) => res.data)
---
// evo ga prefetch next page, odlicno i kratko
// on mount inicijalno povuce 2 stranice, pogledaj u tools
React.useEffect(() => {
  queryCache.prefetchQuery(
    ['posts', { page: postsQuery.latestData?.nextPageOffset }],
    fetchPosts
  )
}, [postsQuery.latestData?.nextPageOffset])
-----------------------------
// 45 infinite queries - cemu sluzi infinite?
// slicno je imalo u Apollo za komplikovanu paginaciju, 2 varijante
----
// zadnji arg je return value od getFetchMore(), keys args + return value as last arg
const fetchPosts = (_, page = 0) =>
  axios
    .get('/api/posts', {
      params: {
        pageOffset: page,
        pageSize: 10,
      },
    })
    .then((res) => res.data)

// getFetchMore required option fn for useInfiniteQuery
const postsQuery = useInfiniteQuery('posts', fetchPosts, {
  getFetchMore: (lastPage) => lastPage.nextPageOffset,
})
------------
// vraca stranice, a ne iteme, 2 petlje
{postsQuery.data.map((page, index) => {
  return (
    <React.Fragment key={index} />
      {page.items.map((post) => (
-------
//
<button
  onClick={() => postsQuery.fetchMore()} // poziva getFetchMore
  disabled={!postsQuery.canFetchMore} // disable next button
>
  Fetch More
</button>
----
sve je single query, key u tools
-------------------------------
// 46 using react query with Next.js, part 1 - getServerSideProps pusta initialData
// za list
----
// next.js podsetnik sustina
getServerSideProps - request time
getStaticProps - build time
---
// inicijalno nema loading state jer je server vec dovuko posts
// i browser ih vuce iz json sinhrono
export const getServerSideProps = async () => {
  const posts = await fetchPosts()

  return {
    props: {
      posts,
    },
  }
}
export default function Posts({ posts }) {
  const postsQuery = useQuery('posts', fetchPosts, {
    initialData: posts, // ovde
    // ako je sa servera vec zastarelo, invalidate, refetch on mount, nema potrebe
    initialStale: true, 
  })
---------------------------------
// 47 using react query with Next.js, part 2
isto samo za item, a ne za list
react query ostaje na klijentu
---
postavi initial data na serveru, sustina
------------------------------------
------------------------------------
// React Query Tutorial for Beginners - Codevolution
// https://www.youtube.com/watch?v=VtWkSCZX0Ec&list=PLC3y8-rFHvwjTELCrPrcZlo6blLBUspd2
// https://github.com/gopinav/React-Query-Tutorials
// tanstack tutorial mnogo potpuniji i ozbiljniji od ovog
// 
// 1 introduction
pregled client state i server state i pregled kursa
--------------
// 2 project setup
json server, ruter i stranice
QueryClientProvider u root component
--------------
// 3 fetching data with useQuery
poredjenje axios i useEffect i useQuery
--------------
// 4 handling query error
axios slucaj - state za error
useQuery - error, isError i to je to
--------------
// 5 react query devtools
<ReactQueryDevtools /> na dno u provider
config props, initial open i bottom right
predstavlja devtools, lici na network tab
---------------
// 6 query cache
network call samo na prvi mount
posle cache
pa background refetch ako se promenilo na serveru
---
isFetching je indikator za background fetch
---
razlika isLoading i isFetching
on mount isti, true pa false
na 2. i dalje razliciti, isLoading false, isFetching true
nema potrebe user da gleda isLoading svaki put pa razdvojeni
---
cacheTime default 5 min, option u useQuery
koliko dugo da se cache drzi uopste, kad prodje izbrise ga, ne invalid
garbage collected zapravo
------------------
// 7 stale time
staleTime - koliko je cache valid, default 0
cim nije valid ide refetch, kad se vratis na tab
fresh -> stale state
-------------------
// 8 refetch - isto options
refetchOnMount default true
radi refetch kad se vratis na tab
---
refetchOnWindowFocus default true
kad odes na drugi app
----
sve ovo podesavanje koliko se cesto client syncuje sa serverom
------------------
// 9 pooling - refetch on interval
lajkovi na twitteru
refetchInterval default false, or ms // puzira kad tab nema focus
loops in fetching -> stale
---
refetchIntervalInBackground: true // pool i kad nema focus
----------------------
// 10 useQuery on click
enabled: false // disable fetch on mount
---
// refetch manually on click
const { refetch } = useQuery(...)
<button onClick={refetch} />
--------------------------
// 11 Success and Error Callbacks - sideeffects
onSuccess, onError options callbacks
pokusa 3x pre nego okine onError
---
data, error args
-----------------------------
// 12 Data Transformation
// option transformise response, project na primer, umesto u jsx data.map(...)
select: (data) => {

}
// sad ovaj data vec transformisan
const { data } = useQuery(...)
-------------------------------
// 13 Custom Query Hook
hooks - reuse same query in multiple components
in hook file:
query hook, fetcher function_
onSuccess, onError as hook args
--------------------------------
// 14 Query by Id
key je samo cache key, i nema veze sa url, url je u fetcher fji
---
fetcher fja prima keys as args.queryKey
-------------------------------
// 15 Parallel Queries - nista
// by default rade paralelno, istovremeno
const { data: superHeroes } = useQuery('super-heroes', fetchSuperHeroes)
const { data: friends } = useQuery('friends', fetchFriends)
------------------------------
// 16 Dynamic Parallel Queries - n queries
// komponenta ne zna unapred broj potrebnih queryja // to, to
// ili se menja od slucaja do slucaja
// useQueries je za dynamic
heroIds = [1, 3] // prop koji se menja
const queryResults = useQueries(
  heroIds.map(id => { // tu
    return {
      queryKey: ['super-hero', id],
      queryFn: () => fetchSuperHero(id)
    }
  })
)
---------------------------
// 17 Dependent Queries - enabled option
2. query koristi result 1. queryja kao arg
---
user?.data.id // mora ?. jer je async poziv, nije odmah dostupan, vazi za sve async values
---
// prvi query
const { data: user } = useQuery(['user', email], () =>
  fetchUserByEmail(email)
)
const channelId = user?.data?.channelId // eto ?.

// drugi query
useQuery(['courses', channelId], () => fetchCoursesByChannelId(channelId), {
  enabled: !!channelId // key i fetch fn arg // inace bi odmah radio sa undefined
})
-------------------------
// 18 Initial Query Data
reuse data (ili samo deo) ako vec imas u cache od prethodnog slicnog poziva
---
// njegov primer samo deo podmece, hero.name, a hero.alterEgo povlaci - components/RQSuperHero.page.js
// hooks/useSuperHeroData.js
import { useQuery, useQueryClient } from 'react-query'
const queryClient = useQueryClient()

return useQuery(['super-hero', heroId], fetchSuperHero, {
  initialData: () => { 
    // ovako vadi iz cache
    const hero = queryClient.getQueryData('super-heroes')
    ?.data?.find(hero => hero.id === parseInt(heroId)) 
  }
  if (hero) return { data: hero } else return undefined // vrati da umetne to sto je naso
}) // mora da vrati undefined da postavi query u hard loading state koji ne pokusava da ref data...
---
rezultat nema loading, samo bg refetch
------------------------------
// 19 Paginated Queries
backend api podrzava paginaciju - ?limit=3&_page=2
---
// state za currentPage
const [pageNumber, setPageNumber] = useState(1)
const { isLoading, isError, error, data, isFetching } = useQuery(
  ['colors', pageNumber], // pageNumber ulazi u cache key i fetch fn
  () => fetchColors(pageNumber),
  {
    // on iskoristio ovo umesto usePaginatedQuery (da ne skace prazna lista)
    keepPreviousData: true 
  }
}
// officijelni tutorijal mnogo potpuniji i ozbiljniji od ovoga
-----------------------------
//  20 Infinite Queries - infinite scroll
backend treba da vrati signal za kraj npr
---
useInfiniteQuery
getNextPageParam - option callback, vraca (racuna) hasNextPage boolean u useInfiniteQuery()
---
// evo ga
const { hasNextPage } = useInfiniteQuery(['colors'], fetchColors, {
---
// vraca jos i
const { data, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage } = useInfiniteQuery(['colors'], ...)
fetchNextPage - triger fja, load more
<button onClick={() => fetchNextPage()} disabled={!hasNextPage} />
isFetching - je kao i za useQuery
---
isFetchingNextPage // ne znam sta je ovaj??
// https://www.youtube.com/watch?v=s92apk05kT4&lc=UgxFhIzJVY5RquA9q7p4AaABAg.9XQpT0jh6Wg9XSu2VjG5oQ
isFetchingNextPage will be true when fetchNextPage called // to, fetching samo za next button
<div>{isFetching && !isFetchingNextPage ?  'Fetching...' : null}</div> // samo initial load, pages prefetched
means only show the Fetching... text when the first fetching was called
----
// data menja strukturu u odnosu na useQuery i usePaginatedQuery
// vraca ugnjezdeno polje pa mora 2 petlje [[...], [...], ...]
{data?.pages.map((group, i) => {
  return (
    <Fragment key={i} />
      {group.data.map(color => (
        <h2 key={color.id} />
--------------------------
// 21 Mutations
prvo obican React primer kontrolisana polja sa state
---
// dosta naivan i nepotpun primer
prosledio controlled fields u mutate fju
// mutate sakrio u useAddSuperHeroData
const { mutate: addHero } = useAddSuperHeroData()
const handleAddHeroClick = () => {
  const hero = { name, alterEgo }
  addHero(hero)
}
----------------------------
// 22 Query Invalidation
da se pojavi item koji je dodat mutacijom
---
// ref na cache
const queryClient = useQueryClient()
// u onSuccess, tj onSettled invalidate cache za refetch
onSettled: () => {
  queryClient.invalidateQueries('super-heroes') // to je dovoljno za refetch, da se pojavi item
}
-----------------------------
// 23 Handling Mutation Response
nema potrebe za refetch get, post od mutacije vec vratio novokreirani objekat
pa taj da se iskoristi
---
// vraca response od post kao arg
onSuccess: (data) => { ... }
---
// pise u cache
queryClient.setQueryData('super-heroes', (oldCacheData) => {
  // i sad immutable update cache
}) 
---
// sve
return useMutation(addSuperHero, { // addSuperHero je axios.post()
  onSuccess: newData => {
    queryClient.setQueryData('super-heroes', oldQueryData => {
      return {
        ...oldQueryData,
        data: [...oldQueryData.data, newData.data] // .data jer cache ima jos prop, ne samo data
      }
    })
  },
-------------------------
// 24 Optimistic Updates
optimistic update = implement onMutate, onError i onSettled callbacks, samo to
---
// okida se pre nego network call, prima iste args kao fetcher fja (axios.post())
onMutate: () => {
// cancel all queries da ne override cache od optimistic updatea
await queryClient.cancelQueries('super-heroes') // async
// backup cache before mutation
const previousHeroData = queryClient.getQueryData('super-heroes')
// edit cache, ista logika kao u  23 Handling Mutation Response
queryClient.setQueryData('super-heroes', oldQueryData => {
  return {
    ...oldQueryData,
    data: [
      ...oldQueryData.data,
      // id je temp placeholder
      { id: oldQueryData?.data?.length + 1, ...newHero }
    ]
  }
})
// rollback on fail, pass it to onError()
return { previousHeroData }
}
---
// apply rollback value from onMutate()
onError: (_err, _newTodo, context) => {
  queryClient.setQueryData('super-heroes', context.previousHeroData)
},
---
// onError or onSucces, samo refetch ovde
onSettled: () => {
  queryClient.invalidateQueries('super-heroes')
}
------------------------------
// 25 Axios Interceptor - middleware
nema veze sa react query
interceptor za: base url, error handling, add barer token in header
---
interceptor je middleware, apply na sve requeste
---
const client = axios.create({ baseURL: 'http://localhost:4000' })
// ovo exportuje
export const request = ({ ...options }) => {
  client.defaults.headers.common.Authorization = `Bearer token`

  const onSuccess = response => response; // nista
  const onError = error => {
    // optionaly catch errors and add additional logging here
    return error
  }
  return client(options).then(onSuccess).catch(onError)
}
---
// pa onda koristi request() umesto axios.get(), bezveze skroz, ne valja
import { request } from '../utils/axios-utils'
const fetchSuperHeroes = () => {
  // return axios.get('http://localhost:4000/superheroes')
  return request({ url: '/superheroes' }) // necitljivo
}
--------------------------
--------------------------
// docs ssr
// https://react-query.tanstack.com/guides/ssr
// sve ovo je samo za prefetching na serveru
// Using Hydration metod najbolji
---
// komentar koristan
function Posts() {
  // This useQuery could just as well happen in some deeper child to
  // the "Posts"-page, data will be available immediately either way
  const { data } = useQuery('posts', getPosts)
  // This query was not prefetched on the server and will not start
  // fetching until on the client, both patterns are fine to mix
  const { data: otherData } = useQuery('posts-2', getPosts)
  // ...
}
---
// hydration dehydration?
in react-query terms, we take the QueryClient and convert it to json 
when dehydrating on the server, and on the client we hydrate it to a full QueryClient again.
bukvalno - serializing/deserializing objects to json/xml be transported through network
---
// https://en.wikipedia.org/wiki/Hydration_(web_development)
ssr server salje go html, ti eventi ne postoje na serveru // ali dovoljno za first contentful paint
hidratacija je attaching event handlers i izvrsavanje onLoad javascripta // zahteva neko vreme
da stranica postane interaktivna
---------------------
// paginacija vektor
1,2,3...7,8,9...15,16,17
vazno je identifikovati stateove i uslove koji ih trigeruju // to to, univerzalno za algoritme
ako ponasanje zavisi samo od ulaza ne treba state, samo funkcija koja transformise ulaz na izlaz
-------------------
prouci react suspense
--------------------
// avatar i header na user settings form
form state and React Query state should be separated
-----------
// fora
koristi enabled prop da sacekas dok async argumenti nisu dostupni
i nema potrebe da prenosis enabled, izracunas ga od argumenata
-----------------
isLoading - prvi fetch dok je cache prazan
isFetching - bilo koji fetch
--------
render je ciklus, sto je an ekranu je jedan poziv fje - komponente
ne postoji trajna memorija koja prikazuje sadrzaj
promena useState je tranzijentna - 1 render, fetch call su duzi - nekoliko rendera
detektuj on/off ivice (prelaze) i onda setuj novi state na true/false
----------------------
// react query error handling
// https://tkdodo.eu/blog/react-query-error-handling

1. query level
// sklanja (unmounts) ceo UI za background refetch errors // to to
const todos = useQuery(['todos'], fetchTodos)

if (todos.isError) {
  return 'An error occurred'
}
----
2. ErrorBoundary
 // will propagate all fetching errors to the nearest Error Boundary
 const todos = useQuery(['todos'], fetchTodos, { useErrorBoundary: true }) 
 // (or provide it via a default config)
 ---
 // ili fja za filter samo neke da hvata EB
 useErrorBoundary: (error) => error.response?.status >= 500,
 ---
 // Error Boundaries cannot catch asynchronous errors
 // zato ovaj RQ flag re-throws it in the next render cycle
----
 3. onError callback
 // will fire toast multiple times, wherever hook is used
 useQuery(['todos'], fetchTodos, {
  onError: (error) =>
    toast.error(`Something went wrong: ${error.message}`),
})
----
4. global callback
const queryClient = new QueryClient({
  queryCache: new QueryCache({
    onError: (error) =>
      toast.error(`Something went wrong: ${error.message}`),
  }),
})
// isto kao axios interceptor - globalno
---
// 4. only for background refetch
// which indicates a failed background update
onError: (error, query) => {
  if (query.state.data !== undefined) {
    toast.error(`Something went wrong: ${error.message}`)
  }
}
----
// on koristi 4 za background refetch, a ErrorBoundary za sve ostalo
--------------------
// v4 disabled queries
// https://react-query-beta.tanstack.com/guides/disabling-queries
const query = useQuery<PostWithAuthor, AxiosError>(
  [QueryKeys.POST, id],
  () => getPost(id),
  {
    enabled: !isNaN(id),
  }
)
// za enabled: false, tj kad je disabliran
isLoading === true // stuck u loading state
fetchStatus === 'idle' // dodatni flag, da je disabliran query
// pa za loading treba ovo // ovo radi, samo na prazan cache
if (isLoading && fetchStatus !== 'idle') return <Loading />;
// ovo iz docs ne radi, probao
(isLoading && !isFetching) return <Loading />;
---
// zapravo ovo najbolje, jer ti taj flag vec imas, samo ponovi iz useQuery.enabled
const isEnabled = !isNaN(id); // !!id
if (isLoading && isEnabled) return <Loading />;
------------
// ako us neki useQuery imas props post iz parent moras i njega da proveris
// pre bilo kog post.nesto pristupa, ako parent prerenderuje // ne verujem da je ovo tacno
if (isLoadingMe || !post) return <Loading isItem />;
// inace ova greska
Warning: Cant perform a React state update on an unmounted component. 
This is a no-op, but it indicates a memory leak in your application. 
To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function_.
---
// uvek moras da imas sve podatke (if loading return) pre pristupanja njima ili rendera
// inace na delete post puca i baca ovu gresku
---------------------
// Status Checks in React Query, pros
// https://tkdodo.eu/blog/status-checks-in-react-query
poenta: RQ refetchuje sam u pozadini bez da user zahteva
moguce da ima data i error podatke u isto vreme u cache // to poenta
zato vazno da prvi if bude id (data) return data.map(...), a if(error) ispod
da bi prikazivao podatke uvek kad ih ima // ok, pros
----
// konvencionalno
if (todos.isLoading) {
  return 'Loading...' // loading, najvisi prioritet
}
if (todos.error) {
  return 'An error has occurred: ' + todos.error.message // error
}
return <div>{todos.data.map(renderTodo)}</div> // data
----
// njegovo
if (todos.data) {
  return <div>{todos.data.map(renderTodo)}</div> // data najvisi prioritet, prikazi data kad god ima
}
if (todos.error) {
  return 'An error has occurred: ' + todos.error.message // pa error
}
return 'Loading...' // loading najnizi
--------------------
// ako vratis promise iz onSuccess to ce biti sacekano
// return Promise from invalidation so that it will be awaited
onSuccess: () => { return queryClient.invalidateQueries(['person', id]); }
-------------------
QueryKeys u React Query je kao dependency array u useEffect
------------------
// reset ErrorBoundary React Query 
// https://react-query-beta.tanstack.com/guides/suspense
// QueryErrorResetBoundary je vezan za ErrorBoundary, sluzi da mu resetuje state, retry query
const { reset } = useQueryErrorResetBoundary(); // mesto render props

return (
<QueryErrorResetBoundary>
  <ErrorBoundary FallbackComponent={ErrorFallback} onReset={reset}>
    ...
  </ErrorBoundary>
</QueryErrorResetBoundary>
---
// ErrorBoundary i mutacije
// default error undefined, hvata ErrorBoundary
// useErrorBoundary: false - da disableujes za tu mutaciju za obican Alert
// throwOnError: false - da ignorises greske potpuno
By default, instead of supplying the error variable when a mutation fails, 
it will be thrown during the next render of the component its used in and propagate 
to the nearest error boundary, similar to query errors. If you wish to disable this, 
you can set the useErrorBoundary option to false. If you wish that errors are not thrown at all, 
you can set the throwOnError option to false as well
------------------
// React Query Typescript
// https://tkdodo.eu/blog/react-query-and-type-script
export function useQuery<
  TQueryFnData = unknown, // return value of fetcher fn, + data
  TError = unknown,
  TData = TQueryFnData, // data, only if it has select transform
  TQueryKey extends QueryKey = QueryKey
>
--------------------------------
// SettingsForm dependant queries, async load images in form state with useQuery
// important: whenever query is not refetching **key didnt change**, is not unique
// this is optimistic updates after mutation, with objects merging story (swr and apollo) 
// instead of refetch
------------------------
isLoading - on mount
isFetching - on invalidateQuery
isRefetching - on refetchQuery
-------------------------------
// useQueries example - dynamic number of queries, broj dolazi sa servera, ne moze hardcoded
const getAsset = async (entryId: string) => {
  const { data } = await axios.get<SectionAsset>(
    `/api/contentful/section-assets/${entryId}`
  )
  return data
}

// ovako tip
const getQueryOptions = (
  entryId: string
): UseQueryOptions<SectionAsset, Error, SectionAsset, QueryKey> => ({
  queryKey: ['section-assets', entryId],
  queryFn: () => getAsset(entryId),
  // pass falsy to disable query
  enabled: !!entryId,
  // todo: move this to global react-query error handler
  onError: (error) => {
    console.error('Error fetching Contentful asset: ', error)
  },
})

/** single query */
export const useAsset = (entryId: string) => useQuery(getQueryOptions(entryId))

/** dynamic number of queries */
export const useAssets = (entryIds: string[]) =>
  useQueries(entryIds.map((entryId) => getQueryOptions(entryId)))
---------
posle samo:
const queries = useAssets(entryIds)
queries[0].isLoading, queries.data ...
---------------------------------
// handle types with enabled: !!id in query
// https://tkdodo.eu/blog/react-query-and-type-script
function fetchGroup(id: number | undefined): Promise<Group> {
  // ✅ check id at runtime because it can be `undefined`
  return typeof id === 'undefined'
    ? Promise.reject(new Error('Invalid id')) // kao throw, ali je promise
    : axios.get(`group/${id}`).then((response) => response.data)
}
// query
function useGroup(id: number | undefined) {
  return useQuery({
    queryKey: ['group', id],
    queryFn: () => fetchGroup(id),
    enabled: Boolean(id), // ovde
  })
}
-------------------------
// react-query key u array moze i seriazable objects, 
// https://tanstack.com/query/v4/docs/react/guides/query-keys
['users', { params }]
----------------------------
// ovako ide refetch u react-query
const {
  data,
  isLoading,
  refetch, // eto, fja
  isRefetching,
} = useQuery(id);
-----
// disable refetch on windows focus in useQuery()
useQuery({refetchOnWindowFocus: false, ... })
------------
// fetch fn u 1 liniji
const getUserWithAccounts = async (id?: string) => {
  if (!id) Promise.reject(new Error(`Invalid user id: ${id}`));

  return axiosInstance
    .get<UserWithAccounts>(`${Routes.API.USERS}${id}/accounts`)
    .then((response) => response.data);
};
------------------------------------
// AxiosError
ovaj data je ApiError tipa, klasa koju sam definisao na serveru sa next-connect i bacam je iz service i controllera
restUpdateUser.error.response?.data as ApiError
-----------
// da li je axios error
import { AxiosError, isAxiosError } from 'axios';
isAxiosError(restUpdateUser.error)
// ovako se kastuje response.data na moju server gresku
// to je tip greski koje bacas samo iz tog contorllera i servicea 99%
// u query i mutacijama postavis
AxiosError<ApiError>
--------------------------
// ovde u react query mutation bi trebao tip
// jesam ovo propustio, sto je tkdodo rekao, default AxiosError<unknown>
const mutation = useMutation<PublicUser, AxiosError<ApiEror/>, UserUpdateMutationData, unknown>({})
-----
// eto T je tip za data
export interface AxiosResponse<T = any, D = any> {
  data: T;
  status: number;
  statusText: string;
  headers: RawAxiosResponseHeaders | AxiosResponseHeaders;
  config: InternalAxiosRequestConfig<D>;
  request?: any;
}
-------
// runtime type check
isApiError() check treba da je zapravo u axiosInstance helper fja // to
sa instanceof, in, hasOwnProperty ili Zod
------------------------
// debounce react-query, samo zakasnis ulazne argumente, i to je to
// https://dev.to/arnonate/debouncing-react-query-with-hooks-2ek6
const debouncedSearchQuery = useDebounce(searchQuery, 600);
const { status, data, error, isFetching } = useReactQuery(
  debouncedSearchQuery,
  page
);
--------------------
restQuery.refetch() refetchuje iperativno za bilo koju vrednost, bypassuje enabled, i fetchuje sa undefined i puca
nego ide ovo await queryClient.invalidateQueries()
ovo je prakticno sema za go poziv sa query hook, samo imas loading i successs i error state pride, ne kesiras nista
// moras arg u state da stavis
setParsedEmail(parsedEmail);
await queryClient.invalidateQueries([QueryKeys.USER, 'email', parsedEmail]);
----------------------
// react-query cacheTime vs staleTime
https://stackoverflow.com/questions/72828361/what-are-staletime-and-cachetime-in-react-query
staleTime - disable refetching for some time, default 0
So if you set staleTime: 120000, in your example, you are guaranteed to not get another 
network request for two minutes after the first successful one
-----
cacheTime - garbage collection
// disable caching
https://github.com/TanStack/query/issues/99
cacheTime: 0 // eto
--------------------
// moze websockets sa react query
https://tkdodo.eu/blog/using-web-sockets-with-react-query