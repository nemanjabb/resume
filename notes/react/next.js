//next js tutorial

code splitting - each page only loads whats necessary for that page
prefetches - the code for the linked page in the background
---
CSS Modules are useful for component-level styles. 
But if you want to load some CSS to be loaded by every page
---
To use CSS Modules, import a CSS file named *.module.css from any component.
To use global CSS, import a CSS file in pages/_app.js.
---
prerendered - html + minimal js
spa - cela app samo js
---
Static Generation is the pre-rendering method that generates the HTML at build time. The pre-rendered HTML is then reused on each request.
Server-side Rendering is the pre-rendering method that generates the HTML on each request.

In development mode (when you run npm run dev or yarn dev), every page is pre-rendered on each request — even for pages that use Static Generation.
We recommend using Static Generation (with and without data) whenever possible because your page can be built once and served by CDN
On the other hand, Static Generation is not a good idea if you cannot pre-render a page ahead of a users request.
---
Static Generation with and without Data
Static Generation with data (build time)
for some pages, you might not be able to render the HTML without first fetching some external data
Essentially, getStaticProps allows you to tell Next.js: "Hey, this page has some data dependencies — so when you pre-render this page at build time, make sure to resolve them first!"
export async function getStaticProps() {
  // Get external data from the file system, API, DB, etc.
  const data = ...

  // The value of the `props` key will be
  //  passed to the `Home` component
  return {
    props: ...
  }
}
getStaticProps dovlaci podatke u build time za static generation i exposuje ih kao props, vraca props
u dev mode izvrsava se za svaki request, u prod u build time
getStaticProps runs only on the server-side.
getStaticProps can only be exported from a page
---
If you need to fetch data at request time instead of at build time, you can try Server-side Rendering:
To use Server-side Rendering, you need to export getServerSideProps instead of getStaticProps from your page
export async function getServerSideProps(context) {
  return {
    props: {
      // props for your component
    }
  }
}
Client-side Rendering - user dashboard pages - seo nebitan, klijent dovlaci podatke
---
// rezime
static generation with data - at build time, nema request podataka - getStaticProps()
server side rendering - server dovlaci podatke na svaki request, podaci o requestu u context arg - getServerSideProps(context)
client side rendering - client dovlaci podatke na svaki request
-----------
dynamic routes
page path depends on external data
Pages that begin with [ and end with ] are dynamic pages in Next.js.
export async function getStaticPaths() {
  // Return a list of possible value for id
}
export async function getStaticProps({ params }) {
  // Fetch necessary data for the blog post using params.id
}
---
i ssg i ssr su pre-rendering, samo jedan u build time drugi na svaki request
<Link href="/posts/[id]" as={/posts/${id}} />
In development (npm run dev or yarn dev), getStaticPaths runs on every request.
In production, getStaticPaths runs at build time.
---
If fallback is false, then any paths not returned by getStaticPaths will result in a 404 page.
---
Catch-all Routes
pages/posts/[...id].js matches /posts/a, but also /posts/a/b, /posts/a/b/c and so on.
getStaticPaths
return [
  {
    params: {
      // Statically Generates /posts/a/b/c
      id: ['a', 'b', 'c']
    }
  }
  //...
]
---
If you want to access the Next.js router, you can do so by importing the useRouter hook
---
To create a custom 404 page, create pages/404.js.
---------
API Routes
pages/api/hello.js
export default (req, res) => {
  res.status(200).json({ text: 'Hello' })
}
http://localhost:3000/api/hello
---
Preview Mode
Youd want Next.js to bypass Static Generation only for this specific case.
---
API Routes can be dynamic
---------
deployment
next build builds the production application in the .next folder.
---------
TypeScript
-----------------
-----------------
//docs
// getInitialProps
getInitialProps enables server-side rendering in a page 
getInitialProps can NOT be used in children components, only in the default export of every page
getInitialProps will disable Automatic Static Optimization.
If you're using Next.js 9.3 or newer, we recommend that you use getStaticProps or getServerSideProps instead of getInitialProps.
getStaticProps (Static Generation)
getServerSideProps (Server-side Rendering)
---
// arg
getInitialProps receives a single argument called context
{pathname, query, asPath, req, res, err}
-----------
//Custom App
//./pages/_app.js
front controller prakticno
You can override it and control the page initialization. 
Which allows you to do amazing things like:
Persisting layout between page changes
Keeping state when navigating pages
Custom error handling using componentDidCatch
Inject additional data into pages
Add global CSS

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}
export default MyApp
//props
Component prop is the active page
pageProps is an object with the initial props that were preloaded for your page by one of our data fetching methods, otherwise it's an empty object
---------
//Custom Document
//./pages/_document.js
master page prakticno
A custom Document is commonly used to augment your applications <html> and <body> tags.
postavio custom SEO head
import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }
  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
export default MyDocument
---
dodatni arg u context
renderPage: Function - a callback that runs the actual React rendering logic

----------------------
// swr
// https://github.com/vercel/swr
mutate upisuje u lokalni cache direktno
import useSWR, { mutate } from "swr";
mutate("user", data.user);
---
destrukturirani mutate je isti samo je boundovan vec za key arg
const { data, mutate } = useSWR('/api/user', fetcher)
mutate({ ...data, name: newName })
---
//trigger
You can broadcast a revalidation message to all SWR data inside any component by calling trigger(key).
import useSWR, { trigger } from "swr";
trigger(`${SERVER_BASE_URL}/articles/${pid}`); //triggeruje update cachea za ovaj kljuc
------------------
//kriterijum za static generation/server-side rendering
You can use Static Generation for many types of pages, including:
Marketing pages
Blog posts
E-commerce product listings
Help and documentation
You should ask yourself: "Can I pre-render this page ahead of a user's request?" If the answer is yes, then you should choose Static Generation.
---
getStaticPaths() da pokrijes url-ove za koje radis static generation, return { paths, fallback: false }
u kombinaciji sa getStaticProps({ params }), params od rutera
---
uvek static generation, sem kad mora ssr ili client
To make a page use Static Generation, either:
1. export the page component, 
2. or export getStaticProps 
3. (and getStaticPaths if necessary)
---
getServerSideProps je server side rendering, on each request, redje
--------------------
--------------------
// Codevolution, Next.js Tutorial for Beginners
// https://www.youtube.com/playlist?list=PLC3y8-rFHvwgC9mj0qv972IO5DmD-H0ZH

// 1 - Introduction
1. file based routing
2. pre-rendering (server side rendering)
3. api routes
4. css modules
5. authentication
6. dev and prod build system
---------
// 2 - Hello World
nista, node, vs code i hello world
---------
// 3 - Project Structure
package.json scripts - dev, build, start (for backend server), lint
next.config.js - react strict mode za warnings, legacy features i lifecycle methods
.eslintrs - eslint config, extends
.next folder - dist
styles
public - public assets, slike itd
pages folder - sav routing je u pages folderu, zapazi
---
_app.js layout
---
execution flow
yarn dev ili npm run dev -> _app.js (entry point, main()) -> index.js (Home component)
---------
// 4 - Routing Section Intro
nista, nabrojao
---------
// 5 - Routing with Pages
trivijal
index.js -> localhost:3000/ // export default component from file
about.js -> localhost:3000/about
----------
// 6 - Nested Routes
blog/first.js ->  localhost:3000/blog/first
blog/index.js ->  localhost:3000/blog
----------
// 7 - Dynamic Routes
[productId].js -> localhost:3000/product/1
const router = useRouter();
router.query.productId
---
fajl ima prioritet spram [id] ako postoji fajl
-----------
// 8 - Nested Dynamic Routes
[productId]/index.js -> localhost:3000/product/1 // [folder] zapazi
[productId]/review/[reviewId].js -> localhost:3000/product/1/review/1 //ok
const router = useRouter();
const { productId, reviewId } = router.query; // oba vadi
-----------
// 9 - Catch All Routes
docs/[...params].js -> localhost:3000/docs/a/b/c - sve ugnjezdene da hvata
const router = useRouter();
const { params } = router.query; // undefined pa array
localhost:3000/docs/feature1/concept1/example1 -> [feature1, concept1, example1] // tacno to
---
docs/[[...params]].js // optional catch all, ovaj fajl umesto 404 page, ...?
isto kao single [] samo za [[]] na /docs se prikazuje [[...params]].js
-------------
// 10 - Link Component Navigation

// client side navigation - u okviru nextjs aplikacije - Link component
// za external linkove <a href=...> npr google.com
// ako koristis <a/> za app nav refresh stranu i gubi state

<Link href="/blog">
	<a>Blog</a>
</Link>

---
<Link href="/blog" replace /> // ne gura url u history stack, back btn vodi 2 unazad
--------------
// 11 - Navigating Programmatically

trivijal
const router = useRouter();
router.push('/products');
router.replace(...) // ista prica
----------------
// 12 - Custom 404 Page
trivijal skroz
pages/404.js -> localhost:3000/bzvz123
----------------
// 13 - Routing Summary
nista, rezime
-----------------
// 14 - Pre-rendering and Data Fetching Section Intro

prerendering = server side rendering

1. static generation
	- without data
	- with data
	- incremental static generation
	- dynamic parameters when fetching data
2. server side rendering
	- data fetching
3. client side data fetching
------------------
// 15 - Pre-rendering

nista...
pre-render = html stize sa servera
hydration = kad js kreira html u create react app

1. brze, manje posla za browser
2. seo

-------------------
// 16 - Static Generation

static generation = html page (with content) generated at build time
prefered kad god moguce, cached cdn
blogs, ecommerce, docs

prod - 1 render na build
dev - render na svaki request

without data
with data - api ili db
--------------------
// 17 - Static Generation with getStaticProps

generate html after fetching data

// component
function UserList ({users}) {
	
	
}

export default UserList;

getSTATICProps jer STATIC generation

// mora da vrati object
// prosledjuje taj object u props komponente u tom fajlu, kao redux
export async function getStaticProps() {
	const data = await fetch(...);
	console.log(data); // loguje u vs code consolu, ne u browser
	// na serveru radi jbg
	
	return {
		props: {
			users: data,
		}
	};
}

eg blog posts, ne zavisi od requesta, uvek isto

-----------------
// 18 - Pages vs Components

getStaticProps definisana samo u pages folderu

components nemaju (ne trebaju) pristup spec fjama getStaticProps..., routing..., samo pages
pages imaju rutu

novi folder components u root
razlika izmedju Pages i Components, i to je sve u ovom videu
-------------------
// 19 - More on getStaticProps

1. getStaticProps() radi na serveru i nikad u browseru // normalno
nema ni definiciju na klijentu

2. moze da cita fajl system ili cita bazu
da koristi tajne api tokene, to nece stici na klijent

3. samo u page, a ne u obicnoj komponenti
4. mora da vrati objekat sa props key, return { props:... }
5. runs at build time // to
dev - every request
---------------------
// 20 - Inspecting Static Generation Builds

// prod
yarn build

analizira console log stats size za global js i po stranici

svaka ruta kako generisana
prazan krug - static, ne cita external data // static html, a ne static generation
pun krug - static generation - SSG, cita data, html + json
... server i ISR generation
----
analizira fajlove u .next folderu
// server folder
users page je users.html + users.json
return iz getStaticProps je zavrsio u json fajlu

// static folder
static/pages - stranice, ostalo framework
ovo se salje klijentu
-----------------------
// 21 - Running Static Generation Builds
sve sa prod build na svaku promenu
yarn start
desni klik na refresh chrome - empty cache i hard reload
---
skida samo html i js za 1 stranicu

ali ako na home ima link ka users skida i users.json i users.js chunk

getStaticProps se zove zamo ako odes direktno na /users, koristi se users.html
ako sa home ides linkom na users koristi se users.json i js clientside
------------------------
// 22 - SSG with Dynamic Parameters
master detail pattern, details za list item

getStaticProps je server side rendering sto bi zvao api u useEffect da je client side
---
// hvatanje parametra iz rute u getStaticProps
export async function getStaticProps(context) {
conts {params} = context;
params.postId
----
<Link passHref /> // kad <a> nije child
-------------------------
// 23 - SSG with getStaticPaths

/pages/posts/[postId].js

// prerender za dynamic params
export async function getStaticPaths() {
  return {
    paths: [
      {params: {postId: '1'}}, 
      {params: {postId: '2'}}, 
      {params: {postId: '3'}}, 
    ]
  }
}
// zajedno sa getStaticProps
// moguce vrednosti rute za koje treba da se renderuje na serveru...?
--------------------------
// 24 - Inspecting getStaticPaths Builds

staticki se generisu stranice za sve id-eve koje getStaticPaths moze da vrati
linkovane stranice se prefetchuju...

json je prefetchovan za sve linkove na stranici
----------------------
// 25 - Fetching Paths for getStaticPaths

ajax i u getStaticPaths i map za {params: {postId: '1'}}, 

----------------------
// 26 - getStaticPaths fallback false

fallback false params koji nisu pokriveni 404

------------------------
// 27 - getStaticPaths fallback true

router.isFallback je true za nepostojece rute

nextjs generise staticku stranicu u request time i ima loading, ne baca exception kao za fallback: false

posle koristi stranice koje je vec generisao

build time i request time sustina
sve ovo isprobava u prod build
json se skida sa servera u request time na client, zato ima loading state

kad?
fallback true se koristi za veliki br 1000 stranica, samo deo u build, a ostalo na request
------------------------
// 28 - getStaticPaths fallback blocking

sto kao true samo nista mesto loading, ui je blokiran, drzi ga server
za seo da se ne indeksira loading page

-------------------------
// 29 - Incremental Static Regeneration

static generation
1. buld time za vel br strana
2. stale data

stale data objasnjenje, cim je stranica jednom generisana build time ili request time ona je stale u json

revalidate arg
--------------------------
// 30 - Inspecting ISR Builds
// veliko ime za 1 arg
revalidate value je br sekundi posle koliko regenerate page
ko je od prslog req proslo 10 sec onda regenerate, inace cached

getStaticProps() {
	return {
		props:...
		revalidate: 10, //sec

regenerac se desava ali nova stranica se salje tek na sledeci req, zasto?
treba 2 req
--------------------------
// 31 - Server-side Rendering

static gener problems: // eto samo ta 2
1. ne moze fetch data at request time
2. nema pristup to reqest data, npr loged in user jer je compile time

server side rendering - page rendered at request time, and not at build time
----------------------------
// 32 - SSR with getServerSideProps

getSTATICprops - static generation - build time, 1
getSERVERSIDEprops - ssr - request time, n // imena fja, lako u kodu sta je sta

news primer za ssr, stalno se menja

// isto sve, vraca props key i salje u komponentu u istom fajlu
export async function getServerSideProps() {
	const data = await fetch(...);
	
	return {
		props: {
			articles: data,
		}
	};
}

ssr sporije od static gen
useEffect na serveru i to je to, u req time

1. radi samo na serveru, ne salje se u browser
2. fajlovi i db sa servera moze u getServerSideProps, kao u node
3. samo page, a ne i regular component
4. { props:...
----------------------------
// 33 - SSR with Dynamic Parameters

promenljiv deo rute - promenljiva
getServerSideProps(context.params.category) // access ruta query params
-----------------------------
// 34 - getServerSideProps context
context.req, res objects dostupni ovde, kao express
req.headers.cookie
res.setHeader('Set-Cookie', ['name=Vishwas'])
query // sve ovo logovano u vs konzolu, ne u browser

localhost:3000/news/sports?subcategory=football
localhost:3000/folder/dynamic-param?query-param=val
----------------------------
// 35 - Inspecting SSR Builds

inspect prod: brises .next folder i yarn build

nista se ne kesira u pages folder
svaka promena u data reflektovana na svaki request
-----------------------------
// 36 - Client-side Data Fetching

user dashboard - private, iza login, ne treba seo

nista isto ko u react, loading, data states i useEffect
------------------------------
// 37 - SWR for Client-side Data Fetching

client samo za client side fetching

ssr je kao blade, jsf, handlebars na serveru samo plus react

const { data, error } = useSWR('ruta', fetcher)
!data je loading
state je u useSWR data vec

refetch na promenu taba...
-----------------------------
// 38 - Pre rendering + Client side Data Fetching

kombinuje ssr i client (fetch)

dovuce listu sa getServerSideProps, pa item dovlaci na click sa obican fetch
shallow routing

budzi link na click
------------------------------
// 39 - Pre-rendering & Data Fetching Summary

rezime, dobar
------------------------------
// 40 - API Routes Section Intro
nabrajanje
------------------------------
// 41 - API Routes
pages/api/index.js
export default function handler(req, res) {
	res.status(200).json({name: 'home api route'})
}
localhost:3000/api
---
pages/api/dashboard.js ili pages/api/dashboard/index.js
localhost:3000/api/dashboard
---
api folder se ne salje u browser
---------------------------------
// 42 - API GET Request
trivijal, ucitao polje u pages component
---------------------------------
// 43 - API POST Request
// https://github.com/gopinav/Next-JS-Tutorials/blob/master/next-api/pages/comments/index.js
const response = await fetch('/api/comments', {
  method: 'POST',
  body: JSON.stringify({ comment }),
  headers: {
	'Content-Type': 'application/json'
  }
})

// api, isti handler za get i post, samo if
export default function handler(req, res) {
  if (req.method === 'GET') {
    res.status(200).json(comments)
  } else if (req.method === 'POST') {
    const comment = req.body.comment
    const newComment = {
      id: Date.now(),
      text: comment
    }
    comments.push(newComment)
    res.status(201).json(newComment)
  }
}
-----------------------------
// 44 - Dynamic API Routes
delete req
pages/api/comments/[commentId].js
req.query.commentId
-----------------------------
// 45 - API DELETE Request
page
http delete commentId
api filter comment, trivijal ceo video
-----------------------------
// 46 - Catch All API Routes
cim je [var] gadja promenlj iz rute

// catch all
pages/api/[...params].js // gadja opseg vars iz rute
const x = req.query.params; // match filename
localhost:3000/api/one/two/three
[one, two, three] // / je spec znak, nov el polja

// optional catch all [[]]
pages/api/[[...params]].js // ne vraca 404 za localhost:3000/api
------------------------------
// 47 - APIs and Pre-rendering
getStaticProps ima pristup db, ne treba api da zove
------------------------------
// 48 - API Routes Summary
api routing princip isti kao za pages
filename gadja rutu
fja se zove handler
req res kao express
u getStaticProps, getServerSideProps, getStaticPaths ne zovi api jer imaju pristup filesystem i db, na bekendu se izvrsavaju
-------------------------------
// 49 - Styling Section Intro
kratak uvod:
global styles
component styles
sass
css in js
-------------------------------
// 50 - Global Styles
u pages/_app.js // wrapper za sve pages u aplikaciji
import '../styles/globals.css' // izvrsava se na svim pages
tu se ukljucuju css frejmvorci css, bootstrap.css npr
-------------------------------
// 51 - Component Level Styles
styles/About.module.css
import styles from '../styles/About.module.css'
.highlight { color: red; } // ista klasa na puno page, scopovana na page
<div className={styles.highlight}></div> // kreira unique class in dom
-------------------------------
// 52 - SASS Support
// modules radi isto sa scss
yarn add sass // mora install prvo

styles/_colors.scss
$red: red;

styles/About.module.scss
@import 'colors';
.highlight { color: $red; }

// identicno modules
import styles from '../styles/About.module.scss'
<div className={styles.highlight}></div>
-------------------------------
// 53 - CSS in JS
nije objasnio pravi css in js, jss, emotion

styled components...
install it and wrapp root component in _app.js
--------------------------------
// 54 - Styling Summary
nista rezime, trivijal 5 vid za styles
-------------------------------
// 55 - Miscellaneous Section Intro
app layout
head component
image component
absolute imports
html export
typescript
preview mode
next config file
redirects
env vars
--------------------------
// 56 - App Layout
bzvz
u _app.js
master template i ifovi da izostavi header i footer
--------------------------
// 57 - Head Component
head za meta, title, script itd, a ne header

// pages/index.js overajduje pages/_app.js
import Head from 'next/head'
<Head>
	<title>Codevolution</title>
	<meta name='description' content='Awesome YouTube channel' />
</Head>
--------------------------
// 58 - Image Component
slike idu u public folder
import Image from 'next/image'
<Image src={`/${path}.jpg`} />
Image resizuje i menja iz jpg u webp i smesta u .next/cache/images
kako fullstack...?
lazy load slika, samo koje su u viewport
----
1 slika, blured placeholder, samo staticki importovane
import img from '../public/1.jpg';
<Image src={img} placeholder="blur" />
blurDataURL="blurovana slika" za dynamic
------------------------
// 59 - Absolute Imports & Module Paths
absolute staze za import komponenti umesto relativnih, kao yarn workspaces

jsconfig.json
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@/layout/*": ["components/layout/*"]
    }
  }
}
import 'styles/globals.css' // "baseUrl": ".",
import Header from '@/layout/Header' // "@/layout/*": ["components/layout/*"] // path alias
-------------------------
// 60 - Static HTML Export
next build // builds in .next folder
next start // static gen i ssr
next export // static html bez nodejs server - backend, no isr, ssr, ali swr radi

"export": "next build && next export"
ne moze Image component, fallback: false

static gen - ssg
----------------------------
// 61 - TypeScript Support
tsconfig.json
yarn add --dev typescript @types/react
yarn dev
---
next-env.d.ts
---
prebaci jsconfig.json u tsconfig.json
---
tipovi: GetStaticPaths, GetStaticProps, GetServerSideProps, NextApiRequest, NextApiResponse
------------------------------
// 62 - Preview Mode
bypass static generation za draft postove iz cms-a bez build

cookies postavljeni u api kad je enabled
getStaticProps context.preview, context.previewData
u preview mode getStaticProps radi na svaki request, pa ne treba rebuild za preview changes

// enable
/pages/api/preview.js
export default function handler(req, res) {
  res.setPreviewData({
    user: 'Vishwas'
  })
  res.redirect(req.query.redirect)
}

// disable
res.clearPreviewData()
res.end('Preview mode disabled')
--------------------------
// 63 - Redirects
next-misc/next.config.js
redirects: async () => {
	return [
	  {
		source: '/about', // about na home
		destination: '/',
		permanent: false // 308 http, menjas rute trajno npr
	  }
	]
}
yarn dev
307 temporary, seo vazno
------------------------
// 64 - Environment Variables
.env.local // gitignored
DB_USER=Vishwas
// pristup u getServerSideProps npr
const user = process.env.DB_USER

yarn dev 
loguje u vs code

iz jsx-a env vars ne stizu u browser
za jsx mora prefix NEXT_PUBLIC_
process.env.NEXT_PUBLIC_ANALYTICS_ID

typically used for: base urls for api endpoints, secret api keys, google analytics key
-------------------------
// 65 - Miscellaneous Summary
nista, rezime
-------------------------
// 66 - Authentication Section Intro
uvod nabrojao, nista
next-auth lib
--------------------------
// 67 - Authentication in Next.js
authentication - identity
authorization - access, permission

next-auth lib uvod, nista
--------------------------
// 68 - NextAuth Setup

yarn add next-auth

github:
app name: bilo sta
homepage url: http://localhost:30000
callback url: http://localhost:30000

// next-authentication/pages/api/auth/[...nextauth].js
import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

export default NextAuth({
  providers: [
    Providers.GitHub({
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET
    })
  ],
 
 // lib kreira sign in page
 http://localhost:30000/api/auth/signin
 http://localhost:30000/api/auth/signout // isto
  
 jwt in cookie
 ------------------------
 // 69 - Sign In and Sign Out
 
 import { signIn, signOut, useSession } from 'next-auth/client'
 
 <Link href='/api/auth/signin'> // kako zajedno?
  <a
	onClick={e => {
	  e.preventDefault()
	  signIn('github')
	}}>
	Sign In
  </a>
</Link>
------------------------------
// 70 - Client-side Authentication
const [session, loading] = useSession()
!loading && !session && signin
session && signout
-------------------------------
// 71 - Securing Pages Client-side

// next-authentication/pages/dashboard.js
import { getSession, signIn } from 'next-auth/client'

const [loading, setLoading] = useState(true)

useEffect(() => {
const securePage = async () => {
  const session = await getSession()
  if (!session) {
	signIn()
  } else {
	setLoading(false)
  }
}

securePage()
}, [])
----------------------
// 72 - NextAuth Provider
next-authentication/pages/_app.js
<Provider session={pageProps.session}>
  <Navbar />
  <Component {...pageProps} />
</Provider>
--------------------------
// 73 - Server-side Authentication

/pages/blog.js
import { getSession, useSession } from 'next-auth/client'

// getSession zato sto ovde nemas provider, niti je react, global scope, a i server je
export async function getServerSideProps(context) {
  const session = await getSession(context)

ako je sesija salji userove blogove, ako ne salji free blogove

// solidna fora
prosledio session is Blog.getServerSideProps u _app.js <Provider session={pageProps.session}> pa u navbar procitano // ne znam kako je to moglo?
moze jer function MyApp({ Component, pageProps }) wrapuje svaki page posebno, pusta postojeci prop u context, a hook u navbar cita context
<Component {...pageProps} /> // Component bi trebalo da se zove Page

ali mora da zove Blog page pre toga, bzvz
-----------------------------
// 74 - Securing Pages Server-side
getServerSideProps salje na signin stranicu ako neulogovan posetis blog

export async function getServerSideProps(context) {
  const session = await getSession(context)
  if (!session) {
    return {
      redirect: { // moze redirect mesto props
        destination: '/api/auth/signin?callbackUrl=http://localhost:3000/blog',
        permanent: false
      }
    }
  }
-------------------------------
// 75 - Securing API Routes
getSession za fje i server i api, useSession za komponente, da ocitas sesiju
ispitas sesiju u api handleru pa saljes json i http code, trivijal

pages/api/test-session.js
// default je export, ne mora da se zove handler
export default async (req, res) => {
  const session = await getSession({ req }) // moze req mesto context
  if (!session) {
    res.status(401).json({ error: 'Unauthenticated user' })
  } else {
    res.status(200).json({ message: 'Success', session })
  }
}
--------------------------------
// 76 - Connecting to a Database
kreira mongodb atlas
u .env.local env var mozes da citas sa $VAR1 kao linux // nisam znao
DB_URL=url...$DB_USER:$DB_PASS... // interpoliras

pages/api/auth/[...nextauth].js

database: process.env.DB_URL, // ako samo ovo sesiju ce cuvati u db
session: {
	jwt: true
},
jwt: {
	secret: 'asdcvbtjhm'
},

yarn moze mesto yarn install da update paket
-------------------------------
// 77 - Callbacks

custom properties u user objektu sesije, session object

callbacks: {
	async jwt(token, user) {
	  if (user) {
		token.id = user.id // ubaci userId u token npr
	  }
	  return token
	},
	async session(session, token) { // token arg odozgo
	  session.user.id = token.id // imamo pristup sesiji, a ne tokenu, forward iz tokena u sesiju
	  return session
	}
}
---------------------------------
// 78 - Authentication Summary
rezime, nabrojao, nista
---------------------------------
// 79 - Deploying Next.js Apps to Vercel
trivijal
vercel.com sign in with github da bi pristupio repozitorijumima
bas prosto 3 klika
moze custom domain

----------------------------------
----------------------------------
getInitialProps() - If you're using Next.js 9.3 or newer, we recommend that you use getStaticProps or getServerSideProps instead of getInitialProps.
-------------
// docs
https://nextjs.org/learn/basics/api-routes
https://nextjs.org/docs/getting-started
https://vercel.com/docs/concepts/next.js/overview
https://vercel.com/guides/nextjs-prisma-postgres
https://github.com/prisma/blogr-nextjs-prisma/tree/main
https://github.com/prisma/prisma-examples
https://github.com/vercel/next.js/tree/canary/examples

-----------------------
// svg type
next-env.d.ts: declare module "*.svg";
------------------------
------------------------
// next env vars
// https://nextjs.org/docs/basic-features/environment-variables

// "default set" - public vars
.env // all environments (dev + prod)
.env.development // development environment
.env.production // production environment

// always overrides the defaults set 
// gazi ih sve, logicno kad si lokalno 
// tu su secrets - private vars
.env.local

// osim test
.env.test // .env.local ne gazi ovo
---
// ostale idu u git, i nemaju secrets
.env.*.local // should be added to .gitignore, ima SECRETS // samo je lokalno, ne koristi se nigde
----
// idu u git jer su javne, i ne trebaju .env.development.example i .env.production.example
.env.development i .env.production 
-------------
NEXT_PUBLIC_... // za browser, ok
-----
process.env.VAR1 // sve to izlazi u process.env
----
// interpolacija =$VAR1 (expand) i escape \$
A=abc
WRONG=pre$A // becomes "preabc"
CORRECT=pre\$A // becomes "pre$A"
------------------
pages filename lower caps zbog ruta
pages folderi kako logicno za rutiranje
api folderi za REST
--------------------
react query je isto sto i swr
-------------------
u tome i jeste stvar, povuces podatke u getServerSideProps,
a ne preko api pa client side rendering
-----
prisma query lici dosta na sequelize
----------------------
// next js middleware, rucno higher order function
// Next.js Tutorial: How to Add Middleware in Next.js API Routes
// https://www.youtube.com/watch?v=XbvnxKzVXks

withProtect(withRoles(handler))
-------------------
design system?...
-----------------------
// https://nextjs.org/docs/middleware
Middleware can be used for anything that shares logic for a set of pages
----
// https://nextjs.org/docs/api-routes/api-middlewares
continuacija moze i preko Promise resolve, reject, ne mora samo funkcija callback
konvertuje callback u promise // pitaj
// fn u sebi poziva ovu anon fju sa result
function runMiddleware(req, res, fn) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => { // fn ubacije svoju neku vrednost kroz result arg 
      if (result instanceof Error) {
        return reject(result)
      }

      return resolve(result)
    })
  })
}

// ovde je poziv fn // fn u svojoj definiciji poziva ovaj callback
function fn(req, res, fnArg) {
  ...
  fnArg(123); // 123 ulazi u result
}

objasnio ovde
// https://zellwk.com/blog/converting-callbacks-to-promises/
// https://stackoverflow.com/questions/22519784/how-do-i-convert-an-existing-callback-api-to-promises
---------------------
--------------------
// Next.js buildtime vs runtime env variables - docs
// https://nextjs.org/docs/basic-features/environment-variables
// https://www.youtube.com/watch?v=Mh9BJNfAVsM
NEXT_PUBLIC_ - rade samo u dev, salju se na client, zamenjuju se u buildtime
moraju biti dostupne na build
----
// buildtime vars - da ubacis vars buildtime u js bundle
// Next.js will replace process.env.customKey with 'my-value' at build time.
// next.config.js
module.exports = {
  // BUILDTIME VARS, zavrse u js bundle
  env: {
    // hardcoded ako je trivijal
    customKey: 'my-value',
    // moze i process.env ali mora da bude dostpan u OS tj ENV u docker
    VAR1: process.env.VAR1,
  },
}
// pristup normalno, both client and server
process.env.customKey
process.env.VAR1 // next.config.js overrideuje OS za next app
----
// runtime vars - dostupne samo serveru i pages i child compnents koje imaju getServerSideProps ili getInitialProps
// incompatible with Automatic Static Optimization - prednost buildtime
// https://nextjs.org/docs/api-reference/next.config.js/runtime-configuration
// next.config.js
module.exports = {
  // Will only be available on the server side
  // private
  serverRuntimeConfig: {
    mySecret: 'secret',
    secondSecret: process.env.SECOND_SECRET, // Pass through env variables (from buildtime)
  },
  // Will be available on both server and client
  // public
  publicRuntimeConfig: {
    staticFolder: '/static',
  },
}
sve sto je secret samo na server, sve sto je public i na server i na client
on client: publicRuntimeConfig available only to page (or component in a page) with getInitialProps (tj getServerSideProps)
and on server
---
u principu na clientu mozes da imas samo buildtime vars u produkciji, jer treba da se builduje
ssr je nesto izmedju pa moze
---
// pristup
import getConfig from 'next/config'
// Only holds serverRuntimeConfig and publicRuntimeConfig
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()
// Will only be available on the server-side
console.log(serverRuntimeConfig.mySecret)
// Will be available on both server-side and client-side
console.log(publicRuntimeConfig.staticFolder)
----
constants.js file - values, hardcoded code
utils.js - functions
----------------------------------
// getStaticProps vs getServerSideProps
// zapazi
// a to se samo u PROD vidi, u dev NE
getStaticProps radi (cita bazu) buildtime, getServerSideProps runtime, i to je cela mudrost 
home stranica
getStaticProps iskompajlira podatke buildtime i posle stranica ne reaguje na promene u bazi runtime
getServerSideProps i getStaticProps mozes da switch da test, isti args i return
---
// original log with explanation
// runtime data
λ  (Server)  server-side renders at runtime (uses getInitialProps or getServerSideProps) /
// no data
○  (Static)  automatically rendered as static HTML (uses no initial props) 
// buildtime data - SSG static site generation
●  (SSG)     automatically generated as static HTML + JSON (uses getStaticProps)
------------------
// dynamic ruta, kako da uhvatis i index, tj folder
// dynamic [], catch all [...], optional routes [[...]]
pages/setings/[[username]] // samo ovo baca exception, not supported
pages/settings/[[...username]] // mora ovako
---
pages/setings/[...username] // hvata sve /setings/a/b/c ali bez indexa settings/
---
pages/settings/[[...username]] - optional catch all, hvata i index i sve nested rute
// sve ovo hvata
https://localhost:3001/settings
https://localhost:3001/settings/a
https://localhost:3001/settings/a/b/c
----
// access param
// u stranici i komponenti
router.query?.username[0] // hvata 'a', 1 hvata 'b' itd
// u [[...username]].tsx getServerSideProps({params})
params?.username[0]
------------------
// rewrite - remap urls to /pages folders in next.config.js
// custom folder structure
// https://nextjs.org/docs/api-reference/next.config.js/rewrites
---------------------
// shallow routing - client side routing
// shallow - Update the path of the current page without rerunning getStaticProps, getServerSideProps or getInitialProps. Defaults to false
// https://nextjs.org/docs/api-reference/next/link#dynamic-routes
---------------------
// Image component
// https://www.youtube.com/watch?v=ckH6RHjy-5U
// Next.js 10's New Image Component - Leigh Halliday // nije bas dobro objasnio
// konvertuje jpg, png u webp
layout="fixed" width={100} height={100} // unapred znas visinu i sirinu i zadati
quality={75} // default
priority={true} // preload, nezavisno od scroll
----
// hendluje samo manje slike od pocetne
layout="intrinsic" width={500} height={500} // max-width max-height, ne raste preko max
----
// mora display: block
layout="responsive" width={500} height={500} // kako resize up zahteva vece i vece slike
-----
<div style={{postion: 'relative', width: '400px', height: '400px'}} >
  <Image layout="fill" /> // nema width, height, uzima od kontejnera, posicionirana absolute, ...?
</div>
---------
loader za external domain, i slike koje nisu public folder, build time
loader - samo url path concat
----------
// Next.js Image Component and Image Optimization + srcset and sizes explanation
//  Bruno Antunes
// https://www.youtube.com/watch?v=R4sdWUI3-mY
// https://github.com/bmvantunes/youtube-2020-dec-nextjs-image-component
na mobile nema potrebe velika rezolucija, na desktop salje vecu rezoluciju
content layout shift - kolapsirana slika dok se ne ucita
koristi web standard tehnologije
---
// razlicite slike za razl viewport width, w je u px
// na resize dovlaci vecu sliku
srcSet=
  small800px 800w,
  medium1200px 1200w,
  large1600px 1600w,
---
umesto 800w moze 1x, 2x - to je onda devicePixelRatio
---
1 sw px na 2-3 hw pixela
800/devicePixelRatio = 300npr // na tu rez menja sliku
devicePixelRatio u konzolu
----
// if else if else ...
sizes="(min-width: 767px) 33vw, (min-width: 568px) 50vw, 100vw" // da zavisi od kontejnera, ne od viewporta
ucitas manju sliku ako zauzima 33%
------------
// next.config.js
deviceSizes: [640, 750, ...] // koliko verzija iste slike, custom
----
intrinsic - default // resize...?
---
fixed - ne smanjuje i ne raste on resize
objectFit="cover" // props mapirani na obican css
objectPosition="bottom right" // da nije top left default, moze %, px too
---
// lazy default
loading="eager" ili priority={true} // da nije lazy na scroll, priority bolje performanse, docs
------------------------
// docs
// https://nextjs.org/docs/basic-features/image-optimization
remote images - koje nisu build time, mora width height props
---
// whitelist domains in next.config.js
domains: ['example.com', 'example2.com'],
---
Largest Contentful Paint (LCP) element - vidljiv pocetno
---
samo build time static images mogu bez width, height, velicina u html odredjena vel slike
-------------
// https://nextjs.org/docs/api-reference/next/image
-------------
// ima per app/document i per page dve razlicite Head komponente
import Document, { Head } from 'next/document'
import Head from 'next/head'
--------------------
--------------------
// seo
// Managing Assets and SEO – Learn Next.js - Lee Robinson
// https://www.youtube.com/watch?v=fJL1K14F8R8
// https://realfavicongenerator.net/
// og tags - open graph
// https://developers.facebook.com/tools/debug/
// https://cards-dev.twitter.com/validator
------------------------
// https://nextjs.org/docs/advanced-features/compiler
// SWC - Rust compiler used for compilation, minification, bundling
// cim imas .babelrc ne koristi swc nego babel, ok
When your application has a .babelrc file, Next.js will automatically fall back 
to using Babel for transforming individual files.
-------------------
// tesko, rastumaci
// 1.
Error: This Suspense boundary received an update before it finished hydrating. 
hydratation error, switched to client side rendering, startTransition
// kad imas useMe fetching jer je MeProvider IZNAD pages
// resenje - prebaci MeProvider u layout
-------
// 2.
// Error: inconsistent state Server: nesto, Client: nesto
// resenje - treba da proveris isMount {isMounted ? children : null}
nesto nisi prosledio u getServerSideProps, a useQuery je dovukao
----------------
// kad nesto nije mountovano to je SSR, mount samo na clientu
// https://github.com/vercel/next.js/discussions/35773
const [isSSR, setIsSSR] = useState(true);

useEffect(() => {
	setIsSSR(false);
}, []);
----------------------
// Next.js .env files
// https://nextjs.org/docs/basic-features/environment-variables
// priority (lowest to highest): 
`.env` -> `.env.development, .env.production` -> `env.local` -> `.env.production.local`
---
// for `NODE_ENV=test`, `env.local` is ignored (only `.env.test.local` is loaded)
---
// naming: .env.something123.local, ends with local
.env*.local
-----
.env.e2e.local ne postoji... samo .env.test.local
----------------------
// https://nextjs.org/docs/basic-features/environment-variables#exposing-environment-variables-to-the-browser
// only browser env vars are inlined in js bundle at build time with NEXT_PUBLIC_
server env vars are read at runtime
------
// ok poenta
.env.development - are default config common for all users, in git, public
.env.local - are real env vars, overrides .env.development, different for all users, not in git, secret + public specific for a user
-----------------
// https://nextjs.org/docs/basic-features/environment-variables#environment-variable-load-order
// Environment Variable Load Order
// 1. highest priority, cim nadje var izlazi
1. process.env
2. .env.$(NODE_ENV).local
3. .env.local (Not checked when NODE_ENV is test.)
4. .env.$(NODE_ENV)
5. .env
--------
better use .env.development.local and .env.production.local 
instead of single .env.local, its possible
------------------
.env.development just loads in regular process.env + priority, and thats it
--------------
// Next.js APP_ENV solutions
// all possible ways tutorial, Method 6 is the best and simplest
// load file you need, works for Next.js, tests and docker-compose.yml
// every process starts with yarn script
// https://getridbug.com/reactjs/how-to-use-different-env-files-with-nextjs/
// https://blitzjs.com/docs/environment-variables
-------------------------
// which env vars are inlined in bundle at prod build time
with NEXT_PUBLIC_ prefix, simple and logical
------------------------
// how long memory (states and cache) lives between pages in next.js app, prouci
-------------------------
// React Query
hydration error - ZESCA RAZLIKA U QUERY KEYS u getServerSideProps i useQuery na klijentu
zato nisi mogao da reprodukujes, jer se slazu knucevi na prostom primery
---------------------
// suspense CSR SSR
// https://dev.to/sidak/how-suspense-works-in-react18-3nm?utm_source=dormosheio&utm_campaign=dormosheio
SSR je samo go html i css loader dok se ne povezu javascript i html (hydration)(interactive page)
samo to je razlika spram CSR, taj html loader
---
hydration - interactivity - attach event handlers, javascript i html
---
SSR je za celu aplikaciju (ne za pojedinacne komponente)
------
jedna spora komponenta blokira ceo view, iako su druge spremne
---
Problem 1 - Fetch Everything, before you can Show Anything
Problem 2 - Load Everything, before you can Hydrate anything
Problem 3 - Hydrate Everything, before you can Interact with anything
-------
Suspense treba da omoguci nezavisno loadovanje svake komponente 
bez blokiranja celog viewa, paralelno
----
SSR isporucio html/css
to je lanac u browseru: 
1. fetch (download) javascript -> 2. load (parse) javacsript -> 3. hydrate (connect js and html)
------
// na srpskom // to to
i poenta je da sa Suspense nema blokiranja da laksa komponenta moze 
da dodje u fazu 3 (interaktivna) iako teza komponenta jos nije zavrsila ni fazu 1
-----
Hence we need not Fetch Everything before you can Show Anything.
Hence we need not Load Everything before you can Hydrate anything
Hence we need not Hydrate Everything before you can Interact with anything
----------------------------
// canary version // eto sta je
// https://github.com/vercel/next.js/blob/canary/contributing.md
Bug reports must be verified against the next@canary release. 
The canary version of Next.js ships daily and includes all features and fixes 
that have not been released to the stable version yet. 
Think of canary as a public beta. // to
----------------------------
// https://nextjs.org/docs/advanced-features/dynamic-import
// deferred loading  - libraries are only imported and included in the JavaScript bundle when they're used
import dynamic from 'next/dynamic'
const DynamicHeader = dynamic(() => import('../components/header'), {
  loading: () => 'Loading...',
})
// import() jeste iz js
-----
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/import
await import() is javascript keyword, samo sintaksa podseca na funkciju
--------------------------
// preview mode
kad se poseti ova ruta iz cms getStaticProps radi kao getServerSideProps i prima dodatne argumente
ima cookie i tokene za privatan pristup
export default function handler(req, res) {
  res.setPreviewData({})
}
za Contentful ima komplikovan setup, privatna ruta, posebno za svaku stranicu
---------------------------
// return fallback: ... from getStaticPaths
// https://nextjs.org/docs/api-reference/data-fetching/get-static-paths
// ako ima mnogo SSG stranica pokrijes samo najcesce, a ostale 404, SSR ili SSG on demand
export async function getStaticPaths() {
  const posts = await fetchPostPaths()
  return {
    paths: posts?.map(({ slug }) => `${routes.POSTS}/${slug}`) ?? [],
    fallback: true, // ovo
  }
}
-------------
// za nepokrivene u getStaticPaths:
fallback: false - 404
fallback: "blocking" - SSR
fallback: true - fallback pa SSG, za Google SSR
----------------
// sta je fallback page
starnica sa loader, trivial
https://nextjs.org/docs/api-reference/data-fetching/get-static-paths#fallback-pages
if (router.isFallback) {
  return <div>Loading...</div>
}
--------------------------------
// next-auth - custom provider
// opet samo api call, keys, urls, profile callback za usera
// gledaj postojece custom providere kao primere
// https://github.com/nextauthjs/next-auth/tree/main/packages/next-auth/src/providers
export default NextAuth({
    providers: [
      Providers.OAuth({
        id: 'custom-oauth',
        name: 'Custom OAuth',
        scope: 'profile',
        clientId: process.env.CUSTOM_OAUTH_CLIENT_ID,
        clientSecret: process.env.CUSTOM_OAUTH_CLIENT_SECRET,
        authorizationUrl: 'https://custom-oauth-provider.com/oauth/authorize',
        tokenUrl: 'https://custom-oauth-provider.com/oauth/token',
        profileUrl: 'https://custom-oauth-provider.com/oauth/userinfo',
        profile: (profile) => {
          return {
            id: profile.id,
            name: profile.name,
            email: profile.email,
          };
        },
      }),
    ],
  });

------------
// ili samo 1 wellKnown url
https://next-auth.js.org/configuration/providers/oauth#using-a-custom-provider
{
    id: "google",
    name: "Google",
    type: "oauth",
    wellKnown: "https://accounts.google.com/.well-known/openid-configuration",
    authorization: { params: { scope: "openid email profile" } },
    idToken: true,
    checks: ["pkce", "state"],
    profile(profile) {
        return {
        id: profile.sub,
        name: profile.name,
        email: profile.email,
        image: profile.picture,
        }
    },
}
------------- 
// posle samo filter u signIn callback
await NextAuth(req, res, {
  providers: [
      Providers.OAuth({
      id: 'custom-oauth',
      // ...
      }),
  ],
  callbacks: {
      async signIn(user, account, profile) {
      if (account.provider === 'custom-oauth') {
          // Handle the custom OAuth provider's response here
      }
      return true;
      },
  },
});
-----------------------------
// refresh token, prosto
// https://authjs.dev/guides/basics/refresh-token-rotation
pamtis i access_token (glavni) i refresh_token (sluzi samo za zamenu) i onda na rutu prosledis refresh_token
i uzmes novi access_token bez da user mora to rucno na time out da radi // ok, prosto
pamtis i expires_at da znas kad da ga zamenis
oauth provider mora da podrzava tu rutu
----------------------------
jwt callback - modifies token, persists data in token, (and returns it)
----------------------------
// OAuth flow
1. get oauth client_id and client_secret
2. call authorize route with id, secret, scope, redirect_uri, crsf_state
3. receive code and state on your redirect_uri
4. call token route to exchange code for access_token, refresh_token, expires_at, userId
5. call api route with access_token in barer header and userId and get user object
6. put user in database and session
-----------------------------
basic http auth je 'Authorization: user pass' header
-----------------------------
allowDangerousEmailAccountLinking: true je za isti email u facebook i google u [...next-auth].ts // eto
-------------
id_token je jwt token sa user info
---------------------------
// next.js eslint setup
// https://nextjs.org/docs/basic-features/eslint
next lint, 3 opcije: strict, base i cancel
{
  "extends": "next/core-web-vitals"
}
next/core-web-vitals - strict
next - base
cancel - bez eslint
--------------------------------
// next-auth
profile() callback u provideru vraca props koji ce biti ubaceni u jwt token
to je token u jwt() callback u [...next-auth].ts
----
// jwt() callback args
token - sta je uslo u jwt token
user - user tabela iz baze
account - account tabela iz baze
profile - user object from oauth provider response
----------------
// moze i events, ne mora jwt
https://next-auth.js.org/configuration/events#createuser
ima samo usera kao argument, bolje jwt callback
createUser: (message: { user: User }) => Awaitable<void>
----------------------------------
// next.js catch all dynamic routes
// /api/users/:id/accounts - to, vise segmenata
fora je fajl mora catch all route da bi matchovao sve segmente, inace samo :id
pages/api/users/[...id].ts
-------
// sa next-connect v1, mora cela ruta jer je catch all, ok, probao
nc.handler.get('/api/users/:id/accounts', async (req, res) => {
// object with array segmenata koji se dobije
req.query { id: [ 'clgywgbuo0000r6rrf8n97q01', 'accounts' ] }
---------
// bez catch all
pages/api/users/[id].ts
matchuje samo req.query.id, ostalo ignorise sa 404 // to
-------------------------------
// custom prisma adapter
return_ od CustomProvider.profile() callback se prosledjuje na PrimaAdapter.createUser()
-----
console log se ne ispisuje, a funkcija se izvrsava, wtf
monkey patched verovatno, kao jest, overriden
async createUser(user) { console.log('createUser user', user); }
------
next-auth ima default ponasanje, linkuje na logged in usera, ako ne kreira novog sa oauth
database adapter je prost, trivialan
-----------------------------
