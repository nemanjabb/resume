// Next.js 13 - The Basics - Beyond Fireship
// https://www.youtube.com/watch?v=__mSgDEOyv8
// https://github.com/fireship-io/next13-pocketbase-demo
nema pages folder nego app
----
// routing
[slug] - dynamic route
(path) - ignored folder for routing, route segment
----
// reserved filenames
page 
app/layout.tsx - master template za ceo app
// layout moze biti nested
app/layout/dashboard/layout/some-component.js
----
next dev --turbo // za turbopack
u next 13 components su server components by default
-------
// server component primer, app/notes/page.tsx

// fetch fja
async function getNotes() {
    // staticka ruta, SSG by default, ovo { cache: 'no-store' } je sada kao getServerSideProps
    const res = await fetch('http://127.0.0.1:8090/api/...', { cache: 'no-store' }); 
    const data = await res.json();
    return data?.items as any[];
}

// page je async komponenta zapazi, page je server componenta by default
// ako je staticka ruta SSG by default, cached
export default async function NotesPage() {

    const notes = await getNotes();  // fetch direktno u telu komponente, zapazi

    return(
        <div>
        <h1>Notes</h1>
        <div className={styles.grid}>
            {notes?.map((note) => {
            return <Note key={note.id} note={note} />;
            })}
        </div>

        <CreateNote />
        </div>
    );
}
-----
// getServerSideProps
fetch(url, {cache: 'no-store'})
------
// export vars iz component da podesis ponasanje, iskljucis cache npr, ako nije fetch()
export const dynamic = 'auto',
  dynamicParams = true,
  revalidate = 0,
  fetchCache = 'auto',
  runtime = 'nodejs',
  preferredRegion = 'auto'
----------
// dinamicka ruta, i dalje [id]
// app/notes/[id]/page.tsx
async function getNote(noteId: string) {
const res = await fetch(
    `http://127.0.0.1:8090/api/collections/notes/records/${noteId}`, // dinamicka ruta, SSR by default
    next: { revalidate: 10 }, // ISR, zapazi, 10 sec
    }
);
const data = await res.json();
return data;
}
  
export default async function NotePage({ params }: any) {
    const note = await getNote(params.id); // get id from url
  
    return (
      <div>
        <h1>notes/{note.id}</h1>
        <div className={styles.note}>
          <h3>{note.title}</h3>
          <h5>{note.content}</h5>
          <p>{note.created}</p>
        </div>
      </div>
    );
}
----
// SSG, prerender za dinamicke rute
export async function generateStaticParams() {} // ekvivalent getStaticPaths
----
// loading state je sad poseban fajl, loader dolazi sa servera?
// app/notes/[id]/loading.tsx
export default function Loading() {
    return <p>Loading...</p>
}
----
// error state, fallback
// app/notes/[id]/error.tsx
export default function Error() {
    return <p>error...</p>
}
-----
// client side rendering - componenta koja ima mutaciju
// app/notes/CreateNote.tsx
nije renderovana na serveru
'use client'; // ova direktiva na vrhu

import { useRouter } from 'next/navigation';

export default function CreateNote() {
  const router = useRouter();

  const create = async() => {
    // send form with http POST

    router.refresh(); // refetch query Notes page, zapazi
  }

  // render form...
}
-----------------------------
-----------------------------
// Next.js 13.1 Explained - roblee
// https://www.youtube.com/watch?v=_q1K7cybyRk
1. font
import { Inter } from '@next/font/google'
2. modularizeImports:
rezbija grupnu u posebne ImageBitmapRenderingContext?, mui primer
postavlja se u next.js.config
3. edge functions - api koje su distribuirane
export const config = { runtime: 'edge'}
4. middleware
kao u express, moze za auth, header, itd
da vrati ili prosledi dalje
5. turbopack
"dev": "next dev --turbo"
-----------------------------
-----------------------------
// Next.js 13 - How to use App folder & Layouts - Tuomo Kankaanpää
// https://www.youtube.com/watch?v=xXwxEudjiAY&t=15s
app/Layout.js je kao _app.js - export default function_ RootLayout({ children }
-----
page.js se ponasa kao index.php za folder, tj rutu
{params: {id}} - ruter u props
----
<a href=""/> refreshuje stranu
<Link href=""/> ne refreshuje stranu
------
// kao Suspense, ili useQuery from react-query
// triggeruje loader u parent komponentama
import { use } from "react"
const posts = use(getPosts())
------
layout je komponenta (page) sa children - i ima children props
fixan content, plus promenljive children, kao tab i content
---
// eto kako radi
layout hvata sibling page.js ili folder (sa novim layout), i prosledjuje u layout.children // ok, to je children prop - page or folder
plus 2 segmenta u ruti...
zapravo to je isto kao _app.js sto je bio
parent template prakticno
------------
vise stvari prosledjuje u layout - bukvalno tab sa rutom // to
page.js ili folder/page.js ili folder/layout.js (nested) // ok
-----------------------------
i head.js fajl se automatski ugradjuje
------
success, loading, error, ali no-data nije handleovan?
----------------------------
// next.js 13 server vs client components
// https://twitter.com/shadcn/status/1584971596389056512/photo/1
// Server Components 
Renders on the server / An async function when fetching data
Use to fetch data / has access to database and node.
Secure / Can use API keys
Cannot use browser APIs
Cannot use useState, useEffect..etc hooks 
No event listeneres (onClick, onSubmit...etc)

// app/posts/page.tsx
const response = await fetch("https://...°) {
  return await response. json()
}

export default async function Page() {
  const posts = await fetchPosts()

  return <Posts posts={posts} />
}
------------------------------
// Client Components
Renders on the client / Is NOT an async function_
Denoted by the "use client" directive
Cannot use API keys
Can use browser APIs
Can use useState, useEffect..etc hooks
Can use event listeneres (onClick, onSubmit...etc)

// app/posts/page.tsx
"use client"

import * as React from "react"

export function Counter() {
  const [count, setCount] = React.useState(0)

  return <button onClick={() => setCount(count + 1)}>Click</button>
}
--------------------------
If you have to fetch data or use Node.js APIs, use a server component.
If you have to use useState/useEffect or listen to onClick/onSubmit, use a client component.
--------------------------
// novi next-connect v1
gresku bacas sa throw new Error, a ne sa next(error);
--------
// moras da prosledis generics da bi radili tipovi od next.js, i rute i error handler
// Default Req and Res are IncomingMessage and ServerResponse, You may want to pass in NextApiRequest and NextApiResponse
const router = createRouter<NextApiRequest, NextApiResponse>();
--------
next-connect subroutes rade relative or absolute u odnosu na fajl? // absolute verovatno
---------------------------
// https://github.dev/ploskovytskyy/next-app-router-trpc-drizzle-planetscale-edge
// validacija env vars sa Zod, dobar, preuzmi
src/env.mjs
--------
// drizzle typescript orm, kao prisma
https://github.com/drizzle-team/drizzle-orm
--------
trpc i react-query dosta komplikovani
-----------------------
[...next-auth].ts.authOptions.callbacks.redrect this callback can be useful with absolute and relative urls. 
Unexpectedly it runs on many unrelated endpoints like `/api/users/:id`, etc. 
Might need override, default checks same domain.

https://next-auth.js.org/configuration/callbacks#redirect-callback

async redirect(params) {
    const { url, baseUrl } = params;
    return baseUrl;
}
-----------------------
// server components and actions (queries and mutations)
// Learn Next.js 13 With This One Project - Web Dev Simplified
// https://www.youtube.com/watch?v=NgayZAuTgwM
// https://github.com/WebDevSimplified/next-13-todo-list
----
moze samo u app dir
next.js hendluje error i loading states
----
// get, query
samo je async component i nema useEffect, evente i druge csr
i pozove prismu u komponenti
export default async function Home() {
  const todos = await prisma.todo.findMany() // eto
  ...
}
---------
// create mutation
samo ima "use server" handler na form action
async function createTodo(data: FormData) { // obican FormData
  "use server" // eto, run server only
  const title = data.get("title")?.valueOf()
  await prisma.todo.create({ data: { title, complete: false } })
}
<form action={createTodo} /> // form tag, action
--------
// update mutation
event, cela komponenta (fajl) mora "use client"
"use client"
onChange={e => toggleTodo(id, e.target.checked)}
---
handler fja u drugom fajlu "use server" i ne sme redirect
async function toggleTodo(id: string, complete: boolean) {
  "use server"
  await prisma.todo.update({ where: { id }, data: { complete } })
}
------------------------
// server components pitfalls, korisno, prouci
https://www.propelauth.com/post/5-common-pitfalls-with-server-components-in-next13-with-examples
------------------------------
// https://vercel.com/blog/introducing-next-js-commerce-2-0
prerendering nije vise page level vec per http request level (component)
// export iz api i pages
export const runtime = 'edge';
--------------------------
// Dockerfile.prod can be improved to exclude node_modules 
from image and reduce image size with `output: 'standalone'` in next.config.js
https://dev.to/code42cate/understanding-nextjs-docker-images-2g08
https://nextjs.org/docs/pages/api-reference/next-config-js/output
----------------------------
----------------------------
// Next.js 13 Crash Course Tutorial - Net Ninja, playlist - bezveze, mnogo trivial, nista
// https://www.youtube.com/watch?v=TJQbDPGzm0Y&list=PL4cUxeGkcC9jZIVqmy_QhfQdi6mzQvJnT
// https://github.com/iamshaunjp/nextjs-masterclass
// 1 - Introduction & New Features
server components js ne ide na client, moze import veliki libs
------
// 2 - SSR & Server Components (theory)
sada je SC CC na nivou componente, a ne stranice, page
jeste SC samo html, css, a CC ima js i hydration
SC - default, CC samo kad ti treba interaktivnost // ok
svaka komponenta je SC by default, ne samo page.tsx
// https://nextjs.org/docs/app/building-your-application/rendering/composition-patterns
// server vs client components, kad koja
// server:
Fetch data	
Access backend resources (directly)	
Keep sensitive information on the server (access tokens, API keys, etc)	
Keep large dependencies on the server / Reduce client-side JavaScript	
// client:
Add interactivity and event listeners (onClick(), onChange(), etc)	
Use State and Lifecycle Effects (useState(), useReducer(), useEffect(), etc)	
Use browser-only APIs	
Use custom hooks that depend on state, effects, or browser-only APIs	
Use React Class components
-------
// 3 - Pages & Routes
routing, trivial skroz, beskorisno
-------
// 4 - Layouts & Links, trivial opet
layout.tsx (children prop) wraps page.tsx u istom folderu, sibling
export const metadata = { ... } // SEO ide u layout export
<Link /> prefetches tu stranicu
-------
// 5 - Styles, Fonts & Images
// objasnjavao iskopirani css 10 minuta, bzvz
import { Rubik } from 'next/font/google' // font je hostovan lokalno
// Rubik je ime fonta
const rubik = Rubik({ subsets: ['latin'], preload: true })
// apply font as class
<body className={rubik.className} />
----
<Image placeholder='blur' /> // prikazuje blur boju id src u loading // eto, samo to
-------
// 6 - Fetching & Revalidating Data
// ISR
van komponente
async function getTickets() {
  const res = await fetch('http://localhost:4000/tickets', {
    next: {
      revalidate: 0 // use 0 to opt out of using cache // 30x, 30 sec pa sledeci req
    }
  })

  return res.json()
}
da li je samo za fetch()?
-------
// 7 - Dynamic Segments (Params) - trivial, van pameti, [id]
-------
// 8 - Static Rendering
cim imas dinamicku rutu [id] i id u fetch url i SSG ili ISR mora generateStaticParams, ex (getStaticPaths)
---
export async function generateStaticParams() {
  const res = await fetch('http://localhost:4000/tickets') // moze i ovde fetch, i ide uvek zapravo
  const tickets = await res.json()
  return tickets.map((ticket) => ({ id: ticket.id })) // vracas array of objects sa id [{id: 1}]
}
---
export const dynamicParams = false // vraca 404, disable SSG
default = true // onda pokusa oba fetch
----
if (!res.ok) { notFound() } // rucno 404 iz komponente
----------
// 9 - Custom 404 Page
pored page.tsx not-found.tsx za custom 404 page // trivial opet
mogu ugnjezdene da budu, onda se samo najdublje prikazuje, prioritet
---------
// 10 - Loading UI & Suspense
radi sa SSR (SC)
na 2 nacina:  // zapazi
1. loading.tsx
2. <Suspense fallback={...}/> boundary
---
loading.tsx sklanja samo page.tsx, ne i layout.tsx
----------
// 11 - Client Form Component
SC moze parent od CC
CC ne moze parent od SC
---
'use client' // koja ima hooks ili event handlers
----
trivial opet, samo POST submit form i router.refresh() za refetch
--------------
// 12 - Building the App
zapravo i jeste Static, SSG i SSR svodi se na props 
Static - go html
SSG - getStaticProps
SSR - getServerProps, getInitialProps
-----
npm run build i reklama, van pameti
---------------------------------
---------------------------------
// docs
// https://nextjs.org/docs/getting-started/project-structure
// reference app, ETO
// https://github.com/vercel/app-playground
routing cheatsheet:
// fajlovi
layout.tsx	         Layout
page.tsx	           Page
loading.tsx	         Loading UI
not-found.tsx	       Not found UI
error.tsx	           Error UI
global-error.tsx	   Global error UI
route.ts	           API endpoint  // eto
template.tsx	       Re-rendered layout // ovo je layout2 zapravo, child of layout.tsx
default.tsx	         Parallel route fallback page // ??
-----
// Dynamic Routes
[folder]	           Dynamic route segment
[...folder]	         Catch-all route segment  // eto
[[...folder]]	       Optional catch-all route segment
-----
// Route Groups and Private Folders
(folder)	           Group routes without affecting routing  // ignore segment u ruti /nesto/
_folder	             Opt folder and all child segments out of routing  // ignore folder from routing
-----
// Parallel and Intercepted Routes // druga ruta za log npr verovatno
ima jos... 
-----------------
// https://nextjs.org/docs/app/building-your-application/routing
u app folder samo page.tsx i route.ts se expose, ostali fajlovi se ignorisu // nije kao pages
----
Parallel Routes: show 2 stranice odjednom
Intercepting Routes: uhvati jednu i prikazi u drugoj
------------------
https://nextjs.org/docs/app/building-your-application/routing/pages-and-layouts#templates
template.tsx - isto kao layout.tsx samo state se reset i children se remount // ok
-----
// in layout.tsx
<head /> metadata, title, meta, za stranicu
export const metadata: Metadata = {
  title: 'Next.js',
}
ili generateMetadata()
-------
<Image /> - lazy loading, resizing
<Link /> - prefetching, soft navigation - render samo promenjeno, bez hard refresh i state reset
-------
time to first byte - server response
first contentful paint - load html and css
time to interactive - hydration
--------
Suspense - SSR loading na nivou komponente, ne stranice, delovi mogu da se ucitaju i budu interaktivni
--------------
// Middleware - kao i express middleware
run code before a request is completed
modify the response by rewriting, redirecting, modifying the request or response headers
or responding directly // moze i to
----
// middleware.ts
// poenta: to je samo obican request, response middleware, nema veze sa edge
export function middleware(request: NextRequest) {
  return NextResponse.redirect(new URL('/home', request.url))
}
// Matching Paths na koju se odnosi, param glob, moze i regex
export const config = {
  matcher: '/about/:path*',
}
moze i if sa return u fji
-----
moze i stranicu i apu rutu
-----
// respond directly
export function middleware(request: NextRequest) {
  return new NextResponse(...)
}
------
flags za '/' i absolute file path ili relative url
skipTrailingSlashRedirect, skipMiddlewareUrlNormalize
----------------
// Server Components - VAZAN CLANAK 1
React renders SC into a special binary format - React Server Component Payload (RSC Payload)
---
// RSC Payload
1. rendered result of Server Components - html
2. placeholders for Client Components and references to their JavaScript files
3. props passed from a Server Component to a Client Component
---
// na clientu:
show rendered html, non-interactive, only first page load
reconcile client virtual dom and update dom
The React Server Components Payload is used to reconcile the Client and Server Component trees, and update the DOM.
hydrate ONLY Client Components and make the application interactive // eto
----
// server rendering: 
1. Static - default, SSG, build time
2. Dynamic - SSR
3. Streaming - loading.tsx - route segments, <Suspense /> - components // zapazi
-----
// SSR triggers, dynamic functions: // zapazi, ENABLE SSR
cookies(), headers()
useSearchParams(), <Suspense><ClientComponent /></ Suspense>, enable CC SSG
searchParams - page prop
---------------------
// client components
opt-in - explicitno se definisu
----
interactivity: state, effects, and event listeners, interactive UI // zapazi ovo, drasticno, SC ne mogu ovo, undefined error
Browser APIs: geolocation, localStorage...
---
'use client' - direktiva iz React, ukljucuje fajl u client side bundle
samo parent, svi children postaju CC
------
sta radi React, a sta Next.js
-----
// initial page load
// server
na serveru render both SC and CC u html, non interactive, without js
react u RSC Payload, Next.js u html
// client
show html without js
reconcile dom
hydrate CC -  hydrateRoot React API
----
// subsequent navigations
only CSR za CC
----------------------
// Server and Client Composition Patterns - kombinovanje SC i CC
rendering poglavlje u docs je sustina
ponovljena tabela kad SC, kad CC
----
// server components patterns
context ne moze u SC // zapazi
fetch() ili React.cache() // zapazi ovu fju
---
u CC env vars without NEXT_PUBLIC are emty strings ''
import 'server-only' // paket da baci exception ako koristis u CC
----
// Using Third-party Packages and Providers
stare react libs will work within CC but will fail in SC
jer same nemaju 'use client', to je React-ova direktiva
resenje: my wrapper sa 'use client'
-----
context isto ne moze u SC
export const ThemeContext = createContext({}) // fails in SC
----
resenje: definisi provider u CC, pa children prop projektuje, nije cela app CC // ista fora kao dole
'use client'
export default function ThemeProvider({ children }) {
  return <ThemeContext.Provider value="dark">{children}</ThemeContext.Provider>
}
samo CC mogu da ga koriste // zapazi
-----
// client components patterns
isolate state u CC i keep them minimal, najnize u stablu
---
props od SC na CC mora da budu seriazable, inace fetch()
----
// Interleaving (preplitanje) Server and Client Components
SC moze child od CC
ne moze import SC u CC, nego mora passed kao prop
<ClientComponent> // CC ne zna nista o SC, samo gde da je prosledi
  <ServerComponent /> // children
</ClientComponent>
-------------------
// Edge and Node.js Runtimes
default node, edge moze da se izabere
node, serverless, edge // tabela
---
edge je cdn plus jos nesto, vercel, 4mb, subset node.js api, scalability highest
node - scalability low
serverless - scalability high, 50mb, jace i sporije od edge
----
page sad nije page nuzno nego route segment // zapazi
----
// define runtime in page or layout
export const runtime = 'edge' // 'nodejs' (default) | 'edge'
-----------------
// Caching in Next.js - VAZAN I DOBAR CLANAK 2
// Request Memoization - za nekoliko rerenders u 1 request
fetch() kesira za url (key) isto kao react-query i swr, u razlicitim komponentama
const res = await fetch('https://.../item/1')
po 1 rendering pass, posle reset - Per-request lifecycle
----
function_ returns - misli na fetch(), http ajax obican -  only applies to the GET method in fetch requests
react feature, u react component tree, nema u api 
kad nema http onda React.cache() da rucno memoize
// disable caching, opt out
const { signal } = new AbortController()
fetch(url, { signal })
-----------------
// Data Cache
isto za fetch() samo ne za request nego persistent
cache layer iza Request Memoization i data source (vidi sliku)
{ cache: 'no-store' } - bypass samo data cache, memoization ostaje
across requests and deployments 
kesira data source
------
// revalidation - updating cache
1. Time-based Revalidation - not critical
// Revalidate at most every hour
fetch('https://...', { next: { revalidate: 3600 } })
prvi request posle isteka i dalje vraca stale data, isto ako revalidate fails // drzi staro dok ne dobije dobro novo
----
2. On-demand Revalidation
by path (revalidatePath) or by cache tag (revalidateTag) - query key
imperativni poziv odmah sklanja (purge) data
-----------
// Opting out - disable cache
// pojedinacni fetch() poziv
fetch(`https://...`, { cache: 'no-store' })
// route segment - query key
export const dynamic = 'force-dynamic'
-----------------
// Full Route Cache - default SSG cache on server, zapazi - html, RSCP, js // js ne verovatno
split into chunks by: 1. routes segments and 2. Suspense boundaries
----
1. React -> Rendering on the Server
React Server Component Payload (RSCP) - Server Components, format optimized for streaming
Client Component - JavaScript
rezultat: RSCP za SC i js za CC
----
RSCP se salje na client za SAMO reconcile (razlika u virtual dom na serveru i clientu, to, a ne state) // poenta
Next.js vec na serveru prevede RSCP u html, i kesira ga // ok zapazi
---
reconciliation - react diffing algorithm // developerway podsetnik
---
2. Next.js Caching on the Server (Full Route Cache) // taj
kesira i html i RSCP (koji sluzi samo za reconcile)
ovo je SSG prakticno, i default je
rezultat: html, RSCP, js // zapazi
---
// eto
3. React Hydration and Reconciliation on the Client
  1. html - show non interactive
  2. RSCP - SC - reconcile server i client
  3. js - hydrate CC

benefit:
html moze odmah da se prikaze (non interactive)
---
4. Next.js Caching on the Client (Router Cache)
RSCP se kesira i na clientu, za prethodno posecene rute i link prefetching
rute ne mora cela stranica, moze i tab
--------------------
// Static and Dynamic Rendering
SSG -  data not personalized to the user, can be known at build time
SSR - data personalized to the user, information that can only be known at request time
dynamic functions - enable SSR // vidi gore Server Components 
dynamic routes arent cached, hit data source svaki put // to je sve
----
invalidate: 1. invalidate data cache, 2. new deployment
----
disable SSG, enable SSR: // eto
1. Using a Dynamic Function, The Data Cache can still be used
2. dynamic = 'force-dynamic' or revalidate = 0 route segment config options, disable both FRC i data cache
3. Opting out of the Data Cache
----------------------
// Router Cache - client cache
Client-side Cache or Prefetch Cache
cuva samo RSCP, key - route segments, duration - user session, both static and dynamic pages
---
svi <Link /> on page prefetched and cached, and back/forward history caching
no page refresh i React state and browser state is preserved
---
// duration
session, or page refresh
auto invalidation: // stale time
dynamic routes - 30 sec, switch to 5 min with prefetch={true} or calling router.prefetch
static routes (pages) - 5 min
----
// Invalidation
1. In a Server Action: // mutacija
  revalidatePath, revalidateTag
  cookies.set or cookies.delete 
2. router.refresh 
----
// disable router cache - nemoguce
<Link prefetch={false} /> // moze samo prefetch disable
----------------------
// Cache Interactions - kako razliciti cache interaguju medjusobno
revalidating Data Cache will invalidate the Full Route Cache, obrnuto ne
revalidating  Data Cache will not immediately invalidate the Router Cache, mora rucno
revalidate data i router cache -> revalidatePath or revalidateTag in a Server Action
-------------------------


