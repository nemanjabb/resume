-----------------
// state vs props
class MyComponent extends LitElement {
    // Define static properties to configure behavior
    static properties = {
      data: { type: Array },
      error: { type: String }, // props
      _myState: { type: String, static: true } // valjda tad je state
    };
  
    constructor() {
      super();
      // Define initial state
      this.data = [];
      this.error = '';
    }
}
-------
// prop forwarded into state .data ??
// <child-component .data=${this.parentData}></child-component>
-----------
// dekoratori su typescript
@property()
name?: string;
----
// javascript
static properties = {
    name: {},
};
------------------
// they dont need to be imported, they just need to be registered in the customElements registry at runtime
// ovo je register komponente, ne mora import u istom fajlu
customElements.define('my-web-component', MyWebComponent);
--------------
// vazno
restart storybook u terminal da vidis nove fajlove
---------------
kad nije registrovana lit componenta ponasa sa kao div // zapazi, velika fora
------------
// class prop ne treba da se forwarduje jer je ovo vec tag
<my-component class="mx-auto max-w-sm flex-grow" />
// sam tag moze da primi klasu, ne ide na root elem, medjuelement?
<my-component class="mx-auto max-w-sm flex-grow" />
-------------
// lit inline styles
ok, use a style=" " attribute until then or static styles = css``
---------------
.prop je za objekte?
-----
trazi document.createElement()  za mount <my-component />
document.createElement('my-component');
--------------------
--------------
variajcije class_ string notacije u lit 
class="py-[${INPUT_STYLES.paddingY}px] box-content"
class="${`py-[${INPUT_STYLES.paddingY}px]}` box-content"
---
prop mora kao string da se prenosi 
position=${this.#messagePositionInGroup} 
i ${var} se ponasa kao `${var}`, sa i bez backticks
----------
[optionalVar] // optional type je u []
-----
methods su default public u javascript class_
------------
all jsdoc types are exported by default 
---
jsdoc export {} je da js moze da importuje fajl, a ts vec importuje tipove
--------
// jsdoc import types, 2 nacina
// mozes destrukturirani objekat direktno da anotiras {{ }}
1. inline
/**
 * @type {{field1?: string, object1?: Partial<import('@package/folder').Object1Type> }}
 */

2. import_ star 
import * as myObject1Types from '@package/folder';
/**
 * @type {{field1?: string, object1?: Partial<myObject1Types.Object1Type>}}
 */
const { field1, object1 } = body;