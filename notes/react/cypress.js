// https://docs.cypress.io/guides/core-concepts/writing-and-organizing-tests#Hooks
beforeAll, afterAll se zove before() i after() u cypress
----
mora tsconfig.json za .ts testove
-------------------
// cypress docker
// Run Cypress with a single Docker command
// https://www.cypress.io/blog/2019/05/02/run-cypress-with-a-single-docker-command/
docker run -it -v $PWD:/e2e -w /e2e cypress/included:3.2.0
---
The image cypress/included:3.2.0 has the entrypoint set to cypress run, so you 
dont need to type it when running our Docker image. If you want a different command, 
you can change the entrypoint and then pass any additional arguments after the image name.
---
// objasnjenje
entrypoint je komanda sa default argumentom, a komanda je overrided argument
spoljna i unutrasnja funkcija, callbacks, kontinuacija
entrypoint = entrypoint + command
---
// samo tests, a gde je app? on web
docker run -it -v $PWD:/e2e -w /e2e --entrypoint=cypress cypress/included:3.2.0 help
----
// disable video with env, ukljucen default
$ CYPRESS_VIDEO=false
$ docker run -it -v $PWD:/e2e -w /e2e -e CYPRESS_VIDEO cypress/included:3.2.0
---
// x11 server strimuje sliku van kontejnera kroz socket, terminal pokrene prozor kao browser
-v /tmp/.X11-unix:/tmp/.X11-unix = map X11 socket file to communicate
-e DISPLAY           = pass environment variable DISPLAY to the container
-------------
// docker-compose
// both app and cypress (test runner) in containers (d-c.yml)
cypress run // koristi default entrypoint i command, headless
// i workdir iz Dockerfile moze da se overriduje u d-c
working_dir: /e2e
----
depends_on:
- app-service
-----
// mora service name mesto localhost, sad je 2 containera, a ne host i container
// override baseUrl from cypress.json with env var
environment:
// pass base url to test pointing at the web application
- CYPRESS_baseUrl=http://sentimentalyzer:8123
-----
// start app, run tests headless, stop services
docker-compose up --exit-code-from cypress
----
// run x11 in d-c
kad imas docker-compose.nesto.yml on extenduje docker-compose.yml (koji je kao base)
---
// override entrypoint i command za gui
entrypoint: cypress open --project /e2e
-----
// cypress container, app on host
// docker RUN
docker run -it \
  --config baseUrl=http://host.docker.internal:2222 // pass hostname of the host
-----------------------
// cypress github actions
// https://github.com/bahmutov/cypress-gh-action-included
----
// # normally we would NOT need to install NPM dependencies
// # but in this repo we are using additional plugins listed in package.json
// # thus we need to install them
- name: Install dependencies 📦
  // # we could simply run "npm ci" but that would not cache dependencies
  // # thus we can use this 3rd party action
  uses: bahmutov/npm-install@v1
---
// mora npm install jer u docker slici nema svih paketa
// slika
container: cypress/included:4.11.0
// dodatni paketi
"devDependencies": {
    "@testing-library/cypress": "6.0.0",
    "cypress-terminal-report": "1.4.1",
    "typescript": "3.9.7"
}
--------------------
// cypress github actions
// https://docs.cypress.io/guides/continuous-integration/github-actions
// tekst iz video klipova prakticno
paralelizacija - install i worker jobs, zato, zapazi
workeri ce raditi paralelno
install i workeri moraji koristiti isti image container
// parallel
// samo ce postojece testove da paralelizije na vise nodes
// a ne svaki job ma drugi node
// to je onaj klip sa dashboard koji sam preskocio
ui-chrome-tests: // sve u worker job
strategy:
fail-fast: false
matrix:
  // # run copies of the current job in parallel
  containers: [1, 2, 3, 4, 5]
---
- name: 'UI Tests - Chrome'
uses: cypress-io/github-action@v2
with:
  ...
  parallel: true
--------
// youtube playlista iz docs
// 1. - what is CI - integrate work from developers
// https://www.youtube.com/watch?v=N0TOFWy1Xvg&list=PL8GlT7H3xOcLJMIPhxlZ8W9kgbeMqW7cH&index=2
----
// trivial github action yml, thats it
// https://github.com/cypress-io/todomvc
----
// 2. GitHub Actions + Cypress: Actions & Workflows
action - automated jobs
workflow - file.yaml, collection of actions
// 3. GitHub Actions + Cypress: Example App Overview
demo todo app and run Cypress gui locally
// 4. GitHub Actions + Cypress: Understanding how to configure a workflow
explains cy-tests.yml file
// 5. GitHub Actions + Cypress: Running Tests in GitHub Actions CI/CD Workflow
pokrene yml na Githubu, nista
// 6. GitHub Actions + Cypress: Debugging Test Failures in CI
Cypress dashboard, nebitno
// 7. GitHub Actions + Cypress: Running Tests in Parallel
real world app yml objasnjen odlicno ispod
// https://github.com/cypress-io/cypress-realworld-app
// projekat je kompletan CI primer za sve testove
//.github/workflows/main.yml
// poenta: koristi image bez Cypress i instalira ga sa github action, a arg runTests: false
jobs:
install:
  runs-on: ubuntu-latest
  // # image with just browsers without Cypress
  container: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  steps:
    - name: Checkout
      uses: actions/checkout@v3

    // # just install Cypress on bare image with browsers
    // # action can install Cypress and run tests
    // # da bi izbegao Dockerfile with additional dependecies // poenta verovatno
    // # actually to reuse install step
    - name: Cypress install
      uses: cypress-io/github-action@v2
      with:
        runTests: false // this
---
// reuse - save built code between jobs
- name: Save build folder
uses: actions/upload-artifact@v3
with:
  name: build
  if-no-files-found: error
  path: build
----
// use it like this in ui-chrome-tests job
- name: Download the build folders
uses: actions/download-artifact@v3
with:
  name: build
  path: build
---
// reuse install job in other jobs
ui-chrome-tests:
// # this, like depends_on, inace jobs default rade paralelno
needs: install
-----------
// docs, u dnu
// github login da se posalje komit u dashboard, da se zna koji komit je testiran
uses: cypress-io/github-action@v4
GITHUB_TOKEN: ${{secrets.GITHUB_TOKEN}}
-----------
npm ci - yarn install --frozen-lockfile
-----------------
// Cypress env vars
// https://docs.cypress.io/guides/guides/environment-variables#Overriding-Configuration
// override, most right - most priority
cypress.config.js -> cypress.env.json -> CYPRESS_*  regular env var -> cypress run --env v1=val -> describe('', {env: {v1: 'val'}}), or it()
------------------------
// firefox, chrome imaju api za automatizaciju - driver
Selenium WebDriver - a W3C compliant implementation of WebDriver
Playwright - a Node.js library to automate Chromium, Firefox and WebKit
Puppeteer - a Node.js library to automate Chrome

