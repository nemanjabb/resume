// only tutorials here

//  How to Setup Shadcn UI + Themes in NextJs 14 - Hamed Bahram
https://www.youtube.com/watch?v=l93uukpAoxE
// nesting client and server components, interleaving, mesanje
1. kada prosledjujes server component kao prop (children) u client component ('use client') sve ok, server component prosledjena u slot 
2. kada IMPORTUJES server component u client component, i ona postaje client component 
primer za theme provider
----------------------
----------------------
// odlican pregled svih formi u react/next.js
// Using React Hook Form, Zod Validation and Server Actions in NextJs - Hamed Bahram
// https://www.youtube.com/watch?v=R_Pj593TH_Q
// https://github.com/HamedBahram/next-rhf
1. event.currentTarget
event.preventDefault()
const form = event.currentTarget
const formData = new FormData(form)
const formDataObject = Object.fromEntries(formData)
2. controlled inputs 
3. react-hook-form register()
4. server actions
----
watch je subscribe, triggeruje rerender samo onChange na tom inputu
console.log(watch('email'))
console.log('rendering')
----
state - rerender, je watch ili subscribe
----
server action ispod haube salje POST na istu stranicu
<form action={serverAction} />
radi i u client i server components
-----
// paralelno teras rhf i server action
rhf - client (validation)
server action - handle data i server validation 
// ovo nije dobro u rhf onSubmit da poziva akciju, nece bez js
<form onSubmit={handleSubmit(processForm)} />
-----------
// React Hook Form & React 19 Form Actions, The Right Way - Jack Herrington
// https://www.youtube.com/watch?v=VLk45JBe8L8
// https://github.com/ProNextJS/forms-management-yt
// mora i useFormState za server action 
import { useFormState } from "react-dom"; 
// state je response, formAction je server action fja
// jeste isti potpis kao useState, zapazi
// moze useTransition uz njega kao uz state
const [state, formAction] = useFormState(onSubmitAction, {
  message: "",
});
<form
ref={formRef}
className="space-y-8"
action={formAction} // sto je vratio useFormState()
onSubmit={(evt) => {
  evt.preventDefault();
  form.handleSubmit(() => { // react hook form handle submit
    formAction(new FormData(formRef.current!));
  })(evt);
}}
/>
// server action
export async function onSubmitAction(
  prevState: FormState, // prvi arg, ima taj potpis, iteracije za vise poziva
  data: FormData
): Promise<FormState> {...}
// api response type prakticno, ti definises, message, error, status
export type FormState = {
  message: string;
  fields?: Record<string, string>; // prosledjuje u krug, bezveze
  issues?: string[];
};
---------------
const {pending} = useFormStatus() // za loading state
-------------------
// route groups (folderName)
// https://nextjs.org/docs/app/building-your-application/routing/route-groups#convention
taj folder ne kreira segment u path, unutra page.tsx, layout.tsx, route.ts su rutabilni i grupisani
--------------------
// unstable_cache } from 'next/cache' vs { cache } from 'react'
// https://nextjs.org/docs/app/building-your-application/data-fetching/fetching#caching-data-with-an-orm-or-database

// this will prerender PAGE on build
import { unstable_cache } from 'next/cache'
const getPosts = unstable_cache(
  async () => {
    return await db.select().from(posts)
  },
  ['posts'],
  { revalidate: 3600, tags: ['posts'] }
)
// page server component, static zbog cache
export default async function Page() {
  const allPosts = await getPosts()
 
  return (
    <ul>
      {allPosts.map((post) => (
        <li key={post.id}>{post.title}</li>
      ))}
    </ul>
  )
}
--------
// will cache db QUERY
import { cache } from 'react'
export const getPost = cache(async (id) => {
  const post = await db.query.posts.findFirst({
    where: eq(posts.id, parseInt(id)),
  })
  return post
})
------------
// what is web api?
https://developer.mozilla.org/en-US/docs/Web/API
-----------------
// route handlers, api routes
// https://nextjs.org/docs/app/building-your-application/routing/route-handlers
// https://nextjs.org/docs/app/api-reference/file-conventions/route-segment-config // reference koj sta
export const dynamic = 'auto' // ssr, ssg ...
export const dynamicParams = true // dynamic route [slug].tsx, 404 slugs fallback to ssr or 404 error page
export const revalidate = false // isr
export const fetchCache = 'auto'
export const runtime = 'nodejs'
export const preferredRegion = 'auto'