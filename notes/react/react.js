//49
properties su bindovani atributi na komponenti, tagu
-----
prosledi argumente u event handler, binduje za lambdu
stanje ne menja nego klonira celo pa u setState()
event handleri su imenovane lambde jer this nije predefinisano, iz klase je
u strict mode this nije window nego undefined
objekti u polju su reference pa moraju i oni spread

za js destrusturing, spread, lambde itd

destructuring novo ime {data: novoIme} = nesto;
----------------
//https://www.youtube.com/watch?v=qh3dYM6Keuw state vs props
react rerender ceo virtual dom svaki put, a stvarni dom samo taj deo
na promenu state-a
state se tice samo te komponente
props se prosledjuju nadole kroz tag, kao @Input
--------------
{nesto} je krace od {nesto: nesto}
{[key]: nesto} computed property
const { [event.target.name]: previousSelectedCountry, availableCountriesForVote } = this.state;
previousSelectedCountry je rename u destructuring
-----
this.setState({
 availableCountriesForVote, //mesto availableCountriesForVote: availableCountriesForVote
 [event.target.name]: parseInt(event.target.value)
})
-----
kad prosledi fju kao prop, poziva je u child a definicija je u parent
-----
za select options postoji hidden atribut
<option key={country.id} value={country.id} hidden={country.selected}>{country.name}</option>
-----------------------
//test
arrange, act, assert

//frameworks
mocha
jasmine
jest
ava

//assertion libraries
chai
expect

//helper lib
react test utils
enzyme //jquery like
-------
action creators - trivial
store - integration test
reducer
mapStateToProps
thunk asinhroni
-----------------------------
//https://www.youtube.com/watch?v=sBws8MSXN7A React JS Crash Course - 2019
fje kao props idu na gore, this.props.fja(bind, idArg), i u root komponenti imas fja(idArg) //pros
u root komponenti menja state
obicni props idu na dole
----
style jsx css backgroundColor:'red', //camelcase, stringovi, objekat
---------------------
//grider
controlled element, input npr, stanje u this.state umesto u dom element, value={this.state.value}
u state samo nesto sto se menja u vremenu, za rerender, inace this.nesto obican
ref={this.nekiRef} je template ref iz angulara
props se prosledjuju kroz constructor{props} ili koponenta (props) => {}
kroz props prosledjuje fju, definicija u parent, zove je child sa argumentima
<parent><child/></parent> child se projektuje gde treba sa {this.props.children}
-----------------
mapStateToProps() state koji je return se pojavi kao props u komponenti koja je prosledjena u connect, pros
procitao, povezao store sa komponentom
{named export}, bez zagrada default export

-----------------------
//15 dobar redux async thunk
connect fja zove store.dispatch() sa akcijama koje su prosledjene kroz objekat argument

reactov posao je rerender na props i state i eventi

store = Redux.createStore(Redux.combineReducers(...)
store pravi od reducera

middleware ko i svaki, pipeline, filter, prosledjuje
thunk je middleware koji moze da primi action creator obj ili fju
redux je pipeline
thunk ima 14 linija koda

(dispatch, getState) fje kao argumenti, dispatch rucno podize action type, payload

reducer vraca deo statea
--------------

u index.js entry point 
const store = createStore(reducers, applyMiddleware(thunk)); //thunk ili bilo koji drugi middleware
<Provider strore={store}><App /> ubacuje store u App komponentu
----
thunk, action creator vraca akciju ili fju, glavna fora, pros

export const fetchPosts = () => async dispatch => {
 const response = await jsonPlaceholder.get('/posts');
 dispatch({type: 'FETTCH_POSTS', payload: response.data});
};

skraceno od 

function fetchPosts() {
 return async function(dispatch, getStore) { 
  //dispatch je fja za dispatchovanje akcija
  //getStore je fja koja vraca store sa svim podacima koje su poslali reduceri
   const response = await jsonPlaceholder.get('/posts');
   dispatch({type: 'FETTCH_POSTS', payload: response.data});
 }
}
----
lodash _. koristan kao collection u laravelu
prekucaj slajd [..., ] ekvivalente za push, pop() itd za reducere
---
sitne akcije pa kombinuje bolje nego memoize za 1 api poziv

jedna linija u lambdi return podrazumevano, to je fora za thunk return fja //TO TO
fja1arg => fja2arg => {} sa function return jasnije vraca fju, PROS

async moze da zakaci gdegod
na zadnjoj liniji await nema potrebe
----------
kroz connect prosledjujes mapsStateToProps() i action creatore
mapsStateToProps prosledjuje state u komponentu pojavi se kao props, da se korisiti u celoj komponenti moze i u componentDidMount()
-----------------
export default connect(mapStateToProps, mapDispatchToProps)(Editor);
mapStateToProps prosledjuje state u komponentu
mapDispatchToProps prosledjuje action creatore u komponentu, grider ih zvao direktno, ovaj ih mota u lambde
akcija a connect() se importuje gore import...
----------------
props se prosledjuju od parenta na child kroz tag
-------
<Switch> returns only one first matching route.
exact returns any number of routes that match exactly.
-------
u reduceru se vide svi propertiji statea, svi caseovi ispred ...spread,npr // to to      

case REGISTER:
      return {
        ...state,
        inProgress: false,
        errors: action.error ? action.payload.errors : null
      };
a eventualno computed property return { ...state, [action.key]: action.value }; vidis gde je dispatched  ta akcija u komponenti dispatch({ type: UPDATE_FIELD_AUTH, key: 'email', value }),

koji reducer je za taj kljuc statea const mapStateToProps = state => ({ ...state.auth });
If you are using combineReducers, then youd look at whatever reducer was attached at the `auth` key at the root
-------------
() zagrade

<laravel7> why would someone write const mapStateToProps = state => ({ ...state.auth }); ,isnt const mapStateToProps = state => { ...state.auth }; enough?
<evulish> laravel7: no, a => { ...a } interprets the {} as brackets for the function
<evulish> laravel7: you could do a => return { ...a } or a => ({ ...a })
vraca objekat a ne blok funkcije, jedna linija sa () podrazumeva return
---------------
//7 slajd
connect(mapStateToProps,                 mapDispatchToProps)
What state should I expose as props?, What actions do I want on props?
//Wrap Manually
function mapDispatchToProps(dispatch) {
 return {
 loadCourses: () => {
 dispatch(loadCourses());
 }
}
// In component...
this.props.loadCourses()
---------------------
class Login extends React.Component {
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => ev => {
      ev.preventDefault();
      this.props.onSubmit(email, password);
    };
  }
<laravel7> qswz, why define local method in constructor instead regular class method?
<qswz> laravel7: for accessing `this` in it
<qswz> use class fields
<qswz> your link is an old way for class Foo { changeEmail = ev => { } }
-----------
//https://github.com/gothinkster/react-redux-realworld-example-app/wiki
//agent.js
na dnu ima 
export default {
  Articles,
  Auth,
...
};
exportuje "servise"
svi koriste 
const requests = {
  del: url =>...
------
General Reducer Patterns
map payload into piece of state
---
mapStateToProps - Take some objects, use them in render.
mapDispatchToProps will be referred to as "handlers"
----
{...state, nesto} je skraceno od {...state, nesto: nesto}
--------------------
// turcin telefoni
kontejneri su class komponente sa reduxom
components su fukcionalne komponente bez reduxa
pages su funkcionalne komponente sa reduxom

//css
rem - <VioletPixel> Like you said above, rem is relative to the font-size of the root element.  On web pages, the <html> element is the root element.  
So, if you have html { font-size: 20px; } that means 1 rem will be 20px, for example.
----
position absolute
absolute 	The element is positioned relative to its first positioned (not static) ancestor element
relative 	The element is positioned relative to its normal position, so "left:20px" adds 20 pixels to the element's LEFT position
--------------------
//component, container
<laravel7> whats the difference between container and component?
<GreenJello> laravel7, a component is a concrete thing. You give components to react, and it does things with them. 
A container is a term that refers to components that interact with data and external services, rather than being focused on presenting UI
<GreenJello> laravel7, you might have a UsersContainer component that fetches user info from an API, and only renders a single element, 
a UsersList component, which doesnt interact with external services, but rather presents the simple array of data it receives from UsersContainer
<GreenJello> you could put all of that in one component; its an organizational pattern, rather than a technical construct
<GreenJello> laravel7, in general, if a component obviously interacts with redux, its doing container things
<GreenJello> presentational components (the ones that define the UI, like buttons, images, etc.) should not interact with redux
--------------------
//lifecycle methods
componentDidUpdate()
The most common use case for the componentDidUpdate() method is updating the DOM in response to prop or state changes.
--------------------
//twitter dm, context api grider
komponente su kao fje, pozivaju se odozgo na dole sa argumentima
mapStateTiProps = (state, ownProps) => ... imas props kao argument zapazi
pravilo - svaka komponenta mora sama da fetch sve svoje podatke, ne sme od druge komponente da koristi
funkcionalna komponenta kad ti ne trebaju lifecycle hoohs methods
PUT kad izostavis property on ga izbrise na serveru
---
portal - da komponenta ne bude child nego stavljena u id="modal" u index.html createPortal(), 2. arg je gde kacis
---
componentDidMpdate() followup rerender
---
promena state izaziva rerender svih child podkomponenti
---
context api prosledi podatak bilo kom unuku, nasledniku...
ubaci vrednost ili default u createContext("english") ili kroz <Provider value={"english"}/> umotas
citanje static contextType = LanguageContext pa u this.context imas "english"
citanje drugi nacin <Consumer>{(value)=> }</Consumer>
za vise contexta drugi nacin, umotas u vise <Providera> i <Consumer>{(color)=>... umotas i u fju zapazi
prosledio language i fju kao objekat kroz context i to mu je store, store mu je state u root komponenti
-------------------
//hooks, grider
hooks su state i lifecycle methods za funkcionalne komponente

array destructuring [arr0, arr1] = arr
[resource, setResource] = useState(init vr)
state je bio objekat, resource je vrednost, setResource() je setState(), pros

componentDidUpdate() se zove na rerender na promenu props ili state, ako menjas state unutra rekurzija loop
prevProps arg je props iz komponente i poredi se da nije jednak sa sto stize iz parenta
console.log() moze, kasni vrednost za 1

componentDidMount() je onLoad, 1 na pocetku se zove

useEffect(()=> ,[arg]) fja u useEffect se zove samo kad se arg promeni, a useEffect() se zove kad i componentDidMount i componentDidUpdate

na [] useEffect() samo 1 kao componentDidMount, na obj i arr reference poredjenje, na izostavi arg zove se na svaki rerender
ili spoljna fja pa poziv ili iife unutra

ne mora u komponentu, moze i fju pa reuse
za reuse sta je ulaz a sta izlaz fje, vraca se polje za slozeno

trivijalno da se prevede state i lifecycle u hooks

mesec dana 47 sati, 27 foldera
---------------------------
//yt clone rad
laravel7, You can do it directly like `render={(renderProps) => { console.log(renderProps); return <Component {...renderProps} />; }}`
<Route path="/results" render={() => { console.log(this.props); return <Search key={this.props.location.key}/>; }}/>
----
vs code zadrzis kursor pokaze scss selektor sta pokriva, dobar
ugnjezdene klase, tako enkapsulira css za komponentu
gledas u import odakle je komponenta
----
sve presentation components samo props, nemaju redux
containers redux
------
export const fetchVideoCategories = fetchEntity.bind(null, api.buildVideoCategoriesRequest, videoActions.categories);
laravel7: they are fixing the arguments ahead of time

----------------------------
const Button = ({ children, loading, ...props }) => (
    <button className="button" disabled={loading} {...props}>
        {loading ? 'Loading...' : children}
    </button>
);
laravel7, It allows any other props to be directly passed to the button. e.g., buttons usually should be passed type or onClick.
pick some pass the rest
------------------------
//saga
//what the splash primer
call i fork - fje, fork istovremeno
put(actionCreator(args))
slusanje akcije - watch - while(true) i take(action), watchStatsRequest() pa unutra handleStatsRequest() npr, logicno
ispred svakog effecta ide yield
select(fja) ako treba nesto iz statea
pros, logican, sve
jedna akcija iz komponente koja trigeruje sagu pa lancano
// saga
saga drzi action creators pure, a sideeffects i asinhroni kod izdvaja u sage
kreira novu akciju za sagu
saga run() resolvuje promise
action creatori samo u thunku
svaki yield je jedno vracanje tj poziv fje i cekanje, zato je dobro za asinhrono

--------------------------
//State Updates May Be Asynchronous 
this.setState((state, props) => ({
  counter: state.counter + props.increment
}));
---
kad gledas komponentu kreces od reneder fje

---------------------
// hoc
higher order component je funkcionalna komponenta koja prima class component kao argument i poziva se na komponentu za reusability
prosledjuje ...this.props u ChildComponent
malo pocetno slovo fajla exportuje fju, veliko klasu

// middleware
middleware next() salje na sledeci midleware, dispatch(action) salje na pocetak tj na prvi midleware
da nije dispatch response bi morao na reducer tj taj da bude zadnji, bilo koji redosled ovako

prekucaj pravila za reducere
-----------------------
//https://tylermcginnis.com/react-router-pass-props-to-components/
<Route
  path='/dashboard'
  render={(props) => <Dashboard {...props} isAuthed={true} />}
/>
kad hoces da prosledis dodatni svoj prop komponenti koju rutiras koristis render prop mesto component
So to recap, if you need to pass a prop to a component being rendered by React Router, instead of using Routes component prop, 
use its render prop passing it an inline function then pass along the arguments to the element you are creating.
------------------------
// Optimizing Performance
shouldComponentUpdate(nextProps, nextState) vraca true ili false da li komponenta treba da se rerenderuje
React.PureComponent radi shallow comparisson in  shouldComponentUpdate, a Component vraca true by default
-------------------
// hoc
higher order komponent nije komponenta nego obicna funkcija koja vraca funkcionalnu ili class komponentu i prosledjuje joj props
funkcija koja prima komponentu kao argument
--------------------
// react material dashboard
u komponentama, Paper npr, samo mapira bool propertije na klase
classNames mala custom biblioteka za vise klasa na react elementu, moze i {klasa: bool}, pros
import withWidth from '@material-ui/core/withWidth'; da uhvatis sirinu lg md kao props.width
NavLink je da bi imao activeClassName={classes.activeListItem}
'& $listItemIcon': je klasa u tom istom fajlu   listItemIcon: { marginRight: 0 }
---
this.setState(newState, callback) verzija, nema je u docs
this.setState({
    name:'value' 
},() => {
    console.log(this.state.name);
});
When you want call a function after the state changed you can use the method
---------
const renderTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
      <Typography className={custom.errorClass} variant="body2">// u custom su svi ostali prosledjeni propertiji, logican
      errorClass je moj property
--------------
action creators rade posao
ti ih pravis i pozivas iz komponente, prema tome argumenti koji ti trebaju
reduceri mecu u state samo

u Header componentWillMount saljes akciju koja pogleda cookie i localstorage, 2 akcije

-----
za facebook mora ceo redirect url da se posalje, za gogle relativni
u ruti relativni
module.exports = {
  googleCallbackURL: "/auth/google/callback",
  facebookCallbackURL: "https://react-material-passport.herokuapp.com/auth/facebook/callback",
  successRedirectURL: "https://react-material-passport.herokuapp.com/"
----
higher order komponent - prima komponentu kao arg ili vraca, i to je to, isto kao fje u javascriptu
bind(this, arg1, arg2...) logican, fja(arg1, arg2...)
thats the point of PureComponent; if the props and state are shallowly equal to their previous values, the component will decline to update
--------
render prop share code between components trough prop whose value is a function
komponenta ne renderuje nista nego poziva sa svojim argumentima this.props.render() prop fju koju mu prosledjuju
//ima state i fju koje deli
class Counter extends Component {
...
  render() {
    return (
      <div>
        {this.props.render(this.state.count, this.incrementCount)}
      </div>
    )
}

//poziv
<Counter render={(count, incrementCount) =>
	<ClickCounterTwo count={count}	incrementCount={incrementCount} />
/>
 
//renderuje props koji su mu prosledjeni
class ClickCounterTwo extends Component {
  render() {
    const { count, incrementCount } = this.props;
		return <button onClick={incrementCount}>{this.props.name } Clicked {count} times</button>
	}
}
----------
//children varijanta
<Counter>
{(count, incrementCount) =>
	<ClickCounterTwo count={count}	incrementCount={incrementCount} />
<Counter/>

class Counter extends Component {
...
  render() {
    return (
      <div>
        {this.props.children(this.state.count, this.incrementCount)}
      </div>
    )
}
cim se prosleduje definicija fje kao prop ili children to je render prop
stvarni argumenti su u klasi, u tagu je cela definicija fje argument
-----
prosledjena fja koja se poziva u render()
svrha nasledjivanje
<laravel7> whats the difference between render prop and simple passed function as prop?
<ljharb> laravel7: nothing but expectations
<ljharb> laravel7: a render prop is expected to be called, with arguments, during the render path, and the result rendered
<ljharb> a passed function could be called, or not, at any time, and might have no arguments
<ljharb> but a render prop with no arguments makes no sense, and a render prop thats not called during render makes no sense
<laravel7> functions are also called inside render() function
<ljharb> laravel7: they might be, but they might not be
<ljharb> laravel7: like event handlers
<laravel7> render prop is always called inside render()
-------------------------
//geopins
Pin.find({}).populate("author") ili  Pin.populate(newPin, "author"); je novi query koji popunjava referencu na osnovu id u modelu, pros
context u resolverima je context iz definicije appolo servera, curentUser
useState() je lokalni state, useContext() globalni, useReducer(initialState, reducer) samo u index.js
--------------------------
//react native
fora ...props da nasledis native komponentu
[styles.style, props.style] da pregazis stilove i primenis oba, to iza ...props, redosled vazan

----------------------------
//testing
static renderer - html string, shalow renderer samo ta komponenta, deep renderer - mount, kad ti trebaju eventi, mora unmount
describe blok - za testove koji imaju zajednicki beforeEach(), beforeEach() mogu biti ugnjezdeni
testiranje reducera, testiranje pure fje za ulaz izlaz, sve staze u switch
testiranje action creatora, da ima akciju i ima payload
---------------
//event loop
//https://www.youtube.com/watch?v=8aGhZQkoFbQ
spore stvari u que umesto na stack jer izmedju que poziva fja moze da se izvrsava render pa je responsivno
dont block the loop
node c++ api umesto web api
stack, webapi, que, event loop, javascript v8 je samo stack i heap, ostalo je browser i multithreaded je
dom api - clicks, set timeout timer sheduler, xhr mreza
-------------
//abramov talk hooks
custom hooks su fje ispod komponente koje pocinju sa useNesto imaju pristup ugradjenim hookovima
da reuse logiku izmedju 2 fje izvuces u trecu fju - hook koji koristis bilo gde
return callback u useEffect je componentWillUnmount mesto za ciscenje
useEffect i lifecycle methods su mesto za sideeffects
-----------------
//create-react-app update
Unless you need it to have a consistent version between projects, Id suggest maybe just ditching 
the global install and using `npx create-react-app` all the time. But you could update like `npm i -g create-react-app@latest`
--------------
destructuring u argumentima funkcije prakticno imenovani argumenti
--------------
BEM naming convention za CSS klase, pros dobar
block element modifier, blok samostalan, elementi cine blok, modifier je flag na bloku ili elementu, disabled, hidden etc
.block__elem--mod, jedan - je space u kompleksnim recima

<form class="form form--theme-xmas form--simple">
  <input class="form__input" type="text" />
  <input
    class="form__submit form__submit--disabled" type="submit" />
</form>

.form { }
.form--theme-xmas { }
.form--simple { }
.form__input { }
.form__submit { }
.form__submit--disabled { }
-------------------
// saga effects, tj operatori
// https://medium.com/@sankalp.lakhina91/effects-in-redux-saga-cheat-sheet-271f9d0bea75
Redux saga expose several methods called Effects, we are going to define several of them:

Fork performs a non-blocking operation on the function1 passed.
Take pauses until action received.
Race runs effects simultaneously, then cancels them all once one finishes.
Call runs a function. If it returns a promise, pauses the saga until the promise is resolved.
Put dispatches an action.
Select Runs a selector function1 to get data from the state
takeLatest means we are going to execute the operations, then return only the results of the last one call. If we trigger several cases, it�s going to ignore all of them except the last one.
takeEvery will return results for all the calls triggered.
--------------
// https://medium.com/trabe/react-useref-hook-b6c9d39e2022
let promenlj = useRef(initVal) daje promenljivu koja prezivljava kao this.promenlj u class komponenti, ili za dom referencu
samo let promenlj u funkcionalnoj komponenti ne prezivljava rerender
postavlja se sa promenljiva.current = nesto u useEffect()
--------------
setPromenlj hook nije odmah reflektovan
moras u useEffect nad njim plus provera sa if da li je postavljen, kao ucomponentDidUpdate
entry point napravis sa useEffect [] kao componentDidMount pa na dalje useEffect nad stateom koji si promenio plus provera
zamrseno ide lancano, nije toliko tesko
---------
sa useReducer i Context samo imas globalno stanje, dispatch i reducer, api pozivi su po komponentama
----------------
if i use useReducer with Context i just have reducer, dispatch and global state, where do i fetch data? in the components? where do action creators from thunk go then?
laravel7, in the components or in functions defined where you define the context
laravel7, you can pass functions that mimic bound action creators through context, but that might be doing too much in one place
is that proper alternative to sideeffects with thunk/saga?
laravel7, don't try to replace redux with hooks/context
laravel7, if you're not using a library for state management, then do things how it makes sense for an individual feature
rather, don't try to reimplement redux with hooks/context
whats the use case then for useReducer and whats for redux?
laravel7, useReducer is mostly for local state; it's just a different way to express the state from useState
laravel7, const [count, increment] = useReducer((state) => state + 1, 0);
i thought dispatch is meant for dispatching from another components 
laravel7, it can be used that way, but I don't know that that's any more typical than local usage
it's probably most commonly used for custom hooks
if useReducer is for local state what is the use case for useReducer and what for useState?
laravel7, it just depends on how you want to express it / where you want to place the logic
laravel7, it's mostly a style choice
is it common to use redux with just functional components and hooks?
laravel7, yeah
in case of useState the logic is in local functions which are called in handlers and ueEffect?
laravel7, yes, though you can pass either or both halves of the useState to children when it makes sense
graphql state is managed by apolo
---------------------
export const addProductToCart = product => (dispatch, getState) => {
  ...
  //FORA MORA DISPATCH A NE RETURN
  dispatch({
    type: Types.ADD_PRODUCT_TO_CART,
    payload,
  });
};
---------------------
//generic catch handler
const errorHandler = (successfn, errorAction, dispatch) => {
  return async (...args) => {
    try {
      await successfn(...args);
    } catch (error) {
      if (error.message) {
        dispatch(errorAction(args[1], error.message));
      }
    }
  };
};
...args spread i rest, args su polje posle, arg[1] je arg2
-----------
http://dribbble.com
http://behance.net/
https://www.uplabs.com/
https://codepen.io/
----------------
//profiler za brzinu sajtova, chrome extension
Use lighthouse chrome extension to understand where the problem is, its also gives relevant solution references.
----------------
useReducer ide za zavisna stanja, kompozitna, a useState za atomicna, nezavisna medjusobno
u jednom event handleru setujes vise stanja, umesto da meces objekt u useState
https://www.youtube.com/watch?v=NnwkRvElx9E
----------------
//useContext
u createContext(initial value) ide initial value kao u useState()
a u useContext(context) ide taj context koji je importovan iz context = createContext()
i izaziva rerender na svoju promenu kao prop
ne moze da se postavi, samo da se cita, za postavljanje redux
koristi se uvek kad ti treba prop koji ide do listova
------------
//https://www.youtube.com/watch?v=e4t5gbmVfHo
<Waypoint /> komponenta za infinite scroll
kaze ti index koji je na donjoj ivici
poziva onLoad svoju metodu koja se okida kad izadje iz polja
@connection direktiva za paginaciju
messages(order: "DESC") @connection(key: "MessagesConnection") {
key stavlja ceo cache pod taj kljuc i ignorise promenljive
ne moras promenljive da specificiras u read cache i write cache, samo filter za delete npr
a mozes i neku od promenljivih iza da stavis, onda je i ona kljuc u cache filter: ["promenljiva"]
ali moras i nju onda u read i write da specificiras
//https://www.apollographql.com/docs/react/data/pagination/#the-connection-directive
store mu je cache
---------------
//apollo testiranje
// https://www.apollographql.com/docs/react/development-testing/testing/
beza apollo providera testovi padaju, query i mutation hookovi ne rade jer apollo client nije u contextu
moze da se umota u apollo provider ali onda radi sa stvarnim bekendom, nepredvidivo
mocked provider ima request i result, result moze da bude i fja pa da logujes i postavljas varijable
mocked provider vraca promise
-----
//testing queries
testing loading state
it('should render loading state initially'
<MockedProvider mocks={[]}>
i nema await wait();
----
testing final state (da je dovuko podatke)
<MockedProvider mocks={[dogMock]} />
await wait(0); // wait for response
-----
testing error state isto kao final samo error key u mock
error: new Error('aw shucks'),
------
//testing mutations
isto kao query samo imas klik event na dugme koji okida mutaciju
button.props.onClick(); // fires the mutation
----
bez await wait() imas loading
----
sa await wait(0); final state
button.props.onClick(); // fires the mutation
----
postavljas promenljivu pa je menjas u result fji mutacije, pa asertujes da li je promenjna
tako vidis da li je pozvana mutacija
  let deleteMutationCalled = false;
  const mocks = [
    {
      request: {
        query: DELETE_DOG_MUTATION,
        variables: { name: 'Buck' },
      },
      result: () => {
        deleteMutationCalled = true; // ovde
        return { data: { deleteDog } };
      },
    },
  ];
   await wait(0); // cekaj
  expect(deleteMutationCalled).toBe(true); //asert
 -------
 u testovima je kopirao primere iz dokumentacije
 ------------------
 ------------------
 Apollo local state management
 //https://www.apollographql.com/docs/react/data/local-state/
 
 //direct writes to cache, bez resolvera
 onClick={() => client.writeData({ data: { visibilityFilter: filter } })}
 ------
 subskripcija
 const GET_VISIBILITY_FILTER = gql`
  {
    visibilityFilter @client
  }
`;
onClick={() => client.writeData({ data: { visibilityFilter: filter } })}
active={data.visibilityFilter === filter}
-------
//local resolver
updateujes cache kroz mutaciju
--------
//queryng, 
identican kao server polja samo imaju @client
--------
//init cache
const data = {
  todos: [],
  visibilityFilter: 'SHOW_ALL',
  networkStatus: {
    __typename: 'NetworkStatus',
    isConnected: false,
  },
};
cache.writeData({ data });
client.onResetStore(() => cache.writeData({ data }));
-------
queryies
resolveri za polja bez kljucne reci Query
logika u njima
moze pomesano local i remote polja
moze async
--------
//fetch policies
@client(always: true), vadi iz resolvera, ignorisi cache, i daljeima cache u contextu resolvera
cache, local resolver, network
--------
//export variables
@client fields exported as variables
const query = gql`
  query currentAuthorPostCount($authorId: Int!) {
    currentAuthorId @client @export(as: "authorId")
    postCount(authorId: $authorId)
  }
`;
-------
//managing the cache
cache.writeData
---
cache.readQuery
cache.writeQuery
---
cache.readFragment
cache.writeFragment
----
cache normalizovan, ima id-jeve i type nesto
cache.writeData({ id: `User:${id}`, data });

za writeData mora id=__typename+id string, za writeQuery samo __typename
za read write fragment isto mora id
u contextu ima helper fja za sastavljanje string id-a
const id = getCacheKey({ __typename: 'TodoItem', id: variables.id })

razlika writeData i writeQuery, writeQuery ima query i radi validaciju, writeData generise query automatski

The only difference between the two is that cache.writeQuery requires that you pass in a query to 
validate that the shape of the data youre writing to the cache is the same as the shape of the data 
required by the query. Under the hood, cache.writeData automatically constructs a query from the data
object you pass in and calls cache.writeQuery
------------
//client-side schema
samo za graphiql docs
----------
//Code splitting
-----------------------
//https://www.robinwieruch.de/react-apollo-link-state-tutorial
sturo, client resolver i toliko
read data, identicno kao za remote resolvers
write data primer, ne pominje id, __typename
-------------------------
-------------------------
// react jest and enzyme testing
//https://www.robinwieruch.de/react-testing-jest-enzyme

// async test
  it('fetches async data', done => {
    const promise = new Promise(...); // napravi promise resolve za mock
    axios.get = jest.fn(() => promise); // mockuje get fje axiosa promiseom
    const wrapper = mount(<App />); // renderuje komponentu
    expect(wrapper.find('li').length).toEqual(0); // asertuje pre podataka
    promise.then(() => {
      setImmediate(() => { // ceka ostale asinhrone fje, gurne ovu na kraj event loopa
        wrapper.update(); // prerenderuje komponentu
        expect(wrapper.find('li').length).toEqual(2); // asertuje posle podataka
        axios.get.mockClear(); // oslobodi get fju axiosa
        done(); // ovo je u then bloku, kaze jestu da ceka promise...?
      });
    });
  });

We need to tell our React component to render again. Fortunately, Enzyme comes with a re-rendering API // wrapper.update();
In addition, we need to wait for all asynchronous events to be executed before updating our React component and making test assertions. 
Thats where the built-in JavaScript function setImmediate comes in, because its callback function gets executed in the next iteration of the event loop.
setImmediate je setTimeout 0, gura tvoj codeblock, tj callback fju na kraj event loop iza postojecih asinhronih fja

//done()
Otherwise, the test will run synchronously and wouldnt wait for the promise to be resolved. Hence, a test cases callback function comes with the 
handy done callback function that can be used to signalize Jest about a finished test explicitly.
--------------------
// https://www.robinwieruch.de/react-testing-jest
//setup
sve su dev dependecies
npm test i watch skripte
jest.config.json 1 linija "testRegex": "((\\.|/*.)(spec))\\.js?$"
-------
//snapshot
jest snapshot test ako ima expect(tree).toMatchSnapshot(); asert u it() tj test()
const component = renderer.create(<Counter counter={1} />); renderuje komponentu, react test renderer npr
let tree = component.toJSON(); //pretvori u json
expect(tree).toMatchSnapshot(); //poredi sa prethodnim snapshotom
pojave se 2 dodatna prompta
 › Press u to update failing snapshots.
 › Press i to update failing snapshots interactively.
-------
//unit integration tests
nije nista rekao o tome, samo reducer
testiranje reducera - testiranje obicne fje, sve staze u switch po 1 test
------------------------
//https://www.robinwieruch.de/react-testing-mocha-chai-enzyme-sinon
// How to test React with Mocha, Chai & Enzyme
counter aplikacija od 2 komponente
-------
//mocha - test runner za React, karma za Angular
First, there needs to be an entity which is responsible to run all of our tests in a certain framework. 
This entity will be Mocha which is a popular test runner in React applications
//chai - assertion library, expect
entity which can be used to make assertions. Someone has to able to say: "Expect X to be equal to Y"
//jsdom - minimalni browser
Since the tests are not executed in a real browser, 
you need to setup the minimal environment for the component tests yourself
---------
//test/helpers.js
import { expect } from 'chai';
global.expect = expect; // eto kako expect svuda bez import
//test/dom.js
...
global.window = window;
global.document = window.document;
global.navigator = {
  userAgent: 'node.js',
};
------
//scripts
"test:unit": "mocha --require @babel/register --require ./test/helpers.js --require ./test/dom.js 'src/**/*.spec.js'"
zapazi --require kao u nodejs require file ili import library
//watch script 
"test:unit:watch": "npm run test:unit -- --watch"
----------
//unit tests
testing pyramid: unit tests, integration, end to end, broj testova piramida
//testiranje fja
// arrange, act, assert
it('should increment the counter in state', () => {
  const state = { counter: 0 }; //arrange
  const newState = doIncrement(state);//act
  expect(newState.counter).to.equal(1);//assert
});
-------
//enzyme
//test/helpers.js
global.mount = mount;
global.render = render;
global.shallow = shallow;
------
//state and props
it('passes all props to Counter wrapper', () => {
 const wrapper = shallow(<App />);
 let counterWrapper = wrapper.find(Counter);
 expect(counterWrapper.props().counter).to.equal(0);// access props and assert them
 wrapper.setState({ counter: -1 }); //setState na wrapper
 counterWrapper = wrapper.find(Counter);
 expect(counterWrapper.props().counter).to.equal(-1);
});
//clicks
  it('increments the counter', () => {
    const wrapper = shallow(<App />);
    wrapper.setState({ counter: 0 });
    wrapper.find('button').at(0).simulate('click');
    expect(wrapper.state().counter).to.equal(1);
  });
 ------------
 //shallow
jedna komponenta bez dece
unit tests, lightweight integration
//mount
cela hijerarhija
real integration tests
// render
if you need access to child components but are not interested in lifecycle methods, 
you can use render() instead of mount()
-----------
testing pyramid says that you should have lots of unit tests
and several integration tests (and only a few end-to-end tests)
-----------------
//React Testing with Sinon: Testing Asynchronous Logic
global.sinon = sinon;
Sinon can be used for spies, stubs, and mocks
  it('calls componentDidMount', () => {
    sinon.spy(App.prototype, 'componentDidMount'); //spy je overrideovana mockovana fja, na prototype, samo calledOnce
    const wrapper = mount(<App />);
    expect(App.prototype.componentDidMount.calledOnce).to.equal(true);
  });
--------
describe('App Component', () => {
  const result = [3, 5, 9];
  const promise = Promise.resolve(result);
  before(() => { //pripremi u before each
    sinon.stub(axios, 'get').withArgs('http://mydomain/counter').returns(promise);//stub je spy fja kojoj je postavljeno sta vraca
  });
  after(() => {
    axios.get.restore(); //pocisti u after each
  });
  ...
  it('fetches async counters', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.state().asyncCounters).to.equal(null);//assert pre podataka
    promise.then(() => {
      expect(wrapper.state().asyncCounters).to.equal(result); //assert posle podataka
    });
  });
});
test nije asinhron nego testira asinhrono ponasanje
-------------
apollo cache update gledas da li je referenca i pre i posle upisa ISTA
logujes iznad i ispod, ako je promenjena i iznad cache ne updateuje
i mora sva polja da vrati, ne sme warnings da ima
----------------------
react router fora
//https://stackoverflow.com/questions/45122800/react-router-switch-behavior
ruter moze da vrati vise od jedne rute u isto vreme
sa <Switch> vrati samo prvi match, redosled vazan
sa exact ne vrati podrutu, npr /something umesto /somethingelse
i redosled ruta je vazan, velika fora
----------------------
apollo query za interval refresh ide pollInterval, a za klikove ide refetch
----------------------
mongo seed, asinhrono, Promise.all() mora da znas sta se desava pre, a sta posle, sa console log
velika fora
npr
  await Promise.all(
    usersPromises.map(async user => {
      await Promise.all([
        user.avatar1.save(),
        user.cover1.save(),
        user.user1.save(),
      ]);
    }),
  );
  const users = await models.User.find();
------
mongodb aggregation, dinamicki dodajes polja i radis upite i sortiranja nad njima
$match je find filter u aggregate [{}]
ceo dan izgubio na 
1. new Date() mora za datum u aggregaciji umesto stringa koji je isao u find()
2. pozicija $match, tj redosled operatora, mora posle $addfields a pre $sort, jer tek tu postoji newCreatedAt field
mongo izmnozis podatke, napravis strukturu pa onda to filtriras, princip rada
-------------
state u reactu je asinhron, mozes samo u useEffect da proveravas vrednosti, u hendlerima su vrednosti netacne
---------------------
eto kako radi override sa clesses property u material ui, pros, dobar, da ne moras css fajlove sa klasama
vidis koji je rule name u CSS sekciji dokumentacije, indicator npr https://material-ui.com/api/tabs/
onda <Tabs classes={{indicator: classes.tvojaKlasa}} https://material-ui.com/customization/components/#overriding-styles-with-classes
znaci 3 nacina, classes prop, mui theme overrides, i css fajl i klasa
root klasa je klasa na root elementu na komponenti, kada se koristi className overriduje root klasu
unutrasnji elementi sa classes
----------
match prop je dostupan u stranicama i navigationu jer je u <Route render={matchProps => prosledjen komponentama ili layaoutu
-----------------
console loguj da vidis redosled izvrsavanja, od index.js, App.js do handlera, hoc za auth, ws link tokeni, 
localstorage gde se setuje, cita, itd...
-------------------
-------------------
// redux saga
// saga effects, tj operatori
// https://medium.com/@sankalp.lakhina91/effects-in-redux-saga-cheat-sheet-271f9d0bea75
// ponovljeno da bi bilo sve o sagama na jednom mestu
Redux saga expose several methods called Effects, we are going to define several of them:

Fork performs a non-blocking operation on the function passed.
Take pauses until action received.
Race runs effects simultaneously, then cancels them all once one finishes.
Call runs a function. If it returns a promise, pauses the saga until the promise is resolved.
Put dispatches an action.
Select Runs a selector function to get data from the state
takeLatest means we are going to execute the operations, then return only the results of the last one call. If we trigger several cases, it�s going to ignore all of them except the last one.
takeEvery will return results for all the calls triggered.
----------------------
//https://www.youtube.com/watch?v=eUMbH6X_Adc
saga je middleware koji watchuje svaku redux akciju i dispatchuje novu
takeEvery je watch, takeEvery(ACTION, callback)//ova akcija nema reducer
put je dispatch({type: NEW_ACTION, value: 1}) //obavezno nova akcija ili loop
-----------
blocking effects
call, take, race, canceled
non blocking effects
put, fork, spawn, cancel, actionchannel
takeEvery, takeLatest su helperi, prvi concurently, drugi jedan
-----
generator je kao petlja, tj iterator
posle poziva vraca objekat koji ima next() fju koja vraca {value, done} // to to
-----
take(akcija) samo pauzira (ceka) dok se ta akcija ne pozove, kad se dispatchuje ta akcija nastavlja
vraca action object sa type i value
-----------
//bindActionCreators
The only use case for bindActionCreators is when you want to pass some action creators down to a component that isn't aware of Redux, and you don't want to pass dispatch or the Redux store to it.
------------
u generatoru moze da postoji yield i return, yield nastavlja od sledece linije, return definitivno izlazi i postavlja done na true
yield ide obicno ispred poziva fje koja vraca promise, slicno kao await, saga resolvuje promise
---------
effecti su helper funkcije u sagi
thread management: call, fork, spawn, apply, cancel
action creation: put
data seeding: select
flow control: take, takeEvery, takeLatest (cekaju ili forkuju)
----
take u istoj niti, ne kreira novu
vraca akciju koja je prosledjena u dispatch
dispatch({type: "SET_STATE", value; 42})
take vraca {type: "SET_STATE", value: 42}
---
put je isto sto i dispatch samo iz sage, mozes da saljes argument uz akciju u take druge sage, put(akcija(data))
call je poziv fje, koristi se za testiranje
apply je isto sto i call osim sto prosledjuje this context, apply(context, fja)
---
fork je kao call, zove fju ali u novom procesu i ne vraca vrednost
cancellovanje ili greska parenta prekida svu decu procese, na cancel se poziva finally block forkovane fje, try, catch, finally
moze nove api pozive da vrati van redosleda tj kako server stigne
nije kao graphql nego puno malih poziva sa id za sve//to to
puno malih api poziva i saga hendluje to sve
---
takeEvery slusa kao take i radi fork za svaku novu akciju
---
cancel(process) js singletheaded, ne moze da ga prekine bilo gde nego na sledecoj yield statement
cancelled vraca true ako je cancellovan a nije greska, isCancelled prakticno u try finally
kao arg prima proces koji terminira i svu njegovu decu
---
takeLatest kombinacija fork, takeEvery i cancel
forkuje child process na akciju i drzi samo jedan ziv, canceluje postojece dete pre forkoanja novog deteta
---
spawn isto kao fork samo nov proces nije dete i na gresku ili cancel ga ne prekida
throw error u sagi, proces loguje gresku i nastavlja da radi i posle greske, fork prekida posle greske
---
all prima polje take-ova i vraca polje njihovih vrednosti
ceka ih sve u bilo kom redosledu
yield krace moze da radi i nad poljem ili poljem koje vraca map
---------
//effects summary //to to
Effects create plain objects – Redux Saga interprets them and executes processes
Take, TakeEvery and TakeLatest wait for a specific kind of action to create a new process
Call, Fork and Spawn create different kinds of new processes
Forked processes are cancelled when their parent is cancelled or errors
Take and Call pause the execution of caller process
-------------
ne mozes da razumes sta se radi ako ne znas sta koji effekat radi
-----------------------------
//channels - argument za take
Action Channels - buffer actions to be processed one at the time
Generic Channels - communicate between two sagas
Event Channels - connects to outside event sources
------
Action Channel je argument u take
hvata sve akcije koje bi inace bile izgubljene jer saga ceka async resolve
osigurava da sve bude uhvaceno
take isto ceka
function* updateSaga() {
const chan = yield actionChannel("UPDATE");
	while(true) {
		yield effects.take(chan);
		console.log("Update logged");
		yield delay(1000);
	}
}
-------
Generic Channel ili krace Channel
komunikacija izmedju 2 sage bez action type
function* saga() {
	const chan = yield channel();
	function* handleRequest(chan) {
		while(true) {
			const payload = yield effects.take(chan);
			console.log("Got payload", payload);
			yield delay(1000);
		}
	}
	yield effects.fork(handleRequest, chan);
	
	yield effects.put(chan, {payload: 42});//nema action type
}	
-------
Event Channel
prima evente iz websocketa i konvertuje ih u akcije	koje mogu da se uhvate u yield take(chan) i onda yield put(akcija(payload))// to je to 
-----------
takeLatest([akcije]) bilo koja od akcije, i ako se ponovi otkazuje proces i kreira novi
mala slova action creatori, velika action types
yield isto meces ispred promise kao await, ispred fetch, response.json()
uglavnom ide 
const {user} = yield take(AKCIJA) 
yield fetch(url+user.id) 
yeld put(response.nesto)
-----------------------
-----------------------
//redux loading state za edit i delete requests koji rerenderuju samo deo liste
isti type akcije moze da se hendluje u vise reducera, oblikuje razlicite delove statea, legitimno, nije tesko
koliko loading ti treba toliko ti treba i reducera, najmanje jedan reducer po stranici, najbolje po komponenti
----
u reducer case postavlja SAMO ONO STO TREBA DA PROMENI, nema potrebe sve kljuceve svaki put
poseban reducer maltene za svaki api http da bi imao poseban loading i error, a poseban za svaku stranicu svakako
reducer je u stvari kljuc u stateu, kakav state ti treba takvi reduceri
------------
// hoc objasnjeno, kinez
//Udemy - Complete React Developer in 2019 (w Redux, Hooks, GraphQL)
hoc unutra definise NOVU komponentu i vraca je, komponenta koja je arg je upotrebljena u toj novodefinisanoj, pros
prosledjuje props nove kreirane komponente prosledjenoj arg komponenti
enhanceuje prosledjenu komponentu tako sto je renderuje u novokreiranoj i vraca novokreiranu
props koji spreaduje na prosledjenoj prvobitno potice od prop-a koji je prosledjen kompletnoj komponenti posle poziva
withData, vracenoj iz hoc, a ne prosledjenoj, fora
-----------------
const FancyButton = React.forwardRef((props, ref) => 
(  <button ref={ref} className="FancyButton">    
 {props.children}
  </button>
));
// You can now get a ref directly to the DOM button:
const ref = React.createRef();
<FancyButton ref={ref}>Click me!</FancyButton>;

Regular function_ or class_ components don’t receive the ref argument, and ref is not available in props either.
kad hoces iz parenta da referenciras ref na elementu u child komponenti
-------------------
// lifecycle methods class components
// https://levelup.gitconnected.com/componentdidmakesense-react-lifecycle-explanation-393dcb19e459
// https://www.youtube.com/watch?time_continue=6&v=DyPkojd1fas
posle componentWillUpdate ide render i ne moze nista da se menja
componentDidUpdate je commit faza posle render, i tu moze sideeffects, prakticno isto kao useEffect samo moras rucno da poredis
nema dependencies nego sa !== ili _.isEqual()
... snapshot vraca getSnapshotBeforeUpdate i to je 3. arg u componentDidUpdate
shouldComponentUpdate vraca true ili false na osnovu state i props i sluzi za optimizaciju, ti da odlucis da li rerender ili ne
getDerivedStateFromProps kada state moras da inicijalizujes od propsa
componentWillMount posle konstruktora, za event listenere koji rade u didMount, jbm li ga
useEffect radi shallow watch nad dependencies, za deep custom hook
----
// virtual DOM
// https://programmingwithmosh.com/react/react-virtual-dom-explained/
virtual dom, drzis dve kopije jednog te istog
glavni dom mora da radi re-rasterizaciju za svaku elementa i dece za svaku promenu, zato spor
virtual dom je samo javascript objekt pa se brze menja
i ima diff algoritam pa rerenderuje samo promenjeni cvor


