// https://www.basedash.com/blog/why-we-had-to-move-away-from-react-query
// https://redux.js.org/usage/structuring-reducers/normalizing-state-shape
// normalized data (redux store) - convert nested (and recursive) state into flat structure with ids
problem sa react query su keys koji je samo polje bez tipova 
pa je tesko precizno da se invalidira cache koji query tacno teba
----------------
// testiranje softvera.ppt
// testiranje stanja
1. prelaz - tekuce stanje, tezina grane izm stanje1 -> stanje2, dogadjaj/akcija 
2. stanje
3. dogadjaj
4. akcija - side effect
-------
// ima objasnjenje za test doubles
// depended-on component, DOC
1. dummy obj - prazan
2. stub - samo prosledjuje vrednosti kroz fje
3. spy - assert u glavnom testu
4. mock - assert u samom objektu
5. fake obj - jednostavnija impl bez sideeffects
-------------
// tdd - nije lose, prakticni saveti
red/green phases
redosled testova:
1. edge cases - null, [], undefined - just to satisfy interface
2. happy path - fundamental
3. alternative
4. exceptions and error states - na kraju, garantuju ispravnost, ne i funkcionisanje
--------------------
// slab clanak
// https://en.wikipedia.org/wiki/React_(JavaScript_library)
reconciliation (pomirenje) - poredjenje virtual dom i real dom, i renderovanje samo promenjenog dela, ne cele strane
----
handlebars prerendering je bio static site generation, ipak NE, to je js, ovo je html
----
// SSR
const express = require('express');
const { useMemo, useCallback } = require('react')
const React = require('react');
const renderToString = require('react-dom/server').renderToString; // sve u node.js

const app = express();

app.get('/', (req, res) => {
  const html = renderToString(<MyApp />); // komponenta iz susednog foldera
  res.send(`...${html}...`);  // vrati stranicu, ubaci html ko string
});
-----
unidirectional data flow - flux, redux prica
redux - actions (objects), dispatcher, store
----------------
plan:
next 13
animations, framer motion
profiling
serverless framework, node.js
rename redux-ecommerce to redux-ecommerce and update readme
-------------------
// key prop na custom component
moze <MyComponent key={id} /> key na bilo koju custom componentu
ne treba nista da se prenosi, niti Fragment
---------------------
// react-hook-form
Controller component sa render props sluzi za controlled fields
ima i hook useController
MUI koristi controlled form fields // to, to
--------------
// dodatni useState() resenje za file sa Controler, C:/fakepath/file.txt
// https://github.com/react-hook-form/react-hook-form/discussions/5394
// https://codesandbox.io/s/long-sun-nsfbk?file=/src/App.js

const [file, setFile] = React.useState('')
  
<Controller
  name={name}
  control={control}
  render={({ field }) => (
	<Stack spacing={1}>
	  <label htmlFor={inputId}>
		<input
		  {...field}
		  {...otherProps}
		  type="file"
		  accept=".pdf,.docx,.doc,.rtf,.txt"
		  hidden
		  id={inputId}
		  value={file}
		  onChange={(e) => {	// ovaj onChange je bitan, zapazi
			setFile(e.target.value)
			field.onChange(e.target.files) // ovde
		  }}
		/>
		<Button
		  component="span" // mora component prop da ima da dize file dijalog od input
		  variant="text"
		>
		  {title}
		</Button>
		</label>
		</Stack>
-------------------------------
// react naming hanlders
handle-event-sta
handleClickResetFile
-----------------------
// https://legacy.react-hook-form.com/api/useformstate/errormessage
react-hook-form ima posebnu komponentu za error message, jer je slozen tip za error
koja se posebno instalira npm install @hookform/error-message
----------------------------------
useFormContext moze da se koristi bez FormProvider
tad koristi default values iz createContext(defaultValue), a puca samo ako KORISTI nedefinisane vrednosti // dobra fora
vazi za svaki provider, ne mora uvek da se mota
// formContext is null when FormProvider is not accessible in React tree
const formContext = useFormContext();
const { control } = formProps || formContext || {}; // eto
------------------------------
yup validator return true je valid
-------
// fora za yup validatore
// convert '' to undefined
.transform((value) => (value === '' ? undefined : value)) // to
.nullable()
.notRequired()
------------------------------
// dobra fora da se kreira samo jednom, a ne na svaki render, stavi u initial state
const [queryClient] = React.useState(() => new QueryClient())
------------------------------
// Preventing React re-renders with composition - Developer way
// https://www.youtube.com/watch?v=7sgBhmLjVws
// 1. prevent rerender with composition - moze za state onClick, modal primer
izdvoj states u posebnu komponentu pa onda ona i spore komponente siblings u parent
izbegnut rerender sporih zbog state change
<>
	<ComponentWithStates />
	<SlowComponent1 />
	<SlowComponent2 />
</>
-----------
// 2. prevent rerender with children prop
wrapping div onScroll ne moze u posebnu komponentu za composition
children je samo prop, <Component children={<SlowComponent1 />} />, nije child komponenta // nije lose
props nisu affected with state change // dobar
// ComponentWithScroll
<div onScroll={(currentTarget) => setState(currentTarget.scrollTop)} >
	{children} // unaffected
</div>
// parent component
<ComponentWithScroll >
	<SlowComponent1 /> // eto children
</ComponentWithScroll>
----------------
// 3. components as props
ista prica samo bilo koji prop, a ne nuzno children
props nisu affected with state change // opet ista fora
-----
Layout ima state-ove, a spore componente su prosledjene gde treba
<Layout
	sidebar= {<SlowComponent1 />}
	content= {<SlowComponent2 />}
/>
----------------
// poenta
isolate state changes, da nisu u istoj komponenti state i spore componente
-------------------
// Why React components re-render - Developer way
// https://www.youtube.com/watch?v=ARWX1XdghLk
razlozi za rerender:
1. state changes
2. parent component rerenders children
3. context updates - usteda sto ne rerender komponente na putu do useContext()
-------
// netacan lazni razlog, myth
props change ne rererenderuje komponentu // eto
props nisu bitni jer se vec parent rerenderuje
props change bitan samo za React.memo i useMemo
------------------
//  Preventing re-renders with React.memo - Developer way
// https://www.youtube.com/watch?v=feEY3Qajrwg
// memo() sprecava rerender childa
const ChildMemo = React.memo(Child);

const Parent = () => {
	return <ChildMemo />
}
-----
// props
poredi currentProps i prevProps, ako nema razlike, nema rerender
-----
za string RadioNodeList, ali za objekt ne jer novi ref na svaki poziv fje
pa je novi objekat, i React.memo() nema nikakvog efekta, uvek rerender
<ChildMemo someProp={{value: 'text'}}/>
----
referential equality for props comparing
// str nije ref type
const a = 'str'
const b = 'str'
a === b // true
---
// object je ref type, python price reference i value equality
// arrays, objects, functions
const a = {value: 'str'}
const b = {value: 'str'}
a === b // false, refs
-----------
// props memoisation
useMemo, useCallback
preserve object reference na rerender
---
const ChildMemo = React.memo(Child);

const Parent = () => {

	// cuva props od menjanja ref i rerender
	const valueMemo = useMemo(() => {
		return {value: 'text'} // od ovog objekta preserver reference, obj koji je vracen
	},[]) // dependencies, sta je unutra

	return <ChildMemo someProp={valueMemo} />
}
-----
react je call stack fja, tj stablo, pozivaju se stablo fja (komponenata)
------
// fja je isto ref
const Parent = () => {
	// rekreira se novi ref na svaki rerender
	const onChange = () => {
		//
	};
	return <ChildMemo onChange={onChange} />
}
// useCallback
const Parent = () => {
	// cuva ref od fje od menjanja
	const onChangeMemo = useCallback(() => {
		//
	},[])
	return <ChildMemo onChange={onChangeMemo} />
}
--------------
every non primitive prop should be memoized // lose u stvari
-------
pure component rerenders samo ako se promenili props
class PureMycomponent extends PureComponent {
	render() {
		//
	}
}
// PureComponent i memo() su identicna stvar
const MyComponent = () => {
	//
})
const MyComponentMemo = React.memo(MyComponent);
----------------
// shouldComponentUpdate
class PureMycomponent extends PureComponent {

	// ova fja finije sprecava rerender
	shouldComponentUpdate(nextProps) {
		// return false da prevent rerender
		return nextProps.bla !== this.props.bla // ovde !==
	}
}
-----
// custom comparison function, ista stvar za React.memo()
const arePropsEqual = (oldProps, newProps) => {
	// obrnuto, rerenders na false
	// return true da sprecis rerender
	return oldProps.bla === newProps.bla // tacno obrnuto, ovde ===, gledaj ime fje
})
const MyComponentMemo = React.memo(MyComponent, arePropsEqual);
-----------
// zakljucak, kad React.memo()
samo kao last resort, kad imas performace problem
-------------------------
// kako react i html handluje new lines (line breaks) (\n i <br />) u textu
----
// to
dangerouslySetInnerHTML treba kad je string '<p></p>'
kad je JSX onda ne treba, bilo fja bilo komponenta return <p/>{string}<p/>
------------------
// key i ref specijalni props moze spread object
radi, probao za key
key je string ili number
// chatGPT
const myProps = { key: 123, prop1: 'value1', prop2: 'value2' };
return <MyComponent {...myProps} />;
------------------
// animacije
// https://youtu.be/u72H_zZzkcw?t=5834
Motion UI, Framer, Three.js, gsap
----------------------------
// useCallback for re-renders: remove most of them - Developer way
// https://www.youtube.com/watch?v=G7RNVYaRS3E
React.memo() je beskoristan ako se makar jedan prop menja
children je obican prop, syntax sugar
moras da memoizujes vrednost, a ne definiciju komponente, Reactx,createElement() ispod JSX
lepo i lako objasnjeno
useMemo, useCallback i React.memo() se skoro i ne koristi, ljudi ih mecu bezveze
--------------------
// https://www.youtube.com/watch?v=76OedwmXlYY
// The mystery of React key: how to write performant lists - Developer way
// https://codesandbox.io/s/static-list-with-and-without-memo-clbl24 - with memo, static array
// https://codesandbox.io/s/dynamic-list-with-index-and-id-6y5gsw - with local state
key sluze kao ids da react identify components
----------
// <Item key={uuid()} /> - ne valja, nikad ne koristi
promenis samo key na postojecu komponentu - unmount, create new component, mount, 
mount kosta vise nego rerender, lose state, lose focus
----------
key must be stable between rerenders and unique
---------
React.memo() kesira ako su isti props, ref objekti props tricky, invalidate memoization
------------
// array index as key
ako dodas na pocetak polja, react misli da se props promenili i rerender sto ne treba, memo ne radi
state ide na pogresan ClipboardItem, focus npr
array index moze ako:
1. array static - no add, remove, reorder
2. no local state, no React.memo()
------------
osim za liste key moze da se koristi i za remounting, tj reset state, kad promenis key
-------------------------
// Refs in React: from access to DOM to imperative API - Developer way
// https://www.youtube.com/watch?v=H9KhRiO1UeU
// sve vec znao, nista novo
ref je reserved prop name
inputRef radi kao obican prop jer ref je obican mutable object
-------------
u classs component ref prop je vezan za instancu
-------------
u functional component rec ref mora sa forwardRef((props, ref) => {...})
ref je drugi argument
-------------
// useImperativeHandle dodeljuje objekat u ref.current, 3 args
useImperativeHandle(
    apiRef, // mora ref
    () => ({ // ova fja vraca object koji se dodeljuje u ref.current
      focus: () => {
        inputRef.current?.focus();
      },
      shake: () => {
        setShouldShake(true);
      }
}),	[]);
// poziv:
inputApiRef.current?.shake();
-------------------
// samo ovo nisam znao do sad
// meze direktna dodela u useEffect ref.current, ekvivalentno sa useImperativeHandle
useEffect(() => {
    if (apiRef) {
      apiRef.current = {
        focus: () => {
          inputRef.current?.focus();
        },
        shake: () => {
          setShouldShake(true);
        }
      };
    }
  }, [apiRef]);
------------------------------------
// React composition patterns: passing components as props - Developer way
// https://www.youtube.com/watch?v=D7DC9pEaLFo
components composition - passing components as props
----
komponenta se renderuje kad je return, ne u vreme definicije _const myVar = <Component />, def je samo object
------
const dialogHeader = <h1>Hello!</h1>;
const dialogContent = <div>Content</div>;
const dialogFooter =  <div>Footer</div>;

return (
{isDialogOpen && (
	<Dialog
	// ovo je kompozicija
	header={dialogHeader}
	footer={dialogFooter}
	content={dialogContent}
	/>
)}
)
------
// ovo je fora za default props, ako je text veci povecaj i ikonu
const iconHasSize = !!icon.props.fontSize // condition mora da bi bio moguc override
const updatedIcon = !iconHasSize
? React.cloneElement(icon, {
	fontSize: appearance === "large" ? "large" : "small"
  })
: icon; // cudno malo
-------------------------------
// https://github.com/immerjs/immer
immer.js da izbegnes kloniranje sa spread, helper lib, treba da se koristi
-------------------------------
// Say no to "flickering" UI: useLayoutEffect, painting and browsers story
// https://www.youtube.com/watch?v=__tm1dyMi4A
useLayoutEffect je synchronous, blokira browser repaint, degradira performanse, sve u 1 repaint
koristi se za dom i refs, da se izbegne flickering
60fps = 13ms, browser u jedan task izbaci batch iz 13ms i 1 repaint
osim ako je async, onda za svaki async 1 repaint
-------------
SSR - dome elementu ne postoje, samo string, niti useEffect i useLayoutEffect ne postoje tamo
opet flicker, vrati server ui, ReducedExperience
----------------------------------
// sort imports with prettier plugin
primer:
// https://github.com/shadcn/taxonomy/blob/main/prettier.config.js
// plugins: ["@ianvs/prettier-plugin-sort-imports"],
-------
ovo je plugin:
// https://github.com/IanVS/prettier-plugin-sort-imports#importorderparserplugins
radi bez problema, samo dodao paket i iskopirao config // to, to
importOrder: [
    "^(react/(.*)$)|^(react$)",
    "^(next/(.*)$)|^(next$)",
    "<THIRD_PARTY_MODULES>",
    "",
    "^types$",
    "^@/types/(.*)$",
    "^@/config/(.*)$",
    "^@/lib/(.*)$",
    "^@/hooks/(.*)$",
    "^@/components/ui/(.*)$",
    "^@/components/(.*)$",
    "^@/styles/(.*)$",
    "^@/app/(.*)$",
    "",
    "^[./]",
  ],
-------------------------
// format prisma.schema
ova extenzija ne radi on save
https://marketplace.visualstudio.com/items?itemName=Prisma.prisma
------
ovaj prettier plugin radi, ukljuci se u polje u prettier.config.js
https://github.com/avocadowastaken/prettier-plugin-prisma
plugins: ["@ianvs/prettier-plugin-sort-imports", "prettier-plugin-prisma"],
--------------------------
// React composition patterns: render props - Developer way
// https://www.youtube.com/watch?v=njYc-boHHZc
// glavna fora sa fjama generalno
poenta - kad prosledis funkciju umesto vrednosti mozes vrednosti da primenis u kasnijem trenutku 
render props - children prop funkcija koja se zove u render i vraca neki jsx
ima detalja...
-------------------------
// tipovi za onClick handler
<a href="#" onClick={(event) => handleShowCustomNumberInput(event)} />
const handleShowCustomNumberInput = (event: MouseEvent<HTMLAnchorElement>) => {
    event.preventDefault();
};
// MORA MouseEvent import iz React da bude <generic>, globalni nije
//https://stackoverflow.com/questions/60471034/type-mouseeventelement-mouseevent-is-not-assignable-to-type-mouseeventhtm
import { FC, MouseEvent, useState } from 'react';
----------------------------
// React reconciliation: how it works and why should we care - Developer way
// https://www.youtube.com/watch?v=724nBX6jGRQ
inicijalni primer, checkbox bindovan na bool state, i render 2 razlicita inputa na taj state
ali react misli da je isti jer su oba Input komponenta (type: Input u virt dom) i nemaju keys
i react uabacio state (text in input) u pogresan input
i ti Input imaju state, text u textbox
oba su type Input, ali razliciti (instance)
znaci keys treba i za conditionals sa istim tipovima, ne samo u map()
------
function Component() {
	const [isCompany, setIsCompany] = useState(false);

	return (
	  {isCompany ?
		<Input id="company" />
		:
		<Input id="person" />
	  })
}
--------------
reconciliation - react diffing algorithm
virtual dom - javascript object, mirror real dom
-------
cela prica je oko virtual dom object
-------
native tag - type: string
components - functions
-------
null - component = mount/unmount
------
react najpre identifikuje elem po type:
u arrays (children) identifikuje elemente po redosledu
------
kad pomesa koji je koji elem state ode u pogresan item
------
key ih identifikuje bolje nego redosled (index)
------
rerender nije isto sto i remount, naravno
remount - create, mnogo skuplje
-----
remount gubi state (resetuje na init) state // zapazi
state je text u input, primer uvek
-------
ne treba definisati lokalne komponente jer se unutrasnja remountuje na svaki rerender spoljne
jer se lokalne fje redefinisu, nova ref na svaki rerender, nije primitive value
------
type: u virtual dom poredi sa ref === // zapazi
-----------------
poenta: kako react identifikuje elementa u virtual dom, jsx
default gleda poziciju u array, ali ako postavis keys onda mu pomognes, to ima prioritet // i to je sve
--------------------------
// prevent submit pristine form, react-hook-form
const { isDirty } = formState;
const onSubmitForm = async (business: BusinessUpdateData) => {
if (isDirty && businessId) updateBusiness({ id: businessId, business });
};
-------------------
// zod required field with custom message
name: z.string().nonempty('Name is required').min(2).max(50),
pogledaj koako moze globalno da se postavi u zod 'firstName cannot be empty', umesto 'string cannot be empty'
-------------
pogledaj kako zod moze da se koristi za tipovanje server response, kao trpc
-------------
// useReducer vs useState
// https://www.reddit.com/r/reactjs/comments/10bohti/usereducer_is_easier_to_adopt_than_you_might_think/
useReducer ima transition function_ (dispatch) koja uvek ima validne prelaze iz stanja A u B
je useState plus validacija
--------------
// react-hook-form, trigger validation on field or entire form
trigger() - undefined - sva polja
trigger('firstName') - jedno polje
------------------------
// react-hook-form async 
// default values
https://react-hook-form.com/docs/useform#values
-------------
// mdodvi za validaciju
https://react-hook-form.com/docs/useform#reValidateMode
mode: 'onTouched' // on first blur, (nije na submit dugme), ali fails ako input ima onBlur handler
------------
prakticno resenje je default mode: 'onSubmit' i 
const isEnabledSubmitButton = isValid || !isSubmitted;
-------------------
// how to fix rerender loop
rerender loop, neki uslov treba strozija provera na state koji se menja, u useEffect
network requests loop - znaci remountuje se komponenta i react query u njoj
zakomentarisi if() pa onda stroziji i stroziji uslov
------------------
// react render profiling
https://github.com/welldone-software/why-did-you-render
-------------------
// shadcn components
https://github.com/ozgurrgul/linkedin-clone/blob/main/src/components/primitives/Dialog.tsx
----
// .displayName zato sto je komponenta definisana kao anonimna arrow fja koja je dodeljena u promenljivu
// za identifikaciju u react dev tools i error messages
const DialogHeader = ({
	className,
	...props
  }: React.HTMLAttributes<HTMLDivElement>) => (
	<div
	  className={cn(
		"flex flex-col space-y-1.5 text-center sm:text-left",
		className
	  )}
	  {...props}
	/>
  );
  DialogHeader.displayName = "DialogHeader";
------------------
// da prosledi (zadrzi) postojece ime od DialogPrimitive.Title.displayName, koja je i root tag u ovoj komponenti
// ovo je samo style override prakticno za @radix-ui/react-dialog, headless library verovatno
import * as DialogPrimitive from "@radix-ui/react-dialog";

const DialogTitle = React.forwardRef<
  React.ElementRef<typeof DialogPrimitive.Title />,
  React.ComponentPropsWithoutRef<typeof DialogPrimitive.Title />
>(({ className, ...props }, ref) => (
  <DialogPrimitive.Title // ovde
    ref={ref}
    className={cn(
      "text-lg font-semibold leading-none tracking-tight",
      className
    )}
    {...props}
  />
));
DialogTitle.displayName = DialogPrimitive.Title.displayName;
--------
// jesu staticki props na klasi
.displayName, .propTypes, .defaultProps, .contextTypes
------------------------
// headless UI libraries
// Unstyled Component Libraries Are A Game Changer - Web Dev Simplified
// https://www.youtube.com/watch?v=yn6vFCRkC3c
decoupled styles and logic, custom styles
1. hook version - daju samo props
2. component version - props + html, without css
------------
// libraries:
1. React Aria // hooks i component versions, accessibility props
2. Downshift // samo select
3. Base UI // MUI
4. Radix
5. Headless UI // Tailwind
6. Reach UI
----
Suggested from audiences:
7. Lion (Unstyled web component)
8. React Table (Headless table)

