// React Testing For Beginners

// 3 Jest Explained
describe, beforeEach, beforeAll...

// 4 Writing Unit Tests With Jest
komponenta, klasa, fja

// 5 Writing Integration Tests
integration - fja zavisi od druge fje
ili komponenta zove drugu komponentu

// 6 Mock Functions  Why
const add = jest.fn((x,y) => 3)
expect(add).toaveBeenCalledWith(1,2)
da ne bi stvarno zvao bazu ili api

// 7 Mocking Modules
mockujes ceo modul ili eksternu biblioteku
import {add} from "./add"
jest.mock("./add", ()=> ({
	add: jest.fn(()=>25)
})
add.mockImplementation(()=>30)
spy - fake function_ da vidis da li je pozvana
mock - fake data
assertion

// 8 Introduction To React Testing
// 9 React Testing Library  Debug
// 10 Testing With Test Ids
testiras klik i promenu u domu, ne state i impl details
// 11 Events In React Testing Library
// 12 Integration Testing In React  Cleanup
// 14 Spying  Mocking Functions in React
mockovana fja je obicna fja
spy je mockovana fja za koju moze da se vidi da li je pozvana itd
const fja = jest.fn()
expect(fja).toHavebeenCalledTimes(1)
// 21 Testing Loading States  More Pitfalls
getByTestId() za ono sto si siguran da postoji
queryByTestId() za ono sto ispitujes da li postoji, loader
----------------------
// Alan Aličković
// https://www.youtube.com/watch?v=zHyV3VD-HW4&t=755s
// https://bulletproof-react-presentation.netlify.app
// https://github.com/alan2207/bulletproof-react
// https://bulletproof-react-app.netlify.app
// unit test procedura, load, interakcija, sklanjanje
// unit - da moduli rade ispravno, moduli u izolaciji
---
// integration - da celina radi ispravno, vise komponenti u istom testu, nesto mockovano
// unit tests ne testiraju medjusobnu komunikaciju komponenti, to radi integration tests
---
// e2e - cela aplikacija, by automated tool - AUTOMATION
---
testing library - app kao user, a ne implementation details - black box
---
mock - data za api Response, msw lib - mock service Worker, http interceptor
-------
// error handling - nije vise testing
api errors - axios interceptor with zustand toast
ui app errors - error boundary component, fallback ui, vise od jedne za posebne delove
-------
// auth
authorization - permission - role based i policy based
-------
// security
source mape za dev i sentry, ne za produkciju, ne vidi kod
-------
// performance
code split by routes, redje za scroll
state definisan sto blize gde se koristi, da izbegne rerender vecih delova
izracunavanje inital state za useState kao callback a ne value da izbegne izracunavanje na svaki rerender
znaci useState prima () = > {} fju koja vraca rezultat?
-------------------------
-------------------------
// Testing Javascript with Kent C. Dodds
// 1. Fundamentals of Testing in JavaScript
// kod
// https://github.com/kentcdodds/js-testing-fundamentals
test je kod koji proverava da li je dobijeno === ocekivano i baca exception ako nije
---
pravi osnovni test framework
1. assertion fju - expect()
2. framework - test() - da pokrece sve testove uvek, i stampa koji je test puko, a ne ugnjezdeni stacktrace
---
// async test
mora da await callback u test() helperu da dobije pravi exception, a ne unhandled promise rejection
---
ubaci expect() i test() u global scope da ne treba import
---
testovi sto napravio rade sa jest
---------------
// 2. Static Analysis Testing JavaScript Applications
.eslintrc config objasnio
js syntax version, rules, environment
----
editor extenzija da vidi greske u kodu bez pokretanja u konzoli
ctrl + . // fix, disable for line, or file
------
extend - import set of rules, [eslint:recommended] npr
rules - override pojedinacno
-----------
// 5.
.eslintignore i .gitignore isti fajlovi
---
// 6. prettier
-------
// 7. prettier config
prettier.io/playground preview odmah opcije i export config json
-------
// 8. extension, format on save true, i set default formatter
-------
// 9. eslint-config-prettier, lose objasnio
da eslint ne podvlaci ono sto ce prettier ionako da skloni
-------
// 10. package.json script, zbrda zdola
"format": "npm run prettier -- --write" // reuse prettier skriptu i dodaj --write option, fora
------
"script1": "neke komande...", // zajednicki deo, ovo ne izvrsavas
"script2": "script1 -- more args" // concat args na script1
------
// 11. typescript
babel da bi js radio u svim browserima, manage js versions
------
// 12. typescript, eslint overrides, da resi preklapanja
------
// 13. husky pre-commit validate
------
// 14. lint-staged
formatira samo sto je staged u git
da ne mora husky da fail pa npm run format
------
// 15. npm-run-all --parallel
izvrsava vise skrpti odjednom umesto &&
concurently
--------------------------
// 3. JavaScript Mocking Fundamentals
// kod
// https://github.com/kentcdodds/js-mocking-fundamentals
---
1. uvod, nista
---
// 2. monkey patching - zamenjivanje originalne fje da je ne bi pokretao (jer ima naplatu npr)
override prop on a module // to, to
const originalGetWinner = utils.getWinner;
utils.getWinner = (p1, p2) => p1;

// posle upotrebe moras da pocistis da ne utices na ostale testove, koji ne patchuju ili 
// patchuju na drugi nacin
utils.getWinner = originalGetWinner;
--------
// 3. jest.fn(implementationFn) mock - dodatna kontrola zamenjene fje
// mozes da expect koliko put je pozvana, sa kojim args, i koji args u kom pozivu, return value, itd
// prakticno svi detalji fje
utils.getWinner = jest.fn((p1, p2) => p1);
---
// implementira sam jest.fn, ok
fn(impl) {
	const mockFn = (...args) => {
		return impl(...args)
	}
	return mockFn;
}
-------
// mock vs spy
mock menja ceo objekt, a spy samo property na njemu
spy moze da restore implementation
---------
// 4. jest.spyOn - restore implementation
// patchuje fju
jest.spyOn(utils, 'getWinner');
utils.getWinner.mockImplementation((p1, p2) => p1)

// cisti patched fju
utils.getWinner.mockRestore();
-----
sad pravi spyOn, mockImplementation i mockRestore sam, i edit existing jest.fn, ok
// https://github.dev/kentcdodds/js-mocking-fundamentals/blob/c1d7c9bf275c0d55e413264d5eb602f5516ff4ae/src/no-framework/spy.js
--------
// 5. inline module mock
monkey patch radi samo na commonJs (require), na es import ne (also hoisted to top)
mockovanje - rad sa js modulima
---
// src/__tests__/inline-module-mock.js
jest.mock() 
// 1. arg relative path do modula, spram gde je jest.mock pozvan
// 2. arg fake module implementation

// ciscenje
utils.getWinner.mockReset()
---
// novi momenti oko js require, zapazi
require.cache - key path u node_modules, val module object // jeste objekat obican
---
// src/no-framework/inline-module-mock.js
const utilsPath = require.resolve('../utils') // hvata stazu samo
require.cache[utilsPath] = {
  id: utilsPath,
  filename: utilsPath,
  loaded: true,
  exports: {
    getWinner: fn((p1, p2) => p1)
  }
}
-------------
// 6. external mock module
da apply jest.mock() na sve fajlove pomeri ga u folder __mock__/utils.js
arg2 je sad iz tog fajla
---
sad impl to sam
istumbao na brzinu, jezivo
------------------------
// 4. Configure Jest for Testing JavaScript Applications
// https://github.com/kentcdodds/jest-cypress-react-babel-webpack
// 1. nista uvod
---
// 2. install jest
js executables in console are in nede_modules/bin/name - jest npr
__tests__ folder da bi ih jest gledao kao testove
file.test.js // ili
npm run test, npm test, npm t // za pokretanje
---
// ci
validate: lint && test && build
------
// 3. jest babel
node ne podrzava import es
---
// babelrc.js
env === 'test' => babel/preset-env commonJs // compile to
else let webpack compile import, tree shaking
--------
// 4. test environment
test env:
1. jsdom - browser in node, window obj npr // frontend
2. node - radi brze // backend
----
// jest.config.js
moduleNameMapper: {
  testEnvironment: 'jest-environment-node',
}
-----------
// 5. support import css
// za test
'\\.css$' require.resolve('./test/style-mock.js'), // stub css files with {} 
// inace za dev prod to radi webpack sa css loader
-------------
// 6. support css modules
identity-obj-proxy - stubuje css modules da dobije ime klase umesto undefined
'\\.module\\.css$': 'identity-obj-proxy', // \\. je regex escape dot // redosled vazan, prva linija
-------------
// 7. snapshots
expect(flyingHeroes).toMatchSnapshot() // prvi put ga samo generise, posle poredi
__snapshots__/file.snap // slicno kao json ali vise, moze i dom node u html
- u da update failing
inline - ubaci ga u kod kao string
---------------
// 8. jest emotion css snapshot
za css in js
---------------
// 9. import resolve in webpack
budzeni abs importi za relativ js fajlove
prebaci isto u jest.config.js
moduleDirectories: ['node_modules', path.join(__dirname, 'src'), 'shared'],
---------------
// 10. extend expect za content html elemenata - toHaveTextContent
setupFilesAfterEnv: ['extend-expect']
---------------
// 11. reusable theme provider u testovima
cirkus 7 min
--------------
// 12. jest watch mode
jest izvrsava testove samo u fajlovima koji imaju promene u gitu
za fajl u src pokrece sve testove koje ukljucuju taj kod
----
interactive mode sa shortcuts - slova i filteri
------------
// 13. chrome debugger tests
"test:debug": "node --inspect-brk ./node_modules/jest/bin/jest.js --runInBand --watch",
chrome://inspect
---
moze i kroz src i kroz tests, samo call stack
------------
// 14. jest code coverage
jest --coverage
kreira coverage folder sa html fajlovima koji mogu da se otvore
collectCoverageFrom: ['**/src/**/*.js'], // sta da ukljuci, sta da iskljuci iz reporta da bude tacan procenat
---
// 4 elements of code coverage
statements, branches, functions, lines
---
coverage folder se ne komituje u git
ide u gitignore
------------
// 15. analyze coverage report - ok
jest-babel-istanbul plugin
ubacuje countere u statements i grane i tako prati da li je izvrseno
/* istanbul ignore next */ da ne ubacuje countere, ignore from report - 100% za taj kod
----------------
// 16. coverage treshold
procenti da izadje warning
global ili za globe/folder
// za vazne delove koda da neko ne smanjuje coverage
// i testovi fail red kad padne ispod treshold broja
coverageTreshold: { global: {brojevi}, 'staza/fajl1.ts': {}}
kad dodas za fajl to oduzima pokrivenost od global
---------------
// 17. codecov
npx codecov uploaduje coverage na codeco.io
---------------
// 18. is-ci, nebitno, trivial
pokrece dve skripte da li je u ci (env var CI) ili ne
--------------
// 19. jest client and server config
split config na 2 js fajla
---------------
// 20. jest projects
projects: [config1.js, config2.js] - u jest.config.js za client i server
kad promenis config mora da restartujes testove
displayName - 'server', 'client' - label u config da znas output odakle dolazi u konzoli
velika bela labela
-----------------
// 21. jest lint, nebitno
da pokreces lint dok testiras, a ne on build script
da ne lintujes ceo source nego samo izmenjeni
---------------
// 22. jest watch select projects, plugin
u watch mode da izvrsi testove iz samo jednog projekta, lint, server, client
-------------
// 23. jest typeahead plugins
filter tests by file name ili test name, pluginovi
/**/*/... staza se zove glob
---------
// 24. jest --findRelatedTests - husky commit hook
husky lint-staged precommit hook da pokrene samo relevantne testove za staged kod
i onemoguci komit za fail
---
lokalno radi pa znas da ce CI da prodje
-------------------------
// 5. Test React Components with Jest and React Testing Library
// https://github.com/kentcdodds/react-testing-library-course
---
// 1. intro jest react testing library
------
// 2. react dom, render react component
create and render div
-------
// 3. jest dom
expect.extend
expect(div.querySelector('input')).toHaveAttribute('type', 'number')
----------
// 4. dom testing library
const {getByLabelText} = getQueriesForElement(div) // nebitno u principu, helperi samo
const input = getByLabelText(/favorite number/i)
----------
// 5. react testing library - render
import {render} from '@testing-library/react'
const {getByLabelText} = render(<FavoriteNumber />)
---------
// 6. debug, za stampanje dom
const {debug} = render(<FavoriteNumber />)
debug() // container default
debug(elem)
---------
// 7. rtl test event handlers
// trigger event on input and assert result on other element
fireEvent.change(input, {target: {value: 10}}) 
---------
// 8. user event
import user from '@testing-library/user-event'
user.type(input, '10')
simulira type event sa skupom eveneta keyUp, keyDown itd...
---------
// 9. props update, rerender
const {debug, rerender} = render(<FavoriteNumber />)
debug()
rerender(<FavoriteNumber max={10} />)
debug()
---------
// 10. assert something not rendered - getBy vs queryBy
{getBySomething} = render(<Component />) // vraca fje koje dalje mogu da selektuju dom
getByNesto baca exception ako ne nadje
queryByNesto ako ne nadje ne baca ex nego vraca null // eto
---
expect(queryByRole('alert')).toBeNull();
--------
// 11. test accesibility
label for...
---
import {axe} from 'jest-axe'
const {container} = render(<AccessibleForm />)
expect(await axe(container)).toHaveNoViolations()
--------
// 12. mock http call - dobar, sustinski vazan
import {render, screen, waitFor} from '@testing-library/react';
import {loadGreeting as mockLoadGreeting} from '../api';
// vazno da ne zoves stvarno server
jest.mock('../api'); // mockuje js modul - postavlja mu prazne fje

// postavi response
const testGreeting = 'TEST_GREETING';
mockLoadGreeting.mockResolvedValueOnce({data: {greeting: testGreeting}}); // eto
// render
const {getByLabelText, getByText} = render(<GreetingLoader />);
// uhvati input i button
const nameInput = getByLabelText(/name/i)
const loadButton = getByText(/load/i)
// kucaj i submituj
userEvent.type(nameInput, 'Mary')
userEvent.click(loadButton)
// assert http fn - mockLoadGreeting() call
// kad mockujes fju moras da proveris da je pozvana kako treba
expect(mockLoadGreeting).toHaveBeenCalledWith('Mary')
expect(mockLoadGreeting).toHaveBeenCalledTimes(1)
// await state update and assert state rendered
await waitFor(() =>
  expect(getByLabelText(/greeting/i)).toHaveTextContent(testGreeting); // response koji je postavljen
)
---
// moze ili ili 
const {getByLabelText} = render(<GreetingLoader />);
const nameInput = getByLabelText(/name/i)
// ekvivalnetno
render(<GreetingLoader />);
const nameInput = screen.getByLabelText(/name/i); // screen staticki neki objekat
----
// warning, code that causes state update should be wrapped in act
setState() posle async http izaziva ovo, da flush updates after event loop
react testing library vec mota u act
---
// ovo ga fix, nastavlja da zove cb dok ne prestane da baca exception...??
// wait() je async
// ceka da komponenta finish updates, rerender verovatno
import {wait} from '@testing-library/react';
await wait(() =>
  expect(getByLabelText(/greeting/i)).toHaveTextContent(testGreeting); // response koji je postavljen
)
-------------------
// 13. mock http with dependency injection
samo ako env ne podrzava jest.mock(), storybook npr
jest.mock() je u jest env, menja svuda u kodu taj import modul
---
// mockuje samo jednu fju, ne ceo modul
const mockLoadGreeting = jest.fn()
// ubacuje je kao prop, morao da izmeni komponentu
render(<GreetingLoader loadGreeting={mockLoadGreeting} />)

// izmena u komponenti
import * as api from './api'
// u komponenti prosledio import kao default prop
function GreetingLoader({loadGreeting = api.loadGreeting}) {...}
--------
// 14. react transition 
da ne ceka 1s za css transition
// mock fju u lib da ne ceka transition
jest.mock('react-transition-group', () => {
  return {
    CSSTransition: (props) => (props.in ? props.children : null),
  }
})
click btn i assert sta ima, a sta nema na ui
---
state change je async i mora wait
----------
// 15. error boundary component
rerender, ne znam bas zasto?
afterEach(() => {
  jest.clearAllMocks()
})
----------
// 16. remove error log, js dom i react dom
beforeAll(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {})
})
afterAll(() => {
  console.error.mockRestore()
})
-----------
// 17. error boundary rerender
rerender fallback ui and assert
-----------
// 18. render wrapper option
// mota sa ErrorBoundary u svim rerender pozivima
const {rerender} = render(<Bomb />, {wrapper: ErrorBoundary})
-----------
// 19. form tdd
trivijal napise test da ocekuje labele, polja i dugme, pa onda formu
-----------
// 20. test submit btn disabled
trivijal
----------
// 21. test form post http call
tdd red green, call http postMessage
mock http call with blank
nista posebno
----------
// 22. tdd router redirect
// mock je to: rename import, set definition, use and assert
import {Redirect as MockRedirect} from 'react-router';
jest.mock('react-router', () => {
  return {
    Redirect: jest.fn(() => null), // functional component
  }
})
---
// fn component props and this
await wait(() => expect(MockRedirect).toHaveBeenCalledWith({to: '/'}, {}))
// svaki async call u komponenti mora da bude sa wait u testu
// wait utility zove callback dok ne prestane da baca exception ili timeout
// sto manje expect() poziva u wait za manje cekanje
----------
// 23. assert dates
// assert da je date ismedju prevDate i postDate, glomazno bzvz
expect(mockSavePost).toHaveBeenCalledWith({
  ...fakePost,
  date: expect.any(String), // nested expect for prop
})
---
// uhvati arg sa kojim je mocked fja pozvana
const date = new Date(mockSavePost.mock.calls[0][0].date).getTime()
------------
// 24. generate fake data, nista posebno
import {build, fake, sequence} from 'test-data-bot'
const postBuilder = build('Post').fields({
  // f je faker
  content: fake((f) => f.lorem.paragraphs().replace(/\r/g, '')), // trim newline
})
// id
const userBuilder = build('User').fields({
  id: sequence((s) => `user-${s}`),
})
-------------
// 25. test error state http response, tdd
// mock rejected promise
mockSavePost.mockRejectedValueOnce({data: {error: testError}})
// findBy je async i retry until timeout, kao wait util
const postError = await screen.findByRole('alert')
expect(postError).toHaveTextContent(testError) // assert
---
// findBy async, getBy i queryBy sync // zapazi
---------------
// 26. shared render function in tests
function renderEditor() {
  const fakeUser = userBuilder()
  const utils = render(<Editor user={fakeUser} />)
  const fakePost = postBuilder()

  screen.getByLabelText(/title/i).value = fakePost.title
  screen.getByLabelText(/content/i).value = fakePost.content
  screen.getByLabelText(/tags/i).value = fakePost.tags.join(', ')
  const submitButton = screen.getByText(/submit/i)
  return {
    ...utils,
    submitButton,
    fakeUser,
    fakePost,
  }
}
---
const {submitButton, fakePost, fakeUser} = renderEditor()
-------------
// 27. router hystory context provider
// kad god koristis bilo koji provider moras i u testovima da ga rekreiras
// provider da bi Link i dr router komonente radile
const hystory = createMemoryHystory({initialEntries: ['/']})
render(
  <Router hystory={hystory}>
    <Main />
  </Router>,
)
expect(screen.getByRole('heading')).toHaveTextContent(/home/i) // h1 tag sa getByRole()
-------------
// 28. test router 404 route, pros
// assert 404 string
expect(screen.getByRole('heading')).toHaveTextContent(/404/i)
--------------
// 29. shared render function for router
// zapazi render args ui component i options object
return rtlRender(ui, {
  wrapper: Wrapper,
  ...renderOptions,
}
---
// rename bzvz
import {render as rtlRender} from '@testing-library/react'
---
// reusable render fn
function render(ui, {route = '/', ...renderOptions} = {}) {
  window.history.pushState({}, 'Test page', route)
  // Wrapper da bi rerender radio
  function Wrapper({children}) {
    return <Router>{children}</Router>; // apply provider in wrapper
  }
  return rtlRender(ui, {
    wrapper: Wrapper,
    ...renderOptions,
  })
}
------------
// 30. test component with redux, korisno
// ovako assert text /count/i, case insensitive regex
expect(screen.getByLabelText(/count/i)).toHaveTextContent('1')
---
// test store and component integrated, a ne posebno reduceri i action creatori
render(
  <Provider store={store}>
    <Counter />
  </Provider>,
)
// test interaction, redux ignored as implementation detail
------------
// 31. redux store initial value
const store = createStore(reducer, {count: 3})
// ostalo isto
------------
// 32. reusable render for redux, ok
function render(
  ui,
  {
    initialState,
    store = createStore(reducer, initialState),
    ...renderOptions
  } = {},
) {
  function Wrapper({children}) {
    return <Provider store={store}>{children}</Provider>
  }
  return {
    ...rtlRender(ui, {
      wrapper: Wrapper,
      ...renderOptions,
    }),
    store,
  }
}
--------------
// 33. test custom hook, koristan
counter hook with state
---
// component in state to call the hook
// convert hook to component
let result
function TestComponent() { // component
  result = useCounter()
  return null
}
render(<TestComponent />) // render that component
---
import {act} from '@testing-library/react'
import React, { useLayoutEffect } from "react"
import { stripLeadingSlash } from "history/PathUtils"
act(() => result.increment()) // fix state update outside of act, slicno kao wait za async
---
// rekao objasnjenje
utils wait, waitFor, fireEvent, findBy automatski motaju u act
ovde direktno setState pa mora act
setState je async, oko toga se vrti, test mora da retry i ceka
--------------
// 34. reusable setup hook function
test default hook args
// destructure, 2x default values
function useCounter({initialCount = 0, step = 1} = {}) {
----
// fora, error ne moze da pristupi return vals hooka
function setup({initialProps} = {}) {
  const result = {} // umesto u var u prop objekta da bi bio isti ref
  // ovo je komponenta, rerenderuje se
  function TestComponent(props) {
    result.current = useCounter(props) // fora, da bi koristio istu instancu
    return null
  }
  render(<TestComponent {...initialProps} />)
  return result
}

test('exposes the count and increment/decrement functions', () => {
  const result = setup() // ovaj se ne re-assignuje, samo jednom, const
  expect(result.current.count).toBe(0) // tu
------------
// 35. renderHook util from rtl, ok
// ovo iz preth klipa vec ima implementirano u rtl
import {renderHook, act} from '@testing-library/react-hooks'
// initialProps option za hook props
const {result} = renderHook(useCounter, {initialProps: {initialCount: 3}}); // samo to
expect(result.current.count).toBe(0); // isto ima current
---
options u js je named args
------------
// 36. rerender() renderHook
// rerender() je render sa novi props koji prosledis, after initialProps // ok
const {result, rerender} = renderHook(useCounter, {
  initialProps: {step: 3}, // initial
})
rerender({step: 2}) // novi props, tu
act(() => result.current.decrement())
expect(result.current.count).toBe(1)
------------
// 37. test portals
useLayoutEffect built in react ???
---
// hvatanje elementa sa data-testid, zapazi
<div data-testid="test" />
expect(getByTestId('test')).toBeInTheDocument()
---
nekoliko nacina da se assert ogranici na podstablo, default render je ceo document.body
queries utils su bounded na document ili podstablo
---
// within() najbolja opcija, render ostaje netaknut
render(
  <Modal>
    <div data-testid="test" />
  </Modal>,
)
// bind queries da trazi u podstablu, node <div id="modal-root" />
const {getByTestId} = within(document.getElementById('modal-root')) // ovde
expect(getByTestId('test')).toBeInTheDocument()
---
// getByTestId je query, sto vrati render
-----
// druge opcije
queries.getByTestId(document.body, 'div-id') // prvi arg je podstablo
---
render(
  <Modal>
    <div data-testid="test" />
  </Modal>,
  {baseElement: document.body} // option za scope
)
----------
// 38. test unmounting, clean up setInterval, solidna lekcija
setState on unmonted component = memory leak
---
beforeAll - setup, afterAll - cleanup // vazno, zapazi
// prevent console.error da ne clutter test log
beforeAll(() => {
  jest.spyOn(console, 'error').mockImplementation(() => {})
})
---
// clean spy for other tests
afterAll(() => {
  console.error.mockRestore()
})
---
mora red, green da break test, da bi znao da testira kako treba
---
testu treba 10 sekundi da testira timer
-----
test('does not attempt to set state when unmounted (to prevent memory leaks)', () => {
  jest.useFakeTimers(); // sredjuje 10 sec
  // mozes da unmount component imperativno, rtl vraca, zapazi
  const {unmount} = render(<Countdown />);
  unmount();
  // ovo mota 10 sec
  act(() => jest.runOnlyPendingTimers()); // sklanja act gresku, tajmer unutra, zapazi...?
  expect(console.error).not.toHaveBeenCalled(); // assert console.error nije zvan
---
act ceka da se stejtovi rerenderuju
---
moras da srusis test za svaki bug koji zelis da sprecis, red green
---------
// 39. multistep form
// ima smisla da se prekrsti u mockSubmitForm
import {submitForm as mockSubmitForm} from '../api'
// jer ovaj poziv ga zameni u ceo app
jest.mock('../api')
// postavi http response
mockSubmitForm.mockResolvedValueOnce({success: true})
// u end to end testovima se zovu pravi http endpointi
---
// trigger click
fireEvent.click(screen.getByText(/fill.*form/i))
// fill in text field, set onChange
fireEvent.change(screen.getByLabelText(/food/i), {
  target: {value: testData.food},
})
---
// drzi debug() dok pise test
--- 
// salje evente i expect text na ui, procedura
// proveri args mock fje i da jepozvana jednom
expect(mockSubmitForm).toHaveBeenCalledWith(testData)
expect(mockSubmitForm).toHaveBeenCalledTimes(1)
-----------
// 40. getBy -> await findBy, trivial
replace all getBy sa await findBy
findBy - retry on dom change
-----------
// 41. fireEvent -> userEvent
// ima smisla
import userEvent from '@testing-library/user-event'
// click
fireEvent.click(screen.getByText(/fill.*form/i))
userEvent.click(await screen.findByText(/fill.*form/i))
// input onChange
fireEvent.change(screen.getByLabelText(/food/i), { target: {value: testData.food}}) // event direktno
userEvent.type(await screen.findByLabelText(/food/i), testData.food) // higher level
--------
// test higher level, nesvestan of implementation details
------------------------------
// 6. Install, Configure, and Script Cypress for JavaScript Web Applications
// calculator project
// https://github.com/kentcdodds/jest-cypress-react-babel-webpack
-----
// 1. uvod, github
----
// 2. install cypress
dev dependency
npx cypress open // runs examples
eslintrc.js // za cypress
gitignore videos and screenshots
----------
// 3. first test
// https://github.com/kentcdodds/jest-cypress-react-babel-webpack/blob/egghead-2018/cypress-03/cypress/e2e/calculator.js
npm run dev // pokrenes app // 1.
describe('anonymous calculator', () => {
  it('can make calculations', () => {
    cy.visit('http://localhost:8080') // 2.
    .get('._2S_Gj6clvtEi-dZqCLelKb > :nth-child(3)') // selektori, sve je async zapazi, in parallel
    .click()
---
// kao phantom.js
hvataj dugmice i klikci, i assert result
----------
// 4. cypress.json config
"baseUrl": "http://localhost:8080",
cy.visit('/')
---
integration tests - jest
e2e - cypress
---
// cypress.json
{
  "baseUrl": "http://localhost:8080",
  "integrationFolder": "cypress/e2e", // rename
  "viewportHeight": 900, // viewport
  "viewportWidth": 400
}
-----------
// 5. add testing-library/cypress
// install
yarn add -D @testing-library/cypress // za selectore, opet mora async, ne moze getBy
// config in cypress/support/index.js
import 'cypress-testing-library/add-commands'
// ok sve isto queryji kao unit tests
---
// use
cy.visit('/')
.getByText(/^1$/) // regex za 1
.click()
.getByText(/^\+$/) // +
.click()
.getByTestId('total') // u src data-testid="total", jer ima 2 btn sa content '3'
------------
// 6. cypress npm scripts
npx cypress run // run headless
---
// za next.js ce biti drukcije
start-server-and-test // concurrently
package.json skripte za cypress i ci
------------
// 7. cypress debug
.then(subject => {debugger; return subject;})
.debug()
.pause()
---
// u app src
if(window.Cypress) {
  // pokrenuto u cypress
}
---
// pises e2e testove kao sto bi user koristio app
-------------
// 8. test register flow
ides po app i klikces
---
// generate.js // gen fake data
import {build, fake} from 'test-data-bot'
const userBuilder = build('User').fields({
  username: fake(f => f.internet.userName()),
  password: fake(f => f.internet.password()),
})
---
// e2e register.js
const user = userBuilder()
cy.visit('/') // polazna stranica
  .getByText(/register/i)
  .click()
  .getByLabelText(/username/i)
  .type(user.username) // eto
---
.url() // subject address bar
.should('eq', `${Cypress.config().baseUrl}/`) // config fajl je js object
.window() // change subj opet
.its('localStorage.token')
.should('be.a', 'string') // expect()
---
subject - ono po cemu robot kuca, inputs, address bar
---------------
// 9. tdd with cypress, write test first
red, green, refactor
add logged in user username
---
kada npr multistep form, za dev da cypress auto fill 6 steps mesto rucno
---------------
// 10. test http error handling on ui
// intercept http, prevent backend
it('should show an error message if theres an error registering', () => {
  // mock http with 500, pros
  cy.server()
  cy.route({
    method: 'POST',
    url: 'http://localhost:3000/register',
    status: 500,
    response: {},
  })
---
it.only('test opis') // only() run samo taj test, ne sve
----
// test blocks
describe('registration', () => {
  it('should register a new user', () => {
-------------
// 11. test login
trivial, copy paste from register
fill username, pass, submit 
assert username label
--------------
// 12. cypress request
// in login replace register form submit with http submit, trivial
cy.request({
  method: 'POST',
  url: 'http://localhost:3000/register',
  body: user,
})
---------------
// 13. reusable cypress command
// cypress/support/commands.js 
Cypress.Commands.add('createUser', overrides => {
  const user = userBuilder(overrides)
  return cy
    .request({
      url: 'http://localhost:3000/register',
      method: 'POST',
      body: user,
    })
    .then(({body}) => body.user)
})
---
// use created user
cy.createUser().then(user => {
  cy.visit('/')
    .type(user.username)
--------------
// 14. reusable asserts as commands, trivial
// cypress/support/commands.js 
Cypress.Commands.add('assertHome', () => {
Cypress.Commands.add('assertLoggedInAs', user => {
----
// usage
cy.visit('/')
.assertHome()
.assertLoggedInAs(user)
----------
// 15. assert logged out username doesnt exist
// bas trivial
.should('not.exist')
-----------
// 16. login with request again, bas trivial
// replace submit login with request and assert localstorage token
-----------
// 17. login user as command, kratak
// custom command se zove ovako, kao postojeci custom helper, chained // zapazi
cy.login(user)
----------
// 18. combine 2 commands into one
// nista specijalno
Cypress.Commands.add('loginAsNewUser', () => {
  cy.createUser().then(user => {
    cy.login(user)
  })
})
-----------
// 19. install react devtools in react
// window.__react_devtools... mora da se desi pre nego je loadovan react
// public/index.html
<script>
if (window.Cypress) {
  window.__REACT_DEVTOOLS_GLOBAL_HOOK__ = window.parent.__REACT_DEVTOOLS_GLOBAL_HOOK__
}
</script>
---
// moze i kao prvi import u index.js
------
// zakljucak: Cypress je prost, samo procedure flow sta bi user radio
// prouci ostale foldere
------------------------
// 7. Use DOM Testing Library to test any JS framework
// test basic state update (counter) in all frameworks
// nebitno celo poglavlje za react, samo radoznalost
// https://github.com/kentcdodds/dom-testing-library-with-anything
---
// 1. dom testing library
uvod u projekt
moze bilo koji framework
-----------
// 2. react render()
// rtl importi
import '@testing-library/jest-dom/extend-expect'
import {getQueriesForElement} from '@testing-library/dom'
import userEvent from '@testing-library/user-event'
---
// samo ove 4 dom manipulacije
function render(ui) {
  const container = document.createElement('div')
  ReactDOM.render(ui, container) // 1
  document.body.appendChild(container) // 2
  return {
    ...getQueriesForElement(container), // ovaj vraca getByText
    container,
    cleanup() { // ocisti dom za sledeci test
      ReactDOM.unmountComponentAtNode(container) // 3
      document.body.removeChild(container) // 4
    },
  }
}
----
test('renders a counter', () => {
  const {getByText, cleanup} = render(<Counter />)
  const counter = getByText('0')
  userEvent.click(counter)
-------------
// 3. preact render()
// react renderuje synchrounsly, preact waits tick of event loop
fireEvent.click(counter)
await wait() // ceka do sledeceg ticka event loopa, iza fireEvent, dok preact async renderuje promene u dom
---
// render skoro isti
function render(ui) {
  const container = document.createElement('div')
  Preact.render(ui, container)
  return {
    container,
    ...getQueriesForElement(container),
  }
}
------------
// 4. jquery
// query - helper koji hvata pomocu selektora
// ovaj iznakaci queries na obican dom node
import {getQueriesForElement} from '@testing-library/dom'
const div = document.createElement('div')
const {getByText} = getQueriesForElement(div)
----
// jquery renderuje preko custom plugina
$(div).countify()
------------
// 5. dojo framework, nebitno
------------
// 6. hyperapp, async render, nebitno
novi framework - fundamentalno razumevanje (modelovanje) problema na drugi nacin
------------
// 7. angular 1, nebitno
import angular from 'angular'; // v1
// mount app, kao ReactDOM.render()
angular.bootstrap(container, config.modules)
------------
// 8. angular 2+, nebitno
// zakomplikovano
import {TestBed, ComponentFixtureAutoDetect} from '@angular/core/testing'
------------
// 9. vue, async render takodje, nebitno
// prost mount
const vm = new Vue(Component).$mount();
const container = vm.$el;
-----------
// 10. mithril, async, indetermenistic render, nebitno
-----------
// 11. fromHTML, nebitno
import fromHTML from 'from-html/lib/from-html'
------------
// 12. svelte, async
// prost mount, samo constructor
new Component({target: container})
--------------------------
// 8. Test Node.js Backends
// https://github.com/kentcdodds/testing-node-apps
------
// 1. book app and repo
--------------
// 2. test pure functions
zadatak, nista
--------------
// 3. solution
test valid, invalid isPasswordAllowed()
red, green
--------------
// 4. test pure function forEach
// test u forEach
describe('isPasswordAllowed only allows some passwords', () => {
  const allowedPasswords = [...]
  allowedPasswords.forEach(password => { // forEach
    test(`allows ${password}`, () => { // test
      expect(isPasswordAllowed(password)).toBe(true)
    })
  })
--------------
// 5. pure function cases
// pure function sve grane prakticno
import cases from 'jest-in-case';
cases(
  'isPasswordAllowed: valid passwords',
  ({password}) => {
    expect(isPasswordAllowed(password)).toBe(true)
  },
  {
    'valid password': {
      password: '!aBc123',
    },
  },
)
-------------
// 6. pure function cases helper fja
// vraca 2 dim polje, zato moze
Object.entries(obj).map(([name, password])
const [name, password]
---
// ubacuje password u opis greske u log, trivial
function casify(obj) { // helper fja
-------------
// 7. test express middleware, zadatak
-------------
// 8. test express error middleware, resenje, UnauthorizedError case
test('responds with 401 for express-jwt UnauthorizedError', () => {
  // mock req, res, next, error
  const req = {}
  const res = {
    json: jest.fn(() => res), // chainable
    status: jest.fn(() => res),
  }
  const next = jest.fn() // just fn
  const error = new UnauthorizedError('fake_code', {
    message: 'Fake Error Message',
  })
  // call
  errorMiddleware(error, req, res, next)
  // assert argumente sa kojima je pozvan i koliko puta je pozvan, next i res mockovi iznad
  expect(next).not.toHaveBeenCalled()
  expect(res.status).toHaveBeenCalledWith(401)
--------------
// 9. error middleware, header sent case, trivial
const res = buildRes({headersSent: true}); // samo res drugaciji
--------------
// 10. generic 500 case
podesi res i next asserts malo, trivial
--------------
// 11. factory for res object refactor, trivial
buildRes()
--------------
// 12. refactor req, res, next
// utils/generate.js
buildReq, buildRes, buildNext
---------------
// 13. controllers - zadatak
controller - collection of middlewares
---------------
// 14. test list items controller
import * as obj from 'nesto'; // import default + all named as obj
----
// ovo mockuje ovaj modul ne samo u test fajlu vec u celom projektu za taj test run
// tj svi importi u drugim fajlovima
// ponovo rekao ovo
// prakticno postavlja sve fje na prazne placeholdere cije return values ce da
// predefinise u testu, samo koje koristi // ok, razumeo
jest.mock('../../db/books'); // da ne zove pravu bazu, nego vrati podmetnute vrednosti
------
// izoliraj mockove u drugim testovima...?
beforeEach(() => {
  jest.resetAllMocks()
})
------
// ok, tipican api endpoint test, happy path, korisno
test('getListItem returns the req.listItem', async () => {
  // spremi user, book (db response), req, res objekte sa reusable build factories
  const user = buildUser()
  const book = buildBook()
  const listItem = buildListItem({ownerId: user.id, bookId: book.id})

  // mock db response
  booksDB.readById.mockResolvedValueOnce(book)

  const req = buildReq({user, listItem})
  const res = buildRes()

  // glavni middleware poziv
  await listItemsController.getListItem(req, res)

  // assert mocked db call args and times
  expect(booksDB.readById).toHaveBeenCalledWith(book.id)
  expect(booksDB.readById).toHaveBeenCalledTimes(1)

  // assert middleware response and times
  expect(res.json).toHaveBeenCalledWith({
    listItem: {...listItem, book},
  })
  expect(res.json).toHaveBeenCalledTimes(1)
})
-------------
// 15. test 400 error if id is not provided, trivial prilicno
expect() je assertion, u jest
---
u - update snapshot, i inline snapshot
-----
// async, zove endpoint
test('createListItem returns a 400 error if no bookId is provided', async () => {
  // pripremi req, res
  const req = buildReq()
  const res = buildRes()

  // ne treba mock, ni ne zove se baza

  // glavni endpoint poziv
  await listItemsController.createListItem(req, res)

  // assert api response, res status i json response
  expect(res.status).toHaveBeenCalledWith(400)
  expect(res.status).toHaveBeenCalledTimes(1)
  // inline snapshot u kodu, zapazi
  expect(res.json.mock.calls[0]).toMatchInlineSnapshot(`
    Array [
      Object {
        "message": "No bookId provided",
      },
    ]
  `)
  expect(res.json).toHaveBeenCalledTimes(1)
})
-----------------
// 16. test real middleware (not endpoint), ok
// middleware koji kaci listItem na req
if (req.user.id === listItem.ownerId) { // ta provera za success
  req.listItem = listItem
  next() // ne vraca res.json() nego next()
}
----
// mock db calls first
jest.mock('../../db/list-items')
// test
test('setListItem sets the listItem on the req', async () => {
  // arrange, act, assert

  // propremi user, listitem, req, res, next
  const user = buildUser()
  const listItem = buildListItem({ownerId: user.id})

  // mock db call fn, ResolvedValue - promise value
  listItemsDB.readById.mockResolvedValueOnce(listItem)

  const req = buildReq({user, params: {id: listItem.id}})
  const res = buildRes()
  const next = buildNext()

  // glavni poziv, system under test
  await listItemsController.setListItem(req, res, next)

  // assert mocked db fn
  expect(listItemsDB.readById).toHaveBeenCalledWith(listItem.id)
  expect(listItemsDB.readById).toHaveBeenCalledTimes(1)

  // assert result, req.param postavljen
  expect(next).toHaveBeenCalledWith(/* nothing */) // next() ako ima arg to je error
  expect(next).toHaveBeenCalledTimes(1)
  expect(req.listItem).toBe(listItem)
})
-------------
// 17. test middleware 404 granu

// next() nije pozvan
expect(next).not.toHaveBeenCalled()

// status 404
expect(res.status).toHaveBeenCalledWith(404)
expect(res.status).toHaveBeenCalledTimes(1)

// res json error message
expect(res.json.mock.calls[0]).toMatchInlineSnapshot(...)
expect(res.json).toHaveBeenCalledTimes(1)
---------------
// 18. test 403 forbidden if else granu
// skoro isto ko 404
---
snapshot ima uuid koji drugaciji svaki put, zameni sa hardcoded mock
--------------
// 19. mock array items

// 2 mocka
booksDB.readManyById.mockResolvedValueOnce(books)
listItemsDB.query.mockResolvedValueOnce(userListItems)

// assert za oba mocka i res
----------------
// 20. test happy path create list item book assoc with user
// treba logiku koda da razumes da bi znao sta da testiras
// happy path je putanja kroz uslove, grane
// toHaveBeenCalledWith(), toHaveBeenCalledTimes() je za funkcije, moze i ugradjene, res.json() npr
3 db fje mock, assert args and times, i assert res.json() // nista specijalno
----------------
// 21. createListItem 400 error, double id
// 400 bad request
// mock db call sa existing item za uslov
listItemsDB.query.mockResolvedValueOnce([existingListItem])
ostalo nista specijalno
-----------------
// 22. update happy path - nista specijalno
// u svim testovima prakticno mock db call, assert res.json() response, i db mocks args
// testira logiku endpoint fje
// bezveze 5 lekcija jedno te isto, a sustinu nije objasnio
-----------------
// 23. test delete call - nista specijalno
// ne treba da se mockuje jer nije bitna return value
expect(listItemsDB.remove).toHaveBeenCalledWith(listItem.id)
----------------
// 24. test auth api - zadatak
zove prave auth api i db, nema mock
-----------------
// 25. test auth api - resenje, integration test

let server; // global instance
// start/stop server pre i posle svih testova (ne svakog nego ukupno)
beforeAll(async () => {
  server = await startServer({port: 8000})
})
// vracen promise, jest ce da ga saceka
afterAll(() => server.close())
beforeEach(() => resetDb())
---
generate user pass and call axios.post('/register')
-----------------
// 26. nastavak - trivial
assert response id, token strings, username to match passed
----------------
// 27. login with same user/pass after register, trivial
// login response === register response
expect(lResult.data.user).toEqual(rResult.data.user)
-----------------
// 28. auth/me get call, trivial
// just send token
const mResult = await axios.get('http://localhost:8000/api/auth/me', {
  headers: {
    Authorization: `Bearer ${lResult.data.user.token}`,
  },
})
// me response === login response
expect(mResult.data.user).toEqual(lResult.data.user)
---
// ceo integration test register -> login -> auth/me, trivial
-------------------
// 29. axios baseUrl
// samo u testu
const api = axios.create({baseURL})
-------------------
// 30. error description, axios interceptior, onSuccess, onError
import {getData, handleRequestFailure} from 'utils/async';
api.interceptors.response.use(getData, handleRequestFailure);
// skoro nista
const getData = res => res.data;
--------------------
// 31. unique server port for each test file, trivial
// test/setup-env.js
const port = 8000 + Number(process.env.JEST_WORKER_ID);
process.env.PORT = port; // moze dodela u env var
---
// recalculate axios baseUrl
const baseURL = `http://localhost:${server.address().port}/api`;
---------------------
// 32. unique username, register 2 times with same username, trivial
// pretvara gresku u success, rejected promise to resolved
.catch(result => result)
-----
convert error to message and assert snapshot
--------------------
// 33. insert user in db, bzvz
umesto register api
--------------------
// 34. blank user, pass... edgecases, trivial
// ovo su integration testovi
// edgecases u unit testovima
----
// toMatchInlineSnapshot zapravo jesto korisno, umesto da rucno kopiras error string
expect(error).toMatchInlineSnapshot(
  `[Error: 400: {"message":"username can't be blank"}]`,
)
---------------------
// 35. integration test crud api, zadatak
// integration test je samo vise povezanih unit testova koji koriste medjurezultate
----------------------
// 36. crud integration test, create
listItem - ima userId i bookId
---
// u svim testovima se ponavlja
// create user, token, attach token, format errors
async function setup() {
  const testUser = await insertTestUser()
  const authAPI = axios.create({baseURL})
  authAPI.defaults.headers.common.authorization = `Bearer ${testUser.token}`
  authAPI.interceptors.response.use(getData, handleRequestFailure)
  return {testUser, authAPI}
}
---
const {testUser, authAPI} = await setup()
// glavni poziv
const cData = await authAPI.post('list-items', {bookId: book.id})
// assert userId and bookId in object
expect(cData.listItem).toMatchObject({
  ownerId: testUser.id,
  bookId: book.id,
})
------------------------
// 37. read - get item by id, trivial
// getById poziv
const rData = await authAPI.get(listItemIdUrl);
// assert create id === read id
expect(rData.listItem).toEqual(cData.listItem)
----------------------
// 38. test update, PUT, trivial
// glavni poziv
const updates = {notes: generate.notes()}
const uResult = await authAPI.put(listItemIdUrl, updates)
// assert
expect(uResult.listItem).toEqual({...rData.listItem, ...updates}) // rucni update
---------------------
// 39. delete, trivial
// glavni poziv
const dData = await authAPI.delete(listItemIdUrl)
expect(dData).toEqual({success: true}); // prvi assert

// pokusaj read i ocekuj gresku, dodatna provera
const error = await authAPI.get(listItemIdUrl).catch(resolve)
expect(error.status).toBe(404)
// uvek kad je neki string stavi inline snapshot
expect(error.data).toEqual({
  message: `No list item was found with the id of ${listItemId}`,
})
----------------------
// 40. handle id in snapshot, trivial
update snapshot cmdline option ili u na pokretanje
----
// rucno ga zameni sa str.replace()
const idlessMessage = error.data.message.replace(listItemId, 'LIST_ITEM_ID')
expect(idlessMessage).toMatchInlineSnapshot(
  `"No list item was found with the id of LIST_ITEM_ID"`,
)
// ima dosta utils foldera u projektu
-----------------------------
-----------------------------
// unit, integration, e2e
// https://github.com/alan2207/bulletproof-react/blob/master/docs/testing.md
unit - component, class, for shared, used in different scenarios
integration - parts of the app, najvazniji, najvise samopouzdanja da radi
e2e - cela aplikacija, kao user
---------------------------
// 1. Introduction to Testing: Concepts for Beginners - React.js Testing Tutorial #1
//  Bruno Antunes
// https://www.youtube.com/watch?v=41ox41v62jU&list=PLYSZyzpwBEWTBdbfStjqJSGaulqcHoNkT
e2e tests - autometed tests lists for a robot instead of manual tester
integration - 2+ units
unit - class_ or component - IN ISOLATION
----
mock - substitute code for isolation
----
when? - probability/severity matrix
-----
test public interface - args and return value, a ne implementation details
jer za breaking change moras testove da bacis i krenes od nule
-----------------
// 2. Setup Testing Env: Jest, React Testing Library, eslint, GitHub Actions -React.js Testing Tutorial
// Setup Testing Env
// https://github.com/bmvantunes/youtube-react-testing-video2-config-jest-react-testing-library
install next typescript ts-jest
it() === test() // u jest
fit(), it.only() // izvrsi samo taj 1 test
it.todo('opis'); // dekleration, nedefinisan za sad
--------
// config, korisno
jest, ts, testing-library, eslint, eslint-plugins, lint-staged, github actions
-------------------
// 3. React Testing Library for Beginners: React.js Testing Tutorial
// React Testing Library
// oduzio trivijalno, lose
// https://github.com/bmvantunes/youtube-react-testing-video3-introduction-jest-react-testing-library
enzyme testira implementation details, zato react testing library
-------
screen.getByText() throws exception ako ne nadje
zato ne moras da proveravas ispod da li elem postoji, test fails
----
// getByRole - accessibility selector
screen.getByRole('button', { name: 'Add to Counter' })
aria-label="Add to Counter"
---
describe() // grupa testova kojima mozes da postavis zajednicki beforeEach() i afterEach()
---
// type event, select text before type, ima ih jos
import user from '@testing-library/user-event';
user.type(screen.getByLabelText(/Incrementor/), '{selectall}5');
--------------------
// 4. React Async Testing using React Testing Library for Beginners: React.js Testing Tutorial 
// React Async Testing
// oduzio i zakomplikovao, skoro beskorisno
// https://github.com/bmvantunes/youtube-react-testing-video4-async-tests-jest-react-testing-library
async test - moras da cekas za rezultat
---
findByText() - prvi odmah, default retry (da - retry) interval = 50ms, default timeout 1s
// findBy se awaituje, async
await screen.findByText('Current Count: 16');
----
// ceo expect() je unutar waitFor() i ceka ui update
// awaituje se, async
await waitFor(() =>
  expect(screen.getByText('Current Count: 1')).toBeInTheDocument()
);
----
// test da se pojavio i sklonio loader
// mora da bude prikazan prvo
import { waitForElementToBeRemoved } from '@testing-library/react';
---
// duga prica update state on unmounted component, nema veze sa testovima
useEffect(() => {
  let active = true;

  if (count >= 15) {
    setTimeout(() => {
      if (active) { // moze samo setState, ne mora ceo setTimeout
        setBigEnough(true);
      }
    }, 300);
  }

  return function cleanup() { // moze imenovana da se vidi u stacktrace za debug
    active = false;
  }
}[]);
---------------------
// 5. Mocking React Components and Functions using Jest for Beginners - React.js Testing Tutorial 
// Mocking React Components and Functions
// https://github.com/bmvantunes/youtube-react-testing-video5-mocking-with-jest-and-react-testing-library

// example 1
// spy - cuva istoriju koliko je puta fja pozvana, sa kojim args
// pass return value, pass implementation
const randomSpy = jest.spyOn(Math, 'random');
// clear history before each test
randomSpy.mockClear().mockReturnValue(0);
----
// example 2 - mock table component from MUI
// done() arg u test() oznaci kraj testa u callbacks
----
// assert handler fn is called by passing mock fn
const myOnMoney = jest.fn();
render(<Example2 onMoney={myOnMoney} />);
expect(myOnMoney).toHaveBeenCalledTimes(1);
----
// mock just one prop in a module
jest.mock('@material-ui/data-grid', () => ({
  ...jest.requireActual('@material-ui/data-grid'),
  DataGrid: jest.fn(() => <div>Table</div>), // this
}));
---
// drugi nacin?
import { mocked } from 'ts-jest/utils';
const mockedDataGrid = mocked(DataGrid);
-----
// props je prvi arg obj react componente, drugi arg obj je context // zapazi
React.Component(props, context)
------
// example 3
// mock my component Drawer, bezveze mu je primer
------
// example 4
// opet MyDrawer, bezveze konfuzno
------
// mock component
// 1. factory object za module
jest.mock('@material-ui/core', () => ({
  ...jest.requireActual('@material-ui/core'),
  SwipeableDrawer: jest.fn(() => <div>HELLOOOOOO</div>), // component
}));
// how to clear this? .mockClear() not defined?
---
// 2. mocked() util
import { mocked } from 'ts-jest/utils';
// mocked componenta je fja
mocked(MyDrawer).mockImplementation(() => <div>mocked: drawer</div>); // component
------
// example 5 - ok
__mocks__ - folder specijalan za jest
// ovo ce da uhvati prostiju verziju iz __mocks__ foldera
// src/VeryComplex/DeepFolder/DeeperFolder/__mocks__/VeryComplex.tsx
jest.mock('../../VeryComplex/DeepFolder/DeeperFolder/VeryComplex');
---
__mocks__ - odmah pored komponente, ili u root folder za module iz node_modules // ok
-----------------------
// 6. Mock HTTP calls using Fetch or Axios - Mock Service Worker - React.js Testing Tutorial 
// mock Axios with MSW - mock service worker
// dobra lekcija, pogotovo kod, jedino nested describe() blokovi
// https://github.com/bmvantunes/youtube-react-testing-video6-mock-http-calls-with-msw-react-testing-library
test fetch data, post data, loading and error states
-----
msw - mock real http, a ne fje u axios ili fetch
----
ima beforeAll() i beforeEach() // zapazi
----
serach type debounce bug, solution cancel requests or loading: number
----
// src/components/PhotoList.spec.tsx je test sa mock axios i fetch
// src/components/Good.PhotoList.spec.tsx je sa msw
----
// msw GET
// <req, res> types
//  (req, res, ctx) => {} callback response kao express middleware
rest.get<DefaultRequestBody, Photo[]>('/api/photos', (req, res, ctx) => {
  const name = req.url.searchParams.get('name') || 'Unknown'; // ?name=Bruno

  // fake response
  return res(
    //   ctx.delay(100),
    ctx.json([
      {
        id: 1,
        thumbnailUrl: '/photo1.png',
        title: name + ': Hello World',
        favourite: false,
      },
    ])
  );
})
---
// node server, a ne browser service worker
import { setupServer } from 'msw/node';
const server = setupServer()

beforeAll(() => server.listen()); // all idu van describe
afterAll(() => server.close()); // pali gasi server
afterEach(() => server.resetHandlers()); // handler je middleware (req, res, ctx) => {} fja koja vraca response

describe('after application fully loads', () => {
  beforeEach(async () => {}) // each ide unutar describe
  // ...
}
----
// msw loading spinner, query params
----
// msw error 500
// kod je ok, ali nije ga bas objasnio
import { rest } from 'msw';
describe('when clicking in "Refresh" Button and server returns error', () => {
  beforeEach(async () => {
    server.use( // zapazi use() za handlere - middleware
      rest.get<DefaultRequestBody, { message: string }>(
        ...
    );
    user.click(screen.getByText('Refresh'));
    await waitForElementToBeRemoved(() => screen.getByText('Loading...'));
  });

  it('renders the error keeping the old data', () => {
    expect(screen.getByText('Sorry Something happened!')).toBeInTheDocument();
  });
});
----
// msw POST
// handleri su skoro isti kao na backendu samo vracaju hardcodirane objekte
// opet ima nested describe, izbeci to
describe('after application fully loads', () => {
  // cekaj loader da se makne
  beforeEach(async () => {
    render(<PhotosList />);
    await waitForElementToBeRemoved(() => screen.getByText('Loading...'));
  });

  describe('when clicking in "Add to Favourites" changes the button text', () => {
    // klikni i cekaj dugme da se makne
    beforeEach(async () => {
      user.click(screen.getByRole('button', { name: 'Add To Favourites' }));
      await waitForElementToBeRemoved(() =>
        screen.getByRole('button', { name: 'Add To Favourites' })
      );
    });

    it('renders "Remove from Favourites"', () => {
      // assert remove dugme pojavilo, add dugme nestalo
      expect(
        screen.getByRole('button', { name: 'Remove from Favourites' })
      ).toBeInTheDocument();
      expect(
        screen.queryByRole('button', { name: 'Add to Favourites' })
      ).not.toBeInTheDocument();
    });
  });
});
// posatavio http interceptor, komponenta ce sama da zove http
// ne treba server u testu da prosledjuje // ok, zapazi
const server = setupServer(
  // POST
  rest.post<Photo, Photo>('/api/favourite', (req, res, ctx) => {
    const photo = req.body;
    return res(
      ctx.delay(200),
      ctx.json({ ...photo, favourite: !photo.favourite })
    );
  }),

----
// zamenio axios sa fetch, testovi i dalje rade, ok
----------------------------
// 7. React Hooks SWR: Test components that useSWR - Mock Service Worker - React.js Testing Tutorial
// SWR and React Query cache
// samo clear swr cache, ostalo isto, nema react query
// https://github.com/bmvantunes/youtube-react-testing-video7-mock-swr-with-msw

// disable swr cache for tests
const AllTheProviders: FC = ({ children }) => {
  return (
    // here
    // dedupingInterval - run every call, default 10 sec
    // provider - clear cache
    <MySwrConfig swrConfig={{ dedupingInterval: 0, provider: () => new Map() }}>
      {children}
    </MySwrConfig>
  );
};
// reusable render, rtl wrapper option
const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, 'wrapper'>
) => render(ui, { wrapper: AllTheProviders, ...options });
----------
// intercept http with fake server
const server = setupServer(
  rest.get<DefaultRequestBody, string[]>(
// start server
beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());
-----
// slucajevi: germany, france - success, italy error 500, empty array
-------------------
// Testing React Forms - React Testing Library - React.js Testing Tutorial 
// 8. https://github.com/bmvantunes/youtube-react-testing-video8-forms-react-testing-library
// multistep form, test happy path, validation messages
// form fields hvatas sa getByRole()
function getFirstName() {
  return screen.getByRole('textbox', { name: /first name/i });
}
// tasting-playground.com - koji rtl selector da upotrebis
----
// findBy ceka dok se ne pojavi dugme u sledeci step forme
function findMoney() {
  return screen.findByRole('spinbutton', { name: /All the money I have/i });
}
-----------
// happy path - najvazniji test
// test happy path, multistep form, ok
it('onSubmit is called when all fields pass validation', async () => {
  user.type(getFirstName(), 'Bruno');
  selectJobSituation('Full-Time');
  user.type(getCity(), 'Vila Real');
  user.click(getMillionaireCheckbox());
  clickNextButton();

  // 2nd step
  user.type(await findMoney(), '1000000');
  clickNextButton();

  // 3rd step
  user.type(await findDescription(), 'hello');
  clickSubmitButton();

  // assert onSubmit args and times
  await waitFor(() => {
    expect(onSubmit).toHaveBeenCalledWith({
      city: 'Vila Real',
      description: 'hello',
      ...
    });
  });

  expect(onSubmit).toHaveBeenCalledTimes(1);
});
----
// test error validation messages, ok
it('has 3 required fields on first step', async () => {
  clickNextButton();

  // dovoljno da sacekas error message na prvom polju
  // validation in formik is async, za react-hook-form ne treba wait, sync je, rekao u videu
  await waitFor(() => {
    expect(getFirstName()).toHaveErrorMessage('Your First Name is Required');
  });

  // assert other error messages, vec sacekane
  expect(getCity()).toHaveErrorMessage('city is a required field');
  expect(getSelectJobSituation()).toHaveErrorMessage(
    'You need to select your job situation'
  );
});
------
// aria for toHaveErrorMessage
// https://github.com/testing-library/jest-dom#tohaveerrormessage
<input
  aria-errormessage="msgID"
  aria-invalid="true"
/>
<span id="msgID" aria-live="assertive" style="visibility:visible">err msg</span>
-------
// test validation min 5 chars, max 9 chars, trivial, samo type
-------
// cross step validation, isto ko happy path, nista novo
povezana polja u razlicitim steps, logika
----------------------
// 9. Unit Testing Next.js Router - useRouter - React.js Testing Tutorial 
// https://github.com/bmvantunes/youtube-react-testing-video9-nextjs-router
// mock router and wrap with context, test <Link />, test router.push()
useRouter() uses context
---
// mock router
export function createMockRouter(router: Partial<NextRouter>): NextRouter {
  return {
    basePath: '',
    query: {},
    back: jest.fn(),
    ...
    ...router,
  };
---
// wrap component with context and pass fake router
import { createMockRouter } from '../test-utils/createMockRouter';
import { RouterContext } from 'next/dist/shared/lib/router-context';

it('renders h1 "Todo ID: 22"', () => {
  render(
    <RouterContext.Provider value={createMockRouter({ query: { id: '22' } })}>
      <BeautifulHeader />;
    </RouterContext.Provider>
  );
-----
// test <Link /> component
// pass args
render(
  <RouterContext.Provider
    value={createMockRouter({ query: { id: '33' }, pathname: 'bruno' })}
  >
    <BeautifulHeader />;
  </RouterContext.Provider>
);
// assert router passed args to <Link />
expect(screen.getByText('Contacts Page')).toHaveAttribute(
  'href',
  '/contacts?id=33&from=bruno'
);
--------
// test router.push(), ok, trivial

// create router object
const router = createMockRouter({
  query: { id: '44' },
  pathname: 'joao',
  basePath: 'antunes',
});

// pass it and render component
render(
  <RouterContext.Provider value={router}>
    <BeautifulHeader />;
  </RouterContext.Provider>
);
// click button with push()
fireEvent.click(screen.getByRole('button', { name: 'Some Action' }));

// assert push arg, obj to query string
expect(router.push).toHaveBeenCalledWith(
  '/contacts?id=44&from=joao&something=antunes'
);
--------
// prevent file.test.ts to create a route in /pages folder, error umesto 404
// nece mi trebati, da stavi test pored stranice
// https://nextjs.org/docs/api-reference/next.config.js/custom-page-extensions
// next.config.js
pageExtensions: ['page.tsx'],
-----------------------------
// Print_Glossary_A4.pdf kent dodds

Monkey Patching - override prop on a object/module
Spy - koliko puta je pozvana fja
Mock - spy + check argumenti i return value // bezveze
Stub - drop-in replacement that returns a value you predeﬁne // bezveze
Test Double - Stub/Spy/Mock // bezveze
--------
test - fja sa string imenom testa i callback sa expect
expect - prima argument i radi sa matcherom
matchers - toBe, toBeGreaterThan, toBeNull // proveravaju rezultat i bacaju exception
describe - test suites - group of tests that are related
---------
beforeEach, beforeAll
--------------------------
describe() blokovi (i nested) - samo grupa ili kontekst slicnih testova koji npr dele beforeEach()
------
import { render } from '@testing-library/react';
render() je wrapper oko ReactDOM.render()
-------------------------
// testing React Query
// https://tkdodo.eu/blog/testing-react-query
// waitFor() pokusava do timeouta ili successa i blokira naredbe ispod
// wait until the query has transitioned to success state
// na svakih 50ms do 1000ms timeouta, i to je to
await waitFor(() => result.current.isSuccess)
expect(result.current.data).toBeDefined()
---------
// https://github.dev/TkDodo/testing-react-query
// 1. for hooks
export function createWrapper() {
  const testQueryClient = createTestQueryClient()
  return ({ children }: {children: React.ReactNode}) => (
      <QueryClientProvider client={testQueryClient}>{children}</QueryClientProvider>
  )
}
// usage
const { result, waitFor } = renderHook(() => useRepoData(), {
  wrapper: createWrapper()
})
----
// 2. for components
// just support both render and rerender
export function renderWithClient(ui: React.ReactElement) {
  const testQueryClient = createTestQueryClient()
  const { rerender, ...result } = render(
      <QueryClientProvider client={testQueryClient}>{ui}</QueryClientProvider>
  )
  return {
      ...result,
      rerender: (rerenderUi: React.ReactElement) =>
          rerender(
              <QueryClientProvider client={testQueryClient}>{rerenderUi}</QueryClientProvider>
          ),
  }
}
// usage
const result = renderWithClient(<Example />)
-----
// simpler way
const ChakraRenderer = ({children}) => {
  return (
      <ThemeProvider>
          <ColorModeProvider value="dark">{children}</ColorModeProvider>
      </ThemeProvider>
  );
};

const customRender = (ui, options) =>
  render(ui, {
      wrapper: ChakraRenderer,
      ...options
  });
----------
// pass arguments (props) to wrapper
// https://github.com/testing-library/react-testing-library/issues/780#issuecomment-687525893
const customRender = (ui, options) =>
  render(ui, { wrapper: props => <AllTheProviders {...props} {...options.wrapperProps} />, ...options })
-----------------
pisanje testova - prosto i dosadno, zapamti za posao, prostije od obicne logike
---------------------
// screen.getBy je bolje nego const {getBy} = render() jer ne moras globalne vars u beforeEach
beforeEach(async () => {
  customRender(<PostView />, { wrapperProps: { router } });
});
//
screen.getByRole(...)
--------------------------
// mock single property/fn from module import, vazno
// https://stackoverflow.com/questions/59312671/mock-only-one-function-from-module-but-leave-rest-with-original-functionality
// cast type 
// https://stackoverflow.com/a/60007123/4383275
// jest.requireActual(...) je kljucno

// import
import { signIn, ClientSafeProvider } from 'next-auth/react';

// set
jest.mock('next-auth/react', () => ({
  ...(jest.requireActual('next-auth/react') as {}), // cast just for spread
  signIn: jest.fn().mockReturnValue({ ok: false }),
}));
const mockedSignIn = jest.mocked(signIn, true); // just for type .mockClear();

// assert
await waitFor(() => expect(mockedSignIn).toHaveBeenCalledWith(providers.facebook.id));
// cleanup mock
mockedSignIn.mockClear();
------
// dont use, deprecated warning
import { mocked } from 'ts-jest/utils';
const mockedDataGrid = mocked(DataGrid);
// use this to cast to type
jest.mocked(signIn, true);
-------------
// drugi nacin sa spyOn() ali moras svaki put implem i return val da postavljas...?
// inace zove stvarnu implementaciju
const utils = require('./utilities.js');
jest.spyOn(utils, 'speak').mockImplementation(() => jest.fn());
----
// https://www.carlrippon.com/how-to-mock-a-function-in-jest-with-typescript/
// jest.spyOn(object, methodName);
import * as data from "./data"; // local import
const mock = jest.spyOn(data, "getCharacter").mockResolvedValue("Bob"); // za promise
mock.mockRestore(); // oslobodi fju na kraju testa
----------------
// mock one value export (not a function) from a module
import { themes } from 'lib-client/constants';

// mock themes array
// must be global scope, important
jest.mock('lib-client/constants', () => ({
  ...(jest.requireActual('lib-client/constants') as {}),
  themes: ['theme-first', 'theme-second', 'theme-third'], // must be here, important, poenta
}));
const mockedThemes = jest.mocked(themes, true);
----------------
jest.mock('../api') // mockuje ceo import module
----------
// mockovana fja mora da ima isti signature
// da prima args istog tipa i da vraca isti tip // logicno
-------
// jest.fn() - i drugi mockovi menjaju - override-uju stvarnu funkcionalnost u tvom kodu ili biblioteci
// za izolaciju i za assert rezultata, zapamti to, nemoj da zaboravis
--------------------------
--------------------------
// ACT - UKUPNA POENTA: u testu linija koja uzrokuje menjanje statea u komponenti se mota u act()
// ili si zavrsio test pre vremena
---
// update inside a test was not wrapped in act
// znaci da u testu ima race i neki async state update u komponenti
// u testu je nesto preko reda i nije awaitovano, ako ima vise await
// motanje u act nije resenje uglavnom, nego da sve se klikce, kuca i hvata po redu
// komponenta i test nisu u sync
// npr nema veze sa Link, nego u testu nije awaitovano userEvent.type()
console.error Warning: An update to Link inside a test was not wrapped in act(...).
When testing, code that causes React state updates should be wrapped into act(...):
act(() => {
  /* fire events that update state */
});
/* assert on the output */
This ensures that youre testing the behavior the user would see in the browser. Learn more at https://reactjs.org/link/wrap-tests-with-act
at Link (/home/username/Desktop/nextjs-prisma-boilerplate/node_modules/next/client/link.tsx:129:19)
----
act() ceka u testu da se state promena - rerender u komponenti reflektuje u testu
----
// pogledaj ovo nekad, vrlo cesta greska u testovima
// https://kentcdodds.com/blog/fix-the-not-wrapped-in-act-warning
// prvi klip, iz lekcija
// tekst u tutorijalu je kod iz videa, lako
// poenta resio sa await waitForElementToBeRemoved()
// sacekao u testu do kraja rerendera komponente
u class_ component ga nema, samo u function_ components
desio se state update u komponenti i posle zavrsetka testa
treba da produzis trajanje testa, da sacekas jos nesto
// 2. resio sa ovim na kraju
await waitForElementToBeRemoved(() => screen.getByText(/saving.../i));
----
// 1. prvo resenje
const promise = Promise.resolve();
const handleUpdateUsername = jest.fn(() => promise); // mock form submit fje
render(<UsernameForm updateUsername={handleUpdateUsername} />) // render FORM
   ...
expect(handleUpdateUsername).toHaveBeenCalledWith(fakeUsername)
await act(() => promise); // resenje za act error, naterao test da saceka reactov rerender
---
// formu testira sa jest.fn() na onSubmit forme, nema http call za msw - zapazi
-------------------
// video za hooks, ok
// poenta - pozivi fja koji trigeruju state change (setState) moraju biti umotani u act()
import {renderHook, act} from '@testing-library/react-hooks'

test('increment and decrement updates the count', () => {
  const {result} = renderHook(() => useCount());
  expect(result.current.count).toBe(0);

  // resenje, increment() triggeruje state change u useCount() hook-u
  act(() => result.current.increment()); 
  expect(result.current.count).toBe(1);

  act(() => result.current.decrement()); // resenje
  expect(result.current.count).toBe(0);
})
-------------------
// video za hook (komponenta je zapravo) sa useImperativeHandle i ref
// isto samo fje koje menjaju state su na ref, trivial
// i one se motaju u act()
function ImperativeCounter(props, ref) { // komponenta
  const [count, setCount] = React.useState(0)

  React.useImperativeHandle(ref, () => ({
    increment: () => setCount(c => c + 1),
    decrement: () => setCount(c => c - 1),
  }))
  return <div>The count is: {count}</div> // jsx
}
ImperativeCounter = React.forwardRef(ImperativeCounter)
----
// test
import {render, screen, act} from '@testing-library/react'
import ImperativeCounter from '../imperative-counter'

test('can call imperative methods on counter component', () => {
  const counterRef = React.createRef()
  render(<ImperativeCounter ref={counterRef} />) // render component

  expect(screen.getByText('The count is: 0')).toBeInTheDocument()

  act(() => counterRef.current.increment()) // eto, trivial, isto
  expect(screen.getByText('The count is: 1')).toBeInTheDocument()

  act(() => counterRef.current.decrement()) // eto
  expect(screen.getByText('The count is: 0')).toBeInTheDocument()
})
--------------------
// video sa Jest fake timers
// linija u testu koja uzrokuje menjanje statea u komponenti se mota u act()
function tick() {
  setState({status: 'pending'}) // ova linija menja state
  ...
}
const id = setInterval(tick, 1000)
---
// test
// ova linija u testu zove taj state update, pa umotana u act
act(() => jest.advanceTimersByTime(1000)); 
--------------
// dobar prost tutorijal za act error
// https://davidwcai.medium.com/react-testing-library-and-the-not-wrapped-in-act-errors-491a5629193b
--------------------
// za testove razmisljaj u smislu ulaz, izlaz, stanja, prenosna funkcija - automati
// arrange componentu - pozoves je slicno kao u kodu ili u storybook
// ako znas da napises logiku za komponentu znaces i test, jer je prostije, samo test najvaznije logike
----------
// forms testing
// each form should be separate component, easier to test
// onSubmit must be a prop, to be mocked in test?
// testiranje forme - happy path onSubmit mock, i validation messages, dosta lako
--------
// mocks scope i ciscenje
// must be declared in describe scope to be cleaned in afterEach()
const onSubmit = jest.fn();
---
// ones with jest.clearAllMocks(); can be defined in local test scope
---
afterEach(() => {
  // one
  onSubmit.mockClear();
  // all
  jest.clearAllMocks();
});
---------------
// Test components that use React.lazy + Suspense - Kent C. Dodds // outdated, trivijalno findByText
// https://www.youtube.com/watch?v=lfb5jvHq9c4
// https://github.com/kentcdodds/react-testing-library-examples/blob/main/src/__tests__/react-lazy-and-suspense.js
---
// 1. nacin, ne postoji vise ova fja
await wait() // ceka sledeci tick event loop-a, sta god to radilo // pooling 40ms
// 2. nacin
const lazyElement = await waitForElement(() => screen.getByText(/i am lazy/i)) // ceka event, mutation observer...
expect(lazyElement).toBeInTheDocument()
// 3. nacin
// trivijalan klip, to je zapravo ovo, jos nije postojalo findByText
const lazyElement = await screen.findByText(/i am lazy/i)
------
// koja ne koristi Suspense
// komponentu koju testiras motas sa Suspense i assert/wait fallback text i to je to
<React.Suspense fallback="loading...">
<Main />
</React.Suspense>
--------
// koja koristi Suspense - nista, samo render // ignorise loading...???
render(<App />)
----------------
// waitFor - ceka do timeouta, da ne baci exception za to vreme
// nema veze sa true
// Wait until the callback does not throw an error. In this case, that means
// it'll wait until the mock function has been called once.
await waitFor(() => expect(mockAPI).toHaveBeenCalledTimes(1))
-------------
// istrazi malo
exception u promise je rejected promise, success je resolved promise
-----------------
// mock console.log(), error(), warn()
const mockedConsoleError = jest.spyOn(console, 'error').mockImplementation();
mockedConsoleError.mockRestore(); // clean
-----------------
testing hooks je samo motanje u komponentu pa testiranje komponente, ne zaboravi
-----------------------
// fix jest act warning
// if expect() isn't for element selected with `await findBy()` you need to retry with 
// `await waitFor()` to assert state update on UI

await userEvent.click(createButton);

await waitFor(() =>
  expect(titleInput).toHaveErrorMessage(/must contain at least 6 character/i)
);
await waitFor(() =>
  expect(contentTextArea).toHaveErrorMessage(/must contain at least 6 character/i)
);
----------------------------------
----------------------------------
// code coverage
// kent dodds
propustio - od pocetka pokreci ovo da vidis koji deo koda je pokriven // zapazi
sta treba da se testira a sta je vec 5x pokriveno // TACNO TI POKAZUJE
ZAPRAVO COVERAGE REPORT TI POKAZUJE STA TREBA DA SE TESTIRA
---
ima smisla samo donekle sto pokrije liniju, ali u kom redosledu i kombinacije svih linija
-----
ima smisla samo za unit i integration (jest), a ne za e2e jer on ne zna kako izgleda kod // zapazi
------
sve se u detalje vidi koja linija, grana... koliko puta prodjena
(statements, grane, fje, linije)
------
jest kreira html obojeni report /coverage folder u root
-----------
// jest.config.js
collectCoverageFrom[/**/*.ts] // po defaultu ignorise *.test.ts test fajlove
-----------
set coverage/ folder u .gitignore
--coverage // samo taj flag na postojece komande i generise coverage/ folder i to je cela nauka
jest --coverage // skripta se pokrece samo sa svim testovima jednom
--------------
// module je za import staze, nevezano za coverage
moduleDirectories: ['<rootDir>', 'node_modules'], // fixes baseUrl absolute imports
----------------
// racuna se za sav kod - jest run konfiguraciju, a ne posebno za --project, ima smisla
// https://github.com/facebook/jest/issues/4255
// https://stackoverflow.com/questions/64281758/jest-coveragedirectory-configuration-for-project-inside-monorepo
coverage must be defined once in root `jest.config.js` (where are `projects`)**, 
and not in `jest.client.js` and `jest.server.js` (jest coverage projects monorepo)
---------------------
// increase timeout in findBy()
const title = await screen.findByRole(
  'heading',
  {
    name: RegExp(`${fakeUser.name}`, 'i'),
  },
  { timeout: 2000 } // default 1000, failed in GA
);
expect(title).toBeInTheDocument();



