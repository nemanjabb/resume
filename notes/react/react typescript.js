
// React TypeScript Tutorial for Beginners
// Codevolution 
// https://www.youtube.com/playlist?list=PLC3y8-rFHvwi1AXijGTKM0BKtHzVC-LSK
// https://github.com/gopinav/React-TypeScript-Tutorials

// 1 - Introduction
// 2 - Getting Started
nista

// 3 - Typing Props
// 4 - Basic Props
props type, trivial

// 5 - Advanced Props
union, optional, children, trivial

// 6 - Event Props
click i change event types, trivial

// 7 - Style Props
css type, trivial
u react 17 ne treba import react

// 8 - Prop Types and Tips
destructure props, export type, trivial

// 9 - useState Hook
useState type inference, trivial

// 10 - useState Future Value
useState<futureValType>(initialValue)
initial value i future value nisu isti type, zato mora <> // ok

// 11 - useState Type Assertion
as type je type assertion, zasto ne cast?

// 12 - useReducer Hook
kad nextState zavisi od prevState ide useReducer umesto useState, ok
type za state i action reducer fje
samo dodefinises tipove koje typescript ne moze da zna, ostalo pustis da infere, i code nije cluttered // dobra poenta, cesto
napises javascript pa samo sto je podvuceno ispravis i to je to

// 13 - useReducer Strict Action Types
type CounterAction = UpdateAction | ResetAction // discriminated union for reducers, nije lose

// 14 - useContext Hook
svi tipovi su infered od createContext(initialValue), ne treba nikakav typescript
lep, jednostavan primer kako treba context inace

// 15 - useContext Future Value
futureValue createContext<>({} as User)
nije bas dobro jer user moze da bude {}, null i user

// 16 - useRef Hook
useRef ima 2 slucaja:
1. read only ref na DOM element
2. mutable class variabla

null! - non null assertion - cast u non null // za ?.focus()
HTMLInputElement za type never

1.
const inputRef = useRef<HTMLInputElement>(null!)
inputRef.current.focus()
<input type='text' ref={inputRef} />

2.
const interValRef = useRef<number | null>(null)
if (interValRef.current) window.clearInterval(interValRef.current) // if not undefined, type cita if

// 17 - Class Component
export class Counter extends Component<CounterProps, CounterState> {
Component<{}, CounterState> // ako nema props

// 18 - Component Prop
prosledi komponentu kao prop
Component: React.ComponentType<ProfileProps> // prosledjena komponenta
export const Private = ({ isLoggedIn, Component }: PrivateProps) => {

// 19 - Generic Props
generics
type ListProps<T> = {
    items: T[]
    onClick: (value: T) => void
}
// ovde def za stvarni argument tip, za T, ispred komponente zanimljivo
// extends je ogranicenje, dodela tipa prakticno
export const List = <T extends { id: number }>({ items, onClick }: ListProps<T>) {...

// 20 - Restricting Props
3 props, ako 1 prosledjen druga 2 ne smeju da budu postavljeni
<RandomNumber value={10} isPositive isNegative isZero />

never tip, ne sme da bude postavljen, da ima vrednost

// zajednicki
type RandomNumberType = {
    value: number
}

type PositiveNumber = RandomNumberType & {
    isPositive: boolean
    isNegative?: never // ovde never
    isZero?: never
}
...

type RandomNumberProps = PositiveNumber | NegativeNumber | Zero;

// 21 - Template Literals and Exclude
ok, 2 stvari
type HorizontalPosition = 'left' | 'center' | 'right';
type VerticalPosition = 'top' | 'center' | 'bottom';

type ToastProps = {
  position: `${HorizontalPosition}-${VerticalPosition}`; `// templ literal radi i nad tipovima (koji su string) kao nad vrednostima
}

// Exclude<ulazni tip, sta da izbaci>
// Include je union jednostavno, |
type ToastProps = {
  position:
    | Exclude<`${HorizontalPosition}-${VerticalPosition}`, 'center-center'> `// zameni center-center sa center
    | 'center'
}

// 22 - Wrapping HTML Elements
extend props od native html element i override children type

// extended custom button
type ButtonProps = {
    variant: 'primary' | 'secondary' // variant +
} & React.ComponentProps<'button'> // props od html button
  
export const CustomButton = ({ variant, children, ...rest }: ButtonProps) => { // children mora odvojeno

& je union prakticno, bzvz
---
// restrict children to string
type ButtonProps = {
    variant: 'primary' | 'secondary'
    children: string // da ostane samo ovaj
} & Omit<React.ComponentProps<'button'>, 'children'> // izbaci children iz html button

// sklanjanje, to to
Omit za sklanjanje kljuceva iz objekta, Exclude za sklanjanje celog tipa iz uniona
  
// 23 - Extracting a Components Prop Types
izvuci prop type od komponente, ok
const CustomComponent = (props: React.ComponentProps<typeof Greet>) => { // od Greet

// 24 - Polymorphic Components
gimnastika sa html tipovima i native html attrs
polymorphic koja vraca h2, p, div itd

React.ElementType // je h2, div, p... ime taga
---

type TextOwnProps<E extends React.ElementType> = { // dodela tipa, tag names
    size?: 'sm' | 'md' | 'lg'
    color?: 'primary' | 'secondary'
    children: React.ReactNode // vs ReactElement?
    as?: E // glavni
  }
  
  // isti React.ComponentProps<E> kao za obicnu komponentu, getPropsFromComponent helper
  type TextProps<E extends React.ElementType> = TextOwnProps<E> &
    Omit<React.ComponentProps<E>, keyof TextOwnProps<E>> // izbaci preklapanja od TextOwnProps
  
  export const Text = <E extends React.ElementType = 'div'>({ // ovde se prosledjuje stvarni param tip, ok fora
    size,
    color,
    children,
    as
  }: TextProps<E>) => {
    const Component = as || 'div' // komponenta od var, cak i od string, ok
    return (
      <Component className={`class-with-${size}-${color}``}>{children}</Component>
    )
  }

// 25 - Wrapping up
rezime sadrzaja








