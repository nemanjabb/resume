// https://react.dev/blog/2024/04/25/react-19
-------
//  Everything You Need to Know About React 19 - CoderOne
// https://www.youtube.com/watch?v=YInQ137_J3Y
useTransition hook
useStateAction hook
useFormStatus hook
useOptimistic hook
New API: use()
Server Components & Actions
ref prop without forwardRef
Form Actions
Context
Better Error Reporting
Document Metadata
Custom Elements
----------------
// Every React 19 Feature Explained in 8 Minutes - Code Bootcamp
// https://www.youtube.com/watch?v=2NPIYnY3ilo
React compiler -> No memoization hooks
No forwardRef
use() hook, Fetch data with useEffect/use, Use context with useContext/use
Directives 'use client', 'use server'
Actions
Client Actions
useFormStatus() hook
useFormState() hook
useOptimistic() hook
------------------
react uzima nove feature iz postojecih biblioteka
