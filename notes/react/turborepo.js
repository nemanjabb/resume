// monorepo - turborepo
// yarn tutorial
// https://blog.logrocket.com/build-monorepo-next-js/
monorepo - vise projekata u jednom git repozitorijumu
poenta: reuse i 1. libs (packages) i 2. config (lint, prettier, typescript)
---------
prednosti:
1. projekti reuse code (libs) and configurations (lint, format)
2. atomic commits - moze da se pokrene ceo app
3. consistency - ceo app isti StyleSheetList, lint, test, itd
------------
primeri: Turborepo, Nx, Bazel, Lerna
---------
workspaces - feature of yarn, npm, pnpm // svih znaci
---------
turborepo features:
Incremental builds - samo changed rebuild, 
Parallel execution - paketi se builduju paralelno
Remote caching - local i ci-cd, save time
Dependency graph visualization - tool
------------
// tutorial je sa yarn
workspaces je cvor u package,json
{
  "workspaces": [
    "apps/*", // two next.js apps
    "packages/config/*", // config za lint i format, zapazi
    "packages/shared/*" // shared libs
  ],
  "packageManager": "yarn@1.22.17"
}
-----------
tasks - ne moze yarn // kao gulp...
-----------
yarn add turborepo -DW
turbo.json u root
----------
// turbo.json
{
    "pipeline": { // polje taskova
    "dev": { // odnosi se na script u package.json u root, workspace package.json
      "cache": false
    }
  }
}
------
// package.json u root
{
  "scripts": {
    "dev": "turbo run dev --parallel" // pokrece yarn dev u app1 i app2
  }
}
------------
// create eslint shared package
packages/config/eslint-config-custom  
// package.json
{
  "name": "eslint-config-custom",
  "version": "1.0.0",
  "main": "index.js", // entry point paketa
}    
// index.js
module.exports = {
  extends: ["next", "turbo", "prettier"],
};
--------
// import shared eslint paketa u apps
{
  "devDependencies": {
   "eslint-config-custom": "*"
  }    
}
// eslintrc.json
{
  "root": true,
  // Tells ESLint to use the "eslint-config-custom" package
  "extends": ["custom"] // zapazi custom se mapira na eslint-config-custom
}
yarn install prebaci lokalan shared paket u node_modules, ima ga folder tamo
---------------
// package.json u app1 i app2
{
  "lint": "eslint .",
  "format": "eslint --fix --ext .js,.jsx ."
}
// tasks in turbo.json
{
  "lint": { // gadja scripts u app1 i app2, eto, zapazi
    "outputs": [] // globs of files to be cached
  },
  "format": {
    "outputs": []
  }  
}
// package.json u root
{
  "lint": "turbo run lint", // script da zove tasks u turbo.json
  "format": "turbo run format"  
}
----------------
workspace package.json -> turbo.json -> app1 package.json
---------------
// create shared component library
packages/shared/ui
----------
app1, app2, eslint, ui... su workspaces
--------
export components from index.js // package entry point
---------
// import dependancy u app1, app2
{
  "dependencies": {
    "ui": "*" // lokalni paketi idu sa *
  }
}
yarn install, da ga prebaci u node_modules
------
// Next.js ne moze da koristi lokalne pakete default
// mora ovaj za transpile lokal packages u app1, app2
yarn workspace app1 add -D next-transpile-modules
// next.config.js
const withTM = require("next-transpile-modules")(["ui"]);
module.exports = withTM({
  reactStrictMode: true,
  swcMinify: true,
});
kad se menja next.config.js mora restart next.js server
-----------------------
//  Monorepos - How the Pros Scale Huge Software Projects // Turborepo vs Nx - Fireship
// https://www.youtube.com/watch?v=9iU_IE6vnJ8
workspaces je feature yarna i npm
dedupes packages u node_modules, samo jednom
npm run test --workspace=a
------
turborepo pipeline - dependsOn, redosled taskova, lint, build, test, ^ - dependencies, kao docker
tasks u pipeline se poklapaju sa scripts u package.json
----
apps - apps
packages - libs
----
npx create-turbo@latest my-app // pita da li yarn ili pnpm, ime foldera
----
lokalni paketi iz workspace su ukljuceni sa "ui": "*",
----
pnpm ima simlinks za duple pakete, zato je brzi
---------------------------
//  Getting started with Turborepo - Hamed Bahram
// https://www.youtube.com/watch?v=mxLLIwZ93nY
// package.json
name se poklapa sa dependencies
main je sta exportuje package, entry point
files - exportovani fajlovi za config npr
config packages idu u devDependencies
------
build tasks u pipeline imaju output folders
-----
dependencies u root package.json se vide u svim?
install dependency samo u neki workspace udjes u taj folder ili // mora yarn install
pnpm --filter web add -D tailwindcss // u web
yarn -w add -D tailwindcss // install u root
-----
shared - ui, typescript, eslint, tailwind
-----
turbo.json moze biti fajl ili key u package.json
----
yarn dev // runs dev scripts paralelno u all workspaces
-----------
nema previse, nije previse tesko
------------------------------
// Turborepo Tutorial | Part 1 - Typescript, Eslint, Tailwind, Husky shared config setup in a Monorepo - Leo Roese
// https://www.youtube.com/watch?v=YQLw5kJ1yrQ
// https://github.com/leoroese/turborepo-tutorial
// typescript, prettier, eslint, tailwind - shared configs
workspace name and version u package.json
-------
// install u workspace
yarn workspace app1 add axios
// install u root
yarn add -D -W husky lint-staged
-----
dependsOn: "^build" - ^ ceka da se zavrsi install, dependsOn docker, redosled
-----
apps folder - apps
packages folder - sharedconfigurations and components
----
yarn.lock samo iz root
----
prettierrc.json ide u root, i instaliran u root
-----
// posle promene paketa, ovo je prod
yarn install && yarn build
// za dev, ima concurrent script
yarn dev
-----
package.json.files - polje fajlova koje je u projektu, ...?
-----------
// tailwind
install next-transpile-modules
// next.config.js
const withTM = require('next-transpile-modules')(['ui'])
tailwind.config.js u svaki app, content paths
------------
// husky
yarn prepare - preinstall script
-----------------
// Turborepo Tutorial | Part 2 - Testing, Deployment, and remote cacheing - Leo Roese
// https://www.youtube.com/watch?v=ns53hT4yZl0
test next.js, test node.js
deploy vercel trivial, github actions build and test, trivial
turbo remote cache, vercel nalog
----------------------
"name": "@apps/blog", // name u package.json je vazno za gadjanje yarn build
@apps je namespace_ paketa, nije obavezan
-------------------------
// peerDependencies, eto jasno
peerDependencies - dependencies that the package expects to be provided by the consuming project
---------
// package.json
// default export of the package
"exports": {
  ".": "./tailwind.config.ts"
},
----
// package.json
// files
"files": [
  "main.js"
],
files that should be included when the package is published to the npm registry
----
"files": means just exports few .js files, doesnt run them // to, to
-----------------------
// delete all node_modules bellow current dir
// print first
find . -name 'node_modules' -type d -prune -print
// delete
find . -name 'node_modules' -type d -prune -exec rm -rf '{}' +
-----------------
// package.json main vs bin
main is entry point of package_
called when imported as module
------
bin is to make shell command from package_
when someone installs your package globally, the "format" command will be available in their command-line interface_
-------------------
// separate .gitignore in each package, level, root is just for root
every .gitignore manages only its level (package) or root
no /**/ osim za node_modules i turbo // ok
----------------
// tsconfig.json base i extends
moze i treba globes for include u tsconfig
"**/*.ts", "**/*.tsx", ...
---------
poenta: include, exclude, base, paths ne ide u base tsconfig.json // eto, zapazi
ponavlja se u svaki izvedeni
---------------
kako se instaliraju paketi iz root u packages/apps?
-------------------
da bi importovao fje iz paketa u monorepo moras da ga ukljucis u dependencies
"@package/shared": "*",
-----
// shared package package.json
"name": "@package/shared",
"type": "module",
"exports": "./index.ts",
----
// FORA, zapazi:
ne smes da kreiras direktni iports iz susednog paketa bez njegovog exports u package.json i include u dependencies 
inace nije standalone paket 
---------------------------
// monorepos
//  How to structure a JS/TS monorepo (From Zero to Turbo - Part 1) - Anthony Shew
// https://www.youtube.com/watch?v=TeOSuGRHq7k
workspaces, parallel, kesiranje
// How to handle dependencies in JS/TS monorepos (From Zero to Turbo - Part 2) - Anthony Shew
// https://www.youtube.com/watch?v=oHag57_zRs8
dobar pregled 
uporedjeni package manageri
peerDependencies - dependencies that the package expects to be provided by the consuming project, a moze da ih importuje // zapazi


