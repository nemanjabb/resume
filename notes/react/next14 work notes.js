--------------------
// disable cache next.js 14
export const dynamic = 'force-dynamic' // in page.tsx
cache: 'no-store', // in fetch
revalidateTag(“articles”) // call in api endpoint
// takodje obrisi .next folder
-----------
preview mode - draft mode - drugi token se salje sa fetch i vraca draft posts
da se vide kako izgledaju pre produkcije
next rebuilduje stranice na svaki request, kao dev 
---
// ovako se detektuje
import { draftMode } from "next/headers";
const { isEnabled } = draftMode(); // pa se prosledjuje u fetch, kaci banner
---
dva api endpointa /api/draft i /api.disable-draft 
-----
revalidation - disable cache, next.js ima nekoliko nivoa cache, obnovi
plus rebuild
/api/revalidate
revalidateTag('articles')
-----------------------
-----------------------
// 7. march, work notes
// server actions
async funkcija umesto api endpoint za formu
u client component server action mora imported from separate file 
---
// https://nehalist.io/react-hook-form-with-nextjs-server-actions/
forma mora u client component 'use client'
useFormStatus - from react-dom, form status information, loading (pending) state
----
react-hook-form onSubmit - FormValues type 
server action - FormData type
---
import { useFormState, useFormStatus } from "react-dom";
useFormStatus hook mora da bude unutar <form></form> taga
---
moze i useTransition za loading state 
const [pending, startTransaction] = useTransition();
----
zod-form-data paket za validiranje FormData tipa
ista schema se importuje i na clientu u react-hook-form 
----
// ima komponenta za greske na fields
import { ErrorMessage } from "@hookform/error-message";
---------
// za refetch - revalidate cache mora ovo: (ni page refresh ne radi)
revalidatePath('/'); ili revalidateTag('tag');
-----
useOptimistic hook from react 
-----
// na koju lokaciju da se stavi actions folder:
// znaci ne u app/api/...
src/app/actions/addTodo.ts
----------------------
// dynamic route params u server component i client component
// server component
async function MyServerComponent({ params }: { params: { slug: string } }) { ... }
// client component
import { useRouter } from 'next/router';
function MyClientComponent() {
    const router = useRouter();
    const { param } = router.query;
    ...
}
-----------------
// server actions and shared validation
// error:
Error: Failed to find Server Action "". This request might be from an older or newer deployment. 
Original error: Cannot read properties of undefined (reading 'workers')
// solution:
server action and shared validation must be defined OUTSIDE of /app folder
-----------
server actions imaju ogranicenje sta mogu da vrate, zbog serializacije
samo object literals, ne mogu klase, prouci, podseti se...?
------------
// redirect 404 on not found slug
import { notFound } from 'next/navigation';
if (params.id === '123') {
    notFound();
}
// moze i not-found.tsx fajl, zapazi
--------------
uvek dodaj generateStaticParams() for dynamic path /resources/[resourceSlug] // vazno, zaboravio
--------------------
// generateStaticParams
// https://nextjs.org/docs/app/building-your-application/routing/dynamic-routes
// sad je uvek folder
Dynamic Segment - [folderName], [id] or [slug]
// params prop u: 
layout, page, route, and generateMetadata functions
----
// glavno:
generateStaticParams - SSG // to
1. statically generate routes 
2. only at build time //to
// inace
on-demand at request time // SSR
---------
fetch u generateStaticParams with same url bice memoized
-------
// todo: generateMetadata ??, ovde je docs
// https://nextjs.org/docs/app/api-reference/functions/generate-metadata
----------
// refernece-api docs
// https://nextjs.org/docs/app/api-reference/functions/generate-static-params
// 404
zapravo SSG ili SSR kontrolise dynamicParams var export
export const dynamicParams = true // default, SSR za not found params
export const dynamicParams = false // SSG, 404 za not found params // to
-----
// Good to know subtitle, dobar
u dev - se poziva na svaki request, kao ssr // zapazi
u prod - samo on build 
na revalidate - se generateStaticParams NE poziva // zapazi
----
revalidation je zapravo ISR // zapazi
----
dynamic rendering je zapravo SSR, preimenovano u next 13
-----------------
// dobar refernece-api clanak, sve vazne vars za export
// https://nextjs.org/docs/app/api-reference/file-conventions/route-segment-config
// svi export params
dynamic	- 'auto' | 'force-dynamic' | 'error' | 'force-static' // SSG ili SSR
dynamicParams - true | false // za 404 u generateStaticParams
revalidate - false | 0 | number (in sec) // zanimljivo, period
fetchCache - advanced, nebitno
runtime	- 'nodejs' | 'edge' // ok
preferredRegion
maxDuration - request timeout
------------
//  Next.js 13 Crash Course #8 - generateStaticParams and its variations - Programming with Umair 
// https://www.youtube.com/watch?v=9goIlPIFNzE
proveri na next build u log da li je stranica SSG pregenerisana // zapazi
---------------
i za dinamicke rute vidi se sve stranice (sve rute sa slugs) koje su kreirane u build log, i stranice u .next/server/app 
-----------------------
// todo: caching, procitaj opet
// https://nextjs.org/docs/app/building-your-application/caching
// Next.js App Router Caching: Explained! - Vercel
// https://www.youtube.com/watch?v=VBlSe8tvg4U
unstable_cache() - kesiranje, iz next.js 
cache - iz react, deduping, memoizacija na serveru, jedan poziv ako su isti args  
----
memoizacija je uvek to - za iste args poziv vraca isti output, React.memo 
----
useOptimistic - iz react isto
-------------------
// generateStaticParams vraca SAMO route segments, ne ceo props (getStaticProps)
umesto getStaticProps fetchujes u page (ili layout) komponenti // ok...
const { isEnabled } = draftMode();
const article = await getArticle(params.slug, isEnabled);
if (!article)  notFound();
------------
// moze da se zove i u generateMetadata
const { isEnabled } = draftMode();
------------
// sad fetch radi isto sto i getStaticProps
{cache: 'force-cache'} // default
https://nextjs.org/docs/app/building-your-application/upgrading/app-router-migration#static-site-generation-getstaticprops
----------------
// contentful next.js revalidation
// contentful webhook
Note that Contentful will need a URL that is publicly accessible for the revalidation to succeed
https://github.com/localtunnel/localtunnel
npm install -g localtunnel
// installed
lt --port 3000
// npx
npx localtunnel --port 3002
dole link koji vrati ip, pa je ip password
global paketi se instaliraju sa npm, ne yarn
----------------
revalidatePath("/") se obicno zove u server actions, kao refetch react query 
---------------------
// disable cache - ukratko
export const dynamic = 'force-dynamic' // in page.tsx
cache: 'no-store', // for debugging // in fetch
---------------
import { redirect } from "next/navigation";
redirect(redirectRelativePath);
radi i relative path '../..' // ok
redirect baca exception, mora van try catch u finally
https://medium.com/@deadlyunicorn/how-to-solve-worarkound-next-navigation-redirect-error-inside-try-catch-nextjs13-500a5b5db89d
-------------------
// ignoring files and folders in Next.js from routing - colocating
// project organization menu
// https://nextjs.org/docs/app/building-your-application/routing/colocation
svi .ts fajlovi osim page.tsx i route.ts su ignorisani // zapazi
_ignored-folder, (ignored-path-segment)
----------
// routing uopsteno
https://nextjs.org/docs/app/building-your-application/routing#colocation
-------------------
// Next.js app router response 
return NextResponse.json() vs return new Response()
// https://nextjs.org/docs/app/api-reference/functions/next-response
// 204 must use 'new Response' syntax
return new Response(null, { status: 204 });
---------------
// get origin in api route
// https://nextjs.org/docs/app/api-reference/functions/next-request#nexturl
const origin = request.nextUrl.origin;
// logs http://localhost:3000
-------------------
// i CC se renderuju na serveru, samo hydrated na clientu
Client Components ('use client') are prerendered on the server and hydrated on the client // zapazi
// baca window undefined, mora ovo:
export function isBrowser(): boolean {
  return typeof window !== 'undefined';
}