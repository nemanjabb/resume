
/**
* CSS working notes.
*
*
*
*/

#id > prvo-direktno-dete-tag
#id bilo-koje-dete-tag
---------------
//ima poeni za prioritet inline, klasa, id itd
display:block !important;
---------------
float:left na divu lepi ulevo jedan za drugim u isti row, float:right zalepi za desnu ivicu, odnosi se na taj div
float:left ostaje svim elementima iza njega dok ne kazes clear:left ili both
--------------
da bi inline elementu postavio sirinu ili visinu mora inline-block
	display: inline-block; 
	height: 25px; 
--------------
prezvrljano u inspector - overajdovano, ili zakomentarisano
------------
blok elementi idu sami jedan ispod drugog ako im ne stavis float
-------------
display:inline-block; dodaje space izmedju elem mora <!-- --> izmedju
-------------
border-collapse: collapse; da se makne border sa <td>

------------------------
//scss
@mixin selektor {} je makro koji se poziva sa @include 
& je alias za parent
:hover selektor za event

.klasa1.klasa2 na istom elementu (bez blanko) (sa blanko descendant)
.blah { & + & { border: 1px solid red; } } --> .blah + .blah { border: 1px solid red; }
div + p svi p odmah iza divova
+ je Adjacent sibling combinator

CSS Combinators
descendant selector (space)
child selector (>)
adjacent sibling selector (+)
general sibling selector (~)

--------------------------
--------------------------
//Pluralsight - Modern Web Layout with Flexbox and CSS Grid

// 2 Grid Layout the Basics
tabele bez html strukture, 2 dimenzije, flex 1
grid container ili grid, grid items
grid lines, linije formiraju column axis - vertikalna, row axis - horizontalna osa
linije zajedno formiraju grid tracks - columns or rows, prostor izmedju 2 linije, zajedno grid cells
grid area - prostor izmedju 4 linije

// 3 Enabling Grid in Various Browsers
browseri enable, nista

// 4 Setting up a Grid
inline-grid - kontejner zauzima samo koliko itemi
grid-template-columns: width width ... sirina kolone
grid-template-rows: height height ... visina reda 
koliko kolona, redova i guttersa ti treba
grid-template-columns: 6em 1em 6em 1em ... 6 sirina kolone, 1 sirina guttersa
grid gutters su isto grid itemi
---
smestanje itema u kolone, redove
grid-column-start: 1, grid-column-end: 2 //horizontalno ih redja nadesno
grid-row-start, grid-row-end
span grid-column-start: 3, grid-column-end: 6
rows isto sve samo definise redove

treba ti 4 grid-column-start, grid-column-end, grid-row-start, grid-row-end
da bi explicitno pozicionirao item

// 5 Auto Grid Features
auto placement
grid-auto-flow: row; je default
1 2 3
4 5 6
grid-auto-flow: column; gore dole, gore dole
1 3 5
2 4 6
---
ako ne treba da se menja span tj velicina itema dovoljan je order: 1;
order: 0; je default, 1 je posle, -1 je pre, postavlja redosled
---
implicit grid-lines, itemi implicitno pozicionirani van definisanih, visak

// 6 Grid Shorthand
umesto grid-template-column, grid-template-row skraceno ide
grid: column column / row row;
---
umesto grid-column-start, grid-column-end, grid-row-start, grid-row-end ide
grid-column: column-start / column-end;
grid-row: row-start / row-end;
jos krace
grid-area: row-start / column-start / row-end / column-end; //zapazi redosled
---
span
grid-column: 2 / span 2;
grid-row: 2 / span 1;
---
repeat
grid-template-columns: repeat(4, 6em); // kao petlja, krace zapisano 4x6em

// 7 Naming Grid Lines  Grid Areas // slozeno
imena mesto brojeva, grid lines
grid-template-columns: [name1] [name2]...;
grid-template-rows: [name1] [name2]...;
ili skraceno u grid: [name1] ... / ...
---
named grid area
u selektor grid-area: title;
grid-template-areas: "title title title" //span x3 prvi row 

// 8 OverlappingLayering Items
auto, raste na osnovu sadrzaja unutra
fr, fractional sizing unit, fraction of available space, kao u flex, basis bese, kao flex 1
overlap, preko prethodnih itema, ispod sledecih, moze i z-index

// 9 Grid Layout  Box Alignment
alignment properties na containeru
justify-content, justify-items, align-content, align-items
isto kao flexbox
---
na itemu
justify-self
align-self

// 10 Grid in the Real World
responsive layout odlican sa media query
---
slike, menja redosled slika automatski
grid-auto-flow: dense;

-----------------------------
-----------------------------

-----------------
-----------------
// flexbox

// 2 Flexbox the Basics
flex container i items
main axis - horizontalno, s leva na desno, USMERENA
main start, main end, main size
cross axis - vertikalno, odozgo na dole, USMERENA 
cross start, cross end, cross size

// 3 The Flex Container
relcija kontejnera i DIREKTNE dece, 1 nivo, items
display: flex; itemi jedan pored drugog umesto jedan ispod drugog za default block
kontejner i dalje blok
display: inline-block; kontejner je inline nije vise ceo red, tj blok, fora

// 4 Flex Flow Direction
flex-direction: row; row-reverse, column, column-reverse

// 5 Flex Line Wrapping
kad nema dovoljno mesta za iteme
text-wrap: nowrap; 1 linija, default i za flex
s leva nadesno lomi u nove redove, flows
flex-wrap: wrap;
1 2
3 4
5 6

flex-wrap: wrap-reverse;
5 6
3 4
1 2

flex-direction: column;
flex-wrap: wrap;
1 4
2 5
3 6

flex-direction: column;
flex-wrap: wrap-reverse;
3 6
2 5
1 4
---
skraceno
flex-flow: column wrap; isto kao odvojeno navedeni

// 6 Display Order
ordinal group 0 default //GRUPE
na itemu 4 order: 1; // i ide na kraj jer je u 1 ordinal group
moze i order: -1; //ide na pocetak
u istoj grupi redosled diktira html struktura kao u 0

// 7 Flexibility
extra space
flex-grow: 1; na iteme, podjednako popune sve
flex-grow: 4; onda taj poraste u tom odnosu // to je ono, ratio koji koliko zauzima
1 4 1 1 // to
---
width: 150px; //items evenly, bez obzira na vecu sirinu
flex-shrink: 2; // 0.5 1 1 1 //odnosi
flex-shrink: 0; dobije sirinu iz width //DOBAR
---
flex-basis: ;// inicijalna sirina itema, u px, pre nego distribuirani u kontejneru
u kombinaciji sa flex-grow, grow mu dodaje na inicijalnu
---
skraceno
flex: flex-grow flex-shrink flex-basis; // DOBAR

// 8 Alignment //jasno
na containeru
flex-direction menja main i cross axis
justify-content (main axis), align-items (jedan red) (cross axis), align-content (ceo kontent)
---
na pojedinacni item
align-self
vrednosti: flex-start, flex-end, space-between, space-around (prva i poslednja margina duplo manja), stretch
---
align-items: baseline; na osnovu teksta, stretch - default
---
align-content za multiline items, za singleline ne radi nista, stretch default

// 9 Flexbox in the Real World
layout: nav, content, sidebar, footer
----------------
// https://www.youtube.com/watch?v=63MbEXhnmpI
// Compare Flex Grow and Flex Shrink within a Flexbox Container
flex-grow: 3; //default 1 // raste 3x brze u odnosu na ostale kada se resizuje u odnosu na default
-----
flex-grow: 1;
flex-shrink: 3;// default 0 // smanjuje se 3x brze u odnosu na ostale kada je resizovan manji od default
-----------
// https://www.youtube.com/watch?v=CFgeJq4l1YM
// Finally understanding Flexbox flex-grow, flex-shrink and flex-basis
flex-grow i flex-shrink kako se koristi EXTRA space kontejnera koji ostaje posle flex-basis // TO
------------
// flex 1 fixed, 1 fluid column with ratio
zapravo je flex-shrink: 1; je to sto mi treba, i flex-shrink: 0; na fiksnoj koloni
oba regulisu dodatni prostor, grow visak, shrink manjak // zapazi
flex-grow - regulise kod koga ide visak
flex-shrink - regulise koji se smanjuje // taj mi zapravo treba
----
// oba zapravo, ali ok
.fixed-column
flex-grow: 0, flex-shrink: 0
.fuild-column
flex-grow: 1, flex-shrink: 1
// shorthand:
flex: 1 1 33%; // flex-grow: 1, flex-shrink: 1, flex-basis: 33%
-----------
display: inline-flex; // ne zauzima celu sirinu kao blok element nego samo koliko ga sire deca iznutra svojim contentom
----------------
// svg
// https://www.youtube.com/watch?v=emFMHH2Bfvo
css selektori, klase i pravila rade i nad rect, circle etc tagovima unutar svg-a
----------------
// stacking context
je scope u kome vaze z-indexi, grupe layera u photoshopu
z-index je relativan u odnosu na strukturu html-a, nije apsolutan
stacking context kreiraju position relative, absolute, display flex, grid itd, mnogi drugi
opacity < 1, filter != none, mix-blend-mode != normal, transform
// https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Positioning/Understanding_z_index/The_stacking_context
z-index da bi radio position != static













