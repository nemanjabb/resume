// https://tailwindcss.com/docs/typography-plugin
add beautiful typographic defaults to any vanilla HTML you dont control
html kome ne mozes da dajes klase, stize kroz api ili rendered .md // ovo je vazno da se zapamti
----
// primer, samo jedna klasa u parent
<article class="prose lg:prose-xl">
  <h1>Garlic bread with cheese: What the science tells us</h1>
  <p>For years parents have espoused ...</p>
  <p>But a recent study shows that the...</p>
  {/* nema tvojih klasa ni stilova ovde */}
</article>
------
// ovako se ukljucuje u tailwind.config.js
module.exports = {
    plugins: [
      require('@tailwindcss/typography'),
      // ...
    ],
  }
-----------
// base font size, na body tagu
Class	Body font size
prose-sm	0.875rem (14px)
prose-base (default)	1rem (16px)
prose-lg	1.125rem (18px)
prose-xl	1.25rem (20px)
prose-2xl	1.5rem (24px)
--------
// prose modifiers
// modifier je prefix, a ne klasa, modifier:klasa
prose-h1 // selektuje h1 tag
prose-headings // selektuje sve h tagove
prose-a // selektuje linkove
// primer, boja linkova
prose-a:text-blue-600
hover:prose-a:text-blue-500 // kombinovanje 2 modifiera
--------
not-prose // klasa za sandbox, da preskoci
-------
// remove prose max-width da popuni kontejner
prose klasa ima max-width pre kontejnera, ovo je sklanja
max-w-none 
--------
// add custom theme color
// celu temu dodaje na sve klase, tagove
module.exports = {
    theme: {
      extend: {
        typography: ({ theme }) => ({
          pink: {
            // css zato sto postavlja css propertije i vars, na html tag
            css: { // css key, zapazi 
                // css vars na html tag, default bez key
              '--tw-prose-body': theme('colors.pink[800]'),
              '--tw-prose-headings': theme('colors.pink[900]'), // pstavlja property koji koristi klasa
              // ...
            },
        }),
      },
    },
}
----
// ovveride default theme (typography)
module.exports = {
    theme: {
      extend: {
        typography: (theme) => ({
          DEFAULT: { // default theme
            css: { // css key, zapazi
              color: theme('colors.gray.800'), // overrides text color on html tag
              // ako ovde nije specificiran tag odnosi se na html tag
              // ...
            },
          },
        }),
      },
    },
  }
----------------
font-sans klasa ne treba jer je default
-------------------
// https://www.youtube.com/watch?v=J0Wy359NJPM
// Styling Markdown and CMS Content with Tailwind CSS - Tailwind Labs, svajcarac
// mocan plugin, 3 klase sve promeni
-----
config require('plugin') // require je import u bilo kom delu koda, zapazi
-----
typografija je teska jer ima puno parametara koji su povezani
line-hight, padding, size ...
----
prose-xl poveca sav font na svim tagovima, kao scroll + + // zapazi, dobar
base od 16 na 20, hoces krupnija slova na sajtu
nije xl:prose koji selektuje na xl viewport
------
prose md:prose-lg lg:prose-xl // mobile, srednji i veci, responsive fonts
------
prose-red boji samo linkove u crveno // zapazi
-------
typography plugin se svodi da definise stilove ZA SVE TAGOVE koji se koriste u tekstu, jer klase ne postoje // to, zapazi
------------
DEFAULT je za prose klasu, i ovde // zapazi
lg je za prose-lg
------------
za typography za elemente ili prefix:prose ili tagovi u theme.typography.DEFAULT.css u tailwind.config.js 
------------
// ovo definise custom prefix, variants
// prose-cactus prose-headings::font-semibold // h1, h2, h3...
typography: (theme) => ({
  cactus: {
    css: {
      "--tw-prose-headings": theme("colors.accent-2 / 1"),
    },
  },
})
----------------------
// za teme za prose trebas puno css vars da predefinisies
// najbolje koristiti gotovu, prose-slate dark:prose-invert, slova su crna uglavnom // ok
-----
// a ovako se definise custom prose tema 
// https://github.com/chrismwilliams/astro-theme-cactus/blob/main/tailwind.config.ts
typography: (theme) => ({
  cactus: {
    css: {
      "--tw-prose-body": theme("colors.textColor / 1"),
      "--tw-prose-headings": theme("colors.accent-2 / 1"),
      "--tw-prose-links": theme("colors.textColor / 1"),
      "--tw-prose-bold": theme("colors.textColor / 1"),
      "--tw-prose-bullets": theme("colors.textColor / 1"),
      "--tw-prose-quotes": theme("colors.quote / 1"),
      "--tw-prose-code": theme("colors.textColor / 1"),
      "--tw-prose-hr": "0.5px dashed #666",
      "--tw-prose-th-borders": "#666",
    },
  },
})
--------------
<Prose /> component je SAMO ZA MARKDOWN koji ne kontrolises // zapazi, eto
za tvoje komponente ide drugi custom styles, recimo <Link /> component
ne motas tvoje komponente u <Prose /> nikada

