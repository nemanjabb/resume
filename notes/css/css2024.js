// design inspiration websites
https://www.curated.design/
https://www.lapa.ninja/
https://dribbble.com/
https://www.awwwards.com/
https://www.behance.net/
https://themeforest.net/
------------------
// :root pseudo-class selector
highest element in document, svodi se na html tag, root bukvalno
sluzi za globalne css properties, vars
--------
// pseudo-elements vs pseudo-classes
pseudo-elements and pseudo-classes are both types of selectors 
// pseudo-classes:
:hover, :active, :focus, :first-child, :nth-child()
a:hover // single colon :
elements based on their state or position in the document
-----------
// pseudo-elements:
::before, ::after, ::first-line, ::first-letter
p::before // double colon :: 
to style virtual parts of an element
----
:root is a pseudo-class_ selector, not a pseudo-element selector // eto, jasno
---------------
// styling links
// a treba odmah do teksta ako je content link
da ne bude h2 border koji je block npr. 
<h2>
    <a>text</a>
</h2>
-------
// text-decoration: underline vs border-bottom
menja text sadrzaj, offset zavisi od velicine fonta
border-bottom, mora <a /> odmah do teksta, h2 npr
--------
// states
hover, focus, active, visited
--------
// moj tailwind primer
'no-underline decoration-1 text-link underline-offset-4 hover:underline hover:text-link-hover focus: active: ...'
--------------------
// 10 design principles
https://tenprinciples.design/
-----------------
// centriranje modala, fora
// https://youtu.be/KvZoBV_1yYE?list=PLlNdnoKwDZdwANQoaakE8-kYMyKFOETlo&t=184
top: 50%;
left: 50%;
ovo stavlja GORNJI LEVI UGAO modala u centar ekrana
// to je fora, zato je potrebna dodatna translacija, da pomeri centar modala u centar ekrana
translate-x: -50%;
translate-y: -50%; // eto
--------------
// ovo je reset, jer browser smara sa default marginama na p, itd. // ok
* {
    border-sizing: border-box;
    padding: 0;
    margin: 0;
}
------------------
outline doesnt affect size, border does
transition requires correct (same) both start and end css rules
// primer
outline-red-300 outline-transparent, a ne outline-0
----------------------
// elemenat siri od parenta, centriranje u parentu, nije 100% jasno
.bustout {
    margin-left: calc(50% - 50vw); // negativno, parent/2 - viewport/2
    margin-right: calc(50% - 50vw); // centriranje
    transform: translateX(calc(50vw - 50%)); // pocetni levi polozaj
    width: 80vw; // sirina
    max-width: 1280px;
}
translateX je centriranje jer inicijalno element je uz levu ivicu parenta, a ne u centru // ok
50% - parent 
50vw - viewport // uvek veci od parenta
negativne margine - centriraju kada je element siri od parenta (a ne sire ga)
------------
// poenta:
ovo je centriranje elementa u parentu kada je element siri od parenta (a ne od vewporta) // to je glavno
sirina je zadata sa width: 80vw, to je sire od parenta (a manje od viewporta)
ostali kod je za centriranje, margine i translateX 
sirina zadata sa vw (% viewporta), a max ogranicena sa px // ok
-----
ne bi radile druge brojke od 50vw, 50% 
-----------
i margina i translate je kompenzovanje leve ivice 
samo leva margina negativna se okida i gura u levu stranu, translateX gura u desnu
---------------
// clamp fja, range, nisam znao
clamp(minValue, preferredValue2, maxValue)
font-size: clamp(2rem, 4vw, 4rem);
width: clamp(150px, 50%, 400px);
--------------------
// :target pseudoclass - navigated id link, nije lose za highlight
:target - pseudo-class_ matches elements that are targeted by the fragment identifier in the URL. 
// primer
https://example.com/page#my-id
<section id="my-id" />
---------------------
// filter css in firefox, computed and elements // zapamti ovo uvek
font-size - p, h1 - live updates dok resizujes // zapazi
lako se broji koliko breakpoints za text i koje velicine
margins, sve sto je responsivno
---
u elements (rules) lako nalazi u kojoj klasi je postavljen
------------
// :where
:where() - CSS pseudo-class_ that matches elements WITHOUT specificity (weight) increase
--------------------
// matrica cards
CardItem nema ogranicene dimenzije (eventualno visinu ili max-h)
na taj nacin moze fluid car i constant gap 
ako postavis visinu images will crop with object-cover, aspect-ratio wont be constant
max-h-48 important for mobile single column view, tu su najsire
-----
grid spolja kontrolise velicinu cards 
stilovi podeljeni izmedju CardList i CardItem 
-------------------
// important:
components should not have outer margins and paddings
caontainers should set margins on their content - components
------------------
flex uvek treba korektan direction (flex-row or flex-col) da ima, cak i za jedan item, zbog overflow // zapazi
----------------------
// min-content, max-content, fit-content
// min-content, max-content, fit-content - intrinsic sizing with CSS - Kevin Powell
// https://www.youtube.com/watch?v=DM244V9KvNs
moze za width, max-width... 
postavlja sirinu na osnovu contenta (text uglavnom, ili slike)
max-content - nema word wrap 
min-content - 1, najduza rec
fit-content - krece kao max pa fallback na auto ... ?
------------------
------------------
// vertical spacings and rithym with margins
u css-u je fora da se radi sa minimalnim setom (presetom) velicina - margine, (semantic) boje, fontovi (familija, weight, boja), radius
na taj nacin se ostvaruje ritam, deluje uredno
-----
// za vertikalni ritam margina
kad su siblings u istom parentu moze margin collapsing i onda se to koristi masovno
i parent-child margine kolapsiraju na vecu, ne samo siblings
---------
unutrasnji (krajnji) tagovi nikad ne trebaju da imaju strcece margine koje uticu izvan parenta, samo siblings 
za krajnje ide mt- i mb- da na ivicama nema unutrasnjih nested margina
----------
komponente nikad ne trebaju da imaju spoljne margine, one se mecu gde ih koristis
paddinge koriste za svoj spacings
za sitnije komponente moze flex gap mesto margina, za ritam 
----------------
// inset // trivial, eto
The inset property is a shorthand property for the following properties:
top, bottom, left, right
inset: 15px 30px 60px 90px;
// isto sto i
top: 15px
right: 30px
bottom: 60px
left: 90px
------------------
h-full sprecava da content odredjuje visinu, nego izaziva scroll 
----
// nested, koncentricni border radiusi, calc, chatGpt
polazis od parent border radius 
min() fja, child mora manji
znaci - (padding + margin) // ok
// trivial
$effective-child-radius: min($child-radius, $parent-radius - padding - margin);
padding na jednoj strani //zapazi
// primer
parent - border-radius: 36px; padding-right:8px 
child - border-radius: 28px; // jeste
kucaj "nested border radius calculator" u google
-------------------
// box-sizing: border-box | content-box // zapazi, vazno za text input
da li padding ulazi u height, za inpute najcesce
content-box - padding ne ulazi u height, mozes odvojeno da ih postavis // to mi trebalo
inace na svim elementima je default border-box // sve ulazi u height
rows je samo fallback kad nije postavljen height, za html bez css
-----------------
// svg clip-path, mask, cut out
// https://www.sarasoueidan.com/blog/css-svg-clipping/#reference-box
za clip path fora je mora odvojeni svg sa clipPath node pa URL, clip-path: url(#svgPath); // to glavno
onda moze reltivni postioining (centering) sa objectBoundingBox,  in the range [0, 1]
clip-path brise, ne treba boja
------
mask moze isto to, samo treba crna i bela, crna zadrzava. bela brise
linear gradient za crnu 
-------------
default background je transparent, ne mora da se postavlja
-------------
// clip-path i mask imaju drugacije atribute
// https://stackoverflow.com/questions/52685648/how-do-maskunits-maskcontentunits-attributes-affect-mask-positioning
// ne moze mixed units
maskUnits="userSpaceOnUse" | "objectBoundingBox" // koji deo maske koristis
maskContentUnits="userSpaceOnUse" | "objectBoundingBox" // zapravo pozicija
----
<mask
id="obb-obb"
maskUnits="objectBoundingBox"
maskContentUnits="objectBoundingBox"
x="0"
y="0"
width="0.5"
height="0.5"
>
<rect x="0" y="0" width="1" height="1" fill="white" />
<circle cx=".5" cy=".5" r=".1" fill="black" />
</mask>

--------
// za clipPath
<clipPath id="clipCircle" clipPathUnits="objectBoundingBox"/>
----------------
// prefers-color-scheme, 'dark' | 'light' | null
// podesavanje browsera, ne OS-a, ima i za OS, ali nije applied to browser
const prefersDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
const prefersLightMode = window.matchMedia('(prefers-color-scheme: light)').matches;
-----
// event
const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
mediaQuery.addEventListener('change', handleColorSchemeChange);
function handleColorSchemeChange(event) {
  if (event.matches) {
    // ...
  }
}
-------------------
// mobile first vs desktop first
// mobile first, min-width: 768px
tailwind sm:, md:, lg:, xl: ...
// zasniva se na min-width
// md:items-center generise:
@media (min-width: 768px) {
  .md\:items-center {
    align-items: center;
  }
}
default_xs, sm:, md:, lg: - zadnja sleva koja je ispunjena se izvrsava, overriduju se nadesno
sve overriduju default
-----------
// desktop first, max-width: 768px
<img srcset="urls" sizes="(max-width: 360px) 240px ..." />
astro Image.sizes je desktop first, max-width: 768px // konvencija
-------
class: `border-8 border-blue-500 [@media(max-width:475px)]:!border-yellow-300 [@media(max-width:768px)]:border-orange-500 [@media(max-width:1280px)]:border-red-500`,
default_2xl, max-sm:, max-md:, max-lg:
default je najveca mera
sve overriduju default, kad nijedna nije ispunjena ide default 
---------
// poenta:
1. sve je isto samo default je min ili max mera // jeste, samo uslov
2. okida se sa leve ili desne strane opsega <, >, sm na primer
min-width - sdesna 
max-width - sleva 
3. to sto ih ima vise okida se prva zadovoljena (sa te strane)
3. uvek su rastuce navedene
3. redosled navodjenja u tailwindu nije bitan zapravo, switch case sa 1 izlazom
---------------
// za nov red u h1, fora iz shadcn
<br className="hidden sm:inline" /> // to
<h1 className="text-3xl font-extrabold leading-tight tracking-tighter md:text-4xl">
  Beautifully designed components <br className="hidden sm:inline" />
  built with Radix UI and Tailwind CSS.
</h1>
--------------------
// tabela u listu na mobile
// https://codepen.io/team/css-tricks/pen/wXgJww
table, thead, tbody, tr, td imaju table- display // zapazi
display: table; table-header-group; table-row-group; table-row; table-cell;
------
kad prebacis u block nije vise tabela 
------
vertical-align radi zaista samo u display table, table-cell, itd. // eto
table-cell je inline by default_, moze da se zadrzi za same line 
-------------
// nauci grid, prost a mocan za layout raspored
<div className="grid [grid-template-areas:'overview'_'content'_'details'_'spacer'_'similar'] grid-cols-[1fr] 
sm:[grid-template-areas:'overview_overview'_'content_details'_'spacer_spacer'_'similar_similar'] 
sm:grid-cols-[1fr_15.125rem] gap-6" />
-----------------
// width: fit-content
width: fit-content i max-width: fit-content se odnosi na duzinu texta unutar div // eto 
w-fit, h-fit
max-h-fit // odlicna fora kad visina zavisi samo od texta
-----------
// centriranje modala, fixed - absolute
// backdrop - ceo ekran
<div className="fixed inset-0 bg-black bg-opacity-50 z-50" />
// modal - na 50/50 plus njegova velicina 50/50
<div className="fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 p-6 rounded-lg shadow-lg w-96" />
proveri za mobile posebno
cesto ide inset-0 - shortcut za top, right, bottom, left 0, ceo screen za backdrop npr
------------------