// unstyled components
https://www.youtube.com/watch?v=fgWM5Y2LTGk
only logic, no styles // trivail
logic - treba da bude generic
design - treba da bude original i custom, design system
-------
kad ih renderujes gole komponente bez stila
decoupled logic and design
-------
// libs:
radix ui, react aria (adobe), headless ui (tailwind), reach ui
------------
// So You Think You Can Build A Dropdown? - Pedro Duarte
https://www.youtube.com/watch?v=pcMYcjtWwVI
// radix unstyled komponente, handled mnogo vise logike nego go mouse events
accessibility - keyboard navigation, typeahead, 
screen reader (voice over, assistive technology), (cita aria attrs), ne samo mis events
stilizovanje ovih komponenti je isto kao bilo kog htmla
------
dropdown primer - ceo kod je samo go jsx, nema js i react logike
plus css klase
-----
animacije - css keyframes, js library - framer, react spring
collisions - dropdown svestan pozicije u viewportu i handluje
-----
stiches - css in js lib
------------
primitives - headless components
themes - stilizovane components // jeste, obicna component library
// sve izlistane ovde
https://www.radix-ui.com/themes/playground
-----------------------
// radix themes
prvo instaliras temu pa theme provider oko root commponent // kao i obicno
i import css
import '@radix-ui/themes/styles.css';
<Theme config={config} >
    <MyApp />
</Theme>
------------------
ne sme import tailwind jer overriduje temu
----------
// kad hoces Slider iz teme, importujes ga iz teme, a ne iz primitive
import { Theme, Flex, Text, Button, Slider } from '@radix-ui/themes';
// a ne
import { Slider } from '@radix-ui/react-slider';
----------
Box provides block-level spacing and sizing // eto, zapazi
Flex and Grid let you create flexible columns, rows and grids
---------------
// custom color shade
<Text style={{ color: 'var(--gray-8)' }}>Some text</Text>
----------------------
// https://www.radix-ui.com/primitives/docs/guides/composition#changing-the-element-type
asChild je kao Fragment za children da bi se prosledio props 
element se klonira da bi mogao props da se prosledi 
asChild - children element ce biti upotrebljen umesto originalnog u komponenti // ok, to
// primer upotrebe:
// ako je u komponenti bio <button /> ovaj <a /> ce ga zameniti
<Tooltip.Trigger asChild>
    <a href="https://www.radix-ui.com/">Radix UI</a>
</Tooltip.Trigger>
----------
// kad se radi sa radix headless komponentama
// zato je tako kako je u shadcn ui komponentama
1. your component must spread props
2. your component must forward ref
------------------
// Slot je utility komponenta za asChild
// https://www.radix-ui.com/primitives/docs/utilities/slot
------------
// procitaj dokumentaciju za primitive
// https://www.radix-ui.com/primitives/docs/overview/introduction

