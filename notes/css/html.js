h1 je jedan na stranici, h2 vise // zapazi
----
<header /> tag je za navbar, oko <nav />, a ne za h1 
----------
// canonical
koncept određivanja jedinstvenog, autoritativnog izvora informacija ili resursa // jedini pravi nacin, tacno sto mi terba
-----
// canonical url
To je preferirana verzija URL adrese za određeni sadrzaj na web stranici
1 canonical i ostali su mirrors
------
koja je verzija glavna i treba da se indeksira i prikazuje u rezultatima pretrage
----------------
// relative vs absolute path for <a href="" />, chatGpt
vazno za base_url env variablu
----
Current URL: https://www.example.com/articles/2024/06
<a href="/folder/path">Absolute Path Link</a> // absolute path /folder/path
<a href="folder/path">Relative Path Link</a> // relative path folder/path
---
// Absolute Path Link:
// Starts from the root of the domain.
Points to https://www.example.com/folder/path
----
// Relative Path Link:
// Appends folder/path to the current URL path. // to, zapazi
Points to https://www.example.com/articles/2024/06/folder/path
----------
// oba rade isto i kad ima subdomen
https://subdomain.example.com/folder/path
https://subdomain.example.com/articles/2024/06/folder/path
-----
// absolute probably fails for example.com/base-path/ // proveri nekad posle
tu je jos i ./folder , ../folder ... istrazi sve to
------------------
// relative urls, href links on html page
1. Relative URL with Path // doda na postojeci url stranice
href="./contact"
current page = https://example.com/blog/post
browser will resolve it as https://example.com/blog/contact
2. Relative URL with Double Dots (..) // isto samo jedan parent up
href="../about"
current page (https://example.com/blog/post)
browser will resolve it as https://example.com/about
3. Root-Relative URL (Starts with /) // absolute, ta mi treba
poenta: it is relative to the origin
href="/contact"
will always resolve to https://example.com/contact regardless of the page you're currently on
---------------
// new URL() object in js, props 
href: 'http://localhost:3000/blog/2023-01-19-example-article-3',
origin: 'http://localhost:3000',
protocol: 'http:',
username: '',
password: '',
host: 'localhost:3000',
hostname: 'localhost',
port: '3000',
pathname: '/blog/2023-01-19-example-article-3',
search: '',
searchParams: URLSearchParams {},
hash: ''
------
baseUrl je origin, domain je host
----------------
cannonical url je per page, izabrani link stranice
<link rel="canonical" href={url} />
---------
koristi relativne linkove gde god je moguce
-------------
