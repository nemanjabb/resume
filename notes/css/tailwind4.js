// dobar pregled, 9 minuta
// https://www.youtube.com/watch?v=ud913ekwAOQ
-------------
// official link
// https://tailwindcss.com/docs/upgrade-guide
-------------
// tailwind 4 with astro
// https://www.youtube.com/watch?v=z4oih8sh6O8
// https://tailwindcss.com/docs/installation/framework-guides/astro
// https://docs.astro.build/en/guides/styling/#tailwind
--------------
// @layer components  i @layer utilities
// https://tailwindcss.com/docs/upgrade-guide#adding-custom-utilities
// https://tailwindcss.com/docs/adding-custom-styles#adding-custom-utilities
i @layer components  i @layer utilities se svode na @utilities
samo @utilities ima manje pravila i na tome se zasniva specificity // proveri
------
// https://tailwindcss.com/docs/adding-custom-styles#adding-base-styles
i dalje postoji @layer base i @layer components, samo je @utilities 
// default tema sta sve ima
// https://tailwindcss.com/docs/theme#default-theme-variable-reference
-------------
// layer() funkcija ide uz @import file, da kaze u koji layer da ga stavi
// ima i layer theme
@import "tailwindcss/theme.css" layer(theme);
@import "tailwindcss/preflight.css" layer(base);
@import "tailwindcss/utilities.css" layer(utilities);
// zapravo je to native cascading layers, deo web standarda, a ne tailwind 4 // zapazi 
-----------
// vazna poenta
deo css utilites zavisi koje css vars su definisane npr --color-mycolor-500 -> bg-mycolor-500
-----------
// https://tailwindcss.com/blog/tailwindcss-v4
// container queries, breakpoint on container level
// @sm:... zapazi
<div class="@container">
    <div class="grid grid-cols-1 @sm:grid-cols-3 @lg:grid-cols-4">
    </div>
</div>