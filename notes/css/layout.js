//
<body>
    <header />
    <main />
    <footer />
</body>
----
body - flex, flex-col, align-stretch (default) za full width, min-h-screen za full height 
main - flex-grow-1 za full height 
----
// header
<header class="group">
    <wrapper>
        <left />
        <right />
    </wrapper>
</header>
group - da bi svi ostali bili deca za tailwind states, za open/closed menu 
wrapper - px-4, max-w-5xl (ne container, samo 1 max-w), md:mx-auto 
py-4 (za vertikalno centriranje, bolje nego flex)
----
// body 
nema ni pading ni mx-auto, da bi svaki section mogao za sebe da kontrolise
ni align-center da bi bio align-stretch za full width 
-----
// centering
mx-auto - samo jedan element potreban, za flex justify-center treba 2
-----------
// main
use container, a ne rucno nekoliko max-w 
container breakpoints definises u tailwind.config.js 
----
main ide u Centered layout, not Base da bi imao odvojen FullWidth layout, bez Container component sa dodatni div 
<main id="main" class="flex-grow container px-4 md:mx-auto" />


