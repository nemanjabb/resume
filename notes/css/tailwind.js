// Tailwind Complete Course For Beginner to Advanced - [2021]
// https://www.youtube.com/watch?v=lZp4salRFFc

lg:container // @screen lg, lg media query (up), container max-width kao i uvek
----
// flex
gap - za spacing
flex-initial je klasa za flex: 0 1 auto, docs
flex-1 - svi grow na 33%
flex-auto - grow based on his width
---
// directives
// 3 layers in tw
@ base, components, utilities // @ - direktiva

@layer base {
    // ubacuje u base gore, goli html elementi
}
@layer components - klasa za komponente, bzvz
@layer utilities - da kreiras svoju custom utility klasu p-17 za 17px npr, ok
---
@variants - za pseudoklase hover, focus itd
@responsive - za svaki breakpoint
@screen sm {}
theme('height.60') // iz configa plugina?, spacing[2.5] ako mera ima . ide []
----------
// config fajl
za custom stilove, postoje i default
theme je za utility klase
----
// postcss
tailwind i autoprefixer su pluginovi u postcss
autoprefixer samo za prefixe za browsere
-----
// jit
h-[33px] - custom height utility class
!font-medium - !important
mora purge uz jit
---------
// theme
breakpoints - screen
colors
spacing - sve spacing mere uzimaju istu skalu // overrides mora sve da ih predefinises
----
extend - da TextDecoderStream, ne moras sve da ih definises
theme: {
    extend: {
        screens: {
            myXl: '2000px',
        },
        colors: {
            myRed: 'red',
        }
    }
}
----
variants - pseudoklase, bezveze preimenovano
isto extend core plugins
--------
// custom plugins
u javascriptu su, imports

const { flex } = require('tailwindcss/defaultTheme')
// custom utilites
const plugin = require('tailwindcss/plugin')
plugins: [
    plugin(function({addUtilities}) { // add custom your utilities
        addUtilities(newUtilites, {
            variants: ['hover']
        })
    })
]
---
// custom components
slicno addcomponents
u js stilovi idu camelCase, fontWeight
scss & ...
--------
prefix: 'tw-'
----------
// addBase - isto za base layer
----------
// preset
share base, comp and util accross multiple projects
shared config file
--------------
// dark mode
dark:bg-red-400 // stil def za dark mode
dark mode je <html class="dark" > klasa na html tagu
---------------------------
---------------------------
// tailwind
// How to Add TailwindCSS to a NextJS Application
// https://www.youtube.com/watch?v=b_Y3HD870Jk
// nema mnogo
npm install tailwindcss // samo to
postcss.config.js overrides next.js default
// tailwind.config.js
purge: {} // remove dead css code for production
purge: ['./pages/**/*.{js,ts,jsx,tsx}'], // whitelist, sta da ostavi, ostalo skloni
tailwind init - kreira config
styles/index.css - include tailwind base, components, utilities, import it as global in _app.tsx
i tailwind ima komponente gotove
---
css baseline - resetuje browser defaults
---------------------
// vertikalno centriranje texta razlicite velicine
items-center ide odmah do teksta, <a /> tag itd, a ne na root navbara, onda centriras divove a ne text
// align-items: baseline
// https://stackoverflow.com/questions/34606879/whats-the-difference-between-flex-start-and-baseline
poravnava elemente po DONJOJ ivici teksta
itemi se ravnaju po textu koji je unutra
baseline - donja ivica teksta
// vertical-align
na inline, inline-block i inline-table elemente
i line-height mora...
-----------------------
// overflow: hidden radi i za zaklonjen border radius, sve ih stavis na taj element (parent)
// koji su mu deca, a prelaze ga
bg-white rounded-lg shadow-md overflow-hidden;
--------------
--------------
// tailwind docs, core concepts, dobar
template partial za templejte je ekvivalent komponenti u reactu
@tailwindcss/typography // plugin za stilizovanje markup htmla samo na osnovu golih tagova
---
// pravljenje sopstvene reusable component klase - ".btn-primary" koja moze u @apply, ide u "@layer components" 
// zapazi components
@layer components {
  .btn-primary {
    @apply py-2 px-4 bg-blue-500 text-white...
  }
}
---
// @layer directive Valid layers are base, components, and utilities.
@layer base { ...
@layer components { ...
@layer utilities { ... // eto custom utility
-------
// #{...} je interpolation za Sass/SCSS
@apply font-bold py-2 px-4 rounded #{!important}; 
------
// functions, theme()
height: calc(100vh - theme('spacing.12')); // . umesto - u utility klasama
height: calc(100vh - theme('spacing[2.5]')); // [] ako value already ima .
----
// screen()
@media screen(sm) {... 
---
// ovako zapravo radi
@screen md {
  --my-color: red;
}
---------------------------
---------------------------
// https://tailwindcss.com/docs/functions-and-directives#layer
// @direktive
// svude moze i apply i css rules, samo u global.scss, ne u komponentama
// base - html tagovi
@layer base {
    h1 {
        @apply text-2xl;
    }
    h2 {
        @apply text-xl;
    }
}
// components - dugmici, cards, bootstrap komponente...
@layer components {
    .btn-blue {
        @apply bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded;
    }
}
// utitlity - tvoji custom tailwind utilities
@layer utilities {
    .filter-none {
        filter: none;
    }
    .filter-grayscale {
        filter: grayscale(100%);
    }
}
-------------------------
// isto layeri moze i kroz plugin
// https://tailwindcss.com/docs/adding-custom-styles#writing-plugins
module.exports = {
    // ...
    plugins: [
      // zapazi args, logican
      plugin(function ({ addBase, addComponents, addUtilities, theme }) {
        addBase({
          'h1': {
            fontSize: theme('fontSize.2xl'),
          },
          ...
---------------------------
// theme
// https://tailwindcss.com/docs/adding-custom-styles#customizing-your-theme
// https://tailwindcss.com/docs/theme // ovde detaljno, jos nisam procitao
module.exports = {
    theme: {
      screens: { // predefinisi tailwind default mere u utilityima
        sm: '480px',
        md: '768px',
        lg: '976px',
        xl: '1440px',
      },
      colors: {
        'blue': '#1fb6ff',
        'pink': '#ff49db',
        'orange': '#ff7849',
        'green': '#13ce66',
        'gray-dark': '#273444',
        'gray': '#8492a6',
        'gray-light': '#d3dce6',
      },
      fontFamily: {
        sans: ['Graphik', 'sans-serif'],
        serif: ['Merriweather', 'serif'],
      },
      extend: { // prosiri, ubaci nove mere u utilitiyima
        spacing: {
          '128': '32rem',
          '144': '36rem',
        },
        borderRadius: {
          '4xl': '2rem',
        }
      }
    }
  }
// sto je direct ispod theme: { ... } je override
// sto je u theme: { extend: { ... } } je extend
// na nivou top level kljuceva spacing overriduje sve spacinge, ne moze merge verovatno
---
// za merge ovako
// so it can reference your own customizations as well as the default theme values
theme: {
  backgroundSize: ({ theme }) => ({
    auto: 'auto',
    cover: 'cover',
    contain: 'contain',
    ...theme('spacing') // ovo
  })
}
---
// ako ti treba samo default tema
const defaultTheme = require('tailwindcss/defaultTheme')
--------------------
// sto je samo css a ne i logika treba da bude tailwind css komponenta - klasa
// a ne react componenta // to to, zapazi
----------------------------
// https://tailwindcss.com/docs/customizing-colors
// custom colors, add directly in theme
theme: {
  colors: {
---
// reuse colors from default theme
// nista ne radi prakticno, samo limitira broj boja u intelisense, ili preimenuje
const colors = require('tailwindcss/colors')
module.exports = {
  theme: {
    colors: {
      white: colors.white,
      green: colors.emerald,
-----------------
// multiple color themes - css variables
// abstract color names (primary, secondary) because you want 
// to support multiple themes in your project
---------------------
// @layer base klase, a ne samo tagovi - razlika spram global styles
// https://v1.tailwindcss.com/docs/adding-base-styles
1. @tailwind base to avoid unintended specificity issues
2. purging the base layer
-----------------------
// plugins
// https://tailwindcss.com/docs/plugins
inject new style using JavaScript instead of CSS // to to
Plugins can be added to your project by installing them via npm, 
then adding them to your tailwind.config.js file
---
// tailwind.config.js
const plugin = require('tailwindcss/plugin')
module.exports = {
  plugins: [
    plugin(function({ addUtilities, addComponents, e, prefix, config }) {
      // Add your custom styles here
    }),
  ]
}
---
// add installed plugins
module.exports = {
  // ...
  plugins: [
    require('@tailwindcss/typography'), // add plugins
    require('@tailwindcss/forms'),
  ]
}
----
addUtilities and matchUtilities functions allow you to register new styles 
in Tailwinds utilities layer
---
// Static utilities
addUtilities - register simple static utilities that dont support user-provided values
hardcodirane klase
moze modifiers - lg:content-visible
---
// Dynamic utilities
matchUtilities - register utilities that map to values defined in the users theme configuration
koristi (mapira) vlicine iz theme() iz configa
podrzava i [] tab-[13]
----------
// addComponents
styles using CSS-in-JS syntax // zapazi
plugins: [
  plugin(function({ addComponents }) {
    addComponents({
      '.btn': {
        padding: '.5rem 1rem',
        borderRadius: '.25rem', // eto
        fontWeight: '600',
      },
---
prefix deluje i na utilities i na components - tw-flex, tw-btn
i components moze sa modifiers
------
// addBase
isto CSS-in-JS syntax:
base layer je za: base typography styles, opinionated global resets, or @font-face rules
samo za tagove selektore, nema prefix za klase i important
------------
// addVariant - custom modifiers - md: hover: dark: - to je variant
--------
// withOptions
plugin.withOptions(function (options) - expose options arg za usera da konfig plugin
---
// usage
plugins: [
  require('./plugins/markdown.js')({ // moze i relative js fajl
    className: 'wysiwyg' // options arg
  })
],
------
// css in js - pise se css u javascriptu - plugin
// moze ovako sa '...'
'.card': {
  'background-color': '#fff',
---
// style react
'.card': {
  backgroundColor: '#fff', // eto
----
// scss nesting & supported - powered by postcss-nested
boxShadow: '0 2px 4px rgba(0,0,0,0.2)',
'&:hover': {
  boxShadow: '0 10px 15px rgba(0,0,0,0.2)',
},
---
// array za isti key
addComponents([...]
---------------
require('...') gadja ime foldera u node_modules
----------------------
// https://tailwindcss.com/docs/functions-and-directives#theme
// directives - @tailwind, @layer, @apply - i to je to
// functions - theme(), screen() - i to je to
--------------
// tailwind.config.js se poziva ka postcss plugin u postcss.config.js
require('tailwindcss')('./src/components/tailwind.config.js'),
---
// vise nacina za install tailwinda, cli, postcss plugin...
// https://tailwindcss.com/docs/installation/using-postcss
Installing Tailwind CSS as a PostCSS plugin is the most seamless way to integrate it 
with build tools like webpack, Rollup, Vite, and Parcel.
---------
// use postcss as preprocessor sa pluginovima jer ga tailwind vec koristi
// postcss plugins
// https://tailwindcss.com/docs/using-with-preprocessors
// Build-time imports - postcss-import - spaja css fajlove u jedan
postcss.config.js
module.exports = {
  plugins: [
    require('postcss-import'), // uvek dolazi prvi
    require('tailwindcss'),
----
// @import mora prvi u fajlu
@import "tailwindcss/base";
@import "./custom-base-styles.css";
------
// Nesting - tailwindcss/nesting - scss nesting .a { b. { } }
// postcss.config.js
module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss/nesting'), // mora pre tailwinda
    require('tailwindcss'),
---
// moze i postcss-nesting plugin...
---------
//  CSS variables - rade bez plugina
----------
// autoprefixer - borwser vendor prefixi
module.exports = {
  plugins: [
    require('tailwindcss'),
    require('autoprefixer'), // uvek na kraj ide
-----------------------
// custom preprocessor - svaki ima svoj quirks sta radi a sta ne
// Using Sass, Less, or Stylus
// moras 2 build steps - scss -> css -> postcss
---
// scss
theme('colors.red.500') // theme() ne radi u scss
@apply bg-red-500 #{!important}; // #{!important}
----
less, stylus - nebitno...
-------------------------
// PostCSS is a tool for transforming CSS with JavaScript plugins
--------------------
// https://tailwindcss.com/docs/adding-custom-styles#removing-unused-custom-css
sve iz @layer base, components, utilities sto nije upotrebljeno u html se ne generise // to to
---
Any custom styles you add to the base, components, or utilities layers 
will only be included in your compiled CSS if those styles are actually used in your HTML.
---
sto hoces bezuslovno css ide bez @layer direktive, redosled gde je
--------------------------
// themes
// u temema bg-color i text-color idu zajedno u paru na istom elementu
// i sto blize root, a ne svud redom, i za sto vise stranica, layout component
// color system:
color variants: primary, secondary, accent, neutral, base (pozadina), plus danger, warning, info, success
pa svaki variant ima: default, focus, content (kontrast boja za text)
---
primary, secondary, neutral mogu biti iste (zajednicke) za light i dark par tema
---------
kod dark tema base100, base200, base300 smer obrnut, 300 svetlija od 100
akcenat ka svetlijem jer je pozadina tamna
-----------------
// opacity just on bg color
bg-th-success/20 // 0.2
-----------------
// svi osim zadnjeg childa
&--item {
  @apply h-56 border border-th-base-200;

  &:not(:last-child) { // mora &:
    @apply mb-4;
  }
}
------------------------------
// twMerge
https://www.youtube.com/watch?v=tfgLd5ZSNPc svajcarac
tailwind po defaultu sortira klase alfabeticaly, twMerge osigurava po redosledu // to glavna fora
twMerge - zadnja klasa pobedjuje, a prethodne sklanja
radi i sa modifiers i svim tailwind features
twMerge, isti args kao clsx
-----------------
border nije radio jer falio style solid, tj @apply border border-solid border-red-300
------------------
// preflight - base styles
// https://tailwindcss.com/docs/preflight
kad se injectuje tailwind u postojeci sajt (ekstenzije) menja stilove, inace ok
to su stilovi iz @tailwind base;
// disable completely, works, tested
// tailwind.config.js
module.exports = {
  corePlugins: {
    preflight: false,
  }
}