// uproscen visual cheat sheet
https://grid.malven.co/
------------
identicna ideja kao flexbox, glavna i sporedna osa, justify, align // ima jos verovatno
ima tu dosta toga jos...
-----------
// sam definises area
grid-template-areas: 'my-area' // prouci ovo?
.my-class {
    grid-area: my-area;
}
------
grid-template-columns: 1fr 768px 1fr; // fr...?
-----------------
// CSS Grid in 100 Seconds - Fireship
https://www.youtube.com/watch?v=uuOXPWCh-6o
grid lines - razdvajaju rows and columns 
grid track - row or column, izmedju 2 lines 
grid area - pravougaonik izmedju 4 lines
grid container, grid items
FR - fractional unit, razlomak - procenat containera
-----
grid-template-areas // matrica imena, string[][], raspored bez sirina i visina
grid-template-rows // width of columns
grid-template-columns // height rows
gap: x y; // space between lines
justify-items, align-items 
----
repeat(3, 1fr), minmax(50px, auto)
----------------
//  CSS Grid Crash Course - Traversy Media
https://www.youtube.com/watch?v=0xMQfnTU6oo
https://github.com/bradtraversy/grid-crash
grid za main page layout, flexbox za components
-------------
// container props
grid-template-columns 
grid-template-rows 
grid-gap 
grid-auto-rows 
grid-auto-columns
grid-template-areas 
justify-content 
align-items 

// items props
grid-column 
grid-row 
grid-area 
align-self 
justify-self 
------------
// na container
display: grid; // prazan nista ne radi na containeru
// broj i sirine kolona (vertikalne)
grid-template-columns: 1fr 1fr; // broj kolona i sirine svake, 1fr 1fr = 50% 50%, fr - udeo
repeat(3, 1fr) = 1fr 1fr 1fr
---
column-gap: 10px; // izmedju kolona
row-gap: 10px; // izmedju vrsta
gap - oba 
-----------
// visine rows
// eksplicitne visine, redje se koristi, uglavnom content odredjuje visinu
default content daje visinu na items // ok
minmax(min, max) - min <= range <= max // manje ili jednako, vece ili jednako
nije heigth nego min-height + max-height
---
// primer
// samo visine bez broja rows
grid-auto-rows: minmax(100px, auto); // daje visine rows
----
// broj i visina rowova
grid-template-rows: 1fr 1fr 1fr; 
-------
align-items: stretch; // default, svi items u istom rowu imaju visinu najveceg // tacno to
start, center, end - svaki item ima visinu svog contenta 
----
justify-content: start; // default, sve kolone ulevo, ako je container veci
ako vec imaju visinu sa grid-auto-rows bice prazno - prostor izmedju item i line, kao 2 elementa // zapazi
----
oba identicno kao flexbox  
-----
// oba mogu na itemu
height: 50px;
witdh: 50px;
align-self: center; // centriranje
justify-self: center; 
------------
// spans
sad su vazne grid lines numbers, 1, 2 default 
za grid line numbers klikni grid badge na element u browser
----
grid-column-start: 1;
grid-column-end: 3; // prve dve celije
---
grid-column: 1 / 3; // isto, skraceno, najbolje
grid-column: 1 / span 2; // opet isto
---
grid-row: 2 / 4; // za vertikalni span
---------------
// responsiveness
// bellow 500px u jednu kolonu
@media (max-width: 500px) {
    .container {
        // sve iteme, celu matricu
        grid-template-columns: 1fr; // ok, polazno bilo grid-template-columns: 1fr 1fr 1fr;
    }
}
----
// flex-wrap
grid-template-columns: repeat(auto-fill, minmax(200px, 1fr)); // ovo ne razumem, onako
200px je min, ako je manje wrap item u novi red
1fr je max - 1 item full width
repeat(value, n) // repeats same value n times
// tailwind primer
xl:grid-cols-3 // tailwind klasa
grid-template-columns: repeat(3, minmax(0, 1fr)) // repeat columns with 0 < width < 100% // ok, pros
-----------------
// grid-template-areas
ovo je za main page layout zapravo
---
// na containeru
height: 100vh; // mora i ovo
display: grid;
// definise positioning po rowovima, stringovima // to
// mapira na grid-area imena
grid-template-areas:
    'header header header' // span ide ponavljanjem
    'nav content sidebar'
    'nav footer footer';

// item selector
.header {
    grid-area: header; // ime iz matrice
}
----
// definise sizeing - width kolona i height rowova
// width kolona
grid-template-columns: 1fr 4fr 1fr; // srednja najsira
// height rowova
grid-template-rows: 80px 1fr 70px; // 1fr (moze i auto) kao flex-grow: 1 ovde, ostali u px
-----
koji su u fiksnim velicinama (px, rem...) ostali 1fr i auto zauzmu ostatak, rasire se 
-----
poenta: vrlo mocno, logicno i prosto // to, za main page layouts
------------------
// holly grail layout
https://github.com/atherosai/ui/tree/main/layout-01
ovde je bez area, nego sa span 
grid-column: 1 / 4; // start, end, 3 kolone, 4 linija kraj 3. kolone
broj_linija = broj_kolona + 1 // ok
-----------
grid use case - matrica sa spanovima, inace nested flex 
-----------------
// demo
srice kod, premotaj 
// po rednom broju selektuje iteme za span
.card:nth-of-type(1) {
    grid-column: 1 / 3;
}
grid-column: 1; // ako se izostavi / 3 ide do zadnje linije by default
-----
grid containeru uglavnom treba width, height 100% jer se ne sire by default 
-------------
// mobile screen, stacked
on ovde radio desktop first design
// container 1 kolona
grid-template-columns: 1fr;
----
resetovao spanove na mobile
columns: 1 / 2; // svi
rows; 1 / 2; 2 / 3; 3 / 4; 4 / 5; // 1, 2, 3, 4, 5, 6
---------------------------
poenta: grid je prilicno lak, tabele...
------------
// center items horizontally
justify-center xs:justify-normal // normal - default
