
-------------------
// DEFAULT word u config
// https://tailwindcss.com/docs/theme#core-plugins
borderRadius: {
  'none': '0',
  'sm': '.125rem',
  DEFAULT: '.25rem', // za utility klasu bez siffixa, .rounded
}
// se mapira na utility klasu bez sufiksa
.rounded-none { border-radius: 0 }
.rounded-sm   { border-radius: .125rem }
.rounded      { border-radius: .25rem } // ovo, ok
------------
// presets vs theme
// https://tailwindcss.com/docs/configuration#presets
preset je baseline za sve projekte
// https://tailwindcss.com/docs/presets
preset je ceo ili deo tailwind.config.js, themes + plugins + extends + overrides ...
u odvojen js fajl koji se importuje pod presets: [...]
-------
theme je za projekat 
presets: [
  require('@acmecorp/base-tailwind-config')
],
// Project-specific customizations
theme: {
--------------
core plugins obezbedjuju sve ugradjene utility klase 
module.exports = {
  corePlugins: {
    float: false, // disabling primer
    objectFit: false,
    objectPosition: false,
  }
}
----
plugin - inject styles with JavaScript instead of CSS
i install preko npm // to, zapazi
1. core plugins
2. official plugins
3. tvoji plugins
--------
// container queries, u odnosu na parent, a ne viewport // zapazi
<div class="@container" />
<div class="@lg:text-sky-400" />
-------
matchUtilities(), for registering new dynamic utility styles // ?
-------------
// typography plugin, @tailwindcss/typography
// https://tailwindcss.com/docs/typography-plugin#customizing-the-css
module.exports = {
  theme: {
    extend: {
      typography: (theme) => ({ // ovo je config za @tailwindcss/typography
        DEFAULT: { // DEFAULT or xl, modifier za velicinu, prose-xl, body font 18px npr // ok
          css: { // mora css prop za config
            color: theme('colors.gray.800'),
            a: 'stilovi za a, linkove...'

            // ...
          },
        },
      }),
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    // ...
  ],
}
------------------
// https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/#what-is-itcss
ITCSS - Inverted Triangle CSS
global namespace, cascade and selectors specificity // specificuity u principu
Settings -> Tools -> Generic -> Elements -> Objects -> Components -> Utilities 
-----
// https://tailwindcss.com/docs/adding-custom-styles#customizing-your-theme
// custom classes idu u main.css
@tailwind base;
@tailwind components;
@tailwind utilities;
.my-custom-style {...} // custom klasa
---
// why layers
zbog specificity, tj redosled gde je definicija u css fajlu, a sve klase imaju isti specificity
----
// @layer directiva 
pomera styles to the corresponding @tailwind directive
enables features like modifiers and tree-shaking
----
// base styles za page, ide na html element
<html lang="en" class="text-gray-900 bg-gray-100 font-serif" />
// @layer base ide za html elemente, tagove
@layer base {
  h1 {
    @apply text-2xl;
  }
}
// @layer components, za komponenete card, btn, badge, mogu da se overriduju sa utilities
@layer components {
  .card {
    background-color: theme('colors.white');
  }
}
// @layer utilities, tvoji utilities, najveci specificity
@layer utilities {
  .content-auto {
    content-visibility: auto;
  }
}
---------------
// Using multiple CSS files
moraju da se spoje u jedan fajl sa plugin ili greske za 'using @layer without the corresponding @tailwind directive'
module.exports = {
  plugins: {
    'postcss-import': {},// spaja sve u jedan css fajl
  }
}
----
// to, zapazi
znaci kad imas vise od 1 css fajlova (global css) koji (uvek) moraju da se spoje u jedan sa postcss-import plugin
mora import'tailwindcss/base' umesto @tailwind base direktive 
da bi koristio @layer base direktivu // ok
--------------
// layeri mogu da se ubace i preko plugina // eto, zapazi
const plugin = require('tailwindcss/plugin')
module.exports = {
  // ...
  plugins: [
    // ovo je inline plugin ovde, zapazi
    plugin(function ({ addBase, addComponents, addUtilities, theme }) { // plugin(...)
      addBase({
        'h1': {
          fontSize: theme('fontSize.2xl'),
        },
      })
      addComponents({
        '.card': {
          backgroundColor: theme('colors.white'),
        }
      })
      addUtilities({
        '.content-auto': {
          contentVisibility: 'auto',
        }
      })
    })
  ]
}
------------------------
// extend vs override
ako je u theme onda je override, ako je u theme.extends onda je extend
// override
theme: {
    fontFamily: {
        sans: ['Fira Code', ...fontFamily.sans],
    },
    extend: {
        // ...
    }
}
// extend
theme: {
    extend: {
        fontFamily: {
            'my-font-family': ['Fira Code', ...fontFamily.sans],
        }
        // ...
    }
}
----------------------
// extend ili tema (re)definise css propertije
// extend extenduje temu, a tema ima sve to
extend: {
    backgroundColor: { // background-color property
      skin: { // .skin utility klasa
        fill: withOpacity("--color-fill"), // skin-fill, suffix utility klase
        accent: withOpacity("--color-accent"),
        inverted: withOpacity("--color-text-base"),
        card: withOpacity("--color-card"),
        "card-muted": withOpacity("--color-card-muted"),
      },
    },
    // ...
}
--------------------
// kljucevi u temi i extends
// https://tailwindcss.com/docs/theme#theme-structure
// https://tailwindcss.com/docs/theme#configuration-reference
extends se odnosi na temu
theme object contains keys for screens, colors, and spacing, as_ well as_ a key for each customizable core plugin
core plugin je utility klasa
---------
svodi se na sve css properties
da se pokriju sa utilityjima
// npr spacings key se odnosi na:
padding, margin, width, height, maxHeight, flex-basis, gap, inset, space, 
translate, scrollMargin, scrollPadding, textIndent 
----
// theme keys struktura
module.exports = {
    theme: {
      screens: { }, // breakpoints
      colors: { }, // palette
      spacings: { }, // width, height, padding...
      // core plugins bellow
      fontFamily: { }, // 3 font families
      // extend za custom props i prosirenje
      extend: { } // za custom prosirenje
    }
  }
-----------------
// 1. nacin - @font-face
// How to add custom (web and locally downloaded) fonts in Tailwind
// https://www.youtube.com/watch?v=5U1okKk-GGE
// host local font, to je to
// najjednostavniji nacin u tailwindu
1. download font.ttf and put it in static folder
2. @font-face in global.css (@base, ...)
@font-face {  // ovo je glavno
    font-family: 'adelia';
    src: URL('./fonts/adelia.ttf'); // web adresa
}
font stavljen u public/fonts/adelia.ttf
3. extend u tailwind.config.js
module.exports = {
    theme: {
      extend: {
        fontFamily: {
            adelia: ['adelia', ...fallbacks]
        }
      },
    },
  }
-------
// example global.css
@tailwind base;
@tailwind components;
@tailwind utilities;

// ovde, a ne u @layer base
@font-face { 
  font-family: "Satoshi";
  src: url("/fonts/Satoshi-Variable.woff2") format("woff2");
  font-display: swap;
}
--------------
// poenta:
font se ne overriduje u typography pluginu, nego u glavnoj temi u configu // zapazi
--------
import { fontFamily } from "tailwindcss/defaultTheme";
--------------------
// 2. nacin - npm paket
// font moze i kao npm paket da se instalira // zapazi
// package.json
dependencies: {
  "@fontsource-variable/inter": "^5.0.15",
}
// Layout.tsx // mora i ovaj import, inace ucitava tailwindow sans
import "@fontsource-variable/inter/index.css";
// tailwind.config.js
theme: {
  extend: { // i ovde kad je isti key je override, a ne extend
    fontFamily: { 
      sans: ["Inter Variable", "Inter", ...defaultTheme.fontFamily.sans],
    },
  },
},
------
// 1500+ open-source fonts bundled into neat npm packages
// i to dobri fontovi
https://fontsource.org/
--------------------------------
// matchUtilities(), matchComponents(), matchVariant() - for dynamic classes
https://tailwindcss.com/docs/plugins#dynamic-utilities
// matchUtilities()
1. for registering new dynamic utility styles
2. to register utilities that map to values defined in the users theme configuration
// to je funkcija // zapazi
plugins: [
  plugin(function({ matchUtilities, theme }) {
    matchUtilities(
      {
        tab: (value) => ({      // set
          tabSize: value
        }),
      },
      { values: theme('tabSize') } // get
    )
  })
]
----
podrzava arbitrary values 
<div class="tab-[13]" />
-----------
https://tailwindcss.com/docs/content-configuration#class-detection-in-depth
tailwind radi za razne template jezike, html, jsx, js, ..., zato ih ne parsuje i ne izvrsava 
nego trazi cela imena klasa svuda (i u js strings) sa regex // zapamti
// ovako ne
<button className={`bg-${color}-600`} /> // ovo ne radi
---
// ovako se radi
const colorVariants = {
  blue: 'bg-blue-600 hover:bg-blue-500', // cele klase
  red: 'bg-red-600 hover:bg-red-500',
}
<button className={`${colorVariants[color]}`} />
----
// jos uvek nije 100% jasno...? primeri...
-----------------
// css nesting
https://tailwindcss.com/docs/using-with-preprocessors#nesting
// plugin api nesting
'tailwindcss/nesting': {},
// new CSS mesting
npm install -D postcss-nesting
'tailwindcss/nesting': 'postcss-nesting',
---------------------
tema je isto sto i layer utility samo sto ima i selector // zapazi
-------------
// shorthand radi sa theme()
padding: theme('spacing.2') theme('spacing.4');
--------------
// extend theme color palete
ovo nested definise rec u klasi, prevodi se u text-skin-base // ok
textColor: {
  skin: {
    base: withOpacity("--color-text-base"),
  },
}
-----
definises uzu color paletu, da radis sa setom (primary, secondary, accent, bg...) a ne default paleta razbacana 
i ponovljena svuda po jsx 
-------------
// todo: semantic colors (primary, accent, base, foreground...)??? // prouci
------------------
// tailwind.config.js theme extends vs @layer base, components vs plain css
u theme.extends.colors ili borderRadius kreiraju se kombinacije utility klasa, text-primary, bg-primary, itd 
u @layer base, components kreiraju se utility klase koje ulaze u taj layer tailwinda, i postaju deo njega, nema kombinacija
plain css - ne ulazi u tailwind, global ili module_ css 
-------------------
// @layer components
idu reusable klase koje mozes da koristis, npr. page-full-width
component level css, eto samo bez js logike
rules klase sa @apply ili css rules
klase postaju deo tailwinda
// @layer base
global rules - fonts, text-color, bg-color 
tag default styles 
theme css vars - semantic colors, spacings, radius
// @layer utilities
highest specificity 
--------------------
// https://tailwindcss.com/docs/hover-focus-and-other-states#differentiating-nested-groups
xl:group-hover/news-carousel:scale-110
---
// parent ima ovu klasu
group/{name} 
// children imaju ovaj modifikator, name i name se matchuju (news-carousel na parent i children)
group-hover/{name}:
----
poenta: dosta neocekivano, ima toga jos, za siblings npr., itd. 
-----------------------
// opet groups za astro navbar mobile menu
// styling-based-on-parent-state
// https://tailwindcss.com/docs/hover-focus-and-other-states#styling-based-on-parent-state
parent je oznacen sa group 
<a href="#" class="group ..." />
child ima state modifier, hover 
<h3 class="text-slate-900 group-hover:text-white ..." />
----------
// styling based on aria-states, aria-*
// https://tailwindcss.com/docs/hover-focus-and-other-states#aria-states
na tom istom elementu, default bg pa aria state bg override
<div aria-checked="true" class="bg-gray-600 aria-checked:bg-sky-700" />
--------
moze da se kombinuje aria state sa parentom group, primer iz astro-cactus 
<button aria-expanded="false" />
<svg class="group-aria-expanded:scale-100 ..." />
--------------------------
--------------------------
// my blog styling prose class
// React component vs @layer components vs @layer base h {...} vs --tw-prose-headings typography
for h1, h2... 
// https://tailwindcss.com/docs/reusing-styles
abstraction - css @layer component btn klasa + @apply, ili html + css React componenta
btn klasa - samo css bez html, ne sadrzi strukturu, html moras da ponavljas opet 
za male stvari gde samo css nekapsulira sve, 1 tag
inace component - template, html + utility classes 
-----
za prose markdown html isto ne kontrolises strukturu pa ima smisla samo css tagovi i klase
----------
// za h1, h2, opcije:
1. Napravis <Heading variant="h1" /> React komponentu
// nevezano za typography temu, prose class
2. definisies stilove za h1 {...} u @layer base u Tailwind
3. definises .my-heading-1 klasu u @layer componets u Tailwindu
----
// ovo je vezano samo za temu, gde je applied prose klasa
4. predefinises typography.DEFAULT.css.h1 pa rules, CSS varijablu u temi za Tailwind typography plugin 
--tw-prose-headings je color vrednost, css variable, samo
---------------
// zapamti, vazna poenta
prose ide za markdown ili cms html nad kojim nemas kontrolu da meces klase 
samo blog md clanak
description u post card je opet markdown, html tagovi, a ne string
moze li string od md u astro? tesko
------
// imaju prose SAMO oko blog clanka 
astro-cactus - layouts/BlogPost.astro // layout za clanak stranicu
astro-paper - styles/base.css, @apply prose-headings:!mb-3, tu je definisana nova prose klasa
applied in layouts/PostDetails.astro, and on about page // layout za blog post kao i obicno
// prose-headings klasa kao prefix???
// to su element modifiers, za selektovanje tagova, h1, a,... // ok, eto
// https://github.com/tailwindlabs/tailwindcss-typography#element-modifiers
prose-headings:{utility}
----
astrowind - components/blog/SinglePost.astro - blog post layout prakticno, i u pages.md MarkdownLayout
ovde sve prose klase u jsx tag kao kod paularmstrong.dev
----
u astro page.md moze layout da bude definisan u frontmatter // zapazi, zanimljivo
// src/pages/terms.md
---
title: 'Terms and Conditions'
layout: '~/layouts/MarkdownLayout.astro'
---
ovidius-astro-theme - blog post wrapper, page md wrapper i card descriptions
---------------
// poenta: prose ide samo oko blog posta - html-a koji nema klase // ok
prose je za markdown 
@layer i jsx je za tvoj jsx
kako drzati stilove u sync izmedju tvog custom html-a i golog markdwon html-a?
isti stilovi definisani na 2 mesta???
-----------
// states, modifiers, hover:
// https://tailwindcss.com/docs/hover-focus-and-other-states
modifikatore na svojim klasama mozes samo ako su definisane u @layer 
lg:my-btn 
----
// modifikatori:
1. Pseudo-classes - :hover, :focus, :first-child, :required, ...
2. Pseudo-elements - ::before, ::after, ::placeholder, ::selection, ...
3. Media and feature queries - lg:, xl:, dark: ...
4. Attribute selectors - [dir="rtl"], [open], aria ... 
----
// group - based on parent state 
group-hover:text-white
// peer - based on sibling state
peer-invalid:visible
// direct children
*:rounded-full
// has - based on children (descendants)
has-[:checked]:bg-indigo-50
-----------
// typography plugin ima element modifiers, ok
// https://github.com/tailwindlabs/tailwindcss-typography#element-modifiers
prose-headings:{utility}
-------------------
// @layer base vs h1 global
// https://tailwindcss.com/docs/adding-custom-styles#using-css-and-layer
redosled pojavljivanja odredjuje specificity, zadnja statement pobedjuje
zato ima 3 @layera
1. @layer direktiva kopira kod na mesto @tailwind direktive, redosled // zapazi ovo
2. omogucava modifiers, lg:my-btn 
3. omogucava tree-shaking za unused classes 
-----
truly custom CSS rules direktno u css bez @layer 
custom - ne koristi vars tailwinda, nije deo design sistema // eto
---
znaci sve treba u @layer, samo sto nema veze sa tailwindom ide u obican globalan css
-----
1. defaults for the page (za STRANICU) - classes u jsx, html
2. default base styles for specific HTML elements (tags), use the @layer base // ok, znaci to ide za h1, a, ...
@apply u css fajlovima je isto sto i theme arg u .js config // zapazi
3. @layer components - btn, card... za stilove koji jos mogu da se overriduju
----
znaci za h1 treba @layer base i @apply // ok
--------------------
// @layer, ono sto sam i ja zakljucio, docs
@layer directive helps you control declaration order by 
automatically relocating your styles to the corresponding directive, 
and also enables features like modifiers and tree-shaking for your own custom CSS
----
putting styles inside a layer you are instructing Tailwind to 
inject those styles into a specific position in the final css file
----
control how the styles cascade
---- 
// final output css // ovo je odlicno i vazno
/** reset styles **/
/** layer preflight styles **/
/** base styles **/
/** layer base styles **/
/** layer component styles **/
/** utility styles **/
/** layer utility styles **/ 
-------------------
// add xs breakpoint
// must not use extend, will add xs to the end
// https://tailwindcss.com/docs/screens#adding-smaller-breakpoints
theme: {
  screens: {
    xs: '475px',
    ...defaultTheme.screens,
  },
  ... 
}
------
// isto je i za medju-tacku, moze spread
screens: {
  xs: '475px',
  // matches max-w-4xl
  md2: '896px',
  ...defaultTheme.screens,
},
---------------------------
// container
// https://tailwindcss.com/docs/container
do md je fluid, iznad md snapuje (max-w) containera na floor (min-w) viewporta // zapazi, fora
<div class="md:container md:mx-auto px-4 md:px-0" /> // u principu ovo
------
centriranje (mx-auto) i px se konfigurisu u tailwind.config.js i onda koristis samo klasu container // ok
------------
poenta containera sa svim breakpoints podesis sadrzaj samo za tacke containera, nije fluid range
-----
u principu meni treba fluid sa jednim max-w-4xl, bez snapping
----------------
// fluid with single max-w, konacan, malo komplikovaniji
<main class="flex-grow w-full max-w-4xl md:mx-auto px-4 lg:px-0" />
max-w-4xl - 896, mora custom jer md - 786, lg - 1024
// fora, onda ok
md:mx-auto - centriranje mora ranije od 4xl 
lg:px-0 - sklanjanje padinga mora kasnije od 4xl
------------------------
// dark mode, dark: modifier
// https://tailwindcss.com/docs/dark-mode
// tailwind.config.js
darkMode: ['selector'], // ovo odredjuje samo kad se dark: modifier aktivira
mode - default (light) and dark:
theme - konkretne boje definisane na selektore u root, svaka tema ima default i dark: mode 
------
znaci u tailwind.config.js stavi samo 'selector' samo za dark: mode, a u @layer base stavi color theme 
:root[data-theme='blue'] {...}
:root.dark[data-theme='blue-dark'] {...} // tema ima dark mode
:root[data-theme='green'] {...}
i togglujes oba u ThemeSwitcher componenti
------
'selector' - isto sto i 'class' samo where: pseudoclass za specificity 
--------------------
tailwind ima set osnovnih elemenata kroz koji je sve modelovano, pa nihove kombinacije
md:bg-px-color-500 itd, stablo
--------------------
// css variable arbitrary value with media query
// https://tailwindcss.com/docs/adding-custom-styles#arbitrary-properties
moze bilo koji css rule sa modifiers, moze i custom modifier 
// css var
<div class="[--scroll-offset:56px] lg:[--scroll-offset:44px]" />
// css rule
<div class="[font-size:16px] hover:[font-size:18px]"></div>
// modifier
<li class="lg:[&:nth-child(3)]:hover:underline">{item}</li>
------------
global.css van @layer base je za kod nezavisan od tailwinda 
-----------------
// tailwind screens (breakpoints)
// https://tailwindcss.com/docs/screens
// theme.screens.sm ... 
screens: {
  'xs': '475px', // izostavljena, ja dodao
  'sm': '640px', // ovih 5 ispod su default ukljucene
  'md': '768px',
  'lg': '1024px',
  'xl': '1280px',
  '2xl': '1536px',
}
------------
// https://tailwindcss.com/docs/responsive-design#arbitrary-values
// za range
<div class="md:max-lg:flex" /> // max-*
// custom one-off breakpoints, i min i max // zapazti, eto ima
<div class="min-[320px]:text-center max-[600px]:bg-sky-300" />
--------------
// adding custom modifiers (variants) - plugin sa addVariant fjom
// prose-img: npr 
https://v2.tailwindcss.com/docs/plugins#adding-variants
// primer
const plugin = require('tailwindcss/plugin')

module.exports = {
  plugins: [
    plugin(function({ addVariant, e }) {
      addVariant('disabled', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.${e(`disabled${separator}${className}`)}:disabled`
        })
      })
    })
  ]
}
----------------
// arbitrary media queries
// min-[] i max-[], odnosi se na @media (min-width: 768px) {...} i @media (max-width: 768px) {...}
// md: je min-md prakticno
<div class="min-[320px]:text-center max-[600px]:bg-sky-300" />
// custom modifier, ne sme space iza max-width: // zapazi, tacno, probao
class="[@media(max-width:768px)]:border-red-500"
----------------
// reference theme values in js
// https://tailwindcss.com/docs/configuration#referencing-in-java-script
// https://www.codu.co/articles/referencing-tailwind-values-in-javascript-k-vzdccl
import resolveConfig from 'tailwindcss/resolveConfig';
import tailwindConfigFile from '../tailwind.config.js';
export const tailwindConfig = resolveConfig(tailwindConfigFile);
---
// losi tipovi, unije
const lgBreakpoint = parseInt(
  (tailwindConfig?.theme?.screens as { lg: string })?.lg
);
mora parseInt() jer vraca sa 'px' '1024px'
parseInt() hvata numericki deo, ostalo ignorise 'px'