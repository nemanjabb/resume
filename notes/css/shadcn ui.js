// shadcn semantic colors explained with examples
https://ui.shadcn.com/docs/theming#css-variables
------
// folder structure
https://ui.shadcn.com/docs/installation/next#app-structure
components/ui - shared components 
components - widgets - koje se samo jednom koriste
-----------
// install in next.js, with npm, can use yarn
// 1. install next.js project
npx create-next-app@latest my-app --typescript --tailwind --eslint
// 2. init shadcn
npx shadcn-ui@latest init
// 3. configure components.json - answer questions
// 4. set font
// 5. add component
npx shadcn-ui@latest add button
----------
// astro install
https://ui.shadcn.com/docs/installation/astro
----------
// components.json opcije, ovde objasnjene
https://ui.shadcn.com/docs/components-json
--------------------
// examples usages
https://ui.shadcn.com/examples/mail
https://github.com/shadcn-ui/ui/tree/main/apps/www/app/examples
------------------
// toast shadcn
// pogledaj docs
https://ui.shadcn.com/docs/components/toast
// needs <Toaster /> in RootLayout
import { Toaster } from "@/components/ui/toaster"
<Toaster />
-----
default duration 3 seconds
ima jos nasledjeni props iz radix primitive
-----
// najcesci args
toast({
  variant: "destructive",
  title: "Uh oh! Something went wrong.",
  description: "There was a problem with your request.",
  action: <ToastAction altText="Try again">Try again</ToastAction>,
})
------------------
mnogo toga u stilovima se svodi na odluku sta je globalno - default ili varinat komponente
a sta ad hoc className na instanci komponente 
radius, padding, bg-color, state color, palette... 
----------------
// svg icon fill color
<Image src="img.svg" /> mece svg u <img /> tag i ne moze boja da se postavi
mora kao React component sa fill="currentColor" ili loader u webpack ili babel
----
// svg to React component tool 
// https://transform.tools/
ostavlja samo 1 fill=""
----
// u shadcn je ovaj fajl components/icons.tsx
import { LucideProps } from 'lucide-react';
{
  // ... object
  gitHub: (props: LucideProps) => (
    <svg viewBox="0 0 438.549 438.549" {...props}>
    <path
      fill="currentColor"
      d="..."
      ></path>
  </svg>
),
}
----
// upotreba, boji se kao text
// moze da se izostavi za default boju sa <body /> taga, najcesce
import { Icons } from '@/components/icons';
<Icons.levelUpGaming className="h-16 text-foreground" />
-------
// theme, color palette
// https://ui.shadcn.com/docs/theming
foreground for accent, secondary, danger je isti
malo svetliji (na dark temi, naglaseniji) od base foreground
---
primary-bg mu je isti kao accent-foreground
--card-foreground je isti kao base --foreground
// u dark
--accent-foreground: theme('colors.slate.50');
--foreground: theme('colors.slate.100');
---
--muted-foreground je malo tamniji
------
// poenta:
radi se o naglasavanju i sakrivanju stvari, muted i accent 
debugiraj sa crvenu boju gde se sve javlja neka boja, npr. --muted-foreground 
-----------
pogledas radiuse na svim elementima u figmi i onda vidis koji je opseg za sm, md, lg
-----
zavise svi od jednog, podesis samo --radius
borderRadius: {
  lg: `var(--radius)`,
  md: `calc(var(--radius) - 2px)`,
  sm: 'calc(var(--radius) - 4px)',
},
-----------
// set default theme 
// https://ui.shadcn.com/docs/dark-mode/next
// ovaj paket
import { ThemeProvider } from "next-themes"
----
ima attribut na provideru, ne trba setTheme() // zapazi
<ThemeProvider attribute="class" defaultTheme="system" enableSystem disableTransitionOnChange / >

