------------
  inline - flex - container je inline
------------
  display: flex changes behavior of the margins
------------
  use paddings on container more often instead margins on children, whenever is a box
-----------
// svi osim zadnjeg
.this - is - child: not(: last - child){
  ----------------
    // position: sticky
    An element with position: sticky; is positioned based on the user's scroll position.
A sticky element toggles between relative and fixed, depending on the scroll position.It is positioned relative until a given offset position is met in the viewport - then it "sticks" in place(like position: fixed).
-----------------
    position: relative is static with top, left, right, bottom
---------------
    flex - grow: 0; // default
  flex - shrink: 1; // default // ne rastu a skupljaju se
definise se NA ITEMU
koliko brzo taj item raste / se skuplja RELATIVNO U ODNOSU na druge iteme
medjusobna raspodela preostalog prostora medju itemima, ne moze apsolutna duzina svih, za to flex - basis
  ---
    flex: 0 1 auto; // default //  grow, shrink, basis
  flex: 1; = 1 1 0 %; // Let all the flexible items be the same length, regardless of its content, da raspodele prostor 
  // nula je nula
  ----
    flex - basis: auto; // default
inicijalna sirina / visina itema u length unit(px, %)
ako je auto onda length itema ili njegov content
  ---------------
    space - between 0, space - around 1 / 2, space - evenly 1, samo razlika u praznom prostoru NA KRAJEVIMA
kad flex - grow: 1; nema praznog prostora, pa nema efekta
  ---------------
    ---------------
      // svg mozes da bojis kao font sa color: blue;
      // dobro
      fill="currentColor"
  -----
    moze i kroz css sa fill ili stroke na < path /> ili < svg />, stroke: blue; to malo neprakticnije
  ---------------
    initial value za svaki css property treba da znas da bi mogao unset
  min - height: auto
  ----------------
    mix - blend - mode: hard - light; // ovo je rupa zapravo
  inset: 10px; // kao margin ali od parenta, ne od childa, rupa
  ----------------
    svg ikonu bojis sa color: red, kao text
  tw - text - success - 500
  ----------------
-----------------
    // novi red u tekstu u jednom tagu \n
    description: 'Please enter the address where\ndeliveries can be made.',
      white - space: pre;

  <br /> ubacuje nov red u textu, pros, moze i u tekstu ne samo izmedju tagova
  -------------
    currentColor samo na path, ne na svg, tj samo koji su imali neku boju postavljenu
na svg ostaje fill = "none"
  ------------------
    mix - blend - mode: has problem in Chrome when background - color is on < body > tag or not set at all
  ------------------
// css animacije
@keyframes - programiras stanja u vremenu i to je to, pros
they are global, not scoped
  ease -in najduze na pocetku
  ease - out najduze na kraju
ease i ease -in -out najduze na sredini animacije, ako imas 50 % u keyframes onda je tu najduze(kraj prakticno), zapazi

  animation: tour - btn - flashing 2s ease infinite

  @keyframes tour - btn - flashing
  0 %
    box - shadow: 0 0 0 0rem rgba(65, 203, 206, 0.5)
  50 %
    box - shadow: 0 0 0 1rem rgba(65, 203, 206, 0.5)
  100 %
    box - shadow: 0 0 0 0rem rgba(65, 203, 206, 0.5)

  // https://stackoverflow.com/questions/9629346/difference-between-css3-transitions-ease-in-and-ease-out

  ease -in will start the animation slowly, and finish at full speed.
    ease - out will start the animation at full speed, then finish slowly.
      ease -in -out will start slowly, be fastest at the middle of the animation, then finish slowly.
ease is like ease -in -out, except it starts slightly faster than it ends.
linear uses no easing.	
------------------
    white - space: normal | nowrap | pre | pre - line | pre - wrap | initial | inherit;
pre 	Whitespace is preserved by the browser. // cuva indentaciju
nowrap 	Sequences of whitespace will collapse into a single whitespace. // merguje vise whitespace u jedan
    whitespace - blanko, tab, newline
  ------------
    // vise transitiona se odvajaju zarezom
    transition: height 0.3s ease - out, opacity 0.3s ease 0.5s
  // toggluj property u browser inspectoru da vidis da li je postepen, ima transition, fora
  -----------------
    // jeste fora, z-index postuje stabla
    izgleda da se z - index postuje samo u podstablu
ako parent ima z - index: 2 sva njegova deca ce za njegove siblinge ili parente imati 2, iako sama deca imaju vise, 5 npr
  -----------------

    // remove box-shadow from top side of the div
    // https://stackoverflow.com/questions/19428584/remove-box-shadow-from-only-top-of-div/19428912

    box - shadow: [horizontal - offset][vertical - offset][blur](optional)[spread](optional)[color]

  // would be a shadow with 5px vertical offset, effectively pushing the shadow down
  // ali zato donja senka malo veca
  box - shadow: 0px 5px 8px rgba(0, 0, 0, 0.3); // ovaj 5px

// element sa malo manjom visinom pa pokrijes gornju senku pseudo elementom
::before pseudo element je dete divu
radi ovo, koliko je visok element toliko ga pomeri na gore
absolute position top: 0 left: 0 u odnosu na parent koji ima relative, zato - 7
div {
    position: relative;
    background - color: white;
    box - shadow: 0 7px 20px 0 rgba(0, 0, 0, .4);
  }

  div:before {
    content: "";
    width: 100 %;
    position: absolute;
    height: 7px;
    top: -7px; // koliko je visok toliko ga pomeri
    background: inherit;
    z - index: 2;
  }

  -------------
    // will-change css
    za izostravanje animations / transitions
  --------------
    stacking context ?
      ---------------
        // links styling
        The four links states are:

  a: link - a normal, unvisited link
  a: visited - a link the user has visited
  a: hover - a link when the user mouses over it
  a: active - a link the moment it is clicked
  ---------------
    // 3 koordinate
    inset: 10 % 5 % -10 %;   /* top left/right bottom */
  translate(x, y);
  ------------------
    // assign global scss variable !global
    // http://www.sass-lang.com/documentation/variables
    $variable: first global value;

.content {
    $variable: second global value!global; // to je fora
    value: $variable;
  }

.sidebar {
    value: $variable;
  }
  -----------------
    // currentcolor keyword
    div {
    color: blue;
    border: 10px solid currentcolor; // referencira na sta je color na tom elementu
  }
  ------------------
    min - height: 0 // default value
  max - height: none // default value
  ------------------
    initial je default u css - u
  ------------------
    // 2 max-height, u vh i px, dobar, min() fja kao calc(), pogledaj dostupne fje
    max - height: min(95vh, 700px);
  ------------------
    Flex items margins won't collapse.
  -----------------
    u inspectoru mozes direktno da pises pravila u media queryijima
  ----------------
    prouci tailwind
  ------------------
// iOS only selector
@supports(-webkit - touch - callout: none) {
    /* CSS specific to iOS devices */
  }
  ------------------
-------------------
    // css velicina za slike
    aspect - ratio: 16 / 9;
  ----------------
    // cascading
    // CSS: The cascade, specificity and inheritance
    // https://www.youtube.com/watch?v=TjMlfWpw7E0
    algorithm that that resolves conflicts by deciding which rule wins, calc weight
  1. origin - browser default, user defined, author
  2. selector specificity, redosled u code
  3. order of appearance
  4. initial and inherited - css defaults

sve mora da se resolvuje na kraju, makar na css defaults
zadnji u kodu pobedjuje samo ako je isti specificity, specificity vazniji od redosleda
linkovan css fajl posle style taga ima prednost jer je zadnji
--------------------------
--------------------------
za navbar ide position sticky, not fixed, onda ne treba top margina za content, fora // ipak ne za mobile
onda hamburger mobile nav gura sadrzaj pa mora fixed, kad se menja position cela strana trepce
najbolje oba fixed, a layout content padding-top, a ne margin-top
jer padding ulazi u visinu za min-height layouta
---
fixed i absolute prave div preko diva, sticky zadrzava flow, kao static
fixed je absolute u odnosu na viewport, a ne na relative parent
-------------
// css, scss variables
ovo mozes da menjas samo iz js dom, iz css selektora ne moze
u child selectoru kreira novu lokalnu var, ne dodeljuje globalnu
:root {
  --navbar-height: 3.5rem;
}
document?.documentElement.style.setProperty('--navbar-height', '0');
moze i na nekom MediaElementAudioSourceNode, ne mora root
----
scss vars su compile TimeRanges, nikakvi eventi ne moze
ponesto sa mixin
------------------
// center pure text content in span with new lines
'Click or \n drag&drop' // \n instead <br />
white-space: pre-line; // da se \n interpretira
text-align-last: center; // za patrljak
text-align: center; // za duzi text
// bez width in px
-------------------
// 100% height html, body, content
// dobar
// https://stackoverflow.com/questions/67675651/div-height-100-not-working-with-body-min-height-100vh
// The answer is simple as percentage min-height is calculated based on 
// parent container's height value not min-height.
min-height izracunava se od deklarisan height parenta, ne min-height, ne stvarni height
min-height: 100vh; // parent
min-height: inherit; // content
--------------------
// responsive width
// postavis widht: 100% pa max-width ogranicavas
w-full sm:max-w-xs md:max-w-sm ...
--------------------
// <input type="text" /> width
obican width: 100% overriduje size=40 (karaktera) ili cols=30
// https://www.w3schools.com/tags/att_input_size.asp
Default value is 20
----------------------------
// style links
// https://www.freecodecamp.org/news/how-to-style-links-in-css/
// https://css-tricks.com/css-basics-styling-links-like-boss/
a:link - a normal, unvisited link
a:visited - a link the user has visited
a:hover - a link when the user mouses over it
a:active - a link the moment it is clicked
----
a:hover MUST come after a:link and a:visited
a:active MUST come after a:hover
-----
// redosled u praksi
/* unvisited link */
a:link {  color: red;}

/* visited link */
a:visited {  color: green;}

/* mouse over link */
a:hover {  color: hotpink;}

/* selected link */
a:active {  color: blue;} 
----------------
// focus 5. state select sa tab key
// u principu isti stil stavis za hover i focus 
Focus (:focus): Like :hover but where the link is selected using the Tab key on a keyboard.
Hover and focus states are often styled together.
a:hover, a:focus {...}
------------------
// css chrome graficka kartica test
chrome://gpu/
---------------------
// cudan space na inline elementima, i flex, <a/> npr
// https://stackoverflow.com/questions/48117071/element-with-display-inline-flex-has-a-strange-top-margin
inline elementi imaju default vertical-align: baseline - donje ivice teksta se poravnavaju, a po njima i kontejneri
// solutions
1. vertical-align: top // ili bilo koij drugi
2. display: flex // na kontejneru, flex ITEMS nisu inline nego block
3. moze i display: block ako je vec flex item
---
// poenta
vertical-align: baseline - default na inline elem, block bilo koji deaktivira v-a
----------------------
// calc in css must have spaces around -/+/*...
--layout-content-min-h: calc(100vh - var(--real-navbar-height) - var(--footer-height));
// same in tailwind [] must not have spaces
// tailwind usage with var, doesnt need calc() calc(var()) for single var
@apply min-h-[var(--layout-content-min-h)];
-----------------
// first element with a class
// moras u okviru parenta da selektujes klase
cy.get('.my-parent .my-child:first-child')
-----------------------
for min-height: 100%; all parents must have height: 100%; ili taj parent da ima neki height
ili flex-align: stretch;
-------------------------
// pitaj, skeleton za video ili sliku
sirina i visina dolaze iznutra, iz kontenta (video, slika), kako imitirati to?
skeleton kako postaviti width, height kao % without hardcoding width height
set blank content in %
------
prouci loader i lazy load za Next.js Image
-------
// keep aspect ratio, vrh 
aspect-ratio: 1 / 1; // ima space izmedju
https://stackoverflow.com/questions/1495407/maintain-the-aspect-ratio-of-a-div-with-css
----
vh (od height) moze da stavis na width, zapazi, logicno
--------------------
// george moller
base 
  reset.css 
  typography.css 
components 
  button.css 
  dropdown.css 
layout 
  navigation.css 
  header.css 
  sidehar.css 
utils 
  variables.css 
  utilities.css 
vendors 
  bootstrap.min.css 
pages 
  login.css 
  contact.css 
main.css 
---------------------
// :focus vs :focus-visible pseudo-classes
focus from mouse and tap, focus-visible from keyboard navigation



