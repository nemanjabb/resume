// Class Variance Authority 
https://github.com/joe-bell/cva
je za varijacije komponenti, link-heading, link-default, link-category... // zapazi
// ovde su primeri upotrebe, prouci
https://github.dev/shadcn-ui/taxonomy
----
prvi arg - bezuslovne klase, kad su samo one, - defaults
variants - basic variante, glavno, moze polje ili string klasa
size - ide posebno
compoundVariants - presek 'and' od variants, kad su obe varijante ispunjene
defaultVariants - default args za button() bez args - koje od postavljenih intent i size postavljas kao default
---
compoundVariants: [
    {
      intent: "primary", // variant 1
      size: "medium", // variant 2
      class: "uppercase", // apply this class when both variants are set
    },
  ],
------
moze kombinacije varianti, ne samo jedna od njih // ok
-----------------------
-----------------------
// docs, nema mnogo, nije velika lib, utility library
// variants
// https://cva.style/docs/getting-started/variants
slicna kao clsx, mala util lib, koja ne prima samo polje nego:
1. uncoditional classes
2. intent, size // ovo su args u poziv samo
3. preseke
4. ima postavljen defaults
5. additional passed ad hoc classes // i ovo je arg
------
ali ima uncoditional, intent and size variants, combinations, and additional ad hoc passed classes
-----
// typescript
// https://cva.style/docs/getting-started/typescript
// vadi tipove od native button
export type ButtonProps = VariantProps<typeof button>;
----
// postavljanje required varinats
export interface ButtonProps
  extends Omit<ButtonVariantProps, "required">,
    Required<Pick<ButtonVariantProps, "required">> {}
-----
kad pozoves rezultat cva(), npr toggleVariants() dobijes string klasa 
zapravo matricu sizes and intents // to
------
// Astro primer
https://cva.style/docs/examples/astro
default variants se postavljaju u Astro.props destructuring
const { intent = "primary", size = "medium" } = Astro.props;
default se postavlja odvojeno za size, intent i ukupni za oba, za component
// npr button
size - default medium 
intent - default primary
default button - size="medium", intent="primary" // oba
----
// size y osa, intent x osa
default btn         , default-size-intent1, default-size-intent2
default-intent-size1, size1-intent1       , size1-intent2
default-intent-size2, size2-intent1       , size2-intent2
-----
compoundVariants: [{ intent: "primary", size: "medium", class: "uppercase" }],
postavlja uppercase samo za intent="primary" size="medium" presek 
a inace "medium" je default size i "primary" je default intent pa su svi defaults uppercase
--------
// poziv u Astro
---
const buttonVariants = cva("classes...", {
    variants: { 
      intent: {
        primary: ['classes...'],
        secondary: ['classes...'],
      },
      size: {
        small: ['classes...'],
        medium: ['classes...'],
      },
    },
    compoundVariants: [{ intent: "primary", size: "medium", class: "uppercase" }],
  });
// nasledjuje oba, native button i cva props variants // ok, zapazi
export interface Props
  extends HTMLAttributes<"button">,
    VariantProps<typeof buttonVariants> {}

// postavljanje default variants - props 
const { intent = "primary", size = "medium" } = Astro.props;

// poziv // eto 
// rezultat je funkcija koja vraca string spojenih klasa // eto, najvaznije
<button class={buttonVariants({ intent, size })}>My Button</button>
------
// important, posle probe
1. cva('', {...}) MUST accept first argument (or empty string) or type error 
2. keys unutar variants mogu nazvani bilo kako i bilo koji broj, ne mora intent i size 
bolje variant umesto intent, onda prop i tip ima variant <Link variant="link-nav" />
zapravo tip variants: {...} objekta se pojavljuje u props, koji god da je // ok, jasno
3. ne treba ti cn() za className, ovaj ga vec prima ({class | className})
linkVariants({ variant, className })
----
// prima varaints from props, a postavljeni i defualt
// koje klase uslovno da vrati za prosledjene variants
const classes = button({ intent, size })
----------------
// twMerge cn() kad god spajas klase, pros
// https://github.com/shadcn-ui/taxonomy
className={cn(buttonVariants(), className)}
------
// arguments
// https://cva.style/docs/api-reference
const component = cva("base", options); // base - clsx syntax {'my-class': true, true && 'my-class', etc}
// options je prost zapravo
{
    variants,
    compoundVariants,
    defaultVariants
}
// cx() heleper je clsx zapravo
const className = cx(classes);
-------
props ide na kraju, jer je class_ destrukturiran ispred, ...restProps
const { class: className, variant = 'link', ...props } = Astro.props;
<a class={linkVariants({ variant, className })} {...props} />
-------
// boolean variant, npr disabled
variants: {
  ...
  disabled: { true: "btn-disabled", },
},
-------
// Cn, twMerge, clsx, cva - Write Clean Tailwind Code with these utility functions - webdecoded 
// https://www.youtube.com/watch?v=h3s47owx8io
-----------------------------
-----------------------------
// MobX
state ti je u klasama, koje mogu da se nasledjuju
akcije su metode klase
computed su getteri 
---------------------
// state libraries, pregled
https://twitter.com/housecor/status/1768997355779428678
--------------------
// cva, compound variants
kad hoces da kombinujes button variant (outline, solid) i colors (primary, neutral)
za outline moras da postavis text-primary, znaci vec ukljucuje boju
zato kombinacije moraju eksplicitno da se definisu, ne moze sa override
// variant1, variant2, klasa // zapazi
compoundVariants: [{ variant: 'outline', colors: 'primary', class: 'button-outline-primary' }, ...]
// 
.button-outline {...}
.button-solid {...}
/* compound variants */
.button-outline-primary {...}
.button-outline-neutral {...}
a nema .button-primary {} // zapazi
