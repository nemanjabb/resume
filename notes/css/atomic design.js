
//atoms
atoms include basic HTML elements like form labels, inputs, buttons, and others that can’t be broken down any further without ceasing to be functional.

//molecules
molecules are relatively simple groups of UI elements functioning together as a unit. For example, a form label, search input, and button can join together to create a search form molecule.

//organisms
Organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms. These organisms form distinct sections of an interface.

//templates
Templates are page-level objects that place components into a layout and articulate the design’s underlying content structure.

//pages
Pages are specific instances of templates that show what a UI looks like with real representative content in place.



Atoms are UI elements that can’t be broken down any further and serve as the elemental building blocks of an interface.
Molecules are collections of atoms that form relatively simple UI components.
Organisms are relatively complex components that form discrete sections of an interface.
Templates place components within a layout and demonstrate the design’s underlying content structure.
Pages apply real content to templates and articulate variations to demonstrate the final UI and test the resilience of the design system.


//advantages
One of the biggest advantages atomic design provides is the ability to quickly shift between abstract and concrete.
iterative process with a feedback loop

modularity, hierarchy

