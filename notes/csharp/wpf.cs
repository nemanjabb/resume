

/**
* WPF working notes. Autumn 2015.
*
*
*
*/




//http://stackoverflow.com/questions/19831896/how-to-disable-ctrla-and-ctrlc-in-richtextbox-in-wpf

private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
{
	if (e.Key == Key.C && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
	{
		MessageBox.Show("CTRL + C was pressed");
		e.Handled = true;
	}
}
//i povezes hendler na kontrolu u editor na PreviewKeyDown event

//ista komanda (copy) moze na vise evenata da se poziva, ctrl c ili desni klik, onda overajdujes copy komandu a ne event
------------
TextInput event se podize jednom za alt123, a 4 KeyUp events

Determining which key was pressed test the e.Key

//razlika
PreviewTextInput  – tunneling
TextInput – bubbling

---------------
// http://stackoverflow.com/questions/4616694/what-is-event-bubbling-and-capturing

//up
With bubbling, the event is first captured and handled by the innermost element and then propagated to outer elements. //uz stablo do root

//down
With capturing, the event is first captured by the outermost element and propagated to the inner elements.//obilazak nanize podstabla od datog elementa //delegirani

dovoljno 1 da uhvatis

//The difference is the order of the execution of the event handlers


--------------------
// https://msdn.microsoft.com/en-us/library/vstudio/ms742806%28v=vs.100%29.aspx

 A routed event is a type of event that can invoke handlers on multiple listeners in an element tree, rather than just on the object that raised the event. 

--------------------
//tab order
//http://stackoverflow.com/questions/359758/setting-tab-order-in-wpf
KeyboardNavigation.TabIndex="0"
KeyboardNavigation.IsTabStop="False"

//alt hotkey
//http://stackoverflow.com/questions/10452462/make-a-hotkey-to-focus-a-textbox-in-wpf
<Label Content="bilosta_Label" Target="{Binding ElementName=SomeTextBox}" /> //na alt L
<TextBox Name="SomeTextBox" />

-------------------
//custom command
http://stackoverflow.com/questions/601393/how-do-i-add-a-custom-routed-command-in-wpf

mora da rebildujes klasu pre nego krenes da dodajes u xml

xmlns:w="clr-namespace:WpfApplication1"
<Window.CommandBindings>
	<CommandBinding Command="w:Command.DoSomething" Executed="CommandBinding_DoSomething" />
</Window.CommandBindings>
<Window.InputBindings>
	<KeyBinding Modifiers="Ctrl+Shift"
				Key="N"
				Command="w:Command.DoSomething" />
</Window.InputBindings>
	
<MenuItem Name="Menu_DoSomething" InputGestureText="Ctrl+Shift+N"  Header="Do Something" Command="w:Command.DoSomething" />

--------------

iza reci Binding ide fja iz cedebehind ili namespace: ako je iz drugog
<Button Command="{Binding DataInitialization}"

----------------
DIP 1/96inch, device independant pixel, isti na svim
--------------
ButtonCommand="{Binding HidePopupCommand}" binding ide preko dependacy propertyja
dependancy property dinamicki inject prop u objekat

binding nije radio propertiji trebali relativno da se proslede u xaml, i komande i argumenti
<Button DockPanel.Dock="Right" Content="X" Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type UserControl}},Path=Command}"/>
<TextBlock Text="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type UserControl}},Path=Title}"/>

kad pravis viewmodel.cs moras binding u reservation da dodas i property u reservationviewmodel
--------------------------
{  } je toString()-ovanje, tipova npr, kao i svaki template
--------------------------
attached property je spec slucaj dependancy propertyja za xaml
resurs je key value skladiste uz tag, tag.Resources je attached property
----------------------------
sirenje iznutra - verticalAlignment=streach na kontejener
dockpanel - vazan redosled, sibling zauzima sto preostane od prethodno navedenih, za sirenje mora lastChildFill=true
----------------------------
ctrl k d - formatira bilo koji kod
ctrl k c - koment
ctrl k u - unkoment
----------------------------
enum treba da bude u namespaceu
----------------------------
stack trace - najskorije pozvana fja na vrhu gore
ancestor - predak - parent, ne mora direktni, path=property na kontroli tipa AncestorType={x:Type UserControl}}
---------------------------
ostao key binding na esc u popupu
sirenje width 100% uvek sa novim dockapanelom lastchildfill
--------------------------
compile time provera tipova 

<Window.DataContext>
	<viewModels:MainWindowViewModel/>
</Window.DataContext>
--------------------------
viewmodel je prosireni model za view
x:Name="nesto" - imas nesto promenljivu u c#, hvatas

komande koriste - routedEvent - izmestena implementac iz kontrole u framework
logicki grupisani eventi, na vise razl dogadjaja 1 handler, ctr c ili desni klik copy
paralelna implement sa eventima
bubbling i tuneling kad je element u stablu - guiju - znaci obavestavaj roditelje (na gore (bubbling)), ili na dole (decu)
direct - samo njega, routingStrategy















