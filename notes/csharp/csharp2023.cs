// https://en.wikipedia.org/wiki/C_Sharp_(programming_language)

Roslyn - open source compiler, 2014
November 2022, C# 11.0, .NET 7.0, Visual Studio 2022 // latest
December 2023 C# 12.0, godina - 1
garbage collection
--------
boxing 
lambda expressions, extension methods, and anonymous types
---------
statements -> methods -> classes -> namespaces.
----------------
// features
// Portability
Common Language Infrastructure - CLI, decoupled, moze da kompajlira u bilo sta
// Typing
nema global functions, mora u klasi...
// Metaprogramming
Reflection, Expression trees, Attributes - metadata 
// Methods and functions
virtual, Extension methods, dynamic allows for run-time method binding, function pointers - keyword delegate, synchronized
// Properties
// Namespace
using - for import
// Memory access
pointers in unsafe blocks
// Exceptions
// Polymorphism
not support multiple inheritance, multiple interfaces
operator overloading
// Language Integrated Query (LINQ)
IEnumerable<T> interface
XML, ADO.NET dataset, SQL databases
// Functional programming
Functions as first-class citizen
Higher-order functions 
Anonymous functions
Closures
Type inference
List comprehension
Tuples
Nested functions
Pattern matching
Immutability
Type classes
-----------------
// Common type system
Categories of data types
1. Value types 
2. Reference types - referential identity, structural identity
// Boxing and unboxing
Boxing is the operation of converting a value-type object into a value of a corresponding reference type // eto
Unboxing - obrnuto naravno
----
// Example:
int foo = 42;         // Value type.
object bar = foo;     // foo is boxed to bar.
int foo2 = (int)bar;  // Unboxed back to value type.
------------------
// Examples
using System; // da mozes da ga izostavis za fully qualified imena, npr Console umesto System.Console
imports all types from the System namespace_
-----
static void Main() {} // static, da ne bi bila instanca programa
-----------------------
// Implementations
Roslyn - compiles into intermediate language (IL)
RyuJIT - JIT (just-in-time), od .NET 4.6
------------------------------
// C# interview questuons
// https://github.com/kansiris/C-Sharp-c-interview-question#
// https://www.reddit.com/r/csharp/comments/tgfqe8/advanced_cnet_interview_questions
// https://www.reddit.com/r/csharp/comments/dstu1b/examples_of_interview_questions_problems
---------------
ASP.NET Core Web API 
designed to build RESTful APIs using the HTTP protocol // REST endpoints in .NET, flask, express equivalent
------------------
// Entity Framework - Object-Relational Mapping (ORM) framework 
// features:
Object-Relational Mapping (ORM)
Code-First and Database-First Approaches
LINQ Integration
Automatic CRUD Operations
Change Tracking
Lazy Loading and Eager Loading
Inheritance Support
Database Providers
Migrations
Query Caching
-------
// schema definition, .cs files
1. Code-Based Configuration (Fluent API) - OnModelCreating() method
2. Data Annotations // npr [Column("FullName")]
-------------
// Linq - 2 sintakse
1. Query Syntax // gola
2. Method Syntax // uobicajena



