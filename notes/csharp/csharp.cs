
/**
* General C# language working notes. 2015.
*
*
*
*/

phonegap android js


get set su geteri i seteri samo u DEFINICIJI klase
get i set funkcije bas, samo promenjena sintaksa, mezes i fje da pozivas unutar getera setera

//properti
internal int ProcessTime
{
	get { return (int)DateTime.Now.Subtract(this.TimeStamp).TotalSeconds; }
}

//
static LogSettings settings;//bez ovoga ne bi bio read only
internal static LogSettings Settings//internal namespace "public"
{
	get
	{
		return settings ?? (settings = (LogSettings)ConfigurationManager.GetSection("logSettings"));
	}

}
Guid GUID = Guid.NewGuid();//guid ima i u c#// kao guid tip i u entity frameworku
generated unique identifier

//ako default get;set; ne moras field da definises ispred
//ako ima implem neku u get set, onda mora pre toga field da se definise
-----------

property je funkcija, get ili set, ali je funkcija
//interfejs moze da ima property

    public interface IHttpHandler
    {
        bool IsReusable { get; }//fja
	}
//klasa impl
	public class webServiceProxy : IHttpHandler
	{	
		//impl propertyja - postavljanje
		public bool IsReusable
		{
			get { return true; }
		}
	}
//ako koristis unmanaged resurse IDisposable using izjava
//unmanaged koji nisu NET, a implementiraju IDisposable interfejs
// da bi oslobodio memoriju Dispose()
-----------
//lambda
//samo uproscena sintaksa za anonimnu fju x=> x + x
//Lambda expressions are a simpler syntax for anonymous delegates and can be used everywhere an anonymous delegate can be used.
delegate int del(int i);//delegat je typedef za funkciju
del myDelegate = x => x * x;//del je tip
int j = myDelegate(5); //j = 25

//prosledi parametrex, y
(x, y) => { x.DoSomething(); y.DoSomethingElse(); }
----
// anonymous delegate
var evens = Enumerable
                .Range(1, 100)
                .Where(delegate(int x) { return (x % 2) == 0; })
                .ToList();

// lambda expression
var evens = Enumerable
                .Range(1, 100)
                .Where(x => (x % 2) == 0)
                .ToList();
				
-----------------
aspx - p=page
ashx handler
ascx control webform
-------------
return ctx.VCT_Users.Where(u => u.UserId == userId).Select(u => new User() { }//lambda u ulazni, iza => izlazni param tj return

---------------

var hex = new HttpResponseException(new HttpResponseMessage(((HttpWebResponse)wex.Response).StatusCode)
      {
       ReasonPhrase = ((HttpWebResponse)wex.Response).StatusDescription,
       Content = new StringContent(responseText)
      }); 

je isto sto i :
var hex = new HttpResponseException(new HttpResponseMessage(((HttpWebResponse)wex.Response).StatusCode);

inicijalizuje se hex objekat koji je vratio konstruktor
----------------------

			var user1 = ctx.VCT_Users.Where(u => u.UserId == user.UserId).SingleOrDefault();
			if (user1 == null) return 0;
			
			user1.Phone = user.Phone;
			user1.LockedDateTime = user.LockedDateTime;
			user1.MustChangePassword = user.MustChangePassword;
			
			//spoj user sa userRoles tabelom
			var userRole = user1.VCT_UserRoles.SingleOrDefault(ur => ur.VCT_Role.RoleName == "admin");

			VCT_Role role; 

			if (user.IsAdmin && userRole == null){
				role = ctx.VCT_Roles.Single(r => r.RoleName == "admin");
				user1.VCT_UserRoles.Add(new VCT_UserRole { VCT_Role = role });
			}
			else if (!user.IsAdmin && userRole != null)
				ctx.VCT_UserRoles.DeleteOnSubmit(userRole);
--------------
refleksija - obilazak nepoznatog objekta da vidis koje metode ima
--------------
late binding - utvrdjivanje objekta nad kojim se poziva fja u runtime, a ne compiletime, reflection takodje

This typically, but not always, comes about when a language supports inheritance/subtyping 
where a type T may be a subtype/child/subclass (terminology depends on language) of a type S.

polimorfizam, na metod klase ili podklase
binding - povezivanje fje na objekat
-----------------
kolekcija - polje ciji elementi imaju razlicite tipove 
----------------------

Regex allRegex = new Regex(@"code"":\s*""(\w{3})""(?:[^\[]*?)\[(?:[^\]]*?""(\w{3})""[^\]]*?)*", RegexOptions.Compiled);
var matches1 = allRegex.Matches(response);

var matches2 = matches1.Cast<Match>().Select(m => new
{
	From = m.Groups[1].Value,
	To = m.Groups[2].Captures.Cast<Capture>().Select(c => c.Value).OrderBy(c => c)
}).OrderBy(m => m.From);
-------------------------
(Input Parameters) => Method Expression
leva ulaz, desna izlaz (rez fje)
-------------------------

exception ne moras da prosledjujes, samo ako zelis da ga uhvatis
bacas sta zelis i hvatas sta zelis, custom
moze i iz catch da se baci
hvatanje vise hvatas Exception pa unutra proveravas koji

public class CustomException : Exception
{
	public CustomException(string message, Exception innerException)
		: base(message, innerException) { }
}
-----------------
atributi readOnly da mogu samo kroz konstruktor da se postave
enkapsulacija i ogranicenje

----------------
linq select je projekcija(vertikalno - kolone, kao sql)
where je horizontalno, selektuje rowove

tabela.Select(ulazni=>izlazni.nekaFja()) - primenjuje fju na sve rowove te kolone, izlazni izlazi iz Select fje
----------------------
Update-Package Selenium.Support -reinstall
kad fali refernecaa
on cita packages.config i dovlaci na build ako nema
------------

osnovna x = new izvedena();//obrnuto ne moze
return osnovna;
(izvedena)osnovna cast
(osnovna)izvedena nema potrebe, jer moze vec

-------------------------------
Select - projekcija, selektor
Where - selekcija, ide predikat, filter
--------------------------------
userDb.VCT_UserRoles.AddRange(rolesToAdd.Select(r => new VCT_UserRole { RoleId = r.RoleId }));
UserId dobija iz userDb, da je na ctx.VCT_UserRoles mora oba
--------------------------------
				
private int _UserId;
public int UserId { get { return _UserId; } }	

sa 	_UserId radis unutar klase, a spolja na objektu obj.UserId

i this.Properti zove getter


ili

private int myProperty;
public int MyProperty { get { return myProperty; } }
isto sto i
public int MyProperty { get; private set; }

property je funkcija koja se u kodu (sintaksno) ponasa kao promenljiva
zato moze u interfejsu

public class MyClass
{
    // this is a field.  It is private to your class and stores the actual data.
    private string _myField;

    // this is a property.  When you access it uses the underlying field, but only exposes
    // the contract that will not be affected by the underlying field
    public string MyField
    {
        get
        {
            return _myField;//poziva se sa leve str =// var x = MyField
        }
        set
        {
            _myField = value;//setProp(object value) //poziva se sa desne str =// MyField=x
        }
    }
}
---------------------------------	
finally se izvrsava i za izuzetak i kad nije	

using(var v1 = new v())		v1 se dealocira
				
backround worker koristi evente, ne sme da pipa kontrole na formi
napises u hendleru (doWork, report i completed) sta oces da se izvrsi umesto prosledjivanja funkcijskog pointera

klt + vise jezgara, blokiranje 1 niti ne blokira ostale
	- mora prelaz u kernel mode, sporije upravljanje u kernel modu
	
ult + promena niti ne zahteva prelaz u kernel mod, bilo koji os, biblioteka
	- sistem poziv iz jedne niti blokira ceo proces, 1 procesor
---------------------
EventHandler specijalni tip delegata (fje)

private EventHandler onValueChanged;
				
//nije funkcija, nije propertu nego event (custom)				
public event EventHandler ValueChanged {
		add {
			onValueChanged += value;//sve sto izvrsava statement u bloku je funkcija
		}
		remove {
			onValueChanged -= value;//malo o
		}
	}
//handler, moze da ima proizvoljan potpis
protected virtual void OnValueChanged(EventArgs e) {//veliko O
  if (ValueChanged != null) {//delegat poredi
	 ValueChanged(this, e);//this sender, e args//event je fja, tj delegat//delegat poziva
  }
}	

ValueChanged custom event				
OnValueChanged() event handler				


public delegate void MultiCBChangedEventHandler(object sender, MultiCBEventArgs e);				
public delegate void EventHandler(Object sender, EventArgs e);
public event EventHandler NoDataEventHandler;

----------

invokeRequried - false moze da se updateuje kontrola
za true, form.invoke()

	if (this.textBox1.InvokeRequired)
	{	
		SetTextCallback d = new SetTextCallback(SetText);
		this.Invoke(d, new object[] { text });
	}
	else
	{
		this.textBox1.Text = text;
	}
kontroli moze samo sa niti u kojoj je kreirana (main)
this.demoThread = new Thread(new ThreadStart(this.ThreadProcSafe));
prosledjujes fju u konstruktor, nema Run() telo
--------------
calling convention __std_call, ko je odgovoran za ciscenje podataka sa steka i argumenata
nit od staticke fje zbog scopea bezbednog, dodadne param prosledis kao objekt, klasa public samo thread safe atribute
---------------
//fora
sa leve strane = i sa desne pointer *, property, drugacije se ponasa pa teze za razumevanje, fora
-------------
keyDown, pa keyUp, pa keyPress, tim redom se poziva
isto i mouse
------------------	
operator overload, overload znaci radi za druge tipove, override predefinisanje je drugo			
-------------------
extenzion metode		

//obicna staticka fja koja se zove kao clanica, ne moze private poljima da pristupa
items.ToList().ForEach(i => i.DoStuff());

Alternatively, write your own ForEach extension method:

public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)//action delegat
{
    foreach(T item in enumeration)
    {
        action(item);
    }
}	
//podrska u kompajler, nivo jezika
--------------------
Select je projekcija (neki propertiji dodela ili citanje), where je selekcija, deo elemenata liste, filter		
reqTextBoxes.Where(t => t.ForeColor == Color.Blue).ToList().ForEach(t => t.ForeColor = Color.Black);
// ne moze select, foreach i moze samo na listi	
items.ToList().ForEach(i => i.DoStuff()); //fja je telo bloka prakticno				
array_map() u linq je ForEach()		
//npr, gde je if u petlji ides .Where()
results = matches.Cast<Match>().Where(route => (Regex.Match(route.Groups[1].Value, @"\b[A-Z]{6}\b").Success && !IsPresentInList(results, route.Groups[1].Value))).Select(r=>r.Groups[1].Value).ToList().Select(r => OrderRouteName(r)).ToList();
moze blok u lambdda {}, implicitno je 1 statement sa return, prima blok definiciju fje normalno
System.Action delegat je void fja(){...nesto globalno}
System.Predicate vraca bool
-------------------
funkcija je najobicniji tip (kompozitni) (ili objekat), gde seda ocekuju se nekoliko tipova, 1 za povratni i args
delegat jos iz C				
--------------

mono je samo .NET framework na linuxu, samo to	
direct x nema na linuxu			
unityscript ima 3 verzije javascript, c# i boo (python)
API - layered arhitekturni model				
				
------------
http 500 je nije naso masinu na mrezi ni dns resolvovao
400 je naso masinu ali resurs na serveru ne radi
-------------
cuvanje stanja prozora, potprozora, mvvm view model, dodatni model na klijentu
-------------
//factory pattern - metoda koja na osnovu argumenta bira konstruktor koje klase poziva i vraca instancu//tacno to, trivijal
//izdvojena selekcija u factory klasu
//IVSR:Factory Pattern
public interface IPeople
{
    string GetName();
}
public class Villagers : IPeople
{
     public string GetName() { return "Village Guy";}
}
public class CityPeople : IPeople
{
    public string GetName()  { return "City Guy"; }
}
public enum PeopleType{RURAL, URBAN}

public class Factory
{
    public IPeople GetPeople(PeopleType type)
    {
        IPeople people = null;
        switch (type)
        {
            case PeopleType.RURAL :
                people = new Villagers();
                break;
            case PeopleType.URBAN:
                people = new CityPeople();
                break;
            default:
                break;
        }
        return people;
    }
}
--------------------------
staticki tipovi su definisani compiletime
dinamicki runtime, implicitno
svrha: simulacija dinamickih propertija van same instance
odrzava se paralelno u drugoj strukturi, registruje kroz factory i vraca singleton onstancu
attached property je spec slucaj dependancy propertyja za xaml

---------------------------
event je fja koja je registrovana u nekom globalnom objektu, i moze da se okine bez direktnog poziva vec slanjem poruke tom objektu			
komuniciranje porukama, objekat slusa i ima registar				
----------------------------				
staticka klasa - sve fje i atributi staticki		
----------------------------
port forvarding je otvaranje porta na firewalu rutera, default su zatvoreni
----------------------------			
imei - id telefona
----------------------------
system.object nasledjujes direktno samo ake ne nasledjujes nista drugo, ako nasledjujes drugu klasu object nasledjujes kroz nju, implicitno
------------------------
Select je Map()
internal is for assembly scope (i.e. only accessible from code in the same .exe or .dll)
------------------------
static constructor - uzi tip konstruktora koji init samo static polja, readonly samo kroz njega
u obican moze i to
public list1 = new list(); izvrsava se u static constr fji, a ne van fje
------------------------
//sender prenosi da bi znao koja instanca je okinula event, moze i drugi tip ako je identican args
//na obj skroz druge klase dodelis += new handlerConstruct(handlerFja)
void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
-------
postavljanje event handlera je dodeljivanje fje u obj	
-----------------------------------
//async await
async i jeste tip fje znaci da vraca promise, asinhrona fja vraca promise ili void
await fja(); znaci da ispod te linije kompajler kopira kod u kontinuaciju (success callback fje()) koji se poziva kad se zavrsi fja(), prakticno cim se pojavljuje await fja vraca promise nebitno sa malo koda ispod ili iznad te linije
return await samo znaci da nema koda ispod
vise awaita samo ugnjezdeni callbackovi
await je resolve promisea var promenlj1 = promise.success( function{promenlj1 = nesto...})
poenta ista kao callback samo druga sintaksa
-------------------------------------
 wc = new WebClient(); // novi klijent na svaki event
memorystream ima read() i write() bytes fje
----------------------
background worker je wrapper - kompozicija oko thread poola koji je queue, omotac sa start, done, progres eventima
lock blok {} - sinchronized u javi {}, otkljucava i zakljucava deljivi resurs za niti, i mece niti za red
sustinski umotan try catch{} isto i using u c#
---------------------------------
---------------------------------
//CUSTOM EVENT izvuceno

public class NekaDrugaKlasa
{
    private MyClassWithCustEvent _eventClass;
	
	//attach
    public NekaDrugaKlasa()
    {
        _eventClass = new MyClassWithCustEvent();
        _eventClass.OnDesiloNestoEvent += new MyClassWithCustEvent.DesiloNestoHandler(DesiloNesto);//attach handlera//moze i fja direktno//dodaje fju u listu
											//new klasa.fja() malo staticki malo construkt, stat fja koja vraca contruct//imenovani delegat, kao namespace delegat je klasa
    }  
	
	//handler
    private void DesiloNesto(object sender, MyEventArgs e)
    {
        // tvoja implementacija hendlera
    }
	//emit
	public void biloKojaFja()
	{
		var instanca = _eventClass; //ili new neki
		EmitujDesiloNesto(instanca, args);//emituj event
		instanca.OnDesiloNestoEvent(instanca, args);//ne moze emit direktno u drugoj klasi, u drugoj klasi event ima samo =+ =- attach/detach operatore, nema () 
		//samo += operator nad eventom je public, ostali je sve private namerno i mora kroz fje eventove klase
	}
	
	
}
public class MyClassWithCustEvent
{
	//def potpisa delegata
	public delegate void DesiloNestoHandler(object sender, AutoCompleteArgs e);
	//deklerac eventa, delegat i name//DesiloNestoHandler je tip delegata
	public event DesiloNestoHandler OnDesiloNestoEvent;//public event handler, ime eventa//event je objekat sa clanovima ime i delegat, iza haube
	
	//posredno emit
	public void EmitujDesiloNesto(object sender, MyEventArgs e) //u klasi gde je def event, jer van klase ne mozes direktno da emitujes event
	{
		// Make sure someone is listening to event
		if (OnDesiloNestoEvent == null) return;

		OnDesiloNestoEvent(sender, e);
	}
	//hendler moze bilo gde
	private void DesiloNesto(object sender, MyEventArgs e)
    {
        // moze i u event klasi implem, bilo koja fja koja se vidi u attach handlera
    }
}
public class MyEventArgs : EventArgs //klasa za arg, dodatne podatke da saljes
{
    public string MyParam { get; private set; }

    public MyEventArgs(string param)
    {
        MyParam = param;
    }
}
------------------	
delegat je tip fje, potpis fje		
public event Func<object, EventArgs, Task> Shutdown; //	Shutdown je event	//anonimni delegat
Func<object, EventArgs, Task> handler = Shutdown;	//delegat = event //dodela...event = delegat + ime eventa
event je lista delegata, reference//emit event je prolaz kroz listu i invoke
event je lista delegata//TO TO dobar
------------------
static - klasa - compiletime //glavno ogranicenje i razlika, mogucnost evolucije
clan - instanca - runtime	
cim fja poziva 1 asinhronu fju i sama postaje asinhrona i vraca promise	
kontinuacija() u tasku prima task kao arg, drugi promise, ugnjezdavanje, ulancavanje asinhr poziva	
----------------------
xslt bukvalno templejting, samo xml na xml, inace json na xml
-------------------------
Select() prima fju koja vraca
c# nema anonim fje, lambde mesto toga
var output = doSomething(variable, () => {
    // Anonymous function code
});
--------------------------
multi curl - najobicniji multitreding, nema veze sa mrezama
--------------------------
samo iz frameworka reference vec ponudjene u vstudio, ostale, externe preko nuget, newtonsoft
regioni su manja granulacja od fajla 
bookmark liniju pa ides napred nazad kao browser, navigacija kroz kod
--------------------------
u output uvek pise greska, gledaj output
select je arraymap()
u select kad je jedini statement implicitno vraca
public delegate TResult Func<in T, out TResult>(T arg)//drugi argument je povratni tip TResult
--------------------------
mesto promenlj prosledis lambdu i invoke?
expression tree?
//u klasi koja je nasledila kontrolu, clanica kontrole//ima smisla za kontrole samo
Invoke(new Action(
    () => 
    {
        checkedListBox1.Items.RemoveAt(i);
        checkedListBox1.Items.Insert(i, temp + validity);
        checkedListBox1.Update();
    }
)
);
//od actiona direktno
Action<string> myAction = SomeMethod;//ili lambda
myAction("Hello World");
myAction.Invoke("Hello World");
//ili lambdu u konstruktor new Action() ili Func()
Action se prosledjuje u Thread, nije svesta niti, samo definicija tipa				
proces apstrakcija resursa - racunara, nit apstrakcija procesora				
--------------------
ref arg u c# ako hoces kopija koja kreirana lokalno da se vidi spolja
objekti default preneseni po referenci
--------------------
event klase - asinhrona funkcija atribut klase (asinhroni delegat atribut), delegat bi bio sinhrona
generatori element po element, umesto celo polje jedina razlika, da bi podrzao lazy
generator - petlja ima veci scope od funkcije, ne moras da vratis polje
generator - vraca jedan element umesto celog polja u promenljivoj, nema dublji smisao, trivijalno
--------------------
iterator da ve strukture (stabla, itd) obilazis kao listu u petljama .next()
 tail rekurzija wiki - optimizuje kompajler
---------------------
extension metoda samo nacin na koji se poziva obj.fja() ili fja(obj), nema dublji smisao
fja mora biti u klasi tog tipa, ne bilo gde
-----------------
lambde u linq mogu da se debagiraju ali mora da se pozove toList() nad tim enumerableom
Cannot convert type 'MimeKit.MimeEntity' to 'MimeKit.MimeMessage' via a 
reference conversion, boxing conversion, unboxing conversion, wrapping conversion, or null type conversion	MailTest	
is da li je tipa, as pokusaj da kastujes, null na neuspeh
OfType<MessagePart>() radi as
----------------------
public static explicit operator MimeMessage(MailMessage message);//ili implicit u letu, operator za cast
rekurzivna funkcija, ti zoves tu funkciju kao bilo koju drugu, sto je to ta ista definicija nebitno				
-----------

		
		
		
	











				
				
				
