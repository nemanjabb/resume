difference between ViewBag and Model and where can ViewBag be seen?
what controller for partial template less than a page?
how to pass additional model in partial?
@Html.Partial("_modal_some", new Acme.MVC.Models.SomeViewModel())
--------------
// debugging razor templates
< script >
$(function(){
    console.log('@Html.Raw(Json.Encode(Model.Products))');
});
</ script >
--------------
// comment in Razor.cshtml
@*Comment goes here *@
----------------
// Acme.cshtml
@section meta { se poziva u master templateu

// _Layout.cshtml
@RenderSection("meta", required: false)

// postavlja master template
@{
    Layout = null;
}
----------------
Is there a clear difference between @Model and ViewBag in Razor templates? 
Both are used to pass data from controller to template
Model is statically typed and ViewBag is dynamic.
mostly viewbag is used for some short string and model is used as base object of the current view.
Model can also be binded to form inputs
-------------------
// call controller action in view 
// https://stackoverflow.com/questions/19869708/how-do-i-call-a-controller-action-from-within-my-view-using-razor
// kao renderView(model) in laravel

@Html.Partial("../Home/Login", model) // just render another template
@Html.Action("action", "controller", parameters) // call action, action also returns template
@{ Html.RenderAction("Login", "Account", new { someParam = value }); }
@{ Html.RenderPartial("Login", "Account"); }
----------------
visual studio bookmarks and back and forward for vs code




