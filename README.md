# Nemanja Mitic resume

I am graduated programmer (MSc, 5 years studies) with 4.5 years of professional experience and knowledge of fundamentals, frontend (React, Next.js, Angular 2+), backend stacks (NodeJs, ASP.NET, Python, PHP), DevOps, and some mobile and desktop development. I made this resume to present my expertise in mentioned fields. 

If you don't have much time you should check the [Short version](#markdown-header-short-version) bellow.

**Email:** [nemanja.mitic.elfak@hotmail.com](mailto:nemanja.mitic.elfak@hotmail.com)

**Github:** [https://github.com/nemanjam](https://github.com/nemanjam)

**Linkedin:** [https://www.linkedin.com/in/nemanja-mitic](https://www.linkedin.com/in/nemanja-mitic)

**Single page resume:** [Nemanja Mitic - Resume 2025.pdf](https://drive.google.com/file/d/1DCMJ_hfPk2s6DJKuURIIYW0riEPodFXG/view)

---

---

# Table of contents

- [Introduction](#markdown-header-introduction)
- [Short version](#markdown-header-short-version)
- [Professional career](#markdown-header-professional-career)
    - [Fleek Labs Inc.](#markdown-header-fleek-labs-inc)
    - [Molin AI](#markdown-header-molin-ai)
    - [Folks Labs LLC](#markdown-header-folks-labs-llc)
    - [One X Tech](#markdown-header-one-x-tech)
    - [Giveinkind](#markdown-header-giveinkind)
    - [Coder Consulting International](#markdown-header-coder-consulting-international)
    - [Ilumnis](#markdown-header-ilumnis)
- [Self education](#markdown-header-self-education)
    - [Frontend](#markdown-header-frontend)
        - [JavaScript and jQuery](#markdown-header-javascript-and-jquery)
        - [React](#markdown-header-react)
        - [NextJs](#markdown-header-nextjs)
        - [Astro](#markdown-header-astro)
        - [React Native](#markdown-header-react-native)
        - [Angular 2+](#markdown-header-angular-2)
        - [TypeScript](#markdown-header-typescript)
        - [CSS](#markdown-header-css)
        - [GraphQL](#markdown-header-graphql)
    - [Backend](#markdown-header-backend)
        - [NodeJs](#markdown-header-nodejs)
        - [Python](#markdown-header-python)
        - [FastAPI](#markdown-header-fastapi)
        - [C#](#markdown-header-c35)
        - [PHP and Laravel](#markdown-header-php-and-laravel)
    - [Testing](#markdown-header-testing)
        - [Jest React](#markdown-header-jest-react)
        - [Jest NodeJs](#markdown-header-jest-nodejs)
        - [Cypress](#markdown-header-cypress)
    - [DevOps](#markdown-header-devops)
        - [Git](#markdown-header-git)
        - [Linux](#markdown-header-linux)
        - [Virtualization](#markdown-header-virtualization)
        - [Docker](#markdown-header-docker)
        - [CI/CD](#markdown-header-cicd)
        - [AWS](#markdown-header-aws)
        - [Ansible](#markdown-header-ansible)
        - [Terraform](#markdown-header-terraform)
        - [Kubernetes](#markdown-header-kubernetes)
    - [Fundamentals](#markdown-header-fundamentals)
        - [Software architectures](#markdown-header-software-architectures)
        - [Software testing](#markdown-header-software-testing)
        - [Databases](#markdown-header-databases)
        - [Functional programming](#markdown-header-functional-programming)
    - [Other self education](#markdown-header-other-self-education)
        - [Productivity and time management](#markdown-header-productivity-and-time-management)
- [Personal projects](#markdown-header-personal-projects)
- [Languages](#markdown-header-languages)
- [Education](#markdown-header-education)
- [Miscellaneous](#markdown-header-miscellaneous)
    - [My programming philosophy](#markdown-header-my-programming-philosophy)
    - [Hobbies](#markdown-header-hobbies)

---

---

# Introduction

Please read this section first so you could have better understanding of the rest of this document as it is pretty extensive and detailed. If you don't have much time you should check the [Short version](#markdown-header-short-version) bellow.

### About professional career

In this section I listed companies I worked for, projects I was involved in and my responsibilities, technology stack I worked with and tools that I used. I can't provide code samples as projects are not open source, I use my personal projects for that.

### About self education

This is the largest chapter in this resume as it is in my career. I am graduated engineer but university education gave me just foundation without any specialized knowledge. I am aware that technologies and associated expertise become obsolete in 3 years leaving you unqualified but I am fine with that, that is what I chose intentionally because it suits my curiosity, programmers are life long students. When I am not learning on my job from colleagues I study by my self initiative, with my own plan and pace.

People that study a lot know that there is no such thing like absolute knowledge. Natural knowledge lifecycle is that in any moment you are a beginner in some topic, confident in another and some of them are already forgotten. In the last 10 years I am heavily involved with JavaScript. In the last 5 years my primary focus is on React and it's ecosystem and then Node.js. I have 3.5 years of professional experience with C\# which currently isn't my top priority but I try to stay in shape with it. I spent 6 months on Angular 6/7 but I left it for React. I studied Laravel 5 for a year and since 2018 it is not my priority. Recently I studied Python theory concepts intensively for 4 months where I tried as much as possible to reuse my knowledge from JavaScript, C\# and PHP to progress faster and I expect to have more practical experience with it in the future.

You may notice DevOps is a large section in this chapter. I am not DevOps engineer and don't intend to switch career from development to DevOps but while working professionally I noticed how testing and deployment process can be risky and stressful. So I spent some time to explore technologies that would allow to script deployment process in advance and reduce pressure and risk. While on college I never imagined programming could be stressful work, I always thought you can safely work on a copy of the code and focus on algorithms.

### About linked notes

For everything I spend time learning I write notes as mind map and reminder to prevent wasting time and energy learning same thing twice. Notes are meant for my personal use therefore are written in mixed English-Serbian, please use translate tool for easier interpretation. There are 1.7 MB of text notes (around 420 pages) I made in last 10 years and relevant notes are linked at the end of each paragraph in this resume.

### About personal projects

My ultimate goal is to use my knowledge and make meaningful products that will provide real value for real people. Obviously that is not an easy task for a single developer because of level of effort and multidisciplinary knowledge needed, at the end that is why companies are invented. Idea behind the most of the projects from this list was to learn by doing and get practical experience, also to have publicly available code samples for my future potential employers.

_"Next.js Prisma Boilerplate"_ is my most ambitious project, I spent 8 months on it, it is a complete foundation for a modern Next.js app using the most up to date technologies and best practices from React/Node.js ecosystem. If you don't have much time and you just want to see an example of my recent code I suggest you check it out in projects section bellow **[Next.js Prisma Boilerplate](#markdown-header-nextjs-prisma-boilerplate)**.

 _"Reddit Unread Comments"_ is a Firefox/Chrome browser extension for easier tracking of new comments in Reddit threads, I made _"MERN Boilerplate"_ to brush up my React, Redux, Express skills, _"Audio Twitter"_ was about React, Apollo and Material-UI, _"RN Chat"_ about React Native and Apollo, _"Redux Ecommerce"_ about React and Redux, etc. From project to project I switch between PostgreSQL and MongoDB databases so I can be in touch with both relational and document model. The only exception is _"Tweet of the day"_ project that was attempt to make real life project by providing logic to filter most liked content on Twitter which was not possible with native API at that time. In the meantime Twitter started curating timeline on its own so my project lost purpose, but I am not completely unsatisfied because this project helped me to get some practical experience with Laravel.

### Ambitions

Primarily I am looking for a job related to React, Next.js, NodeJs and .NET. I seek opportunity to engage with Python. Although I spent last 10 years studying web programming I could be interested in any programming topic that could be professional challenge and fun to learn.

---

---

# Short version

#### Hard skills

My top skills are React/Next.js and I am also decent with Express.js and Astro. Since summer 2022. I started learning Python and I can assist with it. I have 3.5 years of professional experience with C#, but my C# skills are outdated. I am well familiar with both relational and document-based databases. I have decent DevOps knowledge for a developer.

I have been working with JavaScript since 2010 (professionally since 2014), and I know it well enough to understand that JavaScript can be a rabbit hole. I will ensure your project stays in a safe zone and avoid unnecessary complexity.

I hold a Master's degree in Computer Science and have a solid base in computer science fundamentals. Languages I came in touch during education include C, C++, Java, PHP, Lisp, Assembly, Ada.

Frontend technologies where I can progress further are WebRTC, WebGL, and WebAssembly. I am also interested in AI/ML, which is why I started learning Python.

#### Soft skills

I have worked with companies from around the globe, including the USA, Thailand, Singapore, UK and Germany. I am experienced in handling both synchronous and asynchronous work across different time zones. I am also a respectful and open-minded person who functions well in multicultural environments and is looking forward to meeting different cultures.

I speak English at a professional level and have limited proficiency in German.

#### Code examples

If you want to see examples of my most recent code, you should check out these two repositories:

https://github.com/nemanjam/nemanjam.github.io (spring 2024)

https://github.com/nemanjam/hn-new-jobs (autumn 2024)

To see my most comprehensive project, check out this repository:

https://github.com/nemanjam/nextjs-prisma-boilerplate (spring 2022)

To see how I document projects check out `/docs` folder and YouTube demo video in this repository:

https://github.com/nemanjam/reddit-unread-comments (winter 2023)

#### Self education

I am a constant learner and an analytical and organized person. I keep notes about everything I learn, and over the last 10 years, I have collected around 420 pages of notes. You can see them here: 

https://bitbucket.org/nemanjabb/resume/src/master/notes

#### Hobbies

I am a passionate fisherman. I play guitar at an amateur level and enjoy composing in FL Studio. I also like cycling, building DIY audio and radio electronics, experimenting with homelab, and self-hosting.

---

---

# Professional career

### Frontend developer contractor

#### Fleek Labs Inc.

(December 2024 - February 2025) · Remote · New York, USA  

Worked on [fleek.xyz](https://fleek.xyz) Platform as a Service website for a startup from New York.

**Engaged on following projects:**

---

#### _Frontend development:_

**Fleek dashboard and website projects**


Extracted auth and Eliza Agent features into a separate NPM packages. Migrated deployment templates configuration from Next.js dashboard to Astro static website. 

Tech stack: Next.js, Astro, TailwindCSS, GraphQL, Turborepo, Github Actions

---

#### Tools used

- Editors: VS Code
- Versioning: Git and Github
- Task management: Linear
- Design: Figma
- Communication: Discord

---
### Freelance full stack developer

#### Molin AI

(July 2024 - August 2024) · Remote · London, UK  

Worked on [molin.ai](https://molin.ai) e-commerce chat widget for an AI startup from London.

**Engaged on following projects:**

---

#### _Frontend and backend development:_

**Molin AI project**

Implemented a redesign of the chat widget. Implemented PostgreSQL full text search for products. Migrated all HTTP fetch calls across the app to tRPC.

Tech stack: Lit Element, TailwindCSS, OpenAI GPT-4, Cloudflare Workers, PostgreSQL, JSDoc

---

#### Tools used

- Editors: VS Code
- Versioning: Git and Github
- Database: DBeaver
- Task management: Github Projects
- Design: Figma
- Communication: Slack

---
### Freelance full stack developer

#### Folks Labs LLC

(April 2023 - May 2023) · Remote · Los Angeles, USA  

Collaborated on [hifolks.com](https://www.hifolks.com) website development for the early stage startup with team members from Los Angeles.

**Engaged on following projects:**

---

#### _Frontend and backend development:_

**Hi Folks project**

- Developed custom OAuth providers and custom Prisma adapter for Next Auth. Implemented Next.js project base configuration for code linting, formatting, Docker, data fetching with React Query, forms handling with React Hook Form and Zod, developed Next.js API endpoints, worked on Prisma database schema, integrated Twilio API. 

- Technologies used: Next Auth, Next.js, Prisma, React Query, TailwindCSS, React Hook Form

---

#### Tools used

- Editors: VS Code
- Versioning: Git and Github
- Deployment: Vercel
- Task management: Linear
- Design: Figma
- Communication: Slack

---

### Freelance full stack developer

#### One X Tech

(January 2023 - May 2023) · Remote · Singapore  
(March 2024 - April 2024) · Remote · Singapore  

Worked with developers and managers from Singapore.

**Engaged on following projects:**

---

#### _Frontend and backend development:_

**Ste project**

- Worked mostly on the backend. Developed API endpoints, wrote DrizzleORM and MySQL queries, Zod schemas and implemented file uploads. Tested and documented everything with Curl.

- Technologies used: Express.js, DrizzleORM, MySQL, Zod, Multer, Turborepo

**FinX project**

- Developed features for the [finx.com](https://finx.com) website. Implemented Next.js components, static and ISR pages, and API endpoints. Used ShadcnUI and TailwindCSS for styling. Integrated Contentful GraphQL backend with Next.js components and configured webhooks for ISR rendering and preview mode.

- Technologies used: Next.js, ShadcnUI, TailwindCSS, Contentful, Vercel

**TymeX project**

- Developed features for the [tyme.com](https://tyme.com) website. Implemented Next.js components and API endpoints, MUI styling and layouts, set up configuration for React Query data fetching, integrated React Hook Form and MUI into custom form components, integrated Contentful backend with Next.js components.

- Technologies used: Next.js, MUI v5, React Query, React Hook Form, Contentful

---

#### Tools used

- Editors: VS Code
- Versioning: Git and Github
- Deployment: Vercel
- Task management: Productive, Linear
- Communication: Slack, Google Meet

---

### Full stack developer

#### Giveinkind

(July 2020 - June 2021) · Remote · Chiang Mai, Thailand and Seattle, USA

Part of the international distributed remote team (Thailand, Poland and USA).

**Engaged on following projects:**

---

#### _Frontend development:_

**NextJs, React, Storybook projects**

- Development and maintenance of startup's website [giveinkind.com](https://giveinkind.com). Responsible for development of new features, implement frontend logic, translate designs into code, maintain and migrate existing code, fix production bugs, participate in planning and brainstorming new features, review code, participate in release process, update and maintain documentation. Follow component driven development methodology and showcase all pages and components in Storybook. Took lead and responsibility for development of two significant features (Tango cards purchase and Page Settings) and implemented them from beginning to end. 

- Technologies used: React, TypeScript, Next.js, SWR, Storybook, TailwindCSS, SCSS, SASS, BEM, WSL2, Docker, Yarn workspaces, i18n.

#### _Backend development:_

**MVC project**

- Development and maintenance of the backend part of the same website. Migrate Razor templates to React components, enhance existing REST endpoints to meet new requirements. Participate in planning migration from ASP.NET MVC to .NET Core 5.0 of the entire project.

- Technologies used: ASP.NET MVC, Razor, MSSQL, .NET Core.

---

#### Tools used

- Editors: VS Code, Visual Studio 2019
- Versioning: Gitlab and Git
- Task management: Jira, Notion
- Design: Figma, Invision
- Deployment: Azure
- Database: SQL Server 2019, SQL Server Management Studio
- Communication: Slack, Zoom
- Documentation: Notion
- WSL2, Docker
- Firefox and Chrome developer console
- React developer tools, Browserstack
- Datadog, Fullstory

---

### Frontend developer

#### Coder Consulting International

(October 2019 - January 2020) · Remote · Berlin, Germany

Part of the international distributed remote team (Germany and Serbia).

**Engaged on following projects:**

---

#### _Frontend development:_

**React Apollo Antd project**

- Development of the frontend of the shipment tracking platform. Responsible for development of custom components using functional React components, Hooks and writing unit and integration tests using Jest, Enzyme and MockedProvider. Implementing data fetching using Apollo Client with Hooks API. Integrating Antd components, custom styling using Styled components and following Atomic design methodology. Using Git as version control. Using ESLint and Prettier for ensuring code quality and formatting.

- Technologies used: React 16.9, Apollo, Ant Design, Jest, Enzyme, Styled Components, React Intl, Git.

---

#### Tools used

- Editor: Webstorm 2019
- Communication: Discord, Zoom and AnyDesk
- Git console
- Firefox and Chrome developer console
- React and Apollo developer tools

---

### Full stack developer

#### Ilumnis

(September 2014 - March 2017) · Remote and Onsite · Nis, Serbia  

Part of the team consisted of three members in office in Serbia, and around 10 colleagues in USA.

**Engaged on following projects:**

---

#### _Frontend development:_

**Amtrav**

- Development and maintenance of Amtrav's company website [amtrav.com](http://amtrav.com). Responsible for implementing frontend of the website including page logic, form validation and AJAX communication, development of custom controls, templating.
- Technologies used: jQuery, jQueryUI, jQuery Validation, Handlebars.

**AM5**

- New version of Amtrav website. Responsible for porting old frontend codebase to new internal MVC framework and wiring UI to new backend services. Porting form validation to the new library.
- Technologies used: jQuery, jQueryUI, Validate.js, Handlebars, Gulp.

**CheapAir**

- Development of another Amtrav's website [cheapair.com](https://www.cheapair.com/). Responsible for integrating jQueryUI controls and templating.
- Technologies used: jQuery, jQueryUI, Handlebars, Gulp.

**VCT Visa**

- Development of another Amtrav's website. Responsible for templating and validation.
- Technologies used: jQuery, jQuery Validation, Handlebars.

**Atom web view**

- Porting Amtrav's internal application's delphi desktop interface to web implementation using iframes and jQuery. Implementation of view, custom controls and wiring AJAX http communication to backend.
- Technologies used:jQuery, jQueryUI, Handlebars.

---

#### _Backend development:_

**VCT Admin panel**

- Development from scratch of both backend and frontend of custom admin panel with multiple roles. Responsible for database design, backend logic, exception handling, event and exception logging on backend, and templating and exception handling on frontend.
- Technologies used: MSSQL, WCF, WebApi, Linq2SQL, jQuery, Handlebars.

**Regex scrapers**

- Development of web scrapers for collecting and extracting air booking data using internal framework and native .NET Regex and http client implementations.
- Technologies used: .NET 4.5, Regex, Linq.

---

#### _Desktop development_

**Atom WPF view**

- Porting delphi's desktop interface to WPF. Started with WinForms, and switched to building new WPF view from scratch. Wiring with backend services using .NET 4.5.1 async http client. Development and integration of custom controls.
- Technologies used: .NET 4.5.1, WPF.

**Ping Visa**

- Development of scheduled console application for checking service availability and error reporting via email.
- Technologies used: .NET 4.5.1.

**Deal Manager**

- Implementing features on WinForms application for generating html formatted emails from xslt templates.
- Technologies used: .NET 4.0, XML.

---

#### _Test and research projects_

**Mail Test**

- Development of WinForms test application for stripping various types of MIME attachments from email messages.
- Technologies used: .NET 4.5.1.

**Verify Email**

- Development of WinForms test application for investigation possibilities for determining credibility of email addresses using publicly available methods and API from Gmail, Facebook and Twitter.
- Technologies used: .NET 4.5.

---

#### _Tools used_

- Visual Studio 2012, 2013, 2015
- SQL Server 2012, SQL Server Management Studio 2014
- IIS manager
- Telerik Fiddler
- Firefox and Chrome developer console
- Tortoise SVN

---

---

# Self education

### Languages and frameworks

---

### Frontend

#### JavaScript and jQuery

After I started professionally working with JavaScript I realized that as language it has a lot of specificity that needed to be understood in order to be able to interpret and write code, and especially understand more advanced frameworks so I took time to learn these concepts. 

For practical guide I used comprehensive blog _"javascriptissexy.com"_, for global overview Crockford's video presentations, _"Learning JavaScript Design Patterns"_ - book by Addy Osmani , jQuery docs etc.

I got informed about language history, why is it the way it is, and how JS reused ground concepts from Scheme, Self and Java. I learned basic concepts such as scopes and variable visibility, anonymous functions, "this" reference, it's value in different contexts and ways to manipulate it trough built-in functions such as call(), apply(), bind() or $.proxy() jQuery's equivalent, ways to mimic OOP concepts in JS such as encapsulation, prototype inheritance, module pattern, other language concepts like hoisting, borrowing functions, synchronous and asynchronous calls, events, promises, some ES6 and ES7 concepts like generators, lambdas, async/await pattern, iterable/observable interfaces (most of them familiar from C#), and also jQuery concepts like chaining, plugins, widgets factory, selectors, delegated events.

I have put main points in my working notes as reminder and mindmap **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/javascript/javascript.js)** and **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/javascript/javascript2021.js)**. Notes about jQuery are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/jquery)**.

#### React

As React become the most popular JavaScript framework I took time to learn it. I was intrigued that it has small API but you could make very complex applications with it. It also has plenty of pre-made components, CSS frameworks integrations, vibrant community and great learning resources.

As learning resources I used official documentation and following video courses: _"Mastering React"_ by Mosh Hamedani, _"Modern React with Redux [2019 Update]"_ and _"Advanced React and Redux 2018 Edition"_ extensive course from Udemy by Stephen Grider, _"Building Applications with React and Redux in ES6"_ from Pluralsight by Cory House, _"Redux Saga"_ from Pluralsight by Daniel Stern, _Using React Hooks_ from Pluralsight by Peter Kellner, _"Build a Slack Chat App with React, Redux, and Firebase"_ Udemy course by Reed Barger, _"Learn Redux Saga"_ from What the JavaScript YouTube channel, _"ReactJS Tutorial"_ by Codevolution Youtube channel, _"MERN Stack Front To Back: Full Stack React, Redux & Node.js"_, _"Learn The MERN Stack"_ and _"React JS Crash Course"_ by Traversy Media, some of the Techsith YouTube tutorials, and following examples: _"React Redux Realworld example app"_ and _"React Apollo Realworld example app"_ from Thinkster, _"Youtube React"_ YouTube clone in React and Redux Saga from productioncoder, _"React Shopping Cart"_ e-commerce app by jeffersonRibeiro, _"React Ecommerce App-with-Redux"_ e-commerce app by TheCodersDream, _"Dev connector v2"_ by Brad Traversy.

I learned about _class based components_, _stateless functional components_, _state_ - component level state, _this.setState() method_ - the state is immutable and compared by reference,_props_ - component arguments objects or functions, _this.props.children_ - projecting content into a component, _lifecycle methods_ - mounting, updating and unmounting phases, _handling events and passing arguments_, _debugging React_ - using tools like React Developer Tools and Redux DevTools, _controlled elements_ - working with form elements, _JSX_, _virtual DOM_, _context_ - pass data to any descendants, _Higher-Order Components_ - reuse component logic, _portals_ - render component outside of parent hierarchy, _render props_ - share state and logic between components, _refs_ - access DOM elements and components, _propTypes and defaultProps_ - props typechecking, _React.StrictMode_ - detect legacy problems, _Fragments_, _useState and useEffect hooks_ - state and lifecycle methods for functional components, _useContext_ and _useReducer_ for handling global state, _useCallback_ and _useMemo_ for memoizing functions and values, _React router_ for page navigation _Protected routes_, _Axios and fetch API_ for HTTP communication, _Redux_ - unidirectional data flow and state management with store, actions, action creators and reducers as pure functions, _Provider component, connect function and mapStateToProps function_ - connecting Redux with React, _Redux Thunk, promise middleware and Redux Saga_ for async actions, _Redux form, Formik, React Final Form_ - for forms, _middleware_ - custom middleware for sync and async actions, _authentication and authorization_ - integrating with JWT, OAuth and local Passport strategies, _Material UI_ - CSS framework, _Firebase_ - realtime database, auth and storage, _Apollo Boost, Apollo Client and GraphQL Request_ - ApolloClient, ApolloProvider/ApolloConsumer, query and mutation components with render props, refetching queries, optimistic updates and caching, nested queries and mutations, fragments, realtime requests with GraphQL subscriptions, _ErrorBoundary_ - handling errors, _Suspense_ - handling loading state, _useImperativeHandle_ - expose property of a ref, _React Query_ - library for data fetching and managing server state via cache, _Queries_ - read operations, _Mutations_ - write operations, _parallel queries_, _dependent queries_, _retries_, _paginated and infinite queries_ - offset and cursor pagination, _prefetching_, _cache invalidation_, _Hydrate provider_ - for SSR, React `18` features like _startTransition and useTransition_ - to mark state as non urgent, _useDeferredValue_ - for non urgent re-renders, _React Hook Form_ - more performant form library with uncontrolled fields.

Learning notes about React can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/react.js)** and **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/react2021.js)**, about React Query **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/react%20query.js)**.

#### NextJs

NextJs came as logical step forward in my React roadmap after I had good grasp on React fundamentals. In 2020. it was already obvious that NextJs became industry standard for production grade websites built with React. Back in the my university days in 2012. I had Information retrieval course so I was well aware of problems with indexing dynamic pages and onsite and offsite SEO fundamentals early in my career. The fact that Vercel team made NextJs fullstack framework with easy deployment and affordable hosting made it even more appealing to me for possible demo or proof of concept personal projects.

As learning resources I used official tutorial and docs, SWR docs, _"Next.js Tutorial for Beginners"_ Youtube playlist by Codevolution, "_The Next.js Handbook"_ tutorial by Flavio Copes and following examples: _"Next.js RealWorld example app"_ from reck1ess, examples from _"Official NextJs repository"_ by Vercel, _"Prisma Examples"_ from Prisma. I also worked professionally for a year with NextJs and SWR in Giveinkind.

I learned about _pages_ and how they are different from regular components, _pre-rendering_ - two basic forms Static Generation and Server-side Rendering and when to use which one, _pre-rendering advantages_ - SEO and performance, _Static Generation (SSG)_ - pre-render pages at build time, _Static Generation types_ - without and with data, Incremental Static Regeneration, _Server-side Rendering (SSR)_ - fetch data at request time, _folder structure_, _getStaticProps()_ - runs only on server, only in pages, context argument, return value, _Incremental Static Regeneration (ISR)_ - static-generation per-page basis, without rebuilding the entire site, _getServerSideProps()_ - runs only on server at request time, context argument with request and response objects, _file-system based routing_, _nested routes_, _dynamic routes_, _catch all routes_, _optional catch all routes_, _navigation_ - Link component and programmatic, _Static Generation with Dynamic Parameters_, _getStaticPaths()_ - must return array of pages paths that need to be statically pre-rendered, _fallback key_ - false, true or blocking, _Client side data fetching (CSR)_ - using standard useEffect hook, _dev and prod builds_ - in dev mode Static Generation runs on every request, _API Routes_ - serverless backend endpoints using handlers similar to Express, same routing logic like for pages, _global styles_ - imported in \_app.js, _CSS modules_ - component level styles, _SASS support_, _CSS in JS support_ - Styled Components, Emotion, _App Layout_ - defined in \_app.js, _Head component_, _Image component_ - optimize size, format and cache, _absolute imports_ - define baseUrl and path aliases, _Static HTML Export_ - next export command, _Typescript support_, _preview mode_ - bypass static generation, useful to preview draft posts in CMS, _Redirects_ - can be defined in next.config.js, _environment variables_ - in both server and JSX context, NEXT_PUBLIC\_ prefix, _NextAuth library_, _auth providers_, _database adapters_, _useSession() and getSession()_ - access user's session in client and server context, _protecting routes_ - for both pages and APIs, _callbacks_ - pass additional data into session object, _deploying to Vercel_, _getInitialProps()_ - in NextJs 9.3 or newer deprecated in favor of getStaticProps and getServerSideProps, _SWR_ - http client with caching and polling support, _custom http server_ - for runtime uploads and https support, _next-connect_ - api router.

Notes about NextJs are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/next.js)**

#### Astro

I decided to make coding blog and I did a thorough research on best tools for building static websites. I understood that Jekyll, Hugo, Gatsby are in decline, Next.js is more SSR oriented and that currently Astro is the best meta-framework for personal websites. It is high performance, minimum JavaScript, static first framework, well documented, proactive core team, with big community and plenty of examples so its popularity for content driven websites is not a surprise.

For learning resources I used well balanced and easy to read official Astro docs, _"Coding in Public"_ YouTube channel, official examples from _"astro.build/themes"_, _"AstroPaper"_ by Sat Naing, _"AstroWind"_ by onWidget, _"Astroship"_ by Surjithctly, _"Astro Cactus"_ by Chris Williams, _"website-thomas-astro"_ by Thomas Ledoux, and many others.

Astro has a decent of overlap with Next.js and React so I was already familiar with some concepts but I also learned many new things. 

I was already familiar with _Astro template syntax_ - similar to JSX, _rendering modes_ - static generation, server and side rendering, _file-based routing_ - pages, layouts, dynamic routes, API endpoints, _markdown_, _MDX_, _middleware_, _Image component_ - for optimized images, _environment variables_.

New things that I learned include _Astro islands - client:* directive_ - partial hydration, interactive, isolated components, _zero JavaScript by default_ - minimize JavaScript to maximize performance, _astro.config.mjs and Vite_ - different tool for bundling and configuration, _frontmatter_ - server code for a component, _integrations_ - interface for modular architecture, _markdown, MDX, remote content_, _Content Collections_ - structured, typed method for managing and querying content, _static file endpoints_ - build-time GET endpoints, _View Transitions_ - animated page transitions, _framework components_ - integrating different fronted frameworks, _processed and unprocessed script tags_, _global, scoped and external CSS_, _slots_ - children prop to project content, _Container API_ - render Astro component to a string, _RSS feed, sitemap_ - official integrations.

I invested around 4 months into building my blog with Astro, read more about it in the Projects **[section](#markdown-header-developer-blog-nemanjamiticcom)**.

Learning notes about Astro are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/astro)**.

#### React Native

Since I already learned React I decided that the best path to flow into mobile development world for me is React Native.

As learning resources I used Udemy course _"React Native - The Practical Guide"_ by Maximilian Schwarzmuller, official React Native docs, React Navigation docs, NativeBase docs, Apollo client docs, Formik docs, _Handlebar Labs_ Youtube channel, _Ben Awad_ Youtube channel, and following examples: _"Chatty"_ by Simon Tucker, _"Chatify"_ by LFSCamargo, _"Persistent React Native Chat App"_ by Gabriel Rumbaut and _"NativeBase-KitchenSink"_ by GeekyAnts.

I learned about _JSX elements, events and layouts_, _running app in emulator and on physical device_, using _React Native Debugger_, _linking third party libraries_, adding navigation, tabs and drawer, _styling_, _forms_, _accessing camera and image gallery_, _http_ and _auth_.

Notes about React Native are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/react%20native.js)**

#### Angular 2+

I dedicated year 2018. to learn Angular. At that time it was obvious that Angular 1.x is going to history in favour of new completely different framework. I liked the fact that it is comprehensive framework to make an app from ground up without need for external libraries.

Beside official docs as material I used following video courses: _"The complete Angular master class"_ and _"Testing Angular apps"_ from Udemy by Mosh Hamedani, _"Angular 6 - The complete guide"_ and _"Angular full app with Angular Material, Angularfire and NgRx"_ from Udemy by Maximilian Schwarzmuller, _"Angular 2 reactive forms"_, _"Angular routing"_, _"Angular component communication"_, _"Angular forms"_, _"Angular NgRx - getting started"_ from Pluralsight by Deborah Kurata, Duncan Hunter and Mark Zamoyta, Techsith YouTube tutorials, StalkBlitz _"Tour of heroes"_ examples, _"Real world"_ Angular and Angular NgRx examples from Thinkster.

I learned about _components_ - directives with templates, _templates_ - interpolation, expressions, statements, reference variables, _bindings_ - one and two way property bindings, event, attribute, class and style bindings, _@Input_ and _@Output_ properties, _component lifecycle hooks_, _attribute and structural directives_, _pipes_ - for transforming data, _RxJs_ - observables, success, error and complete paths, operators, Subjects, Behavioral Subjects, async pipe, _NgModules_ - feature, shared, core modules, lazy loading module, _services_ - hierarchical injector, class, value and factory providers, _reactive forms_ - form model, form state, form directives, form builder service, form arrays, built-in validators, custom validators, valueChanges observable property, _template driven forms_ - ngModel and ngForm directives, data binding, _routing_ - base path, forRoot and forChild methods, naming routes by feature, activating routes from template and from code, required, optional and query parameters, reading parameters trough snapshot or params observable, route resolvers, child routes, componentless routes, secondary routes and named outlets, route guards: canActivate, canDeactivate, canLoad, lazy loading feature modules, preloading modules and preloading strategies, _component communication_ - _template with component_ - two way binding, getter and setter, valueChanges observable, getting references with @ViewChild and @ViewChildren decorators, _parent to child_ - via child's @Input properties, getters and setters, ngOnChanges lifecycle hook, referencing child component, _child to parent_ - @Output properties, _communicating trough service_ - property bag, retain view settings, pass data between components, state management service, manage and share state, notifications for bound data, state management with notifications, using BehaviorSubject, _passing data using router_ - required, optional and query parameters, _managing state with NgRx_ - _Redux pattern_ - Store - read only single source of truth, change state by dispatching Actions, process Actions and change state with pure functions called Reducers, strongly type the state using interfaces by feature and build Selectors for querying the state, strongly type the Actions using action creator classes with type and payload properties, keep components pure by isolating the side-effects operations using Effects, organize components into presentational and container components, folder structure by feature, _HttpClient_ - getting json data or read full HttpResponse<T> response, type checking, encapsulate http in data access service, handling errors, setting headers and parameters, interceptors, _Animations_ - custom, any \* and void css states, transitions, triggers, timings, run animations in sequence or in parallel, parent child animations, animation callbacks, multiple steps animations with keyframes, reusable parametrized animations, route animations, _Testing_ - arrange, act, assert, unit, integration and end-to-end tests, testing services - sync and async tests, providing dependencies, using TestBed utility, using spies and stubs, testing http services, testing components as classes, testing DOM with fixtures, testing bindings, providing dependencies, routed components, testing attribute directives and pipes.

Learning notes about Angular can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/angular/angular2.js)**, **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/angular/angular1%20and%202%20and%20react.js)** and **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/angular/angular1%20docs.js)**.

#### TypeScript

I first met TypeScript while learning Angular. As the complexity of JavaScript applications began to grow introducing a type system like the one in C\# or Java was logical step. In the meantime it became standard for React too. I was already familiar with statically typed languages so transition for me wasn't too hard but regardless it's syntax sometimes can look complex so I spent some time studying it. It can help in larger projects to keep things explicit and documented, easier navigating through code and catch some of the errors at compile time.

For learning material I used _"Understanding TypeScript"_ - Udemy course by Maximilian Schwarzmuller, _"Using TypeScript with React"_ - Udemy course by Dmytro Danylov, _"React & TypeScript For Everyone"_ from Level Up Tutorials, _"React TypeScript Tutorial for Beginners"_ - Youtube playlist by Codevolution  and following examples: _"react-hooks-typescript-realworld"_ by Haythem Chagwey, examples from Next.js official repository. I also worked with it one year professionally.

I learned about _type inference_ - you don't have to type everything explicitly, compiler is smart enough, _type alias_, _interfaces_ - a bit different than in other OOP languages, _union and intersection operators_, _typing classes, functions, tuples, enums_, _optional and default arguments_, _getters and setters_, _function overloads_, _modifiers_, _type casting and assertion_, _optional chaining_, _nullish coalescing_, _generics_, _decorators_, _modules_, _namespaces_, _typeof_ - for JavaScript types, _instanceof_ - for TypeScript classes, _any, unknown and never types_, _Partial, Required, Readonly, Exclude, Omit_ - helpers, _literal types_. Also typing React specific patterns: _functional and class components_, _props_, _render props_, _hoc_, _built in hooks_, _refs_, _DOM elements_, _events_.

Learning notes about TypeScript can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/javascript/typescript.js)** and **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/javascript/typescript2021.js)**. Notes about React specific TypeScript are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/react%20typescript.js)**.

#### CSS

React is a visual library and it requires advanced CSS knowledge. In the past few years there has been a shift from frameworks that provide a set of pre-made components e.g. Bootstrap to the ones that allow higher level of customization and more unique designs like Tailwind. This requires good understanding of CSS fundamentals and how it works at lower level. Also flexbox and grid became standard for making layouts. I was aware of these trends so I took time to reiterate on fundamentals and adopt new technologies.

For learning material I used _"Advanced CSS and Sass: Flexbox, Grid, Animations and More"_ - comprehensive Udemy course by Jonas Schmedtmann, _"Modern Web Layout with Flexbox and CSS Grid"_ - Pluralsight course by Brian Treese, tutorials from Traversy Media Yuotube channel, official documentation from TailwindCSS, Bootstrap, Material-UI frameworks.

I learned about _cascading_ - algorithm that that resolves conflicts by deciding which rule wins, _specificity_ - weight of the selector that determines if style will be applied, _inheritance_ - mostly for font properties, _flexbox_ - in detail, main tool for layouts, _grid_ - for advanced layouts, _box model_, _block and inline elements_, _element positioning_, _transform_, _transition_, _animation and keyframes_, _gradients_, _pseudo-classes and pseudo-elements_, _media queries_ - responsive design, _stacking context_ - how elements are displayed along the z-axis, _BEM_ - methodology for reusable components, _Sass_ - preprocessor that enables variables, nesting, operators, partials and imports, mixins, functions, extending, control directives. Also Tailwind features like _functions, directives_, _@tailwind_ - directive for Tailwind's base styles, _@layer_ - directive for defining custom base, component, utilities styles, _@apply_ - apply classes inside CSS files, _variants_, _tailwind.config.js_ - main configuration file, _content_ - including templates, _themes_ - visual identity, _plugins_ - reusable styles as JavaScript, _presets_ - custom base configuration, _screens_ - defining breakpoints.

Learning notes about CSS can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/css/)**.

#### GraphQL

As GraphQL emerged as serious alternative to REST solving it's problems like over and under fetching, deeply nested routes, API versioning I decided to get involved with it.

As learning resources I used following courses: _"Build a Realtime App with React Hooks and GraphQL"_ and _"Full-Stack React with GraphQL and Apollo Boost"_ Udemy courses by Reed Barger, _"GraphQL with React: The Complete Developers Guide"_ Udemy course by Stephen Grider, _"Hands-On Full-Stack Web Development with GraphQL and React"_ by Bruno Dobrin Ganev, official Apollo docs.

I learned about _queries_, _mutations_, _schema types_, _connectors_ and _resolvers_, _root query entry points_, _subscriptions_, _optimistic updates_, _pagination_, _authorization_, _error handling_, _state management_, _fragments_, _hooks_, different _data sources_, _playground_.

---

### Backend

#### NodeJs

Since Node and Mongo are usual backend stack with React and Angular and I was already familiar with other backend stacks I took time to learn the basics. I noticed its popularity and performance when it comes to IO operations.

As learning resource I used _"Node.js: The Complete Guide to Build RESTful APIs"_ by Mosh Hamedani Udemy course, _"OAuth Login (Passport.js) Tutorial"_ and _"MongoDB Tutorial"_ YouTube playlists by Net Ninja, Sequelize docs, _"What the heck is the event loop anyway?"_ by Philip Roberts, _"Node.js, Express, MongoDB & More: The Complete Bootcamp"_ by Jonas Schmedtmann, and _"Node Express Realworld example app"_ from Thinkster.

I learned about _Node module system_, _package managers_, _node version manager_, _event loop_, _ES6, ES7 features_ like _default parameters, template literals, multiline strings, let and const block scopes, classes, arrow functions, improved object literals, object destructuring, callbacks, promises, async await, spread and rest operators, observables_, _RESTful services with Express_, _RESTful routing_, _middleware, controllers, services_ - separation of concerns and testable architecture, _Mongoose ODM_, _Prisma ORM_, _schemas_, _models_, _querying_, _validation_, _relationships_, _JSON Web Tokens_, _Passport JWT, local and OAuth_ authorization strategies, _handling errors_, _unit and integration testing_, _process managers_.

Notes about Node.js are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/nodejs/nodejs.js)** and **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/nodejs/nodejs2021.js)**.

#### Python

I am a beginner in Python. It is high on my "to-learn" list, I believe it's important to be brave enough to step out of comfort zone and be a beginner in something and start from scratch once again. I had few attempts to learn it between 2010. and 2014. but I gave up on it in favor of PHP. Main reasons were unavailability of free hosting at the time and I wanted to see my ideas alive. Also it was divided between versions 2.7 which was dominant and 3 which introduced additional confusion. I look at it from C/JavaScript perspective and it looks to me like a mixed bag, it's obvious that C libraries are under the hood, PHP-like snake case naming, OOP and functional paradigms mixed together like in JavaScript, familiar concepts from C#/JavaScript named or implemented differently, etc. In 2020. I had idea to make basic synth virtual instrument to get some practical experience but after some research I was disappointed to see how inferior Python was comparing to C++/C# in availability of mature GUI and audio signal processing libraries. I know Python really shines in Data science, Machine learning and Artificial intelligence but that is completely different land from web development which requires years of dedication on it's own. The most realistic opportunity for me is to make Flask backend for some my future React project.

For learning resources I used extensive _"Core Python learning path"_ on Pluralsight including _"Core Python 3: Getting-Started"_, _"Python 3: Beyond the Basics"_ and _"Advanced Python"_ by Austin Bingham and Robert Smallshire, _"Managing Python Packages and Virtual Environments"_ and _"Python Best Practices for Code Quality"_ from Reindert-Jan Ekker, and also _"Python Programming Tutorial for a JavaScript Developer"_ Youtube tutorial by Get it Done, _"Python for JavaScript Developers"_ blog article by Valentino Galigardi, _"Python Tutorials"_ Youtube playlist by Corey Schafer, _"Fullstack Flask - Build a Complete SaaS App with Flask"_ by Sumukh Sridhara, _"Python Crash Course For Beginners"_ and _"Pipenv Crash Course"_ Youtube tutorials by Brad Traversy, _"Python Tutorial for Programmers - Python Crash Course"_ Youtube tutorial by Mosh Hamedani, _"flask_for_startups"_ Github repository example by nuvic. There is real abundance of learning material for Python on internet.

I learned that Python is general purpose, multi-paradigm, interpreted, garbage collected, dynamically typed language with comprehensive standard library, _scalar types_ - all immutable, _relational operators_, _logical operators_, _in_ - membership operator, _is_ - identity operator, _str_ - immutable string type, _bytes_ - binary type, _modules_ - .py files, _python execution model_, _passing command line arguments_, _shebang_, _docstrings_, _object and type models_, _identity equality_, _value equality_, _references, not boxes_, _pass by reference_, _positional and keyword arguments_, _strong, dynamic type system_, _4 levels scope - LEGB_ - local, enclosing, global, built in, _everything is an object_, _list [...], tuple (...), ranges, dictionary {key: val}, set {...}_ - built in collections, _protocols_ - interface, _try, except, raise, finally_ - exceptions, _EAFP_ - easier to ask forgiveness than permission, _LBYL_ - look before you leap, _comprehensions_, _generator functions_ - iterables defined via functions, _iteration protocols_ - iter() and next(), _generator expressions_, _classes_ - custom types, _self_ - this, _\_\_init\_\_() _ - initializer, _methods_, _no access modifiers_ - public, _invariants_ - constraints, _duck typing_ - argument fitness determined at runtime, _late binding_ - use object to resolve method calling at runtime, _polymorphism_ - through late binding, _inheritance_ - used only for reuse, _files and file like objects_ - text and binary mode, _universal newline_ - OS independent, _with block_, _packages_ - folders, _modules_ - files, _sys.path_, _PYTHONPATH_, _\_\_init\_\_.py_, _subpackages_, _relative imports_, _namespace packages_, _executable directories_, _recommended layout_, _module singletons_, _positional, keyword and default arguments_, _callable instances_, _callables_, _conditional expressions_, _lambdas_ - single expression anonymous functions, _extended formal and actual arguments syntax_, _unpacking_ - destructuring, _global and local functions_, _closures_ - maintain references from parent scopes, _function factories_, _nonlocal and global keywords_, _decorators_ - syntactic sugar for higher oder functions, _@functools.wraps()_ - forwarding metadata, _instance attributes_, _class attributes_, _@staticmethod_, _@classmethod_, _@property_ - property getter, _@name.setter_ - property setter, _str(), repr()_ - simple and verbose representation, _format()_, _ascii(), ord(), chr()_ - ascii, unicode helpers, _int, float, Decimal, Fraction, complex()_ - number types, _abs(), round()_ - math helpers, _bin(), oct(), hex(), int()_ - base conversion, _datetime module_, _advanced comprehensions_, _map(), filter(), reduce()_, _iter(iterable)_ - iterable protocol, _next(iterator)_ - iterator protocol, _single and multiple inheritance_, _isinstance(), issubclass()_, _method resolution order - MRO_ - search order of inheritance graph, _super()_, _collection protocols_, _collections.abc_ - base collection protocols, _standard exception hierarchy_, _custom exceptions_, _implicit and explicit chaining exceptions_, _traceback objects_, _asserts_, _context manager protocol_, _@contextlib.contextmanager_ - decorator, _introspection_ - programs can inspect their own structure and state, _while else, for else clauses_, _try else clause_, _emulating switch statements_, _bitwise operators_, _bin(), to_bytes()_, _bytes and bytearray type_ - immutable and mutable sequences, _reading C structures_, _memoryview, memory mapped files_, _object internal representation as dictionary_ - attributes in obj.\_\_dict\_\_, methods in obj.\_\_class\_\_.\_\_dict\_\_, _descriptors_ - property objects that implement descriptor protocol, getter and setter equivalent, _instance allocation and initialization_, _metaclasses_ - convert class definition into a class object, customization of class creation, type of a class object, _class decorators_ - simpler and less powerful metaclasses alternative, _abstract base classes_ - customizable with metamethods, _abc module_ - tools for ABCs, _@abstractmethod_ - prevent instantiation of incomplete subclasses, _pip_ - manage packages, _virtualenv, venv_ - isolated, per project dependencies and runtime, _pipenv, poetry_ - deterministic dependency management, _pylint, black_ - PEP8 formatting and linting, _docstrigns_, _sphinx_ - .rst to html converter, _apidoc_ - docstrings to html, _type hints_ - optional type information, _mypy_ - static type checking.

Learning notes about Python can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/python)**.

#### FastAPI

When transitioning to Python, I looked for something relatively familiar and similar to Express. I started with Flask but quickly switched to FastAPI because it is more modern and has a brighter future. It is a high performance, asynchronous framework that is actively developed, has proactive leadership, a supportive community, and integrates well with machine learning frameworks. The plan with this was to make a side project and start gaining practical Python experience.

I did extensive research on boilerplate projects, looking for examples that would help me understand how to create a well-structured FastAPI backend. I found quite a few of them and carefully researched and analyzed them. the examples include updated _"full-stack-fastapi-template"_ by Tiangolo - FastAPI creator, _"fastapi_production_template"_ by Yerassyl Zhanymkanov, _"fastapi-rocket-boilerplate"_ by asacristani, _"FastAPI-boilerplate"_ by Igor Benav, _"fastapi-microservices"_ by Kludex, _"fastapi-alembic-sqlmodel-async"_ by jonra1993, _"FastAPI-template"_ by  Pavel Kirilin, _"fastapi-postgres-boilerplate"_ by kamranabdicse, _"whisper.api"_ by Ved Gupta, _"ultimate-fastapi-tutorial"_ example with tutorial by ChristopherGS, _"fastapi-tutorial"_ by Hamed Asgari. I read most of the relevant tutorials on "_testdriven.io_" and "_realpython.com_" by Michael Herman and other authors, very valuable blogs for learning Python. Also _"ArjanCodes"_ Youtube channel.

I learned about _Uvicorn ASGI_ - Asynchronous Server Gateway Interface, _native asynchronous support_ - key for high performance, _Pydantic_ - runtime checking, validation and serialization, _declarative routing_ - path operation decorator, function and parameters, _Dependency Injection_ - resolving dependencies for path operations,  _middleware_, _handling forms, files, headers_, _Response Model_ - infer, control, protect and document responses, _Background Tasks_, _Lifespan Events_ - startup and shutdown hooks, _Handling Errors_, _CORS_ - manage headers to allow clients from different origins, _templates_ - render HTML, _OpenAPI, ReDoc_ - autogenerated documentation.

Learning notes about FastAPI are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/fastapi)**.

#### C\#

I have 3 and a half years professional experience with .NET stack including both web (ASP, MVC, WebAPI, WCF) and desktop (WinForms, WPF) development and MSSQL dbms. Some of the projects I participated were large and complex, developed for years, by number of developers and designers.

Doing this I encountered with many C# language specifics like properties, events, nullable types, IDisposable, IQueryable<T>, IEnumerable<T> interfaces, lambdas, extension methods, expression trees, Func<T>, Action<T> delegates, late binding (dynamic), tuples, async/await, iterators (yield), etc. And also .NET features like threading, BackgroundWorker, parallel loops, reflection, async HttpClient.

As I used it every day on job I had no need using spare time to self-educate but I did made lessons learned clarifications as I worked, which can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/csharp)**.

#### PHP and Laravel

I first met PHP in 2010. in college course Web programming and since then I used it as main language for personal web projects until 2017. In july 2014. I read all Symfony docs expecting to find a job related to it. As Laravel got more popular I chose to put focus on it and use the benefits of it's growing community and excellent learning resources concerning general programming not just framework. At the time I was learning Laravel versions around 5.1 until 5.5 were actual.

I learned about all concepts from framework, like _databases_ - migrations, seeding, schema and query builder, _Eloquent_ - defining models and relations, _service providers_ - registering, booting, deferring, _services_ - binding, instantiating, facades, _caching_ - using Cache facade and different drivers, _error handling and logging_ - handling exceptions, error pages, monolog and custom handlers and formatters, _validation_ - in controllers, using Validator facade, form request classes, displaying errors, _testing_ - HTTP requests API, page interaction, response assertions, database testing, Eloquent model factories, mocking events and facades, _scheduler_ - scheduling functions, commands and scripts, _Elixir_ - Gulp automatization and assets managing, _Commands_ - encapsulating actions into command, sync and async dispatching, piping, _Authentication and authorization_ - using built-in authentication, guarding routes, HTTP basic authentication, custom providers, gates and policies, _Blade_ - templating, _routing and controllers_ - mapping routes to clojures and controllers, defining parameters, preventing CSRF, generating URLs, grouping request handling logic into controllers, using middleware (filters), _events_ - registering, publishing, subscribing, queuing (async - non-blocking), broadcasting events to client.

As resource I used Laracsts videos, and official docs, notes can be found on the following links: **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/php/laravel.php)**, **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/php/laravel%20docs.php)** and **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/php/laravel%20testing.php)**.

---

### Testing

#### Jest React

Since both React and Jest are made by Facebook Jest is recommended framework for testing React applications. In one of my previous jobs we used Jest with Enzyme helper library but recently its place took React Testing Library that better hides implementation details and makes tests more robust. So I invested some time to study this stack and I used it in my personal project for practice.

Beside official Jest and Testing Library docs for learning material I used _"React Testing For Beginners"_ by Scott Tolinski, _"Testing Javascript"_ with Kent C. Dodds, _"React.js Unit Testing and Integration Testing Tutorial"_ - Youtube playlist from Bruno Antunes, _"bulletproof-react"_ example repo by alan2207, _"TkDodo's blog"_ - blog about React Query.

I learned about _unit_ - component or a class, _red, green states_ - tests are based on exceptions, _ESLint_ - for static analysis, _jsdom_ - test environment similar to browser, _monkey patching_ - override property, _mock_ - replace original value or function and assert args or return value, _spy_ - mock that can restore original implementation, _ts-jest or babel_ - configuration, _projects_ - support for multiple configurations, _mock CSS and images_, _getBy, findBy, queryBy_ - query elements with Testing Library, _fireEvent, userEvent_ - dispatching events, _async tests_ - query elements with retries and timeout, _mocking http calls_, _msw_ - mock calls on http level, _act_ - wrapping React state updates, _testing redux_ - by testing interaction, _testing hooks_ - wrap in a temp component, _SWR and React Query_ - handling cache in tests, _testing forms_, _mocking router_, _Suspense and ErrorBoundary_ - testing loading and error states, _code coverage_ - optimally cover code with tests, _statements, branches, functions, lines_ - coverage elements, _jest-preview_ - visual tests debugging.

Learning notes about React testing are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/react%20testing.js)**.

#### Jest NodeJs

Jest is also dominant for testing Node.js backends which makes it logical choice for backend testing as well at present moment.

For learning resources I used _"Test Node.js Backends"_ chapter from Kent C. Dodds course,_"Testing Express REST API With Jest & Supertest"_ - Youtube tutorial from TomDoesTech, _"unit and integration tests"_ from Prisma docs, _"Test Node with Docker, docker-compose and Postgres"_ - Youtube playlist from productioncoder, various tutorials from dev.to and hashnode.com.

I learned about _middleware, controllers, services_ - layered, testable backend architecture, _node_ - test environment , _pure functions_ - easy to test, _unit testing_ - test every layer in isolation, _singleton and dependency injection_ -  two strategies for mocking the Prisma client in services unit tests, _Supertest_ - test client for controllers, _integration testing_ - test all layers at once including test database, _handling test database_ - create, migrate, destroy to keep tests isolated, _test CRUD operations_ - assert success and error responses and statuses. 

Learning notes about Node.js testing are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/nodejs/nodejs%20testing.js)**.   

#### Cypress

Cypress is modern headless browser used for automated testing, well documented with plenty of practical examples, with good support for Docker and CI environments so I invested some time in it.

For learning I used very good official docs including Youtube lessons and Github examples, _glebbahmutov.com/blog_ - blog articles by Gleb Bahmutov.

I learned about _installing and configuring_, _querying elements_, _command chains_, _retries_, _asserting_ - UI states, _http requests_ - intercepting and requesting, _fixtures_ - for fake data, _aliases_ - share context between tests, _session and cookies_ - handle user session, _custom commands_, _tasks_ - run arbitrary Node processes independent from browser, _environment variables_ - precedence, _running in Docker_ - using base and preinstalled images, _running in CI environments_ - locally, containers or actions, _taking screenshots and videos_.

Learning notes about Cypress are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/react/cypress.js)**.

---

### DevOps

#### Git

On my first job we used Subversion with very simple workflow. For my personal projects I always used Git. I was aware that Git is dominant tool in the industry and that it can get much more complex than that with a lot of specific use cases so I had to learn it on my own. Especially because versioning of the code is such sensitive operation where work of the entire team can be at risk if done incompetently. And the only way to stay in control is with knowledge and experience.

For learning resources I used _"Productive Git for Developers"_ - Egghead course by Juri Strumpflohner, _"Practical Git for Everyday Professional Use"_ - Egghead course by Trevor Miller, _"Practical Git"_ - course by Codecourse, _"How Git Works"_ - Pluralsight course by Paolo Perrotta, Youtube tutorials by Corey Schafer, various other Youtube tutorials, Stackoverflow answers for specific cases.

I learned about _distributed version control system_ - every member has local copy of the repository, _working directory, staging area, local repository and remote repository_ - core areas in Git, _commits_ - snapshots in time, _branches_ - copies of a repository, pointer to a commit, _feature, develop, staging, master, hotfix_ - basic workflow with branches, _merging_ - import changes from another branch, _rebasing_ - simplifying history graph, operations that change commit id must not be performed on public commits (amend, rebase, reset), _pull, push_ - syncing local and remote repository, _reset_ - undo local commits, _revert_ - undo public commits, _merge conflicts_ - avoiding and resolving, _stash_ - save temporary changes, _tag_ - non moving pointer to a commit with optional metadata, _configuring Git_, _pull request_ - mechanism that allows reviewing changes before merging into the branch, _cherry-pick_ - apply arbitrary commit from another branch, _patch_ - apply change without access to the repository, _submodules_ - include other repositories.

Learning notes about Git can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/git)**.

#### Linux

Linux is dominant OS on servers and a prerequisite for learning many other technologies. I first met Linux in 2010. while studying for college course Operating Systems where it was the main topic of study and then I learned for the first time about fundamental OS concepts in general. I was surprised by the complexity and the importance of the topic because OS is an environment where your programs will run so to use it efficiently you have to know how OS works as well. For practice I mostly used regular Ubuntu in VirtualBox because you are free to try out new things and if something goes wrong you can easily recreate environment. Ubuntu is widely used so all common errors are already well documented on internet.

For learning resources I used material from college course, Digital Ocean docs, _"Operating Systems & Linux Basics"_ - chapter from DevOps Bootcamp by Techworld With Nana, _"Getting Started with Linux"_ and _"Getting Started with the Linux Command Line"_ - Pluralsight courses by David Clinton, some Youtube videos from tutoriaLinux and David Bombal (on WSL).

I learned about _system and application software_, _kernel and user modes_ - two CPU modes, _processes_ - abstraction of a running program, _virtual memory, paging and segmenting_ -  memory management, _monolith, layered and microkernel_ - OS architectures, _system calls_, _POSIX_ - standardized OS interface, _process states_, _proces data structures_, _program memory segments_ - code segment - executable code, data segment - static and global variables, stack - local variables and function arguments, heap - dynamically allocated variables, _pipes, signals, queues and shared memmory_ - process communication, _threads_ - lightweight processes, _mutexes, semaphores, monitors, conditional variables_ - threads synchronization, _character and block devices_, _procfs and sysfs_ - virtual in-memory file systems, in Linux everything is a file, _Linux file system_, _command line_, _package managers_ - apt for Debian and yum for Red Hat, the most important difference between distributions, _snap_ - self contained installations without shared libraries, _vim and nano_ - command line text editors, _root, users and groups_, _file ownership and permissions_, _pipes and redirects_ - chain commands or redirect output to file, _sh and bash_ - shell interpreters, _shell syntax_ - variables, operators, arguments, conditionals, loops, functions, _environment variables_ - print, export, unset vars, _networking_, _ssh_ - access remote servers.

Learning notes about Linux can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/linux)**.

#### Virtualization

I use VirtualBox since 2015. and I always liked flexibility and convenience virtualization technology has to offer in quickly OS provisioning, safely testing new things, public repositories with pre-made images etc. In 2016. I first met Vagrant while working with Homestead environment and I liked this tool that enables you to have scripted configuration for a virtual machine. In 2020. I started thinking about setting up my own mini home server that will allow me to have hosting solution for personal projects without any limitations and in the process get a lot of practical experience with virtualization, Linux administration and DevOps. After I researched this topic on internet and Youtube I came to conclusion that it is wiser to just use cloud because it is pretty complex and time consuming topic and that I won't be able to maintain focus on JavaScript and do this in parallel. Uncontrolled curiosity can be harmful. But I still read and research on this topic from time to time because I find it interesting, I can afford that much.

For learning resources I used _"Vagrant Crash Course"_ Youtube tutorial by Traversy Media, virtualization tutorials from OneMarcFifty, LearnLinuxTV and The Digital Life Youtube channels.

I learned about _Type 1 and Type 2 hypervisors_ - weather they are installed directly on hardware or host OS, e.g. Type 1: VMware ESXi, MS Hyper-V, Proxmox, _virtualization advantages_ - decouple hardware and OS, flexible resource allocation, portability - OS as image file, backup VM state with snapshots, _Proxmox_ - virtual data-center based on Debian, _Portainer_ - web based administration tool for Docker containers, _Webmin_ - web based administration tool for Linux, _headless server_ - base OS controlled only through network, _migrate physical server to virtual machine_, _Vagrant_ - tool for provisioning virtual development environments, _Vagrantfile_ - configuration file in Ruby that describes virtual machine, _Vagrant provider_ - underlying hypervisor to be used e.g. VirtualBox, _Vagrant provisioner_ - method for installing software and configuring OS in virtual machine e.g. shell, Ansible, _Vagrant network_, _Vagrant synced_folder_ - access virtual machine's file system.

Learning notes about Virtualization are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/homelab)**.

#### Docker

I first met Docker in 2016. while I was learning Laravel. I researched alternative ways to have isolated development environment and one of them was Docker. I immediately recognized how fundamental container technology is, how convenient is ability to package application in containers and to have them just work in any environment without any additional configuration, isolation it provides, repositories with pre-made images, speed and smaller image sizes comparing to virtual machines, etc. So I made my own minimal dockerized Laravel environment with Xdebug support forked from Laradock. It helped me to get some practical experience. Github repository is **[here](https://github.com/nemanjam/laradock/tree/my_laradock)**. Later I also made another dockerized MERN environment which repository can be seen **[here](https://github.com/nemanjam/mern-docker)**. Nextjs Prisma Boilerplate, my project from 2022. fully relies on Docker containers for development, production, testing and end-to-end testing environments.

Beside official docs for learning material I used various tutorials on tech blogs, and tutorials from Traversy Media, Techworld With Nana and Fireship Youtube channels. Also I used Docker in professional scenario for a year.

I learned about _namespaces and control groups_ - Linux kernel isolation features that made OS level virtualization and containers possible, _Union File System_ - file system Docker uses for image layers, _Docker daemon and client_ - Docker uses client server architecture model with REST communication over Unix sockets, _image_ - read only layers, _container_ - top most writable layer or a running process, _Dockerfile_ - image blueprint where each command creates separate layer in the image, _docker-compose.yaml_ - support for multi-container applications, _Dockerfile and docker-compose syntax_, _docker and docker-compose commands_ - Docker CLI, _bind mounts (Host volumes), anonymous and named volumes_ - support for persistent data, _ports and environment variables_, _networks_ - support for communication between containers, _Docker Hub_ - repositories with images, _inspect command_ - debugging containers, _commands and entrypoints_ - overridable and non-overridable starting commands, _devcontainers_ - using containers for development environments, _ARG_ - build-time arguments, _environment variables_ - runtime arguments in containers and compose files, _extending docker-compose.yml_, running containers as non-root user, handling volumes and file permissions, _multistage builds_ - to reduce image size for prod environments, dockerizing Node.js applications, inspecting and optimizing image layers with Dive, administering containers with Portainer, routing containers in production with Treafik.

Learning notes about Docker can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/docker)**.

#### CI/CD

Modern software development implies shipping new features frequently in short cycles, utilizing automation in all stages of this process, allowing new code to be added with lowest risk possible, validated trough tests that catch errors in early stages, allowing multiple teams to work in parallel, and all of this in cost and time effective way. CI/CD is a development practice that makes this possible. Developer as a part of this loop is directly responsible not just for implementing features but also for writing unit and integration tests, ensuring code quality with linting and formatting, integrating code in version control from all team members, ensuring that testable build is available entire time. Me as a developer was aware of this so I invested some time to study about this subject.

For learning material I used _"CI/CD with Jenkins"_ - chapter from DevOps Bootcamp by Techworld With Nana, tutorials about Github Actions from Techworld With Nana, CoderDave and Kahan Data Solutions Youtube channels.

I learned about _code, build, test, release, deploy, operate, monitor, plan_ - steps in the devops loop, installing Jenkins as a Docker container, _Jenkins plugins_, _Freestyle job_ - used for simple standalone workflows, _Pipeline job_ - scripted pipeline as code, _Jenkinsfile_ - definition for pipeline written in Groovy, _Multibranch pipeline_ - one pipeline per each Git branch, _managing credentials in Jenkins_, _Jenkins Shared Library_ - reusable Groovy libraries, _Webhooks_ - trigger pipeline automatically on Git events. Related to Github Actions I learned about _events_ - Git events, _action_ - handler for event, _workflow_ - chain of actions, _step_ - shell command or action, _job_ - set of steps, _runners_ - Github hosted or self-hosted environments, _matrix_ - run job multiple time in different environments, _needs_ - run jobs sequentionally instead of parallel (default), _environment variables_, _secrets_, _caching_, _services_ - custom containers useful for test databases, _marketplace_ - public registry with actions, building Docker images, deploying with SSH actions, publishing releases with semantic versioning.

Learning notes about CI/CD are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/ci%20cd)**.

#### AWS

As Amazon is the most prominent cloud provider on internet and there is a realistic possibility that I will work with it in the future I took effort to get basic familiarity with it's most frequently used services. I already had one year professional experience with Azure as developer but I thought that it is not sufficient as AWS are known for complexity and large number of specialized services and there is plenty to learn.

For learning material I used _"AWS Services"_ and _"Kubernetes on AWS"_ chapters from DevOps Bootcamp by Techworld with Nana.

I learned about _IAM - Identity and Access Management_ - system for managing access to services and resources, _users_ - human or system users, _groups_ - multiple users, _roles_ - grant access to a service via policy, _policy_ - permission, _Regions_ - geographic locations around the world, _Availability Zones_ - discrete data centers within one region, used for high availability, _VPC - Virtual Private Cloud_ - private network across multiple availability zones, _subnet (private or public)_ - part of the network within single availability zone, _NACL - Network Access Control Lists_ - firewall on subnet level, _seecurity group_ - firewall on instance level, _CIDR Block_ - range of IP addresses defined through number of fixed bits, _EC2 - Elastic Compute Cloud_ - virtual server in the cloud, _AWS CLI_ - manage services via command line, _EKS - Elastic Kubernetes Service_ - Kubernetes on AWS, _Nodegroup_ - worker nodes, _eksctl_ - kubectl for EKS, _Fargate_ - serverless containers on AWS, _ECR - Elastic Container Registry_ - repositories with Docker images on AWS.

Learning notes about AWS are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/devops/aws2021.js)**.

#### Ansible

Ansible is the most interesting infrastructure as code tool for me. It is very applicable for small personal projects and in professional scenario the ability to script server configuration, provisioning software and application deployment and test it ahead of time is very valuable because it reduces the risk, eliminates possibility of human error and removes psychological pressure from team members. Configuration as code can be written only once so it can be reused in different environments (development, testing, production) or at some point in the future which saves time and reduces amount of tedious work, it can be versioned and evolve, also it documents server state.

As learning material I used _"Configuration Management with Ansible"_ - chapter from DevOps Bootcamp by Techworld With Nana, tutorials from Simplilearn and Edureka Youtube channels.

I learned that Ansible is _push system_ - controlling machine pushes the configuration to the target servers, _agentless_ - no need to install agents on managed systems, only SSH (on Linux), _declerative_ - it compares the difference between configuration and actual state and acts, _idempotent_ - can be executed multiple times without side-effects, _automated reporting_ - targets report commands success and failure, _playbook_ - configuration definition in yaml file, _task_ - specific job to be done, _module_ - the smallest unit of execution, _collection_ - from v2.10 bundle that consists of playbooks, modules, plugin and docs, _ad hoc commands_, _inventory_ - list of IP's or hostnames of target machines, can be dynamic, _roles_ - modular, reusable configurations with predefined folder structure including tasks, static files, variables, custom modules, _plugins_ - augment core functionality of the controlling node, _Ansible Tower_ - web UI dashboard, _Ansible Galaxy_ - repository with reusable roles, Ansible can manage both Unix and Windows systems with Python as only requirement.

Learning notes about Ansible are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/iac/ansible2021.js)**.

#### Terraform

It is not always practical to provision your cloud infrastructure through the UI or command line especially when you need multiple or disposable environments or when your infrastructure uses multiple providers. All these use cases are handled by Terraform allowing infrastructure as code approach for provisioning part similarly as Ansible does for configuring. I took effort to get basic familiarity with this tool and it's core concepts although I didn't yet get into configuration details as I have other topics of greater priority so I left that for some future time.

For learning resources I used tutorials from Sanjeev Thiyagarajan and TechWorld with Nana Youtube channels. _"IaC with Terraform"_ - chapter from DevOps Bootcamp by Techworld With Nana is something I plan to go over in the future.

I learned about _providers_ - plugins that allow management of resources on specific cloud provider or platform, _resources_ - services of a given provider, _state_ - file that tracks infrastructure state, _declerative_ - core component tracks difference between configuration and state and creates execution plan (similar as Ansible and Kubernetes), _module_ - set of configuration files that allows grouping and reusing resources, _variables_ - reusable values, _registry_ - repository with modules, 4 basic commands: _init_ - downloads providers specified in .tf configuration, _plan_ - calculates execution plan, _apply_ - executes the plan by creating or modifying infrastructure, _destroy_ - destroys infrastructure.

Learning notes about Terraform are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/iac/terraform2021.js)**.

#### Kubernetes

Kubernetes is industry standard for managing containerized applications. With growing popularity of microservices architecture and need for large scale applications arises the need for managing large number of containers which is impossible without orchestration tool. Very soon I realized that it is complex technology that requires serious effort and dedication to master, but I saw it as a challenge and decided to invest some time to try to understand core principles behind it.

For learning material I used _"Orchestration with Kubernetes"_ - chapter from DevOps Bootcamp by Techworld With Nana.

I learned about Kubernetes features: _high availability_ - no downtime, _scalability_ - easy to change number of containers, _auto-scaling_ - auto-adjust number of worker nodes based on load, _self-healing_ - health of containers is monitored and repaired, _disaster recovery_ - state of the cluster has backup and can be restored, _declerative_ - compares configuration with actual state and tries to match them, Kubernetes architecture: _master and worker nodes_ - master slave architecture, processes on master include: _API server_ - cluster entry point for UI, API and CLI client, _Controller manager_ - ensures that required state and cluster state match, implements control loop, _scheduler_ - assigns containers to nodes, _etcd_ - key value database that stores cluster state and snapshots for backup and restore, processes on workers include: _kubelet_  - process that ensures communication between master node and container runtime on that worker node, _cAdvisor_ - collects metrics about resource usage and performance of the container, _kube-proxy_ - process that ensures network communication between pods, the host, and the outside world, implements service component, _container runtime_ - containerd, runs containers, Kubernetes components: _pod_ - abstraction over container, _pod is ephemeral_ - dies and recreates frequently, _deployment_ - yaml definition for a pod, _replicaset_ - number of pods per deployment, _service_ - network interface for a pod, lives longer than the pod, internal of type ClusterIP or external of type LoadBalancer, _volumes_ - persisted data for the cluster, _persistent volume_ - not namespaced, _persistent volume claim_ - namespaced abstraction of the volume, _ConfigMap_ - volume type for configs defined in yaml, _Secret_ - volume type for secret data defined in yaml, _Namespaces_ - organize components logicaly inside the cluster, _Ingress_ - connects services inside the cluster with outside world, _StatefulSet_ - component for managing stateful containers - databases, _Helm_ - package manager, _chart_ - packaged application, _Operator_ - custom control loop that handles replicas for stateful containers, _kubectl_ - CLI interface, _Minikube_ - local cluster used for development and testing.

Learning notes about Kubernetes are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/iac/kubernetes2021.js)**.

---

### Fundamentals

#### Software architectures

I realized that for optimal understanding and utilization of OOP frameworks I should be familiar with fundamental concepts and theoretical background they use so I took effort to study architecture design. For resource I used material from college course which itself is concise collage of profession standard literature.

From there I learned the importance of not reinventing the wheel (_Only a fool learns from his own mistakes. The wise man learns from the mistakes of others. - Bismarck_ ) and how much reusability (in all of it's forms) can improve work productivity and quality of your programs. I learned about basic OOP design principles such as GRASP, SOLID, abstraction, encapsulation, different granularity of software architectures such as architectural styles (patterns), frameworks, design patterns, language idioms, writing modular and testable code with inversion of control and dependency injection, basic design patterns and their classification. I also learned about basic architectural styles like _layered (n-tier), pipe-filter, client-server, event driven, microservices, broker (queues), pear to pear, blackboard, master-slave_, 3 tier enterprise application architecture (MVC), two basic ORM patterns (Data Mapper and Active record) and principles they use (Unit of work, Identity map, Lazy load, data mapping), concepts used in presentation layer like one and two-step views, templating, page and front controller, ways to organise logic like service layer and transaction script.

Some notes about architectures are **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/other/architectures.js)**.

#### Software testing

I realized that frameworks are built to deal with complexity that grows along with the size of application keeping it at least linear and preventing exponential growth and the main methods to accomplish this are modules (complexity of the application is complexity of it's biggest module) and testing. Correctness of every stage in development process must be tested before releasing in production. Proof for this is the fact that in common application 40% of time is spent on implementation and 60% on testing, or even more. So I spent effort to learn about testing theory to understand testing concepts in frameworks. As resource I used material from faculty course _"Testing and Software quality"_.

I learned about different _tests granularity_ - unit, integration, system, acceptance, _testing methods classification_ - white and black box testing, bottom-up and top-down, code covering tests, functional tests. Also specifics in OOP and web apps testing, docblocks, debugging, centralized and distributed versioning systems. 

Some notes about this can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/other/testing.js)**.

#### Databases

For optimal understanding of Object Relational Mappers like Prisma, Entity Framework or Eloquent (model design, mappings, referenced objects...) I took time to remind myself about concepts from relational model like realtional algebra, schema analysis, normal forms, table decomposition, denormalization, redundancy, anomalies, joins. I used material from college course.

Notes about databases can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/databases)**.

#### Functional programming

As reading Staltz RX tutorial and from Crockford's lessons I found out that in order to fully understand functional concepts (like monads) in JavaScript you should know one true functional language like Haskel. And some problems are a lot easier to solve with functional than imperative programming. As I learned Lisp in college I took some time to remind with course material I had.

Notes about this can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/other/lisp.js)**.

---

### Other self education

#### Productivity and time management

With working on projects I realized that this requires self-organization skills so I took time to learn the basics.

I learned that you have to adapt your habits and workflow to the way human brain, psyche and body are functioning because the other way around is not possible. I learned cruciality of intrinsic motivation for creative intellectual work, common productivity pitfalls such as perfectionism, multitasking, procrastination, overanalysis (you can't predict everything), _priorities_ - rational allocation of resources by assigning priorities to tasks and goals and respecting them (long term goals before short term, important before urgent), _habbits_ - power of productive or unproductive habits and trashold for action to become automated and unconscious, _task managment_ - aggregate similar tasks, power of focus, one at the time, finishing unperfect, not losing in details (respect priority), outsorcing less important, _goals_ - goal and purpose oriented lifestyle, setting realistic goals and measuring results and progress, _distractions managment_ - 90% internal sources, focus on present not future or past, _problem managment_ - not reactive but proactive and predictive, focus on solutions, not problems, _importance of rest_ - energy is limited resource, make pauses, do more in long run, incubation phase is necessary for creative work, engaging in hobbies, _the way human mind works_ - single-threaded, internal dialogue, flow state, momentum, physical, emotional, intellectual level, _optimizing_ - 80/20 principle, recoverable and non-recoverable resources.

I learned about cognition principles like cognition biases, perception distortions, blindspots that can be useful in negotiation.

In order to be more efficient with learning I use text to speech software to read more with less effort, I use text or video resources when appropriate, cheetsheats, I take notes for easier reminding of what I learned, including lessons learned while doing projects.

Notes about this can be found **[here](https://bitbucket.org/nemanjabb/resume/src/master/notes/other/time%20organization%20and%20productivity.js)**.

---

---

# Personal projects

### HackerNews New Jobs

This is an analytics website for the HackerNews "Who's hiring" threads. It provides an easy and clear insight into fresh and recurring job ads by categorizing all companies into  first-time, new, and recurring posters for any month since June 2015. This helps job seekers focus on fresh opportunities. Search functionality provides quick look up of the entire posting history for any company.

My goal was to quickly create a functional prototype to gauge public interest in an app with this functionality. And the tech stack is suited for such purpose. It uses Algolia API as a data source along with scheduled task that parses new threads each month. The extracted data is then stored into SQLite database for performant and precise querying. Query results are then cached with Keyv for faster page responses. Http requests are also cached to mitigate Algolia API rate limiting. The website is Next.js app with default ShadcnUI components and charts. The app is deployed as a Docker image built either locally or in Github Actions.

More implementation details are documented [here](https://github.com/nemanjam/hn-new-jobs#implementation-details).

Demo link: [https://hackernews-new-jobs.arm1.nemanjamitic.com](https://hackernews-new-jobs.arm1.nemanjamitic.com)

You can read more in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/hn-new-jobs/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/hn-new-jobs/?at=master). Github repository is [here](https://github.com/nemanjam/hn-new-jobs). 

_Technologies used:_ Next.js 15, ShadcnUI, SQLite, better-sqlite3, Keyv, node-cron, TailwindCSS, Docker.  
_Date of development:_ autumn 2024.

### Developer blog nemanjamitic.com 

I decided to make a coding blog because I regularly research and solve coding problems, document them in my personal notes, and discuss them on online forums. Rather than having my insights dispersed across the internet and in notes, it's more effective to consolidate it into well rounded blog articles that others can use too, requiring only minimal additional effort. I would benefit from this by building a recognizable online presence and enhancing my ability to explain complex technical subjects clearly and effectively which is a valuable skill in any job.

I didn't want to rely on existing platforms like Hashnode, DevTo, or Medium. Instead, I wanted my own space on the internet. After researching various static site generators like Jekyll, Hugo, and Gatsby, I found they were outdated and declining, so I chose Astro as currently the best tool for the job.

After thoroughly researching and reviewing various open-source Astro personal site examples, I combined and reused the best solutions from each one (see the credits [section](https://github.com/nemanjam/nemanjam.github.io#credits)). After four months of work, I had a functional blog that I am satisfied with and ready to populate with content.

I intend to use it for years to come, so I devoted great care to creating a well-organized, clean, understandable, maintainable, and customizable code structure. It uses all the latest Astro features, including some experimental ones, and has a comprehensive feature set for a developer blog website. 

Content is organized around Post and Project models and supports both tags and categories, pagination, metadata handling, and a sitemap for good SEO. It includes dynamic Open Graph image creation, hierarchical layouts, optimized responsive images, React integration for interactive components, Tailwind styling with responsive layouts and typography, dark mode, and semantic color themes. The setup also allows for rendering remote markdown content, RSS and JSON feed endpoints, Twitter and YouTube embeds, code quality configuration, and three automated deployment methods with GitHub Actions, GitHub Pages, Nginx, and Docker, all documented in a comprehensive README file.

More information about the implementation can be found [here](https://github.com/nemanjam/nemanjam.github.io#implementation-details).


You can find more about the project in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/nemanjam.github.io/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/nemanjam.github.io/?at=master). Github repository is [here](https://github.com/nemanjam/nemanjam.github.io). I suggest that you use Github link because the project originally hosted there.

For the demo, there are currently 3 mirrors available:

1. **Nginx:** [https://nemanjamitic.com](https://nemanjamitic.com)
2. **Github Pages:** [https://nemanjam.github.io](https://nemanjam.github.io)
3. **Docker:** [https://nmc-docker.arm1.nemanjamitic.com](https://nmc-docker.arm1.nemanjamitic.com)

_Technologies used:_ Astro `4.14.4`, TailwindCSS `3.4.7`, React `18.3.1`, Zod, Satori, class-variance-authority.   
_Date of development:_ spring 2024.

### Reddit Unread Comments

This is the first Firefox/Chrome browser extension that I developed. I always thought that browser extensions development offers a lot of room for creativity for enhancing existing websites and building original features. While reading Reddit I felt that I sometimes I lose time having to reread long thread just to find a few new comments. So I built something that I wanted to use and wished already existed. It is a free, open source, privacy aware Firefox/Chrome extension that runs completely client side and doesn't affect existing Reddit behavior. After a few years working within declarative frontend frameworks it was useful reminder to work again a bit more with native JavaScript DOM and browser API's.

It is a browser extension for easier tracking of new comments in threads by highlighting them. It has two highlighting modes: 1. Highlight unread comments, 2. Highlight comments by time. You can quickly scroll through highlighted comments using `Ctrl + Space` for the next page of comments and `Ctrl + Shift + Space` to reset scroll to the first page. Additionally you can override the default Reddit's sort order to `By New` in all threads, reset a single or all threads history and User Settings to default.

If you use Reddit a lot, I suggest you give it a try.

You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/reddit-unread-comments/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/reddit-unread-comments/?at=master). Github repository is [here](https://github.com/nemanjam/reddit-unread-comments). 

YouTube demo video is here: [https://www.youtube.com/watch?v=dHw0pM3ZzqY](https://www.youtube.com/watch?v=dHw0pM3ZzqY)

I suggest that you use Github link because layout is optimized for Github markdown.

_Technologies used:_ React, TypeScript, IndexedDB, React Hook Form, RadixUI Themes, TailwindCSS.  
_Date of development:_ winter 2023.

### Next.js Prisma Boilerplate

React landscape changed, once again it made sense to take the most promising libraries from React ecosystem, make a practical project with them, see for myself how they work together in practice and brush up skills with the most up to date knowledge. At present time Next.js is dominant way to work with React, SSR became standard, Prisma is the most interesting ORM in TypeScript world, React Query is high quality library so the choice of libraries wasn't too difficult.

Typically in companies foundation for a project is made by the most experienced developers that need to make correct decisions about software architecture, choice of technologies and libraries, establish best practices for writing reliable, testable and maintainable code, so that project can scale well as it grows in future without risk of becoming unmaintainable at some point. I saw challenge in doing that myself and that's why I chose to make such base boilerplate project.

This time I made complete foundation for a project including multiple development, testing and production environments, scalable component architecture on frontend, separated concerns with middleware, controllers and services on backend, highly customizable styling with theming support, unit and integration tests for both frontend and backend, automatized end-to-end tests, CI/CD pipelines, detailed documentation, heavily relying on Docker containers in every step of the process.

You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/nextjs-prisma-boilerplate/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/nextjs-prisma-boilerplate/?at=master). Github repository is [here](https://github.com/nemanjam/nextjs-prisma-boilerplate). There is also a separate repository for deployment with Traefik with [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/traefik-proxy/README.md?at=master&fileviewer=file-view-default) and [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/traefik-proxy/?at=master). Github repository is [here](https://github.com/nemanjam/traefik-proxy).

I suggest that you use Github links because layout is optimized for Github markdown.

_Technologies used:_ React `18.2.0`, Next.js `12.2.0`, Node.js `16.13.1`, Prisma `4`, Postgres `14.3`, TypeScript `4.7.4`, React Query `4-beta`, Axios, React Hook Form `8-alpha`, React Dropzone, Zod, msw, TailwindCSS `3`, Jest `28`, Testing Library React, Cypress `9.6.1`, Traefik `2.5.6`.   

_Date of development:_ winter 2021. - spring 2022.

### MERN Boilerplate

This improved version of the MERN boilerplate I did last year. It is a fullstack template that will serve as base for my future projects that use REST architecture. It has robust Auth system, User and Message models with full CRUD operations, server and client validations, unified ES syntax on both client and server, User and Admin roles, Redux state management and more.

You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/mern-boilerplate/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/mern-boilerplate/?at=master). Github repository is [here](https://github.com/nemanjam/mern-boilerplate).

_Technologies used:_ React with Hooks, Redux, Thunk, Formik, Node.js, Babel, Passport, Mongoose, MongoDB.  
_Date of development:_ spring 2020.

### Audio Twitter

This is pretty complex Twitter clone with audio messages instead text. I made it ti polish my skills in working with audio, making realtime GraphQL app, state management without Redux with Apollo cache and working with MongoDB. It has number of features like recording and visualizing audio, number of subscriptions, paginations and 4 dark/light green/orange Material-UI themes.

You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/audio-twitter/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/audio-twitter/?at=master). Github repository is [here](https://github.com/nemanjam/audio-twitter).

_Technologies used:_ React with Hooks, Apollo Client, Apollo Server, Material-UI, Wavesurfer, Mongoose, MongoDB.  
_Date of development:_ winter 2019.

### RN Chat

This is chat app made with React Native, NativeBase and Apollo Hooks. I made it to demonstrate my skills in React Native, GraphQL and database design.

You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/rn-chat/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/rn-chat/?at=master). Github repository is [here](https://github.com/nemanjam/rn-chat).

_Technologies used:_ React Native, Apollo Client, Apollo Server, NativeBase, React Navigation, Redux, Sequelize, PostgreSQL.  
_Date of development:_ autumn 2019.

### Redux Ecommerce

This is 7 pages ecommerce website made with React Hooks and Redux. It is made to demonstrate my skills with Hooks and Redux.

You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/redux-ecommerce/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/redux-ecommerce/?at=master). Github repository is [here](https://github.com/nemanjam/redux-ecommerce).

_Technologies used:_ React, Hooks, Redux, Redux Thunk, React Bootstrap.  
_Date of development:_ summer 2019.

### Curiouscat clone

This is UI clone of the website [curiouscat.me](https://curiouscat.me) based on the Tweeper template from Material-UI docs. It is made for practise with Material-UI themes.

You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/curious-cat/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/curious-cat/?at=master). Github repository is [here](https://github.com/nemanjam/curious-cat).

_Technologies used:_ React, Material UI.  
_Date of development:_ summer 2019.

### MERN Docker

This is Docker template for the development of MERN applications. It makes use of docker-compose and separate Dockerfiles for client and server. It has 4 containers: client, server, mongo and adminmongo. It contains basic MERN CRUD Todo app. You can find more details in [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/mern-docker/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/mern-docker/?at=master). Github repository is [here](https://github.com/nemanjam/mern-docker).

_Technologies used:_ Docker.  
_Date of development:_ spring 2019.

### Tweet of the day

This application is my mini start-up project. Main functionality is traversing Twitter timelines which enables custom filtering that is not supported with API, and embedding tweets into Twitter collections or presenting them in other way. Preference is to reuse Twitter feeds as possible and embed them into static pages, and also keeping as less as possible data in database which contributes to real time character of the application since posts on Twitter have short term relevance.

It is done in Laravel 5.3 and it's using Angular directive to embed Twitter collections with custom CSS. You can read more details from [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/tweetoftheday/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/tweetoftheday/?at=master). Serbian and Turkish bots are still alive and available here: [@totd_rs](https://twitter.com/totd_rs) and [@totd_tr](https://twitter.com/totd_tr).

_Technologies used:_ Laravel, Angular.  
_Date of development:_ autumn 2018.

**Doctrine version Tweet of the day**

At first it was started as Doctrine application which is abandoned as priorities were not relevant nor optimal. To examine code of this deprecated project see [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/tweetoftheday%20doctrine/README.md?at=master&fileviewer=file-view-default) and [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/tweetoftheday%20doctrine/?at=master)

_Technologies used:_ Doctrine.  
_Date of development:_ spring 2015.

### Follows whom

This application was made unplanned as quick prototype to test amount of public interest for this kind of service. It utilizes feature of Twitter API that it is returning chronologically ordered results of who someone followed recently, and then uses that data to make histogram overview of how popular accounts is someone following. To see details, screenshots and working demo see [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/followswhom/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/followswhom/?at=master).

_Technologies used:_ jQuery, Bootstrap, Doctrine.  
_Date of development:_ spring 2015.

### Tweets on map

This is mashup application combining API from two sources into one presentation. It allows you to search geotagged tweets and displays them on the Google map. This project is done as practical part of my master thesis. To see details, screenshots and workng demo see [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/tweetsonmap/README.md?at=master&fileviewer=file-view-default) and to examine code see [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/tweetsonmap/?at=master).

_Technologies used:_ Google Maps v3, jQuery, Bootstrap.  
_Date of development:_ december 2013.

### My Laradock

In may 2016. Laradock was the most complete Docker environment for Laravel, but even as such it had not support for debugger and database admin, so I added Xdebug and PHPMyAdmin, and removed unnecessary containers. It helped me to get practical experience with Docker. For details see [README.md](https://bitbucket.org/nemanjabb/resume/src/master/projects/my_laradock/README.md?at=master&fileviewer=file-view-default) and [repository root](https://bitbucket.org/nemanjabb/resume/src/master/projects/my_laradock/?at=master). It is originally hosted on Github as branch of the fork of the Laradock, link [here](https://github.com/nemanjam/laradock/tree/my_laradock).

_Technologies used:_ Docker.  
_Date of development:_ may 2016.

---

#### Tools used

- VS Code
- Netbeans
- Docker Toolbox for Windows
- Git
- DBeaver SQL admin
- Studio 3T MongoDB admin
- Android Studio
- Nox and MEmu emulators

---

---

# Languages

- Serbian (native)
- English (full working proficiency, studied it 4 years in high school, 2 years in college, self learned, main language at work, majority of learning material and daily reading)
- German (limited working proficiency, I studied it for 8 years in school, knew it very well, I haven't had a chance to use it)

---

---

# Education

### Faculty of Electronic Engineering, University of Nis, Serbia

#### Master's degree, Department for Computer Science, Information Technologies Module

(October 2008. - July 2014.)

#### [Link to institution website](http://www.elfak.ni.ac.rs/en/courses/master-academic-studies)

### Courses:

#### 1st year

1. Fundamentals of electrical engineering 1
2. Physics
3. Introduction to computing
4. Mathematics 1
5. Mathematics 2
6. Fundamentals of electrical engineering 2
7. Algorithms and programming
8. Electronic components
9. Business communications
10. Sociology

#### 2nd year

1. Discrete mathematics
2. Digital electronics
3. Object oriented programming
4. Computer systems
5. Logic design
6. Probability and statistics
7. Data structures
8. Programming languages
9. Telecommunications
10. Operating systems

#### 3rd year

1. Databases
2. Computer architecture
3. English 1
4. Computer networks
5. Object-oriented design
6. Web programming
7. Introduction to software engineering
8. Information systems
9. Microcomputer systems
10. Automatic control
11. Project / Final exam

#### 4th year

1. English 2
2. Distributed systems
3. Methods and systems for signal processing
4. Information security (Cryptography)
5. Artificial intelligence (Lisp)
6. Computer graphics (GD, OpenGL)
7. Parallel systems
8. Multimedia systems
9. Compilers
10. Internet technology
11. Graph theory

#### 5th year

1. Legal aspects of informatics
2. Project management
3. Information retrieval
4. Advanced methods for image processing
5. Electronic commerce (JEE)
6. Design patterns
7. Professional practice / team project
8. Master thesis

#

---

#### Projects done during studies

- Bibtex parser, PHP, MySQL - web application, in course Project/Final exam, 2010.
- Modified Huffman fax machine compression, C++ Boost, Dynamic Bitset - Windows application, recognized on ICEST 2011 conference, in course Methods and systems for signal processing, 2011.
- Axis webservice, JAXP, JSP, XPath - Swing application, in course Internet technology, 2011.
- Security of the web applications - presentation, in course Internet technology, 2011.
- Website search engine optimization - presentation, in course Information Retrieval, 2012.
- J2EE Webshop, JPA, JSP, EJB - web application, in course Electronic commerce, 2012.
- ElfakAmp - MS Project gantt chart project, in course Project management, 2012.
- Tweets on map, Twitter API, Google Maps Api, Bootstrap, PHP, jQuery - web application, practical part of the master thesis, 2014.
- Mashup applications and Twitter software architecture, theoretical part of the master thesis, 2014.

---

- 1\. Oct. 2008. - 1. Feb. 2014. - 5 years 4 months - 50 exams, 7.58
- 1\. Oct. 2008. - 2. Jul. 2014. - 5 years 9 months - master thesis

---

---

# Miscellaneous

### My programming philosophy

**Reusability**

My first steps in web development were in 2010 when I started experimenting with PHP and small personal projects. In the beginning I wrote logic for everything by myself reinventing the wheel in every step. Then I discovered software architectures and frameworks and I realized how important is reusing existing wisdom from other people. I find reusability as important tool to write better software and be more efficient.

> Is there anyone so wise as to learn by the experience of others? - Voltaire  
> One month of inventing your own algorithm can save up to 15 minutes of Googling.   
> The man who asks a question is a fool for a minute, the man who does not ask is a fool for life. -  Confucius   

**Simplicity**

I prefer simplicity in design, music, human relations and not just in code. Human intellect is not without limits and it can handle complexity only up to certain extent.

> Simplicity is prerequisite for reliability. - Dijkstra  
> If you can't explain it simply, you don't understand it well enough. - Einstein  

**Work smarter, not harder**

I believe hard work, persistence and discipline are important. On the other hand sometimes you need to step back and rethink and reflect if your method is optimal or even completely wrong. So both sides are important, they need to be in balance.

> Insanity is doing the same thing over and over again and expecting different results. - Einstein   

**Pragmatism over idealism**

I believe way to success is paved with small steps that are good enough, it is iterative process with trial and errors rather than a single perfect shot.

---

### Hobbies

Fishing, lure making, cycling, playing guitar, music production in FL Studio, DIY electronics, homelab and selfhosting.