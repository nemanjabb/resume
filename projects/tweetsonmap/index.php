<?php

/* Load required lib files. */
session_start();
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');
require_once('fje.php');
require_once('log.php');
mojlog();

$connection = null;
$status = '';
/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) 
{
    //header('Location: ./clearsessions.php');
	//ocistim sesiju iz koda clearsessions.php
	$_SESSION = array();
	
	//proverim moje tokene, i odstampam dugme connect.php
	if (CONSUMER_KEY === '' || CONSUMER_SECRET === '' || CONSUMER_KEY === 'CONSUMER_KEY_HERE' || CONSUMER_SECRET === 'CONSUMER_SECRET_HERE') 
	{
		echo 'You need a consumer key and secret to test the sample code. Get one from <a href="https://dev.twitter.com/apps">dev.twitter.com/apps</a>';
		exit;
	}
	$status = 'you need to sign with twitter to be able to search.';
	$login_dugme = '<a id="login_a" href="./redirect.php" class="btn btn-default btn-sm">Sign in with Twitter</a>';//Sign in with Twitter
} else {
	/* Get user access tokens out of the session. */
	//iz sesije ih vadi, ja iz baze; ne treba mi baza, ne cuvam nista o useru
	//sve iz sesije. dok se ne loguje sa tw nista ne znam o njemu
	$access_token = $_SESSION['access_token'];

	/* Create a TwitterOauth object with consumer/user tokens. */
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
	//odstampaj loged in dugme
	$login_dugme = '<a id="login_a" href="./logout.php" class="btn btn-default btn-sm" alt="Log out" title="Log out">Log out</a>';
}
	
	if($connection != null)
	{
		if(!isset($_SESSION['user']))	{
			$_SESSION['user'] = proveri_usera($connection);
			
		}
		$status = 'you are logged in.';
	}

?>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta name="keywords" content="tweet, twitter, map, google, geotagged">
	<meta name="description" content="See geotagged tweets on Google map.">
	<link rel="icon" href="favicon.ico" type="image/ico">
	
	<title>Tweets on map</title>
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="./css/moj.css">
	<script src="./js/jquery-1.10.2.min.js"></script><!--jquery-2.0.3.min-->
	<script src="./bootstrap/js/bootstrap.js"></script>
	
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
	<script src="./js/moment.min.js"></script>
	<script src="./js/infobox.js"></script>
	
	<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
	<script src="./scrollbar/jquery.nicescroll.min.js"></script>
	<script src="./js/URI.js"></script>
	<script src="./glavna.js"></script>	
	
</head>
<body>
<form method="post" action="ajax.php"  name="gl_forma" id="gl_forma">

    <div id="navbar1" class="navbar navbar-default navbar-static-top" role="navigation">
        <a class="navbar-brand" href="#">Tweets on map</a>
          <ul class="nav navbar-nav">
			  <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
				<ul class="dropdown-menu">
				  <li id="full_scr" ><a id="full_scr_a" href="#">Full screen</a></li>
				  <li id="clear_all"><a href="#">Clear all fields</a></li>				  				
				  <li id="tw_table"><a id="tw_table_a"href="#">Show tweets table</a></li>
				  <li style="display: none" id="share_rez"><a id="share_rez_a" href="">Share on Twitter</a></li>
				  <li data-toggle="modal" data-target="#myModall" ><a id="share_link_a" href="#">Share link</a></li>
				  <li data-toggle="modal" data-target="#myModal1" ><a href="#">Help</a></li>
				</ul>
			  </li>
			</ul>

			<div style="" class="col-md-5">
			  <div style="width:100%" class="navbar-form navbar-left" role="search">			  
				<div  class="input-group">
				  <input type="text" style="width:100%" name="glavni_search" id="glavni_search" class="form-control input-sm">
				  <span class="input-group-btn">
					<button name="search_button" id="search_button" value="Search" data-loading-text="Wait..." class="btn btn-default btn-sm" type="submit">Search</button>
				  </span>
				</div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</div>
		
			  <div class="navbar-form navbar-left">
				<div class="btn-group btn-group-sm left-offset">
				  <button name="previous" id="previous" value="Previous" type="button" data-loading-text="Wait..." class="btn btn-default btn-sm">Previous</button>
				  <button name="next" id="next" value="Next" type="button" data-loading-text="Wait..." class="btn btn-default btn-sm">Next</button>
				</div>
			  </div>
			  			 
 			  <div style="margin-right: 13px; float:right;" class="navbar-form navbar-right">
	<?php
			if(isset($_SESSION['user']['avatar'])) {
				echo '<img class="left-offset" height="29px" width="29px" style="margin-top:1px; margin-right:7px;" src="'.$_SESSION['user']['avatar'].'" />';
				echo $login_dugme; //<a href="./redirect.php" class="btn btn-default btn-sm">Sign in with Twitter</a>
			} else
				echo $login_dugme;
	?>
			  </div>
       
	</div>

<div id="container1" class="container">
	  
<div  id="row1" class="row row1">
  <!--<div id="gmap_row"> -->
<!--
Extra small grid (<768px) 	.col-xs-*
Small grid (≥768px) 	.col-sm-*
Medium grid (≥992px) 	.col-md-*
Large grid (≥1200px) 	.col-lg-* -->
  <div id="left-column" class="col-xs-12 col-sm-4 col-md-4 col-lg-3"><!--col-xs-6 za gornji i donji moze, col-md-3:9 nece nesto -->
  
	<div id="left-column-form"class="form-horizontal" role="form">
	
	<div class="panel-group" id="accordion">
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
		  <span style="margin-right:10px" class="glyphicon glyphicon-list"></span>
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">			
			  Query help			  
			</a>
		  </h4>
		</div>
		<div id="collapseOne" class="panel-collapse collapse">
		<div id="q_h_div" class="panel-body">

		<table class="table"> 
		 <tr >
			<th>Words:</th>
		  </tr>
		  <tr>
			<td class="form-group"><label  class="col-sm-4 control-label moja-labela">All of these</label>
			<input class="form-control input-sm" type="text" name="all" id="all"></td>
		  </tr>
		  <tr>
			<td class="form-group"><label style="text-align:left;" class="col-sm-4 control-label moja-labela">Exact phrase</label>
			<input class="form-control input-sm" type="text" name="exact" id="exact"></td>
		  </tr>
		  <tr>
			<td class="form-group"><label  class="col-sm-4 control-label moja-labela">Any of these</label>
			<input class="form-control input-sm" type="text" name="any" id="any"></td>
		  </tr>
		  <tr>
			<td class="form-group"><label  class="col-sm-4 control-label moja-labela">None of</label>
			<input class="form-control input-sm" type="text" name="none" id="none"></td>
		  </tr>
		  <tr>
			<td class="form-group"><label  class="col-sm-4 control-label moja-labela">Hashtags</label>
			<input class="form-control input-sm" type="text" name="hash" id="hash"></td>
		  </tr>
		  <tr >
			<th>Accounts:</th>
		  </tr>
		  <tr>
			<td class="form-group"><label  class="col-sm-4 control-label moja-labela">From these</label>
			<input class="form-control input-sm" type="text" name="from" id="from"></td>
		  </tr>
		  <tr>
			<td class="form-group"><label  class="col-sm-4 control-label moja-labela">To these</label>
			<input class="form-control input-sm" type="text" name="to" id="to"></td>
		  </tr>
		  <tr>
			<td class="form-group"><label  class="col-sm-4 control-label moja-labela">Mentioning</label>
			<input class="form-control input-sm" type="text" name="mention" id="mention"></td>
		  </tr>
		  </table>
		  
		  </div>
		</div>

	
	<div class="panel-heading">
	  <h4 class="panel-title">
	  <span style="margin-right:10px" class="glyphicon glyphicon-list"></span>
		<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
		  Parameters
		</a>
	  </h4>
	</div>
	
	<div id="collapseTwo" class="panel-collapse collapse">	<!--in-->
	<div style="" class="panel-body">	
	  <div class="list-group">
		  <div style="margin-bottom:-15px" class="list-group-item">  
			  <div class="form-group">
				<label for="latitude" class="col-sm-4 control-label">Latitude</label>
				<div class="col-sm-7">
				  <input type="text" class="form-control input-sm" name="latitude" id="latitude" placeholder="Latitude">
				</div>
			  </div>
					 
			  <div class="form-group">
				<label for="longitude" class="col-sm-4 control-label">Longitude</label>
				<div class="col-sm-7">
				  <input type="text" class="form-control input-sm" name="longitude" id="longitude" placeholder="Longitude">
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="radius" class="col-sm-4 control-label">Radius</label>
				<div class="col-sm-7">
				  <input type="text" class="form-control input-sm" name="radius" id="radius" placeholder="Radius">
				</div>
			  </div>
			</div> 
			<div class="list-group-item"> 
				 <select name="lang" id="lang" class="form-control input-sm">
				  <option value="all">Any language</option>
				  <option value="en">English</option>
				  <option value="de">German</option>
				  <option value="ru">Russian</option>
				</select>
			</div>	
			<div class="list-group-item"> 	
				<select name="type_radio" id="type_radio" class="form-control input-sm">
				  <option value="mixed">Mixed</option>
				  <option value="recent">Recent</option>
				  <option value="popular">Popular</option>
				</select>
			</div>
		  <div style="margin-bottom:-15px" class="list-group-item">  
		  <div class="form-group">
			<label for="count" class="col-sm-5 control-label">Count</label>
			<div class="col-sm-7">
			  <input type="text" class="form-control input-sm" name="count" id="count" value="99" placeholder="100">
			</div>
		  </div>
		  <div class="form-group">
			<label for="until" class="col-sm-5 control-label">Older than</label>
			<div class="col-sm-7">
			  <input type="text" class="form-control input-sm" name="until" id="until" placeholder="YYYY-MM-DD">
			</div>
		  </div>
		</div>
		
<!--	<div class="list-group-item">
		  <h4 class="list-group-item-heading">Request</h4>
		  <p class="list-group-item-text">success.</p>
		</div>
		<div class="list-group-item">
		  <h4 class="list-group-item-heading">Results found</h4>
		  <p class="list-group-item-text">zero.</p>
		</div>
-->		
<!--		<div class="list-group-item">
		  <h4 class="list-group-item-heading">Status</h4>
		  <p  class="list-group-item-text">Status: <span class="loading2" id="statusId"><?php //echo $status; ?></span><span style="margin-left:2px;" class="loading1"><img src="./images/ajax-loader.gif" /></span></p>
		  samo ga pregazi status iz ajax.php ovog iz index.php
		</div>
-->
  
	</div><!--list-group-->
	</div>
	</div><!--collapseTwo-->
		<!--status van panela-->
		<div class="list-group-item">
		  <h4 class="list-group-item-heading">Status</h4>
		  <p  class="list-group-item-text">Status: <span class="loading2" id="statusId"><?php echo $status; ?></span><span style="margin-left:2px;" class="loading1"><img src="./images/ajax-loader.gif" /></span></p>
		  <!--samo ga pregazi status iz ajax.php ovog iz index.php-->
		</div>
		
	</div>
	</div><!--panel-group-->
	</div>
	
  </div><!--left-column-->
  
  <div id="wrapper1" class="col-xs-12 col-sm-8 col-md-8 col-lg-9"><!--col-xs-6 za gornji i donji moze -->
  
	<div id="gmap"></div><!--to izbacen kontejner za mapu da zakacis za row-->
	<div id="over_map_cont">
	<div id="over_map_info"  class="navbar-default">
			<div id="resultsId1" class="alert-link1"><span class="loading2" id="resultsId"><?php echo $status; ?></span><span style="" class="loading1"><img src="./images/ajax-loader.gif" /></span></div>
			<!--<a class="close" style="margin-left:3px; display: inline-block;" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>--><!--ovo je close x -->
	</div>
	</div>
  </div>
	<!--</div>za mapu-->
</div>


<div style="" id="row3" class="row">
	<table style="margin-bottom:0px;" class="table table-bordered table-condensed">
		<tr id="th1"><th style="width:4%;"><span style="margin-left:5px; margin-top:3px;" class="glyphicon glyphicon-user"></span></th><th style="width:12%;">User</th><th style="width:8%;">Date</th><th style="width:76%;">Text</th></tr> 
	</table>
</div>
<div style="" id="row2" class="row"><!--table-responsive-->
	<table class="table table-bordered table-condensed table-hover "><!--table-striped-->
		<!--<tr><th>Avatar</th><th>Screen name</th><th>Date</th><th>Text</th></tr>-->
		<tbody style="font-size:12px; " id="tbody1"></tbody>
	</table>
</div>


</div>
</form>

<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Help</h4>
      </div>
      <div style="/*padding:0px*/" class="modal-body">
	  
	<!--<h4>Help</h4>-->
	<p>Use either "Query help" or "Search" fields. You can't use both. "Query help" fields are optional and you can achieve same effect with "Search" field using search operators listed in table below.</p>
	<p>Note that when hided field value is preserved. Use "Clear all fields" or doubleclick any text field to clear all input fields (hided fields included, full screen mode included).</p>
	<p>Getting coordinates on map click in full-screen mode is disabled.</p>
	<p>You can also tweet, favorite and retweet tweets directly from the map.</p>

	
	<hr>
	<h5><b>Search operators</b></h5>
	<table style="font-size:10px; margin-bottom:0px;" class="table table-bordered table-condensed">
	  <thead>
		<tr>
		  <th>Operator</th>
		  <th>Finds tweets...</th>
		</tr>
	  </thead>
	  <tbody>
		<tr><td>twitter search</td><td>containing both "twitter" and "search". This is the default operator.</td></tr>
		<tr><td><b>"</b>happy hour<b>"</b></td><td>containing the exact phrase "happy hour".</td></tr>
		<tr><td>love <b>OR</b> hate</td><td>containing either "love" or "hate" (or both).</td></tr>
		<tr><td>beer <b>-</b>root</td><td>containing "beer" but not "root".</td></tr>
		<tr><td><b>#</b>haiku</td><td>containing the hashtag "haiku".</td></tr>
		<tr><td><b>from:</b>alexiskold</td><td>sent from person "alexiskold".</td></tr>
		<tr><td><b>to:</b>techcrunch</td><td>sent to person "techcrunch".</td></tr>
		<tr><td><b>@</b>mashable</td><td>referencing person "mashable".</td></tr>
		
		<!--<tr><td>"happy hour" <b>near:</b>"san francisco"</td><td>containing the exact phrase "happy hour" and sent near "san francisco".</td></tr>
		<tr><td><b>near:</b>NYC <b>within:</b>15mi</td><td>sent within 15 miles of "NYC".</td></tr>
		<tr><td>superhero <b>since:</b>2010-12-27</td><td>containing "superhero" and sent since date "2010-12-27" (year-month-day).</td></tr>
		<tr><td>ftw <b>until:</b>2010-12-27</td><td>containing "ftw" and sent up to date "2010-12-27".</td></tr>
		-->
		<tr><td>movie -scary <b>:)</b></td><td>containing "movie", but not "scary", and with a positive attitude.</td></tr>
		<tr><td>flight <b>:(</b></td><td>containing "flight" and with a negative attitude.</td></tr>
		<tr><td>traffic <b>?</b></td><td>containing "traffic" and asking a question.</td></tr>
		<tr><td>hilarious <b>filter:links</b></td><td>containing "hilarious" and linking to URLs.</td></tr>
		<!--<tr><td>news <b>source:twitterfeed</b></td><td>containing "news" and entered via TwitterFeed</td></tr>
		-->
	 </tbody>
	</table>
     
      </div>
      <div style="margin-top:0px;" class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal redirect ok-->
<div class="modal fade " id="myModalr" tabindex="-1" role="dialog" aria-labelledby="myModalLabelr" aria-hidden="true">
 <div class="modal-dialog ">
    <div class="modal-content">
	
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabelr">Redirecting...</h4>
      </div>
      <div class="modal-body">
      <p>You need to be signed with Twitter in order to see the results. Click "Proceed" to redirect to Twitter authorization page.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
        <a id="proceed_a" type="button" class="btn btn-primary btn-sm">Proceed</a>
      </div>
	  
    </div>
  </div>
</div>

<!-- Modal share link-->
<div class="modal fade " id="myModall" tabindex="-1" role="dialog" aria-labelledby="myModalLabell" aria-hidden="true">
 <div class="modal-dialog ">
    <div class="modal-content">
	
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabell">Share link</h4>
      </div>
      <div class="modal-body">
	  <!--<input id="share_link_input" type="text" class="form-control input-sm">-->
	  <textarea id="share_link_input" style="resize: none;" class="form-control" rows="2"></textarea>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
	  
    </div>
  </div>
</div>

</body>	
</html>