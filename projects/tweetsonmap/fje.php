<?php


	function validateDatum($str)	{
		return preg_match('/^([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $str);
	}
	function validateLatLongKm($str)	{
		return preg_match('/^(\-?\d+\.?\d*),\s*(\-?\d+\.?\d*),\s*(\d+km|^\d+mi)$/', $str);
	}
	function validateBrDo100($str)	{
		return preg_match('/^(100|\d{1,2})$/', $str);
	}
	function sendPost($url, $data)//mora celo url http://
	{	
		$options = array(
			'http' => array(
			'method'  => 'POST',
			'content' => json_encode($data),
			'header'=>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
			)
		);

		$context  = stream_context_create( $options );
		$result = file_get_contents( $url, false, $context );
		return $response = json_decode($result);		
	}
	
	function search_tweets($connection, $params)
	{
	
		$response = $connection->get('search/tweets', $params, true);//trebalo true
		if(!empty($response['errors']))
			return $response;
		/*
		$i=0;
		foreach($response['statuses'] as $tvit)	{
			echo 'i = '.$i++.'</br>';
			//echo 'geo: '. print_r($tvit['geo']).'</br>';
			//echo 'coordinates: '.	print_r($tvit['coordinates']) .'</br>';
			//echo 'place: '.	print_r($tvit['place']).'</br>';
			echo 'geo x : '. $tvit['geo']['coordinates'][0].'</br>';
			echo 'geo y : '. $tvit['geo']['coordinates'][1].'</br>';
			echo 'text: '.	$tvit['text'].'</br></br>';
			
		}
		*/
		array_sort_by_column($response['statuses'], 'id');
		//ime, screen_name,avatar url, kordinate, tekst tvita, tvit_id, 
		$rez = array();
		$i=0; $geo = 0; $non_geo=0; $debug ='';
		foreach($response['statuses'] as $tvit)	
		{
			$rez1 = array();
			$rez1['created_at'] 				= $tvit['created_at'];
			$rez1['id_str'] 					= $tvit['id_str'];
			$rez1['text'] 						= $tvit['text'];
			$rez1['user']['id_str']  			= $tvit['user']['id_str'];
			$rez1['user']['name'] 				= $tvit['user']['name'];
			$rez1['user']['screen_name'] 		= $tvit['user']['screen_name'];
			$rez1['user']['profile_image_url']  = $tvit['user']['profile_image_url'];
			$rez1['coordinates'] 				= $tvit['geo']['coordinates'];
			$debug .= $i.' '.$rez1['id_str'].'</br>';
			
			if(!isset($rez1['coordinates']) && isset($tvit['place'])){
                $rez1['coordinates'] = geo_from_place($tvit);               
            }
			
			if(isset($rez1['coordinates']))
			{				
				$rez['geo'][$geo] = $rez1;
				$geo++;
			}
			else
			{
				$rez['non_geo'][$non_geo] = $rez1;
				$non_geo++;
				
			}
			$i++;
		}															//veci br noviji tvit
		$rez['zadnji_id_str'] = $response['statuses'][0]['id_str'];//najveci id//najmladji tw
		$rez['prvi_id_str'] = $response['statuses'][count($response['statuses'])-1]['id_str'];//najmanji
		$rez['count'] = $i;
		$rez['geo_count'] = $geo;
		$rez['non_geo_count'] = $non_geo;
		$rez['search_metadata'] = $response['search_metadata'];
		$rez['debug'] = $debug;
		return $rez;
	}
	function geo_from_place($tvit){
        $coords = $tvit['place']['bounding_box']['coordinates'][0];
        $x = ($coords[0][0] + $coords[2][0])/2;
        $y = ($coords[0][1] + $coords[2][1])/2;
        $xoffset = rand(0,100000)/(1000*1000);
        $yoffset = rand(0,100000)/(1000*1000);
        $x = round($x + $xoffset, 6);
        $y = round($y + $yoffset, 6);
        $coord = [$y, $x];//samo obrnuto ovde
        return $coord;
    }
	//array_sort_by_column($array['statuses'], 'id');
	function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
		$reference_array = array();
		foreach($array as $key => $row) {
			$reference_array[$key] = $row[$column];
		}
		array_multisort($reference_array, $direction, $array);
	}
	function proveri_usera($connection)
	{
		$response = $connection->get('account/verify_credentials');
		if(empty($response)) {
			echo 'los user';
			return null;
		}
		
		$user['id_str'] = $response['id_str'];
		$user['screen_name'] = $response['screen_name'];
		$user['name'] = $response['name'];
		$user['avatar'] = $response['profile_image_url'];
		return $user;
	}
	function make_curl($URL)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_URL, $URL);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}	
/**
        * Parse out url query string into an associative array
        *
        * $qry can be any valid url or just the query string portion.
        * Will return false if no valid querystring found
        *
        * @param $qry String
        * @return Array
        */
        function queryToArray($qry)
        {
				$qry = urldecode($qry);
                $result = array();
                //string must contain at least one = and cannot be in first position
                if(strpos($qry,'=')) {
 
                 if(strpos($qry,'?')!==false) {
                   $q = parse_url($qry);
                   $qry = $q['query'];
                  }
                }else {
                        return false;
                }
 
                foreach (explode('&', $qry) as $couple) {
                        list ($key, $val) = explode('=', $couple);
                        $result[$key] = $val;
                }
 
                return empty($result) ? false : $result;
        }

/*
		^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$ logit,latit
		
		moj konacan za lat, long,km mi
		^(\-?\d+\.?\d*),\s*(\-?\d+\.?\d*),\s*(\d+km|^\d+mi)$
		
	$str = '37.781157,-122.398720,100km';	
	var_dump( validateLatLongKm($str) );	
		$str = '2012-09-01';
	var_dump( validateDatum($str) );
	
	$url = '?max_id=417702453627600895&q=%23onokad&result_type=mixed';
	$queryString = parse_url($url, PHP_URL_QUERY); 
	parse_str($queryString, $params);
	//parse_str($url, $params);
	print_r($params); die();
*/	
		
?>