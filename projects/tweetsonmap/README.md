# Tweets on map  

This project is done as practical part of my master thesis. It was ment to demonstrate making of mashup application that combines multiple API from different sources into one presentation.

# Demo  

At the time of writting this document working demo is available on [http://tweetsonmap.herokuapp.com](http://tweetsonmap.herokuapp.com) .

# Screenshot  

![Screenshot 1](Screenshot_1.png)


# Features 

- search form with supported all imput args from [GET search/tweets](https://dev.twitter.com/rest/reference/get/search/tweets) and previous next results pagination
- Twitter login implemented with OAuth authorization flow from [abraham/twitteroauth](https://github.com/abraham/twitteroauth)
- displaying tweets on map as iframes embeded in infobox component
- auto zooming and positioning on map based on results
- fully responsive Bootstrap 3 design with full screen mode
- sharing results as link
- included table with full search results (including non-geotaged tweets)  

# Code guide

It is mainly JavaScript application, PHP is used just as requisite to make OAuth authentication for [GET search/tweets](https://dev.twitter.com/rest/reference/get/search/tweets) requests. Backend is without framework using native $_SESSION and $_REQUEST globals. All JavaScript functionality is in `glavna.js` file, and requests handler is in `ajax.php` file.    

# Installation on Heroku

This application has no database, you just have to push code to Heroku. In `config.php` replace API keys as well as `tweetsonmap.herokuapp.com` with your domain in following line:
```
define('OAUTH_CALLBACK', 'http://tweetsonmap.herokuapp.com/callback.php');
```

# Used libs

- Bootstrap 3 
- Google Maps API v3
- [Infobox.js](https://github.com/nmccready/google-maps-utility-library-v3-infobox)
- jQuery

# Project status

This project is done in december 2013. and deprecated. Only 5% of tweets are geotaged. It was valuable expirience working with Google Maps v3 API.

# Licence 

Use this only as portfolio item in order to determine my programming expertize level. Do not use or distribute. `nemanja.mitic.elfak@hotmail.com` december 2013. `readme.md` updated summer 2016.