


	//samo ako je zadnji put pritiso next, pamtis mu previous
	
	var result_gl = null;
	var status = '';// isto... post ifovi u ajax.php status
	var results = '';// string za ispis broja rezultata
	var previous_urls = [];// za previous dugme dok ne refresh stranu
	var map = null;
	var markeri = [];
	var klik_na_mapu = null;
	var infobox_gl = make1infobox();
	var minZoomLevel = 2;//2
	var left_col_scroll = null;
	var body_scroll = null;
	var refresh_url_gl = '';
	//43.32517767999296, 21.9232177734375 Nis, 44.762336674810996, 20.4840087890625 beograd
	var centar = new google.maps.LatLng(51.508742,-0.120850);//lokacija objekat
	
	$(document).ready( function() {
		$('.loading1').hide();
		$('#row2').hide();
		$('#row3').hide();
		updateVisina();
		google.maps.event.addDomListener(window, 'load', initialize);
		left_col_scroll = $("#left-column").niceScroll({touchbehavior:false,cursorcolor:"#969696",cursoropacitymax:0.6,cursorwidth:10});
		//body_scroll = $("html").niceScroll({horizrailenabled:false,touchbehavior:false,cursorcolor:"#0000FF",cursoropacitymax:0.6,cursorwidth:10});
		table_scroll = $("#row2").niceScroll({touchbehavior:false,cursorcolor:"#969696",cursoropacitymax:0.6,cursorwidth:10});

		$('#left-column').mouseover(function() {
			left_col_scroll.resize();
		});
		$('#row2').mouseover(function() {
			table_scroll.resize();
		});
		$(window).on('resize', function(){ //trigerujem i na fulscr/regular_scr
			//body_scroll.resize();
			updateVisina();
			google.maps.event.trigger(map, "resize");
			//zoom
			ograniciZoom();
		});
		
		parsujLink();
		//zoom
		ograniciZoom();
	});
/*	
	monitori
	1024x600
	1024x738
	1268x800
	1366x768
	1440x900
	1600x900
	1680x1050
	1920x1080
	1920x1200
*/	

	function ograniciZoom() {
		if($(window).width() > 1300)
			minZoomLevel = 3;//2
		else
			minZoomLevel = 2;
			
		if(map != null  && map.getZoom() < minZoomLevel)//dodatni uslov da ne reset na ful_scr
			map.setZoom(minZoomLevel);
	}
	
	function updateVisina() {
		//return;
		updateThTd();
		updateDonjaMargina();
		//sirina
		updateSirinaMape();
		
		if($(window).width() < 768)//960reset visine lev kol i gmap
		{
			$('#left-column').css({"height": ''});		
			$('#gmap').css({"height": ''}); 
			$('#wrapper1').css({"width": ''});
			$('#wrapper1').css({'margin-left': '', 'margin-right': ''});
			return;
		}	
		//$("#glavni_search").val($(window).width());	
		//return;
		
		//visina
		var visina = $(window).height() - $('#navbar1').height() - 40;		
		//moras direktno gmap da postavis a ne wrapper1
		$('#gmap').css({"height": visina+'px'}); 
		$('#left-column').css({"height": visina+'px'});
		
		
	}
	function updateSirinaMape()
	{
		if($(window).width() < 768)
		return;
		
		if($('#full_scr_a').text() === 'Full screen') {
			//nije moglo fluid column jer bootstrap ima 12 grid
			var sirina_mape = $('#row1').width() - $('#left-column').width() - 60;
			$('#wrapper1').css({"width": sirina_mape+'px'});
			$('#wrapper1').css({'margin-left': '', 'margin-right': ''});
		} else	if($('#full_scr_a').text() === 'Regular screen') {//sad je full
			$('#wrapper1').css({"width": ''});
			$('#wrapper1').css({'margin-left': '15px', 'margin-right': '15px'});
		} 
		if(map === null) return;
			centar = map.getCenter();
			google.maps.event.trigger(map, 'resize');
			map.setCenter(centar); 
	}

	function updateThTd()
	{
		var tds = $("#tbody1").find("tr:first").find('td');
		var ths = $("#th1").find("th");
		
		tds.each(function(index, element) {
			var width1 = $(element).width();
			ths.eq(index).css({"width": width1+'px'});
			//alert(width1);
		});	
	}

	function updateDonjaMargina() {
	
		var tabela1 = ($('#tw_table_a').text() === 'Hide tweets table');
		var full_scr_mali = (($('#full_scr_a').text() === 'Regular screen') && $(window).width() < 768);
		if(tabela1 || full_scr_mali) {
			$('#container1').css({'margin-bottom': '-20px'});
		} else	 {
			$('#container1').css({'margin-bottom': '0px'});
		}

	}
	//treba izbaci nepotrebne x=y=... i encode base64
	/*var encodedData = window.btoa("Hello, world"); // encode a string
	var decodedData = window.atob(encodedData); // decode the string*/

	function parsujLink() 
	{
		if(getParameterByName('glavni_search') === '')
			return;
						
		//var link11 = $(location).attr('href');//to je to, ubaci u polja sa jquery
		//var link11 = location.search.substring(1);//bez ?	
		var link11 = location.hash.substring(1);//bez ?		
		refresh_url_gl = link11;
		
		if($('#login_a').html() !== 'Log out') {
			$('#myModalr').modal();
			//location.href = './redirect.php?'+link11;//on ce na dugme link
			$('#proceed_a').attr("href", './redirect.php?'+link11);
		}
		
		$('#glavni_search').val(getParameterByName('glavni_search'));
		$('#latitude').val(getParameterByName('latitude'));
		$('#longitude').val(getParameterByName('longitude'));
		$('#radius').val(getParameterByName('radius'));
		$('#type_radio').val(getParameterByName('type_radio'));
		$('#lang').val(getParameterByName('lang'));
		$('#count').val(getParameterByName('count'));
		$('#until').val(getParameterByName('until'));
		//glavni_search=miki&all=&exact=&any=&none=&hash=&from=&to=&mention=&latitude=&longitude=&radius=&lang=all&type_radio=mixed&count=99&until=&max_id=428665927068942337
		//127.0.0.1/dizajn/index.php?glavni_search=onokad&all=&exact=&any=&none=&hash=&from=&to=&mention=&latitude=&longitude=&radius=&lang=all&type_radio=mixed&count=99&until
		$.post("ajax.php", link11,
			function(result, status){
			
				result_gl = result;
				izvrti_tvitove(result);								
				//location.search = '';//redirektuje..
				location.hash = '';
				
		}, "json"); 	
	}
	function createShareUrl() { //max_id ubaci... mozda bolje preko #
		
		/*var ae = '127.0.0.1/dizajn/index.php?glavni_search=onokad&all=&exact=&any=&none=&hash=&from=&to=&mention=&latitude=&longitude=&radius=&lang=all&type_radio=mixed&count=99';
		alert(removePrazno(ae));
		return;
		*/if(refresh_url_gl === '')
			return;
		//treba redirekt ako nije logovan
		
		//refresh_url_gl.replace(/#done/g, '');
		$('#share_rez').show();
		var host_path = location.protocol +'//'+ location.hostname + location.pathname;//'http://tweetsonmap.herokuapp.com/#'
		var tw_url = 'https://twitter.com/intent/tweet?';
		tw_url += 'url='+encodeURIComponent(host_path +'#'+ removePrazno('?'+refresh_url_gl));//mora da pocinje sa ?
		tw_url += '&text=I found this result on tweetsonmap.herokuapp.com';
		tw_url += '&hashtags=tweetsonmap';
		$('#share_rez_a').attr("href", tw_url);
		
	}
	function removePrazno(url1) { //mora da pocinje sa ?
		var uri = URI(url1);	
		var arr = uri.search(true);
		
		if(uri.hasQuery("max_id") === false)//nema
			uri.addSearch("max_id", result_gl.moj_metadata.zadnji_id_str);
			
		for (var k in arr){
			if (arr.hasOwnProperty(k)) {
				 //console.log("Key is " + k + ", value is " + arr[k]);
				 if(arr[k] === '')
					uri.removeSearch(k);			 
			}
		}
		return uri.query();//bez ?
	}
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		//var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			//results = regex.exec(location.search);
		var regex = new RegExp("[\\#&]" + name + "=([^&]*)"),
			results = regex.exec(location.hash);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	$(document).ajaxStart(function(){
		//alert('start');
		$('.loading1').show();
		$('.loading2').hide();
		$('#over_map_info').show();
	 }).ajaxStop(function(){
		//alert('stop');
		$('.loading1').hide();
		$('.loading2').show();
		$('button[data-loading-text]').button('reset');
		update_previous_dugme();
		updateThTd();
		createShareUrl();
	 });
	// submit search 
	$(function(){	
		$("#gl_forma").submit(function(e){  		  
		   e.preventDefault(); 
	
			spoji_polja();
			var link1 = $("#gl_forma").serialize();
			previous_urls = [];
			previous_urls.push(link1);
			refresh_url_gl = link1;//bez ? 

			$.post("ajax.php", link1,
				function(result, status){

					result_gl = result;
					izvrti_tvitove(result);					
					
			}, "json"); 
			return false;
		});
	});

	//previous, cuvas kao glob prom
	$(document).ready( function() {//izvrsava se 1 onload	
		
		$('#previous').click(function() {			
			//bzv klik
			if(result_gl.status != 'search successful.')
				return;		
				
			if(previous_urls.length > 0) {//uvek ispunjeno bzv
				//alert(previous_urls.length);
				var last = previous_urls.pop();
				//update_previous_dugme();
				refresh_url_gl = last;//nema ?
				
				$.post("ajax.php", last,
				
					function(result, status){
						result_gl = result;
						//$("#footer").html(JSON.stringify(result));
						izvrti_tvitove(result);

				}, "json");//json povratni
				
			} else alert('No more previous.');
        });
	});
	//next, i guras trenutne za previous u polje// since_id = noviji od. max_id stariji od
	$(document).ready( function() {
		$('#next').click(function() {

			if(result_gl.status != 'search successful.')
				return;
	
			var next = result_gl.moj_metadata.next_url;//i count treba da nakalemis, za previous
			var refresh_url = result_gl.moj_metadata.refresh_url;
			/*var count = result_gl.result.search_metadata.count;
			if(check_count())
				count = check_count();*/

			previous_urls.push(refresh_url);
			//update_previous_dugme();
			refresh_url_gl = next;//nema?

			$.post("ajax.php", next,
			
				function(result, status){
				
					result_gl = result;
					//$("#footer").html(JSON.stringify(result));
					izvrti_tvitove(result);					

			}, "json"); 

		});
	});
	//resetujes sve dugmice u ajax-stop zapazi
	function update_previous_dugme() {
		//ovo treba van dugmeta
		if(previous_urls.length > 1) {
			//alert(previous_urls.length + ' vece od 1');
			$('#previous').text('Previous ('+previous_urls.length+')');
		} else {
			//alert(previous_urls.length + ' nije vece od 1');
			$('#previous').text('Previous');
		}
	}
	function stampaj_previous() {
	return;
		var str = '';
		for (var i = 0; i < previous_urls.length; i++ ) {
			str += previous_urls[i]+'</br>';
		}
		str += result_gl.debug;
		$("#footer").html(str);
	}
	function check_count() {
		
		var count1 = parseInt($('#count').val());
		
		if($('#count').val() != '')
		if(count1 > 0 && count1 < 101)
			return $('#count').val();
		else
			alert('count can be 1-100');
			
		return null;	
	}
//================bootstrap	

	$(document).ready( function() {
		$('#full_scr').click(function() {	
			if($('#full_scr_a').text() === 'Full screen') {//Full
				$('#left-column').hide(); //hide()
				$("#wrapper1").attr('class', 'row');
				$('#row1').removeClass('row1');
				$('#row1').css({'margin-right': '-15px'});
				$('#full_scr_a').text("Regular screen");
				//onemoguci klik koord
			} else
			
			if($('#full_scr_a').text() === 'Regular screen') {
				$('#left-column').show();//show();
				$("#wrapper1").attr('class', 'col-xs-12 col-sm-8 col-md-8 col-lg-9');
				$('#row1').css({'margin-right': ''});
				$('#row1').addClass("row1");				
				$('#full_scr_a').text("Full screen");
			}
			updateVisina();
			//var zoom1 = map.getZoom();			
			$(window).trigger('resize');//to je
			//map.setZoom(zoom1);
			
			//ok reload map
		/*	var center1 = map.getCenter();
			google.maps.event.trigger(map, 'resize');
			map.setCenter(center1); 
		*/	//return false;	
		});
		//show hide tabelu
		$('#tw_table').click(function() {	
			if($('#tw_table_a').text() === 'Show tweets table') {				
				$('#row3').show();
				$('#row2').show();
				$('#tw_table_a').text("Hide tweets table");
			} else
			
			if($('#tw_table_a').text() === 'Hide tweets table') {				
				$('#row3').hide();
				$('#row2').hide();
				$('#tw_table_a').text("Show tweets table");
			}
			updateThTd();
			updateVisina();						
			$(window).trigger('resize');
		});
		
		//mora na document ready
		$('#clear_all').click(function() {//ajax radi non stop, nisam zatvorio zagradu
			$('#gl_forma').trigger("reset");
		});
		//sakri div izn mape
		$('#over_map_info').click(function() {
			$('#over_map_info').hide();
		});
		//loading buttons ajax
		$('button[data-loading-text]').click(function () {
			$(this).button('loading');
		});
		//reset formu na dupli klik bilo gde
		$('#gl_forma').dblclick(function() {
			$('#gl_forma').trigger("reset");
		});
		//share link modal
		$('#share_link_a').click(function() {
			var host_path = location.protocol +'//'+ location.hostname + location.pathname;//'http://tweetsonmap.ml/#'
			host_path += '#'+ removePrazno('?'+refresh_url_gl);
			$('#share_link_input').val(host_path);
		});
	});
	

	

		
//================bootstrap	
	var tr1_gl = null;
	$(document).ready( function() {
		//nece na .click() FORA selektuje ali nece click event
		$(document).on('click', '.success', function (e) {//dblclick
			var tw_id1 = $(this).find('.hide1').text();
			
			var tr1 = $(this);
			tr1.find('td').css({'background-color': 'rgb(160, 230, 130)'});
			if(tr1_gl !== null)
				tr1_gl.find('td').css({'background-color': ''});
			tr1_gl = tr1;
			//alert(tw_id);
		for(index in markeri){
			if( tw_id1 == markeri[index].tvit_id) {
				//kliknuoMarker(markeri[index]);
				google.maps.event.trigger(markeri[index], 'click');
				break;
			}
		}						
		});
	});
	function napraviTabelu(jsonobj) {
		$("#tbody1").html('');
				
		//var tabela = '<table class="table table-bordered table-condensed">';
		//tabela += '<tr><th>Avatar</th><th>Screen name</th><th>Date</th><th>Text</th></tr>';
		var tabela = '';
		if(jsonobj.result.geo !== undefined)
		for(var i=0; i< jsonobj.result.geo.length; i++) {
			tabela += '<tr class="success"><td><span class="hide1">'+jsonobj.result.geo[i].id_str+'</span><img width="29px" height="29px" src="'+jsonobj.result.geo[i].user.profile_image_url+'"/></td><td>@'+jsonobj.result.geo[i].user.screen_name+'</td><td nowrap="nowrap">'+moment(jsonobj.result.geo[i].created_at).format("DD. MMM. YYYY.")+'</td><td>'+jsonobj.result.geo[i].text+'</td></tr>';		
		}
		if(jsonobj.result.non_geo !== undefined)
		for(var i=0; i< jsonobj.result.non_geo.length; i++) {
			tabela += '<tr class="non-geo"><td><img width="29px" height="29px" src="'+jsonobj.result.non_geo[i].user.profile_image_url+'"/></td><td>@'+jsonobj.result.non_geo[i].user.screen_name+'</td><td nowrap="nowrap">'+moment(jsonobj.result.non_geo[i].created_at).format("DD. MMM. YYYY.")+'</td><td>'+jsonobj.result.non_geo[i].text+'</td></tr>';				
		}
		//tabela += '</table>';
		$("#tbody1").html(tabela);
	}
	function izvrti_tvitove(jsonobj)  {
	
		//ako search/tweets api ne uspe
		if('errors' in jsonobj)	{				

			var length = jsonobj.errors.length;
			var str1 = '';
			for (var i = 0; i < length; i++) {
				str1 += 'error mesage ' + i + ' is: '+ jsonobj.errors[i].message;
			}
			$("#statusId").html(str1);
			return;
		} 
		
		status = jsonobj.status;
		if(jsonobj.status == 'search successful.')	{				
			results = 'Found '+ jsonobj.result.geo_count+' geocoded and '+ jsonobj.result.non_geo_count +' nongeocoded results.';							
		} else {		
			$("#statusId").html(status);
			return;
		}
		$("#statusId").html(status);
		$("#resultsId").html(results);
		
		obrisiSveMarkere();
		napraviTabelu(jsonobj);
		
		if(jsonobj.result.geo === undefined)
			return;

		//cOOrdinates ima 2 oo jbt
		for(var i=0; i< jsonobj.result.geo.length; i++) {
			
			var tvit = napraviTvitHtml(jsonobj, i);
			var x1 = jsonobj.result.geo[i].coordinates[0];
			var y1 = jsonobj.result.geo[i].coordinates[1];
			var lokacija=new google.maps.LatLng(x1,y1);
			markeri[i]=new google.maps.Marker({
				position:lokacija,
				map: map,
				tvit_id: jsonobj.result.geo[i].id_str,
				//infoBoxObj: infoboxArr[i],//infowind cuvas u markeru
				infob_is_otv: 0,
				r_br: i,
				html: tvit,
			});
			
			google.maps.event.addListener(markeri[i], 'click', function(event) {
				kliknuoMarker(this);//
			
			});
	
		}
		//podesi granice da stanu svi markeri, van petlje
		new_boundary = new google.maps.LatLngBounds();
		for(index in markeri){
		  position = markeri[index].position;
		  new_boundary.extend(position);
		}
		if(map != null) {
			map.fitBounds(new_boundary);			
			centar = map.getCenter();
			//i ogranici zoom ako 1 marker
			//0 - 19; 0 lowest zoom (whole world)
			if(map.getZoom() > 11)
				map.setZoom(11);
		}
	
	}
	function napraviTvitHtml(jsonobj, i)	{
/*
' becomes \x27
" becomes \x22
&quot;
<div id="cont1" style="width:200px;" >
min-height:220px
*/		
		var text = jsonobj.result.geo[i].text;
		var id_str = jsonobj.result.geo[i].id_str;
		var datum = jsonobj.result.geo[i].created_at;
		datum = moment(datum).format("MMM DD, YYYY");//July 24, 2013 need this
		var name = jsonobj.result.geo[i].user.name;
		var screen_name = jsonobj.result.geo[i].user.screen_name;
		var link = 'https://twitter.com/'+screen_name+'/statuses/'+ id_str;
		
		var str2 = '<div class="twcont" style="width:auto; " ><blockquote class="twitter-tweet" lang="en"><p>'+text+'</p>&mdash; '+name+' (@'+screen_name+') ';
		str2 += '<a href="'+link+'">'+datum+'</a></blockquote></div>';
		//str2 += '<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></' + 'script>';

		return str2;
	}
	
	function clear_polja()  {	
		$('#all').val('');
		$('#exact').val('');
		$('#any').val('');
		$('#none').val('');
		$('#hash').val('');
		$('#from').val('');
		$('#to').val('');
		$('#mention').val('');
		//$("#div_id :input");//nodes jqurey-wrapped collection
/*		you can use jQuery.each to iterate over the collection
		or you can manually do it with a for-loop
		it's not an array, but it is an array-like object
		you can reference it with numeric indexes, and it provides a .length property like an array would
*/	
	}
	
	function spoji_polja()  {
	
		var sve = $('#all').val();
		sve += $('#exact').val();
		sve += $('#any').val();
		sve += $('#none').val();
		sve += $('#hash').val();
		sve += $('#from').val();
		sve += $('#to').val();
		sve += $('#mention').val();
		
		
		/*
		var str1 = str.replace(new RegExp('\\s+', 'g'), ' OR ');
		// alternately, a regexp literal could be used
		var str2 = str.replace(/\s+/g, ' OR ');*/
		
		var str = '';
		if($('#all').val() != '')
			str += $('#all').val();
		if($('#exact').val() != '')
			str += ' "'+$('#exact').val()+'" ';
		if($('#any').val() != '') {
			str += $('#any').val().replace(/\s+/g, ' OR ');			
		}
		if($('#none').val() != '')
			str += ' -'+$('#none').val();
		if($('#hash').val() != '')
			str += ' #'+$('#hash').val();
		if($('#from').val() != '')
			str += ' from:'+$('#from').val();
		if($('#to').val() != '')
			str += ' to:'+$('#to').val();
		if($('#mention').val() != '')
			str += ' @'+$('#mention').val();
		
		if(sve != '') {
			//ne treba clear, ovo postavlja
			$('#glavni_search').val(str);
			clear_polja();
		}
	
	}

//====================== MAPE =======================

	function make1infobox() {	
		//setting up the infobox
		var infobox1 = new InfoBox({
			//content: boxText,
			disableAutoPan: true,
			boxClass: "infoBox",
			boxStyle: {  width: "auto" },
			isHidden:false,
			pixelOffset: new google.maps.Size(-20, 5),//-15
			closeBoxMargin: "-10px -20px -20px 2px",
			closeBoxURL: "./images/close50.png",
			pane: "floatPane",
			enableEventPropagation: true,
			infoBoxClearance: new google.maps.Size(1, 1),
			});
			
		google.maps.event.addListener(infobox1, 'domready', function () {
			! function (d, s) {
				var js, fjs = d.getElementsByTagName(s)[0];
				js = d.createElement(s);
				js.src = "//platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, "script");//zove sam sebe sa param
		});
		return infobox1;
	}
	function kliknuoMarker(marker) {

		//za togle infowind
		if (isInfoBoxOpen(infobox_gl) && marker.infob_is_otv != 0){
			// do something if it is open
			//marker.infoBoxObj.close();
			marker.infob_is_otv = 0;
			infobox_gl.close();
		} else {
			// do something if it is closed
			reset_is_otv();
			marker.infob_is_otv = 1;
			map.panTo(marker.getPosition());
			infobox_gl.setContent(marker.html);
			infobox_gl.setPosition(marker.getPosition());
			infobox_gl.open(map,marker);
			
			centar = marker.getPosition();
			map.setCenter(centar);
		}

	}
	function reset_is_otv() {
		for (var i = 0; i < markeri.length; i++ ) {
			markeri[i].infob_is_otv = 0;
		}	
	}
	function isInfoBoxOpen(ib){
		var mapa = ib.getMap();
		//console.log(typeof ib.getMap);
		if (mapa == null){							
			return false;
		} else {
			return true;
		}
	}

	var toggle_koord = 2;
	function mouseMoveMapu(event) {
		if(toggle_koord === 0) {
			$("#latitude").val(event.latLng.lat());
			$("#longitude").val(event.latLng.lng());
			$("#radius").val('100km');
		}
	}
	function kliknuoMapu(event) {
		if($('#full_scr_a').text() === 'Regular screen')
			return;
			
		if(toggle_koord === 2){//radi-zamrzni
			//alert('2');
			toggle_koord = 0;
		} else
		if(toggle_koord === 1){//obrisi->radi
			//alert('1');
			toggle_koord = 2;
			$("#latitude").val('');
			$("#longitude").val('');
			$("#radius").val('');
		} else
		if(toggle_koord === 0) {//zamrzni->obrisi
			toggle_koord = 1;
			//alert('0');
			$("#latitude").val(event.latLng.lat());
			$("#longitude").val(event.latLng.lng());
			$("#radius").val('100km');
			
		} 

	}
	
	function obrisiSveMarkere() {
	  for (var i = 0; i < markeri.length; i++ ) {
		markeri[i].setMap(null);
	  }
	  markeri.length = 0;
	}
	
	function initialize()
	{
		var mapProp = {
		  center: centar,
		  zoom: minZoomLevel,//5
		  mapTypeId:google.maps.MapTypeId.ROADMAP
		  };

		map = new google.maps.Map(document.getElementById("gmap"),mapProp);

		google.maps.event.addListener(map, 'mousemove', function(event) {//mousemove
			mouseMoveMapu(event);
		});
		google.maps.event.addListener(map, 'click', function(event) {//mousemove
			kliknuoMapu(event);
		});
		// Limit the zoom level
		google.maps.event.addListener(map, 'zoom_changed', function() {
			if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
		});

	}
	
	//google.maps.event.addDomListener(window, 'load', initialize);
/*
    $(function () {
        $("#map_canvas").css("height", $(window).height());
    });
	
	@import url('http://getbootstrap.com/dist/css/bootstrap.css');
	$(window).on('resize', function(){$('div').height()});
*/

/*
<a class="btn" href="">Link</a>
<button class="btn" type="submit">Button</button>
<input class="btn" type="button" value="Input">
<input class="btn" type="submit" value="Submit">

*/







