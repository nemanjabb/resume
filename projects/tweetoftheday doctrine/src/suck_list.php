<?php

require_once(__DIR__ . '/bot.php');

/*
  sredi usisavanje liste bootstrap html interfejs
  sredi mapiranje doctrine
  kreiraj srpsku listu
  namesti rt bota da radi
  moze da stampa api limit info, ili posebna stranica
  sync listu i db

  87Zt23e5v37SEnKMyA0cqdhcd
  4TbCD7JlspZzvGTUFnnMOZLPUYh8X2d5d7bMX1qm6N3Kfcv0Y2
  2880717957-UQQ8cUL5l9bYc7fyu1xOiCfxLNEWx6Vzwf3wm1L
  8XD0cdZBMdr4MukkY1AMa4Rt3kaTdWAGcK7BF3ES2zDsV

  prvitest
  123456;
  user_id 2880717957
  list_id 204452518
  ratko2012@yahoo.com

  treba suck_list i kontroler objektno da se srede
  user[] ima avatar, a prilagodjen je entity user i listUsertb
  html interfejs selektovanje usera na tabeli za brzo dodavanje i izbac iz liste
  input iz formi da se validira
  limit modul da se ubaci za api
  izuzeci u twapi da se hendluju i emituju, i u db repository
  modul koji odlucuje koga dodaje a koga izbacuje iz liste, i da ne predje limit da rasporedi akciju u vremenu
  u bazu mozes odah da dodas, pa shedle u listu
  testkejsovi za orphane i cascade
 * soft delete sync sa listom, ogranici velicinu baze
 * svaki put ubacis u bazu sa flegom 5=temp, koji se brise na svaki zahtev, a da ubacis samo promenis 5 u 1
 * 1=aktivan
 * 5=cache//ne valja
 * 
 * NAPRAVI I SELEKCIJU TOP 10 USERS na stranici, kao top 10 tweets, ce da se loze
 */

class suck_list extends bot {

    //$tw, $db doctrine repository, inherited
    //db em i list tabela
    public function __construct($em, $list_id) {
        parent::__construct($em, $list_id);
    }

    //sync db to list
    //ubaci u tw listu nove iz baze sa in_list=1, izbaci iz tw liste sa in_list=0
    //izbaci iz db suspendovane usere sa tw, update screen_name, followers, statuses prema user_id
    //novi fajl validate forms, svaka forma klasa i validate() i hendlovanje gresaka, status ispis na html
    //validate activate users
    //return users
    public function activate_users($activate_form) {
        $userArr = $this->db->select_listUsers($activate_form);

        if ($activate_form->btn == 'Show')        //treba da vratis usere a ne veznu tabelu           
            return usersDb2users($userArr);

        if ($activate_form->btn == 'Update') {
            $r = $this->db->update_listuser($activate_form);
            $userArr['rows'] = $r;
            return usersDb2users($userArr);
        }
        $em1 = $this->db->em;
        if ($activate_form->btn == 'Delete') {

            array_map(function($listUser) use ($em1) {
                $user = $listUser->getUsers();
                $em1->remove($user);
            }, $listUser_arr);
            $em1->flush();
        }

        return array_map(function($listUser) {
            return $listUser->getUsers();
        }, $listUser_arr);
    }

    public function usersDb2users($userArr) {
        return array_map(function($user) {    
            return $this->userDb2user($user);
        }, $userArr);
    }

    public function userDb2user($userDb) {

        $user['profile_image_url'] = 'empty';
        $user['id_str'] = $userDb->getIdstr();
        $user['followers_count'] = $userDb->getFollowersCount();
        $user['friends_count'] = $userDb->getFriendsCount();
        $user['statuses_count'] = $userDb->getStatusesCount();
        $user['screen_name'] = $userDb->getScreenName();
        $user['name'] = $userDb->getName();
        return $user;
    }

    //bot i ovo odvojene klase i skripta suck_list.php
    public function dl_list_to_db($list_arr, $kriter_arr, $count) {
        $users = $this->formated_list_users($list_arr['slug'], $list_arr['owner_screen_name'], $count);
        // $users1 = $users;
        // unset($users['profile_image_url']);//ne mora ne smeta mu visak u konstruktor
        $users50 = self::filter_users($users, $kriter_arr);
        $this->db->insert_or_update_users($users50);
        return $users50; //for print
    }

    //samo za listanje bez upisa
    public function formated_list_users($slug, $owner_screen_name, $count) {
        $response = $this->tw->get_list_members($slug, $owner_screen_name, $count); //lists/members
        return $users = self::users_from_list_members($response);
    }

    public static function filter_users($users, $cond) {
        //array_sort_by_column($users, 'followers_count', SORT_DESC);
        //minimum
        if (!is_array($cond) || $cond['followers_count'] < 500 || $cond['statuses_count'] < 500)
            $cond = ['followers_count' => 500, 'statuses_count' => 500]; //default

        $users1000 = [];
        foreach ($users as $user) {
            $cond1 = $user['followers_count'] > $cond['followers_count'];
            $cond1 = $cond1 && $user['statuses_count'] > $cond['statuses_count'];
            if ($cond1)
                $users1000[] = $user;
        }
        return $users1000;
    }

    public static function user_from_tweet($tweet_user) {
        $user = [];
        $tweet = $tweet_user;
        //$keys = array('id_str', 'followers_count', 'friends_count', 'statuses_count', 'screen_name', 'name');
        //$user  = array_intersect_key($tweet, array_fill_keys($keys, $tweet[$keys]));
        $user['profile_image_url'] = $tweet['profile_image_url']; //visak za entity
        $user['id_str'] = $tweet['id_str'];
        $user['followers_count'] = $tweet['followers_count'];
        $user['friends_count'] = $tweet['friends_count'];
        $user['statuses_count'] = $tweet['statuses_count'];
        $user['screen_name'] = $tweet['screen_name'];
        $user['name'] = $tweet['name'];
        return $user;
    }

    public static function users_from_list_members($response) {
        $users = [];
        foreach ($response['users'] as $user1) {
            $user = self::user_from_tweet($user1);
            $user = self::default_user($user);
            $users[] = $user;
        }
        return $users;
    }

    public static function default_user($user) {
        $user['priority'] = 0;
        $user['date_added'] = time();
        $user['date_removed'] = 0;
        $user['date_last_rt'] = time();
        $user['in_list'] = 0;
        return $user;
    }

}
