<?php

require_once(__DIR__ . '../../lib/format.php');

date_default_timezone_set("Europe/Belgrade");



$html = 'BIG STRING FULL OF HTML';
$format = new Format;
$formatted_html = $format->HTML($html);

function mojlog($func, $str) {
    global $format;
    if ($func == "s")
        $file = __DIR__ . "/files/search.html";
    if ($func == "l")
        $file = __DIR__ . "/files/list.html";

    if (!file_exists($file))
        file_put_contents($file, ' ', FILE_APPEND);


    //$datum = date('d.m.Y H:i:s');
    $formatted_html = $format->HTML($str);
    file_put_contents($file, $formatted_html, FILE_APPEND);
    flush();
}

function reformat() {
    global $format;
    $file1 = __DIR__ . "/files/list.html";
    $str1 = file_get_contents($file1);
    $formatted_html = $format->HTML($str1);
    file_put_contents($file1, $formatted_html);
}

//reformat();
?>