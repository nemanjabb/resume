<?php

class activate_form {

    public $screenName;
    public $inList;
    public $followersCount;
    public $statusesCount;
    public $count;

    public function __construct() {
        
    }

    //string fail, object success
    public function validate($activate_post) {

        if (preg_match('/Update|Delete|Show/', $activate_post['btn']))
            $this->btn = $activate_post['btn'];
        else
            return "invalid btn";


        $user_valid = preg_match('/^(\w{1,15})$/', $activate_post['screen_name']);
        $in_list_valid = preg_match('/^(\d{1,6})$/', $activate_post['in_list']);
        $followers_valid = preg_match('/^(\d{1,7})$/', $activate_post['followers']);
        $statuses_valid = preg_match('/^(\d{1,6})$/', $activate_post['statuses']);
        $count_valid = preg_match('/^(\d{1,6})$/', $activate_post['count']);
/*
   if (form_data.btn == 'Show') {
        
        if (user_valid && (followers_valid || statuses_valid || count_valid))
            return "Set just user.";
        if (   !(count_valid && (user_valid || followers_valid || statuses_valid || in_list_valid))  )
            return "Set count and one filter criteria."
    }
    if (form_data.btn == 'Update') {
        if (!  (in_list_valid && count_valid && (followers_valid || statuses_valid ))  )
            return "Count and list required and one filter.";
    }
    if (form_data.btn == 'Delete') {
        if (user_valid && (followers_valid || statuses_valid || count_valid))
            return "Set just user.";
        if (!  (in_list_valid && count_valid && (followers_valid || statuses_valid ))  )
            return "Count and list required and one filter.";
    }
*/

        $this->screenName = self::emptyToNull(trim($activate_post['screen_name'], "@"));
        $this->inList = self::emptyToNull($activate_post['in_list']);
        $this->followersCount = self::emptyToNull($activate_post['followers']);
        $this->statusesCount = self::emptyToNull($activate_post['statuses']);
        $this->count = self::emptyToNull($activate_post['count']);
        $this->above = $activate_post['above'] == "true";

        return $this;
    }

    public static function emptyToNull($string) {
        if (trim($string) === '')
            $string = null;
        return $string;
    }

}

/*
 *         $keys = array('screen_name', 'in_list', 'followers', 'statuses', 'count', 'above', 'btn' );

        if(count(array_intersect_key(array_flip($keys), $activate_post)) === count($keys)) {
            return "not all keys are set. ".  print_r($activate_post,1);            
        }
 */