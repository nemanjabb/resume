<?php
require_once(__DIR__ . '/bot.php');

class rt_best extends bot {

    public function __construct($em, $list_id) {
        parent::__construct($em, $list_id);
    }
    //dodaj staticke fje iz utils
    //idi niz tajmlajn od 2h starog pa do dna (iz baze)//hendluj i ako 2h isto kao iz proslog poziva
    public function go_back() {

        $ids = $this->tw->get_2_3_hours_tweet_ids();
        $max_id = $ids[2]["id_str"]; //gornji //max_id//2sata
        $since_id = $this->db->get_top_id("last_id"); //donji iz baze//since_id//ne menja se

        $same_hour = false;
        $temp = $this->db->get_top_id("list_temp_max_id");

        //init
        if ($this->db->get_top_id("bigben_2h") < $max_id) {
            $this->db->set_top_id("bigben_2h", $max_id);
        }
        if ($this->db->get_top_id("prev_last_id") == '0') {
            $this->db->set_top_id("prev_last_id", $ids[4]["id_str"]); //pre 4 h	
        }

        $bigben_2h = $this->db->get_top_id("bigben_2h");
        //jos smo u istom satu idi na dole, ako ne prvi gore pa na dole
        if ($max_id == $bigben_2h && $temp != '0') {
            $max_id = $temp;
            $since_id = $this->db->get_top_id("prev_last_id");
            $same_hour = true;
        }

        if ($since_id >= $max_id) {
            $x1 = '<thead><tr class="danger"><th colspan="7" >Stigo do kraja, since_id = ' . $since_id . ', max_id = ' . $max_id . '</th></tr></thead>';
            mojlog("l", $x1);
            return;
        }
        //radis samo sa max_id nadole
        for ($i = 0; $i < 3; $i++) {  //180 za 15 min	//upisi pozive u bazu i sracunaj da se izvr maksimalno
            $str = '';
            if ($i > 0) {
                $str = '</tbody>';
                $alert = "info";
            } else {
                $alert = "success";
            }
            $x = $str . '<thead><tr class="' . $alert . '"><th colspan="2" >Iteracija: ' . $i . '</th>';
            $x .= '<th colspan="1" >max_id: ' . $max_id . '</th>';
            $x .= '<th colspan="4" >since_id: ' . $since_id . '</th></tr></thead><tbody>';
            mojlog("l", $x);
            //since_id  max_id //podrzavam oba, odavde krenes da gledas od max_id
            //since_id moras da podrzavas zbog poredjenja, ne zbog poziva
            $response = $this->tw->get_list_tweets($since_id, $max_id); //since_id ne treba
            if (!empty($response['errors'])) {
                mojlog('l', '<tr colspan="7" class="danger">' . print_r($response, 1) . '</tr>');
                return;
            }
            if (count($response) < 1) {
                mojlog('l', '<tr colspan="7" class="danger"> Nula rezultata ' . print_r($response, 1) . '</tr>');
                return;
            }
            /*last_id - last top cursor
             * prev_last_id -previous last top cursor
             * list_temp_max_id - temp cursor u petlji za vise poziva api
             * bigben_2h - id tvita starog 2h
             * 
             * 
             */
            $max_id1 = $response[0]["id_str"]; //gornji
            $since_id1 = end($response)["id_str"]; //donji
            //ima jos
            if ($since_id1 > $since_id) {
                $max_id = $since_id1;           //new top=bottom
                $this->check_fav_rt($response);
                if (!$same_hour) {
                    $this->db->set_top_id("prev_last_id", $since_id);
                    $this->db->set_top_id("last_id", $max_id1);
                }
                $this->db->set_top_id("list_temp_max_id", $since_id1);
                continue;
            } else {
                //doso do kraja					
                if ($max_id1 > $since_id) { //ostatak preklopljeni//ne moze da se izvrsi zbog since_id
                    $response1 = array();
                    foreach ($response as $tweet) {
                        if ($tweet["id_str"] > $since_id)
                            array_push($response1, $tweet);
                    }
                    $x = '<tr class="danger"><th colspan="3"> preklopljeno max_id1: ' . $max_id1 . '</th>';
                    $x .= '<th colspan="4">preklopljeno since_id: ' . $since_id . '</th></tr>';
                    mojlog("l", $x);
                    $this->check_fav_rt($response1);
                }
                return;
            }
            //nije doso do kraja
        }
        mojlog("l", $str);
    }
/*
 * poenta primecena
 * razdvojiti odgovornosti, odvojene samostalne mikro aplikacije
 * koristiti evente i exceptione, templejte, objekte
 * imenovati i napraviti helper fje da bude jasno na 1 pogled
 * sustina je problem koji se resava
 * 
 */
    private function check_fav_rt($response) {
        foreach ($response as $tweet) {
            //$tweet["retweet_count"] 
            if ($tweet["favorite_count"] > 0) {
                //retweetStatus($tweet["id_str"]);
                //ako ga retvitujes dodaj korisnika u listu, proveri da li je u listi
                rt_best::log_tweet('l', $tweet, "Retweeted");
            }
        }
    }  
}
