<?php

//require_once(__DIR__ .'/db/sqlite.php');
require_once( __DIR__ . "/../bootstrap.php" );

interface irepository {

    public function create_timeline();

    public function get_top_id($key);

    public function set_top_id($key, $val);

    public function reset_timeline_tb();

    public function insert_or_update_users($users);

    public function remove_user($user_id);

    public function delete_user($user_id);

    public function insert_list($list_assoc, $em);

    public function get_list($ownerScreenName, $slug);

    public function delete_list($list_id, $em);

    public function insert_tweet($tweet_assoc);
}

class doctrine_repository implements irepository {

    public $list;
    public $em;

    public function __construct($em, $list_id) {
        try {
            $this->em = $em;
            $this->list = $em->find('ListTb', $list_id); //lista mora vec da postoji
        } catch (DoctrineException $e) {
            throw new Exception("Greska u doctrine_repository::__construct. " . $e->getMessage());
        }
    }

    public function create_timeline() {

        $exists = $this->get_top_id("last_id");
        if ($exists)
            return;

        $timeline_arr = array(
            array("key1" => "last_id", "val" => "2", 'list' => $this->list),
            array("key1" => "prev_last_id", "val" => "0", 'list' => $this->list),
            array("key1" => "search_id", "val" => "0", 'list' => $this->list),
            array("key1" => "bigben_2h", "val" => "0", 'list' => $this->list),
            array("key1" => "list_temp_max_id", "val" => "0", 'list' => $this->list)
        );
        foreach ($timeline_arr as $item) {
            $timeline = new TimelineTb($item);
            $this->em->persist($timeline);
        }
        $this->em->flush();
    }

    public function get_top_id($key) {
        return $this->em->getRepository('TimelineTb')->findBy(array('key1' => $key, 'list' => $this->list));
    }

    public function set_top_id($key, $val) {
        $timeline = $this->em->getRepository('TimelineTb')->findBy(array('key1' => $key, 'list' => $this->list));
        $timeline->setVal($val);
        $this->em->persist($timeline);
        $this->em->flush($timeline);
    }

    public function reset_timeline_tb() {
        $this->set_top_id('last_id', '2');
        $this->set_top_id('prev_last_id', '0');
        $this->set_top_id('search_id', '0');
        $this->set_top_id('bigben_2h', '0');
        $this->set_top_id('list_temp_max_id', '0');
    }

    public function insert_or_update_users($users_assoc) {//in this list
        foreach ($users_assoc as $user_assoc) {

            $user1 = $this->em->find('UsersTb', array("idStr" => $user_assoc['id_str'])); //null za neuspeh           
            //user postoji update
            if ($user1) {
                //update
                $user1->setScreenName($user_assoc['screen_name']);
                $user1->setName($user_assoc['name']);
                $user1->setFollowersCount($user_assoc['followers_count']);
                $user1->setFriendsCount($user_assoc['friends_count']);
                $user1->setStatusesCount($user_assoc['statuses_count']);

                $id = $this->list->getListId();
                $bool1 = false;
                $listUserArr = $user1->getListUserTbs()->toArray(); //ima
                array_map(function($listUser) use ($id, &$bool1) {//po referenci &
                    if ($listUser->getList()->getListId() == $id) {
                        $bool1 = true;
                        return true;
                    }
                    return false;
                }, $listUserArr); //listUser ne sadrzi listTb
                //user postoji u drugoj listi, u ovoj ne
                if (!$bool1) {

                    $listUser1 = new ListUserTb();
                    $listUser1->setUser($user1);
                    $listUser1->setList($this->list);

                    $this->em->persist($user1);
                    $this->em->persist($this->list);
                    $this->em->persist($listUser1);
                } else {
                    $this->em->persist($user1);
                }
            } else {

                $user = new UsersTb($user_assoc); //item assoc arr
                $listUser = new ListUserTb();
                $listUser->setUser($user);
                $listUser->setList($this->list);

                $this->em->persist($user);
                $this->em->persist($this->list);
                $this->em->persist($listUser);
            }
        }
        $this->em->flush();
    }

    //update in_list column
    public function update_listuser($activate_form) {
        $ge;
        $activate_form->above ? $ge = '>' : $ge = '<';
        $qb = $this->em->createQueryBuilder();
        $q = $qb->update('ListUserTb', 'lu')
                ->set('lu.inList', ':inList')
                ->setParameter(':inList', $activate_form->inList)
                ->where('lu.list = :listId')
                ->setParameter(':listId', $this->list->getListId());

        if ($activate_form->screenName) {
            $qb->andWhere('lu.user = :screenName')
                    ->setParameter(':screenName', $activate_form->screenName);
            $q = $qb->getQuery();
            $p = $q->execute();
            return;
        }
        if ($activate_form->followersCount) {
            $qb->andWhere('lu.user.followersCount ' . $ge . ' :followersCount')
                    ->setParameter(':followersCount', $activate_form->followersCount);
        }
        if ($activate_form->statusesCount) {
            $qb->andWhere('lu.user.statusesCount ' . $ge . ' :statusesCount')
                    ->setParameter(':statusesCount', $activate_form->statusesCount);
        }
        if ($activate_form->count) {
            $qb->setMaxResults($activate_form->count);
        }

        $q = $qb->getQuery();
        return $rows = $q->execute();
    }

    public function select_listUsers($activate_form) {
        $ge;
        $activate_form->above ? $ge = '>' : $ge = '<';
        $qb = $this->em->createQueryBuilder();
        $q = $qb->select('lu')
                ->from('ListUserTb', 'lu')
                ->where('lu.list = :listId')
                ->setParameter(':listId', $this->list->getListId());

        if ($activate_form->screenName) {
            $qb->andWhere('lu.user = :screenName')
                    ->setParameter(':screenName', $activate_form->screenName);
            $q = $qb->getQuery();
            return $p = $q->getArrayResult();
        }
        if ($activate_form->inList || $activate_form->inList == 0) {
            $qb->andWhere('lu.inList = :inList')
                    ->setParameter(':inList', $activate_form->inList);
        }
        if ($activate_form->followersCount) {
            $qb->andWhere('lu.user.followersCount ' . $ge . ' :followersCount')
                    ->setParameter(':followersCount', $activate_form->followersCount);
        }
        if ($activate_form->statusesCount) {
            $qb->andWhere('lu.user.statusesCount ' . $ge . ' :statusesCount')
                    ->setParameter(':statusesCount', $activate_form->statusesCount);
        }
        if ($activate_form->count) {
            $qb->setMaxResults($activate_form->count);
        }

        $q = $qb->getQuery();
        $listUserArr = $q->getArrayResult();//nije entity
        
        //userList to user
        return array_map(function($listUser) {
            return $this->em->find('UsersTb', $listUser['id_str']);
        }, $listUserArr);
    }

    //soft delete
    public function remove_user($user_id) {
        $listUser = $this->em->find('ListUserTb', array("user" => $user_id, "list" => $this->list));
        $listUser->setInList(2); //izbacen
        $this->em->persist($listUser);
        $this->em->flush();
    }

    public function delete_user($user_id) {
        $user = $this->em->find('UsersTb', $user_id);
        $this->em->remove($user);
        $this->em->flush();
    }

    public function insert_list($list_assoc, $em) {
        try {
            $list = new ListTb($list_assoc); //item assoc arr
            $em->persist($list);
            $em->flush();
            return $list;
        } catch (DoctrineException $e) {
            throw new Exception("Greska u insert_list. " . $e->getMessage());
        }
    }

    public static function get_list_static($ownerScreenName, $slug, $em) {
        //try {
        $list = $em->getRepository('ListTb')->findBy(array("ownerScreenName" => $ownerScreenName, "slug" => $slug));
        return $list[0];
        /* } catch (Exception $e) {
          return null; //nije greska, nema liste
          } */
    }

    public function get_list($ownerScreenName, $slug) {
        try {
            $list = $this->em->getRepository('ListTb')->findBy(array("ownerScreenName" => $ownerScreenName, "slug" => $slug));
            return $list[0];
        } catch (Exception $e) {
            return null; //nije greska, nema liste          
        }
    }

    public function delete_list($list_id, $em) {
        $list = $em->find('ListTb', $list_id);
        $em->remove($list);
        $em->flush();
    }

    public function insert_tweet($tweet_assoc) {
        $tweet = new TweetsTb($tweet_assoc);
        $tweet->setList($this->list);
        $this->em->persist($tweet);
        $this->em->flush();
    }

}
