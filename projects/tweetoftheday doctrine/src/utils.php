<?php

function is_2_hours_old($created_at, $hours = "2") {
    $debug = false;

    $now = new DateTime(date("H:i:s d-m-Y"));
    $bg_time = new DateTimeZone("Europe/Belgrade");
    $now->setTimezone($bg_time);
    $now_timestamp = $now->getTimestamp();

    $h2_old = strtotime('-' . $hours . ' hours', $now_timestamp);

    $h2_old = date('H:i:s d-m-Y', $h2_old);
    if ($debug) {
        $t1 = (new DateTime($created_at))->setTimezone($bg_time);
        $t2 = (new DateTime($h2_old))->setTimezone($bg_time);
        echo "created_at: " . $t1->format('H:i:s d-m-Y') . "<br/>";
        echo "old:        " . $t2->format('H:i:s d-m-Y') . "<br/>";
    }
    if (strtotime($created_at) < strtotime($h2_old))
        return true;
    else
        return false;
}

function to_belgrade($created_at) {
    $t1 = new DateTime($created_at);
    $bg_time = new DateTimeZone("Europe/Belgrade");
    $t1->setTimezone($bg_time);
    return $t1->format('H:i:s d-m-Y');
}

function get_tweet_url($tweet) {
    return "https://twitter.com/" . $tweet["user"]["screen_name"] . "/status/" . $tweet["id_str"];
}

function has_mention($tweet) {
    if (count($tweet['entities']['user_mentions']) < 1)
        return false;

    foreach ($tweet['entities']['user_mentions'] as $screen_name) {
        if (strtolower($screen_name['screen_name']) != 'last_2_hours_rs')//has mention
            return true;
    }
    return false;
}

//array_sort_by_column($array['statuses'], 'id');
function array_sort_by_column(&$array, $column, $direction = SORT_ASC) { //SORT_ASC
    $reference_array = array();
    foreach ($array as $key => $row) {
        $reference_array[$key] = $row[$column];
    }
    array_multisort($reference_array, $direction, $array);
}

//unset prazno iz array
function clean_array($array, $isRepeat = false) {
    foreach ($array as $key => $value) {
        if ($value === null || $value === '') {
            unset($array[$key]);
        } else if (is_array($value)) {
            if (empty($value)) {
                unset($array[$key]);
            } else
                $array[$key] = clean_array($value);
        }
    }
    if (!$isRepeat) {
        $array = clean_array($array, true);
    }
    return $array;
}

function debug_results($response) {
    foreach ($response as $tweet) {
        log_tweet($tweet);
    }
}

//returns matched sta je reko
function match_phrase($phrase, $input) { //trazi rec phrase
    if (preg_match("/" . $phrase . "/", strtolower($input), $matches, PREG_OFFSET_CAPTURE) == 1) {
        return $matches[0]; //mora arr, ne str	
    }
    return false;
}

function delete_file($fajl, $reset) {
    if (file_exists($fajl) && $reset) {
        unlink($fajl);
    }
}

/*
	//$datetime = new DateTime('Sat Nov 15 23:58:46 +0000 2014');//created_at
	echo strtotime('Sat Nov 15 23:58:46 +0000 2014');
	$datetime = new DateTime(date("Y-m-d H:i:s"));//now
	//echo $datetime->getTimestamp();//sekunde
	echo $datetime->format('Y-m-d H:i:s') . "\n";
	$bg_time = new DateTimeZone("Europe/Belgrade");
	$datetime->setTimezone($bg_time);
	echo $datetime->format('Y-m-d H:i:s');
	
		//if( is_2_hours_old('Sat Nov 15 23:58:46 +0000 2014', '0') ) echo "true"; else echo "false"; die;

*/
/*
//ima jos//max_id odozgo vraca
if($max_id > $max_id1) {
	$since_id = $max_id1;			
	check_fav_rt($response);
	set_top_id($max_id1);
	continue;
} else {
	return;			
}*/		
/*
Array
(
    [errors] => Array
        (
            [0] => Array
                (
                    [code] => 44
                    [message] => since_id parameter is invalid.
                )

        )

)*/			
		//created_at Sat Nov 15 23:58:46 +0000 2014 // pise 1:58 AM - 16 Nov 2014//12:58 kod mene
	
	
	
	
	
	
	
	
	





