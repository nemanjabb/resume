<?php
require_once(__DIR__ . '/bot.php');

//on kaze dodaj me u listu ovaj u search nadje i doda ga bzv
class autoresponder extends bot {
    
    public function __construct($em, $list_id) {
        parent::__construct($em, $list_id);
    }

    //ne ceka da ostare 2 sata
    //jedan zahtev, motaj na dole dok ne dodjes do since_id
    public function search_timeline() {
        $term = "to: -from:";

        $since_id = $this->db->get_top_id('search_id'); //novije od ovog
        if (!$since_id)
            $since_id = null;
        $max_id = null;
        $updated_top = null;


        for ($j = 0; $j < 3; $j++) { //3x200	
            $str = '';
            if ($j > 0) {
                $str = '</tbody>';
                $alert = "success";
            } else {
                $alert = "info";
            }
            mojlog('s', $str . '<thead><tr class="' . $alert . '"><th>' . date('d.m.Y H:i:s') . ", i = " . $j . '</th><th colspan="4">query = ' . $term . "</th><th>since_id_db = " . $since_id . "</th><th>max_id = " . $max_id . "</th></tr></thead><tbody>");
            $response = $this->tw->get_search_tweets($term, $since_id, $max_id);

            if (!empty($response['errors'])) {
                mojlog('s', '<tr class="danger"><th colspan="7" >' . print_r($response, 1) . '</th></tr>');
                return;
            }
            if (count($response['statuses']) < 1) {//nije vratio nista
                mojlog('s', '<tr class="danger"><th colspan="7" >Empty response</th></tr>');
                return;
            }
            $response = $response['statuses'];
            $max_id1 = $response[0]["id_str"]; //najveci, najnoviji 		//[0] veci, noviji, [1] manji stariji	
            $since_id1 = end($response)["id_str"]; //najmanji, najstariji	
            if ($j == 0) {
                $this->db->set_top_id("search_id", $max_id1);
                $updated_top = $max_id1;
            }//odmah ga cuvam

            mojlog('s', '<tr><td>' . count($response) . ' results.<td colspan="2"> (najmladji) response[0] = ' . $max_id1 . '</td><td colspan="2">response[n] = ' . $since_id1 . '</td><td colspan="2"></td></tr>');
            $this->add_mentions_to_list($response);
            $max_id = $since_id1;
        }
        mojlog('s', '<tr class="info"><th colspan="7" >' . date('d.m.Y H:i:s') . ", Kraj http zahteva, Updated since_id_db = " . $updated_top . "</th></tr></tbody>");
    }

    private function add_mentions_to_list($response) {

        $i = 1;
        $bot_name = "last_2_hours_rs";
        $users = [];

        foreach ($response as $tweet) {
            if ($tweet['user']['screen_name'] == $bot_name)
                continue;

            mojlog('s', '<tr><td>tweet_id = ' . $tweet['id_str'] . '</td><td colspan="4">@' . $tweet['user']['screen_name'] . ' kaze: ' . $tweet['text'] . "</td>");

            $mention_true = has_mention($tweet);
            $phrase = match_phrase("dodaj me u listu", $tweet['text']);

            if ($mention_true && $phrase[0] == 'dodaj me u listu' || 1) {
                mojlog('s', '<td>favourited</td><td> @' . $tweet['user']['screen_name'] . ' Added to list</td></tr>');
                //log_tweet('s', $tweet);
                //fejvuj i dodaj u listu	//proveri da nije vec u listi
                $user_ids[] = $tweet['user']['id_str'];
                //favouriteStatus($tweet['id_str']);			//zakom	
                $user = user_from_tweet($tweet['user']);
                $user = default_user($user);
                $users[] = $user;
            }
            $i++;
        }
        //odjednom ih dodaj u listu
        //$this->tw->add_users_to_list($user_ids);			//zakom
        //$this->db->insert_users($users);			//zakom
    }



}