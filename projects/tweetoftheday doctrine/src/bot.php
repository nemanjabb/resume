<?php

require_once(__DIR__ . '/loged/loged.php');
require_once(__DIR__ . '/utils.php');
require_once(__DIR__ . '/repository.php');
require_once(__DIR__ . '/twapi.php');
date_default_timezone_set('Europe/London');

//verify_acc_works();
//$response = get_list_tweets();
//print_r( get_2_3_hours_tweet_ids() );
//go_back();
//search_timeline();
//print_r( filter_users_by_followers(formated_list_users($slug, $owner_screen_name, $count = 10)));
//$list_ar = array('slug' => 'dabetićeva-lista', 'owner_screen_name' => 'IDabetic');

/*
$temp_d = new doctrine_repository();
$list1 = $temp_d->get_list('IDabetic', 'dabetićeva-lista', $em);

if (!$list1) {
    $list_assoc['list_id'] = "130874743";
    $list_assoc['slug'] = 'dabetićeva-lista';
    $list_assoc['owner_screen_name'] = 'IDabetic';
    $list_assoc['user_id'] = "587755891";
    $list_assoc['country'] = "serbia1";
    $keys1 = ['Bn6bJ0eF1w9hcOpakHDw',
        'enQ1oyivTjAlf5Rq1k72cO41EPOVHtmapfeR2Fq0ks',
        '925217292-TqqbetMooLwtWXKP3fylPkTTlCqMOAKNvAywoGRA',
        'yrWjhUqeFi2KlpADQevnky9TFuzqGXcmbWvzAZkzV0M'];

    $list_assoc['keys1234'] = implode(",", $keys1);

    $temp_d->insert_list($list_assoc, $em);
}

$list2 = $temp_d->get_list('IDabetic', 'dabetićeva-lista', $em);
$b = new bot($em, $list2->getListId());
$list_arr = array('slug' => 'dabetićeva-lista', 'owner_screen_name' => 'IDabetic');
$b->dl_list_to_db($list_arr, 5); //lista koju usisavas
*/

class bot {

    public $db;//entity manager
    public $tw;//tw api

    public function __construct($em, $list_id) {
        $this->db = new doctrine_repository($em, $list_id);
        $this->db->create_timeline(); //if not exists

        $list = $em->find('ListTb', $list_id);
        $this->tw = new twapi($list); //keys, list
    }
    public static function log_tweet($file, $tweet, $rt = '') {
        $str = '<tr><td>' . $rt . '</td>';
        $str .= '<td>id_str: ' . $tweet['id_str'] . '</td>';
        $str .= '<td><a href="' . get_tweet_url($tweet) . '" target="_blank" title="' . $tweet['text'] . '" >' . substr($tweet['text'], 0, 70) . '</a></td>';
        $str .= '<td>' . $tweet['created_at'] . '</td>';
        $str .= '<td>RT: ' . $tweet['retweet_count'] . '</td>';
        $str .= '<td>Fav: ' . $tweet['favorite_count'] . '</td>';
        $str .= '<td>lang: ' . $tweet['lang'] . '</td></tr>';
        //echo $str;
        mojlog($file, $str);
    }
}





?>