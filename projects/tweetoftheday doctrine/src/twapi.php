<?php

require_once(__DIR__ . '/twitteroauth/twitteroauth.php');

class twapi {

    /*
    87Zt23e5v37SEnKMyA0cqdhcd
    4TbCD7JlspZzvGTUFnnMOZLPUYh8X2d5d7bMX1qm6N3Kfcv0Y2
    2880717957-UQQ8cUL5l9bYc7fyu1xOiCfxLNEWx6Vzwf3wm1L
    8XD0cdZBMdr4MukkY1AMa4Rt3kaTdWAGcK7BF3ES2zDsV

    prvitest
    123456;
    */
    
    //nemoze_macka
    /*public $keys = ['Bn6bJ0eF1w9hcOpakHDw',
                    'enQ1oyivTjAlf5Rq1k72cO41EPOVHtmapfeR2Fq0ks',
                    '925217292-TqqbetMooLwtWXKP3fylPkTTlCqMOAKNvAywoGRA',
                    'yrWjhUqeFi2KlpADQevnky9TFuzqGXcmbWvzAZkzV0M'];*/

    public $list = []; //array('slug' => 'dabetićeva-lista', 'owner_screen_name' => 'IDabetic');
    public $connection;

    public function __construct($list1) {
 
        $keys = explode(",", $list1->getKeys1234());       
        $this->connection = new TwitterOAuth($keys[0], $keys[1], $keys[2], $keys[3]);
        $this->list['slug'] = $list1->getSlug();
        $this->list['$owner_screen_name'] = $list1->getOwnerScreenName();
    }
    
    
    //sacekaj da ostare 2 sata
    public function get_2_3_hours_tweet_ids() {
        //@big_ben_clock
       
        $params = [
            'screen_name' => "big_ben_clock",
            'count' => 10, //def 15
            'include_rts' => 0,
            'include_entities' => 0,
        ];
        $response = $this->connection->get('statuses/user_timeline', $params);
        //print_r($response);

        $ids = array();
        foreach ($response as $tweet) {
            $hour_ids = array();
            for ($i = 1; $i < 10; $i++) {
                if (is_2_hours_old($tweet["created_at"], $i) && !is_2_hours_old($tweet["created_at"], $i + 1)) {
                    $hour_ids["id_str"] = $tweet["id_str"];
                    $hour_ids["bg_time"] = to_belgrade($tweet["created_at"]);
                    $ids[$i] = $hour_ids;
                }
            }
        }
        return $ids;
    }

    public function get_search_tweets($upit, $since_id = null, $max_id = null) {
        //since_id - Returns results with an ID greater than (that is, more recent than)
        //max_id - Returns results with an ID less than (that is, older than) 	
        
        $params = array('q' => $upit,
            'count' => 3, //def 15
            'result_type' => 'recent',
            //'result_type' => 'mixed',
            'since_id' => $since_id,
            'max_id' => $max_id,
                //'include_entities' => 1,
        );
        return $response = $this->connection->get('search/tweets', $params);
    }

    //nadji prvi stariji od 2h i idi nadole sa max_id koliko mozes do 180*200
    public function get_list_tweets($since_id = null, $max_id = null) {
        /*
          max_id vraca ispod njega
          kada je sa since_id vraca iznad njega
          since_id vraca bas najnovije bezveze
          gore veci dole manji
          stariji manji broj, noviji veci
         */

        $debug = 0;
        //$max_id   = "534027154468929537";//fora mora veci od since_id//inkluzivan
        //$since_id = "534026806194868225";
        $params = [
            //'list_id' => $list_id,
            'slug' => $this->list['slug'],
            'owner_screen_name' => $this->list['owner_screen_name'],
            'count' => 4, //def 15
            'include_rts' => 1,
            'max_id' => $max_id,
            'since_id' => $since_id, //da ne preklopi
                //'include_entities' => 1,
        ];
        //if($since_id == '2') unset($params["since_id"]);
        $response = $this->connection->get('lists/statuses', $params);

        //debug
        if ($debug == 1)
            debug_results($response);
        if ($debug == 2)
            print_r($response);

        return $response;
    }

    public function verify_acc_works() {

        try {
            $response = $this->connection->get('account/verify_credentials');
            //print_r($response);
            $screenName = $response['screen_name'];
            return true;
        } catch (Exception $ex) {
            
        }
        return false;
    }

    public function add_users_to_list($users_ids_arr) {

        $users_ids_coma = rtrim(implode(',', $users_ids_arr), ',');
        $params = [
            //'list_id' => $list_id,
            'slug' => $this->list['slug'],
            'owner_screen_name' => $this->list['owner_screen_name'],
            'user_id' => $users_ids_coma,
                //'screen_name ' => $screen_names_coma,
        ];

        $response = $this->connection->post('lists/members/create_all', $params);
        // up to 100 are allowed in a single request.
    }

    public function get_list_members($slug, $owner_screen_name, $count) {

        $params = [
            //'list_id' => $list_id,
            'slug' => $slug,
            'owner_screen_name' => $owner_screen_name,
            //'screen_name ' => $screen_names_coma,
            'count' => $count, //5000
            'cursor' => -1,
            'include_entities ' => false,
        ];

        return $response = $this->connection->get('lists/members', $params);
        // 15-min window (user auth)180
        // 15-min window (app auth)15
        // count default is 20, with a maximum of 5,000.//ima i kursor
    }

    public function retweetStatus($id) {
        $params = array('id' => $id,);
        $response = $this->connection->post('statuses/retweet/' . $id); //ok
        //print_r($response);
    }

    public function favouriteStatus($id) {
        $params = array('id' => $id,);
        $response = $this->connection->post('favorites/create', $params);
    }

}
