
var staza_do_src = '/1/last2hours';

$(document).ready(function () {

    //search
    $("#print_list_btn").click(function () {
        $.get(staza_do_src + "/src/kontroler.php", {print_list: $("#print_list_input").val()})
                .done(function (data) {
                    var data1 = $.parseJSON(data);
                    $('#table_main').bootstrapTable('load', data1);
                });
    });
    //set my list in session
    $("#set_my_list_btn").click(function () {
        $.get(staza_do_src + "/src/kontroler.php", {set_my_list: $("#set_my_list_input").val()})
                .done(function (data) {
                    var data1 = $.parseJSON(data);
                    $('#suck_list_h4').html('Suck list: ' + data1.set_my_list);
                }).fail(function () {
            $('#suck_list_h4').html('Suck list: No list js.');
        });
    });
    //db_insert
    $("#db_insert_btn").click(function () {
        var form_data = $('.my_db_insert input').serializeObject();
        if (!validate_db_insert(form_data)) {
            alert("invalid db_insert data");
            return false;
        }
        $.get(staza_do_src + "/src/kontroler.php", {db_insert: form_data})
                .done(function (data) {
                    var data1 = $.parseJSON(data);
                    $('#table_main').bootstrapTable('load', data1);
                })
    });
    //activate_users
    $("#update_act_btn, #delete_act_btn, #show_act_btn").click(function () {
        var form_data = $('.act_users input').serializeObject();
        form_data.above == "on" ? form_data.above = true : form_data.above = false;
        form_data.btn = $(this).html();
        var msg = validate_act_users(form_data);
        if (msg) {
            alert(msg);
            return false;
        }
/*
        $.ajax({
            type: "GET",
            contentType: "",
            url: staza_do_src + "/src/kontroler.php",
            data: JSON.stringify({act_users: form_data})
            //dataType: "json"
        }).done(function (data) {
                    var data1 = $.parseJSON(data);
                    $('#table_main').bootstrapTable('load', data1);
                });
                */

        $.getJSON(staza_do_src + "/src/kontroler.php", {act_users: form_data})
                .done(function (data) {
                    //var data1 = $.parseJSON(data);
                    $('#table_main').bootstrapTable('load', data);
                })
    });

    //enter
    $("#screen_name_input").keypress(function (event) {
        if (event.which == 13) {

        }
    });
    //click user link
    $(document).on("click", ".userLink", function () {

    });
    //get50 radio
    $(document).on('change', 'input:radio[name="recent50"]', function (event) {
        var value = $(this).val();
        $.get(staza_do_src + "/src/following.php", {recent50: value})
                .done(function (data) {
                    getLinks(false);
                });
    });

    $(".login_tw").click(function () {
        //window.location.href = "../login/redirect.php";
    });

});
function validate_db_insert(form_data) {
    if (!/(\w+\s*,\s*\w+)/.test(form_data.from_list))
        return false;//treba detaljnije
    if (!/^(\d{1,7})$/.test(form_data.followers))
        return false;
    if (!/^(\d{1,6})$/.test(form_data.statuses))
        return false;
    if (!/^(\d{1,4})$/.test(form_data.count))
        return false;
    return true;
}
function validate_act_users(form_data) {
    var followers_valid = /^(\d{1,7})$/.test(form_data.followers);//^$prazan
    var statuses_valid = /^(\d{1,6})$/.test(form_data.statuses);
    var count_valid = /^(\d{1,4})$/.test(form_data.count);
    var in_list_valid = /^(\d{1,6})$/.test(form_data.in_list);
    var user_valid = /^(\w{1,15})$/.test(form_data.screen_name);

    if (form_data.btn == 'Show') {

        if (user_valid && (followers_valid || statuses_valid || count_valid))
            return "Set just user.";
        if (!(count_valid && (user_valid || followers_valid || statuses_valid || in_list_valid)))
            return "Set count and one filter criteria."
    }
    if (form_data.btn == 'Update') {
        if (!(in_list_valid && count_valid && (followers_valid || statuses_valid)))
            return "Count and list required and one filter.";
    }
    if (form_data.btn == 'Delete') {
        if (user_valid && (followers_valid || statuses_valid || count_valid))
            return "Set just user.";
        if (!(in_list_valid && count_valid && (followers_valid || statuses_valid)))
            return "Count and list required and one filter.";
    }

    return false;//valid form
}
$.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
/*
 <!--
 Desktop:
 [ 2nd col ] [ 1st col ]
 
 Mobile:
 [ 2nd col ]
 [ 1st col ]
 -->
 <div class="row">
 <div class="col-md-6 col-md-push-6">1st col</div>
 <div class="col-md-6 col-md-pull-6">2nd col</div>
 </div>
 https://scotch.io/bar-talk/bootstrap-3-tips-and-tricks-you-might-not-know
 */
















