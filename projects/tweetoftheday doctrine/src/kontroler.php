<?php

require_once(__DIR__ . '/suck_list.php');
require_once(__DIR__ . '/validation.php');
session_start();


//print_r($_GET); die;
new kontroler($_GET, $em);

class kontroler {

    private $post;
    private $em;

    public function __construct($post, $em) {
        $this->post = $post;
        $this->em = $em;
        $this->switch_f();
    }

    public function switch_f() {
        $case = -1;
        if (array_key_exists('print_list', $this->post)) {
            $case = 1;
        } else if (array_key_exists('db_insert', $this->post)) {
            $case = 2;
        } else if (array_key_exists('act_users', $this->post)) {
            $case = 3;
        }
        switch ($case) {

            case 1:
                $this->echo_print_list();
                break;
            case 2:
                $this->echo_download_list();
                break;
            case 3:
                $this->echo_activate_users();
                break;
            
            default:
                $this->echo_set_my_list();
                break;
        }
    }

    public function echo_activate_users() {
        
        $list1 = $this->get_my_db_list_from_str($_SESSION['set_my_list']['set_my_list']);//moja lista iz db
        $suck_l = new suck_list($this->em, $list1->getListId());
        $act_form = (new activate_form())->validate($this->post['act_users']);
        if(!is_object($act_form)) {
            echo $act_form;
            return;
        }
        $users = $suck_l->activate_users($act_form);
        echo json_encode($users);
    }
    
    
    //IDabetic, dabetićeva-lista
    public function echo_print_list() {
        $list_arr = self::list_arr_from_comma($this->post['print_list']);
        $list1 = $this->default_list($this->em);
        $suck_l = new suck_list($this->em, $list1->getListId());
        $list_users = $suck_l->formated_list_users($list_arr['slug'], $list_arr['owner_screen_name'], 50);
        echo json_encode($list_users);
    }
    //5000 mozes da iscitas iz liste, 100 da ubacis u tviterovu listu
    public function echo_download_list() {
        $list_arr = self::list_arr_from_comma($this->post['db_insert']['from_list']);//dabetic lista
        $kriter_arr['followers_count'] = $this->post['db_insert']['followers'];
        $kriter_arr['statuses_count'] = $this->post['db_insert']['statuses'];
        $count = $this->post['db_insert']['count'];
        
        $list1 = $this->get_my_db_list_from_str($_SESSION['set_my_list']['set_my_list']);//moja lista iz db
        $suck_l = new suck_list($this->em, $list1->getListId());
        $list_users = $suck_l->dl_list_to_db($list_arr, $kriter_arr, $count);
        echo json_encode($list_users);
    }

    public static function list_arr_from_comma($slug_owner_str) {//neka post, staticka
        $arr = [];
        $arr['slug'] = trim(explode(',', $slug_owner_str)[1]);
        $arr['owner_screen_name'] = trim(explode(',', $slug_owner_str)[0]);
        return $arr;
    }

    public function get_my_db_list_from_str($slug_owner_str) {
        $l_arr = self::list_arr_from_comma($slug_owner_str);
        return $list1 = doctrine_repository::get_list_static($l_arr['owner_screen_name'], $l_arr['slug'], $this->em);
    }

    //validate set my list
    public function echo_set_my_list() {
        if (array_key_exists('set_my_list', $this->post)) {
            //neka validacija
            $_SESSION['set_my_list'] = ['set_my_list' => $this->post['set_my_list']];//dabeticeva, ne iz baze
        }
        else if (empty($_SESSION['set_my_list']))
            $_SESSION['set_my_list'] = ['set_my_list' => 'No list.'];
        echo json_encode($_SESSION['set_my_list']);
    }

    public function default_list() {

        $list1 = doctrine_repository::get_list_static('prvitest', 'prva', $this->em);
        if ($list1 != null)
            return $list1;

        $list_assoc['list_id'] = "204452518";
        $list_assoc['slug'] = 'prva';
        $list_assoc['owner_screen_name'] = 'prvitest';
        $list_assoc['user_id'] = "2880717957";
        $list_assoc['country'] = "serbia1";
        $keys1 = ['87Zt23e5v37SEnKMyA0cqdhcd',
            '4TbCD7JlspZzvGTUFnnMOZLPUYh8X2d5d7bMX1qm6N3Kfcv0Y2',
            '2880717957-UQQ8cUL5l9bYc7fyu1xOiCfxLNEWx6Vzwf3wm1L',
            '8XD0cdZBMdr4MukkY1AMa4Rt3kaTdWAGcK7BF3ES2zDsV'];
        $list_assoc['keys1234'] = implode(",", $keys1);

        $doctr_rep = new doctrine_repository($this->em, new ListTb($list_assoc));
        return $doctr_rep->insert_list($list_assoc, $this->em); //vraca listTb entity
    }

}
