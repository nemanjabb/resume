
# Tweet of the day (Doctrine version)  

This project is first and deprecated Doctrine version of _Tweet of the day_ application made in Laravel. Reason for this is that it's started as to much database-centric application with wrong priorities like list manager which is unnecessary as users shouldn't be added or removed from list to often. Also database acts like cache therefore reducing realtime character of the application and causing consistency problems.
 
# Features  

**Traverse list timeline**    

In `/src/rt_best.php` is implemented list traversing, not fully optimal (exit criterium) which is improoved in Laravel version. Cursors are kept in database.

**Doctrine repository**  

In `/src/repository.php` are grouped all Doctrine database queries in to one repository.

**Twitter API repository**

In `/src/twapi.php` are grouped all functions that call Twitter API in to one repository.

**List manager**

This functionality was ment to help managing large number of users in to number of lists. It included html form in `/src/html/index.html`, controller in `/src/kontroler.php`, and database and Twitter API syncing logic in `/src/suck_list.php`.

**Database**

Database schema is in `/db`, models are in `/entities`, and yml mappings are in `/config/yaml` folder. 

# Project status  

Functionalities are in different stage of development and this project is deprecated in favour of Laravel version.

# Licence

Use this only as portfolio item in order to determine my programming expertise level. Do not use or distribute. Author: `nemanja.mitic.elfak@hotmail.com` spring 2015.

