<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TweetsTb
 */
class TweetsTb
{
    /**
     * @var string
     */
    private $idStr;

    /**
     * @var string
     */
    private $text1;

    /**
     * @var string
     */
    private $ownerIdStr;
    
    /**
     * @var string
     */
    private $screenName;
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $dateProcessed;

    /**
     * @var integer
     */
    private $favCount;

    /**
     * @var integer
     */
    private $rtCount;

    /**
     * @var integer
     */
    private $rtEd;

    /**
     * @var \ListTb
     */
    private $list;

    public function __construct($arr){
        $this->idStr = $arr['id_str'];
        $this->listId = $arr['list_id'];
        $this->text1 = $arr['text'];
        $this->ownerIdStr = $arr['owner_id_str'];
        $this->screenName = $arr['screen_name'];
        $this->createdAt = $arr['created_at'];
        $this->dateProcessed = $arr['date_processed'];
        $this->favCount = $arr['fav_count'];
        $this->rtCount = $arr['rt_count'];
        $this->rtEd = $arr['rt_ed'];
    }
    
    /**
     * Get idStr
     *
     * @return string 
     */
    public function getIdStr()
    {
        return $this->idStr;
    }

    /**
     * Set text1
     *
     * @param string $text1
     * @return TweetsTb
     */
    public function setText1($text1)
    {
        $this->text1 = $text1;

        return $this;
    }

    /**
     * Get text1
     *
     * @return string 
     */
    public function getText1()
    {
        return $this->text1;
    }

    /**
     * Set ownerIdStr
     *
     * @param string $ownerIdStr
     * @return TweetsTb
     */
    public function setOwnerIdStr($ownerIdStr)
    {
        $this->ownerIdStr = $ownerIdStr;

        return $this;
    }

    /**
     * Get ownerIdStr
     *
     * @return string 
     */
    public function getOwnerIdStr()
    {
        return $this->ownerIdStr;
    }
    /**
     * Set ownerIdStr
     *
     * @param string $screenName
     * @return TweetsTb
     */
    public function setScreenName($screenName)
    {
        $this->screenName = $screenName;

        return $this;
    }

    /**
     * Get screenName
     *
     * @return string 
     */
    public function getScreenName()
    {
        return $this->screenName;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return TweetsTb
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set dateProcessed
     *
     * @param \DateTime $dateProcessed
     * @return TweetsTb
     */
    public function setDateProcessed($dateProcessed)
    {
        $this->dateProcessed = $dateProcessed;

        return $this;
    }

    /**
     * Get dateProcessed
     *
     * @return \DateTime 
     */
    public function getDateProcessed()
    {
        return $this->dateProcessed;
    }

    /**
     * Set favCount
     *
     * @param integer $favCount
     * @return TweetsTb
     */
    public function setFavCount($favCount)
    {
        $this->favCount = $favCount;

        return $this;
    }

    /**
     * Get favCount
     *
     * @return integer 
     */
    public function getFavCount()
    {
        return $this->favCount;
    }

    /**
     * Set rtCount
     *
     * @param integer $rtCount
     * @return TweetsTb
     */
    public function setRtCount($rtCount)
    {
        $this->rtCount = $rtCount;

        return $this;
    }

    /**
     * Get rtCount
     *
     * @return integer 
     */
    public function getRtCount()
    {
        return $this->rtCount;
    }

    /**
     * Set rtEd
     *
     * @param integer $rtEd
     * @return TweetsTb
     */
    public function setRtEd($rtEd)
    {
        $this->rtEd = $rtEd;

        return $this;
    }

    /**
     * Get rtEd
     *
     * @return integer 
     */
    public function getRtEd()
    {
        return $this->rtEd;
    }

    /**
     * Set list
     *
     * @param \ListTb $list
     * @return TweetsTb
     */
    public function setList(\ListTb $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \ListTb 
     */
    public function getList()
    {
        return $this->list;
    }
}
