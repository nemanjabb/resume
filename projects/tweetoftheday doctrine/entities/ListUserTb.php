<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ListUserTb
 */
class ListUserTb
{

    /**
     * @var integer
     */
    private $priority;

    /**
     * @var \DateTime
     */
    private $dateAdded;

    /**
     * @var \DateTime
     */
    private $dateRemoved;

    /**
     * @var \DateTime
     */
    private $dateLastRt;

    /**
     * @var integer
     */
    private $inList;

    /**
     * @var \ListTb
     */
    private $list;
    
    /**
     * @var \UsersTb
     */
    private $user;
    
    public function __construct()
    {
        $this->dateAdded = new DateTime(); //doctr ne podrzava default mysql pa mora ovako
        $this->inList = 0;
        $this->priority = 1;
    }

    /**
     * Set user
     *
     * @param \UsersTb $user
     * @return ListUserTb
     */
    public function setUser(\UsersTb $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \UsersTb 
     */
    public function getUsers()
    {
        return $this->user;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return ListUserTb
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     * @return ListUserTb
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime 
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateRemoved
     *
     * @param \DateTime $dateRemoved
     * @return ListUserTb
     */
    public function setDateRemoved($dateRemoved)
    {
        $this->dateRemoved = $dateRemoved;

        return $this;
    }

    /**
     * Get dateRemoved
     *
     * @return \DateTime 
     */
    public function getDateRemoved()
    {
        return $this->dateRemoved;
    }

    /**
     * Set dateLastRt
     *
     * @param \DateTime $dateLastRt
     * @return ListUserTb
     */
    public function setDateLastRt($dateLastRt)
    {
        $this->dateLastRt = $dateLastRt;

        return $this;
    }

    /**
     * Get dateLastRt
     *
     * @return \DateTime 
     */
    public function getDateLastRt()
    {
        return $this->dateLastRt;
    }

    /**
     * Set inList
     *
     * @param integer $inList
     * @return ListUserTb
     */
    public function setInList($inList)
    {
        $this->inList = $inList;

        return $this;
    }

    /**
     * Get inList
     *
     * @return integer 
     */
    public function getInList()
    {
        return $this->inList;
    }

    /**
     * Set list
     *
     * @param \ListTb $list
     * @return ListUserTb
     */
    public function setList(\ListTb $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \ListTb 
     */
    public function getList()
    {
        return $this->list;
    }
}
