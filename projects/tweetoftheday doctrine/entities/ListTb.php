<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ListTb
 */
class ListTb
{
    /**
     * @var string
     */
    private $listId;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $ownerScreenName;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $country;
    
    /**
     * @var string
     */
    private $keys1234;
    
    /**
     * @var \Doctrine\Common\Collections\Collection;
     */
    private $listUserTb;
    
    public function __construct($arr){
        $this->listId = $arr['list_id'];
        $this->slug = $arr['slug'];
        $this->ownerScreenName = $arr['owner_screen_name'];
        $this->userId = $arr['user_id'];
        $this->country = $arr['country'];
        $this->keys1234 = $arr['keys1234'];
   
        $this->listUserTb = new \Doctrine\Common\Collections\ArrayCollection();
        
    }
    /**
     * Add listUserTb
     *
     * @param \ListUserTb $listUserTbl
     * @return UsersTb
     */
    public function addListUserTb(\ListUserTb $listUserTbl)
    {
        $this->listUserTb[] = $listUserTbl;
        return $this;
    }
 
    /**
     * Remove listUserTb
     *
     * @param \ListUserTb $listUserTbl
     */
    public function removeListUserTb(\ListUserTb $listUserTbl)
    {
        $this->listUserTb->removeElement($listUserTbl);
    }
 
    /**
     * Get listUserTbs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListUserTbs()
    {
        return $this->listUserTb;
    }

    /**
     * Get listId
     *
     * @return string 
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ListTb
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set ownerScreenName
     *
     * @param string $ownerScreenName
     * @return ListTb
     */
    public function setOwnerScreenName($ownerScreenName)
    {
        $this->ownerScreenName = $ownerScreenName;

        return $this;
    }

    /**
     * Get ownerScreenName
     *
     * @return string 
     */
    public function getOwnerScreenName()
    {
        return $this->ownerScreenName;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return ListTb
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return ListTb
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }
       
    /**
     * Set keys1234
     *
     * @param string $keys1234
     * @return ListTb
     */
    public function setKeys1234($keys1234)
    {
        $this->keys1234 = $keys1234;

        return $this;
    }

    /**
     * Get keys1234
     *
     * @return string 
     */
    public function getKeys1234()
    {
        return $this->keys1234;
    }
}
