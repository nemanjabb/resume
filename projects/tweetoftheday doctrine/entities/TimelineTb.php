<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TimelineTb
 */
class TimelineTb
{
    /**
     * @var string
     */
    private $key1;

    /**
     * @var string
     */
    private $val;

    /**
     * @var \ListTb
     */
    private $list;

    public function __construct($arr){
        $this->key1 = $arr['key1'];
        $this->val = $arr['val'];
        $this->setList($arr['list']);
    }

    /**
     * Set key1
     *
     * @param string $key1
     * @return TimelineTb
     */
    public function setKey1($key1)
    {
        $this->key1 = $key1;

        return $this;
    }

    /**
     * Get key1
     *
     * @return string 
     */
    public function getKey1()
    {
        return $this->key1;
    }

    /**
     * Set val
     *
     * @param string $val
     * @return TimelineTb
     */
    public function setVal($val)
    {
        $this->val = $val;

        return $this;
    }

    /**
     * Get val
     *
     * @return string 
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Set list
     *
     * @param \ListTb $list
     * @return TimelineTb
     */
    public function setList(\ListTb $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \ListTb 
     */
    public function getList()
    {
        return $this->list;
    }
}
