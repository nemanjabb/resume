<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * UsersTb
 */
class UsersTb
{
    /**
     * @var string
     */
    private $idStr;

    /**
     * @var string
     */
    private $screenName;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $followersCount;

    /**
     * @var integer
     */
    private $friendsCount;

    /**
     * @var integer
     */
    private $statusesCount;
    
    /**
     * @var \Doctrine\Common\Collections\Collection;
     */
    private $listUserTb;
    
    public function __construct($arr){
        $this->idStr = $arr['id_str'];
        $this->screenName = $arr['screen_name'];
        $this->name = $arr['name'];
        $this->followersCount = $arr['followers_count'];
        $this->friendsCount = $arr['friends_count'];
        $this->statusesCount = $arr['statuses_count'];
    
        $this->listUserTb = new \Doctrine\Common\Collections\ArrayCollection();
        
    }
    /**
     * Add listUserTb
     *
     * @param \ListUserTb $listUserTbl
     * @return UsersTb
     */
    public function addListUserTb(\ListUserTb $listUserTbl)
    {
        $this->listUserTb[] = $listUserTbl;
        return $this;
    }
 
    /**
     * Remove listUserTb
     *
     * @param \ListUserTb $listUserTbl
     */
    public function removeListUserTb(\ListUserTb $listUserTbl)
    {
        $this->listUserTb->removeElement($listUserTbl);
    }
 
    /**
     * Get listUserTbs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListUserTbs()
    {
        return $this->listUserTb;
    }

    /**
     * Get idStr
     *
     * @return string 
     */
    public function getIdStr()
    {
        return $this->idStr;
    }

    /**
     * Set screenName
     *
     * @param string $screenName
     * @return UsersTb
     */
    public function setScreenName($screenName)
    {
        $this->screenName = $screenName;

        return $this;
    }

    /**
     * Get screenName
     *
     * @return string 
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UsersTb
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set followersCount
     *
     * @param integer $followersCount
     * @return UsersTb
     */
    public function setFollowersCount($followersCount)
    {
        $this->followersCount = $followersCount;

        return $this;
    }

    /**
     * Get followersCount
     *
     * @return integer 
     */
    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    /**
     * Set friendsCount
     *
     * @param integer $friendsCount
     * @return UsersTb
     */
    public function setFriendsCount($friendsCount)
    {
        $this->friendsCount = $friendsCount;

        return $this;
    }

    /**
     * Get friendsCount
     *
     * @return integer 
     */
    public function getFriendsCount()
    {
        return $this->friendsCount;
    }

    /**
     * Set statusesCount
     *
     * @param integer $statusesCount
     * @return UsersTb
     */
    public function setStatusesCount($statusesCount)
    {
        $this->statusesCount = $statusesCount;

        return $this;
    }

    /**
     * Get statusesCount
     *
     * @return integer 
     */
    public function getStatusesCount()
    {
        return $this->statusesCount;
    }
}
