
CREATE TABLE IF NOT EXISTS timeline_tb (
    key1     VARCHAR(255), 
    val      VARCHAR(255),
    list_id  VARCHAR(255) NOT NULL,		/*fk list_tb, od liste ka ovome*/
    PRIMARY KEY (key1, list_id)			
);

CREATE TABLE IF NOT EXISTS timeline_tb (
    max_id      VARCHAR(255), /*id_str str broj*/
    prev_max_id VARCHAR(255), /*ne ides do dna svih tvitova liste nego do proslog*/
    temp_max_id VARCHAR(255), /*since_id najobicniji*/
    bigben_2h   VARCHAR(255),
    list_id     VARCHAR(255) NOT NULL,		
    PRIMARY KEY (list_id)			
);

CREATE TABLE IF NOT EXISTS list_tb (
    list_id               VARCHAR(255), 
    slug                  VARCHAR(255) NOT NULL, 
    owner_screen_name     VARCHAR(255) NOT NULL, /*screen_name*/
    user_id               VARCHAR(255) NOT NULL,
    country               VARCHAR(255) NOT NULL,/*serbia 1 2 3*/
    keys1234              VARCHAR(1000)NOT NULL,
    PRIMARY KEY (list_id)
);


CREATE TABLE IF NOT EXISTS users_tb (			/*users of 1 on more lists*/
    id_str          VARCHAR(255) PRIMARY KEY,             
    screen_name     VARCHAR(255) NOT NULL,
    name            VARCHAR(255) NOT NULL,
    followers_count INT(10)      NOT NULL,
    friends_count   INT(10)      NOT NULL,
    statuses_count  INT(10)      NOT NULL   
);


CREATE TABLE IF NOT EXISTS list_user_tb (               /*inversedBy je za bidirekcionu*/
    list_id         VARCHAR(255),
    user_id         VARCHAR(255),
    priority        INT(10)  DEFAULT 1,
    date_added      DATETIME DEFAULT CURRENT_TIMESTAMP,
    date_removed    DATETIME DEFAULT NULL,
    date_last_rt    DATETIME DEFAULT NULL,
    num_rts         INT(10)  DEFAULT 0,
    in_list         INT(10)  DEFAULT 1, /*0 ne, 1 da, 2 izbacen, 3 kandidat*/
    PRIMARY KEY (list_id, user_id)
);

CREATE TABLE IF NOT EXISTS tweets_tb (
	id_str    VARCHAR(255) PRIMARY KEY,  
	list_id   VARCHAR(255) NOT NULL,	/*fk list_tb*/
	text1     VARCHAR(255),
	owner_id_str  VARCHAR(255),
    screen_name   VARCHAR(255),
	created_at      DATETIME,
	date_processed  DATETIME,
	fav_count       INT(10),
	rt_count        INT(10),
	rt_ed           INT(3)
	/*text, owner_id_str, created_at, fav_count, rt_count, rt_ed, date_processed*/
);

ALTER TABLE timeline_tb 
	ADD CONSTRAINT  fk_timeline_list FOREIGN KEY (list_id) REFERENCES list_tb(list_id);		/*ok*/
			
ALTER TABLE list_user_tb 
	ADD CONSTRAINT fk_list_user_tb_list_tb FOREIGN KEY (list_id) REFERENCES list_tb(list_id);
/*	
ALTER TABLE list_user_tb 
	ADD CONSTRAINT fk_list_user_tb_users_tb FOREIGN KEY (user_id) REFERENCES users_tb(id_str);
*/
ALTER TABLE tweets_tb 
	ADD CONSTRAINT fk_tweets_list_tb FOREIGN KEY (list_id) REFERENCES list_tb(list_id);

	
	
/*

INSERT OR IGNORE INTO timeline_tb (key1, val) 
VALUES ('last_id', '2', 'serbia'), ('prev_last_id', '0', 'serbia'), ('search_id', '0', 'serbia'), ('bigben_2h', '0', 'serbia'), ('list_temp_max_id', '0', 'serbia');

INSERT OR IGNORE INTO users_tb (id_str, screen_name, name, followers_count, friends_count, statuses_count)
VALUES ('0', '0', '0', 0, 0, 0);



http://stackoverflow.com/questions/22013511/mysql-error-1215-hy000-cannot-add-foreign-key-constraint


ALTER TABLE timeline_tb 
	ADD CONSTRAINT  fk_timeline_list FOREIGN KEY (country) REFERENCES list_tb(country);
	    ON DELETE CASCADE
		ON UPDATE CASCADE


*/






