<?php

    $glavna_db = __DIR__ . '/bin/glavna.sqlite';
    $country = "serbia";

class sqlite_db {//singleton treba
    
    private $glavna_db;
    public $file_db;
    
    public function __construct() {
        $this->glavna_db = __DIR__ . '/bin/glavna.sqlite';
        $this->file_db = new PDO('sqlite:' . $this->glavna_db);
        $this->file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    public function get_file_db(){
        return $this->file_db;
    }
}
class timeline_ extends sqlite_db{//kad nasledis klasu dodajes promenljive u this koje mozes da koristis

    public $country;

    public function __construct($country) {
        parent::__construct();//file_db
        $this->country = $country;
    }

    public function create_timeline_tb() {

        //create
        $create_tables = "CREATE TABLE IF NOT EXISTS timeline_tb (
                            key     TEXT, 
                            val     TEXT,
                            country TEXT,
                            PRIMARY KEY (key, country));";
        $this->file_db->exec($create_tables);
        //default
        $insert = "INSERT OR IGNORE INTO timeline_tb (key, val, country) 
		   VALUES ('last_id', '2', '$this->country'), ('prev_last_id', '0', '$this->country'), ('search_id', '0', '$this->country'), ('bigben_2h', '0', '$this->country'), ('list_temp_max_id', '0', '$this->country');";
        $stmt = $this->file_db->prepare($insert);
        $stmt->execute();
    }

    public function get_top_id($key) { //sqlite zadnji procesiran id

        try {
            $stmt = $this->file_db->prepare("SELECT val FROM timeline_tb WHERE key='$key' AND country='$this->country'"); //AS last_id		
            $stmt->execute();
            $last_id = $stmt->fetchColumn(); //$stmt->fetchAll(PDO::FETCH_NUM);		
            return $last_id;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function set_top_id($key, $val) {

        try {
            $stmt = $this->file_db->prepare("UPDATE timeline_tb SET val = (:val) WHERE key='$key' AND country='$this->country'");
            $stmt->bindValue(':val', $val, PDO::PARAM_STR);
            $stmt->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function reset_timeline_tb() {
        $drop_table = "DROP TABLE IF EXISTS timeline_tb;";
        $this->file_db->exec($drop_table);
        $this->create_timeline_tb();
    }
}
class user_ extends sqlite_db{

    public function __construct() {
        parent::__construct();
    }
    
    public function create_users_tb() {
        //create
        $create_tables = "CREATE TABLE IF NOT EXISTS users_tb (
                            id_str          TEXT PRIMARY KEY  CHECK (id_str <> ''),
                            screen_name     TEXT             NOT NULL,
                            name            TEXT             NOT NULL,
                            followers_count INTEGER          NOT NULL,
                            friends_count   INTEGER          NOT NULL,
                            statuses_count  INTEGER          NOT NULL,
                            priority        INTEGER          NOT NULL,
                            date_added      INTEGER          NOT NULL,
                            date_removed    INTEGER          NOT NULL,
                            date_last_rt    INTEGER          NOT NULL,
                            in_list         INTEGER          NOT NULL);"; //0 ne, 1 da, 2 izbacen, 3 kandidat
        $this->file_db->exec($create_tables);

        //default	
        $insert = "INSERT OR IGNORE INTO users_tb (id_str, screen_name, name, followers_count, friends_count, statuses_count, priority, date_added, date_removed, date_last_rt, in_list)
                               VALUES ('0', '0', '0', 0, 0, 0, 0, 0, 0, 0, 0);";
        $stmt = $this->file_db->prepare($insert);
        $stmt->execute();
    }

    public function insert_user($user) {

        if (empty($user['screen_name']))
            $user['screen_name'] = '';
        if (empty($user['name']))
            $user['name'] = '';

        try {
            $columns = "(id_str, screen_name, name, followers_count, friends_count, statuses_count, priority, date_added, date_removed, date_last_rt, in_list)";
            $values = "(:id_str, :screen_name, :name, :followers_count, :friends_count, :statuses_count, :priority, :date_added, :date_removed, :date_last_rt, :in_list)";
            $query = "INSERT OR IGNORE INTO users_tb " . $columns . " VALUES " . $values;
            $stmt = $this->file_db->prepare($query);
            $stmt->bindValue(':id_str', $user['id_str'], PDO::PARAM_STR);
            $stmt->bindValue(':screen_name', $user['screen_name'], PDO::PARAM_STR);
            $stmt->bindValue(':name', $user['name'], PDO::PARAM_STR);
            $stmt->bindValue(':followers_count', $user['followers_count'], PDO::PARAM_INT);
            $stmt->bindValue(':friends_count', $user['friends_count'], PDO::PARAM_INT);
            $stmt->bindValue(':statuses_count', $user['statuses_count'], PDO::PARAM_INT);
            $stmt->bindValue(':priority', $user['priority'], PDO::PARAM_INT);
            $stmt->bindValue(':date_added', $user['date_added'], PDO::PARAM_INT);
            $stmt->bindValue(':date_removed', $user['date_removed'], PDO::PARAM_INT);
            $stmt->bindValue(':date_last_rt', $user['date_last_rt'], PDO::PARAM_INT);
            $stmt->bindValue(':in_list', $user['in_list'], PDO::PARAM_INT);
            $stmt->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function insert_users($users) {
        foreach ($users as $user)
            $this->insert_user($user);
    }
    public function reset_users_tb() {
        $drop_table = "DROP TABLE IF EXISTS users_tb;";
        $this->file_db->exec($drop_table);
        $this->create_users_tb();
    }
}

class list_ extends sqlite_db{
    
    public $country;

    public function __construct($country) {
        parent::__construct();
        $this->country = $country;
    }
    
    public function create_list_tb() {

        $create_tables = "CREATE TABLE IF NOT EXISTS list_tb (
                            list_id               TEXT,
                            slug                  TEXT NOT NULL, 
                            owner_screen_name     TEXT NOT NULL,
                            user_id               TEXT NOT NULL,
                            screen_name           TEXT NOT NULL,
                            country               TEXT,
                            PRIMARY KEY (list_id, country));";
        $this->file_db->exec($create_tables);
    }
}




/*
  $str11 = <<<EOT
  My name is "$name". I am printing some
  Now, I am printing some
  This should print a capital 'A': \x41
  EOT;
 */
?>