<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Carbon\Carbon;
use App\MyApp\UserTimeline;

class UserTimelineTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function __test_traverse_usertimeline()
    {      
        Cache::flush();
        $userTimeline = new UserTimeline('serbia1');
        
        $now = Carbon::now(); 
        $todayMidnight = Carbon::today();
        $yesterdayMidnight = Carbon::yesterday();
        
        $result = $userTimeline->traverse_usertimeline('zareseizigro', $todayMidnight, $yesterdayMidnight, 50);
        $this->assertTrue(!$result['traverse_usertimeline']['warning']);
    }
    
    
    public function test_top_10_from_midnight_for_collection() {
        Cache::flush();
        $userTimeline = new UserTimeline('serbia1');
        $result = $userTimeline->top_10_from_midnight_for_collection('zareseizigro');
    }
    
    public function __test_top_3_yesterdays_for_fb() {
        Cache::flush();
        $userTimeline = new UserTimeline('serbia1');
        $result = $userTimeline->top_3_yesterdays_for_fb('zareseizigro');
    }
    
    public function __test_top_1_of_last_run_for_fb() {
        Cache::flush();
        $userTimeline = new UserTimeline('serbia1');
        //treba cache arr, mock cache
        $result = $userTimeline->top_1_of_last_run_for_fb(10);
    }
    
    public function testBasicExample()
	{
		$this->assertTrue(true);
	}
    
}
