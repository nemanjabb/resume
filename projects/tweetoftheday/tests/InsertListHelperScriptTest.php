<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Carbon\Carbon;
use App\MyApp\InsertListHelperScript;

/*
 * what run configuration do you use while developing if application has no views and http routes, invoking from tests seems convenient but it intercepts exceptions? you define artisan command and set that in run configuration?
 */

/*
 * //dobar
 * https://laravel.com/docs/5.1/artisan#calling-commands-via-code
 * treba da se naprave artisan komande i to pozivas gde treba
 * i jedna za testiranje
 * i jedna za listu ubacivanje
 * list i user idu u queue
 * 
 */

class InsertListHelperScriptTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function __testExample()
    { 
        $o = new InsertListHelperScript();
        $o->insert();
        $this->assertTrue(true);
    }
}
