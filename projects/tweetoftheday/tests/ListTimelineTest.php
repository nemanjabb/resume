<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


use App\MyApp\TwApi;
use Carbon\Carbon;
use App\MyApp\LogHelper;
use App\MyApp\ListTimeline;

class ListTimelineTest extends TestCase
{

    public function __rt_listtimeline_more_than_0_results($top_h, $bottom_h)
    {
        $listTimeline = new ListTimeline('serbia1');
        $listTimeline->config['list_timeline']['top_h'] = $top_h;
        $listTimeline->config['list_timeline']['bottom_h'] = $bottom_h;
        
        $result = $listTimeline->rt_listtimeline();
        return $result;
    }
    
    public function __test_0_1_hours()
    {
        Cache::flush();
        $result = $this->rt_listtimeline_more_than_0_results(1, 2);
        //$result = $this->rt_listtimeline_more_than_0_results(0, 3);
        assertTrue(count($result['all_results']) > 0);
    }
    
    //2 consecutive calls
    public function __test_2_successive_calls()
    {
        Cache::flush();
        //ako prvi vrati 0, drugi se ponasa kao prvi, izlazi na vreme
        //to je test za uslov        
        $result1 = $this->rt_listtimeline_more_than_0_results(2, 3);
        $result2 = $this->rt_listtimeline_more_than_0_results(1, 2);
        assertTrue(true);
    }
    
    //debuging...
    public function __test2twapisinceid()
    {
        $config = MyConfig::get('serbia1');
        $list = ['list_id'=> null, 'slug' => $config['profile']['slug'], 'list_owner_screen_name' => $config['profile']['list_owner_screen_name']];   
        
        $max_id = '775666348659736576';//575
        $since_id = '775665950892912641';       
        
        //$max_id = '775681722235125761';//ne postoji//sto sam oduzeo onaj -1 za incl max id
        //$since_id = "775681697803296768"; 
        
        $response = TwApi::get_list_tweets($since_id, $max_id, 200, $list); 

         dd($response);   
    }
    public function testBasicExample()
	{
		$this->assertTrue(true);
	}

}
     /*
    assertTrue()
    assertFalse()
    assertEquals()
    assertNull()
    assertContains()
    assertCount()
    assertEmpty()
     */