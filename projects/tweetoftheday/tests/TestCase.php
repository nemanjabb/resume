<?php

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }
}

trait setUpTrait {
    
    
    /*
     * setUp Monolog, Twitter, Cache driver
     * 
     */
    public function setUp() 
    {
        //setUp Monolog, Twitter
        $ep = EntryPoint::getInstance();
        $ep->reconfigAll($testing_country);
        
        //setup db cache driver and mockup
        
    }
}