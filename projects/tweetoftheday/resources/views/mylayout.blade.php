<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Tweet of the day</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="Description" lang="en" content="ADD SITE DESCRIPTION">
		<meta name="author" content="ADD AUTHOR INFORMATION">
		<meta name="robots" content="index, follow">

		<!-- icons -->
		<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
		<link rel="shortcut icon" href="favicon.ico">
        
                
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">-->
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" integrity="sha384-VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>-->
                
        <!--libs-->
        <script type="application/javascript" src="<% elixir('js/libs.js') %>"></script>
        <link rel="stylesheet" href="/css/bootstrap.css"/>
        
        <!-- my files-->
        <script type="application/javascript" src="<% elixir('js/page2.js') %>"></script>        
        <link rel="stylesheet" href="<% elixir('css/mycss.css') %>"/>

	</head>
	<body  ng-app="myApp" ng-controller="MainController" >

		<!-- Navigation -->
	    
        <nav style="" class="navbar navbar-fixed-top navbar-inverse margin-0" role="navigation">
			<div class="container-fluid">

				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
<!--					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>-->
					<a class="navbar-brand" href="#">Tweet of the day</a>
				</div>
				<!-- /.navbar-header -->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
<!--					<li><a href="#">Nav item 1</a></li>
						<li><a href="#">Nav item 2</a></li>
						<li><a href="#">Nav item 3</a></li>-->
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<!-- /.navbar -->

		<!-- Page Content -->
		<div class="container-fluid" style="height:100%;">

			<!-- /.row -->

			<div class="row my-row">
                
                <div class="col-sm-4 col-lg-3 col-lg-push-9 hk-right">                    
                    <div class="panel panel-default">
						<div class="panel-heading">
                            <h4 class="panel-title">Countries</h4>
                        </div>

                        <div ng-repeat="item in countries" class="list-group margin-b-3">
                            <a href="#" ng-class="{'active': $first}" ng-click="selectCountry($event, item)"   data-collection-id="{{ item.collection_url }}" data-country="{{item.country}}" class="list-group-item hk-country"><span class="flag-icon {{item.flags}} my-flag-span"></span>{{item.country_name}}</a>                        
                        </div>
                         
                    </div>
				</div>
                
                <!--<div class="clearfix visible-sm"></div>-->
                <div class="col-sm-4 col-lg-3 col-lg-pull-3 hk-left">                                       
                	<!-- Panel -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">Today's prominent users</h4>
						</div>
<!--						<div class="panel-body">
							<p>Lista avatari itd. Moze prva tri, zlato, srebro, bronza.Kratak opis, lozenje slava, podaci o njemu, promocija njegova. Opcija da uhvati print screen sa datumom i tvitom kad je bio zlatan.</p>
						</div>-->
                        <!-- twprofilecards -->
                        <!-- code start -->
                        <div ng-repeat="item in top3Users track by $index" class="panel-body">
                        <div class="twPc-div">
                            <a class="twPc-bg twPc-block" style="background-image: url({{ item.profile_banner_url }}/600x200)"></a>

                            <div>
                                <!--
                                <div class="twPc-button">
                                    <!-- Twitter Button | you can get from: https://about.twitter.com/tr/resources/buttons#follow ->
                                    <a href="https://twitter.com/mertskaplan" class="twitter-follow-button" data-show-count="false" data-size="large" data-show-screen-name="false" data-dnt="true">Follow @mertskaplan</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                    <!-- Twitter Button ->   
                                </div>
                                -->
                                <a title="{{ item.name }}" href="https://twitter.com/{{ item.screen_name }}" class="twPc-avatarLink">
                                    <img alt="{{ item.name }}" ng-src="{{ item.profile_image_url }}" class="twPc-avatarImg">
                                </a>

                                <div class="twPc-divUser">
                                    <div class="twPc-divName elipsis1">
                                        <a class="" href="https://twitter.com/{{ item.screen_name }}">{{ item.name }}</a>
                                    </div>
                                    <span>
                                        <a href="https://twitter.com/{{ item.screen_name }}">@<span>{{ item.screen_name }}</span></a>
                                    </span>
                                </div>

                                <div class="twPc-divStats">
                                    <ul class="twPc-Arrange">
                                        <li class="twPc-ArrangeSizeFit">
                                            <a href="https://twitter.com/{{ item.screen_name }}" title="{{ item.statuses_count }} Tweet">
                                                <span class="twPc-StatLabel twPc-block">Tweets</span>
                                                <span class="twPc-StatValue">{{ item.statuses_count }}</span>
                                            </a>
                                        </li>
                                        <li class="twPc-ArrangeSizeFit">
                                            <a href="https://twitter.com/{{ item.screen_name }}/following" title="{{ item.friends_count }} Following">
                                                <span class="twPc-StatLabel twPc-block">Following</span>
                                                <span class="twPc-StatValue">{{ item.friends_count }}</span>
                                            </a>
                                        </li>
                                        <li class="twPc-ArrangeSizeFit">
                                            <a href="https://twitter.com/{{ item.screen_name }}/followers" title="{{ item.followers_count }} Followers">
                                                <span class="twPc-StatLabel twPc-block">Followers</span>
                                                <span class="twPc-StatValue">{{ item.followers_count }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>                       
                        </div>
                        <!-- code end -->
                        </div>
					</div> 
                
                <div class="col-sm-8 col-lg-6 col-lg-pull-3 scroll-parent hk-center">
                    <div class="scroll-child"> 
                        <div id="twitter-timeline-container">
                            <!--<twitter-timeline="https://twitter.com/serbia1acc/timelines/831630017813684224"/> {{countries[0].collection_url}}-->
                            <!--<a twitter-timeline="{{countries[0].collection_url}}"  auto-resize="true"  data-tweet-limit="10">Loading tweets...</a>-->                 

                        <twitter-timeline id="tw-timeline"  twitter-timeline-type="collection"
                                          twitter-timeline-id=selectedTimeline
                                          twitter-timeline-options='{"chrome": "noheader"}'>
                        </twitter-timeline>


                     
                        </div>
                    </div>
                </div>                
			</div>
            </div>
			<!-- /.row -->

			<!--<hr>-->
			<!--
            <footer class="margin-tb-3">
				<div class="row">
					<div class="col-lg-12">
						<p>Copyright &copy; Sitename 2015</p>
					</div>
				</div>
			</footer>
            -->
		</div>
		<!-- /.container-fluid -->
        <script>
           
        $(window).scroll(function() {
            var left = $('.hk-left').height();
            var center = $('.hk-center').height();
            var width = $(window).innerWidth();
            
            if(left > center) {
              $('.hk-center').height(left+10);  
            }
            //console.log(left + 'levoooooooo');
        });
        
        $(window).resize(function() {
            resizeCenterColumn();           
        })
        
        resizeCenterColumn();
         
        function resizeCenterColumn() {
            var width = $(window).innerWidth();
            var height = $(window).innerHeight();
            if (width < 1180 && width>=750){
                var left = $('.hk-left').height();
                var right = $('.hk-right').height();
                if ( (left+right) > height) {
                    $('.hk-center').height(left+right);
                } else {
                    $('.hk-center').height(height-60);
                }
            } else if (width >= 1180) {               
                $('.hk-center').height(height-60);
            }
            //console.log(width);
        }   
            
        //768px) and (max-width : 1200px)    
        /*
        var width = $(window).innerWidth();
        if (width < 992 && width>=768){
           $('.hk-center').insertBefore('.hk-left');        
        }else if(width<768 || width>=992)
        {
           $('.hk-left').insertBefore('.hk-center');        
        }
        
        $(window).resize(function() {
            var width = $(window).innerWidth();
            if (width < 1200 && width>=750){
                $('.hk-center').insertBefore($('.hk-left'));  
            }else if(width<750 || width>=1200)
            {
                $('.hk-left').insertBefore($('.hk-center'));   
            }
            console.log(width);
        })
        */
        </script>
  </body>
</html>


<!--<div class="container text-center">  
	<div class="row">   	
    <div class="col-sm-4"> 
      <div class="row">      
        <div class="col-sm-12"> 
          <div class="alert alert-info">B</div>
        </div> 
        <div class="col-sm-12">  
			     <div class="alert alert-warning">A</div> 
		    </div>  
      </div>   
    </div>  
    <div class="col-sm-8">
        <div class="alert alert-danger">C<br><br><br><br></div>
    </div>  		
	</div>
</div>-->
