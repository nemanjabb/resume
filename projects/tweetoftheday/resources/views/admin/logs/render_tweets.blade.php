
<table class="table-condensed">
<?php $i1 = 0; ?>
@foreach ($tweets as $tw)                      
    @if ($i1==0)           
        <thead><tr>            
        @foreach ($tw as $key => $value)
            @if ($key!='tw_link' && $key!='user_link')
                <th><% $key %></th>
            @endif
        @endforeach
        </tr></thead><tbody>           
    @endif                      
    <tr>
    @foreach ($tw as $key => $value)
        @if ($key!='tw_link' && $key!='user_link')
            <td>
                @if ($key=='name')
                    <a href=<% $tw["user_link"] %> target="_blank" ><% $value %></a>
                @elseif ($key=='created_at')
                    <a href=<% $tw["tw_link"] %> target="_blank" ><% $value %></a>
                @else
                    <%% $value %%>
                @endif
            </td>
        @endif
    @endforeach                          
    </tr>                
    <?php $i1++; ?>
@endforeach 
<tbody></table>

