
@foreach ($head as $p)
    <div <%% isset($p['class']) ? 'class="'.$p['class'].'"' : '' %%> >
    @if( isset($p['value']) )
        <?php $i1 = 0; $i2 = count($p['value']); ?>
        @foreach ($p['value'] as $key1 => $value1)
            @if ($i1==0)
                <span class="bold_span"><%% $value1 %%>:: </span>
            @else
                <span><%% $key1 %%>: <%% $value1 %%><% ($i1 != $i2-1) ? ', ' : '' %></span>
            @endif
            <?php $i1++; ?>
        @endforeach 
    @endif
    </div>
@endforeach
    
    
 