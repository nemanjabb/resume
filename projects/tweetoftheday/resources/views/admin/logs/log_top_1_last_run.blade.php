<div class="item-div">     
    <p><strong>fja:::top_1_of_last_run </strong></p>           
    <p><strong>razlog izlaska:: </strong> <%% $d_r["debug"]["izlaz"] %%></p>
    <p <% $d_r["warning"] ? 'class=bg-danger' : '' %> ><strong>status:: </strong>upozorenje: <% var_export($d_r["warning"], true) %>, api_screenshot: <% $d_r["debug"]["api_screenshot"] %>, api_posted_on_fb: <% $d_r["debug"]["api_posted_on_fb"] %></p>        
    <p><strong>rezultati:: </strong>top_n: <% count($d_r["top_n"]) %></p>
    <div class="h-divider"></div> 

    <p><strong>top_n:</strong></p>
    <table class="table-condensed">
        <?php $i1 = 0; ?>
        @foreach ($d_r["top_n"] as $tw) 
            <tr>           
            @if ($i1==0)
                @foreach ($tw as $key => $value)
                @if ($key!='tw_link' && $key!='user_link')
                    <th><% $key %></th>
                @endif
                @endforeach 
            </tr><tr>
            @endif 
            @foreach ($tw as $key => $value)
            @if ($key!='tw_link' && $key!='user_link')
                <td>
                    @if ($key=='name')
                    <a href=<% $tw["user_link"] %> target="_blank" ><% $value %></a>
                    @elseif ($key=='created_at')
                    <a href=<% $tw["tw_link"] %> target="_blank" ><% $value %></a>
                    @else
                    <%% $value %%>
                    @endif
                </td>
            @endif
            @endforeach                          
            <?php $i1++; ?>
            </tr>
        @endforeach 
    </table>   
</div>

