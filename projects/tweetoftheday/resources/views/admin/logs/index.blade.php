<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Logs</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- icons -->
		<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
		<link rel="shortcut icon" href="favicon.ico">

        <!--libs-->
        <script type="application/javascript" src="<% elixir('js/libs.js') %>"></script>
        <link rel="stylesheet" href="/css/bootstrap.css"/>

        <style type="text/css">
            html {
                font-size: 14px;
              }
            body {
                padding-top: 35px; 
            }
            .h-divider {
                margin-top:5px;
                margin-bottom:5px;
                height:1px;
                width:100%;
                border-top:1px solid gray;
            }
            .h-divider-thick {
                margin-top:5px;
                margin-bottom:5px;
                height:10px;
                width:100%;
                border-top:3px solid gray;
                border-bottom:3px solid gray;
            }
            .item-div {
                padding: 0px 5px 0px 5px;
            }
            p {
                margin:0px;
            }
            .table-condensed  td, .table-condensed  th {
                padding: 0px 0px 0px 7px !important;
            }
            .bg-danger{
                background-color: red;
            }
            .bg-warning{
                background-color: #ff6e14;
            }
            .bg-top_10_for_collection {
                 background-color: #F5FFF7;
            }
            .a-countries {
                margin-right: 20px;
            }
            .center {
                text-align: center;
            }
            .navbar-nav > li > a {padding-top:5px !important; padding-bottom:5px !important;}
            .navbar {min-height:32px !important}
            
        </style>
	</head>
    <body class="text-left text-nowrap">
        
        <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <ul class="nav navbar-nav">
            @foreach ($channels as $ch)
                <li><a href="<% route('log.show', $ch) %>"><% $ch %></a></li>
            @endforeach
          </ul>
        </div>
      </nav>
     
        @foreach ($logs as $log)
            <%% $log->message %%>
        @endforeach
    </body>
</html>

