
@if ($d_r[1]["header"] == 'top_10_for_collection')
    <div class="item-div bg-top_10_for_collection"> 
    @yield('top_10_for_collection')  
@elseif ($d_r[1]["header"] == 'top_3_yesterdays_for_fb')
    <div class="item-div bg-top_3_yesterdays_for_fb"> 
    @yield('top_3_yesterdays_for_fb') 
@else
    <div class="item-div">
@endif



@if ($d_r[1]["has_items"])
    @section('traverse_timeline')
        <p><strong>traverse:::</strong></p>
        @if ($d_r[0]["tr_info"])   
        <p><strong> args::</strong> from_date: <% $d_r[0]["debug"]["from_date"] %>, from_time_tw: <% $d_r[0]["debug"]["from_time_tw"] %>, to_date: <% $d_r[0]["debug"]["to_date"] %>, top_n: <% $d_r[0]["debug"]["top_n"] %></p>
        <p><strong>razlog izlaska:: </strong><%% $d_r[0]["debug"]["izlaz"] %%></p>
        <p <% $d_r[0]["warning"] ? 'class=bg-danger' : '' %> ><strong>status:: </strong>traverse upozorenje: <% var_export($d_r[0]["warning"], true) %> </p>        
        <p><strong>rezultati:: </strong>ukupno tvitova: <% count($d_r[0]["all_results"]) %>, top_n_tweets: <% count($d_r[0]["top_n_tweets"]) %>, br iteracija: <% count($d_r[0]["debug"]["po_iteracijama"]) %>, iteracije: 
            @foreach ($d_r[0]["debug"]["po_iteracijama"] as $iter)
                iteracija<% $iter["iteracija"] %>=<% $iter["broj_rezultata"] %>, 
            @endforeach
        @endif
        </p>
        <p><strong>gornji i donji:: </strong> now: <% $d_r[0]["debug"]["now"] %>, first_tw: <% $d_r[0]["debug"]["first_tw"] %>, last_tw: <% $d_r[0]["debug"]["last_tw"] %>, first_id_str: <% $d_r[0]["debug"]["first_tw_id_str"] %>, last_id_str: <% $d_r[0]["debug"]["last_tw_id_str"] %></p>
        @if ($d_r[0]["top_n"])
            <div class="h-divider"></div> 
            <p><strong>top_n_tweets:</strong></p>            
            <table class="table-condensed">
            <?php $i1 = 0; ?>
            @foreach ($d_r[0]["top_n_tweets"] as $tw) 
                <tr>           
                @if ($i1==0)
                    @foreach ($tw as $key => $value)
                    @if ($key!='tw_link' && $key!='user_link')
                        <th><% $key %></th>
                    @endif
                    @endforeach 
                </tr><tr>
                @endif 
                @foreach ($tw as $key => $value)
                @if ($key!='tw_link' && $key!='user_link')
                    <td>
                        @if ($key=='name')
                        <a href=<% $tw["user_link"] %> target="_blank" ><% $value %></a>
                        @elseif ($key=='created_at')
                        <a href=<% $tw["tw_link"] %> target="_blank" ><% $value %></a>
                        @else
                        <%% $value %%>
                        @endif
                    </td>
                @endif
                @endforeach                          
                <?php $i1++; ?>
                </tr>
            @endforeach 
            </table>   
        @endif

        @if ($d_r[0]["all"])
            <div class="h-divider"></div> 
            <p><strong>rto-top_n_tweets:</strong></p>            
            <table class="table-condensed">
            <?php $i1 = 0; ?>
            @foreach ($d_r[0]["all_results"] as $tw) 
                <tr>           
                @if ($i1==0)
                    @foreach ($tw as $key => $value)
                    @if ($key!='tw_link' && $key!='user_link')
                        <th><% $key %></th>
                    @endif 
                    @endforeach 
                </tr><tr>
                @endif 
                @foreach ($tw as $key => $value)
                @if ($key!='tw_link' && $key!='user_link')
                    <td>
                        @if ($key=='name')
                        <a href=<% $tw["user_link"] %> target="_blank" ><% $value %></a>
                        @elseif ($key=='created_at')
                        <a href=<% $tw["tw_link"] %> target="_blank" ><% $value %></a>
                        @else
                        <%% $value %%>
                        @endif
                    </td>
                @endif
                @endforeach                          
                <?php $i1++; ?>
                </tr>
            @endforeach 
            </table>              
        @endif
  
    @show
@endif

</div>
