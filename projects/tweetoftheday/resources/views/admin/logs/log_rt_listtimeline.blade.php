
<div class="item-div" >
    <p><strong>fja:::rt_listtimeline </strong></p>
    <strong>args::</strong> <mark>top_h: </mark><% $d_r["debug"]["args"]["top_h"] %>, <mark>bottom_h: </mark><% $d_r["debug"]["args"]["bottom_h"] %>, <mark>from time (top_h): </mark><% $d_r["debug"]["args"]["time_h_m"] %></p>        
    <p <% $d_r["warning"] ? 'class=bg-danger' : '' %> > <strong>status:: </strong><mark>first_run: </mark><% var_export($d_r["debug"]["first_run"], true) %>, <mark>upozorenje: </mark><% var_export($d_r["warning"], true) %> </p>
    <p><strong>razlog izlaska:: </strong><%% $d_r["debug"]["izlaz"] %%></p>
    <div class="h-divider"></div>
    <p><strong>rezultati:: </strong><mark>ukupno tvitova: </mark><% count($d_r["all_results"]) %>, <mark>rt-vanih: </mark><% count($d_r["rt_ed"]) %>, <mark>br iteracija: </mark><% count($d_r["debug"]["po_iteracijama"]) %>, <mark>iteracije: </mark> 
    @foreach ($d_r["debug"]["po_iteracijama"] as $iter)
        iteracija<% $iter["iteracija"] %>=<% $iter["broj_rezultata"] %>,  
    @endforeach
    </p>
    <p> <mark>now: </mark><% $d_r["debug"]["now"] %>, <mark>first_tw: </mark><% $d_r["debug"]["first_tw"] %>, <mark>last_tw: </mark><% $d_r["debug"]["last_tw"] %>, <mark>first_id_str: </mark><% $d_r["debug"]["first_tw_id_str"] %>, <mark>last_id_str: </mark><% $d_r["debug"]["last_tw_id_str"] %></p>

    @if ($d_r["rt"])
        <div class="h-divider"></div>
        <p><strong>rto-vani:</strong></p>
        <table class="table-condensed">
        <?php $i1 = 0; ?>
        @foreach ($d_r["rt_ed"] as $tw) 
            <tr>           
            @if ($i1==0)
                @foreach ($tw as $key => $value)
                @if ($key!='tw_link' && $key!='user_link')
                    <th><% $key %></th>
                @endif
                @endforeach 
            </tr><tr>
            @endif 
            @foreach ($tw as $key => $value)
            @if ($key!='tw_link' && $key!='user_link')
                <td>
                    @if ($key=='name')
                    <a href=<% $tw["user_link"] %> target="_blank" ><% $value %></a>
                    @elseif ($key=='created_at')
                    <a href=<% $tw["tw_link"] %> target="_blank" ><% $value %></a>
                    @else
                    <%% $value %%>
                    @endif
                </td>
            @endif
            @endforeach                          
            <?php $i1++; ?>
            </tr>
        @endforeach 
        </table>
    @endif

    @if ($d_r["all"])
    <div class="h-divider"></div>
    <p><strong>all: </strong></p>
        <table class="table-condensed">
        <?php $i1 = 0; ?>
        @foreach ($d_r["all_results"] as $tw) 
            <tr>           
            @if ($i1==0)
                @foreach ($tw as $key => $value)
                @if ($key!='tw_link' && $key!='user_link')
                    <th><% $key %></th>
                @endif
                @endforeach 
            </tr><tr>
            @endif 
            @foreach ($tw as $key => $value)
            @if ($key!='tw_link' && $key!='user_link')
                <td>
                    @if ($key=='name')
                    <a href=<% $tw["user_link"] %> target="_blank" ><% $value %></a>
                    @elseif ($key=='created_at')
                    <a href=<% $tw["tw_link"] %> target="_blank" ><% $value %></a>
                    @else
                    <%% $value %%>
                    @endif
                </td>
            @endif
            @endforeach                          
            <?php $i1++; ?>
            </tr>
        @endforeach 
        </table>
    @endif
</div>

