<div class="col-sm-4 col-lg-3 col-lg-push-9">                    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Countries</h4>
        </div>
        <div class="list-group margin-b-3">
            <a href="#" data-collection-id="<% $collection_urls[0] %>" data-country="serbia1" class="active list-group-item hk-country"><span class="flag-icon flag-icon-rs my-flag-span"></span>  Serbia</a>
            <a href="#" data-collection-id="<% $collection_urls[0] %>" data-country="usa1" class="list-group-item hk-country"><span class="flag-icon flag-icon-de my-flag-span"></span> Germany</a>
            <a href="#" data-collection-id="https://twitter.com/SuncicaKaric/timelines/661681825044033537"  data-country="uk1" class="list-group-item hk-country"><span class="flag-icon flag-icon-fr my-flag-span"></span> France</a>
        </div> 
    </div>
</div>
