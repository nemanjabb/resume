<?php $i1 = 1; ?>
@foreach ($users as $user)
<div class="panel-body-<% $i1 %>">
<!-- code start -->
<div class="twPc-div">
    <a class="twPc-bg twPc-block" style="background-image: url(<% $user['profile_banner_url'] %>/600x200)"></a>

	<div>
        <!--
		<div class="twPc-button">
            <!-- Twitter Button | you can get from: https://about.twitter.com/tr/resources/buttons#follow ->
            <a href="https://twitter.com/mertskaplan" class="twitter-follow-button" data-show-count="false" data-size="large" data-show-screen-name="false" data-dnt="true">Follow @mertskaplan</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            <!-- Twitter Button ->   
		</div>
        -->
		<a title="<% $user['name'] %>" href="https://twitter.com/<% $user['screen_name'] %>" class="twPc-avatarLink">
			<img alt="<% $user['name'] %>" src="<% $user['profile_image_url'] %>" class="twPc-avatarImg">
		</a>

		<div class="twPc-divUser">
			<div class="twPc-divName elipsis1">
				<a class="" href="https://twitter.com/<% $user['screen_name'] %>"><% $user['name'] %></a>
			</div>
			<span>
				<a href="https://twitter.com/<% $user['screen_name'] %>">@<span><% $user['screen_name'] %></span></a>
			</span>
		</div>

		<div class="twPc-divStats">
			<ul class="twPc-Arrange">
				<li class="twPc-ArrangeSizeFit">
					<a href="https://twitter.com/<% $user['screen_name'] %>" title="<% $user['statuses_count'] %> Tweet">
						<span class="twPc-StatLabel twPc-block">Tweets</span>
						<span class="twPc-StatValue"><% $user['statuses_count'] %></span>
					</a>
				</li>
				<li class="twPc-ArrangeSizeFit">
					<a href="https://twitter.com/<% $user['screen_name'] %>/following" title="<% $user['friends_count'] %> Following">
						<span class="twPc-StatLabel twPc-block">Following</span>
						<span class="twPc-StatValue"><% $user['friends_count'] %></span>
					</a>
				</li>
				<li class="twPc-ArrangeSizeFit">
					<a href="https://twitter.com/<% $user['screen_name'] %>/followers" title="<% $user['followers_count'] %> Followers">
						<span class="twPc-StatLabel twPc-block">Followers</span>
						<span class="twPc-StatValue"><% $user['followers_count'] %></span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- code end -->
</div>
<?php $i1++; ?>
@endforeach 

