<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MyApp\UserTimeline;
use App\MyApp\EntryPoint;

class RunUserTimeline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usertimeline:run {country} {functionname} {screen_name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run functions from UserTimeline class, <country> <functionname> <screen_name>';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $country = $this->argument('country'); 
        $fname = $this->argument('functionname'); 
        $screen_name = $this->argument('screen_name'); 
        
        //reconfig
        $ep = EntryPoint::getInstance();
        $ep->reconfigAll($country);
        
        $userTimeline = new UserTimeline();
        $result = null;
        
            switch (true)
            {
                //queue
                case $fname == 'top_10_from_midnight_for_collection' :
                $result = $userTimeline->top_10_from_midnight_for_collection($screen_name);                   
                break;
            
                case $fname == 'top_1_of_last_run_for_fb' :
                $result = $userTimeline->top_1_of_last_run_for_fb();                   
                break;   
                
                //17:45
                case $fname == 'top_3_yesterdays_for_fb' :
                $result = $userTimeline->top_3_yesterdays_for_fb($screen_name);                   
                break;
            }
        
    }
}
