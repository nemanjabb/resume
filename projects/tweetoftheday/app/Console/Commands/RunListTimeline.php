<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MyApp\ListTimeline;
use App\MyApp\EntryPoint;

class RunListTimeline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listtimeline:run {country}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run rt_listitmeline() function from ListTimeline class.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * Entry point
     * 
     * @return mixed
     */
    public function handle()
    {
        $country = $this->argument('country');  
        
        //reconfig
        $ep = EntryPoint::getInstance();
        $ep->reconfigAll($country);
        
        $listTimeline = new ListTimeline();
        $result = $listTimeline->rt_listtimeline();
        $this->info('listtimeline:run success');
    }
}
