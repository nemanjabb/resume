<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

//use App\MyApp\Config\MyConfig;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,               
        Commands\RunListTimeline::class,        
        Commands\RunUserTimeline::class,
        Commands\MyRun::class//,
        //Commands\SchedulerDaemon::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
	protected function schedule(Schedule $schedule)
	{
		//$schedule->command('inspire')->cron('* * * * * *');
        
        /*
         * 0 je 0, * je svaki
         * https://crontab.guru/#10_*_*_*_*
         */

 //------------- serbia1--------------------------
        /*
         * niz listu
         * svakog sata u 12:15
         * u queue ovde
         */
//        $schedule->command('listtimeline:run serbia1')   
//                 ->cron('15 * * * * *')->timezone('Europe/Belgrade'); //12:15

        /*
         * 5 minuta posle listtimeline
         */          
//        $schedule->command('usertimeline:run serbia1 top_10_from_midnight_for_collection')
//                  ->cron('20 * * * * *')->timezone('Europe/Belgrade');

        /*
         * tvit jucerasnjeg dana
         * jednom dnevno u 17:30
         */
        //$schedule->command('usertimeline:run '.$profile['country'].' top_3_yesterdays_for_fb')
        //         ->dailyAt('17:30')->timezone($profile['timezone']);
        //->command('usertimeline:run '.$profile['country'].' top_1_of_last_run_for_fb')
	}

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
