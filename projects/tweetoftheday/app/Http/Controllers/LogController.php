<?php namespace App\Http\Controllers;

//use Illuminate\Http\Request;
//use Todo\Http\Requests;
//use Todo\Http\Controllers\Controller;
use Stevebauman\LogReader\Models\Log;

class LogController extends Controller
{
    /**
     * Displays all log entries. 
     *
     * @return \Illuminate\View\View
     */
    public function index($country='serbia1')
    {        
        $channels = Log::select('channel')->distinct()->pluck('channel');
        $logs = Log::where('channel', $country)->orderBy('id', 'desc')->get();
        return view('admin.logs.index', ['logs' => $logs])->with(['channels' => $channels]);
    }

}
