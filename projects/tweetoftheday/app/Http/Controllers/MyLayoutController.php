<?php namespace App\Http\Controllers;

use Cache;
use MyConfig;
use View;

class MyLayoutController extends Controller
{
    
    public function getTop3Users($country)
    {   
//        Cache::shouldReceive('get')->once()->with($country.'_top_3_users_arr')->andReturn(['1', '2', '3']);
//        $rt_ed_ids_arr = Cache::get($country.'_top_3_users_arr');
        
//        $countries = MyConfig::getActiveCountries();
        //prvi default za inicijalno
        //get profilecards
        $twpc = '';
        $top_3_users = Cache::get($country. '_top_3_users_arr');
        if(count($top_3_users) == 3)
        {
            $twpc = $top_3_users = self::formatUsers($top_3_users);//dd($top_3_users);      
//            $twpc = View::make('twprofilecard', ['users' => $top_3_users])->render();
        } else {
            //nista ako nema 3
            //$twpc = var_export($top_3_users, true);
        }
        
        return $twpc;//json umesto View::      
    }
    
    
    
    /**
     * Displays all log entries. 
     *
     * @return \Illuminate\View\View
     */
    public function countries()
    {      
        $countries = MyConfig::getActiveCountries();

        
        /*
         *
         * data-country="serbia1"
         * flag-icon flag-icon-rs
         * collection_id
         * screen_name
         */
        
        //mora setCountry
        $result = [];
        $result1 = [];
        foreach ($countries as $country) {
            MyConfig::setCountry($country['country']);
            
            $screen_name = MyConfig::getCountry($country)['profile']['screen_name'];
            $collection_id = MyConfig::getCountry($country)['profile']['collection_id'];
            
            $result['collection_url'] = 'https://twitter.com/'.$screen_name.'/timelines/'.$collection_id;
            $result['flags'] = MyConfig::getCountry($country)['profile']['flags'];
            $result['country'] = $country['country'];            
            $result['country_name'] = $country['country_name'];

            $result1[] = $result;

        }
        return $result1;
//        return view('mylayout', ['countries' => $countries, 'collection_urls' => $collection_urls]);


    }

 
    
    private static function formatUsers($users) {
        return array_map(function($user){
            $user['followers_count'] = self::thousand_to_K($user['followers_count']);
            $user['friends_count'] = self::thousand_to_K($user['friends_count']);
            $user['statuses_count'] = self::thousand_to_K($user['statuses_count']);
            return $user;
        }, $users);    
    }
    
    private static function thousand_to_K($value) {              
        $floorp = function($val, $precision)
        {
            $mult = pow(10, $precision);
            return floor($val * $mult) / $mult;
        };
        if ($value > 9999 && $value <= 999999) {
            $result = $floorp($value / 1000, 0).'K';
        }elseif ($value > 999 && $value <= 9999) {
            $result = $floorp($value / 1000, 3);
        } elseif ($value > 999999) {
            $result = $floorp($value / 1000000, 0).'M';
        } else {
            $result = $value;
        }
        return $result;
    }

}
