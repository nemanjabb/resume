<?php

return  [
    
    0 => [
        'country' => 'serbia1',
        'country_name' => 'Serbia',
        'active'  => true,
        'profile' => [
            //user
            'screen_name'       => 'totd_RS',
            'user_id'           => '778671055745540100',

            //lista
            'list_id'                => '130874743',	            
            'slug'                   => 'test1',
            'list_owner_screen_name' => 'totd_RS',
            'list_user_id' 			 => '587755891',
            'collection_id'          => '831630017813684224',   //https://twitter.com/totd_RS/timelines/831630017813684224
            
            'flags'                  => 'flag-icon-rs',
             
            //facebook
            'app_id'                     => '', 
            'app_secret'                 => '',
            'page_id'                    => '',
            'extended_page_access_token' => '',


            'country' 			=> 'serbia1',
            'timezone'          => 'Europe/Belgrade',
            'keys1234' 			=> ',,,',
        ],
        'list_timeline' => [          
            //slide list config, must be optional
            'top_h'         => 0,                                                  
            'bottom_h'      => 1, 
            'count'         => 200,                                                 //oko 180 vadi
            'fav_count'     => 50, 
            'lang'          => 'sr',
            'for_i'         => 5,  
            'format'        => "H:i:s d-m-Y",
        ],
        'user_timeline' => [            
            //slide user optional config
            'count'         => 200,  
            'for_i'         => 5
            ],
        'banned_users' => [  
            ['user_id' => '335419599', 'screen_name' => 'nichimizazvan', 'description' => 'banned...'], 
            ['user_id' => '874010389', 'screen_name' => 'raspevana', 'description' => 'banned...']
        ], 
          
    ],
        //----------------- usa1 ---------------------
    1 => [
        'active'  => true,
        'country' => 'usa1',        
        'country_name' => 'USA',
        'profile' => [
            //user
            'screen_name'       => 'totd_us',
            'user_id'           => '1087404745575354368',

            //lista
            'list_id'                => '1087412684419674112',	            
            'slug'                   => 'test1',
            'list_owner_screen_name' => 'totd_us',
            'list_user_id' 			 => '1087404745575354368',
            'collection_id'          => '1087415704146587649',   //https://twitter.com/totd_us/timelines/1087415704146587649
            
            'flags'                  => 'flag-icon-us',
             
            //facebook
            'app_id'                     => '', 
            'app_secret'                 => '',
            'page_id'                    => '',
            'extended_page_access_token' => '',
 
            'country' 			=> 'usa1',
            'timezone'          => 'America/New_York',
            'keys1234' 			=> ',,,',
        ],
        'list_timeline' => [          
            //slide list config, must be optional
            'top_h'         => 0,                                                  
            'bottom_h'      => 1, 
            'count'         => 200,                                                
            'fav_count'     => 300, 
            'lang'          => 'en',
            'for_i'         => 5,  
            'format'        => "H:i:s d-m-Y",
        ],
        'user_timeline' => [            
            //slide user optional config
            'count'         => 200,  
            'for_i'         => 5
            ],
        'banned_users' => [  
            ['user_id' => '', 'screen_name' => '', 'description' => 'banned...']
        ], 
    ],
    //----------------- germany1 ---------------------
    2 => [
        'active'  => true,
        'country' => 'germany1',        
        'country_name' => 'Germany',
        'profile' => [
            //user
            'screen_name'       => 'totd_de',
            'user_id'           => '925217292',

            //lista
            'list_id'                => '1087766103340838913',	            
            'slug'                   => 'test2',
            'list_owner_screen_name' => 'totd_de',
            'list_user_id' 			 => '925217292',
            'collection_id'          => '1087767529265811457',   //https://twitter.com/totd_de/timelines/1087767529265811457
            
            'flags'                  => 'flag-icon-de',
             
            //facebook
            'app_id'                     => '', 
            'app_secret'                 => '',
            'page_id'                    => '',
            'extended_page_access_token' => '',
 
            'country' 			=> 'germany1',
            'timezone'          => 'Europe/Berlin',
            'keys1234' 			=> ',,,',
        ],
        'list_timeline' => [          
            //slide list config, must be optional
            'top_h'         => 0,                                                  
            'bottom_h'      => 1, 
            'count'         => 200,                                                 
            'fav_count'     => 100, 
            'lang'          => 'de',
            'for_i'         => 5,  
            'format'        => "H:i:s d-m-Y",
        ],
        'user_timeline' => [            
            //slide user optional config
            'count'         => 200,  
            'for_i'         => 5
            ],
        'banned_users' => [  
            ['user_id' => '', 'screen_name' => '', 'description' => 'banned...']
        ], 
    ],
    //----------------- turkey1 ---------------------
    3 => [
        'active'  => true,
        'country' => 'turkey1',        
        'country_name' => 'Turkey',
        'profile' => [
            //user
            'screen_name'       => 'totd_tr',
            'user_id'           => '1091043141111697408',

            //lista
            'list_id'                => '1091049973104500736',	            
            'slug'                   => 'test1',
            'list_owner_screen_name' => 'totd_tr',
            'list_user_id' 			 => '1091043141111697408',
            'collection_id'          => '1091050992999809024',   //https://twitter.com/totd_tr/timelines/1091050992999809024
            
            'flags'                  => 'flag-icon-tr',
             
            //facebook
            'app_id'                     => '', 
            'app_secret'                 => '',
            'page_id'                    => '',
            'extended_page_access_token' => '',
 
            'country' 			=> 'turkey1',
            'timezone'          => 'Europe/Istanbul',
            'keys1234' 			=> ',,,',
        ],
        'list_timeline' => [          
            //slide list config, must be optional
            'top_h'         => 0,                                                  
            'bottom_h'      => 1, 
            'count'         => 200,                                                 
            'fav_count'     => 200, 
            'lang'          => 'tr',
            'for_i'         => 5,  
            'format'        => "H:i:s d-m-Y",
        ],
        'user_timeline' => [            
            //slide user optional config
            'count'         => 200,  
            'for_i'         => 5
            ],
        'banned_users' => [  
            ['user_id' => '', 'screen_name' => '', 'description' => 'banned...']
        ], 
    ]
];

