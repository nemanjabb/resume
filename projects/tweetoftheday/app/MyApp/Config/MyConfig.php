<?php namespace App\MyApp\Config;

use File;

/**
 * Description of MyConfig
 *
 * @author username
 */
class MyConfig {   
    
    private $country;

    public function setCountry($country) {
        $this->country = $country;
    }
    
    public function getActiveCountries() {   
        $all_config = MyConfig::getAllConfig();
        
        $active = [];
        foreach ($all_config as $item) {
            if($item['active'])
                $active[] = $item;
        }       
        return $active;
    }
    
    public function getAllConfig() {   
        $rez = File::getRequire(__DIR__.'/MyAppConfig.php');   
        return $rez;
    }
    
//     arg serbia1
    public function getCountry() {   
        $all_config = MyConfig::getAllConfig();
        
        $item1 = null;
        foreach ($all_config as $item) {
            if($item['country'] == $this->country){
                $item1 = $item;
                break;
            }
        }
        return $item1;
    }
}


