<?php namespace App\MyApp;

use Log;
use Cache;
use Twitter;
use Carbon\Carbon;
use Exception;
use MyConfig;
use Codebird\Codebird;

/**
 * Description of TwApi
 * samo stat fje
 * @author username
 */
class TwApi {
  
    
    /*
     * 
     * treba konstruktor sa kljucevima za Twitter::
     * i carbon
     */
    
    //post tweet
    //Twitter::postTweet(['status' => 'Laravel is beautiful', 'format' => 'json']);
    //POST statuses/destroy/:id
    
    
    public static function countExceptions($api_cache_key)
    {
        if(!Cache::has($api_cache_key))
            Cache::put($api_cache_key,  1, Carbon::now()->addMinutes(1)); 
        else   {
            $i = Cache::get($api_cache_key);
            if($i > 2) {
                throw new Exception('3+ handled exceptions');
            } else 
                Cache::put($api_cache_key, $i+1, Carbon::now()->addMinutes(1));           
        }
    }
    
    /*
     * ne treba da zna za usera iz modela
     */
    public static function get_users_tweets($screen_name, $since_id, $max_id, $count)
    {
        $params = [
            'screen_name'        => $screen_name,
            'count'              => $count,                     
            'include_rts'        => true,
            'max_id'             => $max_id,                                    //da ne preklopi            
            'since_id'           => $since_id,
            'format'             => 'array'
        ];
        //remove nulls ''
        $params = array_filter($params);
      
        try
        {
            $response = Twitter::getUserTimeline($params);
        }
        catch (Exception $e)
        {            
            Log::error($e);
        }

        return $response;      
    }   
    
    public static function get_list_tweets($since_id, $max_id, $count, $list)
    {
        $params = [
            'list_id'            => $list['list_id'],
            'slug'               => $list['slug'],
            'owner_screen_name'  => $list['list_owner_screen_name'],
            'count'              => $count,                     
            'include_rts'        => true,
            'max_id'             => $max_id,                                                
            'since_id'           => $since_id,
            'format'             => 'array',
            //'include_entities' => 1,                       
        ];
        $params = array_filter($params);
        /*
        //test data
        $params = [
            'slug'               => 'dabetićeva-lista',
            'owner_screen_name'  => 'IDabetic',
            'count'              => 200,                                                         
            'include_rts'        => 1,
            'max_id'             => '731968664447139840',                                             
            //'since_id'           => '729219544485597184',
            'format'             => 'array',
            //'include_entities' => 1,            
            //'list_id'          => $list_id,
            //'max_id'             => '729235151792832515', //bben                                             
            //'since_id'           => '729204194524147714',
        ];
         */      
        try
        {
            $response = Twitter::getListStatuses($params);
        }
        catch (Exception $e)//treba twitter exception
        {   
            $l = Twitter::logs();
            Log::error($l);
            Log::error($e);
        }

        return $response;
    }
    
    
    public static function get_collections_tweets($id, $max_position=null, $min_position=null) 
    {
       //https://dev.twitter.com/rest/reference/get/collections/entries
        $params = [
            'id'               => 'custom-'.$id,
            'count'            => 20,
            'max_position'     => $max_position,                                                         
            'min_position'     => $min_position,
            'format'            => 'array'
        ];
        $params = array_filter($params);      
        try
        {
            $response = Twitter::get('collections/entries', $params);
        }
        catch (Exception $e)
        {            
            Log::error($e);
        }
        if(isset($response['objects']['tweets']))
            return $response['objects']['tweets'];
        else 
            return [];        
    }
    
    public static function curate_collection($id, $new_top10_ids, $old_top10_ids) 
    {
        $keys1 = MyConfig::getCountry()['profile']['keys1234'];  
        $keys = explode(',', $keys1);       
        /*
        "consumer_key"      => $keys[0],
        "consumer_secret"   => $keys[1],
        "token"             => $keys[2],
        "secret"            => $keys[3],
         */
        \Codebird\Codebird::setConsumerKey($keys[0], $keys[1]); 
        $cb = \Codebird\Codebird::getInstance();
        $cb->setReturnFormat(CODEBIRD_RETURNFORMAT_ARRAY);
        $cb->setToken($keys[2], $keys[3]);
//        $cb->setProxy('localhost', '8888');
        
        $add_arr = array_map(function($item) { return ['op' => 'add', 'tweet_id' => $item];}, $new_top10_ids);
        $remove_arr = array_map(function($item) { return ['op' => 'remove', 'tweet_id' => $item];}, $old_top10_ids);
//        $arr_all = array_merge($remove_arr, $add_arr);
//        $arr_all = array_reverse($arr_all);
        
        $response1 = [];
        $response2 = [];
        try
        {            
            //skloni sve, dodaj sve, da bi bili sortirani
            $response1 = $cb->collections_entries_curate(['id'      => 'custom-'.$id,
                                                          'changes' => $remove_arr
                                                        ]);    
            $response2 = $cb->collections_entries_curate(['id'       => 'custom-'.$id,
                                                           'changes' => $add_arr
                                                         ]);
        }
        catch (Exception $e)
        {       
            Log::error($e);
        }
        //count($response['response']['errors']
        return $response1['httpstatus'] < 300 && $response2['httpstatus'] < 300;
        
    }

    public static function add_or_remove_to_collection($add_or_remove, $id, $tweet_id, $relative_to=null, $above=null) 
    {
        //https://dev.twitter.com/rest/reference/post/collections/entries/add
        //https://dev.twitter.com/rest/reference/post/collections/entries/remove
        $params = [
            'id'               => 'custom-'.$id,
            'tweet_id'         => $tweet_id,
            'relative_to'      => $relative_to,  //other tweet id                                                       
            'above'            => $above,        //false ispod relativnog, true iznad
            'format'           => 'array'
        ];
        //$params = array_filter($params);     //sklanja false 
        try
        {
            if($add_or_remove == 'add')
                $response = Twitter::post('collections/entries/add', $params);
            
            if($add_or_remove == 'remove')
                $response = Twitter::post('collections/entries/remove', $params);
        }
        catch (Exception $e)
        {            
            Log::error($e);
        }
        /*//na gresku
        { "objects": {},
            "response": { "errors": [] } //count 0
        }
        */
        return $response;
    }
    
     public static function delete_tweet($tweet_id) 
    {
        $params = ['id'=> $tweet_id, 'trim_user' => true];
        $params = array_filter($params);      
        try {
            $response = Twitter::post('statuses/destroy', $params);
        }
        catch (Exception $e)
        {            
            Log::error($e);
        }
        return $response;
    }
    
    public static function get_bben_arr()
    {  
        //@big_ben_clock      
        $params = [
            'screen_name'       => "big_ben_clock",
            'count'             => 35,                                          //24h
            'include_rts'       => 0,
            'include_entities'  => 0,
            'format'            => 'array'
        ];
         
        try
        {            
            $response = Twitter::getUserTimeline($params);
        }
        catch (Exception $e)
        {            
            //$l = Twitter::logs();
            //dd($l);
            Log::error($e);
            throw $e;
        }

        //zona postoji u tvitu
        $ids = [];
        for ($i = 0; $i < count($response); $i++) {   
            $tweet = $response[$i];
            $ids[] = ["id_str" => $tweet["id_str"], "created_at" => $tweet["created_at"], "text" => $tweet["text"]];
        }        
        return $ids;
    }

    
    /*
     * prvi iznad
     */
    public static function bben_index_from_datetime($bben_arr, $datetime) 
    {
        $i = 0;
        foreach ($bben_arr as $item) 
        {           
            if( !(is_array($item) && array_key_exists('created_at', $item)) )
                continue;
                        
            $tw_time = new Carbon($item["created_at"]);
            if($tw_time < $datetime){
                break;
            }
            $i++;
        }
        return $i;
    }

}    
    
     

/*
 * // set data of the twitter account you are going to publish to..
if (some_condition) {
    $consumer_key =  "account_consumer_key";
    $consumer_secret =  "account_consumer_secret";
    $access_token = "account_access_token";
    $access_token_secret = "account_access_token_secret";
} else if (some_condition) {
    $consumer_key =  "account_consumer_key";
    $consumer_secret =  "account_consumer_secret";
    $access_token = "account_access_token";
    $access_token_secret = "account_access_token_secret";
}

Twitter::reconfig([
    "consumer_key" => $consumer_key,
    "consumer_secret"  => $consumer_secret,
    "token" => $access_token,
    "secret" => $access_token_secret,
]);

 */