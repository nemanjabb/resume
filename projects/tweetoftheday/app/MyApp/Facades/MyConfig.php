<?php namespace App\MyApp\Facades;

use Illuminate\Support\Facades\Facade;


/**
 * Description of MyConfig
 *
 * @author username
 */
class MyConfig extends Facade{
    protected static function getFacadeAccessor() { return 'MyConfig'; }
}