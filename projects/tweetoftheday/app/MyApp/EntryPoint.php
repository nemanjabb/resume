<?php namespace App\MyApp;

use Twitter;
use MyConfig;
use Log;
use App\Handlers\MyEloquentHandler;
    
    /*
     * ucitaj config, postavi time zonu 
     * set kljuceve za twapi
     * zovi ovo u rutu ili u sheduler
     * zemlja prefix u cache kljucevima da se ne mesaju//vec jesu, svi cache keys su atributi timeline klasa
     * app singleton ili ne?
     * app kao paket
     * Your AppServiceProvider has a boot() method.
     * When you're actually using the instance. $carbon->timezone('...'), clan klase, jednom po klasi
     * nece bude kesiran config, ti imas vise fajlova, svi ce da se kesiraju
     * za kes ide obicna timezona, to je na jednom mestu na serveru
     * jeste time zona nebitna, bitna samo za ispis toString, za poredjenje nebitna, ulazi u datum svakako, i u tvit
     * Carbon::today(); ovde jeste bitna
     * 
     * singleton, izvrsava samo 1
     */

class EntryPoint {

    
    /*
     * Private ctor so nobody else can instance it
     */
    private function __construct() {}
    
    /*
     * Call this method to get singleton
     *
     * @return UserFactory
     */
    public static function getInstance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new EntryPoint();
        }
        return $inst;
    }
 
    /*
     * na toj instanci, a jedna je instanca, samo ona se intanc ili vrati
     * init Monolog and Twitter facades
     */
    public function reconfigAll($country) 
    {            
        static $called;

        // Function has already run
        if ( $called !== null )
            return;
        
        //set up Monolog
        $monolog = Log::getMonolog();
        $handler = new MyEloquentHandler($country);
        $monolog->pushHandler($handler);
        
        //set up country
        MyConfig::setCountry($country);
        
        //set up Twitter
        $keys1 = MyConfig::getCountry()['profile']['keys1234'];  
        $keys = explode(',', $keys1);       
        Twitter::reconfig([
            "consumer_key"      => $keys[0],//'123',//
            "consumer_secret"   => $keys[1],
            "token"             => $keys[2],
            "secret"            => $keys[3],
            
            /*
            //fiddler
            'curl_ssl_verifyhost'        => 0, 
            'curl_ssl_verifypeer'        => false, 
            'curl_proxy'                 => '127.0.0.1:8888'//bez https:// obavezno
            */
        ]);
        
        $called = 'run only once';        
    }

    //samo config() pozovem za reconfigure jbt
    //u hendler stavi da brise starije od dan
}
