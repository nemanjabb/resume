

//phantomjs js.js https://twitter.com/TarantulaFX/status/831500863613108224 [role='main'] capture.png

var args = require('system').args;
var page = require('webpage').create();

var url = args[1];
var selector = args[2];
var file = args[3];


console.log(args);

page.open(url, function(status) {
	//console.log(selector);
	page.viewportSize = { width: 1440, height: 900 };
	var bb = page.evaluate(function (selector_1) {  
		//console.log(selector_1);
		return document.querySelector(selector_1).getBoundingClientRect();
	}, selector);

	var o1 = 5;
	var o2 = 10;
	page.clipRect = {
		top:    bb.top+o1,
		left:   bb.left+o1,
		width:  bb.width-o2,
		height: bb.height-o2
	};
	//console.log(file);
	page.render(file);
	phantom.exit(0);
});


/*
page.open('http://google.com', function() {
  // being the actual size of the headless browser
  page.viewportSize = { width: 1440, height: 900 };

  var clipRect = page.evaluate(function(){
    return document.querySelector('#hplogo').getBoundingClientRect();
  });

  page.clipRect = {
    top:    clipRect.top,
    left:   clipRect.left,
    width:  clipRect.width,
    height: clipRect.height
  };

  page.render(file);
  phantom.exit();
});
*/





