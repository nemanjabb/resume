<?php namespace App\MyApp;

use MyConfig;
use Facebook;
use Log;


class FbApi {


    public function __construct() {}
    
    public static function uploadPhotoOnPage($message, $img_path, $page_id, $extended_page_access_token)
    {
        $id = null;
        //$photo_path = storage_path().'/app/temp.jpg';        
        $data = [
            'message' => $message,
            'source' => Facebook::fileToUpload($img_path),
        ];

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = Facebook::post('/'.$page_id.'/photos', $data, $extended_page_access_token); //'/PageID/photos'
            $graphNode = $response->getGraphNode();
            $id = $graphNode['id'];//image id   
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            Log::error($e);
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            Log::error($e);
        }
        return $id;    
    }
    
    /*
     * "Error validating access token: Session has expired on Monday, 05-Dec-16 10:59:12 PST. The current time is Tuesday, 14-Feb-17 16:38:13 PST."
     */
    public static function testUploadPhotoOnPage()
    {
        //page access token, stranice, ne usera u api tool
        //pise tamo da li je od stranice ili usera, na get extended stranici
        //plavi kruzic glavni
        
        //selektuj serbia1 app i test1 page u api tool
        //plavi kruzic open in access token tool, tamo get extended
        //manage_pages, publish_pages, publish_actions, public_profile
        $extended_page_access_token = 'EAAOIwraErTsBAAnSmqfsSoMMyyhAJZBB4EZBrr9WnxI1rFPvsBqa1MbWxeVYFrFWygEMFZBbBtlUK0SlVtglIpUz66YVzZAJ5FxDeckDDrO5zLAMz7RdjP68nGDlS6XGCkQRwJEK2JEFlMSghcy7iG4hApnNsvapuhjqMjd0DwZDZD';
        $page_id = '1291831577513785';
        $photo_path = storage_path().'/app/flower.jpg';
        
        $data = [
            'message' => 'My awesome photo upload example.',
            'source' => Facebook::fileToUpload($photo_path),
        ];

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = Facebook::post('/'.$page_id.'/photos', $data, $extended_page_access_token); //'/PageID/photos'
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

          echo 'Photo ID: ' . $graphNode['id'];
      
    }

}
