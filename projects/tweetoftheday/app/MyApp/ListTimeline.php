<?php namespace App\MyApp;

use Log;
use App\MyApp\TwApi;
use App\MyApp\LogHelper;
use Carbon\Carbon;
use Cache;
use MyConfig;
use App;
use Twitter;

class ListTimeline
{
    
    public  $config = [];  //config ne sme da bude staticki, tj sti u svim instancama
    private $bben_ids_arr = [];
    
    /*
     * sav cache treba da zivi tacno zadnja 2 pokretanja, duze je pogresno
     * stringovi za cache
     */
    private $cursors_keys = [];
    
    /*
     * cache zadnje pokretanje, rtovani
     * first, last //kljuc samo
     * $cache_keys['rt_ed_id_str_arr']
     * datum zadnji uspesan rt>0
     * $cache_keys['last_rt_run_success_date']
     */
    private $cache_keys = [];

    function __construct() {
        $this->config = MyConfig::getCountry();  
        $this->bben_ids_arr = TwApi::get_bben_arr();
        
        //cursors
        $this->cursors_keys['max_id'] = $this->config['country'].'_max_id';
        $this->cursors_keys['since_id_0'] = $this->config['country'].'_since_id_0';//za poredjenje
        $this->cursors_keys['since_id_1'] = $this->config['country'].'_since_id_1';//za upit
        $this->cursors_keys['cursors_expires_at'] = Carbon::now()->addMinutes(90);//zadnja 2 pokretanja
       
        $this->cache_keys['rt_ed_id_str_arr'] = $this->config['country'].'_rt_ed_id_str_arr';
        $this->cache_keys['last_rt_run_success_date'] = $this->config['country'].'_last_rt_run_success_date';//dan ipo
        
        
        
    }
       
    /*
     * returns bool
     */
    private function criterium($item) 
    {
        //broj fav mozes da skaliras na broj pratilaca
        //samo rt-ovi?
        //['retweeted'] da ga nisi vec rt, ['lang']
        // da li je danasnji tvit
        
        $lang = $this->config['list_timeline']['lang'];
        $fav_count = $this->config['list_timeline']['fav_count'];
        //banned
        $banned_ids = collect($this->config['banned_users'])
                      ->map(function ($item, $key) { return $item["user_id"]; });
         
        $tz = $this->config['profile']['timezone']; 
        $todayMidnight = Carbon::today($tz)->subHours(2);
        $tw_time = new Carbon($item['created_at']);    
        $isToday = $tw_time > $todayMidnight;          
                      
                      
        return 
        $isToday &&
        !$item['retweeted'] &&  // nije vec retvitovan    
        !$banned_ids->contains($item["id_str"]) && 
        $item['favorite_count'] > $fav_count
        && (($lang != 'en' && ($item['lang'] != 'en')) //nije eng
            || ($lang == 'en' && ($item['lang'] == 'en'))); //ili jeste eng
    }
     
    /*
     * $result_0 //prethodna fja
     * $result_1 //ova fja
     * 
     * ['rt_listtimeline']
     * $result_1['rt_ed']
     * 
     */
    public function rt_listtimeline()
    {
        $result_1['rt_ed'] = [];
        
        $all_result = $this->traverse_listtimeline(); 
        $result_0 = $all_result['traverse_listtimeline'];
               
        //ima rezultata
        if(count($result_0['all_results']) > 0) {   
            
            $collection = collect($result_0['all_results']);
                       
            $for_rt = $collection->filter(function ($item) {                
                return $this->criterium($item);  
            })->unique('id_str')->take(20)->values(); //->unique('id_str')// za rt already

            $result_1['rt_ed'] = $for_rt->all();
            
            //obrcem ih, da bi najfejvovaniji prvi rt
            $for_rt1 = $for_rt->sortBy('favorite_count')->values();
            $for_rt1->each(function ($item, $key) {
                try{
                    Twitter::postRt($item['id_str']);      
                } catch(Exception $e) {
                    Log::error($e);
                }
            });
            
            //ipak to ne mora, svaki tvit je ili pre ili posle ponoci
            //kljucevi za ponoc-sad
            //kljucevi za jucerasnji dan ponoc-ponoc
            
            //kljucevi za zadnje pokretanje
            $rt_ed_ids_arr = collect(Cache::get($this->cache_keys['rt_ed_id_str_arr'], []));//arr default
            $rt_ed_ids_arr = $rt_ed_ids_arr->prepend(['first_rt' => $for_rt->first()['id_str'], 'last_rt' => $for_rt->last()['id_str']])
                                           ->take(2)//vise je zastarelo
                                           ->values()//postavlja numeric kljuceve
                                           ->all();

            Cache::put($this->cache_keys['rt_ed_id_str_arr'], $rt_ed_ids_arr, $this->cursors_keys['cursors_expires_at']);            
            Cache::put($this->cache_keys['last_rt_run_success_date'], Carbon::now()->toRfc1123String(), Carbon::now()->addHours(36));
        }
        
        $all_result['rt_listtimeline']['rt_ed'] = $result_1['rt_ed'];
        $log = LogHelper::log_rt_listtimeline($all_result);                   
        return $all_result;
    }
  
    /*
     * //all in
     * ['traverse_listtimeline']
     * 
     * //results
     * $result_0['all_results']
     * $result_0['warning']
     * 
     * //debug
     * //in args
     * $debug_0['args']['time_h_m']
     * $debug_0['args']['top_h']
     * $debug_0['args']['bottom_h']
     * 
     * $debug_0['izlaz']
     * $debug_0['po_iteracijama']
     * $debug_0['first_run']
     * 
     * $debug_0['first_tw'] 
     * $debug_0['last_tw']
     * $debug_0['first_tw_id_str']
     * $debug_0['last_tw_id_str']
     * $debug_0['now']
     * 
     */               

    public function traverse_listtimeline()
    { 
        //results
        $result_0['all_results'] = [];
        $result_0['warning'] = false;
        
        //debug args
        $debug_0 = [];
        $init_arr = $this->init_cursors_listtimelne();
        $debug_0['args'] = $init_arr['debug'];
        
        //debug
        $debug_0['izlaz'] = '';
        $debug_0['po_iteracijama'] = [];
        
        $debug_0['first_tw'] = '';
        $debug_0['last_tw'] = '';
        $debug_0['first_tw_id_str'] = '';
        $debug_0['last_tw_id_str'] = '';
        $debug_0['now'] = $this->logDate(Carbon::now());
        $debug_0['country'] = $this->config['country'];
        
        //null na neuspeh
        $since_id_0 = $init_arr['since_id_0'];//za poredjenje, gornji
        $since_id_1 = $init_arr['since_id_1'];//za upit//stariji
        $debug_0['first_run'] = $first_run = !$since_id_0 && !$since_id_1;
        
        $timeZone = $this->config['profile']['timezone'];//greska bila, mora za ::now
        //mora ceo sat ovde, a ne now
        $bottom4h = Carbon::now($timeZone)->minute(0)->second(0)->subHours($this->config['list_timeline']['bottom_h']);
        $i = -10; 
        $for_i = $this->config['list_timeline']['for_i'];
        //radis samo sa max_id nadole
        for ($i = 0; $i < $for_i; $i++) {  //180 za 15 min	//podesi prema protoku liste
            
            $list = ['list_id'=> null, 'slug' => $this->config['profile']['slug'], 'list_owner_screen_name' => $this->config['profile']['list_owner_screen_name']];
            $max_id = Cache::get($this->cursors_keys['max_id']);
            //vec hendlovan except u api fji          
            $response = TwApi::get_list_tweets($since_id_1, $max_id, $this->config['list_timeline']['count'], $list); 

            //mora odvojeno da se poredi 0-ta i ostale iteracije
            //ostale 1 pa unshift 0
            $count_response = count($response);
            if ($count_response < 2) {
                //nista kursori//abs_since_id ostaje
                if ($i == 0) {
                    $debug_0['izlaz'] = 'izlaz, '.$count_response.' rezultata';//nije ok
                    $result_0['warning'] = true;
                } else {
                    $debug_0['izlaz'] = 'izlaz, '.$count_response.' rezultata ali u '.$i.' iteraciji';//ok
                }
                break;//kraj
            }
         
            //donja granica za sledeci poziv
            if($i==0) {
                //0 za poredjenje - iznad, da vrati i njega, noviji
                //1 za upit - ispod, stariji               
                Cache::put($this->cursors_keys['since_id_0'], $response[0]['id_str'], $this->cursors_keys['cursors_expires_at']);
                Cache::put($this->cursors_keys['since_id_1'], $response[1]['id_str'], $this->cursors_keys['cursors_expires_at']);

            } else {
                //ako nije 0-ta iterac, isto se ponasaju
                //u 0-toj ni jedan nista, jer je na sat, isto se ponasaju
                //remove first elem, vec bio u prosloj iteraciji
                array_shift($response);//vraca prvi
            }      
            
            if($first_run)
            {   
                //first run
                
                $older_than = false;
                $filtered = [];
                $tw_time = null;
                foreach ($response as $tweet) {
                    $tw_time = new Carbon($tweet['created_at']);
                    if($tw_time > $bottom4h){
                        $filtered[] = $tweet;                    
                    } else {
                        $older_than = true;
                        break;
                    }
                }
                //log filtered
                $debug_0['po_iteracijama'][] = ['iteracija' => $i, 'broj_rezultata' => count($filtered)];    
                
                if (count($filtered) > 0)
                {
                    //uspeh, radi
                    $new_max_id = end($filtered)["id_str"]; //donji

                    //all //redosled
                    // += bio bug, to radi samo za razlicite kljuceve//append
                    $result_0['all_results'] = array_merge($result_0['all_results'], $filtered);

                }    
                if($older_than){
                    //izadji
                    $debug_0['izlaz'] = 'izlaz, first_run=true, na vremenski limit. tw_time='.$this->logDate($tw_time).' > bottom4h='.$this->logDate($bottom4h);
                    break;                
                } else {
                    //da bi iso dalje na dole
                    Cache::put($this->cursors_keys['max_id'], $new_max_id, $this->cursors_keys['cursors_expires_at']);
                }
            } else {
                //not first run
                //response mesto filtered
                
                $debug_0['po_iteracijama'][] = ['iteracija' => $i, 'broj_rezultata' => count($response)];
                
                $new_max_id = end($response)['id_str']; //donji
                $result_0['all_results'] = array_merge($result_0['all_results'], $response);

                //since_id je za tekuce pokret, abs_since_id za iduce, mora 2
                //ovo da bi pokrio sve do 1 tvitove liste
                //samo u prvom pokretanju da izadje na datum, a posle da izlazi ovde na since id
                //proveri da li postoji since_id ti je uslov prvog pokretanja
                //treba ti absolute_since_id odakle si poso da ne ides do beskonac
                //radi kad nije od bbena
                $razlika = bcsub($new_max_id, $since_id_0);
                if ($razlika > 0) { 
                    Cache::put($this->cursors_keys['max_id'], $new_max_id, $this->cursors_keys['cursors_expires_at']);
                    //nova iterac
                } else { //na == //kraj
                    //nista kursori
                    $debug_0['izlaz'] = 'izlaz, dosao do dna. razlika='.$razlika;//razlika 0 je ok u testu
                    break;
                }
            }
        }
        
        //debug relevantno samo, min i jasno, i templejt prema tome, i ostalim fjama
        
        //petlja se izvrtela
        if($for_i == $i){
            $debug_0['izlaz'] = 'izlaz, petlja se izvrtela, i='.$i;
            $result_0['warning'] = true;            
        } 
        //log times 
        if(count($result_0['all_results']) > 0) {
            
            $result_0['all_results'] = collect($result_0['all_results'])->map(function ($item, $key) {                      
                //fav count nema direktno, 0
                if(array_key_exists ('retweeted_status', $item)) {
                    $item['retweeted_status']['retweeter'] = $item['user']['screen_name']; 
                    return $item['retweeted_status'];
                }
                else {
                    $item['retweeter'] = 'none';
                    return $item;                    
                }
            })->sortByDesc('favorite_count')->values()->all();
            
            $this->logDatesAndIds($result_0, $debug_0);
        }
		$result_0['debug'] = $debug_0;
        $d_res1['traverse_listtimeline'] = $result_0;
        return $d_res1;
    }

    //return string
    //sav log bi trebao na tz profila
    private function logDate($carbon_date) {
        return $carbon_date->setTimezone($this->config['profile']['timezone'])->format('H:i'); //'h:i A'
    }
    //by ref
    public  function logDatesAndIds(& $result_0, & $debug_0) {    
        
        $first_tw = $result_0['all_results'][0];
        $last_tw = end($result_0['all_results']);
        $debug_0['first_tw'] = $this->logDate(new Carbon($first_tw['created_at']));
        $debug_0['last_tw'] = $this->logDate(new Carbon($last_tw['created_at']));
        $debug_0['first_tw_id_str'] = $first_tw['id_str'];
        $debug_0['last_tw_id_str'] = $last_tw['id_str'];
    }
    
    /*
     * set top 2h old od big bena
     * set absolute bottom 4h old if older
     */
    private function init_cursors_listtimelne()
    {              
        /*
         * //return
         * $result_0['since_id_0']   
         * $result_0['since_id_1']
         * //debug
         * $debug_0['time_h_m']                                               
         * $debug_0['top_h']        
         * $debug_0['bottom_h']
         * 
         */  
        $result_0 = [];
        $bben_ids_arr = $this->bben_ids_arr;
                     
        //set top
        $top_h = $this->config['list_timeline']['top_h'];		
		Cache::put($this->cursors_keys['max_id'], $bben_ids_arr[$top_h]["id_str"], $this->cursors_keys['cursors_expires_at']);//pre 1 ili 1+h
        
        //log
        $debug_0['time_h_m'] = $this->logDate(new Carbon($bben_ids_arr[$top_h]["created_at"]));
                                                
        $debug_0['top_h'] = $top_h;        
        $debug_0['bottom_h'] = $this->config['list_timeline']['bottom_h'];   
        
        //[0] je gornji za poredjenje
        $result_0['since_id_0'] = Cache::get($this->cursors_keys['since_id_0']);    
        //[1] je donji,za upit
        $result_0['since_id_1'] = Cache::get($this->cursors_keys['since_id_1']); 
        //ovi trenutnog izvrsavanja ne moraju perzistentni, nigde se ne citaju u sledecem pokretanju
		$result_0['debug'] = $debug_0;
        return $result_0;
    }
    

}

