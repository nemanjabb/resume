<?php namespace App\MyApp;

use Log;
use App\MyApp\TwApi;
use Storage;
use Carbon\Carbon;
use Cache;
use MyConfig;
use App\MyApp\LogHelper;
use App\MyApp\MyPhantomJs;
use App\MyApp\FbApi;
use App\MyApp\TwCollection;

/*
 * jos templatei za log za sve fje pojedinacno
 * post to fb with image
 * render phantom, logo i natpis, izgooglaj gotovo
 * post to collection
 */


class UserTimeline
{

    private $config = []; 
    private $bben_ids_arr = [];

    private $cursors_keys = [];
    
    //za top1 last run
    private $cache_keys = [];

    function __construct() {
        $this->config = MyConfig::getCountry();  
        $this->bben_ids_arr = TwApi::get_bben_arr();
        
        //cursors, since_id nema nikakav, datumi
        $this->cursors_keys['max_id'] = $this->config['country'].'_max_id';
        $this->cursors_keys['cursors_expires_at'] = Carbon::now()->addMinutes(90);//zadnja 2 pokretanja
       
        $this->cache_keys['rt_ed_id_str_arr'] = $this->config['country'].'_rt_ed_id_str_arr';
        $this->cache_keys['last_rt_run_success_date'] = $this->config['country'].'_last_rt_run_success_date';//dan ipo
        $this->cache_keys['top_3_users_arr'] = $this->config['country'].'_top_3_users_arr';
    }

    public function initAllResult($from_date, $to_date, $top_n) {
        $result_0 = [];
        $result_0['all_results'] = [];
        $result_0['warning'] = false;
        $result_0['top_n_tweets'] = [];
        
        $debug_0['from_date'] = $this->logDate($from_date);
        $debug_0['from_time_tw'] = 'undefined';
        $debug_0['to_date'] = $this->logDate($to_date);
        $debug_0['top_n'] = $top_n;
        $debug_0['po_iteracijama'] = [];
                
        $debug_0['first_tw'] = '';
        $debug_0['last_tw'] = '';
        $debug_0['first_tw_id_str'] = '';
        $debug_0['last_tw_id_str'] = '';
        $debug_0['now'] = $this->logDate(Carbon::now());
        $debug_0['country'] = $this->config['country'];
        $debug_0['izlaz'] = 'izlaz, nije ni usao';
        
        $result_0['debug'] = $debug_0;
        $d_res1['traverse_usertimeline'] = $result_0;
        return $d_res1;
    }
    
    /*
     * //prethodna fja, rezultati
     * ['traverse_usertimeline']
     * $result_1['all_results']
     * $result_1['top_n_tweets']
     * $result_1['warning']  
     * 
     * //ova fja 
     * ['top_10_for_collection']
     * 
     * //result
     * $result_1['top_1_pinned'] 
     * 
     * //debug
     * $result_1['warning']
     * $debug_1['izlaz']
     * $debug_1['api_update_collection'] 
     * $debug_1['api_pin_top_1'] 
     * 
     */   
    public function top_10_from_midnight_for_collection($screen_name = null)
    {     
        //result
        $result_1['top_1_pinned'] = []; 
        //debug
        $result_1['warning'] = false;
        $debug_1['izlaz'] = '';
        $debug_1['api_update_collection'] = false;
        $debug_1['api_pin_top_1'] = false;
        
        $rt_date_key = $this->cache_keys['last_rt_run_success_date'];
        
        $tz = $this->config['profile']['timezone'];
        $now = Carbon::now($tz); 
        $todayMidnight = Carbon::today($tz);//zadnja ponoc, pocetak danasnjeg dana   

        $all_result = $this->initAllResult($now, $todayMidnight, 20); 
        
        //treba traverse init polje za neuspeh i 0 rez da dodje u debug log
        //ima kljuc i mladji od 90 min//pozitiv uslov
        //if (App::environment('testing'))
        //ne nego treba da se mockupuje validan cache ovde

        if ($screen_name || Cache::has($rt_date_key) && (new Carbon(Cache::get($rt_date_key))) > Carbon::now()->subMinutes(90))
        {               
			$screen_name ? $screen_name : $this->config['profile']['screen_name'];			
            $all_result = $this->traverse_usertimeline($screen_name, $now, $todayMidnight, 20);

            $result_0 = $all_result['traverse_usertimeline'];
            
            //treba rted tvitovi
            //ima rez, pozitivni uslovi
            if(count($result_0['top_n_tweets']) > 0)
            {
                //sve ok

                //update collection//write debug if success
                //if (App::environment('production'))
                //$new_top10_ids = array_map(function($item) { return $item['id_str']; }, $result_0['top_n_tweets']);
                $collection_id = $this->config['profile']['collection_id'];
                $debug_1['api_update_collection'] = TwCollection::updateCollection($collection_id, $result_0['top_n_tweets']);

                //set top 3 users
                $top3users = LogHelper::trimUsersFromTweetsArray(array_slice($result_0['top_n_tweets'], 0, 3));
                Cache::forever($this->cache_keys['top_3_users_arr'], $top3users);

                //quote, delete, pin top 1
                $result_1['top_1_pinned'] = $result_0['top_n_tweets'][0]; 
                $debug_1['izlaz'] = 'izlaz, sve uradio';
            } else {
                $result_1['warning'] = true;
                $debug_1['izlaz'] = 'izlaz, 0 top rezultata';
            } 

        }  else {
            $result_1['warning'] = true;
            $debug_1['izlaz'] = 'izlaz, cache stariji od 90min, code=#cache';            
        } 
        
		$result_1['debug'] = $debug_1;
        $all_result['top_10_for_collection'] = $result_1;        
        //log
        $log = LogHelper::log_top_10_from_midnight_for_collection($all_result);        
        return $all_result;
    }
    
        
    /*
     * //rezultati
     * ['traverse_usertimeline']
     * $result_1['all_results']
     * $result_1['top_n_tweets']
     *  
     * 
     * ['top_3_yesterdays_for_fb']
     * $result_1['warning'] 
     * $result_1['cache_error'] 
     * 
     * //debug
     * $debug_1['izlaz']
     * $debug_1['api_screenshot']
     * $debug_1['api_posted_on_fb']
     * 
     * 
     */  
    public function top_3_yesterdays_for_fb($screen_name = null)
    {
        //$result_1['top_n_tweets'] top 3       
        $result_1['warning'] = false;
        $debug_1['izlaz'] = '';
        $debug_1['api_screenshot'] = false;
        $debug_1['api_posted_on_fb'] = false;
        
        $rt_date_key = $this->cache_keys['last_rt_run_success_date'];
        
        $tz = $this->config['profile']['timezone'];
        $todayMidnight = Carbon::today($tz); 
        $yesterdayMidnight = Carbon::yesterday($tz);  

        $all_result = $this->initAllResult($todayMidnight, $yesterdayMidnight, 3);
        
        //ima kljuc i mladji od 1 dan, 17:45 juce//pozitiv uslov
        if ($screen_name || Cache::has($rt_date_key) && (new Carbon(Cache::get($rt_date_key))) > Carbon::now()->subHours(24)) //veci=mladji
        {

            $screen_name ? $screen_name : $this->config['profile']['screen_name'];

            $all_result = $this->traverse_usertimeline($screen_name, $todayMidnight, $yesterdayMidnight, 3);

            $result_0 = $all_result['traverse_usertimeline'];

            if(count($result_0['top_n_tweets']) > 0)
            {
                //sve ok, postuj
                $screenshot = true;
                $fb = true;
                //top 3 screenshot, post to fb, debug
                foreach ($result_0['top_n_tweets'] as $tweet) {
                    $tweet_url = sprintf('https://twitter.com/%s/status/%s', $tweet['user']['screen_name'], $tweet['id_str']);
                    $debug_temp = $this->screenshot_tweet_and_post_on_fb($tweet_url, $message = ''); 
                    $screenshot = $screenshot && $debug_temp['screenshot'];
                    $fb = $fb && $debug_temp['fb'];
                }
                $debug_1['api_screenshot'] = $screenshot;
                $debug_1['api_posted_on_fb'] = $fb;                
                $debug_1['izlaz'] = 'izlaz, sve uradio';
                
            } else {
                $result_1['warning'] = true;                
                $debug_1['izlaz'] = 'izlaz, 0 top rezultata';
            }

        } else {
            $result_1['warning'] = true;
            $debug_1['izlaz'] = 'izlaz, cache stariji od 24h, code=#cache';            
        }
		
        $result_1['debug'] = $debug_1;
        $all_result['top_3_yesterdays_for_fb'] = $result_1;        
        //log
        $log = LogHelper::log_top_3_yesterdays_for_fb($all_result);
        return $all_result;
    }
    
	/* 
     * 
     * manje od 200, 1 api poziv
     * ne zove traverse usertimeline
     *  
     * //result
     * ['top_1_of_last_run']
     * $result_1['top_n']
     * 
     * //debug
     * $result_1['warning']
     * $debug_1['izlaz']
     * $debug_1['api_screenshot'] 
     * $debug_1['api_posted_on_fb'] 
     * 
     */ 
    public function top_1_of_last_run_for_fb($top_n = 1)
    {
        $result_1['top_n'] = [];       
        $result_1['warning'] = false;
        $debug_1['izlaz'] = '';
        $debug_1['api_screenshot'] = false;
        $debug_1['api_posted_on_fb'] = false;
        $debug_1['country'] = $this->config['country'];
        
        $rt_date_key = $this->cache_keys['last_rt_run_success_date'];       
        //ima kljuc i mladji od sat ipo
        if (Cache::has($rt_date_key) && (new Carbon(Cache::get($rt_date_key))) > Carbon::now()->subMinutes(90)) //veci=mladji
        {
            if (Cache::has($this->cache_keys['rt_ed_id_str_arr'])) {
                               
                $ids_arr = collect(Cache::get($this->cache_keys['rt_ed_id_str_arr']))->values()-all();
                $since_id = $ids_arr[1]['last_rt']; //predzadnje pokretanje, zadnji tvit                
                $screen_name = $this->config['profile']['screen_name'];
                
                //test
                //$since_id = null;
                //$screen_name = 'zareseizigro';
                //obican api, 1 poziv, nije vrtenje
                $api_result = TwApi::get_users_tweets($screen_name, $since_id, null, 200); //mora vise da izvadi
                
                //sort by favs
                $result_1['top_n'] = collect($api_result)->sortByDesc('favorite_count')->values()->take($top_n)->values()->all();

                if(count($result_1['top_n']) > 0)
                {
                    //sve ok
                    //slika phantom
                    //post to fb
                    $tweet_url = sprintf('https://twitter.com/%s/status/%s', $result_1['top_n']['user']['screen_name'], $result_1['top_n']['id_str']);
                    $debug_temp = $this->screenshot_tweet_and_post_on_fb($tweet_url, $message = '');
                    $debug_1['api_screenshot'] = $debug_temp['screenshot'];                    
                    $debug_1['api_posted_on_fb'] = $debug_temp['fb'];
                    $debug_1['izlaz'] = 'izlaz, sve uradio';
                    
                } else {
                    $result_1['warning'] = true;
                    $debug_1['izlaz'] = 'izlaz, 0 top rezultata';               
                }
            } else {
                $result_1['warning'] = true;
                $debug_1['izlaz'] = 'izlaz, nema rt_ed_id_str_arr u cache, code=#cache';
            }
        }  else {
            $result_1['warning'] = true;
            $debug_1['izlaz'] = 'izlaz, cache stariji od 90min, code=#cache';            
        } 
		
        $result_1['debug'] = $debug_1;
        $all_result['top_1_of_last_run'] = $result_1;
        //log
        $log = LogHelper::log_top_1_of_last_run_for_fb($all_result);
        
        return $all_result;
    }
    
    private function screenshot_tweet_and_post_on_fb($tweet_url, $message = '') 
    {
        $debug_1['screenshot'] = false;
        $debug_1['fb'] = false;

        //slika phantom
        $img_path = MyPhantomJs::render_tweet($tweet_url);
        if($img_path) $debug_1['screenshot'] = true;

        //post to fb
        $page_id = $this->config['profile']['page_id'];
        $extended_page_access_token = $this->config['profile']['extended_page_access_token'];
        $image_id = FbApi::uploadPhotoOnPage($message, $img_path, $page_id, $extended_page_access_token);
        if($img_path) $debug_1['fb'] = true;
        return $debug_1;
    }
    
    private function init_cursors_usertimelne($from_date) 
    {   
        $from_index = TwApi::bben_index_from_datetime($this->bben_ids_arr, $from_date);
		//set top
        Cache::put($this->cursors_keys['max_id'], $this->bben_ids_arr[$from_index]["id_str"], $this->cursors_keys['cursors_expires_at']); 
        $from_time = $this->bben_ids_arr[$from_index]['created_at'];
        return $from_time;
    }
    /*
     * ides na dole, u proslost
     * $from_date mladje 16:00 Carbon datum
     * $to_date starije  15:00
     * ulazi datumi, a ovamo bben indexi
     * 
     * razlika od list, vrti od 2 datuma, ne treba da skuplja sve tvitove timelinea
     * nema since_id
     * 
     * 
     * //rezultati
     * ['traverse_usertimeline']
     * $result_0['all_results']
     * $result_0['top_n_tweets']
     * $result_0['warning'] 
     * 
     * //debug
     * $debug_0['from_date']
     * $debug_0['from_time_tw']
     * $debug_0['to_date'] 
     * $debug_0['top_n'] 
     * $debug_0['izlaz']
     * $debug_0['po_iteracijama']
     * $debug_0['first_tw'] i last
     * $debug_0['first_tw_id_str'] i last
     * 
     */   
    public function traverse_usertimeline($screen_name, $from_date, $to_date, $top_n) 
    {       
        $result_0 = [];
        $result_0['all_results'] = [];
        $result_0['warning'] = false;
        $result_0['top_n_tweets'] = [];
        
        $from_time_tw = $this->init_cursors_usertimelne($from_date);//created_at
        
        $debug_0['from_date'] = $this->logDate($from_date);
        $debug_0['from_time_tw'] = $this->logDate(new Carbon($from_time_tw));
        $debug_0['to_date'] = $this->logDate($to_date);
        $debug_0['top_n'] = $top_n;
        $debug_0['po_iteracijama'] = [];
                
        $debug_0['first_tw'] = '';
        $debug_0['last_tw'] = '';
        $debug_0['first_tw_id_str'] = '';
        $debug_0['last_tw_id_str'] = '';
        $debug_0['now'] = $this->logDate(Carbon::now());
        $debug_0['country'] = $this->config['country'];
        
        $results = [];
        $for_i = $this->config['user_timeline']['for_i'];
        for ($i = 0; $i < $for_i; $i++)  
        {           
            $response = TwApi::get_users_tweets($screen_name, null, Cache::get($this->cursors_keys['max_id']), $this->config['user_timeline']['count']); 
   
            $count_response = count($response);
            if ($count_response < 2) {
                //nista kursori
                if ($i == 0) {
                    $debug_0['izlaz'] = 'izlaz, '.$count_response.' rezultata';//nije ok
                    $result_0['warning'] = true;
                } else {
                    $debug_0['izlaz'] = 'izlaz, '.$count_response.' rezultata ali u '.$i.' iteraciji';//ok
                }
                break;//kraj
            }
            
            if($i != 0) {
                array_shift($response);
            }
            
            $older_than = false;
            $filtered = [];
            $tw_time = null;
            foreach ($response as $tweet) {
                $tw_time = new Carbon($tweet['created_at']);
                if($tw_time > $to_date){
                    $filtered[] = $tweet;                    
                } else {
                    $older_than = true;
                    break;
                }
            }           
            //0 filtered
            if (count($filtered) > 0){
                $new_max_id = end($filtered)['id_str'];
                $debug_0['po_iteracijama'][] = ['iteracija' => $i, 'broj_rezultata' => count($filtered)];              
                $result_0['all_results'] = array_merge($result_0['all_results'], $filtered);
            }
            //bez since_id radim
            if($older_than) {
                $debug_0['izlaz'] = 'izlaz, first_run=true, na vremenski limit. tw_time='.$this->logDate($tw_time).' > to_date='.$this->logDate($to_date);
                break;
            } else {
                Cache::put($this->cursors_keys['max_id'], $new_max_id, $this->cursors_keys['cursors_expires_at']);
            }           
        }
     
        if($for_i == $i){
            $debug_0['izlaz'] = 'izlaz, petlja se izvrtela';
            $result_0['warning'] = true;            
        }   
        //log times  
        if(count($result_0['all_results']) > 0) {
            $this->logDatesAndIds($result_0, $debug_0);
            
            //rted
            $result_0['all_results'] = collect($result_0['all_results'])->map(function ($item, $key) {                      
                if(array_key_exists ('retweeted_status', $item))
                    return $item['retweeted_status'];
                else 
                    return $item;              
            })->sortByDesc('favorite_count')->values()->all();
            
            $result_0['top_n_tweets'] = collect($result_0['all_results'])->take($top_n)->values()->all();
        }
		$result_0['debug'] = $debug_0;
        $d_res1['traverse_usertimeline'] = $result_0;
        return $d_res1;
    }
    
    //return string
    private function logDate($carbon_date) {
        return $carbon_date->setTimezone($this->config['profile']['timezone'])->format('H:i');
    }
    //by ref
    public function logDatesAndIds(& $result_0, & $debug_0) {            
        $first_tw = $result_0['all_results'][0];
        $last_tw = end($result_0['all_results']);
        $debug_0['first_tw'] = $this->logDate(new Carbon($first_tw['created_at']));
        $debug_0['last_tw'] = $this->logDate(new Carbon($last_tw['created_at']));
        $debug_0['first_tw_id_str'] = $first_tw['id_str'];
        $debug_0['last_tw_id_str'] = $last_tw['id_str'];
    }
}
