<?php namespace App\MyApp;

use Log;
use Cache;
use Twitter;
use Carbon\Carbon;
use MyConfig;
use Exception;
use Storage;
use Codebird\Codebird;
  
        /*
         * uzmi 5000 ids, napavi polje
         * [redni_broj, id_str, screen_name, followers_count, following_count, tw_count, last_active, priority_in_list, inserted, banned]
         * hidrate svih 5k, po 100, popuni polja, lakse nego posle kriterijuma
         * redni broj vazan samo za hidrate, posle na 0
         * kriterijum na polje
         * insert po 100
         * 
         * filter nepotrebnih kad i rangiranje
         * 
         */
        


        
        /*
         * file storage() za json
         * 'root'   => storage_path().'/app', default root u filesystems.php
         * $contents = Storage::get('file.jpg');
         * Storage::put('file.jpg', $contents);
         * queue exceptioni i duzina, treba stekovanje, da se zavrsi prvi
         * krastavac: what's your setup? supervisor that executes queue:work every third second is a new process all the time.
         */


class InsertListHelperScript
{
    
    //public $list = ['slug' => 'test2', 'owner_screen_name' => 'totd_de'];//ne treba, treba iz myconfig u constr    
    // serbia1 public $keys1234 = ['iFnMh4cKMAKxYK3Ii6vQ9VB5j', 'LPqiEYl7S6HeaSJQGmxLsm7yPMId8W5FpZutxpHHbEn5jVeQd7', '778671055745540100-6Xqjokw2QKpVd68i2CPyIIpokrwcibG', 'yil4StAqJ3ZP661jOxCQojRgMAg1PCOMqxHlVJg9Wb7Wk'];
    //turkey1
    //public $keys1234 = ['gMq0PjgjklyuNgnn4LDBXAFRv', '7sV88zD9EOM7b9uYMu02QsJFnbQfPjGwG442dtvoUcC5VTUKlw', '1091043141111697408-qzflHY1Mzb04z2Oq1V2qmMM8eTAElC', 'SomvPwcRukLtASb5SCLsYc7y6BIYRORz2HV3nRMrUcKDO'];
    
    
    public $filename = 'turkey1.json';
    public $keys1 = ['index', 'redni_broj', 'id_str', 'screen_name', 'followers_count', 'friends_count', 'statuses_count', 'last_active', 'priority', 'redni_br_for_insert', 'razlog_odbacivanja', 'inserted', 'banned'];
    public $header_keys = ['ukupno', 'non_hidrated', 'inserted', 'p_6', 'p_1', 'p_0', 'banned'];            
    
    
    public function __construct($custom = true) {
        
        if($custom){
            Twitter::reconfig([
                "consumer_key"      => $this->keys1234[0],
                "consumer_secret"   => $this->keys1234[1],
                "token"             => $this->keys1234[2],
                "secret"            => $this->keys1234[3]
            ]);
            MyConfig::setCountry('turkey1');
            $config = MyConfig::getCountry();
            $this->list['slug'] = $config['profile']['slug'];
            $this->list['owner_screen_name'] = $config['profile']['list_owner_screen_name'];
        }
        
        /*
         * config
         * getIds do 5000
         * hidrate do 100
         * redni_broj = -1 //uradjen hidrate
         * priority 0 init, 1 odbacen, 6 za ubacivanje
         * treba zaglavlje koliko kojih rezultata
         * //internetrebecca usa1
         * SophiePassmann, PatrickGensing, holgertma //de
         * benlaz61, haklisinyaren, madensuyu44, Mehmet31053845 //turkey
         */
        //$this->getIds('Mehmet31053845', 5000);
        //$this->hidrate(99);
        //$this->calcPriority();
        //$this->insert_scheduled();
    }
   
    /*
     * 
     * redni_br_for_insert = -1 //init, koji priority < 5, njima ostaje -1
     * redni_broj = -1 //init, koji nije hidratisan
     * priority init 0, 1, 6
     * 
     */
       
    public function insert_scheduled() {        
        $users_c = $this->loadUsers(); 
        $users_to_ins = $users_c->filter(function ($item) {           
                                return $item['priority'] > 5 && !$item['banned'] && !$item['inserted'];
                            })
                            ->sortBy('redni_br_for_insert')->values()
                            ->take(99);                            
        $user_ids = $users_to_ins->implode('id_str', ',');        
        $response = $this->add_list_members($this->list, $user_ids);        
        $users_to_ins = $users_to_ins->each(function ($item_in, $key_in) use ($users_c) {
            $index = $users_c->search(function ($item, $key) use ($item_in) {
                return $item['id_str'] == $item_in['id_str'];
            });
                
            $item_in1 = $users_c->get($index);
            $item_in1['inserted'] = true;    
            $users_c->put($index, $item_in1);
        });
        $this->saveUsers($users_c);              
    }
    
    //postavljaju ['header'] info
    public function loadUsers() {
        $exists = Storage::has($this->filename);
        $users_c = collect();
        if($exists) {
            $contents = Storage::get($this->filename);
            $users = json_decode($contents, true);
            $users_c = collect($users);
            if($users_c->has('header')) $users_c->forget('header');
        }         
        return $users_c;
    }
    public function saveUsers($users_c) {
        
        $header = ['ukupno' => 0, 'non_hidrated' => 0, 'inserted' => 0, 'p_6' => 0, 'p_1' => 0, 'p_0' => 0, 'banned' => 0];   
        $users_c = $users_c->map(function ($item, $key) use (&$header) {
                                    $item['index'] = $key;
                                    $header['ukupno']++;
                                    if($item['inserted']) $header['inserted']++;
                                    if($item['banned']) $header['banned']++;
                                    if($item['redni_broj'] > -1) $header['non_hidrated']++;
                                    if($item['priority'] == 0) $header['p_0']++;
                                    else if ($item['priority'] < 6 && $item['priority'] > 0) $header['p_1']++;
                                    else if($item['priority'] > 5) $header['p_6']++;                                    
                                    return $item;
                                }); 
        $users_c->prepend($header, 'header');
        $users_res = $users_c->all();
        $contents = json_encode($users_res, JSON_PRETTY_PRINT);
        Storage::put($this->filename, $contents);
    }
    public function calcPriority() {
        
    /*
     * aktivan zadnjih 10 dana
     * vise od 2k tvitova
     * vise od 200 pratilaca, manje od 20k
     * pratilaca> 0.9*pracenih //vazno, ako ne citaju njega nece ni tebe, polje, on filtrira
     * 
     */

        //vraca usera sa postavljenim priority, razlog_odbacivanja
        $criterium = function($user) {        
            switch (true)
            {
                //negativni uslovi
                case (new Carbon($user['last_active'])) < Carbon::now()->subDays(10) :
                     $user['priority'] = 1; //1 racunat nekad
                     $user['razlog_odbacivanja'] = 'nije titovao 10 dana';
                     break;

                case $user['statuses_count'] < 2000 :
                     $user['priority'] = 1; 
                     $user['razlog_odbacivanja'] = 'manje od 2k tvitova';
                     break;

                case $user['followers_count'] < 200 :
                     $user['priority'] = 1; 
                     $user['razlog_odbacivanja'] = 'manje od 200 pratilaca';
                     break;

                case $user['followers_count'] > 20000 :
                     $user['priority'] = 1; 
                     $user['razlog_odbacivanja'] = 'vise od 20k pratilaca';
                     break;

                case $user['followers_count'] < $user['friends_count']*0.9 :
                     $user['priority'] = 1; 
                     $user['razlog_odbacivanja'] = 'mnogo zapratio, prate ga: '.$user['followers_count'].', on prati: '.$user['friends_count'];
                     break;

                //proso
                default:
                    $user['priority'] = 6;
                    break;
            }
            return $user;
        };
    
        $users_c = $this->loadUsers();   
        $users_c = $users_c->map(function ($item, $key) use ($criterium) {
                                return $criterium($item);
                            }); 
        
        $sorted = $users_c->sortByDesc(function ($item, $key) {
            return $item['priority']*10*1000*1000 //criterium 1
                 + $item['followers_count']; //criterium 2
        })->values();
        
        $i=0;
        $users_c1 = $sorted->map(function ($item, $key) use (&$i) {
            if($item['priority'] > 5)
                $item['redni_br_for_insert'] = $i++;     
            return $item;
         }); 
        
        $this->saveUsers($users_c1);    
    }
    
    /*
     * hidrira koji nemaju postavljen screen_name
     * screen_name < 2
     */
    public function hidrate($count) {
        $users_c = $this->loadUsers(); 
        $ids_str = $users_c
                ->filter(function ($item) {           
                    return strlen($item['screen_name']) < 2;  
                }); //mora prvo ovo, dok su kljucevi ispremestani
                
        $ids_str = $ids_str->take($count)
                ->values()
                ->implode('id_str', ',');
  
        $users_h = $this->hidrate_user_ids($ids_str);
        $users_h_c = collect($users_h);
        $users_h_c = $users_h_c->each(function ($item_h, $key_h) use ($users_c) {
            $index = $users_c->search(function ($item, $key) use ($item_h) {
                return $item['id_str'] == $item_h['id_str'];
            });
            if(!self::array_keys_exist($item_h, ['screen_name', 'followers_count', 'statuses_count', 'status'])) {
                $users_c->pull($index);
                return;
            }                  
            $item_i = $users_c->get($index);
            $item_i['redni_broj']      = -1;    //da posle filtriras koji su od tekuceg usera
            $item_i['screen_name']     = $item_h['screen_name'];
            $item_i['followers_count'] = $item_h['followers_count'];
            $item_i['friends_count']   = $item_h['friends_count'];
            $item_i['statuses_count']  = $item_h['statuses_count'];
            $item_i['last_active']     = $item_h['status']['created_at'];
            $users_c->put($index, $item_i);
        });
        $this->saveUsers($users_c); 
    }
    
    public function getIds($screen_name, $count) {       
        $users_c = $this->loadUsers(); 
        //izbaci nevalidne        
        $users_c = $users_c->filter(function ($item) {
            return is_array($item) && self::array_keys_exist($item, $this->keys1);
        });
                
        $users = [];
        $ids_arr = $this->get_following_ids($screen_name, $count);        
        $i=0;
        foreach ($ids_arr as $id_str) {     
            //naopako args zapazi od 5.3
            $present = $users_c->contains(function ($item, $key) use ($id_str){
                return $item['id_str'] == $id_str;
            });
            if(!$present){
                $users[] = ['redni_broj' => $i++, 'id_str' => $id_str, 'screen_name' => '', 'followers_count' => '', 'friends_count' => '', 'statuses_count' => '', 'last_active' => '', 'priority' => 0, 'redni_br_for_insert' => -1, 'razlog_odbacivanja' => '', 'inserted' => false, 'banned' => false];
            }           
        }   
        $merged = $users_c->merge($users);
        $this->saveUsers($merged);
    }

    /*
     * sync with list bi trebalo
     */
    public function getIdsFromList($count, $recreate = true) {       
        $users_c = $this->loadUsers(); 
        //izbaci nevalidne        
        $users_c = $users_c->filter(function ($item) {
            return is_array($item) && self::array_keys_exist($item, $this->keys1);
        });
                     
        $users = [];
        $ids_arr = $this->get_list_members($this->list);        
        $i=0;
        foreach ($ids_arr as $id_str) {     
            //naopako args zapazi od 5.3
            $present = $users_c->contains(function ($item, $key) use ($id_str){
                return $item['id_str'] == $id_str;
            });
            if(!$present){
                if(!$recreate)
                    $users[] = ['redni_broj' => $i++, 'id_str' => $id_str, 'screen_name' => '', 'followers_count' => '', 'friends_count' => '', 'statuses_count' => '', 'last_active' => '', 'priority' => 0, 'redni_br_for_insert' => -1, 'razlog_odbacivanja' => '', 'inserted' => false, 'banned' => false];
                else
                    $users[] = ['redni_broj' => $i++, 'id_str' => $id_str, 'screen_name' => '', 'followers_count' => '', 'friends_count' => '', 'statuses_count' => '', 'last_active' => '', 'priority' => 7, 'redni_br_for_insert' => 0, 'razlog_odbacivanja' => '', 'inserted' => true, 'banned' => false];
            }           
        }   
        $merged = $users_c->merge($users);
        $this->saveUsers($merged);

    }
    
    public function get_list_members($list)
    {
        $params = [
            'slug'               => $list['slug'],
            'owner_screen_name'  => $list['owner_screen_name'],
            'format'             => 'array',
        ];
        $params = array_filter($params);
        $response = Twitter::getListMembers($params);
        return $response;
    }
    
    public function add_list_members($list, $user_ids)
    {
        $params = [
            'list_id'            => null,
            'slug'               => $list['slug'],
            'owner_screen_name'  => $list['owner_screen_name'],
            'owner_id'           => null,       
            'user_id'            => $user_ids, //comma separated ids
            'screen_name'        => null, //$screen_names, //comma separated names 
            'format'             => 'array',
        ];
        $params = array_filter($params);
        $response = Twitter::postListCreateAll($params);
        return $response;
    }
    
    public function add_list_members__($list, $user_ids)
    {
        $params = [
            'list_id'            => null,
            'slug'               => $list['slug'],
            'owner_screen_name'  => $list['owner_screen_name'],
            'owner_id'           => null,       
            'user_id'            => $user_ids, //comma separated ids
            'screen_name'        => null, //$screen_names, //comma separated names 
            //'format'             => 'array',
        ];
        $params = array_filter($params);

        Codebird::setConsumerKey($this->keys1234[0], $this->keys1234[1]); 
        $cb = Codebird::getInstance();
        $cb->setReturnFormat(CODEBIRD_RETURNFORMAT_ARRAY);
        $cb->setToken($this->keys1234[2], $this->keys1234[3]);
        //$cb->setProxy('127.0.0.1', '8888');
                
        $response = [];

        try
        {            
            $response = $cb->lists_members_createAll($params);    
        }
        catch (Exception $e)
        {       
            Log::error($e);
        }
        return $response;       
    }
    
    public function get_following_ids($screen_name, $count)
    {      
        $params = [            
            'screen_name'        => $screen_name, 
            'user_id'            => null,
            //'cursor'             => '1439732801410953509',
            'count'              => $count,
            'format'             => 'array',
        ];
        $params = array_filter($params);
        $response = Twitter::getFriendsIds($params);
        return $response['ids'];
    }
    public function hidrate_user_ids($user_ids)
    {        
        $params = [            
            'user_id'            => $user_ids,
            'screen_name'        => null, 
            'include_entities'   => null, //u tvitu hashevi i linkovi, ne treba     
            'format'             => 'array',
        ];
        $params = array_filter($params);
        $response = Twitter::getUsersLookup($params);
        return $response;
    }
    
    

    
        
    /**
     * Checks if multiple keys exist in an array
     *
     * @param array $array
     * @param array|string $keys
     *
     * @return bool
     */
    public static function array_keys_exist( array $array, $keys ) {
        $count = 0;
        if ( ! is_array( $keys ) ) {
            $keys = func_get_args();
            array_shift( $keys );
        }
        foreach ( $keys as $key ) {
            if ( array_key_exists( $key, $array ) ) {
                $count ++;
            }
        }
        return count( $keys ) === $count;
    }
    
    
    /*
     * $json_string = json_encode($data, JSON_PRETTY_PRINT);
     * 
     * 
     *                                         ->map(function ($item, $key) {
                                            return $item['id_str'];
                                        })
     * 
     */
    
    
    
    
} 
    /*   
     * 
     * uzmi od usera pracene 
     * baza da ne unosis istog 2x   
     * baza screen_name, id_str, list_id
     * pauza, shedule itd
     * filter aktivni, broj pratioca, tvitova, blacklisted...
     * bekap ids, ako suspenduju
     * 
     * 
     */
    
    /*
     * GET lists/members //clanovi liste
     * GET friends/ids //on prati
     * GET users/lookup  //za hidrate
     * GET followers/ids //njegovi pratioci
     * POST lists/members/create_all
     * 
     * 
     */




    /**
	 *  Parameters :
	 * - user_id
	 * - screen_name
	 * - include_entities (0|1)
	
	public function getUsersLookup($parameters = [])
    	
	 * Returns a cursored collection of user IDs for every user following the specified user.
	 *
	 * Parameters :
	 * - user_id
	 * - screen_name
	 * - cursor
	 * - stringify_ids (0|1)
	 * - count (1-5000)
	 
	public function getFriendsIds($parameters = [])
	{
		return $this->get('friends/ids', $parameters);
	}
    
    
    	/**
	 * Adds multiple members to a list, by specifying a comma-separated list of member ids or screen names. The authenticated user must own the list to be able to add members to it. Note that lists can’t have more than 5,000 members, and you are limited to adding up to 100 members to a list at a time with this method.
	 *
	 * Parameters :
	 * - list_id
	 * - slug
	 * - user_id
	 * - screen_name
	 * - owner_screen_name
	 * - owner_id
	 
	public function postListCreateAll($parameters = [])
	{
		if (!array_key_exists('list_id', $parameters) && !array_key_exists('slug', $parameters))
		{
			throw new Exception('Parameter required missing : list_id or slug');
		}

		return $this->post('lists/members/create_all', $parameters);
	}
    	/**
	 * Returns the members of the specified list. Private list members will only be shown if the authenticated user owns the specified list.
	 *
	 * Parameters :
	 * - list_id
	 * - slug
	 * - owner_screen_name
	 * - owner_id
	 * - cursor
	 * - include_entities (0|1)
	 * - skip_status (0|1)
	 
	public function getListMembers($parameters = [])
	{
		if (!array_key_exists('list_id', $parameters) && !array_key_exists('slug', $parameters))
		{
			throw new Exception('Parameter required missing : list_id or slug');
		}

		if (array_key_exists('slug', $parameters) && (!array_key_exists('owner_screen_name', $parameters) && !array_key_exists('owner_id', $parameters)))
		{
			throw new Exception('Parameter required missing : owner_screen_name or owner_id');
		}

		return $this->get('lists/members', $parameters);
	}
    
    */
    
    
    
    
    
