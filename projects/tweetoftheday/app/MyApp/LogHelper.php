<?php namespace App\MyApp;

use Carbon\Carbon;
use MyConfig;
use Log;
use View;


/**
 * Description of LogHelper
 *
 * @author username
 */
class LogHelper {

    //moze u debug vremena najmladjeg i najstarijeg tvita
    //oni ga izfejvuju za 10 minuta
    //da ne moze isti tvit da bude tv dana, juce...prvih 10 zadnjih 10 dana nista, u stvari svi rtovani
    //u stvari samo u rt prover da nisi vec rt i to je to
    //formater zeza za [][] u log
    
    //kolekcija update
    //delete, quote, pin
    //css za iframe profil
    /*
    function buildKeyFilter($keys) { return function($k) use ($keys) { return in_array($k, $keys); }; }

    $array = ['foo' => 'bar', 'baz' => 'bat', 'quux' => 'quuz'];
    $filterFor = ['foo', 'quux'];

    var_dump(array_filter($array, buildKeyFilter($filterFor), ARRAY_FILTER_USE_KEY));
     */

    //hvatanje izlaza viewa u promenljivu
    //$rendered = View::make(...)->render();
    
    //channel je zemlja, prosledjuje se u custom hendler
    //$model->context     = array_get($record, 'context');//zemlja
    

   /*
    * //results
    * $all_result['traverse_listtimeline']['all_results']
    * $all_result['traverse_listtimeline']['warning']
    * $all_result['rt_listtimeline']['rt_ed']
    * 
    * //debug
    * //in args
    * sve ['traverse_listtimeline']
    * $result_0['debug']['args']['time_h_m']
    * $result_0['debug']['args']['top_h']
    * $result_0['debug']['args']['bottom_h']
    * 
    * $result_0['debug']['izlaz']
    * $result_0['debug']['po_iteracijama']
    * $result_0['debug']['first_run']
    * 
    * $result_0['debug']['first_tw'] 
    * $result_0['debug']['last_tw']
    * $result_0['debug']['first_tw_id_str']
    * $result_0['debug']['last_tw_id_str']
    * $result_0['debug']['now']
    */ 
    public static function log_rt_listtimeline($all_r){
        
        $r_0 = $all_r['traverse_listtimeline'];
        $d_0 = $r_0['debug'];

        $p = [];
        //Zemlja: serbia1 , Level: INFO , Datetime UTC: Thu, 09 Feb 2017 20:54:03 +0000 , Datetime Belgrade +2: Thu Feb 9 21:54 
        $level = $r_0['warning'] ? 'WARNING' : 'INFO' ; $utc = Carbon::now('UTC')->toRfc1123String(); $bg_time = Carbon::now('Europe/Belgrade')->format('D M j H:i');
        $p[0] = ['value' => [0 => 'pocetak', 'zemlja' => $d_0['country'], 'Level' => $level, 'Datetime UTC' => $utc, 'Datetime Belgrade +2:' => $bg_time]];         
        //rt_listtimeline:::
        $p[1] = ['value' => [0 => 'rt_listtimeline:']];
        //args:: top_h: 0, bottom_h: 1, from time (top_h): 04:02 PM
        $p[2] = ['value' => [0 => 'args', 'top_h' => $d_0['args']['top_h'], 'bottom_h' => $d_0['args']['bottom_h'], 
                                  'from time (top_h)' => $d_0['args']['time_h_m']]];
        //status:: first_run: true, upozorenje: false 
        $warning = $r_0['warning'];
        $p[3] = ['value' => [0 => 'status', 'first_run' => $d_0['first_run'], 'warning' => $warning]];
        if($warning) $p[1]['class'] = 'bg-danger';
        //razlog izlaska:: izlaz, first_run=true, na vremenski limit. tw_time=02:59 PM > bottom4h=03:00 PM
        $p[4] = ['value' => [0 => 'razlog izlaska', 'izlaz' => $d_0['izlaz']]];
        $p[5] = ['class' => 'h-divider'];
        //rezultati:: ukupno tvitova: 131, rt-vanih: 12, br iteracija: 1, iteracije: iteracija0=131, 
        $iteracije = ''; foreach ($d_0['po_iteracijama'] as $iter){ $iteracije .= 'iteracija'. $iter['iteracija'].'='.$iter['broj_rezultata'].','; }
        $p[6] = ['value' => [0 => 'rezultati', 'ukupno tvitova' => count($r_0['all_results']),
                        'rt-vanih' => count($all_r['rt_listtimeline']['rt_ed']), 
                        'br iteracija' => count($d_0['po_iteracijama']), 'iteracije' => $iteracije]];
        //now: 04:04 PM, first_tw: 04:01 PM, last_tw: 03:00 PM, first_id_str: 783668530533236736, last_id_str: 783653013730824192
        $p[7] = ['value' => [0 => 'vremena', 'now' => $d_0['now'], 'first_tw' => $d_0['first_tw'], 'last_tw' => $d_0['last_tw'], 
                        'first_tw_id_str' => $d_0['first_tw_id_str'], 'last_tw_id_str' => $d_0['last_tw_id_str']]];
        $p[8] = ['class' => 'h-divider'];

        
        $rt_ed = self::trimTweetsArray($all_r['rt_listtimeline']['rt_ed']); //['all_results'] //za sve       

        $head   = View::make('admin.logs.render_head',   ['head'   => $p])->render();         
        $tweets = View::make('admin.logs.render_tweets', ['tweets' => $rt_ed])->render();        
        $item   = View::make('admin.logs.render_item',   ['head'   => $head, 'tweets' => $tweets])->render();

        $r_0['warning'] ? Log::warning($item) : Log::info($item);        
        return $item;
        
    }

    
    /*
     * //ova fja 
     * ['top_10_for_collection']
     * 
     * //result
     * $result_1['top_1_pinned']
     * $result_1['top_n_tweets']
     * $result_1['warining']
     * 
     * //debug
     * $result_1['debug']['izlaz']
     * $result_1['debug']['api_update_collection'] 
     * $result_1['debug']['api_pin_top_1'] 
     * 
     */ 
    public static function log_top_10_from_midnight_for_collection($all_r){

        $r_0 = $all_r['traverse_usertimeline'];
        $d_0 = $r_0['debug'];
        $r_1 = $all_r['top_10_for_collection'];
        $d_1 = $r_1['debug'];
        
        $cache_error = preg_match("/code=#cache/i", $d_1 ['izlaz']);
        
        $has_items = !$cache_error && isset($r_0) &&
                     isset($r_0['all_results']) && count($r_0['all_results']) > 0 &&
                     isset($r_0['top_n_tweets']) && count($r_0['top_n_tweets']) > 0;
        
        //ima gresku kad user nema tvitova u tom periodu
        if($has_items)
        {            
            $r_0['all_results'] = self::trimTweetsArray($r_0['all_results']);
            $r_0['top_n_tweets'] = self::trimTweetsArray($r_0['top_n_tweets']);  
            $r_1['top_1_pinned'] = self::trimTweetsArray([$r_1['top_1_pinned']]);
        } else {
            $r_0['top_n_tweets'] = [];
        }
        
        $is_warning = $r_0['warning'] || $r_1['warning'];
        
        $p = [];
        //Zemlja: serbia1 , Level: INFO , Datetime UTC: Thu, 09 Feb 2017 20:54:03 +0000 , Datetime Belgrade +2: Thu Feb 9 21:54 
        $level = $is_warning ? 'INFO' : 'WARNING'; $utc = Carbon::now('UTC')->toRfc1123String(); $bg_time = Carbon::now('Europe/Belgrade')->format('D M j H:i');
        $p[0] = ['value' => [0 => 'pocetak', 'zemlja' => $d_0['country'], 'Level' => $level, 'Datetime UTC' => $utc, 'Datetime Belgrade +2:' => $bg_time]];        
        //top_10_for_collection:::
        $p[1] = ['value' => [0 => 'top_10_for_collection:']];
        //razlog izlaska:: izlaz, sve uradio
        $p[2] = ['value' => [0 => 'razlog izlaska', 'izlaz' => $d_1['izlaz']]];
        //status:: top_10 upozorenje: false, api_update_collection: no, api_pin_top_1: no
        $warning = $r_1['warning'];
        $p[3] = ['value' => [0 => 'status', 'top_10 upozorenje' => $warning, 'api_update_collection' => $d_1['api_update_collection'], 'api_pin_top_1' => $d_1['api_pin_top_1'] ]];
        if($warning) $p[2]['class'] = 'bg-danger';
        //rezultati:: top_1_pinned: 0, top_n_tweets: 10
        $p[4] = ['value' => [0 => 'rezultati', 'top_1_pinned' => count($r_1['top_1_pinned']), 'top_n_tweets' => count($r_0['top_n_tweets'])]];
        $p[5] = ['class' => 'h-divider'];
        
        //traverse_usertimeline:::
        $p[6] = ['value' => [0 => 'traverse_usertimeline:']];
        //args:: from_date: 19:44, from_time_tw: 19:00, to_date: 00:00, top_n: 10
        $p[7] = ['value' => [0 => 'args', 'from_date' => $d_0['from_date'], 'from_time_tw' => $d_0['from_time_tw'], 'to_date' => $d_0['to_date'], 'top_n' => $d_0['top_n']]];
        //razlog izlaska:: izlaz, first_run=true, na vremenski limit. tw_time=23:54 > to_date=00:00        
        $p[8] = ['value' => [0 => 'razlog izlaska', 'izlaz' => $d_0['izlaz']]];
        //status:: traverse upozorenje: false
        $warning = $r_0['warning'];
        $p[9] = ['value' => [0 => 'status', 'warning' => $warning]];
        if($warning) $p[8]['class'] = 'bg-danger';
        //rezultati:: ukupno tvitova: 101, top_n_tweets: 10, br iteracija: 1, iteracije: iteracija0=101,
        $iteracije = ''; foreach ($d_0['po_iteracijama'] as $iter){ $iteracije .= 'iteracija'. $iter['iteracija'].'='.$iter['broj_rezultata'].','; }
        $p[10] = ['value' => [0 => 'rezultati', 'ukupno tvitova' => count($r_0['all_results']), 'top_n_tweets' => count($r_0['top_n_tweets']), 
                                'br iteracija' => count($d_0['po_iteracijama']), 'iteracije' => $iteracije]];
        //gornji i donji:: now: 19:44, first_tw: 18:56, last_tw: 00:05, first_id_str: 817429674875817985, last_id_str: 817145083023790082    
        $p[11] = ['value' => [0 => 'gornji i donji', 'now' => $d_0['now'], 'first_tw' => $d_0['first_tw'], 'last_tw' => $d_0['last_tw'], 
                        'first_tw_id_str' => $d_0['first_tw_id_str'], 'last_tw_id_str' => $d_0['last_tw_id_str']]];
        $p[12] = ['class' => 'h-divider'];
  
        $head   = View::make('admin.logs.render_head',   ['head'   => $p])->render();         
        $tweets = View::make('admin.logs.render_tweets', ['tweets' => $r_0['top_n_tweets']])->render();        
        $item   = View::make('admin.logs.render_item',   ['head'   => $head, 'tweets' => $tweets])->render();
        
        $is_warning ? Log::warning($item) : Log::info($item);        
        return $item;
    }

    /*
     * //ova fja
     * //result
     * ['top_3_yesterdays_for_fb']
     * $result_1['warining'] 
     * 
     * //debug
     * $result_1['debug']['izlaz']
     * $result_1['debug']['api_screenshot']
     * $result_1['debug']['api_posted_on_fb']
     * 
     */  
    public static function log_top_3_yesterdays_for_fb($all_r) {
        
        $r_0 = $all_r['traverse_usertimeline'];
        $d_0 = $r_0['debug'];
        $r_1 = $all_r['top_3_yesterdays_for_fb'];
        $d_1 = $r_1['debug'];
        
        $cache_error = preg_match("/code=#cache/i", $d_1['izlaz']);
        $has_items = !$cache_error && isset($r_0) &&
                     isset($r_0['all_results']) && count($r_0['all_results']) > 0 &&
                     isset($r_0['top_n_tweets']) && count($r_0['top_n_tweets']) > 0;
               
        if($has_items)
        {
            $r_0['all_results'] = self::trimTweetsArray($r_0['all_results']);
            $r_0['top_n_tweets'] = self::trimTweetsArray($r_0['top_n_tweets']); 
        } else {
            $r_1['top_n_tweets'] = []; 
        }

        $is_warning = $r_0['warning'] || $r_1['warning'];
        
        $p = [];
        //Zemlja: serbia1 , Level: INFO , Datetime UTC: Thu, 09 Feb 2017 20:54:03 +0000 , Datetime Belgrade +2: Thu Feb 9 21:54 
        $level = $is_warning ? 'INFO' : 'WARNING'; $utc = Carbon::now('UTC')->toRfc1123String(); $bg_time = Carbon::now('Europe/Belgrade')->format('D M j H:i');
        $p[0] = ['value' => [0 => 'pocetak', 'zemlja' => $d_0['country'], 'Level' => $level, 'Datetime UTC' => $utc, 'Datetime Belgrade +2:' => $bg_time]];        
        //top_3_yesterdays_for_fb:::
        $p[1] = ['value' => [0 => 'top_3_yesterdays_for_fb:']];
        //razlog izlaska:: izlaz, sve uradio
        $p[2] = ['value' => [0 => 'razlog izlaska', 'izlaz' => $d_1['izlaz']]];
        //status:: top_3 upozorenje: false, api_screenshot: no, api_posted_on_fb: no
        $warning = $r_1['warning'];
        $p[3] = ['value' => [0 => 'status', 'top_3 upozorenje' => $warning, 'api_screenshot' => $d_1['api_screenshot'], 'api_posted_on_fb' => $d_1['api_posted_on_fb'] ]];
        if($warning) $p[2]['class'] = 'bg-danger';
        //rezultati:: top_1_pinned: 0, top_n_tweets: 10
        $p[4] = ['value' => [0 => 'rezultati', 'top_n_tweets' => count($r_0['top_n_tweets'])]];
        $p[5] = ['class' => 'h-divider'];
        
        $head   = View::make('admin.logs.render_head',   ['head'   => $p])->render();         
        $tweets = View::make('admin.logs.render_tweets', ['tweets' => $r_0['top_n_tweets']])->render();        
        $item   = View::make('admin.logs.render_item',   ['head'   => $head, 'tweets' => $tweets])->render();
        
        $is_warning ? Log::warning($item) : Log::info($item);        
        return $item;  
    }
    
    /*  
     * //ova fja
     * //result
     * ['top_1_of_last_run']
     * $result_1['top_n']
     * 
     * //debug
     * $result_1['warining']
     * $result_1['debug']['izlaz']
     * $result_1['debug']['api_screenshot'] 
     * $result_1['debug']['api_posted_on_fb'] 
     * 
     */ 
    public static function log_top_1_of_last_run_for_fb($all_r){

        $r_1 = $all_r['top_1_of_last_run'];
        $d_1 = $r_1['debug'];
        
        $cache_error = preg_match("/code=#cache/i", $d_1['izlaz']); 
        if(!$cache_error && count($r_1['top_n']) > 0)
        {           
            $r_1['top_n'] = self::trimTweetsArray($r_1['top_n']);   
        } else {
            $r_1['top_n'] = []; 
        }

        $is_warning = $r_1['warning'];
        
        $p = [];
        //Zemlja: serbia1 , Level: INFO , Datetime UTC: Thu, 09 Feb 2017 20:54:03 +0000 , Datetime Belgrade +2: Thu Feb 9 21:54 
        $level = $is_warning ? 'INFO' : 'WARNING'; $utc = Carbon::now('UTC')->toRfc1123String(); $bg_time = Carbon::now('Europe/Belgrade')->format('D M j H:i');
        $p[0] = ['value' => [0 => 'pocetak', 'zemlja' => $d_1['country'], 'Level' => $level, 'Datetime UTC' => $utc, 'Datetime Belgrade +2:' => $bg_time]];        
        //top_1_of_last_run:::
        $p[1] = ['value' => [0 => 'top_1_of_last_run:']];
        //razlog izlaska:: izlaz, sve uradio
        $p[2] = ['value' => [0 => 'razlog izlaska', 'izlaz' => $d_1['izlaz']]];
        //status:: top_1 upozorenje: false, api_update_collection: no, api_pin_top_1: no
        $warning = $r_1['warning'];
        $p[3] = ['value' => [0 => 'status', 'top_1 upozorenje' => $warning, 'api_screenshot' => $d_1['api_screenshot'], 'api_posted_on_fb' => $d_1['api_posted_on_fb'] ]];
        if($warning) $p[2]['class'] = 'bg-danger';
        //rezultati:: top_1_pinned: 0, top_n_tweets: 10
        $p[4] = ['value' => [0 => 'rezultati', 'top_n' => count($r_1['top_n'])]];
        $p[5] = ['class' => 'h-divider'];
        
        $head   = View::make('admin.logs.render_head',   ['head'   => $p])->render();         
        $tweets = View::make('admin.logs.render_tweets', ['tweets' => $r_1['top_n']])->render();        
        $item   = View::make('admin.logs.render_item',   ['head'   => $head, 'tweets' => $tweets])->render();       

        $is_warning ? Log::warning($item) : Log::info($item);        
        return $item; 
        
    }


    public static function trimTweetsArray($arr) {
        if(is_array($arr) && count($arr) > 1) {
            return array_map(function($tweet){
                return self::trimTweet($tweet);
            }, $arr);    
        } else {
            return [];
        }
    }
    
    public static function trimTweet($tweet) {

        return [
            //by order
            'fv'             => $tweet['favorite_count'],
            'rt'             => $tweet['retweet_count'],
            'sc_name'        => $tweet['user']['screen_name'],  
            'retweeter'      => array_key_exists ('retweeter', $tweet) ? $tweet['retweeter'] : '',
            'text'           => $tweet['text'], //preg_replace('/\s+/', ' ', $tweet['text']), //remove newlines i spaces

            //less important
            'name'           => $tweet['user']['name'],
            'created_at'     => $tweet['created_at'],
            'id_str'         => $tweet['id_str'],
            'user_id'        => $tweet['user']['id_str'],
            'tw_link'        => 'https://twitter.com/'.$tweet['user']['screen_name'].'/status/'.$tweet['id_str'],
            'user_link'      => 'https://twitter.com/'.$tweet['user']['screen_name']
        ];
    }
    
    public static function trimUsersFromTweetsArray($arr) {
            return array_map(function($tweet){
                return self::trimUser($tweet['user']);
            }, $arr);    
    }
    public static function trimUser($user) {
        $x=3;
        if(!array_key_exists ('profile_banner_url', $user))
            $user['profile_banner_url'] = '';
        
        return [
            'screen_name'        => $user['screen_name'],
            'name'               => $user['name'],
            'followers_count'    => $user['followers_count'],  
            'friends_count'      => $user['friends_count'], 
            'statuses_count'     => $user['statuses_count'],
            'profile_banner_url' => $user['profile_banner_url'], //samo ako je postavljen 
            'profile_image_url'  => $user['profile_image_url'], 
        ];
    }
 
}
        /*
            <div class="h-divider-thick"></div>
            <p <% $log->level > 200 ? 'class=bg-warning' : '' %> >
                <strong>Zemlja: <% $log->channel %></strong>
                <strong>, Level: </strong><% $log->level_name %>
                <strong>, Datetime UTC: </strong><% (new \Carbon\Carbon($log->generated))->toRfc1123String() %>                
                <strong>, Datetime Belgrade +2: </strong><% (new \Carbon\Carbon($log->generated))->setTimezone('Europe/Belgrade')->format('D M j H:i') %>

            </p>
        */