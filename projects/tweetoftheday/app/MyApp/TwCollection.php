<?php namespace App\MyApp;

use App\MyApp\TwApi;

//pin preko phantoma

class TwCollection {


    public static function updateCollection($collection_id, $new_top10) 
    {        
        $old_top10 = TwApi::get_collections_tweets($collection_id);
        $new_top10 = collect($new_top10)->sortBy('favorite_count')->values()->all();

        $old_top10_ids = array_keys(self::idsFromTweets($old_top10));
        $new_top10_ids = self::idsFromTweets($new_top10);

        $response = TwApi::curate_collection($collection_id, $new_top10_ids, $old_top10_ids);//add, remove ids
    }
    
    public static function updateCollection__($collection_id, $new_top10) 
    {        
        $old_top10 = TwApi::get_collections_tweets($collection_id);
        $old_top10 = collect($old_top10)->sortByDesc('favorite_count')->values()->all();

        $old_top10_ids = self::idsFromTweets($old_top10);
        $new_top10_ids = self::idsFromTweets($new_top10);

        $old_top10_ids1 = array_filter($old_top10_ids, function($elem) use($new_top10_ids){
                                        return !in_array($elem, $new_top10_ids); });
        $new_top10_ids1 = array_filter($new_top10_ids, function($elem) use($old_top10_ids){
                                        return !in_array($elem, $old_top10_ids);});
        //ce da treba redosled
        //i proverava da ima 10
        //samo odmah posle ponoci nema 10 pa dopuni postojecim
        $response = TwApi::curate_collection($collection_id, $new_top10_ids1, $old_top10_ids1);//add, remove ids
    }
        
    //na gore, pa na dole
    //svedi ih na redne brojeve 0 1 2 3 4 5 6 7 8 9
    //imam 2 5 8 9
    //treba se ubaci na 0 1 3 4 6 7 
    // idi do 2 ubaci 1 0
    //indeksi treba da se prevedu u insertAfter i insertBefore

    public static function updateCollection1($collection_id, $new_top10) 
    {        
        $old_top10 = TwApi::get_collections_tweets($collection_id);
        $old_top10 = collect($old_top10)->sortByDesc('favorite_count')->values()->all();

        $old_top10_ids = self::idsFromTweets($old_top10);
        $new_top10_ids = self::idsFromTweets($new_top10);

        $d_r = self::calculateCollection($old_top10, $new_top10);
        
        if(false && !$d_r['replace_all']) {
        
            //remove
            foreach ($d_r['remove'] as $tweet_id) 
                $response1 = TwApi::add_or_remove_to_collection('remove', $collection_id, $tweet_id);

            //insert
            if(isset($d_r['insert_after']))
            foreach ($d_r['insert_after'] as $item) 
                $response1 = TwApi::add_or_remove_to_collection('add', $collection_id, $item['tweet_id'], $item['pointer'], false);

            //before
            if(isset($d_r['insert_before']))
            foreach ($d_r['insert_before'] as $item) 
                $response1 = TwApi::add_or_remove_to_collection('add', $collection_id, $item['tweet_id'], $item['pointer'], true);      
        } else {
            
            $response1 = TwApi::curate_collection($collection_id, $new_top10_ids, $old_top10_ids);//add, remove ids
            /*           
            foreach ($old_top10_ids as $tweet_id) 
                $response1 = TwApi::add_or_remove_to_collection('remove', $collection_id, $tweet_id);
            
            $relative_to = null;
            $above = null;
            foreach ($new_top10_ids as $tweet_id) {
                $response1 = TwApi::add_or_remove_to_collection('add', $collection_id, $tweet_id, $relative_to, $above);
                $relative_to = $tweet_id;
                $above = false;
            }
            */
        }
        return true;//uspeh
    }
    
    public static function idsFromTweets($tw_arr) 
    {   
        $id_arr = array_map(function($item) { return $item['id_str']; }, $tw_arr);
        return $id_arr;
    }
    public static function favFromId($tw_arr, $id) 
    {   
        $tw_id_arr = self::idsFromTweets($tw_arr);
        $index = array_search($id, $tw_id_arr);
        return $tw_arr[$index]['favorite_count'];
    }
    //samo polja id-ova, opadajuca, ne rastuca
    public static function calculateCollection($old_top10, $new_top10) {

        $d_r = [];
        $d_r['replace_all'] = false;        
        $d_r['debug']['izlaz'] = 'init ok';
        $d_r['remove'] = [];
        
        $old_top10_ids = self::idsFromTweets($old_top10);
        $new_top10_ids = self::idsFromTweets($new_top10);
                
        $old = var_export($old_top10_ids, true);
        $new = var_export($new_top10_ids, true);
        //remove no longer in top 10
        $to_remove = array_diff($old_top10_ids, $new_top10_ids);//old - new        
        foreach ($to_remove as $tweet_id)
            $d_r['remove'][] = $tweet_id;

        $all_reused = array_diff($old_top10_ids, $to_remove);//keep = old - to_remove
        $reused = self::lis($all_reused);   //najduza podsekvenca
        
        //remove prisutne ali van rasporeda
        $non_reused = array_diff($old_top10_ids, $reused);  //remove van podsekvence      
        foreach ($non_reused as $tweet_id)
            if(!in_array($tweet_id, $d_r['remove']))
                $d_r['remove'][] = $tweet_id;
        
        //izbaci sve i ubaci nove
        if(count($reused) < 2) {
            $d_r['debug']['izlaz'] = 'sekvenca < 2';
            $d_r['replace_all'] = true;
            return $d_r;
        }
        
        $reused1 = var_export($reused, true);
        $new1 = var_export($new_top10_ids, true);
        
        $j=0;    
        $fm = array_search($reused[$j], $new_top10_ids);//returns the first corresponding key
        
        $pointer = $reused[$j];  
        for($i1 = $fm; $i1 < count($new_top10_ids); $i1++){
            if(!in_array($new_top10_ids[$i1], $reused)) {
                $id = $new_top10_ids[$i1];
                $fav = self::favFromId($new_top10, $id);
                $d_r['insert_before'][] = ['pointer' => $pointer, 'tweet_id' => $id, 'fav' => $fav];
                $pointer=$new_top10_ids[$i1];
            } else {                              
                $pointer=$reused[$j];
                $j++;
            }
        }
        $pointer = $reused[0]; 
        for($i2 = $fm; $i2 > 0; $i2--){
            $id = $new_top10_ids[$i2-1];
            $fav = self::favFromId($new_top10, $id);
            $d_r['insert_after'][] = ['pointer' => $pointer, 'tweet_id' => $id, 'fav' => $fav];           
            $pointer=$new_top10_ids[$i2];
        }
        return $d_r;
    }
    
    //longest increasing subsequence
    public static function lis($in_arr) {
        //opadajucu
        $n = array_reverse($in_arr);
        
        $pileTops = array();
        // sort into piles
        foreach ($n as $x) {
            // binary search
            $low = 0; $high = count($pileTops)-1;
            while ($low <= $high) {
                $mid = (int)(($low + $high) / 2);
                if ($pileTops[$mid]->val >= $x)
                    $high = $mid - 1;
                else
                    $low = $mid + 1;
            }
            $i = $low;
            $node = new Node();
            $node->val = $x;
            if ($i != 0)
                $node->back = $pileTops[$i-1];
            $pileTops[$i] = $node;
        }
        $result = array();
        for ($node = count($pileTops) ? $pileTops[count($pileTops)-1] : NULL;
            $node != NULL; $node = $node->back)
            $result[] = $node->val;

        return $result; //array_reverse($result);
    }
}

class Node {
    public $val;
    public $back = NULL;
}




