<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MyConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('MyConfig', function()
        {
            return new \App\MyApp\Config\MyConfig;
        });
    }
}
