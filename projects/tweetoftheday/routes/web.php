<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('listtimeline_run_serbia1', function () {
    $exitCode = Artisan::call('listtimeline:run', ['country' => 'serbia1']);  
});
Route::get('top_10_from_midnight_for_collection_serbia1', function () { 
     $exitCode = Artisan::call('usertimeline:run', ['country' => 'serbia1', 'functionname' => 'top_10_from_midnight_for_collection'/*, 'screen_name' => 'zareseizigro'*/]);
});

Route::get('listtimeline_run_usa1', function () {
    $exitCode = Artisan::call('listtimeline:run', ['country' => 'usa1']);  
});

Route::get('listtimeline_run_germany1', function () {
    $exitCode = Artisan::call('listtimeline:run', ['country' => 'germany1']);  
});

Route::get('listtimeline_run_turkey1', function () {
    $exitCode = Artisan::call('listtimeline:run', ['country' => 'turkey1']);  
});




//Route::get('/', 'MyLayoutController@index');
//Route::get('/countries/{country?}', 'MyLayoutController@country');
Route::get('/logs/{country?}', 'LogController@index')->name('log.show');

Route::any('adminer', '\Miroc\LaravelAdminer\AdminerController@index');
//Route::any('/adminer', '\Miroc\LaravelAdminer\AdminerAutologinController@index');

Route::get('/', function () {
    return view('mylayout');
});


/*
Route::get('/partials/index', function () {
    return view('partials.index');
});

//partial html vuce angular
//i to nisu blade templatei nego angular templatei
Route::get('/partials/{category}/{action?}', function ($category, $action = 'index') {
    return view(join('.', ['partials', $category, $action]));
});

Route::get('/partials/{category}/{action}/{id}', function ($category, $action = 'index', $id) {
    return view(join('.', ['partials', $category, $action]));
});

//rute za json api, bez view
// Additional RESTful routes.
Route::post('/api/user/login', 'UserController@login');
Route::get('/api/user/getByToken', 'UserController@getByToken');

// Getting RESTful
Route::resource('/api/todo', 'TodoController');
Route::resource('/api/user', 'UserController');



// Catch all undefined routes. Always gotta stay at the bottom since order of routes matters.
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('layout');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');

*/
// Using different syntax for Blade to avoid conflicts with AngularJS.
// You are well-advised to go without any Blade at all.
//Blade::setEscapedContentTags('<%%', '%%>'); 
//Blade::setContentTags('<%', '%>');
//Blade::setRawTags('<%%', '%%>');
