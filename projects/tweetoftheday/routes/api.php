<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//dodaje /api automatski
//http://zemke53.app/api/getTop3Users/serbia1
Route::get('/getTop3Users/{country}', 'MyLayoutController@getTop3Users');
//http://zemke53.app/api/countries
Route::get('/countries', 'MyLayoutController@countries');

Route::post('/user/login', 'UserController@login');
Route::get('/user/getByToken', 'UserController@getByToken');

Route::resource('/todo', 'TodoController');
Route::resource('/user', 'UserController');

