# Tweet of the day

This application is a bot for traversing Twitter timelines based on real world Twitter API response and not just API docs specs since responses tend to vary depending of combination params used, traffic load etc... This can be used to filter Twitter with custom queries that are not supported in `GET search/tweets` for example.

For now are implemented trvaresing user and list timelines because I only had need for those, but it can be abstracted to traverse any Twitter timeline. 

# Demo  

Serbian and Turkish bots are still alive and available here: **[@totd_rs](https://twitter.com/totd_rs)** and **[@totd_tr](https://twitter.com/totd_tr)**.

# Screenshots  

The application consists of Twitter bot, Facebook bot that isn't present on the screenshots but is implemented, one page with curated timelines, and the logs.  

![Screenshot log](Screenshot_1.png)
![Screenshot 2](Screenshot_2.png)
![Screenshot 3](Screenshot_3.png)

# Features  

### MyConfig service

Main entity application is working with is profile (Profil_tb), and that profile can have timelines that can be traversed (user timeline, list timeline, etc). Data about profiles are stored in `/app/MyApp/Config/config.php`. All data about profile are constants except cursors for timelines. That means this data can be used directly from this array and there is no need for database nor Eloquent with the exception of cursors stored in cache with "keep forever" flag. 

This way of thinking is supported with the fact that all relevant data are on Twitter and any kind of database will behave as cache causing consistency issues. Another argument is that this is realtime application and all data have short-term relevance. But for now data from config service is used as database seed in `/database/seeds/DatabaseSeeder.php` , and accessed trough Eloquent API in the rest of the application. Main reason for this is possibility that this data will be used in intersections with some future tables.

### Traversing timelines

To traverse timeline we need 3 cursors `max_id`, `since_id` and `absolute_since_id` wich are top, bottom, and "top from last run" cursors. As [GET lists/statuses](https://dev.twitter.com/rest/reference/get/lists/statuses) and [GET statuses/user_timeline](https://dev.twitter.com/rest/reference/get/statuses/user_timeline) dont offer passing times as input arguments we use workaround using [@big_ben_clock](https://twitter.com/big_ben_clock) account to translate times to id's, this can be seen in `/app/MyApp/TwApi.php` in `bben_index_from_datetime()` function.

Traversing functionality is implemented for list and user timelines and can be found in `/app/MyApp/SlideListTimeline.php` and `/app/MyApp/SlideUserTimeline.php`. 

#### List timeline  

It consists of 2 functions `init_cursors_listtimelne()` to set top and bottom cursors and  `go_back()` to do a traverse. Intent is to pass all tweets in timeline with handling edge cases esspecially determining criterium for breaking the loop, and these are so far:

- for loop has limmited number of itterations
- Twitter API returned 0 results or exception
- tweet from response is older than specified date
- tweet from response has lower id than top id from last run

All criteriums are read from config array via `MyConfig::` facade. Tweets are processed in every iteration with `process_timeline_response()`.

#### User timeline  

User timeline has slightly different requirements, bottom is limited only by date and unaware of last run and it does not process tweets in each itteration but merge them and return as array. Analog functions are `init_cursors_usertimelne_max_id()` for init cursors and `get_n_best_tweets_from_usertimelne_max_id()` for traversing.

At this moment all behaviour is logged using default monolg LineFormatter but this will be changed for easier application tracking.

### Twitter API repository

In `/app/MyApp/TwApi.php` is `TwApi` class which is repository for all Twitter API calls. It is using `Twitter::` facade, does exception handling and `GET statuses/user_timeline` for `@big_ben_clock` is cached for 1 hour using default `Cache::` facade. There is also Twitter collections support which will be displayed and styled in the view via Angular directive.

### Scheduler

In `/app/Console/Kernel.php` is implemented scheduler which will call traversing a list every hour, traversing user timeline once per day, and once per hour.


### Features to implement in the future:

- write tests, now testing functions are called in dafault route
- write json monolog formatter and display data via long pulling and templates
- integrate angular directive to display collections feeds from Twitter
- add blacklisted users support in config and database
- separate env configurations for Postgre and MySQL, Docker and Heroku, etc
 


# Requirements

- php 5.5 or higher
- MySQL 5.6 or higher
- PostgreSQL 9.1 or higher
- Composer, Bower, npm

# Used libs

- Laravel 5.1 Angular 1.3 starter project [Zemke/starter-laravel-angular](https://github.com/Zemke/starter-laravel-angular)
- Twitter API package for Laravel [thujohn/twitter](https://github.com/thujohn/twitter)
- Twitter timeline Angular directive [userapp-io/twitter-timeline-angularjs](https://github.com/userapp-io/twitter-timeline-angularjs)


# Installation and running  

1. create database and set connection data in `.env` file
2. run migrations with `php artisan migrate`
3. seed database with `php artisan db:seed`
4. running: in `/app/Http/routes.php` uncomment desired functionality and visit `/` route in the browser.

# Project status

Active and in development. This is current snapshot of my mini start-up project in development.


# Licence  

Use this only as portfolio item in order to determine my programming expertise level. Do not use or distribute. Author: `nemanja.mitic.elfak@hotmail.com` spring 2016.

