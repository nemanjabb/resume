<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;

/*
    $app->configureMonologUsing(function($monolog) {
        $handler = new RotatingFileHandler(storage_path().'/logs/timeline.html', Logger::DEBUG);
        
        // the default date format is "Y-m-d H:i:s"
        //http://www.php.net/manual/en/class.datetime.php //formati
        $dateFormat = "d.m.Y. H:i:s O";
        // the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
        $output = "[%datetime%] %channel%.%level_name% %message%".PHP_EOL; // %context% %extra% ova 2 pravila [] []
        // finally, create a formatter
        $formatter = new LineFormatter($output, $dateFormat);
        $formatter->allowInlineLineBreaks(true);
        $monolog->pushHandler($handler->setFormatter($formatter));               
    });
 * $monolog->withName('security');//postavljanje channel-a 
*/
