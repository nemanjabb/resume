var elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
  mix
    .less([
      'bootstrap.less', //on povlaci bootstrap.css//kompajlira
      'flag-icon.less', 
    ], 'public/css/bootstrap.css')
    .styles([
      'theme style.css',
      'directive style.css',
      'profilecard style.css'
    ], 'public/css/mycss.css') //iza polja[] prosledis ime fajla gde da ga stavi
    .scripts([
      '../../../bower_components/jquery/dist/jquery.js',
      '../../../bower_components/angular/angular.js',
      '../../../bower_components/ngstorage/ngStorage.js',
      '../../../bower_components/angular-route/angular-route.js',
      '../../../bower_components/angular-resource/angular-resource.js',
      '../../../bower_components/bootstrap/dist/js/bootstrap.js'//,
      //'../../../bower_components/ngtweet/dist/ngtweet.min.js'
    ], 'public/js/libs.js')
    .scripts([
      'page1/app.js',
      'page1/appRoutes.js',
      'page1/controllers/**/*.js',
      'page1/services/**/*.js',
      'page1/directives/**/*.js'  
    ], 'public/js/page1.js')
    .scripts([
      'page2/app.js',
      'page2/appRoutes.js',
      'page2/controllers/**/*.js',
      'page2/services/**/*.js',
      'page2/directives/**/*.js'  
    ], 'public/js/page2.js')
    .version([
      'css/bootstrap.css',
      'css/mycss.css',
      'js/libs.js',
      'js/page1.js',
      'js/page2.js'
    ])
    .copy('public/js/libs.js.map'               , 'public/build/js/libs.js.map')
    .copy('public/js/page1.js.map'              , 'public/build/js/page1.js.map')
    .copy('public/js/page2.js.map'              , 'public/build/js/page2.js.map')
    .copy('public/css/bootstrap.css.map'        , 'public/build/css/bootstrap.css.map')
    .copy('public/css/mycss.css.map'            , 'public/build/css/mycss.css.map')
    .copy('bower_components/bootstrap/fonts/**' , 'public/build/fonts')
    .copy('bower_components/flag-icon-css/flags/4x3**' , 'public/build/fonts/flags/')

    //.livereload();
});
