<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * UserTb
 */
class UserTb
{
    /**
     * @var string
     */
    private $screenName;

    /**
     * @var string
     */
    private $json;

    /**
     * @var \DateTime
     */
    private $dateChecked;

    /**
     * @var integer
     */
    private $reqCount;

    public function __construct($arr){
        $this->screenName = $arr['screenName'];
        $this->json = $arr['json'];
        $this->dateChecked = $arr['dateChecked'];
        $this->reqCount = $arr['reqCount'];
    }

    /**
     * Set screenName
     *
     * @param string $screenName
     * @return UserTb
     */
    public function setScreenName($screenName)
    {
        $this->screenName = $screenName;

        return $this;
    }

    /**
     * Get screenName
     *
     * @return string 
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * Set json
     *
     * @param string $json
     * @return UserTb
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Get json
     *
     * @return string 
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Set dateChecked
     *
     * @param \DateTime $dateChecked
     * @return UserTb
     */
    public function setDateChecked($dateChecked)
    {
        $this->dateChecked = $dateChecked;

        return $this;
    }

    /**
     * Get dateChecked
     *
     * @return \DateTime 
     */
    public function getDateChecked()
    {
        return $this->dateChecked;
    }

    /**
     * Set reqCount
     *
     * @param integer $reqCount
     * @return UserTb
     */
    public function setReqCount($reqCount)
    {
        $this->reqCount = $reqCount;

        return $this;
    }

    /**
     * Get reqCount
     *
     * @return integer 
     */
    public function getReqCount()
    {
        return $this->reqCount;
    }
}
