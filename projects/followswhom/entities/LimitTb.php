<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * LimitTb
 */
class LimitTb
{
    /**
     * @var string
     */
    private $userId;
    /**
     * @var string
     */
    private $api;
    /**
     * @var integer
     */
    private $reqCount;

    /**
     * @var \DateTime
     */
    private $checkpoint;

    public function __construct($arr){
        $this->userId = $arr['userId'];
        $this->api = $arr['api'];
        $this->checkpoint = $arr['checkpoint'];
        $this->reqCount = $arr['reqCount'];
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return LimitTb
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set api
     *
     * @param string $api
     * @return LimitTb
     */
    public function setApi($api)
    {
        $this->api = $api;

        return $this;
    }

    /**
     * Get api
     *
     * @return string 
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * Set reqCount
     *
     * @param integer $reqCount
     * @return LimitTb
     */
    public function setReqCount($reqCount)
    {
        $this->reqCount = $reqCount;

        return $this;
    }

    /**
     * Get reqCount
     *
     * @return integer 
     */
    public function getReqCount()
    {
        return $this->reqCount;
    }

    /**
     * Set checkpoint
     *
     * @param \DateTime $checkpoint
     * @return LimitTb
     */
    public function setCheckpoint($checkpoint)
    {
        $this->checkpoint = $checkpoint;

        return $this;
    }

    /**
     * Get checkpoint
     *
     * @return \DateTime 
     */
    public function getCheckpoint()
    {
        return $this->checkpoint;
    }
}
