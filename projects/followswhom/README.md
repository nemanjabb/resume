

# Follows whom  

This application utilize feature of [GET friends/ids](https://dev.twitter.com/rest/reference/get/friends/ids) that it is returning followers id's ordered chronologically so you can see who someone followed most recently. Then it uses [GET users/lookup](https://dev.twitter.com/rest/reference/get/users/lookup) to hydrate those id's into user objects suitable for calculating stats and rendering. At the end it uses [GET users/show](https://dev.twitter.com/rest/reference/get/users/show) to get basic info about user in question.

# Demo  

At the time of writing this document working demo is available on [https://followswhom.herokuapp.com/](https://followswhom.herokuapp.com/) .

# Screenshots 

#### App  

![Screenshot App](Screenshot_1.png)  

#### Log  

![Screenshot Log](Screenshot_2.png) 


# Features  

Application is made of 4 services and which route is executed is determined with GET key in the request `validate_user`, `screen_name`, `limits`, `links`.  

#### Login  

Although application can be used without logging in, it will call API's on behalf of default application account. With user logging in calls are on user's behalf meaning more calls on app level until limit. For OAuth login is used authorization flow from  [abraham/twitteroauth](https://github.com/abraham/twitteroauth). On the view you can see this functionality marked with **1**.

#### Validate search query

Search field has server side validation trough separate service performing HEAD http request on that username profile and checking for 200 status code which was enough at the time when it was made. On screenshot this is marked with **2**.

#### Following users structure 

Main functionality consists of getting last 100 results with `GET friends/ids`, hydrating them with  `GET users/lookup` trimming and sending data trough separate service to browser where histogram with following structure break down, and chronologically ordered table with followed users data are rendered. For histogram is used [highcharts/highcharts](https://github.com/highcharts/highcharts), and for all tables on page [wenzhixin/bootstrap-table](https://github.com/wenzhixin/bootstrap-table) lib. Also to save API calls results are cached into database if query is more recent than 2 days, and last result is cached into PHP session to be returned in case of API limit is exceeded. On screenshot this is marked with **3** and **4**.

#### API limit info

Another separate service is responsible for feeding data about remaining limits for all 3 API's used in app for currently logged in user or default app account. This data is persisted into database for each user or kept in session for default account and is being reset to default values as found in API docs. On screenshot this is marked with **5**.

#### Recent searches

Last service is responsible for data about recently searched usernames, it has two modes: most recent and most wanted which can be selected via radio button in the bottom. On screenshot this is marked with **6**.

#### Logging  

On the path `/src/loged/vidilog.php` can be found table with logged queries and exceptions.

# Installation on Heroku  

- make new Heroku application, clone this repo and push it on Heroku
- use `mydb.dump` file or if you already have it installed export postgre data with 
~~~~
pg_dump -Fc --no-acl --no-owner --format=c -h localhost -U postgres followswhom > mydb.dump
~~~~

- create postgre database on Heroku with
~~~~
heroku addons:create heroku-postgresql:hobby-dev
~~~~

- upload `mydb.dump` somewhere and imoprt it with (replace with your url)
~~~~
heroku pg:backups restore 'https://github.com/nemanjam/test/raw/master/mydb.dump' DATABASE_URL
~~~~

# Code guide

Main backend functionality is in the `/src` folder, Doctrine mappings are in `/config/yaml`, database connection in `bootstrap.php` and JavaScript logic is in the `\src\html\script.js` file.


# Used libs

- Bootstrap 3 template
- jQuery
- [wenzhixin/bootstrap-table](https://github.com/wenzhixin/bootstrap-table)
- [highcharts/highcharts](https://github.com/highcharts/highcharts)
- [abraham/twitteroauth](https://github.com/abraham/twitteroauth)
- Doctrine 2.4


# Project status

This was made in spring 2015. unplanned and like as soon as possible prototype trial balloon to test if people will find this interesting and useful. I got this idea while reading API docs for `GET friends/ids` and fact that it is returning chronologically ordered results. Current project status is **deprecated** because it needs major refactor and priority is not that high.


#### Possible new features and refactoring

- port backend to Laravel/Lumen services  
- make use of Cache:: facade instead existing caching
- port view to Angular directives or React
- rearrange columns collapsing order in template
- implement share histogram image on twitter
- write tests and docblocks  
- make easier database provisioning with zero rows

# Licence 

Use this only as portfolio item in order to determine my programming expertise level. Do not use or distribute. `nemanja.mitic.elfak@hotmail.com` spring 2015. `readme.md` updated summer 2016.
