<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once(__DIR__ . '/login/config.php');

class limit {

    public $em;

    public function __construct($em) {
        $this->em = $em;
    }

    public static function getUserType() {
        if (!empty($_SESSION["status"]) && $_SESSION["status"] == "verified")
            return "user";
        else
            return "app";
    }

    public static function getKeys() {

        $keys = [CONSUMER_KEY, CONSUMER_SECRET];
        if (limit::getUserType() == "user") {
            //user
            $keys[] = $_SESSION['access_token']['oauth_token'];
            $keys[] = $_SESSION['access_token']['oauth_token_secret'];
        } else {
            //app
            $keys[] = ACCESS_TOKEN;
            $keys[] = ACCESS_TOKEN_SECRET;
        }
        return $keys;
    }

    public function abstractIncApi($apiName, $limit_app, $limit_user, $inc) {
        if (limit::getUserType() == "user")
            return $this->incApi($apiName, $limit_user, $inc);
        else
            return $this->incApiAppDb($apiName, $limit_app, $inc);
    }

    //-1 izaso iz limita//za sve korisnike, pisu u sesiju
    public function incApi($apiName, $limit1, $inc) {
        $arr = [];
        $arr['limit'] = $limit1;
        //ako prvi ili izaslo 15 min, reset
        if (empty($_SESSION[$apiName]) || ($_SESSION[$apiName]['time'] < (new DateTime())->modify("-15 minutes"))) {
            $_SESSION[$apiName]['count'] = 1;
            $_SESSION[$apiName]['time'] = new DateTime(); //samo tu set time
            $arr['spent'] = 1;
            $arr['next_slot'] = (new DateTime())->modify("+15 minutes");
        } else {
            $slot = $_SESSION[$apiName]['time'];
            $slot_copy = clone $slot;
            $arr['next_slot'] = $slot_copy->modify("+15 minutes");
            if (($_SESSION[$apiName]['count'] + 1) > $limit1)
                $arr['spent'] = -1; //izaso iz limita
            else {
                if($inc)
                    $_SESSION[$apiName]['count'] ++;
                
                $arr['spent'] = $_SESSION[$apiName]['count'];
            }
        }
        return $arr;
    }
    //-1 izaso iz limita, broj poziva//za svakog posebno u bazu
    public function incApiAppDb($apiName, $limit1, $inc) {//read only
        $arr = [];
        $arr['limit'] = $limit1;
        
        $limit = $this->get_limit("app", $apiName);
        //ako prvi ili starije od 15 min, reset
        if ($limit == null || ($limit->getCheckpoint() < (new DateTime())->modify("-15 minutes"))) {
            $this->set_limit("app", $apiName, 1, true);
            $arr['spent'] = 1;
            $arr['next_slot'] = (new DateTime())->modify("+15 minutes");
        } else {
            $arr['next_slot'] = $limit->getCheckpoint()->modify("+15 minutes");
            $reqCount = $limit->getReqCount();
            if ($reqCount + 1 > $limit1) {
                $arr['spent'] = -1; //izaso iz limita                
            } else {
                if($inc) {
                    $this->set_limit("app", $apiName, $reqCount + 1);
                    $arr['spent'] = $reqCount + 1;
                } else {
                    $arr['spent'] = $reqCount;
                }
            }
        }
        return $arr;
    }

    public function parseHeader() {
        
    }

    public function get_limit_session($user_id, $api) {
        return $_SESSION[$api]['time'];
    }

    public function get_limit($user_id, $api) {
        try {
            return $limit = $this->em->find('LimitTb', array('userId' => $user_id, "api" => $api));
        } catch (DoctrineException $e) {
            return null;
        }
    }

    public function set_limit($user_id, $api, $reqCount, $reset = false) {
        $limit = $this->get_limit($user_id, $api);
        if ($limit == null)
            $limit = new LimitTb(array("userId" => $user_id, "api" => $api, "checkpoint" => new DateTime(), 'reqCount' => 1));
        else if ($reset) {
            $limit->setCheckpoint(new DateTime());
            $limit->setReqCount(1);
        } else
            $limit->setReqCount($reqCount);

        //$limit->setCheckpoint($checkpoint);//moze samo da se resetuje, ne da se postavi
        $this->em->persist($limit);
        $this->em->flush();
    }

}

/*
print_r($_SESSION);
Array
(
    [access_token] => Array
        (
            [oauth_token] => 1591219418-010Mrh2yG4w93zWFMIDgOJCczbOYuA4izqX2DqO
            [oauth_token_secret] => c5ZB5569Bb9MQZnkwKIOmtTviEXVghkwBUOOYvTKy0kF1
            [user_id] => 1591219418
            [screen_name] => acczasearchapi
        )

    [status] => verified
)

*/





/*
echo "1";
echo "2";
echo "3";
*/



