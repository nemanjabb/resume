
//var staza_do_src = '/1/followswhom';
var staza_do_src = '';

$(document).ready(function () {

    waitingDialog.show('Please wait, collecting data...', {dialogSize: 'm', progressType: 'info'});  
    $("#alert_info").hide();
    //search
    $("#search_btn").click(function () {
        userExists();
    });
    //enter
    $("#screen_name_input").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            userExists();
        }
    });
    //click user link
    $(document).on("click", ".userLink", function () {
        //preventDefault
        var user = $(this).html().substring(1);
        $("#screen_name_input").val("@" + user);
        $("#search_btn").click();
        /*
        $.get(staza_do_src + "/src/following.php", {screen_name: user})
                .done(function (data) {
                    var data1 = $.parseJSON(data);
                    updateUsers(data1);
                });
        */
    });
    
    //get50 radio
    $(document).on('change', 'input:radio[name="recent50"]', function (event) {
        var value = $(this).val();
        $.get(staza_do_src + "/src/following.php", {recent50: value})
                .done(function (data) {
                    getLinks(false);
                });
    });
    
    $(document).ajaxStart(function () {
        //waitingDialog.show('Please wait', {dialogSize: 'm', progressType: 'info'});
    });
    $(document).ajaxStop(function () {
        //waitingDialog.hide();
    });


    $(".login_tw").click(function () {
        window.location.href = staza_do_src +"/src/login/redirect.php";
    });
    $(".row").hide();
    getLinks(true);
    checkLogin();

});
function userExists() {
    var screen_name = $.trim($("#screen_name_input").val().replace(/@/i, ""));
    $.get(staza_do_src + "/src/following.php", {validate_user: screen_name})
            .done(function (data) {
                var data1 = $.parseJSON(data);
                if (!data || !data1.valid) {
                    //$('#screen_name_input').popover({ title: 'Error', content: "There is no such user." , delay: { "hide": 1000 }});
                    $('#screen_name_input').popover('show');
                    setTimeout(function () {
                        $('#screen_name_input').popover('hide');
                    }, 2000);
                } else {
                    getData(screen_name);//zovi servise
                }
            });
}
function checkLogin() {
    $.get(staza_do_src + "/src/login/login.php")
            .done(function (data) {
                data = $.parseJSON(data);

                if (data && data.loged_in) {
                    $("#login_tw_id").html("Log out").attr("href", "../login/logout.php");
                    var avatar = '<a style="padding-top:8px; padding-bottom:8px;"  href="https://twitter.com/' + data.user.screen_name + '"><img style="height:32px; width:32px;" src="' + data.user.avatar + '"/></a>';
                    $("#avatar_li").html(avatar);
                }
//                 else
//                    $("#login_tw").html("Sign in with Twitter").attr("href","../login/redirect.php");               
            });
}

function randomUser(users) {
    var user = getParameterByName("screen_name");
    if (user)
        return user;
    return users[getRandNumber(0, users.length - 1)].screen_name;
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function getRandNumber(minimum, maximum) {
    return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
}

//samo on dovoljan, zove obe api
function handleEchoUserLimit(data) {

    var arr = [];
    var izaso = false;
    var next_slot1;
    if (data.hasOwnProperty('api1')) {
        var obj = {};
        if (data.api1.spent > 0) {
            obj.api = "api1";
            obj.spent = data.api1.spent;
            obj.remaining = data.api1.limit - data.api1.spent;
            obj.total = data.api1.limit;
            arr.push(obj);
        } else {//izaso iz limita
            obj.api = "api1";
            obj.spent = data.api1.limit;
            obj.remaining = 0;
            obj.total = data.api1.limit;
            arr.push(obj);
            izaso = true;
        }
        next_slot1 = moment(data.api1.next_slot.date).tz(data.api1.next_slot.timezone).format('h:mm a');
        $('#next_slot').html(next_slot1);

    }
    var obj2 = print_api_limit(data, "api2");
    arr.push(obj2);
    var obj3 = print_api_limit(data, "api3");
    arr.push(obj3);

    $('#api_info').bootstrapTable('load', arr);
    if (izaso && obj2.izaso && obj3.izaso) {
        if ($("#login_tw_id").html() == "Sign in with Twitter")
            $("#limitSignInModal").modal();
        else {
            $("#limitModal").modal();
            $("#next_slot_modal").html(next_slot1);
        }
    }
}
function print_api_limit(data, api) {
    var obj = {};
    if (data.hasOwnProperty(api)) {
        if (data[api].spent > 0) {
            obj.api = api;
            obj.spent = data[api].spent;
            obj.remaining = data[api].limit - data[api].spent;
            obj.total = data[api].limit;
        } else {
            obj.api = api;
            obj.spent = data[api].limit;
            obj.remaining = 0;
            obj.total = data[api].limit;
            obj.izaso = true;
        }
    }
    return obj;
}

function getData(screen_name) {
    $("#alert_info").show(); 
    $.get(staza_do_src + "/src/following.php", {screen_name: screen_name})
            .done(function (data) {
                var data1 = $.parseJSON(data);
                updateUsers(data1);
                $("#alert_info").hide();
                waitingDialog.hide();               
                $("#row").css('visibility', 'visible').hide().fadeIn();
            });

}
function getLimits() {
    $("#alert_info").show(); 
    $.get(staza_do_src + "/src/following.php", {limits: 1})
            .done(function (data) {
                var data1 = $.parseJSON(data);
                handleEchoUserLimit(data1);
            });

}
function getLinks(firstLoad) {
    $.get(staza_do_src + "/src/following.php", {links: 50})
            .done(function (data) {
                var data1 = $.parseJSON(data);
                $('#table_links').bootstrapTable('load', data1.items);
                if (firstLoad) {
                    $("#screen_name_input").val("@" + randomUser(data1.items));
                    $("#search_btn").click();
                    $(".row").fadeIn();
                }
            });
}
function updateUsers(data1) {
    //handleEchoUserLimit(data1);
    getLimits();
    updateHeadings(data1);
    $('#table_main').bootstrapTable('load', data1.items);
    drawGraph(data1);
    getLinks();
    if ($("#tableCaption").length == 0)
        $($('.fixed-table-toolbar').get(0)).append('<h4 id="tableCaption" style="margin-top:18px;" class="pull-left"> Table ordered by most recently followed</h4>');
    //$("html, body").animate({ scrollTop: $(document).height() }, "fast");
    $("html, body").animate({scrollTop: 0}, "slow");
}

function updateHeadings(data) {
    $("#h_screen_name").html('<a href="https://twitter.com/' + data.screen_name + '">@' + data.screen_name + '</a>');
    $("#h_average").html(calcAverage(data));
    //var datum = moment(data.date.date);
    $("#h_date").html(moment().format("dddd, MMMM Do YYYY, h:mm a"));//now
}

function calcAverage(data) {
    var suma = 0;
    var i;
//    var count1 = 0;
    for (i = 0; i < data.items.length; ++i) {
//        if (data.items[i]['followers_count'] > 30000) 
//            count1 = 30000;  
//        else 
//             count1 = data.items[i]['followers_count'];

        suma += data.items[i]['followers_count'];
        i++;
    }
    return Math.floor(suma / i);
}

function obj2arr(obj) {
    var arr = [];
    for (var j in obj) {
        arr.push([j, obj[j]]);
    }
    return arr;
}
function drawGraph(data) {

    //var users = $.parseJSON(getJson());
    var data1 = raspodeli(data.items);
    data1 = obj2arr(data1);

    $('#grafik').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Share of users @' + data.screen_name + ' followed'
        },
        subtitle: {
            text: '<a href="#">www.last2hours.com/followswhom</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            title: {
                text: 'Number of followers'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Percentage (%)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            //pointFormat: '{point.y:.1f}%'
            formatter: function () {
                return '<b>' + this.y + '</b>% users have ' + this.key + ' followers';

            }
        },
        series: [{
                name: 'Share',
                data: data1,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: 4,
                    y: 10,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
    });
}




function raspodeli(users) {
    //var arr = [];
    var key;
    //zbog redosleda
    var arr = {'0_50': 0, '50_100': 0, '100_200': 0, '200_500': 0, '500_1k': 0, '1k_2k': 0, '2k_5k': 0, '5k_10k': 0, '10k_20k': 0, '20k_50k': 0, '50k_100k': 0, '100k_200k': 0, '200k_500k': 0, '500k_1m': 0, '1m+': 0};


    //objekat ili for in
    users.forEach(function (user) {

        if (user['followers_count'] < 51) {
            key = '0_50';
            arr[key] = inc(arr, key);
            return;
        }
        if (50 < user['followers_count'] && user['followers_count'] < 101) {
            key = '50_100';
            arr[key] = inc(arr, key);
            return;
        }
        if (100 < user['followers_count'] && user['followers_count'] < 201) {
            key = '100_200';
            arr[key] = inc(arr, key);
            return;
        }
        if (200 < user['followers_count'] && user['followers_count'] < 501) {
            key = '200_500';
            arr[key] = inc(arr, key);
            return;
        }
        if (500 < user['followers_count'] && user['followers_count'] < 1001) {
            key = '500_1k';
            arr[key] = inc(arr, key);
            return;
        }
        if (1000 < user['followers_count'] && user['followers_count'] < 2001) {
            key = '1k_2k';
            arr[key] = inc(arr, key);
            return;
        }
        if (2000 < user['followers_count'] && user['followers_count'] < 5001) {
            key = '2k_5k';
            arr[key] = inc(arr, key);
            return;
        }
        if (5000 < user['followers_count'] && user['followers_count'] < 10001) {
            key = '5k_10k';
            arr[key] = inc(arr, key);
            return;
        }
        if (10000 < user['followers_count'] && user['followers_count'] < 20001) {
            key = '10k_20k';
            arr[key] = inc(arr, key);
            return;
        }
        if (20000 < user['followers_count'] && user['followers_count'] < 50001) {
            key = '20k_50k';
            arr[key] = inc(arr, key);
            return;
        }
        if (50000 < user['followers_count'] && user['followers_count'] < 100001) {
            key = '50k_100k';
            arr[key] = inc(arr, key);
            return;
        }
        if (100000 < user['followers_count'] && user['followers_count'] < 200001) {
            key = '100k_200k';
            arr[key] = inc(arr, key);
            return;
        }
        if (200000 < user['followers_count'] && user['followers_count'] < 500001) {
            key = '200k_500k';
            arr[key] = inc(arr, key);
            return;
        }
        if (500000 < user['followers_count'] && user['followers_count'] < 1000001) {
            key = '500k_1m';
            arr[key] = inc(arr, key);
            return;
        }
        if (user['followers_count'] > 1000001) {
            key = '1m+';
            arr[key] = inc(arr, key);
            return;
        }
    });
    return arr;
}
function inc(arr, key) {
    if (arr[key] === undefined) {
        return 1;
    }
    //console.log("key="+key+" arr[key]="+arr[key]);	
    return arr[key] + 1;
}

/*
 <!--
 Desktop:
 [ 2nd col ] [ 1st col ]
 
 Mobile:
 [ 2nd col ]
 [ 1st col ]
 -->
 <div class="row">
 <div class="col-md-6 col-md-push-6">1st col</div>
 <div class="col-md-6 col-md-pull-6">2nd col</div>
 </div>
 https://scotch.io/bar-talk/bootstrap-3-tips-and-tricks-you-might-not-know
 */
















