<?php

require_once(__DIR__ . '../../lib/format.php');

date_default_timezone_set("Europe/Belgrade");

function moj_log($arr) {
    if (!is_array($arr))
        return;
    $file = __DIR__ . "/files/data.jsonmy";
    //file_put_contents($file, $arr, FILE_APPEND);    
    append_json($file, $arr);
    //flush();
}

function append_json($file, $arr) {
    if (file_exists($file)) {
        $data = json_decode(file_get_contents($file), 1);
        
        if (count($data) > 200)
            $data = array_slice($data, 0, 150); //keep at 150
        
        if (count($data) > 2)
            array_unshift($data, $arr); //push at top
        else
            $data[] = $arr;
            file_put_contents($file, json_encode($data));
    }else {
        $data = [];
        $data[] = $arr;
        file_put_contents($file, json_encode($data));
    }
}

?>