<?php

session_start();
require_once(__DIR__ . '/twitteroauth/twitteroauth.php');
require_once(__DIR__ . '/repository.php');
require_once(__DIR__ . "/../bootstrap.php" );
require_once(__DIR__ . "/limit.php" );
require_once(__DIR__ . "/loged/loged.php" );

class twapi {

    public $keys = [];
    public $connection;
    public $screen_name;
    public $limit;

    public function __construct($screen_name, $em) {
        $this->keys = limit::getKeys();
        $this->connection = new TwitterOAuth($this->keys[0], $this->keys[1], $this->keys[2], $this->keys[3]);
        $this->screen_name = $screen_name;
        $this->limit = new limit($em);
    }

    //users/show 180/180 za screen_name
    public function get_ids($count) {
        $params = array('count' => $count,
            'screen_name' => $this->screen_name
        );
        $response = $this->connection->get('friends/ids', $params)['ids'];
        //if ($connection->getLastHttpCode() == 200) {
        if (array_key_exists('errors', $response))
            throw new Exception("Greska u friends/ids1. " . $response['errors'][0]['message']);
        return $response;
    }

    public function get_user_info($screen_name) {
        $params = array('screen_name' => $this->screen_name);
        $response = $this->connection->get('users/show', $params);
        if (array_key_exists('errors', $response))
            throw new Exception("Greska u users/show1. " . $response['errors'][0]['message']);
        return twapi::minimize_user_info($response);
    }

    public static function minimize_user_info($response) {

        $user_info = [];
        $user_info['id_str'] = $response['id_str'];
        $user_info['name'] = $response['name'];
        $user_info['screen_name'] = $response['screen_name'];
        $user_info['location'] = $response['location'];
        $user_info['description'] = $response['description'];
        $user_info['followers_count'] = $response['followers_count'];
        $user_info['friends_count'] = $response['friends_count'];
        $user_info['favourites_count'] = $response['favourites_count'];
        $user_info['statuses_count'] = $response['statuses_count'];
        $user_info['lang'] = $response['lang'];
        $user_info['profile_image_url_https'] = $response['profile_image_url_https'];
        return $user_info;
    }

    /*
      {
      "errors": [{
      "code": 17,
      "message": "No user matches for specified terms."
      }]
      }
     */

    //count < 100
    public function hidrate_ids($comma_ids) {
        $params = array('user_id' => $comma_ids);
        $response = $this->connection->get('users/lookup', $params);
        //file_put_contents(__DIR__ . "/hidrate_ids1.json", json_encode($response));die;
        if (array_key_exists('errors', $response))
            throw new Exception("Greska u users/lookup1. " . $response['errors'][0]['message']);

        return $response;
    }

    public function hidrate_screen_names($comma_names) {
        $params = array('screen_name' => $comma_names);
        $response = $this->connection->get('users/lookup', $params);
        if (array_key_exists('errors', $response))
            throw new Exception("Greska u users/lookup2. " . $response['errors'][0]['message']);
        return $response;
    }

    public function reorder_users($ids, $users) {
        $reordered = [];
        foreach ($ids as $id) {
            foreach ($users as $user) {
                if ($id == $user['id'])
                    $reordered[] = $user;
            }
        }
        return $reordered;
    }

    public static function minimze($users) {
        $user1 = [];
        foreach ($users as $user) {
            $temp = [];
            $temp['profile_image_url'] = $user['profile_image_url'];
            $temp['screen_name'] = $user['screen_name'];
            $temp['name'] = $user['name'];
            $temp['friends_count'] = $user['friends_count'];
            $temp['followers_count'] = $user['followers_count'];
            $temp['id_str'] = $user['id_str'];
            $user1[] = $temp;
        }
        return $user1;
    }

    public function update_with_media($image, $status) {
        $info = getimagesizefromstring($image);
        $mime_type = $info['mime'];
        $filename = $in_reply_id . '.' . end(explode('/', $info['mime']));

        $params = array(
            'status' => $status,
            //'media[]'  => $image,//file_get_contents($image),
            'media[]' => "{$image};type={$mime_type};filename={$filename}",
                //'in_reply_to_status_id' => $in_reply_id
        );

        $response = $this->connection->post('statuses/update_with_media', $params, true); //trebalo true

        if (isset($response['id'])) { //uspeh
            return true;
        }
        return false;
    }

}

class work {

    public $twapi;
    public $rep;
    public $screen_name;

    public function __construct($screen_name, $em) {
        $this->screen_name = $screen_name;
        $this->twapi = new twapi(trim($screen_name), $em);
        $this->rep = new doctrine_rep($em);
    }

    public function get_user_with_api() {

        $ids = $this->twapi->get_ids(100);
        //$ids = array_slice($ids, 0, 50);
        $comma_ids = implode(',', $ids);
        $users = $this->twapi->hidrate_ids($comma_ids);
        $reordered = $this->twapi->reorder_users($ids, $users);

        return $results = twapi::minimze($reordered);
    }

    /*
     * $datetime = DateTime::createFromFormat('g:i:s', $selectedTime);
      $datetime->modify('+15 minutes');
      echo $datetime->format('g:i:s');
     */

    public function get_user_from_api($user) {
        //from api
        $json = $this->get_user_with_api();
        //insert new user
        if ($user == null) {
            if (count($json) < 3)
                die;
            $user_assoc = array('json' => json_encode($json), 'screenName' => $this->screen_name, 'dateChecked' => new DateTime(), 'reqCount' => 1);
            $user1 = $this->rep->insert_user($user_assoc);
            return $user1;
        } else {
            //update user ako postoji
            $user->setReqCount(1);
            $user->setJson(json_encode($json));
            $user->setDateChecked(new DateTime());
            $this->rep->em->persist($user);
            $this->rep->em->flush();
            return $user;
        }//brisi usere koji su stari i nisu trazeni, odr bazu na neki broj
    }

    public function update_user_db($user) {
        try {
            $user->setReqCount($user->getReqCount() + 1); //za rangiranje
            $user->setDateChecked(new DateTime());
            $this->rep->em->persist($user);
            $this->rep->em->flush();
            return $user;
        } catch (DoctrineException $e) {
            throw new Exception("Greska u update_user_db. " + $e->getMessage());
        }
    }

    public function echo_limits() {
        $limits = [];
        try {
            $limits['api3'] = $this->twapi->limit->abstractIncApi('users/show', 180, 180, false);
            $limits['api1'] = $this->twapi->limit->abstractIncApi('friends/ids', 15, 15, false);
            $limits['api2'] = $this->twapi->limit->abstractIncApi('users/lookup', 60, 180, false);
            echo json_encode($limits);
        } catch (Exception $ex) {
            work::log_exception($ex);
        }
    }

    public function echo_user() {

        try {
            $user2 = [];
            $cached = false;
            $user = $this->rep->get_user($this->screen_name); //db  

            $user_info = true;
            $user2['api3'] = $this->twapi->limit->abstractIncApi('users/show', 180, 180, true);
            if ($user2['api3']['spent'] > 0) {
                $user2['user_info'] = $this->twapi->get_user_info($this->screen_name);
            } else {
                $user2['user_info'] = $_SESSION['echo_user']['user_info'];
                $user_info = false;
            }
            //from db
            if ($user != null && $user->getDateChecked() > (new DateTime())->modify("-2 days")) {//mladji od 2 dana           
                $user2['api1'] = $this->twapi->limit->abstractIncApi('friends/ids', 15, 15, false);
                $user2['api2'] = $this->twapi->limit->abstractIncApi('users/lookup', 60, 180, false); //inc=false//ne inkrementiraj           
                $user1 = $this->update_user_db($user); //json from db cache
            } else {
                //from api
                $user2['api1'] = $this->twapi->limit->abstractIncApi('friends/ids', 15, 15, true);
                $user2['api2'] = $this->twapi->limit->abstractIncApi('users/lookup', 60, 180, true); //60app, 180user
                if ($user2['api1']['spent'] > 0 && $user2['api2']['spent'] > 0) {
                    $user1 = $this->get_user_from_api($user); //from api + update
                } else {//izaso iz limita
                    $user2['items'] = $_SESSION['echo_user']['items'];
                    $cached = true;
                }
            }
            //not from session
            if (!$cached) {
                $user2['items'] = json_decode(trim($user1->getJson(), '"'), 1); //mora trim "" na kraju 			
            }
            //$_SESSION['echo_user']['items'] treba da se resetuje, ne mora nije u uslovu
            $cond1 = (!empty($user2['api1']) && $user2['api1']['spent'] > 0 && ($user2['api1']['limit'] - $user2['api1']['spent']) < 2);
            $cond2 = (!empty($user2['api2']) && $user2['api2']['spent'] > 0 && ($user2['api2']['limit'] - $user2['api2']['spent']) < 2);
            if ($cond1 || $cond2 || !$user_info) {//kesiraj u sesiju svaki put
                $_SESSION['echo_user']['items'] = $user2['items'];
                $_SESSION['echo_user']['user_info'] = $user2['user_info'];
            }
            $user2['date'] = $user1->getDateChecked();
            $user2['screen_name'] = $this->screen_name;
            moj_log(work::log_user($user2));
            echo json_encode($user2);
        } catch (Exception $ex) {
            work::log_exception($ex);
        }
    }

    public function echo_links() {
        try {
            $user2 = [];
            $rep = new doctrine_rep($this->rep->em);
            if (empty($_SESSION['recent50']))
                $_SESSION['recent50'] = "recent"; //default
            $sc_names = $rep->get_top50(10, $_SESSION['recent50']);
            $arr = [];
            foreach ($sc_names as $name) {
                $arr[] = $name['screenName'];
            }
            $comma_str = implode(',', $arr);
            $user2['api2'] = $this->twapi->limit->abstractIncApi('users/lookup', 60, 180, true);
            if ($user2['api2']['spent'] > 0) {
                $users = $this->twapi->hidrate_screen_names($comma_str);
                $user2['items'] = twapi::minimze($users);
                if (($user2['api2']['limit'] - $user2['api2']['spent']) < 2)//kesiraj u sesiju
                    $_SESSION['echo_links']['items'] = $user2['items'];
            } else {
                $user2['items'] = $_SESSION['echo_links']['items'];
            }
            echo json_encode($user2);
        } catch (Exception $ex) {
            work::log_exception($ex);
        }
    }

    public static function log_exception($ex) {
        $arr1 = [];
        $arr1['api1']['spent'] = $ex->getMessage();
        moj_log(work::log_user($arr1));
    }

    public static function log_user($user) {
        $user2 = [];
        $user2['username'] = limit::getUserType();
        array_key_exists('screen_name', $user) ? $user2['search'] = $user['screen_name'] : $user2['search'] = '';
        (array_key_exists('api1', $user) && array_key_exists('spent', $user['api1'])) ? $user2['api1'] = $user['api1']['spent'] : $user2['api1'] = '';
        (array_key_exists('api2', $user) && array_key_exists('spent', $user['api2'])) ? $user2['api2'] = $user['api2']['spent'] : $user2['api2'] = '';
        (array_key_exists('api3', $user) && array_key_exists('spent', $user['api3'])) ? $user2['api3'] = $user['api3']['spent'] : $user2['api3'] = '';
        (array_key_exists('api1', $user) && array_key_exists('next_slot', $user['api1'])) ? $user2['slot'] = $user['api1']['next_slot']->format('H:i') : $user2['slot'] = '';
        $user2['ip'] = $_SERVER['REMOTE_ADDR'];
        $user2['time'] = date('H:i:s d.m.Y');
        return $user2;
    }

    public static function validate_username($username) {
        $username = trim($username, '@');
        if (!preg_match('/^[A-Za-z0-9_]{1,15}$/', $username))
            return false;
        return work::if_curl_postoji("https://twitter.com/" . $username);
    }

    public static function if_curl_postoji($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_NOBODY, true); //ovo je head zahtev
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); //to
        $result = curl_exec($curl);
        $ret = false;

        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }
        curl_close($curl);
        return $ret;
    }

}

class kontroler {

    public function __construct($post, $em) {
        //if(empty($post)) die;

        if (array_key_exists('screen_name', $post)) {
            //curl da li postoji user
            $case = 1;
        } else if (array_key_exists('recent50', $post)) {
            $case = 4;
        } else if (array_key_exists('validate_user', $post)) {
            $case = 3;
        } else if (empty($post) || array_key_exists('links', $post)) {
            $case = 2;
        } else if (array_key_exists('limits', $post)) {
            $case = 5;
        }
        switch ($case) {

            case 1:
                $w = new work($post['screen_name'], $em);
                $w->echo_user();
                break;
            case 2:
                $w = new work("bilo_sta", $em);
                $w->echo_links();
                break;
            case 3:
                echo json_encode(array("valid" => work::validate_username($post['validate_user'])));
                break;
            case 4:
                $_SESSION['recent50'] = $post['recent50'];
                break;
            case 5:
                $w = new work("bilo_sta", $em);
                $w->echo_limits();
                break;
            default:
                //code block default;
                break;
        }
    }

}

//kontroler($_POST);
new kontroler($_GET, $em);
//sad bootstrap
//da li postoji screen_name curl


//http://www.highcharts.com/demo/column-rotated-labels


/*
 * 
 datumi poredjenje 2 dana kesh iz baze
 share slike
 sign in with twitter
 brojac poziva api
 random ucitavanje pocetne stranice
 prosek u js
 ajax load modal please wait
 cuvas prosek uz korisnika
 kljuc korisnika u bazu za token da bi brojao api, u $_SESSION ide a ne u bazu
 na read i write se resetuju useri kojima izaslo 15 min. useseiju mozes i da brojis zahteve, ne mora baza
 skrol za tabelu
 keshiraj zadnji zahtev za offline za limit
 -------------
 obavestenje da si izaso iz api limit, handleLimit() js
 bekap zadnji poziv app key value, logovan u sesiji//kesiraj ga u sesiju iako je iz baze limit
 baza brise stare protocni red
 share rezultat api modal 
 visibility hidden na row
 procedura triger na velicinu
 -----------
log limita i usera i share jos
i visibility
-----------------------
user, search, api1 api2 spent/total, slot, time, ip, css limit
json i bootstrap tabela
=================
april
 
radio za recently i most viewed, iz sesije, a na change radio salje ajax da promeni u sesiji
share
boje <tr> prema crveno na broj pratilaca
----------
    hendlovanje gresaka api i db, da pise u log tabelu postojecu
    users/show api, avatar i detalji
    httaccess
    visibility hidden za row ondomready, scrollintoview mesto fadein
share resultata za logovanog, zove servis za sliku
	avatari linkuju na profile
instaliraj git i uploaduj private rep na bitbuket
    sifra na log
    tw aplikacija link i privilegije, prvo domen
    izbaci index.html u root
pocisti visak fajlova
jos jedan mali modal dok dovlaci  
 * 
 Order allow,deny
Deny from all
<FilesMatch index\.html>
        Allow from all
</FilesMatch>
 * /var/log/apache2/error.log
 * 
 */
/*
razdvoj go_back od autoresponder, usisavanje liste, odvojeni fajlovi i klase
izuzeci u db i api klasama
log preko json i tabele, samobrisuci
html bootstrap tabela interfejs za usisavanje
bootstrap css globalni negde
test folderi i fajlovi
mapiranje po tutorijalu
stranica sa tajmlajnima i overide css
poredjenje datuma treba bolje mozda
init app db izdvojeno
beleske za svaki projekat
komentari na fjama, sta radi, ulaz, izlaz, izuzeci
beleske i config na git i bitbuket
sifre poso i druge odvojeno i na jednom mestu
netbeans radi preko sftp tj ssh direktno
notepad++ nppftp plugin mesto nano
*/
//following
//20 april
	//modal za nepostojeceg usera, popover, servis username exists
	//calculating google modal, alert bootstrap
//loging modul
//desna kolona da bezi gore 
// ../../ relativne staze
//promeni strukturu foldera
//26 april
//asinhroni delovi stranice, nekad treba da budu objedinjeni na nivou stranice npr
//odvojeni servis za get limits tabelu, dekaplirani servisi za sve
//jos user_info da se odvoji u servis i kesiranje handling da se odvoji















