<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once(__DIR__ . '/../twitteroauth/twitteroauth.php');
require_once(__DIR__ . '/config.php');

echo_login();

//ajax poziva da vidi da li je sesija napunjena tj da li je logovan ili nije
function echo_login() {

    $arr = [];
    $connection = null;

    //nije logovan
    if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
        //eto gde unistavam sesiju
        $_SESSION = array();
        $arr['loged_in'] = false; //Sign in with Twitter
    } else {
        //logovan
        $access_token = $_SESSION['access_token'];

        /* Create a TwitterOauth object with consumer/user tokens. */
        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
        //odstampaj loged in dugme
        $arr['loged_in'] = true;
    }

    if ($connection != null) {
        if (!isset($_SESSION['user'])) {
            $arr['user'] = proveri_usera($connection);
        }
    }
    //$arr["sesija"] = $_SESSION;
    echo json_encode($arr);
}

function proveri_usera($connection) {
    $response = $connection->get('account/verify_credentials');
    if (empty($response)) {
        echo 'los user';
        return null;
    }

    $user['id_str'] = $response['id_str'];
    $user['screen_name'] = $response['screen_name'];
    $user['name'] = $response['name'];
    $user['avatar'] = $response['profile_image_url'];
    return $user;
}

?>