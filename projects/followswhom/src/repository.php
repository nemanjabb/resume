<?php

class doctrine_rep {

    public $em;

    public function __construct($em) {
        $this->em = $em;
    }

    public function get_user($screen_name) {
        try {
            return $user = $this->em->find('UserTb', array("screenName" => $screen_name));
        } catch (DoctrineException $e) {
            //nije greska, nema usera
            return null;
            //throw new Exception("Greska u get_user. "+$e->getMessage()); 
        }
    }

    public function insert_user($user_assoc) {
        try {
            $user = new UserTb($user_assoc); //item assoc arr            
            $this->em->persist($user);
            $this->em->flush();
            return $user;
        } catch (DoctrineException $e) {
            throw new Exception("Greska u insert_user. ".$e->getMessage());
        }
    }

    public function delete_user($screen_name) {
        try {
            $user = $this->em->find('UserTb', array("screenName" => $screen_name));
            $this->em->remove($user);
            $this->em->flush();
        } catch (DoctrineException $e) {
            throw new Exception("Greska u delete_user. ".$e->getMessage());
        }
    }

    public function get_top50($count, $recent) {
        $this->delete_bottom50();
        try {
            $q1 = 'SELECT u.screenName FROM UserTb u ORDER BY u.dateChecked DESC';
            if($recent != "recent")
                $q1 = 'SELECT u.screenName FROM UserTb u ORDER BY u.reqCount DESC';//wanted

            $query = $this->em->createQuery($q1)->setMaxResults($count);
            return $users = $query->getResult(); // array 
        } catch (DoctrineException $e) {
            throw new Exception("Greska u get_top50. ".$e->getMessage());
        }
    }

    public function delete_bottom50() {
        try {
            $query = $this->em->createQuery('SELECT COUNT(u.screenName) FROM UserTb u');
            $count = $query->getSingleScalarResult();
            if ($count < 1050)//1050
                return;

            $days10 = (new DateTime())->modify("-10 days");
            $query = $this->em->createQuery('SELECT u FROM UserTb u WHERE u.reqCount < 2 AND u.dateChecked < :d10 ORDER BY u.dateChecked ASC')
                    ->setParameter('d10', $days10)
                    ->setMaxResults(50);
            $users = $query->getResult();
            $this->em->remove($users);
            $this->em->flush();     
        } catch (DoctrineException $e) {
            throw new Exception("Greska u delete_bottom50. ".$e->getMessage());
        }
    }

}

//$rep = new doctrine_rep($em);



