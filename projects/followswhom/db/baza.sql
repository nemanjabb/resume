
/*
	json
	datum provere
	screen_name, json, datum, br pregleda
	mentioni
	favourites
	br zahteva u 15 min
	log
*/
CREATE TABLE IF NOT EXISTS user_tb (			
    screen_name     VARCHAR(255) PRIMARY KEY,             
    json            VARCHAR(20000) NOT NULL,
    date_checked    DATETIME     NOT NULL,
    req_count       INT(10)      NOT NULL  
	/*prosek INT(10) NOT NULL */
);

CREATE TABLE IF NOT EXISTS limit_tb (
    user_id         VARCHAR(50), 
    api             VARCHAR(50),
    req_count       INT(10) NOT NULL,   /*br u 15 min*/
    checkpoint      DATETIME NOT NULL,  /*15min	*/	
    PRIMARY KEY	(key1, api)
);

